#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "oceanFFT.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew64.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int Initialize(void);
void UnInitialize(void);
void Display(void);
void Update(void);
void Resize(int width, int height);



//
// OpenGL Context Related Variables
//
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;
//
// FullScreen Related Variables
//
DWORD pep_gdwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool pep_gbActiveWindow = false;
bool pep_gbIsFullScreen = false;

bool pep_Ocean_gbAnimate = true;
float pep_Ocean_gAnimationTime = 0.0f;
float pep_Ocean_gPrevTime = 0.0f;
float pep_Ocean_gAnimationRate = -0.001f;

// Mouse controls
int pep_gMouseOldX;
int pep_gMouseOldY;
float pep_Ocean_gRotateX = 20.0f;
float pep_Ocean_gRotateY = 0.0f;
float pep_Ocean_gTranslateX = 0.0f;
float pep_Ocean_gTranslateY = 0.0f;
float pep_Ocean_gTranslateZ = -2.0f;

StopWatchInterface *pep_Ocean_gTimer = NULL;

bool pep_Ocean_gbDrawPoints = false;

int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpszCmdLine,
	int iCmdShow)
{

	//
	// Variable Declarations
	//
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("CUDA FFT Ocean Simulation");

	//
	// Code
	//
	if (0 != fopen_s(&pep_gpFile, "Log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created/Opend Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName, 
		TEXT("CUDA FFT Ocean Simulation"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();

	if (0 != iRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Failed\n", __FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}

	SetTimer(pep_gHwnd, PEP_OCEAN_REFRESH_DELAY_ID, PEP_OCEAN_REFRESH_DELAY, NULL);

	while (false == bDone) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} 
		else
		{
			if (pep_gbActiveWindow)
			{
			}
			Update();
			Display();
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	void ToggledFullScreen(void);

	//
	// Code
	//
	switch (iMsg)
	{
	case WM_MOUSEMOVE:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		float dx, dy;
		dx = (float)(x - pep_gMouseOldX);
		dy = (float)(y - pep_gMouseOldY);

		switch (wParam)
		{
		case MK_LBUTTON:
			pep_Ocean_gRotateX += dy * 0.2f;
			pep_Ocean_gRotateY += dx * 0.2f;
			break;

		case MK_MBUTTON:
			pep_Ocean_gTranslateX += dx * 0.01f;
			pep_Ocean_gTranslateY -= dy * 0.01f;
			break;

		case MK_RBUTTON:
			pep_Ocean_gTranslateZ += dy * 0.01f;
			break;
		}

		pep_gMouseOldX = x;
		pep_gMouseOldY = y;
	}
	break;

	case WM_LBUTTONDOWN:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		int button = 0;
		switch (wParam)
		{
		case MK_LBUTTON:
			button = 0;
			break;

		case MK_MBUTTON:
			button = 1;
			break;

		case MK_RBUTTON:
			button = 2;
			break;
		}

		pep_gMouseOldX = x;
		pep_gMouseOldY = y;
	}
	break;

	case WM_LBUTTONUP:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		pep_gMouseOldX = x;
		pep_gMouseOldY = y;
	}
	break;

	case WM_TIMER:
	{
		switch (wParam)
		{
		case PEP_OCEAN_REFRESH_DELAY_ID:
		{
			float time = sdkGetTimerValue(&pep_Ocean_gTimer);

			if (pep_Ocean_gbAnimate)
			{
				pep_Ocean_gAnimationTime += (time - pep_Ocean_gPrevTime) * pep_Ocean_gAnimationRate;
			}

			pep_Ocean_gPrevTime = time;

			SetTimer(pep_gHwnd, PEP_OCEAN_REFRESH_DELAY_ID, PEP_OCEAN_REFRESH_DELAY   , NULL);
		}
		break;
		}
	}
	break;

	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;

	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KILLFOCUS:
		pep_gbActiveWindow = false;
		break;

	case WM_SETFOCUS:
		pep_gbActiveWindow = true;
		break;
	case WM_KEYDOWN:
	{
		switch (wParam) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'F':
		case 'f':
			ToggledFullScreen();
			break;

		case 'W':
		case 'w':
			if (pep_Ocean_gbDrawPoints)
			{
				pep_Ocean_gbDrawPoints = false;
			}
			else
			{
				pep_Ocean_gbDrawPoints = true;
			}
			break;
		}
	}
	break;
	}  // End Of Switch Case

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbIsFullScreen)
	{
		pep_gdwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_gdwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gdwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		pep_gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		pep_gbIsFullScreen = false;
		ShowCursor(TRUE);
	}

	return;
}

int Initialize(void)
{
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	//
	// Code
	//
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	pep_gHdc = GetDC(pep_gHwnd);

	index = ChoosePixelFormat(pep_gHdc, &pfd);
	if (0 == index)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : ChoosePixelFormat Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd))
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : SetPixelFormat Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	GLenum result;

	result = glewInit();
	if (GLEW_OK != result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Successful\n", __FILE__, __LINE__, __FUNCTION__);

	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);

	Ocean_Initialize();
}

void UnInitialize(void)
{
	Ocean_UnInitialize();

	if (wglGetCurrentContext() == pep_gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
	}

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}
}

void Update(void)
{
	Ocean_Update();
}

void Display(void)
{
	Ocean_Display();
	SwapBuffers(pep_gHdc);
}
void Resize(int width, int height)
{
	Ocean_Resize(width, height);
}
