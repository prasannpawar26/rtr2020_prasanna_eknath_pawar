#include "vmath.h"
#include "oceanFFT.h"

using namespace std;
using namespace vmath;

extern float pep_Ocean_gRotateX;
extern float pep_Ocean_gRotateY;
extern float pep_Ocean_gTranslateX;
extern float pep_Ocean_gTranslateY;
extern float pep_Ocean_gTranslateZ;

extern float pep_Ocean_gAnimationTime;
extern float pep_Ocean_gPrevTime;
extern float pep_Ocean_gAnimationRate;
extern bool pep_Ocean_gbAnimate;

extern bool pep_Ocean_gbDrawPoints;
extern StopWatchInterface *pep_Ocean_gTimer;
extern FILE *pep_gpFile;

enum {
	PEP_ATTRIBUTES_POSITION = 0,
	PEP_ATTRIBUTES_COLOR,
	PEP_ATTRIBUTES_NORMAL,
	PEP_ATTRIBUTES_TEXCOORD0,
	PEP_ATTRIBUTES_TEXCOORD1
};

//Cuda kernels Wrappers Forward Declarations
extern "C" void Ocean_CudaGenerateSpectrumKernel(float2 *, float2 *, unsigned int, unsigned int, unsigned int, float, float);
extern "C" void Ocean_CudaUpdateHeightmapKernel(float *, float2 *, unsigned int, unsigned int,bool);
extern "C" void Ocean_CudaCalculateSlopeKernel(float *, float2 *, unsigned int, unsigned int);

const unsigned int pep_Ocean_gMeshSize = 2048/*256*/;
const unsigned int pep_Ocean_gSpectrumWidth = pep_Ocean_gMeshSize + 4;
const unsigned int pep_Ocean_gSpectrumHeight = pep_Ocean_gMeshSize + 1;
const int pep_Ocean_gFrameCompare = 4;

unsigned int pep_Ocean_gWindowWidth = 1920, pep_Ocean_gWindowHeight = 1080;

vmath::mat4 pep_gProjectionMatrix;

// Shader Related Variables
GLuint pep_Ocean_gVertexShaderObject;
GLuint pep_Ocean_gFragmentShaderObject;
GLuint pep_Ocean_gShaderProgramObject;

// Uniforms
GLuint pep_Ocean_gUniformModelMatrix;
GLuint pep_Ocean_gUniformViewMatrix;
GLuint pep_Ocean_gUniformProjectionMatrix;
GLuint pep_Ocean_gUniformHeightScale;
GLuint pep_Ocean_gUniformChopiness;
GLuint pep_Ocean_gUniformSize;
GLuint pep_Ocean_gUniformDeepColor;
GLuint pep_Ocean_gUniformShallowColor;
GLuint pep_Ocean_gUniformSkyColor;
GLuint pep_Ocean_gUniformLightDir;

// OpenGL vertex buffers
GLuint pep_Ocean_gVertexPositionBuffer;
GLuint pep_Ocean_gVertexHeightBuffer;
GLuint pep_Ocean_gVertexSlopeBuffer;

// Cuda Introp Related Variables - Handles OpenGL-CUDA Exchange
struct cudaGraphicsResource *pep_Ocean_gCudaResourceVertexHeightBuffer;
struct cudaGraphicsResource *pep_Ocean_gCudaResourceVertexSlopeBuffer;

GLuint pep_Ocean_gIndexBuffer;

bool pep_Ocean_gbWireFrame = false;
bool pep_Ocean_gbHasDouble = false;

// FFT data
cufftHandle pep_Ocean_gFFTPlan;
float2 *pep_Ocean_gHeightFieldDeltaAtTime0 = 0;   // heightfield at time 0
float2 *pep_Ocean_gH0 = 0;
float2 *pep_Ocean_gHeightFieldDeltaAtTimeT = 0;   // heightfield at time t
float2 *pep_Ocean_gDeltaSlope = 0;

// pointers to device object
float *pep_Ocean_gHptr = NULL;
float2 *pep_Ocean_gSptr = NULL;

// simulation parameters
const float pep_Ocean_gGravitationalConstant = 9.81f;
const float pep_Ocean_gWaveScaleFactor = 1e-7f;
const float pep_Ocean_gPatchSize = 100;
float pep_Ocean_gWindSpeed = 100.0f;
float pep_Ocean_gWindDirection = CUDART_PI_F/3.0f;
float pep_Ocean_gDirectionDepend = 0.07f;

// Auto-Verification Code
const int pep_Ocean_gFrameCheckNumber = 4;
int pep_Ocean_gFpsCount = 0;        // FPS count for averaging
int pep_Ocean_gFpsLimit = 1;        // FPS limit for sampling
unsigned int pep_Ocean_gFrameCount = 0;
unsigned int pep_Ocean_gTotalErrors = 0;

float Ocean_Urand(void)
{
	//
	// Code
	//
	return rand() / (float)RAND_MAX;
}

//
// Generates Gaussian random number with mean 0 and standard deviation 1.
//
float Ocean_Gauss(void)
{
	//
	// Function Declarations
	//
	float Ocean_Urand(void);

	//
	// Variable Declarations
	//
	float u1 = Ocean_Urand();
	float u2 = Ocean_Urand();

	//
	// Code
	//
	if (u1 < 1e-6f)
	{
		u1 = 1e-6f;
	}

	return sqrtf(-2 * logf(u1)) * cosf(2*CUDART_PI_F * u2);
}

// Ocean_Phillips spectrum
// (Kx, Ky) - normalized wave vector
// Vdir - wind angle in radians
// V - wind speed
// A - constant
float Ocean_Phillips(float Kx, float Ky, float windDirection, float windSpeed, float waveScaleFactor, float directionDepend)
{
	//
	// Variable Declarations
	//
	float k_squared = Kx * Kx + Ky * Ky;

	//
	// Code
	//
	if (k_squared == 0.0f)
	{
		return 0.0f;
	}

	// largest possible wave from constant wind of velocity v
	float L = windSpeed * windSpeed / pep_Ocean_gGravitationalConstant;

	float k_x = Kx / sqrtf(k_squared);
	float k_y = Ky / sqrtf(k_squared);
	float w_dot_k = k_x * cosf(windDirection) + k_y * sinf(windDirection);

	float phillips = waveScaleFactor * expf(-1.0f / (k_squared * L * L)) / (k_squared * k_squared) * w_dot_k * w_dot_k;

	// filter out waves moving opposite to wind
	if (w_dot_k < 0.0f)
	{
		phillips *= directionDepend;
	}

	// damp out waves with very small length w << l
	/*float w = L / 10000;
	phillips *= expf(-k_squared * w * w);*/

	return phillips;
}

//
// Generate base heightfield in frequency space
//
void Ocean_Generate_h0(float2 *h0)
{
	//
	// Function Declarations
	//
	float Ocean_Phillips(float, float, float, float, float, float);
	float Ocean_Gauss(void);

	//
	// Code
	//
	for (unsigned int y = 0; y<=pep_Ocean_gMeshSize; y++)
	{
		for (unsigned int x = 0; x<=pep_Ocean_gMeshSize; x++)
		{
			float kx = (-(int)pep_Ocean_gMeshSize / 2.0f + x) * (2.0f * CUDART_PI_F / pep_Ocean_gPatchSize);
			float ky = (-(int)pep_Ocean_gMeshSize / 2.0f + y) * (2.0f * CUDART_PI_F / pep_Ocean_gPatchSize);

			float P = sqrtf(Ocean_Phillips(kx, ky, pep_Ocean_gWindDirection, pep_Ocean_gWindSpeed, pep_Ocean_gWaveScaleFactor, pep_Ocean_gDirectionDepend));

			if (kx == 0.0f && ky == 0.0f)
			{
				P = 0.0f;
			}

			//float Er = urand()*2.0f-1.0f;
			//float Ei = urand()*2.0f-1.0f;
			float Er = Ocean_Gauss();
			float Ei = Ocean_Gauss();

			float h0_re = Er * P * CUDART_SQRT_HALF_F;
			float h0_im = Ei * P * CUDART_SQRT_HALF_F;

			int i = y* pep_Ocean_gSpectrumWidth + x;
			h0[i].x = h0_re;
			h0[i].y = h0_im;
		}
	}

	return;
}


void Ocean_RunCuda(void)
{
	//
	// Variable Declarations
	//
	size_t num_bytes;

	//
	// Code
	//

	// generate wave spectrum in frequency domain
	Ocean_CudaGenerateSpectrumKernel(
		pep_Ocean_gHeightFieldDeltaAtTime0,
		pep_Ocean_gHeightFieldDeltaAtTimeT,
		pep_Ocean_gSpectrumWidth,
		pep_Ocean_gMeshSize,
		pep_Ocean_gMeshSize,
		pep_Ocean_gAnimationTime,
		pep_Ocean_gPatchSize);

	// execute inverse FFT to convert to spatial domain
	cufftExecC2C(pep_Ocean_gFFTPlan, pep_Ocean_gHeightFieldDeltaAtTimeT, pep_Ocean_gHeightFieldDeltaAtTimeT, CUFFT_INVERSE);

	// update heightmap values in vertex buffer
	cudaGraphicsMapResources(1, &pep_Ocean_gCudaResourceVertexHeightBuffer, 0);
	cudaGraphicsResourceGetMappedPointer((void **)&pep_Ocean_gHptr, &num_bytes, pep_Ocean_gCudaResourceVertexHeightBuffer);

	// Execute Kernel
	Ocean_CudaUpdateHeightmapKernel(pep_Ocean_gHptr, pep_Ocean_gHeightFieldDeltaAtTimeT, pep_Ocean_gMeshSize, pep_Ocean_gMeshSize, false);

	// calculate slope for shading
	cudaGraphicsMapResources(1, &pep_Ocean_gCudaResourceVertexSlopeBuffer, 0);
	cudaGraphicsResourceGetMappedPointer((void **)&pep_Ocean_gSptr, &num_bytes, pep_Ocean_gCudaResourceVertexSlopeBuffer);

	// Execute Kernel
	Ocean_CudaCalculateSlopeKernel(pep_Ocean_gHptr, pep_Ocean_gSptr, pep_Ocean_gMeshSize, pep_Ocean_gMeshSize);

	cudaGraphicsUnmapResources(1, &pep_Ocean_gCudaResourceVertexHeightBuffer, 0);
	cudaGraphicsUnmapResources(1, &pep_Ocean_gCudaResourceVertexSlopeBuffer, 0);

	return;
}

//void computeFPS()
//{
//    frameCount++;
//    fpsCount++;
//
//    if (fpsCount == fpsLimit) {
//        fpsCount = 0;
//    }
//}


float light_direction = 1.0f;
void Ocean_Display()
{
	//
	// Function Declarations
	//
	void Ocean_RunCuda(void);

	//
	// Code
	//


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	vmath::mat4 modelMatrix = mat4::identity();
	vmath::mat4 viewMatrix = mat4::identity();

	modelMatrix =
		modelMatrix *
		translate(pep_Ocean_gTranslateX, pep_Ocean_gTranslateY, pep_Ocean_gTranslateZ) *
		rotate(pep_Ocean_gRotateX, 1.0f, 0.0f, 0.0f) *
		rotate(pep_Ocean_gRotateY, 0.0f, 1.0f, 0.0f);

	glUseProgram(pep_Ocean_gShaderProgramObject);

	glUniformMatrix4fv(pep_Ocean_gUniformModelMatrix, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_Ocean_gUniformViewMatrix, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_Ocean_gUniformProjectionMatrix, 1, GL_FALSE, pep_gProjectionMatrix);
	glUniform1f(pep_Ocean_gUniformHeightScale, 0.050f);
	glUniform1f(pep_Ocean_gUniformChopiness, 1.0f);
	glUniform2f(pep_Ocean_gUniformSize, (float)pep_Ocean_gMeshSize, (float)pep_Ocean_gMeshSize);
	glUniform4f(pep_Ocean_gUniformDeepColor, 0.0f, 0.4f, 0.6f, 1.0f);
	glUniform4f(pep_Ocean_gUniformShallowColor, 0.1f, 0.3f, 0.3f, 1.0f);
	glUniform4f(pep_Ocean_gUniformSkyColor, 1.0f, 1.0f, 1.0f, 1.0f);
	glUniform3f(pep_Ocean_gUniformLightDir, 0.0f, light_direction, 0.0f);

	if (pep_Ocean_gbDrawPoints)
	{
		glDrawArrays(GL_POINTS, 0, pep_Ocean_gMeshSize * pep_Ocean_gMeshSize);
	}
	else
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Ocean_gIndexBuffer);

		glPolygonMode(GL_FRONT_AND_BACK, pep_Ocean_gbWireFrame ? GL_LINE : GL_FILL);
		glDrawElements(GL_TRIANGLE_STRIP, ((pep_Ocean_gMeshSize * 2) + 2) * (pep_Ocean_gMeshSize - 1), GL_UNSIGNED_INT, 0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	glUseProgram(0);



	//if (light_direction > 0.0f)
	//{
	//	light_direction -= 0.0005f;
	//}

	//computeFPS();

	return;
}

void Ocean_Resize(int width, int height)
{
	//
	// Code
	//
	glViewport(0, 0, width, height);

	pep_gProjectionMatrix = mat4::identity();

	pep_gProjectionMatrix =
		perspective(60.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);

	pep_Ocean_gWindowWidth = width;
	pep_Ocean_gWindowHeight = height;

	return;
}

GLint Ocean_InitializeShaderProgram(void)
{
	//
	// Code
	//
	pep_Ocean_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_Ocean_gVertexShaderObject)
	{
		return -1;
	}

	const GLchar *ocean_vertexShaderSourceCode =

		"#version 450 core" \
		"\n" \
		"in vec4 Ocean_vPosition;" \
		"in float Ocean_vTexCoord0;" \
		"in vec2 Ocean_vTexCoord1;" \

		"out vec3 eyeSpacePos;" \
		"out vec3 worldSpaceNormal;"\
		"out vec3 eyeSpaceNormal;" \

		"uniform mat4 u_ocean_modelMatrix;" \
		"uniform mat4 u_ocean_viewMatrix;" \
		"uniform mat4 u_ocean_projectionMatrix;" \

		"uniform float u_ocean_heightScale; "\
		"uniform float u_ocean_chopiness;" \
		"uniform vec2  u_ocean_size;"

		"void main()" \
		"{" \
			"float height     = Ocean_vTexCoord0.x;" \
			"vec2  slope      = Ocean_vTexCoord1.xy;" \

			"vec3 normal      = normalize(cross( vec3(0.0, slope.y*u_ocean_heightScale, 2.0 / u_ocean_size.x), vec3(2.0 / u_ocean_size.y, slope.x*u_ocean_heightScale, 0.0)));" \
			"worldSpaceNormal = normal;" \


			"vec4 pos         = vec4(Ocean_vPosition.x, height * u_ocean_heightScale, Ocean_vPosition.z, 1.0);" \
			"gl_Position      = u_ocean_projectionMatrix * u_ocean_viewMatrix * u_ocean_modelMatrix * pos;" \

			"eyeSpacePos      = (u_ocean_viewMatrix * u_ocean_modelMatrix * pos).xyz;" \
			"eyeSpaceNormal   = (transpose(inverse(mat3(u_ocean_viewMatrix * u_ocean_modelMatrix))) * normal).xyz;" \
		"}";

	glShaderSource(pep_Ocean_gVertexShaderObject, 1, (const GLchar **)&ocean_vertexShaderSourceCode, NULL);
	glCompileShader(pep_Ocean_gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Ocean_gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {

		glGetShaderiv(pep_Ocean_gVertexShaderObject, GL_INFO_LOG_LENGTH,
			&iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Ocean_gVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_Ocean_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_Ocean_gFragmentShaderObject) {
		return -1;
	}

	const GLchar *ocean_fragmentShaderSourceCode =

		"#version 450 core"
		"\n"

		"in vec3 eyeSpacePos;" \
		"in vec3 worldSpaceNormal;" \
		"in vec3 eyeSpaceNormal;" \

		"uniform vec4 u_ocean_deepColor;" \
		"uniform vec4 u_ocean_shallowColor;" \
		"uniform vec4 u_ocean_skyColor;" \
		"uniform vec3 u_ocean_lightDir;" \

		"out vec4 FragColor;" \

		"void main()" \
		"{" \
			"vec3 eyeVector              = normalize(eyeSpacePos);" \
			"vec3 eyeSpaceNormalVector   = normalize(eyeSpaceNormal);" \
			"vec3 worldSpaceNormalVector = normalize(worldSpaceNormal);" \
			"float facing    = max(0.0, dot(eyeSpaceNormalVector, -eyeVector));" \
			"float fresnel   = pow(1.0 - facing, 5.0);" \
			"float diffuse   = max(0.0, dot(worldSpaceNormalVector, u_ocean_lightDir));" \
			"vec4 waterColor = u_ocean_deepColor;" \
			"FragColor = waterColor*diffuse + u_ocean_skyColor*fresnel;" \
		"}";

	glShaderSource(
		pep_Ocean_gFragmentShaderObject,
		1,
		(const GLchar **)&ocean_fragmentShaderSourceCode,
		NULL);

	glCompileShader(pep_Ocean_gFragmentShaderObject);

	glGetShaderiv(
		pep_Ocean_gFragmentShaderObject,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(
			pep_Ocean_gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(
					pep_Ocean_gFragmentShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	pep_Ocean_gShaderProgramObject = glCreateProgram();

	glAttachShader(pep_Ocean_gShaderProgramObject, pep_Ocean_gVertexShaderObject);
	glAttachShader(pep_Ocean_gShaderProgramObject, pep_Ocean_gFragmentShaderObject);

	glBindAttribLocation(pep_Ocean_gShaderProgramObject, PEP_ATTRIBUTES_POSITION,
		"Ocean_vPosition");
	glBindAttribLocation(pep_Ocean_gShaderProgramObject, PEP_ATTRIBUTES_TEXCOORD0,
		"Ocean_vTexCoord0");
	glBindAttribLocation(pep_Ocean_gShaderProgramObject, PEP_ATTRIBUTES_TEXCOORD1,
		"Ocean_vTexCoord1");

	glLinkProgram(pep_Ocean_gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(
		pep_Ocean_gShaderProgramObject,
		GL_LINK_STATUS,
		&iProgramLinkStatus);

	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(
			pep_Ocean_gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(
					pep_Ocean_gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	pep_Ocean_gUniformModelMatrix = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_modelMatrix");
	pep_Ocean_gUniformViewMatrix = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_viewMatrix");
	pep_Ocean_gUniformProjectionMatrix = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_projectionMatrix");
	pep_Ocean_gUniformHeightScale = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_heightScale");
	pep_Ocean_gUniformChopiness   = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_chopiness");
	pep_Ocean_gUniformSize        = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_size");
	pep_Ocean_gUniformDeepColor = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_deepColor");
	pep_Ocean_gUniformShallowColor = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_shallowColor");
	pep_Ocean_gUniformSkyColor = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_skyColor");
	pep_Ocean_gUniformLightDir = glGetUniformLocation(pep_Ocean_gShaderProgramObject, "u_ocean_lightDir");

	return 0;
}


int Ocean_Initialize(void)
{
	int iRet;
	cudaError_t cudaRet;
	cufftResult cufftRet;

	iRet = Ocean_InitializeShaderProgram();
	if (0 != iRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Ocean_InitializeShaderProgram Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program Initialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	// create FFT plan
	cufftRet = cufftPlan2d(&pep_Ocean_gFFTPlan, pep_Ocean_gMeshSize, pep_Ocean_gMeshSize, CUFFT_C2C);
	if (CUFFT_SUCCESS != cufftRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftPlan2d Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	// allocate memory
	int spectrumSize = pep_Ocean_gSpectrumWidth * pep_Ocean_gSpectrumHeight * sizeof(float2);
	cudaRet = cudaMalloc((void **)&pep_Ocean_gHeightFieldDeltaAtTime0, spectrumSize);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Ocean_InitializeShaderProgram Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	pep_Ocean_gH0 = (float2 *) malloc(spectrumSize);
	Ocean_Generate_h0(pep_Ocean_gH0);
	cudaRet = cudaMemcpy(pep_Ocean_gHeightFieldDeltaAtTime0, pep_Ocean_gH0, spectrumSize, cudaMemcpyHostToDevice);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMemcpy Failed For HeightFieldDeltaAtTime0\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	int outputSize =  pep_Ocean_gMeshSize * pep_Ocean_gMeshSize * sizeof(float2);
	cudaRet = cudaMalloc((void **)&pep_Ocean_gHeightFieldDeltaAtTimeT, outputSize);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMalloc Failed For HeightFieldDeltaAtTimeT\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	cudaRet = cudaMalloc((void **)&pep_Ocean_gDeltaSlope, outputSize);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMalloc Failed For DeltaSlope\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	sdkCreateTimer(&pep_Ocean_gTimer);
	sdkStartTimer(&pep_Ocean_gTimer);
	pep_Ocean_gPrevTime = sdkGetTimerValue(&pep_Ocean_gTimer);

	//
	// create vertex buffers and register with CUDA
	//

	// Vertex-Height Buffer
	glGenBuffers(1, &pep_Ocean_gVertexHeightBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Ocean_gVertexHeightBuffer);
	glBufferData(GL_ARRAY_BUFFER, pep_Ocean_gMeshSize * pep_Ocean_gMeshSize * sizeof(float), 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(PEP_ATTRIBUTES_TEXCOORD0, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	cudaRet = cudaGraphicsGLRegisterBuffer( &pep_Ocean_gCudaResourceVertexHeightBuffer, pep_Ocean_gVertexHeightBuffer, cudaGraphicsMapFlagsWriteDiscard);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsGLRegisterBuffer Failed For VertexHeightBuffer\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	// Vertex-Slope Buffer
	glGenBuffers(1, &pep_Ocean_gVertexSlopeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Ocean_gVertexSlopeBuffer);
	glBufferData(GL_ARRAY_BUFFER, outputSize, 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(PEP_ATTRIBUTES_TEXCOORD1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_TEXCOORD1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	cudaRet = cudaGraphicsGLRegisterBuffer( &pep_Ocean_gCudaResourceVertexSlopeBuffer, pep_Ocean_gVertexSlopeBuffer, cudaGraphicsMapFlagsWriteDiscard);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsGLRegisterBuffer Failed For VertexSlopeBuffer\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	//
	// create fixed vertex buffer to store mesh vertices
	//
	glGenBuffers(1, &pep_Ocean_gVertexPositionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Ocean_gVertexPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pep_Ocean_gMeshSize * pep_Ocean_gMeshSize * 4 * sizeof(float), 0, GL_STATIC_DRAW);

	float *vertexPositionBuffer = (float *) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	if (!vertexPositionBuffer)
	{
		return -1;
	}

	for (int y=0; y < pep_Ocean_gMeshSize; y++)
	{
		for (int x=0; x < pep_Ocean_gMeshSize; x++)
		{
			float u = x / (float)(pep_Ocean_gMeshSize - 1);
			float v = y / (float)(pep_Ocean_gMeshSize - 1);

			*vertexPositionBuffer++ = u * 2.0f - 1.0f;
			*vertexPositionBuffer++ = 0.0f;
			*vertexPositionBuffer++ = v * 2.0f - 1.0f;
			*vertexPositionBuffer++ = 1.0f;
		}
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glVertexAttribPointer(PEP_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//
	// create index buffer for rendering quad mesh
	//
	// create index buffer
	glGenBuffers(1, &pep_Ocean_gIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Ocean_gIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ((pep_Ocean_gMeshSize * 2) + 2) * (pep_Ocean_gMeshSize - 1) * sizeof(GLuint), 0, GL_STATIC_DRAW);

	// fill with indices for rendering mesh as triangle strips
	GLuint *indicesBuffer = (GLuint *) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

	if (!indicesBuffer)
	{
		return -1;
	}

	for (int y=0; y < pep_Ocean_gMeshSize - 1; y++)
	{
		for (int x=0; x < pep_Ocean_gMeshSize; x++)
		{
			*indicesBuffer++ = y * pep_Ocean_gMeshSize + x;
			*indicesBuffer++ = (y + 1) * pep_Ocean_gMeshSize + x;
		}

		// start new strip with degenerate triangle
		*indicesBuffer++ = (y +1 )* pep_Ocean_gMeshSize +(pep_Ocean_gMeshSize - 1);
		*indicesBuffer++ = (y + 1)* pep_Ocean_gMeshSize;
	}

	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	Ocean_RunCuda();

	Ocean_Resize(pep_Ocean_gWindowWidth, pep_Ocean_gWindowHeight);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	return 0;
}

void Ocean_UnInitialize(void)
{
	//
	// Code
	//
	sdkDeleteTimer(&pep_Ocean_gTimer);
	checkCudaErrors(cudaGraphicsUnregisterResource(pep_Ocean_gCudaResourceVertexHeightBuffer));
	checkCudaErrors(cudaGraphicsUnregisterResource(pep_Ocean_gCudaResourceVertexSlopeBuffer));

	glDeleteBuffers(1, &pep_Ocean_gVertexPositionBuffer);
	pep_Ocean_gVertexPositionBuffer = 0;

	glDeleteBuffers(1, &pep_Ocean_gVertexHeightBuffer);
	pep_Ocean_gVertexHeightBuffer = 0;

	glDeleteBuffers(1, &pep_Ocean_gVertexSlopeBuffer);
	pep_Ocean_gVertexSlopeBuffer = 0;

	if (NULL != pep_Ocean_gHeightFieldDeltaAtTime0)
	{
		cudaFree(pep_Ocean_gHeightFieldDeltaAtTime0);
	}

	if (NULL != pep_Ocean_gDeltaSlope)
	{
		cudaFree(pep_Ocean_gDeltaSlope);
	}

	if (NULL != pep_Ocean_gHeightFieldDeltaAtTimeT)
	{
		cudaFree(pep_Ocean_gHeightFieldDeltaAtTimeT);
	}

	if (NULL != pep_Ocean_gH0)
	{
		free(pep_Ocean_gH0);
	}

	cufftDestroy(pep_Ocean_gFFTPlan);

	return;
}

void Ocean_Update(void)
{
	// run CUDA kernel to generate vertex positions
	if (pep_Ocean_gbAnimate)
	{
		Ocean_RunCuda();
	}
}
