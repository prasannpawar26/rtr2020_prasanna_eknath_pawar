#pragma once
// includes
#include <helper_gl.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <cufft.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include <math_constants.h>
#include <rendercheck_gl.h>

#define PEP_OCEAN_MAX_EPSILON 0.10f
#define PEP_OCEAN_THRESHOLD   0.15f
#define PEP_OCEAN_REFRESH_DELAY     2 //ms
#define PEP_OCEAN_REFRESH_DELAY_ID     2001 //ms

int Ocean_Initialize(void);
void Ocean_UnInitialize(void);
void Ocean_Display(void);
void Ocean_Resize(int width, int height);
void Ocean_Update(void);

