
#include <stdio.h>
#include <stdlib.h>

#include <cuda_runtime.h>
#include <cufft.h>          // CUDA FFT Libraries
#include <helper_cuda.h>    // Helper functions for CUDA Error handling

// OpenGL Graphics includes
#define HELPERGL_EXTERN_GL_FUNC_IMPLEMENTATION
#include <helper_gl.h>


// FluidsGL CUDA kernel definitions
#include "fluidsGL_kernels.cuh"

// Particle data
extern GLuint pep_gCpuVbo;                 // OpenGL vertex buffer object
extern struct cudaGraphicsResource *pep_gCudaVboResource; // handles OpenGL-CUDA exchange

extern FILE* pep_gpFile;

// Texture pitch
extern size_t pep_gTexturePitch;
extern cufftHandle pep_CufftPlanRealToComplex;
extern cufftHandle pep_CufftPlanComplexToReal;
float2 *pep_gVelocityInField_X = NULL;
float2 *pep_gVelocityInField_Y = NULL;

// Note that these kernels are designed to work with arbitrary
// domain sizes, not just domains that are multiples of the tile
// size. Therefore, we have extra code that checks to make sure
// a given thread location falls within the domain boundaries in
// both X and Y. Also, the domain is covered by looping over
// multiple elements in the Y direction, while there is a one-to-one
// mapping between threads in X and the tile size in X.
// Nolan Goodnight 9/22/06

// This method adds constant force vectors to the velocity field
// stored in 'v' according to v(x,t+1) = v(x,t) + dt * f.
__global__ void
addForces_k(float2 *v, int dx, int dy, int spx, int spy, float fx, float fy, int r, size_t pitch)
{

    int tx = threadIdx.x;
    int ty = threadIdx.y;
    float2 *fj = (float2 *)((char *)v + (ty + spy) * pitch) + tx + spx;

    float2 vterm = *fj;
    tx -= r;
    ty -= r;
    float s = 1.f / (1.f + tx*tx*tx*tx + ty*ty*ty*ty);
    vterm.x += s * fx;
    vterm.y += s * fy;
    *fj = vterm;
}

// This method performs velocity diffusion and forces mass conservation
// in the frequency domain. The inputs 'vx' and 'vy' are complex-valued
// arrays holding the Fourier coefficients of the velocity field in
// X and Y. Diffusion in this space takes a simple form described as:
// v(k,t) = v(k,t) / (1 + visc * dt * k^2), where visc is the viscosity,
// and k is the wavenumber. The projection step forces the Fourier
// velocity vectors to be orthogonal to the vectors for each
// wavenumber: v(k,t) = v(k,t) - ((k dot v(k,t) * k) / k^2.
__global__ void
diffuseProject_k(float2 *vx, float2 *vy, int dx, int dy, float dt,
                 float visc, int lb)
{

    int gtidx = blockIdx.x * blockDim.x + threadIdx.x;
    int gtidy = blockIdx.y * (lb * blockDim.y) + threadIdx.y * lb;
    int p;

    float2 xterm, yterm;

    // gtidx is the domain location in x for this thread
    if (gtidx < dx)
    {
        for (p = 0; p < lb; p++)
        {
            // fi is the domain location in y for this thread
            int fi = gtidy + p;

            if (fi < dy)
            {
                int fj = fi * dx + gtidx;
                xterm = vx[fj];
                yterm = vy[fj];

                // Compute the index of the wavenumber based on the
                // data order produced by a standard NN FFT.
                int iix = gtidx;
                int iiy = (fi>dy/2)?(fi-(dy)):fi;

                // Velocity diffusion
                float kk = (float)(iix * iix + iiy * iiy); // k^2
                float diff = 1.f / (1.f + visc * dt * kk);
                xterm.x *= diff;
                xterm.y *= diff;
                yterm.x *= diff;
                yterm.y *= diff;

                // Velocity projection
                if (kk > 0.f)
                {
                    float rkk = 1.f / kk;
                    // Real portion of velocity projection
                    float rkp = (iix * xterm.x + iiy * yterm.x);
                    // Imaginary portion of velocity projection
                    float ikp = (iix * xterm.y + iiy * yterm.y);
                    xterm.x -= rkk * rkp * iix;
                    xterm.y -= rkk * ikp * iix;
                    yterm.x -= rkk * rkp * iiy;
                    yterm.y -= rkk * ikp * iiy;
                }

                vx[fj] = xterm;
                vy[fj] = yterm;
            }
        }
    }
}

// This method updates the pep_gParticlesPositionInHostMemory by moving particle positions
// according to the velocity field and time step. That is, for each
// particle: p(t+1) = p(t) + dt * v(p(t)).
__global__ void
advectParticles_k(float2 *part, float2 *v, int dx, int dy,
                  float dt, int lb, size_t pitch)
{

    int gtidx = blockIdx.x * blockDim.x + threadIdx.x;
    int gtidy = blockIdx.y * (lb * blockDim.y) + threadIdx.y * lb;
    int p;

    // gtidx is the domain location in x for this thread
    float2 pterm, vterm;

    if (gtidx < dx)
    {
        for (p = 0; p < lb; p++)
        {
            // fi is the domain location in y for this thread
            int fi = gtidy + p;

            if (fi < dy)
            {
                int fj = fi * dx + gtidx;
                pterm = part[fj];

                int xvi = ((int)(pterm.x * dx));
                int yvi = ((int)(pterm.y * dy));
                vterm = *((float2 *)((char *)v + yvi * pitch) + xvi);

                pterm.x += dt * vterm.x;
                pterm.x = pterm.x - (int)pterm.x;
                pterm.x += 1.f;
                pterm.x = pterm.x - (int)pterm.x;
                pterm.y += dt * vterm.y;
                pterm.y = pterm.y - (int)pterm.y;
                pterm.y += 1.f;
                pterm.y = pterm.y - (int)pterm.y;

                part[fj] = pterm;
            }
        } // If this thread is inside the domain in Y
    } // If this thread is inside the domain in X
}


// These are the external function calls necessary for launching fluid simulation
extern "C"
void AddForces(float2 *v, int dx, int dy, int spx, int spy, float fx, float fy, int r)
{

    dim3 tids(2*r+1, 2*r+1);

    addForces_k<<<1, tids>>>(v, dx, dy, spx, spy, fx, fy, r, pep_gTexturePitch);
    getLastCudaError("addForces_k failed.");
}

extern "C"
void DiffuseProject(float2 *vx, float2 *vy, int dx, int dy, float dt, float visc)
{
	//
	//Variable Declarations
	//
	cufftResult cufft_result;

	//
	// Code
	//

    // Forward FFT
	cufft_result = cufftExecR2C(pep_CufftPlanRealToComplex, (cufftReal *)vx, (cufftComplex *)vx);
	if (CUFFT_SUCCESS != cufft_result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftExecR2C Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cufft_result);
		return;
	}

	cufft_result = cufftExecR2C(pep_CufftPlanRealToComplex, (cufftReal *)vy, (cufftComplex *)vy);
	if (CUFFT_SUCCESS != cufft_result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftExecR2C Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cufft_result);
		return;
	}

    uint3 grid = make_uint3((dx/PEP_TILE_WIDTH)+(!(dx%PEP_TILE_WIDTH)?0:1),
                            (dy/PEP_TILE_HEIGHT)+(!(dy%PEP_TILE_HEIGHT)?0:1), 1);
    uint3 tids = make_uint3(PEP_TIDS_IN_X, PEP_TIDS_IN_Y, 1);

    diffuseProject_k<<<grid, tids>>>(vx, vy, dx, dy, dt, visc, PEP_TILE_HEIGHT/PEP_TIDS_IN_Y);
    getLastCudaError("diffuseProject_k failed.");

    // Inverse FFT
	cufft_result = cufftExecC2R(pep_CufftPlanComplexToReal, (cufftComplex *)vx, (cufftReal *)vx);
	if (CUFFT_SUCCESS != cufft_result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftExecR2C Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cufft_result);
		return;
	}

	cufft_result = cufftExecC2R(pep_CufftPlanComplexToReal, (cufftComplex *)vy, (cufftReal *)vy);
	if (CUFFT_SUCCESS != cufft_result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftExecR2C Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cufft_result);
		return;
	}

	return;
}

extern "C"
void AdvectParticles(GLuint pep_gCpuVbo, float2 *v, int dx, int dy, float dt)
{
	//
	// Variable Declarations
	//
    dim3 grid((dx/PEP_TILE_WIDTH)+(!(dx%PEP_TILE_WIDTH)?0:1), (dy/PEP_TILE_HEIGHT)+(!(dy%PEP_TILE_HEIGHT)?0:1));
    dim3 tids(PEP_TIDS_IN_X, PEP_TIDS_IN_Y);

    float2 *p;
	cudaError_t cuda_error;
	size_t num_bytes;

	// 
	// Code
	//
	cuda_error = cudaGraphicsMapResources(1, &pep_gCudaVboResource, 0);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsMapResources Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return;
	}

	cuda_error = cudaGraphicsResourceGetMappedPointer((void **)&p, &num_bytes,
                                         pep_gCudaVboResource);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsResourceGetMappedPointer Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return;
	}

    advectParticles_k<<<grid, tids>>>(p, v, dx, dy, dt, PEP_TILE_HEIGHT/PEP_TIDS_IN_Y, pep_gTexturePitch);
	cuda_error = cudaGetLastError();
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsResourceGetMappedPointer Failed ErrorCode: %d msg: %s\n", __FILE__, __LINE__, __FUNCTION__, cuda_error, cudaGetErrorString(cuda_error));
		return;
	}

	cuda_error = cudaGraphicsUnmapResources(1, &pep_gCudaVboResource, 0);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsUnmapResources Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return;
	}

	return;
}
