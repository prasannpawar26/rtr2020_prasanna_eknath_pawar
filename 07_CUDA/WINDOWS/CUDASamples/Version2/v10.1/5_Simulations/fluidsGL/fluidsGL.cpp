#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

#include <GL/glew.h>
#include <GL/gl.h>

#include <helper_gl.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <cufft.h>
#include <helper_functions.h>
#include <rendercheck_gl.h>
#include <helper_cuda.h>

#include "defines.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew64.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

vmath::mat4 pep_gProjectionMatrix;

// Shader Related Variables
GLuint pep_gFluidVertexShaderObject;
GLuint pep_gFluidFragmentShaderObject;
GLuint pep_gFluidShaderProgramObject;

// Uniforms
GLuint pep_gUniformModelMatrix;
GLuint pep_gUniformViewMatrix;
GLuint pep_gUniformProjectionMatrix;
GLuint pep_gUniformColor;

enum {
	PEP_ATTRIBUTES_POSITION = 0,
	PEP_ATTRIBUTES_COLOR,
	PEP_ATTRIBUTES_NORMAL,
	PEP_ATTRIBUTES_TEXCOORD0,
};

// CUFFT plan handle
cufftHandle pep_CufftPlanRealToComplex;
cufftHandle pep_CufftPlanComplexToReal;
static float2 *pep_gVelocityInField_X = NULL;
static float2 *pep_gVelocityInField_Y = NULL;

float2 *pep_gHostVelocityField = NULL;
float2 *pep_gDeviceVelocityField = NULL;

int pep_gWindowWidth  = MAX(1920, PEP_DIM);
int pep_gWindowHeight = MAX(1080, PEP_DIM);
int pep_gClicked  = 0;

// Particle data
GLuint pep_gVao = 0;
GLuint pep_gCpuVbo = 0;                 // OpenGL vertex buffer object
struct cudaGraphicsResource *pep_gCudaVboResource; // handles OpenGL-CUDA exchange
float2 *pep_gParticlesPositionInHostMemory = NULL; // particle positions in host memory

int pep_gLast_X = 0;
int pep_gLast_Y = 0;

bool pep_gbStart = false;

enum DIRECTION
{
	DIRECTION_X = 0 ,
	DIRECTION_Y,
	DIRECTION_BOTH,
};

typedef struct _letter
{
	int starting_X;
	int starting_Y;

	int current_X;
	int current_Y;

	int prev_X;
	int prev_Y;

	int direction;

	bool direction_positive;

	int ending_X;
	int ending_Y;

	bool completed;

}LETTER;

int pep_LetterWidth = 100;
int pep_Letterheight = 265;
int pep_LetterStarting_Y = 35;
int pep_LetterGap_X = 50;

LETTER Letter_H[3] = {
	{
		105, pep_LetterStarting_Y,
		105, pep_LetterStarting_Y,
		105, pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	},
	{
		105 + pep_LetterWidth, pep_LetterStarting_Y,
		105 + pep_LetterWidth, pep_LetterStarting_Y,
		105 + pep_LetterWidth, pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	},
	{
		105, pep_LetterStarting_Y + (pep_Letterheight / 2),
		105, pep_LetterStarting_Y + (pep_Letterheight / 2),
		105, pep_LetterStarting_Y + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		105 + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_A[4] = {
	{
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	},
	{
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	},
	{
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_X,
		true,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		105 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_P[4] = {
	{
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	},
	{
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + (pep_Letterheight / 2),
		false
	},
	{
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_X,
		true,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		105 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_P_2[4] = {
	{
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	},
	{
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + (pep_Letterheight / 2),
		false
	},
	{
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_X,
		true,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		105 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_Y[4] = {
	{
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + (pep_Letterheight / 2),
		false
	},
	{
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + (pep_Letterheight / 2),
		false
	},
	{
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0, 
		false
	},
	{
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + (pep_Letterheight / 2),
		105 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + (pep_Letterheight / 2),
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + pep_Letterheight,
		false
	}
};

// 
// BIRTHDAY
//
LETTER Letter_B[5] = {
	{
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375,
		DIRECTION_X,
		true,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375 + pep_Letterheight,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375 + pep_Letterheight,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375 + pep_Letterheight,
		DIRECTION_X,
		true,
		500 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_I[3] = {
	{
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_X,
		true,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + pep_Letterheight,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + pep_Letterheight,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + pep_Letterheight,
		DIRECTION_X,
		true,
		500 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_R[5] = {
	{
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		false
	},
	{
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_X,
		true,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_BOTH,
		true,
		500 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	}
};

LETTER Letter_T[2] = {
	{
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375,
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375,
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_X,
		true,
		500 + (3 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_H_2[3] = {
	{
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		500 + (4 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_D[4] = {
	{
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375,
		DIRECTION_X,
		true,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375 + pep_Letterheight,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375 + pep_Letterheight,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) - 20, pep_LetterStarting_Y + 375 + pep_Letterheight,
		DIRECTION_X,
		true,
		500 + (5 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_A_2[4] = {
	{
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	},
	{
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_X,
		true,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		500 + (6 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_Y_2[4] = {
	{
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		false
	},
	{
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 375,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		false
	},
	{
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0, 
		false
	},
	{
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		500 + (7 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 375 + (pep_Letterheight / 2),
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 375 + pep_Letterheight,
		false
	}
};


//
// SIR
//
LETTER Letter_S[5] = {
	{
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		false
	},
	{
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 750 + (pep_Letterheight / 2),
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y+ 750 + (pep_Letterheight / 2),
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 750 + pep_Letterheight,
		false
	},
	{
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		DIRECTION_X,
		true,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + pep_Letterheight,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + pep_Letterheight,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + pep_Letterheight,
		DIRECTION_X,
		true,
		1420 + (0 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_I_2[3] = {
	{
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 750,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 750,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + (pep_LetterWidth / 2), pep_LetterStarting_Y + 750,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 750 + pep_Letterheight,
		false
	},
	{
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		DIRECTION_X,
		true,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + pep_Letterheight,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + pep_Letterheight,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + pep_Letterheight,
		DIRECTION_X,
		true,
		1420 + (1 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	}
};

LETTER Letter_R_2[5] = {
	{
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 750 + pep_Letterheight,
		false
	},
	{
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 750,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 750,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth, pep_LetterStarting_Y + 750,
		DIRECTION_Y,
		true,
		0,
		pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		false
	},
	{
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750,
		DIRECTION_X,
		true,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		DIRECTION_X,
		true,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		0,
		false
	},
	{
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)), pep_LetterStarting_Y + 750 + (pep_Letterheight / 2),
		DIRECTION_BOTH,
		true,
		1420 + (2 * (pep_LetterWidth + pep_LetterGap_X)) + pep_LetterWidth,
		pep_LetterStarting_Y + 750 + pep_Letterheight,
		false
	}
};

// Texture pitch
size_t pep_gTexturePitch = 0;

//
// OpenGL Context Related Variables
//
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;

//
// FullScreen Related Variables
//
DWORD pep_gdwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool pep_gbActiveWindow = false;
bool pep_gbIsFullScreen = false;

extern "C" void AddForces(float2 *v, int dx, int dy, int spx, int spy, float fx, float fy, int r);
extern "C" void DiffuseProject(float2 *vx, float2 *vy, int dx, int dy, float dt, float visc);
extern "C" void AdvectParticles(GLuint pep_gCpuVbo, float2 *v, int dx, int dy, float dt);

// very simple von neumann middle-square prng.  can't use rand() in -qatest
// mode because its implementation varies across platforms which makes testing
// for consistency in the important parts of this program difficult.
float Rand(void)
{
    //static int seed = 72191;
    //char sq[22];

    //if (ref_file)
    //{
    //    seed *= seed;
    //    sprintf(sq, "%010d", seed);
    //    // pull the middle 5 digits out of sq
    //    sq[8] = 0;
    //    seed = atoi(&sq[3]);

    //    return seed/99999.f;
    //}
    //else
    //{
        return rand()/(float)RAND_MAX;
    /*}*/
}

void InitParticles(float2 *p, int dx, int dy)
{
	//
	// Function Declarations
	//
	float Rand(void);

	//
	// Variable Declarations
	//
    int i, j;

    for (i = 0; i < dy; i++)
    {
        for (j = 0; j < dx; j++)
        {
            p[i*dx+j].x = (j+0.5f+(Rand() - 0.5f))/dx;
            p[i*dx+j].y = (i+0.5f+(Rand() - 0.5f))/dy;
        }
    }

	return;
}

void Resize(int x, int y)
{
    pep_gWindowWidth = x;
    pep_gWindowHeight = y;
	glViewport(0, 0, x, y);

	pep_gProjectionMatrix = mat4::identity();
	pep_gProjectionMatrix = ortho(0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f);

}

void Cleanup(void)
{
	//
	// Code
	//
	if (NULL != pep_gCudaVboResource)
	{
		cudaGraphicsUnregisterResource(pep_gCudaVboResource);
		pep_gCudaVboResource = NULL;
	}

    // Free all host and device resources
	if (NULL != pep_gHostVelocityField)
	{
		free(pep_gHostVelocityField);
		pep_gHostVelocityField = NULL;
	}

	if (NULL != pep_gParticlesPositionInHostMemory)
	{
		free(pep_gParticlesPositionInHostMemory);
		pep_gParticlesPositionInHostMemory = NULL;
	}

	if (NULL != pep_gDeviceVelocityField)
	{
		cudaFree(pep_gDeviceVelocityField);
		pep_gDeviceVelocityField = NULL;
	}

	if (NULL != pep_gVelocityInField_X)
	{
		cudaFree(pep_gVelocityInField_X);
		pep_gVelocityInField_X = NULL;
	}

	if (NULL != pep_gVelocityInField_Y)
	{
		cudaFree(pep_gVelocityInField_Y);
		pep_gVelocityInField_Y = NULL;
	}

    cufftDestroy(pep_CufftPlanRealToComplex);
    cufftDestroy(pep_CufftPlanComplexToReal);

	if (0 != pep_gCpuVbo)
	{
		glDeleteBuffers(1, &pep_gCpuVbo);
		pep_gCpuVbo = 0;
	}

	return;
}

void UnInitialize(void)
{
	// 
	// Function Declarations
	//
	void Cleanup(void);

	//
	// Code
	//
	Cleanup();

	if (wglGetCurrentContext() == pep_gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
	}

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}
	return;
}

int Initialize(void)
{
	//
	// Function Declarations
	//
	void Resize(int, int);
	void ToggledFullScreen(void);
	void InitParticles(float2 *, int, int);
	GLint InitializeShaderProgram(void);

	//
	// Variable Declarations
	//
	int index;
	PIXELFORMATDESCRIPTOR pfd;
	cufftResult cufft_result;
	cudaError_t cuda_error;
	int iRet;

	//
	// Code
	//
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	pep_gHdc = GetDC(pep_gHwnd);

	index = ChoosePixelFormat(pep_gHdc, &pfd);
	if (0 == index)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : ChoosePixelFormat Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd))
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : SetPixelFormat Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	GLenum result;

	result = glewInit();
	if (GLEW_OK != result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Successful\n", __FILE__, __LINE__, __FUNCTION__);

	iRet = InitializeShaderProgram();
	if (0 != iRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : InitializeShaderProgram Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program Initialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	// Allocate and initialize host data
	//GLint bsize;

	pep_gHostVelocityField = (float2 *)malloc(sizeof(float2) * PEP_DOMAINSIZE);
	if (NULL == pep_gHostVelocityField)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : malloc Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	memset(pep_gHostVelocityField, 0, sizeof(float2) * PEP_DOMAINSIZE);

	// Allocate and initialize device data
	cuda_error = cudaMallocPitch((void **)&pep_gDeviceVelocityField, &pep_gTexturePitch, sizeof(float2)*PEP_DIM, PEP_DIM);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMallocPitch Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return -1;
	}

	cuda_error = cudaMemcpy(pep_gDeviceVelocityField, pep_gHostVelocityField, sizeof(float2) * PEP_DOMAINSIZE,
		cudaMemcpyHostToDevice);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMemcpy Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return -1;
	}

	// Temporary complex velocity field data
	cuda_error = cudaMalloc((void **)&pep_gVelocityInField_X, sizeof(float2) * PEP_PADDEDDOMAINSIZE);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMalloc Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return -1;
	}

	cuda_error = cudaMalloc((void **)&pep_gVelocityInField_Y, sizeof(float2) * PEP_PADDEDDOMAINSIZE);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMalloc Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return -1;
	}

	// Create particle array
	pep_gParticlesPositionInHostMemory = (float2 *)malloc(sizeof(float2) * PEP_DOMAINSIZE);
	memset(pep_gParticlesPositionInHostMemory, 0, sizeof(float2) * PEP_DOMAINSIZE);

	InitParticles(pep_gParticlesPositionInHostMemory, PEP_DIM, PEP_DIM);

	// Create CUFFT transform plan configuration
	cufft_result = cufftPlan2d(&pep_CufftPlanRealToComplex, PEP_DIM, PEP_DIM, CUFFT_R2C);
	if (CUFFT_SUCCESS != cufft_result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftPlan2d Failed Error Code : %d\n", __FILE__, __LINE__, __FUNCTION__, cufft_result);
		return -1;
	}

	cufft_result = cufftPlan2d(&pep_CufftPlanComplexToReal, PEP_DIM, PEP_DIM, CUFFT_C2R);
	if (CUFFT_SUCCESS != cufft_result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftPlan2d Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cufft_result);
		return -1;
	}

	glGenBuffers(1, &pep_gCpuVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_gCpuVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float2) * PEP_DOMAINSIZE,
		pep_gParticlesPositionInHostMemory, GL_DYNAMIC_DRAW);

	//glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &bsize);
	//if (bsize != (sizeof(float2) * PEP_DOMAINSIZE))
	//{
	//	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glBufferData Failed\n", __FILE__, __LINE__, __FUNCTION__);
	//	return -1;
	//}

	glVertexAttribPointer(PEP_ATTRIBUTES_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	cuda_error = cudaGraphicsGLRegisterBuffer(&pep_gCudaVboResource, pep_gCpuVbo, cudaGraphicsMapFlagsNone);
	if (cudaSuccess != cuda_error)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsGLRegisterBuffer Failed ErrorCode: %d\n", __FILE__, __LINE__, __FUNCTION__, cuda_error);
		return -1;
	}

	glClearColor(0.50, 0.80, 0.20, 1.0);
	//glClearColor(1.0, 0.0, 0.0, 1.0);
	//glEnable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	//glDisable(GL_CULL_FACE);

	//Resize(750,750);
	ToggledFullScreen();

	return 0;
}

int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpszCmdLine,
	int iCmdShow)
{
	//
	// Function Declarations
	//
	int Initialize(void);
	void Display(void);
	void Update(void);
	//
	// Variable Declarations
	//
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("Fluid Simulation");

	//
	// Code
	//
	if (0 != fopen_s(&pep_gpFile, "Log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created/Opend Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	ATOM  at = RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName, 
		TEXT("Fluid Simulation"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();

	if (0 != iRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Failed\n", __FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}

	while (false == bDone) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} 
		else
		{
			if (pep_gbActiveWindow)
			{
			}

			Update();

			Display();
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void Resize(int, int);
	void InitParticles(float2 *, int, int);
	void Draw_Letters(int x, int y, int *lastX, int *lastY);
	//
	// Code
	//
	switch (iMsg)
	{
		case WM_MOUSEMOVE:
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);

			fprintf(pep_gpFile, "position : x = %d y = %d\n", x, y);

			// Convert motion coordinates to domain
			float fx = (pep_gLast_X / (float)pep_gWindowWidth);
			float fy = (pep_gLast_Y / (float)pep_gWindowHeight);
			int nx = (int)(fx * PEP_DIM);
			int ny = (int)(fy * PEP_DIM);

			if (pep_gClicked && nx < PEP_DIM-PEP_FORCE_UPDATE_RADIUS && nx > PEP_FORCE_UPDATE_RADIUS-1 && ny < PEP_DIM-PEP_FORCE_UPDATE_RADIUS && ny > PEP_FORCE_UPDATE_RADIUS-1)
			{
				
				int ddx = x - pep_gLast_X;
				int ddy = y - pep_gLast_Y;
				fx = ddx / (float)pep_gWindowWidth;
				fy = ddy / (float)pep_gWindowHeight;
				int spy = ny-PEP_FORCE_UPDATE_RADIUS;
				int spx = nx-PEP_FORCE_UPDATE_RADIUS;
				AddForces(pep_gDeviceVelocityField, PEP_DIM, PEP_DIM, spx, spy, PEP_FORCE * PEP_DELTA_T * fx, PEP_FORCE * PEP_DELTA_T * fy, PEP_FORCE_UPDATE_RADIUS);
				pep_gLast_X = x;
				pep_gLast_Y = y;
			}
		}
		break;

		case WM_LBUTTONDOWN:
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);
			fprintf(pep_gpFile, "\n\npep_gClicked position : x = %d y = %d\n\n", x, y);
			pep_gLast_X = x;
			pep_gLast_Y = y;
			pep_gClicked = !pep_gClicked;
		}
		break;

		case WM_LBUTTONUP:
		{
			pep_gClicked = !pep_gClicked;
		}
		break;

		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;

		case WM_ERASEBKGND:
			return 0;
			break;

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_SIZE:
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
			break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
			break;
		case WM_KEYDOWN:
		{
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;

			case 'F':
			case 'f':
				ToggledFullScreen();
				break;

			case 'S':
			case 's':
				if (pep_gbStart)
				{
					pep_gbStart = false;
				}
				else
				{
					pep_gbStart = true;
				}
				break;

			case 'r':
			case 'R':
				memset(pep_gHostVelocityField, 0, sizeof(float2) * PEP_DOMAINSIZE);
				cudaMemcpy(pep_gDeviceVelocityField, pep_gHostVelocityField, sizeof(float2) * PEP_DOMAINSIZE,
					cudaMemcpyHostToDevice);

				InitParticles(pep_gParticlesPositionInHostMemory, PEP_DIM, PEP_DIM);

				cudaGraphicsUnregisterResource(pep_gCudaVboResource);

				glBindBuffer(GL_ARRAY_BUFFER, pep_gCpuVbo);
				glBufferData(GL_ARRAY_BUFFER, sizeof(float2) * PEP_DOMAINSIZE,
					pep_gParticlesPositionInHostMemory, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);

				cudaGraphicsGLRegisterBuffer(&pep_gCudaVboResource, pep_gCpuVbo, cudaGraphicsMapFlagsNone);

				break;
			}
		}
		break;
	}  // End Of Switch Case

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbIsFullScreen)
	{
		pep_gdwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_gdwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gdwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		//ShowCursor(FALSE);
		pep_gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		pep_gbIsFullScreen = false;
		//ShowCursor(TRUE);
	}

	return;
}

void Update(void)
{
	//
	// Code
	//
	void SimulateFluids(void);

	SimulateFluids();

	return;
}

void SimulateFluids(void)
{
	//
	// Code
	//

	// simulate fluid
	DiffuseProject(pep_gVelocityInField_X, pep_gVelocityInField_Y, PEP_CPADW, PEP_DIM, PEP_DELTA_T, PEP_VISCOSITY_CONSTANT);
	AdvectParticles(pep_gCpuVbo, pep_gDeviceVelocityField, PEP_DIM, PEP_DIM, PEP_DELTA_T);

	return;
}

void Update_Letter_H(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 3; i++)
	{
		if (false == Letter_H[i].completed)
		{
			if (Letter_H[i].direction_positive)
			{
				if (DIRECTION_X == Letter_H[i].direction)
				{
					if (Letter_H[i].current_X/*gX*/ < Letter_H[i].ending_X)
					{
						Letter_H[i].current_X += 2;
					}
					else
					{
						Letter_H[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_H[i].direction)
				{
					if (Letter_H[i].current_Y/*gY*/ < Letter_H[i].ending_Y)
					{
						Letter_H[i].current_Y += 2;
					}
					else
					{
						Letter_H[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_H[i].direction)
				{
					if (Letter_H[i].current_X/*gX*/ > Letter_H[i].ending_X)
					{
						Letter_H[i].current_X -= 2;
					}
					else
					{
						Letter_H[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_H[i].direction)
				{
					if (Letter_H[i].current_Y/*gY*/ > Letter_H[i].ending_Y)
					{
						Letter_H[i].current_Y -= 2;
					}
					else
					{
						Letter_H[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_H[i].current_X, Letter_H[i].current_Y, &Letter_H[i].prev_X, &Letter_H[i].prev_Y);
	}

	return;
}

void Update_Letter_A(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_A[i].completed)
		{
			if (Letter_A[i].direction_positive)
			{
				if (DIRECTION_X == Letter_A[i].direction)
				{
					if (Letter_A[i].current_X/*gX*/ < Letter_A[i].ending_X)
					{
						Letter_A[i].current_X += 2;
					}
					else
					{
						Letter_A[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_A[i].direction)
				{
					if (Letter_A[i].current_Y/*gY*/ < Letter_A[i].ending_Y)
					{
						Letter_A[i].current_Y += 2;
					}
					else
					{
						Letter_A[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_A[i].direction)
				{
					if (Letter_A[i].current_X/*gX*/ > Letter_A[i].ending_X)
					{
						Letter_A[i].current_X -= 2;
					}
					else
					{
						Letter_A[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_A[i].direction)
				{
					if (Letter_A[i].current_Y/*gY*/ > Letter_A[i].ending_Y)
					{
						Letter_A[i].current_Y -= 2;
					}
					else
					{
						Letter_A[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_A[i].current_X, Letter_A[i].current_Y, &Letter_A[i].prev_X, &Letter_A[i].prev_Y);
	}

	return;
}

void Update_Letter_P(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_P[i].completed)
		{
			if (Letter_P[i].direction_positive)
			{
				if (DIRECTION_X == Letter_P[i].direction)
				{
					if (Letter_P[i].current_X/*gX*/ < Letter_P[i].ending_X)
					{
						Letter_P[i].current_X += 2;
					}
					else
					{
						Letter_P[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_P[i].direction)
				{
					if (Letter_P[i].current_Y/*gY*/ < Letter_P[i].ending_Y)
					{
						Letter_P[i].current_Y += 2;
					}
					else
					{
						Letter_P[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_P[i].direction)
				{
					if (Letter_P[i].current_X/*gX*/ > Letter_P[i].ending_X)
					{
						Letter_P[i].current_X -= 2;
					}
					else
					{
						Letter_P[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_P[i].direction)
				{
					if (Letter_P[i].current_Y/*gY*/ > Letter_P[i].ending_Y)
					{
						Letter_P[i].current_Y -= 2;
					}
					else
					{
						Letter_P[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_P[i].current_X, Letter_P[i].current_Y, &Letter_P[i].prev_X, &Letter_P[i].prev_Y);
	}

	return;
}

void Update_Letter_P_2(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_P_2[i].completed)
		{
			if (Letter_P_2[i].direction_positive)
			{
				if (DIRECTION_X == Letter_P_2[i].direction)
				{
					if (Letter_P_2[i].current_X/*gX*/ < Letter_P_2[i].ending_X)
					{
						Letter_P_2[i].current_X += 2;
					}
					else
					{
						Letter_P_2[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_P_2[i].direction)
				{
					if (Letter_P_2[i].current_Y/*gY*/ < Letter_P_2[i].ending_Y)
					{
						Letter_P_2[i].current_Y += 2;
					}
					else
					{
						Letter_P_2[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_P_2[i].direction)
				{
					if (Letter_P_2[i].current_X/*gX*/ > Letter_P_2[i].ending_X)
					{
						Letter_P_2[i].current_X -= 2;
					}
					else
					{
						Letter_P_2[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_P_2[i].direction)
				{
					if (Letter_P_2[i].current_Y/*gY*/ > Letter_P_2[i].ending_Y)
					{
						Letter_P_2[i].current_Y -= 2;
					}
					else
					{
						Letter_P_2[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_P_2[i].current_X, Letter_P_2[i].current_Y, &Letter_P_2[i].prev_X, &Letter_P_2[i].prev_Y);
	}

	return;
}

void Update_Letter_Y(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_Y[i].completed)
		{
			if (Letter_Y[i].direction_positive)
			{
				if (DIRECTION_X == Letter_Y[i].direction)
				{
					if (Letter_Y[i].current_X/*gX*/ < Letter_Y[i].ending_X)
					{
						Letter_Y[i].current_X += 2;
					}
					else
					{
						Letter_Y[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_Y[i].direction)
				{
					if (Letter_Y[i].current_Y/*gY*/ < Letter_Y[i].ending_Y)
					{
						Letter_Y[i].current_Y += 2;
					}
					else
					{
						Letter_Y[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_Y[i].direction)
				{
					if (Letter_Y[i].current_X/*gX*/ > Letter_Y[i].ending_X)
					{
						Letter_Y[i].current_X -= 2;
					}
					else
					{
						Letter_Y[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_Y[i].direction)
				{
					if (Letter_Y[i].current_Y/*gY*/ > Letter_Y[i].ending_Y)
					{
						Letter_Y[i].current_Y -= 2;
					}
					else
					{
						Letter_Y[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_Y[i].current_X, Letter_Y[i].current_Y, &Letter_Y[i].prev_X, &Letter_Y[i].prev_Y);
	}

	return;
}

void Update_Letter_B(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 5; i++)
	{
		if (false == Letter_B[i].completed)
		{
			if (Letter_B[i].direction_positive)
			{
				if (DIRECTION_X == Letter_B[i].direction)
				{
					if (Letter_B[i].current_X < Letter_B[i].ending_X)
					{
						Letter_B[i].current_X += 2;
					}
					else
					{
						Letter_B[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_B[i].direction)
				{
					if (Letter_B[i].current_Y < Letter_B[i].ending_Y)
					{
						Letter_B[i].current_Y += 2;
					}
					else
					{
						Letter_B[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_B[i].direction)
				{
					if (Letter_B[i].current_X > Letter_B[i].ending_X)
					{
						Letter_B[i].current_X -= 2;
					}
					else
					{
						Letter_B[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_B[i].direction)
				{
					if (Letter_B[i].current_Y > Letter_B[i].ending_Y)
					{
						Letter_B[i].current_Y -= 2;
					}
					else
					{
						Letter_B[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_B[i].current_X, Letter_B[i].current_Y, &Letter_B[i].prev_X, &Letter_B[i].prev_Y);
	}

	return;
}

void Update_Letter_I(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 3; i++)
	{
		if (false == Letter_I[i].completed)
		{
			if (Letter_I[i].direction_positive)
			{
				if (DIRECTION_X == Letter_I[i].direction)
				{
					if (Letter_I[i].current_X/*gX*/ < Letter_I[i].ending_X)
					{
						Letter_I[i].current_X += 2;
					}
					else
					{
						Letter_I[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_I[i].direction)
				{
					if (Letter_I[i].current_Y/*gY*/ < Letter_I[i].ending_Y)
					{
						Letter_I[i].current_Y += 2;
					}
					else
					{
						Letter_I[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_I[i].direction)
				{
					if (Letter_I[i].current_X/*gX*/ > Letter_I[i].ending_X)
					{
						Letter_I[i].current_X -= 2;
					}
					else
					{
						Letter_I[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_I[i].direction)
				{
					if (Letter_I[i].current_Y/*gY*/ > Letter_I[i].ending_Y)
					{
						Letter_I[i].current_Y -= 2;
					}
					else
					{
						Letter_I[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_I[i].current_X, Letter_I[i].current_Y, &Letter_I[i].prev_X, &Letter_I[i].prev_Y);
	}

	return;
}

void Update_Letter_R(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 5; i++)
	{
		if (false == Letter_R[i].completed)
		{
			if (Letter_R[i].direction_positive)
			{
				if (DIRECTION_X == Letter_R[i].direction)
				{
					if (Letter_R[i].current_X/*gX*/ < Letter_R[i].ending_X)
					{
						Letter_R[i].current_X += 2;
					}
					else
					{
						Letter_R[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_R[i].direction)
				{
					if (Letter_R[i].current_Y/*gY*/ < Letter_R[i].ending_Y)
					{
						Letter_R[i].current_Y += 2;
					}
					else
					{
						Letter_R[i].completed = true;
					}
				}
				else if (DIRECTION_BOTH == Letter_R[i].direction)
				{
					if(Letter_R[i].current_Y >= Letter_R[i].ending_Y && Letter_R[i].current_X >= Letter_R[i].ending_X)
					{
						Letter_R[i].completed = true;
					}
					else
					{
						if (Letter_R[i].current_X/*gX*/ < Letter_R[i].ending_X)
						{
							Letter_R[i].current_X += 2;
						}

						if (Letter_R[i].current_Y/*gY*/ < Letter_R[i].ending_Y)
						{
							Letter_R[i].current_Y += 2;
						}
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_R[i].direction)
				{
					if (Letter_R[i].current_X/*gX*/ > Letter_R[i].ending_X)
					{
						Letter_R[i].current_X -= 2;
					}
					else
					{
						Letter_R[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_R[i].direction)
				{
					if (Letter_R[i].current_Y/*gY*/ > Letter_R[i].ending_Y)
					{
						Letter_R[i].current_Y -= 2;
					}
					else
					{
						Letter_R[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_R[i].current_X, Letter_R[i].current_Y, &Letter_R[i].prev_X, &Letter_R[i].prev_Y);
	}

	return;
}

void Update_Letter_T(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 2; i++)
	{
		if (false == Letter_T[i].completed)
		{
			if (Letter_T[i].direction_positive)
			{
				if (DIRECTION_X == Letter_T[i].direction)
				{
					if (Letter_T[i].current_X/*gX*/ < Letter_T[i].ending_X)
					{
						Letter_T[i].current_X += 2;
					}
					else
					{
						Letter_T[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_T[i].direction)
				{
					if (Letter_T[i].current_Y/*gY*/ < Letter_T[i].ending_Y)
					{
						Letter_T[i].current_Y += 2;
					}
					else
					{
						Letter_T[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_T[i].direction)
				{
					if (Letter_T[i].current_X/*gX*/ > Letter_T[i].ending_X)
					{
						Letter_T[i].current_X -= 2;
					}
					else
					{
						Letter_T[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_T[i].direction)
				{
					if (Letter_T[i].current_Y/*gY*/ > Letter_T[i].ending_Y)
					{
						Letter_T[i].current_Y -= 2;
					}
					else
					{
						Letter_T[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_T[i].current_X, Letter_T[i].current_Y, &Letter_T[i].prev_X, &Letter_T[i].prev_Y);
	}

	return;
}

void Update_Letter_H_2(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 3; i++)
	{
		if (false == Letter_H_2[i].completed)
		{
			if (Letter_H_2[i].direction_positive)
			{
				if (DIRECTION_X == Letter_H_2[i].direction)
				{
					if (Letter_H_2[i].current_X/*gX*/ < Letter_H_2[i].ending_X)
					{
						Letter_H_2[i].current_X += 2;
					}
					else
					{
						Letter_H_2[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_H_2[i].direction)
				{
					if (Letter_H_2[i].current_Y/*gY*/ < Letter_H_2[i].ending_Y)
					{
						Letter_H_2[i].current_Y += 2;
					}
					else
					{
						Letter_H_2[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_H_2[i].direction)
				{
					if (Letter_H_2[i].current_X/*gX*/ > Letter_H_2[i].ending_X)
					{
						Letter_H_2[i].current_X -= 2;
					}
					else
					{
						Letter_H_2[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_H_2[i].direction)
				{
					if (Letter_H_2[i].current_Y/*gY*/ > Letter_H_2[i].ending_Y)
					{
						Letter_H_2[i].current_Y -= 2;
					}
					else
					{
						Letter_H_2[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_H_2[i].current_X, Letter_H_2[i].current_Y, &Letter_H_2[i].prev_X, &Letter_H_2[i].prev_Y);
	}

	return;
}

void Update_Letter_D(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_D[i].completed)
		{
			if (Letter_D[i].direction_positive)
			{
				if (DIRECTION_X == Letter_D[i].direction)
				{
					if (Letter_D[i].current_X/*gX*/ < Letter_D[i].ending_X)
					{
						Letter_D[i].current_X += 2;
					}
					else
					{
						Letter_D[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_D[i].direction)
				{
					if (Letter_D[i].current_Y/*gY*/ < Letter_D[i].ending_Y)
					{
						Letter_D[i].current_Y += 2;
					}
					else
					{
						Letter_D[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_D[i].direction)
				{
					if (Letter_D[i].current_X/*gX*/ > Letter_D[i].ending_X)
					{
						Letter_D[i].current_X -= 2;
					}
					else
					{
						Letter_D[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_D[i].direction)
				{
					if (Letter_D[i].current_Y/*gY*/ > Letter_D[i].ending_Y)
					{
						Letter_D[i].current_Y -= 2;
					}
					else
					{
						Letter_D[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_D[i].current_X, Letter_D[i].current_Y, &Letter_D[i].prev_X, &Letter_D[i].prev_Y);
	}

	return;
}

void Update_Letter_A_2(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_A_2[i].completed)
		{
			if (Letter_A_2[i].direction_positive)
			{
				if (DIRECTION_X == Letter_A_2[i].direction)
				{
					if (Letter_A_2[i].current_X/*gX*/ < Letter_A_2[i].ending_X)
					{
						Letter_A_2[i].current_X += 2;
					}
					else
					{
						Letter_A_2[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_A_2[i].direction)
				{
					if (Letter_A_2[i].current_Y/*gY*/ < Letter_A_2[i].ending_Y)
					{
						Letter_A_2[i].current_Y += 2;
					}
					else
					{
						Letter_A_2[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_A_2[i].direction)
				{
					if (Letter_A_2[i].current_X/*gX*/ > Letter_A_2[i].ending_X)
					{
						Letter_A_2[i].current_X -= 2;
					}
					else
					{
						Letter_A_2[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_A_2[i].direction)
				{
					if (Letter_A_2[i].current_Y/*gY*/ > Letter_A_2[i].ending_Y)
					{
						Letter_A_2[i].current_Y -= 2;
					}
					else
					{
						Letter_A_2[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_A_2[i].current_X, Letter_A_2[i].current_Y, &Letter_A_2[i].prev_X, &Letter_A_2[i].prev_Y);
	}

	return;
}

void Update_Letter_Y_2(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 4; i++)
	{
		if (false == Letter_Y_2[i].completed)
		{
			if (Letter_Y_2[i].direction_positive)
			{
				if (DIRECTION_X == Letter_Y_2[i].direction)
				{
					if (Letter_Y_2[i].current_X/*gX*/ < Letter_Y_2[i].ending_X)
					{
						Letter_Y_2[i].current_X += 2;
					}
					else
					{
						Letter_Y_2[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_Y_2[i].direction)
				{
					if (Letter_Y_2[i].current_Y/*gY*/ < Letter_Y_2[i].ending_Y)
					{
						Letter_Y_2[i].current_Y += 2;
					}
					else
					{
						Letter_Y_2[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_Y_2[i].direction)
				{
					if (Letter_Y_2[i].current_X/*gX*/ > Letter_Y_2[i].ending_X)
					{
						Letter_Y_2[i].current_X -= 2;
					}
					else
					{
						Letter_Y_2[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_Y_2[i].direction)
				{
					if (Letter_Y_2[i].current_Y/*gY*/ > Letter_Y_2[i].ending_Y)
					{
						Letter_Y_2[i].current_Y -= 2;
					}
					else
					{
						Letter_Y_2[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_Y_2[i].current_X, Letter_Y_2[i].current_Y, &Letter_Y_2[i].prev_X, &Letter_Y_2[i].prev_Y);
	}

	return;
}

void Update_Letter_S(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 5; i++)
	{
		if (false == Letter_S[i].completed)
		{
			if (Letter_S[i].direction_positive)
			{
				if (DIRECTION_X == Letter_S[i].direction)
				{
					if (Letter_S[i].current_X/*gX*/ < Letter_S[i].ending_X)
					{
						Letter_S[i].current_X += 2;
					}
					else
					{
						Letter_S[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_S[i].direction)
				{
					if (Letter_S[i].current_Y/*gY*/ < Letter_S[i].ending_Y)
					{
						Letter_S[i].current_Y += 2;
					}
					else
					{
						Letter_S[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_S[i].direction)
				{
					if (Letter_S[i].current_X/*gX*/ > Letter_S[i].ending_X)
					{
						Letter_S[i].current_X -= 2;
					}
					else
					{
						Letter_S[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_S[i].direction)
				{
					if (Letter_S[i].current_Y/*gY*/ > Letter_S[i].ending_Y)
					{
						Letter_S[i].current_Y -= 2;
					}
					else
					{
						Letter_S[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_S[i].current_X, Letter_S[i].current_Y, &Letter_S[i].prev_X, &Letter_S[i].prev_Y);
	}

	return;
}

void Update_Letter_I_2(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 3; i++)
	{
		if (false == Letter_I_2[i].completed)
		{
			if (Letter_I_2[i].direction_positive)
			{
				if (DIRECTION_X == Letter_I_2[i].direction)
				{
					if (Letter_I_2[i].current_X/*gX*/ < Letter_I_2[i].ending_X)
					{
						Letter_I_2[i].current_X += 2;
					}
					else
					{
						Letter_I_2[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_I_2[i].direction)
				{
					if (Letter_I_2[i].current_Y/*gY*/ < Letter_I_2[i].ending_Y)
					{
						Letter_I_2[i].current_Y += 2;
					}
					else
					{
						Letter_I_2[i].completed = true;
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_I_2[i].direction)
				{
					if (Letter_I_2[i].current_X/*gX*/ > Letter_I_2[i].ending_X)
					{
						Letter_I_2[i].current_X -= 2;
					}
					else
					{
						Letter_I_2[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_I_2[i].direction)
				{
					if (Letter_I_2[i].current_Y/*gY*/ > Letter_I_2[i].ending_Y)
					{
						Letter_I_2[i].current_Y -= 2;
					}
					else
					{
						Letter_I_2[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_I_2[i].current_X, Letter_I_2[i].current_Y, &Letter_I_2[i].prev_X, &Letter_I_2[i].prev_Y);
	}

	return;
}

void Update_Letter_R_2(void)
{
	//
	// Function Declarations
	//
	void Draw_Letters(int x, int y, int *lastX, int *lastY);

	//
	// Code
	//
	for (int i = 0; i < 5; i++)
	{
		if (false == Letter_R_2[i].completed)
		{
			if (Letter_R_2[i].direction_positive)
			{
				if (DIRECTION_X == Letter_R_2[i].direction)
				{
					if (Letter_R_2[i].current_X/*gX*/ < Letter_R_2[i].ending_X)
					{
						Letter_R_2[i].current_X += 2;
					}
					else
					{
						Letter_R_2[i].completed = true;
					}
				}
				else if (DIRECTION_Y == Letter_R_2[i].direction)
				{
					if (Letter_R_2[i].current_Y/*gY*/ < Letter_R_2[i].ending_Y)
					{
						Letter_R_2[i].current_Y += 2;
					}
					else
					{
						Letter_R_2[i].completed = true;
					}
				}
				else if (DIRECTION_BOTH == Letter_R_2[i].direction)
				{
					if(Letter_R_2[i].current_Y >= Letter_R_2[i].ending_Y && Letter_R_2[i].current_X >= Letter_R_2[i].ending_X)
					{
						Letter_R_2[i].completed = true;
					}
					else
					{
						if (Letter_R_2[i].current_X/*gX*/ < Letter_R_2[i].ending_X)
						{
							Letter_R_2[i].current_X += 2;
						}

						if (Letter_R_2[i].current_Y/*gY*/ < Letter_R_2[i].ending_Y)
						{
							Letter_R_2[i].current_Y += 2;
						}
					}
				}
			}
			else
			{
				if (DIRECTION_X == Letter_R_2[i].direction)
				{
					if (Letter_R_2[i].current_X/*gX*/ > Letter_R_2[i].ending_X)
					{
						Letter_R_2[i].current_X -= 2;
					}
					else
					{
						Letter_R_2[i].completed = true;
					}

				}
				else if (DIRECTION_Y == Letter_R_2[i].direction)
				{
					if (Letter_R_2[i].current_Y/*gY*/ > Letter_R_2[i].ending_Y)
					{
						Letter_R_2[i].current_Y -= 2;
					}
					else
					{
						Letter_R_2[i].completed = true;
					}
				}
			}
		}

		Draw_Letters(Letter_R_2[i].current_X, Letter_R_2[i].current_Y, &Letter_R_2[i].prev_X, &Letter_R_2[i].prev_Y);
	}

	return;
}

void Display(void)
{

	//
	// Code
	//
	if (pep_gbStart)
	{
		Update_Letter_H();
		Update_Letter_A();
		Update_Letter_P();
		Update_Letter_P_2();
		Update_Letter_Y();

		Update_Letter_B();
		Update_Letter_I();
		Update_Letter_R();
		Update_Letter_T();
		Update_Letter_H_2();
		Update_Letter_D();
		Update_Letter_A_2();
		Update_Letter_Y_2();

		Update_Letter_S();
		Update_Letter_I_2();
		Update_Letter_R_2();
	}

	glClear(GL_COLOR_BUFFER_BIT);

	vmath::mat4 modelMatrix = mat4::identity();
	vmath::mat4 viewMatrix = mat4::identity();

	glUseProgram(pep_gFluidShaderProgramObject);

	glUniformMatrix4fv(pep_gUniformModelMatrix, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_gUniformViewMatrix, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_gUniformProjectionMatrix, 1, GL_FALSE, pep_gProjectionMatrix);
	glUniform4f(pep_gUniformColor, 0.0f, 0.0f, 0.0f, 1.0f);
	//glUniform4f(pep_gUniformColor, 0.960f, 0.760f, 0.760f, 1.0f);

	glBindBuffer(GL_ARRAY_BUFFER, pep_gCpuVbo);
	glDrawArrays(GL_POINTS, 0, PEP_DOMAINSIZE);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glUseProgram(pep_gFluidShaderProgramObject);

	SwapBuffers(pep_gHdc);

	return;
}

GLint InitializeShaderProgram(void)
{
	//
	// Code
	//
	pep_gFluidVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_gFluidVertexShaderObject)
	{
		return -1;
	}

	const GLchar *fluid_vertexShaderSourceCode =

		"#version 450 core" \
		"\n" \
		"in vec2 vPosition;" \

		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \

		"void main()" \
		"{" \

			"gl_Position      = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vec4(vPosition, 0.0, 1.0);" \

		"}";

	glShaderSource(pep_gFluidVertexShaderObject, 1, (const GLchar **)&fluid_vertexShaderSourceCode, NULL);
	glCompileShader(pep_gFluidVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_gFluidVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {

		glGetShaderiv(pep_gFluidVertexShaderObject, GL_INFO_LOG_LENGTH,
			&iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_gFluidVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_gFluidFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_gFluidFragmentShaderObject) {
		return -1;
	}

	const GLchar *fluid_fragmentShaderSourceCode =

		"#version 450 core"
		"\n"

		"uniform vec4 u_color;" \

		"out vec4 FragColor;" \

		"void main()" \
		"{" \

			"FragColor = u_color;" \

		"}";

	glShaderSource(
		pep_gFluidFragmentShaderObject,
		1,
		(const GLchar **)&fluid_fragmentShaderSourceCode,
		NULL);

	glCompileShader(pep_gFluidFragmentShaderObject);

	glGetShaderiv(
		pep_gFluidFragmentShaderObject,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(
			pep_gFluidFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(
					pep_gFluidFragmentShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	pep_gFluidShaderProgramObject = glCreateProgram();

	glAttachShader(pep_gFluidShaderProgramObject, pep_gFluidVertexShaderObject);
	glAttachShader(pep_gFluidShaderProgramObject, pep_gFluidFragmentShaderObject);

	glBindAttribLocation(pep_gFluidShaderProgramObject, PEP_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(pep_gFluidShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(
		pep_gFluidShaderProgramObject,
		GL_LINK_STATUS,
		&iProgramLinkStatus);

	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(
			pep_gFluidShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(
					pep_gFluidShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	pep_gUniformModelMatrix = glGetUniformLocation(pep_gFluidShaderProgramObject, "u_modelMatrix");
	pep_gUniformViewMatrix = glGetUniformLocation(pep_gFluidShaderProgramObject, "u_viewMatrix");
	pep_gUniformProjectionMatrix = glGetUniformLocation(pep_gFluidShaderProgramObject, "u_projectionMatrix");
	pep_gUniformColor = glGetUniformLocation(pep_gFluidShaderProgramObject, "u_color");

	return 0;
}

void Draw_Letters(int x, int y, int *lastX, int *lastY)
{
	// Convert motion coordinates to domain
	float fx = (*lastX/*pep_gLast_X*/ / (float)pep_gWindowWidth);
	float fy = (*lastY/*pep_gLast_Y*/ / (float)pep_gWindowHeight);
	int nx = (int)(fx * PEP_DIM);
	int ny = (int)(fy * PEP_DIM);

	if (/*pep_gClicked &&*/ nx < PEP_DIM-PEP_FORCE_UPDATE_RADIUS && nx > PEP_FORCE_UPDATE_RADIUS-1 && ny < PEP_DIM-PEP_FORCE_UPDATE_RADIUS && ny > PEP_FORCE_UPDATE_RADIUS-1)
	{
		//fprintf(pep_gpFile, "position : x = %d y = %d\n", x, y);
		int ddx = x - *lastX/*pep_gLast_X*/;
		int ddy = y - *lastY/*pep_gLast_Y*/;
		fx = ddx / (float)pep_gWindowWidth;
		fy = ddy / (float)pep_gWindowHeight;
		int spy = ny-PEP_FORCE_UPDATE_RADIUS;
		int spx = nx-PEP_FORCE_UPDATE_RADIUS;
		AddForces(pep_gDeviceVelocityField, PEP_DIM, PEP_DIM, spx, spy, PEP_FORCE * PEP_DELTA_T * fx, PEP_FORCE * PEP_DELTA_T * fy, PEP_FORCE_UPDATE_RADIUS);
		*lastX/*pep_gLast_X*/ = x;
		*lastY/*pep_gLast_Y*/ = y;
	}
}
