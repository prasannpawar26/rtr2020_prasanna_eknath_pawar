#ifndef DEFINES_H
#define DEFINES_H

#define PEP_DIM    1920       // Square size of solver domain
#define PEP_DOMAINSIZE    (PEP_DIM*PEP_DIM)  // Total domain size
#define PEP_CPADW (PEP_DIM/2+1)  // Padded width for real->complex in-place FFT
#define PEP_RPADW (2*(PEP_DIM/2+1))  // Padded width for real->complex in-place FFT
#define PEP_PADDEDDOMAINSIZE   (PEP_DIM*PEP_CPADW) // Padded total domain size

#define PEP_DELTA_T     0.09f     // Delta T for interative solver
#define PEP_VISCOSITY_CONSTANT    0.0025f   // Viscosity constant
#define PEP_FORCE (5.8f*PEP_DIM) // Force scale factor 
#define PEP_FORCE_UPDATE_RADIUS     4         // Force update radius

#define PEP_TILE_WIDTH 64 // Tile width
#define PEP_TILE_HEIGHT 64 // Tile height
#define PEP_TIDS_IN_X 64 // Tids in X
#define PEP_TIDS_IN_Y 4  // Tids in Y

#endif
