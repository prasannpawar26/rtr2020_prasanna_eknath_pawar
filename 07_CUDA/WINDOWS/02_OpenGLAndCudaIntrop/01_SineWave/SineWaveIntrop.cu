#include <cuda.h>
#include <helper_cuda.h>
#include <helper_math.h>
#include "device_launch_parameters.h"
//#include <helper_gl.h>
//#include <cuda_runtime.h>
//#include <helper_cuda.h>
//#include <helper_math.h>
//#include <helper_cuda.h>
//#include <helper_functions.h>
//#include <math_constants.h>
//#include <rendercheck_gl.h>
////

__global__ void createVertices(float4* positions, float time, unsigned int width, unsigned int height)
{
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
	// Calculate uv coordinates
	float u = x / (float)width;
	float v = y / (float)height;
	u = u * 2.0f - 1.0f;
	v = v * 2.0f - 1.0f;
	// calculate simple sine wave pattern  
	float freq = 4.0f; //number of complete cycles of wave
	float w = sinf(u * freq + time) * cosf(v * freq + time) * 0.5f; // w = y axis point
																	// Write positions
	positions[y * width + x] = make_float4(u, w, v, 1.0f);
}

extern "C" 
void launch_kernel(dim3 dimGrid, dim3 dimBlock, float4* positions, float time,
	unsigned int width, unsigned int height)
{
	createVertices<<<dimGrid, dimBlock>>>(positions, time, width, height); 
}
