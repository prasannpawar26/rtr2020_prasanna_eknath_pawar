
/*

The OpenGL resources that may be mapped into the address space of CUDA are OpenGL buffer, texture, and renderbuffer objects

Interoperability with OpenGL requires that the CUDA device be specified by cudaGLSetGLDevice() before any other runtime calls

A OpenGL buffer object is registered with CUDa using **cudaGraphicsGLRegisterBuffer**. ()
OR
A texture or renderbuffer object is registered using **cudaGraphicsGLRegisterImage**. supports all texture formats with 1, 2, or 4 components 
OR cudaGraphicsRegisterFlagsSurfaceLoadStore => 2D

*/

/*
THIS CODE DEMONSTRATE HOW TO USE INTROP FOR "GL_ARRAY_BUFFER" Buffers
We have used GL_ARRAY_BUFFER For Following uptill now
Vertices
Color
Text Coordinates
*/
#define WINDOWS_LEAN_AND_MEAN
#define NOMINMAX

#include <GL/glew.h>
#include <GL/gl.h>
#include <windows.h>
#include <windowsx.h>
#include <stdio.h>

#define _USE_MATH_DEFINES 1
#include <math.h>


#include "vmath.h"
#include "cuda_gl_interop.h"
//#include <helper_gl.h>
//#include <cuda_runtime.h>
//#include <cuda_gl_interop.h>
//#include <helper_cuda.h>
//#include <helper_math.h>
//#include <helper_cuda.h>
//#include <helper_functions.h>
//#include <math_constants.h>
//#include <rendercheck_gl.h>
//#include "device_launch_parameters.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "glew32.lib")

const unsigned int mesh_width = 256;
const unsigned int mesh_height = 256;
const unsigned int RestartIndex = 0xffffffff;

int pep_gMouseOldX;
int pep_gMouseOldY;
float pep_gRotateX = 20.0f;
float pep_gRotateY = 0.0f;
float pep_gTranslateX = 0.0f;
float pep_gTranslateY = 0.0f;
float pep_gTranslateZ = -2.0f;

extern "C" 
void launch_kernel(float4* pos, uchar4* posColor,
	unsigned int mesh_width, unsigned int mesh_height, float time);

//GLushort* qIndices=NULL; // index values for primitive restart
//int qIndexSize=0;

const int qIndexSize=mesh_width*mesh_height*2*3;
GLushort qIndices[qIndexSize];

int pep_DrawMode=GL_POINTS; // the default draw mode

int pep_SleepTime=0, pep_SleepInc=100;

// break the file modulatity so both perlin and demo kernels build
// some initial values for Perlin
float pep_Gain = 0.75f;
float pep_XStart=2.f;
float pep_YStart=1.f;

float pep_ZOffset = 0.0f;
float pep_Octaves = 2.f;
float pep_Lacunarity = 2.0f;

////////////////////////////////////////////////////////
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow;
bool gbIsFullScreen;

int gWidth;
int gHeight;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum {
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao;
GLuint vbo_position;
GLuint vbo_color;
GLuint vbo_element;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;

mat4 perpsectiveProjectionMatrix;

struct cudaGraphicsResource* positionsVBO_CUDA; 
struct cudaGraphicsResource* colorsVBO_CUDA; 

#define min(a, b) (a < b ? a : b)

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow) {
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("CUDA Introp");

	// code
	if (0 != fopen_s(&gpFile, "log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("CUDA Introp"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, 800, 600, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-2 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-3 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
			"Failed.\nExitting Now... "
			"Successfully\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-4 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-5 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
			"\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (WM_QUIT == msg.message) {
				bDone = true;
			} else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} else {
			if (gbActiveWindow) {
			}
			Update();

			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// code
	switch (iMsg) {
	case WM_MOUSEMOVE:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		float dx, dy;
		dx = (float)(x - pep_gMouseOldX);
		dy = (float)(y - pep_gMouseOldY);

		switch (wParam)
		{
		case MK_LBUTTON:
			pep_gRotateX += dy * 0.2f;
			pep_gRotateY += dx * 0.2f;
			break;

		case MK_MBUTTON:
			pep_gTranslateX += dx * 0.01f;
			pep_gTranslateY -= dy * 0.01f;
			break;

		case MK_RBUTTON:
			pep_gTranslateZ += dy * 0.01f;
			break;
		}

		pep_gMouseOldX = x;
		pep_gMouseOldY = y;
	}
	break;

	case WM_LBUTTONDOWN:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		int button = 0;
		switch (wParam)
		{
		case MK_LBUTTON:
			button = 0;
			break;

		case MK_MBUTTON:
			button = 1;
			break;

		case MK_RBUTTON:
			button = 2;
			break;
		}

		pep_gMouseOldX = x;
		pep_gMouseOldY = y;
	}
	break;

	case WM_LBUTTONUP:
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		pep_gMouseOldX = x;
		pep_gMouseOldY = y;
	}
	break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	case WM_ERASEBKGND:
		return 0;
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_SIZE:
		//gWidth = LOWORD(lParam);
		//gHeight = HIWORD(lParam);
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN: {
		switch (wParam) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'F':
		case 'f':
			ToggledFullScreen();
			break;
		case 'd': case 'D': // pep_DrawMode
			switch(pep_DrawMode)
			{
				case GL_POINTS:
					pep_DrawMode = GL_LINE_STRIP;
					break;

				case GL_LINE_STRIP:
					pep_DrawMode = GL_TRIANGLE_FAN;
					break;

				default: pep_DrawMode=GL_POINTS;
			}
			break;

		case '+': // Perlin: lower the ocean level
			pep_ZOffset += 0.01;
			pep_ZOffset = (pep_ZOffset > 1.0)? 1.0:pep_ZOffset; // guard input
			break;
		case '-': // Perlin: raise the ocean level
			pep_ZOffset -= 0.01;
			pep_ZOffset = (pep_ZOffset < -1.0)? -1.0:pep_ZOffset; // guard input
			break;
		case 'k': case 'K':// move within the Perlin function
			pep_YStart -= 0.1;
			break;
		case 'j':case 'J': // move within the Perlin function
			pep_YStart += 0.1;
			break;
		case 'l': // move within the Perlin function
			pep_XStart += 0.1;
			break;
		case 'h':case 'H': // move within the Perlin function
			pep_XStart -= 0.1;
			break;
		case 'I': // Perlin: change pep_Gain
			pep_Gain += 0.25;
			break;
		case 'i': // Perlin: change pep_Gain
			pep_Gain -= 0.25;
			pep_Gain = (pep_Gain < 0.25)?0.25:pep_Gain; // guard input
			break;
		case 'O': // Perlin: change pep_Octaves
			pep_Octaves += 1.0f;
			pep_Octaves = (pep_Octaves > 8)?8:pep_Octaves; // guard input
			break;
		case 'o': // Perlin: change pep_Octaves
			pep_Octaves -= 1.0f;
			pep_Octaves = (pep_Octaves<2)?2:pep_Octaves; // guard input
			break;
		case 'P': // Perlin: change pep_Lacunarity
			pep_Lacunarity += 0.25;
			break;
		case 'p': // Perlin: change pep_Lacunarity
			pep_Lacunarity -= 0.25;
			pep_Lacunarity = (pep_Lacunarity<0.2)?0.2:pep_Lacunarity; // guard input
			break;
		case 'S': // Slow the simulation down
			pep_SleepTime += 100;
			break;
		case 's': // Speed the simulation up
			pep_SleepTime = (pep_SleepTime > 0)?pep_SleepTime -= pep_SleepInc:0;
			break;

		}
	} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen) {
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle) {
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	} else {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

int Initialize(void) {
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	// function declarations
	void ReSize(int, int);
	void Uninitialize(void);

	gWidth = 800;
	gHeight = 600;
	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc) {
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result) {
		return -5;
	}

	// Explicitly set device 0
	//cudaGLSetGLDevice(0); 

	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \

		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"out vec4 color;"
		"void main(void)" \
		"{" \
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
			"color = vColor;"
		"}";

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFragmentShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 color;"
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"FragColor = color;" \
		"}";

	glShaderSource(giFragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(giFragmentShaderObject);

	glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);
	glAttachShader(giShaderProgramObject, giFragmentShaderObject);

	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");
	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_COLOR,
		"vColor");

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		glGetShaderiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	modelUniform = glGetUniformLocation(giShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(giShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(giShaderProgramObject, "u_projection_matrix");

	glGenVertexArrays(1, &vao);

	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, mesh_width * mesh_height * sizeof(float4) /*gWidth * gHeight * 4 * sizeof(float)*/, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
	glBufferData(GL_ARRAY_BUFFER, mesh_width * mesh_height * sizeof(uchar4) /*gWidth * gHeight * 4 * sizeof(float)*/, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 4, GL_UNSIGNED_BYTE, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	//

	//Registers the buffer object specified by vbo_position for access by CUDA
	cudaGraphicsGLRegisterBuffer(&positionsVBO_CUDA, vbo_position, cudaGraphicsMapFlagsWriteDiscard);
	cudaGraphicsGLRegisterBuffer(&colorsVBO_CUDA, vbo_color, cudaGraphicsMapFlagsWriteDiscard);

	// allocate and assign trianglefan indicies 
	//qIndexSize = 5*(mesh_height-1)*(mesh_width-1);
	/*qIndexSize = mesh_width*mesh_height*2*3;
	qIndices = (GLushort *) malloc(qIndexSize*sizeof(GLushort));*/
	//int Indicesindex=0;
	//for(int i=1; i < mesh_height; i++) {
	//	for(int j=1; j < mesh_width; j++) {
	//		qIndices[Indicesindex++] = (i)*mesh_width + j; 
	//		qIndices[Indicesindex++] = (i)*mesh_width + j-1; 
	//		qIndices[Indicesindex++] = (i-1)*mesh_width + j-1; 
	//		qIndices[Indicesindex++] = (i-1)*mesh_width + j; 
	//		qIndices[Indicesindex++] = RestartIndex;
	//	}
	//}

	GLushort* id=&qIndices[0];
	for (int i = 0; i < mesh_height; i++) {
		for (int j = 0; j < mesh_width; j++) {
			int i0 = i * (mesh_width+1) + j;
			int i1 = i0 + 1;
			int i2 = i0 + (mesh_width+1);
			int i3 = i2 + 1;
			if ((j+i)%2) {
				*id++ = i0; *id++ = i2; *id++ = i1;
				*id++ = i1; *id++ = i2; *id++ = i3;
			} else {
				*id++ = i0; *id++ = i2; *id++ = i3;
				*id++ = i0; *id++ = i3; *id++ = i1;
			}
		}
	}

	glGenBuffers(1, &vbo_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(qIndices), qIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	perpsectiveProjectionMatrix = mat4::identity();

	ReSize(gWidth, gHeight);

	return 0;
}

void ReSize(int width, int height) {
	// code
	if (0 == height) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perpsectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	return;
}

void Display(void) {
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(giShaderProgramObject);

	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;

	mat4 rotationMatrix;

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();

	modelMatrix =
		modelMatrix *
		rotate(pep_gRotateX, 1.0f, 0.0f, 0.0f) *
		rotate(pep_gRotateY, 0.0f, 1.0f, 0.0f);

	viewMatrix = translate(pep_gTranslateX, pep_gTranslateY, pep_gTranslateZ);

	projectionMatrix = perpsectiveProjectionMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

	switch(pep_DrawMode)
	{
		case GL_LINE_STRIP:
			glBindVertexArray(vao);
			for (int i = 0; i < mesh_width*mesh_height; i += mesh_width)
			{
				glDrawArrays(GL_LINE_STRIP, i, mesh_width);
			}
			glBindVertexArray(0);
			break;
		case GL_TRIANGLE_FAN:
		{
			glBindVertexArray(vao);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element);
			glDrawElements(GL_TRIANGLE_FAN, qIndexSize, GL_UNSIGNED_SHORT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}
		break;
		default:
			glBindVertexArray(vao);
			glDrawArrays(GL_POINTS, 0, mesh_width * mesh_height);
			glBindVertexArray(0);
			break;
	}

	glUseProgram(0);

	SwapBuffers(gHdc);

	return;
}

void Uninitialize(void)
{
	//code
	// Unregisters the graphics resource resource so it is not accessible by CUDA
	cudaGraphicsUnregisterResource(colorsVBO_CUDA);
	cudaGraphicsUnregisterResource(positionsVBO_CUDA);

	if (vbo_color)
	{
		glDeleteBuffers(1, &vbo_color);
		vbo_color = 0;
	}

	if (vbo_position)
	{
		glDeleteBuffers(1, &vbo_position);
		vbo_position = 0;
	}

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully ",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != gpFile)
	{
		fclose(gpFile);
	}

	return;
}

float animTime = 0.0;
void Update(void)
{
	// Map buffer object for writing from CUDA
	size_t num_bytes;
	float4* positions;
	uchar4* colors;
	// Maps the count graphics resources in resources for access by CUDA.
	// The resources in resources may be accessed by CUDA until they are unmapped.
	//The graphics API from which resources were registered should not access any resources while they are mapped by CUDA
	cudaGraphicsMapResources(1, &positionsVBO_CUDA, 0);
	cudaGraphicsResourceGetMappedPointer((void**)&positions, &num_bytes, positionsVBO_CUDA); // Returns in *devPtr a pointer through which the mapped graphics resource resource may be accessed by CUDA
	cudaGraphicsMapResources(1, &colorsVBO_CUDA, 0);
	cudaGraphicsResourceGetMappedPointer((void**)&colors, &num_bytes, colorsVBO_CUDA);

	// execute the kernel
	launch_kernel(positions, colors, mesh_width, mesh_height, animTime);

	// Unmap buffer object
	// Unmaps the count graphics resources in resources.
	// Once unmapped, the resources in resources may not be accessed by CUDA until they are mapped apep_Gain
	cudaGraphicsUnmapResources(1, &colorsVBO_CUDA, 0);
	cudaGraphicsUnmapResources(1, &positionsVBO_CUDA, 0);

	animTime += 0.01f;
}
