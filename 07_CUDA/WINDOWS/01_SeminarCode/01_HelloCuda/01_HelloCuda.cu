// Header Files
#include <stdio.h>
#include <cuda.h>

// Global Variables
int gInputLength = 5;

float *gHostInput1 = NULL;
float *gHostInput2 = NULL;
float *gHostOutput = NULL;

float *gDeviceInput1 = NULL;
float *gDeviceInput2 = NULL;
float *gDeviceOutput = NULL;

// Global Kernel Function Definations

__global__ void vecAdd(float *in1, float *in2, float *out, int len)
{
	// Variable Declarations
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	// Code
	if(i < len) // Need For Boundary Condition
	{
		out[i] = in1[i] + in2[i];
	}
}

int main(int argc, char *argv)
{
	// Function Declarations
	void Cleanup(void);

	// Code

	// Allocate Host(CPU) Memory For Input Array 1
	gHostInput1 = (float *)malloc(gInputLength * sizeof(float));
	if(NULL == gHostInput1)
	{
		printf("CPU (Host) Memory Fatal Error = Can Not Allcoate Enough  Memory For Host Input Array 1 \n Exitting...\n");
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Allocate Host(CPU) Memory For Input Array 2
	gHostInput2 = (float *)malloc(gInputLength * sizeof(float));
	if(NULL == gHostInput2)
	{
		printf("CPU (Host) Memory Fatal Error = Can Not Allcoate Enough  Memory For Host Input Array 2 \n Exitting...\n");
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Allocate Host(CPU) Memory For Input Array 3
	gHostInput2 = (float *)malloc(gInputLength * sizeof(float));
	if(NULL == gHostInput2)
	{
		printf("CPU (Host) Memory Fatal Error = Can Not Allcoate Enough  Memory For Host Input Array 3 \n Exitting...\n");
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Allocate Host(CPU) Memory For Output Array
	gHostOutput = (float *)malloc(gInputLength * sizeof(float));
	if(NULL == gHostInput1)
	{
		printf("CPU (Host) Memory Fatal Error = Can Not Allcoate Enough  Memory For Host Output Array. \n Exitting...\n");
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Fill Above Input Host Vectors / Arrays With Arbitary But Hard Coded Data
	gHostInput1[0] = 101.0;
	gHostInput1[1] = 201.0;
	gHostInput1[2] = 301.0;
	gHostInput1[3] = 401.0;
	gHostInput1[4] = 501.0;

	gHostInput2[0] = 1101.0;
	gHostInput2[1] = 1201.0;
	gHostInput2[2] = 1301.0;
	gHostInput2[3] = 1401.0;
	gHostInput2[4] = 1501.0;

	// Allocate Device Memory (GPU)
	int size = gInputLength * sizeof(float);
	cudaError_t err = cudaSuccess;

	err = cudaMalloc((void **)&gDeviceInput1, size);
	if(cudaSuccess != err)
	{
		printf("Device Memory Allocation Fail Error = %s.\n Exitting Now...\n", cudaGetErrorString(err));
		Cleanup();
		exit(EXIT_FAILURE);
	}

	err = cudaMalloc((void **)&gDeviceInput2, size);
	if(cudaSuccess != err)
	{
		printf("Device Memory Allocation Fail Error = %s.\n Exitting Now...\n", cudaGetErrorString(err));
		Cleanup();
		exit(EXIT_FAILURE);
	}

	err = cudaMalloc((void **)&gDeviceOutput, size);
	if(cudaSuccess != err)
	{
		printf("Device Memory Allocation Fail Error = %s.\n Exitting Now...\n", cudaGetErrorString(err));
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Copy Host Memory Contents To Device Memory
	err = cudaMemcpy(gDeviceInput1, gHostInput1, size, cudaMemcpyHostToDevice);
	if(err != cudaSuccess)
	{
		printf("cudaMemcpy Failed. Error : %s \n", cudaGetErrorString(err));
		Cleanup();
		exit(EXIT_FAILURE);
	}

	err = cudaMemcpy(gDeviceInput2, gHostInput2, size, cudaMemcpyHostToDevice);
	if(err != cudaSuccess)
	{
		printf("cudaMemcpy Failed. Error : %s \n", cudaGetErrorString(err));
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Cuda kernel Configuration
	dim3 DimGrid = dim3(ceil(gInputLength / 256.0), 1, 1);
	dim3 DimBlock = dim3(256, 1, 1);

	// Call Kernel
	vecAdd<<<DimGrid, DimBlock>>>(gDeviceInput1, gDeviceInput2, gDeviceOutput, gInputLength);

	err = cudaMemcpy(gHostOutput, gDeviceOutput, size, cudaMemcpyDeviceToHost);
	if(err != cudaSuccess)
	{
		printf("cudaMemcpy Failed. Error : %s \n", cudaGetErrorString(err));
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Result
	for(int i = 0; i < gInputLength; i++)
	{
		printf("%f %f = %f\n", gHostInput1[i], gHostInput2[i], gHostOutput[i]);
	}

	// Cleanup
	Cleanup();

	return 0;
}

void Cleanup(void)
{
	// Code
	if(NULL != gDeviceOutput)
	{
		cudaFree(gDeviceOutput);
		gDeviceOutput = NULL;
	}

	if(NULL != gDeviceInput2)
	{
		cudaFree(gDeviceInput2);
		gDeviceInput2 = NULL;
	}

	if(NULL != gDeviceInput1)
	{
		cudaFree(gDeviceInput1);
		gDeviceInput1 = NULL;
	}

	if(NULL != gHostOutput)
	{
		free(gHostOutput);
		gHostOutput = NULL;
	}

	if(NULL != gHostInput2)
	{
		free(gHostInput2);
		gHostInput1 = NULL;
	}

	if(NULL != gHostInput1)
	{
		free(gHostInput1);
		gHostInput1 = NULL;
	}

	return;
}
