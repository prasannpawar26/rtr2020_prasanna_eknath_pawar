// ObjModelLoading.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <conio.h>

#define LINE_LENGTH 256
FILE *gpFile = NULL;

#define VERTEX_IN_3D_PLANE 3
#define TEXCOORD 2
#define NORMAL_IN_3D_PLANE 3
#define FACE_TUPLE 3

typedef struct _TREE_DATA
{
	int data_count;
	float *pTreeVertices;
	float *pTreeTexcoords;
	float *pTreeNormals;
} TREE_DATA;
TREE_DATA *pTreeDataArray = NULL;

int main(int argc, char **argv)
{
	typedef struct _MODEL
	{
		int object_count;
		//
		// Complete Vertices Data (Not Properly arrange)  -> To Get Proper Faces/Arranged Data Of Each Object Refer Per Object Arrays And Use Elements/Indices To Get Proper Value of Vertices Data In Below Mentioned Arrays
		// 1. Vertex Position
		// 2. Normals
		// 3. TexCoords
		//
		int v_count; // count without vec3 touple
		float *pVerticesArray;

		int vn_count;
		float *pNormalsArray;

		int vt_count;
		float *pTexcoordsArray;

	} MODEL;

	MODEL model;

	typedef struct _OBJECT
	{
		// Face/Indices To Used In  Model Arrays
		int element_count; // count without vec3 touple
		int *pElementVertices;
		int *pElementTexcoord;
		int *pElementNormal;

	} OBJECT;
	OBJECT *pObjectArray = NULL;

	char buf[LINE_LENGTH];
	
	if (argc < 2)
	{
		printf("USAGE : ObjModelLoading.exe <ModelFileName.obj>");
		exit(0);
	}

	if (0 != fopen_s(&gpFile, argv[1], "r"))
	{
		printf("Failed To Open File");
		return 1;
	}

	memset(&model, 0, sizeof(MODEL));

	unsigned int line_index = 0;
	unsigned int object_count = 0;
	memset(buf, 0, LINE_LENGTH);
	while (NULL != fgets(buf, LINE_LENGTH, gpFile))
	{
		char *context = NULL;
		char *token = strtok_s(buf, " ", &context);
		
		if (0 == strcmp(token, "o"))
		{
			if (NULL == pObjectArray)
			{
				pObjectArray = (OBJECT *)malloc(sizeof(OBJECT));
				memset(pObjectArray, 0, sizeof(OBJECT));
				model.object_count = 1;
			}
			else
			{
				model.object_count += 1;
				pObjectArray = (OBJECT *)realloc(pObjectArray, model.object_count * sizeof(OBJECT));
				memset(pObjectArray + model.object_count - 1, 0, sizeof(OBJECT));
			}
		}
		else if (0 == strcmp(token, "v"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pVerticesArray)
			{
				model.v_count = 3;
				model.pVerticesArray = (float *)malloc(model.v_count * sizeof(float));
				memset(model.pVerticesArray, 0, sizeof(model.v_count * sizeof(float)));
			}
			else
			{
				model.v_count += 3;
				model.pVerticesArray = (float *)realloc(model.pVerticesArray, model.v_count * sizeof(float));
			}

			model.pVerticesArray[model.v_count - 3] = x;
			model.pVerticesArray[model.v_count - 2] = y;
			model.pVerticesArray[model.v_count - 1] = z;

		}
		else if (0 == strcmp(token, "vn"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pNormalsArray)
			{
				model.vn_count = 3;
				model.pNormalsArray = (float *)malloc(model.vn_count * sizeof(float));
				memset(model.pNormalsArray, 0, sizeof(model.vn_count * sizeof(float)));
			}
			else
			{
				model.vn_count += 3;
				model.pNormalsArray = (float *)realloc(model.pNormalsArray, model.vn_count * sizeof(float));
			}

			model.pNormalsArray[model.vn_count - 3] = x;
			model.pNormalsArray[model.vn_count - 2] = y;
			model.pNormalsArray[model.vn_count - 1] = z;

		}
		else if (0 == strcmp(token, "vt"))
		{
			float x, y;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pTexcoordsArray)
			{
				model.vt_count = 2;
				model.pTexcoordsArray = (float *)malloc(model.vt_count * sizeof(float));
				memset(model.pTexcoordsArray, 0, sizeof(model.vt_count * sizeof(float)));
			}
			else
			{
				model.vt_count += 2;
				model.pTexcoordsArray = (float *)realloc(model.pTexcoordsArray, model.vt_count * sizeof(float));
			}

			model.pTexcoordsArray[model.vt_count - 2] = x;
			model.pTexcoordsArray[model.vt_count - 1] = y;
		}

		else if (0 == strcmp(token, "f"))
		{
			char *first_token = strtok_s(NULL, " ", &context);
			char *second_token = strtok_s(NULL, " ", &context);
			char *third_token = strtok_s(NULL, " ", &context);

			char *first_context = NULL;
			int v1 = (int)atoi(strtok_s(first_token, "/", &first_context));
			int vt1 = (int)atoi(strtok_s(NULL, "/", &first_context));
			int vn1 = (int)atoi(strtok_s(NULL, "/", &first_context));

			char *second_context = NULL;
			int v2 = (int)atoi(strtok_s(second_token, "/", &second_context));
			int vt2 = (int)atoi(strtok_s(NULL, "/", &second_context));
			int vn2 = (int)atoi(strtok_s(NULL, "/", &second_context));

			char *third_context = NULL;
			int v3 = (int)atoi(strtok_s(third_token, "/", &third_context));
			int vt3 = (int)atoi(strtok_s(NULL, "/", &third_context));
			int vn3 = (int)atoi(strtok_s(NULL, "/", &third_context));

			pObjectArray[model.object_count - 1].element_count += 3;
			if (NULL == pObjectArray[model.object_count - 1].pElementVertices)
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementVertices, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)realloc(pObjectArray[model.object_count - 1].pElementVertices, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 3] = v1;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 2] = v2;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 1] = v3;

			if (NULL == pObjectArray[model.object_count - 1].pElementNormal)
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementNormal, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)realloc(pObjectArray[model.object_count - 1].pElementNormal, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 3] = vn1;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 2] = vn2;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 1] = vn3;

			if (NULL == pObjectArray[model.object_count - 1].pElementTexcoord)
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementTexcoord, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)realloc(pObjectArray[model.object_count - 1].pElementTexcoord, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 3] = vt1;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 2] = vt2;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 1] = vt3;
		}

		memset(buf, 0, LINE_LENGTH);
	}

	printf("---------------------------------------------------------------------------------------------------------------------------------------------------------\n");
	printf("\nobject_count : %d\n", model.object_count);

	printf("\n\n");
	printf("VERTICES COUNT: %d\n", (int)model.v_count / 3);
	printf("TEXCOORDS COUNT: %d\n", (int)model.vt_count / 2);
	printf("NORMALS COUNT: %d\n", (int)model.vn_count / 3);

	printf("\n\n");
	printf("---------------------------------------------------------------------------------------------------------------------------------------------------------\n");

	
	pTreeDataArray = (TREE_DATA *)malloc(sizeof(TREE_DATA) * model.object_count);
	memset(pTreeDataArray, 0, sizeof(TREE_DATA) * model.object_count);

	for (int i = 0; i < model.object_count; i++)
	{
		printf("\n\n===========================================================Object %d ==========================================================\n\n", i + 1);
		pTreeDataArray[i].data_count = pObjectArray[i].element_count / 3;

		pTreeDataArray[i].pTreeVertices = (float *)malloc(sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * VERTEX_IN_3D_PLANE);
		memset(pTreeDataArray[i].pTreeVertices, 0, sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * VERTEX_IN_3D_PLANE);

		pTreeDataArray[i].pTreeTexcoords = (float *)malloc(sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * TEXCOORD);
		memset(pTreeDataArray[i].pTreeTexcoords, 0, sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * TEXCOORD);

		pTreeDataArray[i].pTreeNormals = (float *)malloc(sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * NORMAL_IN_3D_PLANE);
		memset(pTreeDataArray[i].pTreeNormals, 0, sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * NORMAL_IN_3D_PLANE);

		int vi = 0;
		int vt = 0;
		int vn = 0;
		for (int fn = 0; fn < pObjectArray[i].element_count / FACE_TUPLE; fn++)
		{
			int v1 = pObjectArray[i].pElementVertices[fn * FACE_TUPLE + 0] - 1; // Index is array start with Zero Hence "-1" From The Element Index Which Is Start From 1
			int v2 = pObjectArray[i].pElementVertices[fn * FACE_TUPLE + 1] - 1;
			int v3 = pObjectArray[i].pElementVertices[fn * FACE_TUPLE + 2] - 1;

			pTreeDataArray[i].pTreeVertices[vi + 0] = model.pVerticesArray[v1 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeVertices[vi + 1] = model.pVerticesArray[v1 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeVertices[vi + 2] = model.pVerticesArray[v1 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeVertices[vi + 3] = model.pVerticesArray[v2 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeVertices[vi + 4] = model.pVerticesArray[v2 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeVertices[vi + 5] = model.pVerticesArray[v2 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeVertices[vi + 6] = model.pVerticesArray[v3 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeVertices[vi + 7] = model.pVerticesArray[v3 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeVertices[vi + 8] = model.pVerticesArray[v3 * VERTEX_IN_3D_PLANE + 2];

			int t1 = pObjectArray[i].pElementTexcoord[fn * FACE_TUPLE + 0] - 1;
			int t2 = pObjectArray[i].pElementTexcoord[fn * FACE_TUPLE + 1] - 1;
			int t3 = pObjectArray[i].pElementTexcoord[fn * FACE_TUPLE + 2] - 1;

			pTreeDataArray[i].pTreeTexcoords[vt + 0] = model.pTexcoordsArray[t1 * TEXCOORD + 0];
			pTreeDataArray[i].pTreeTexcoords[vt + 1] = model.pTexcoordsArray[t1 * TEXCOORD + 1];
			pTreeDataArray[i].pTreeTexcoords[vt + 2] = model.pTexcoordsArray[t2 * TEXCOORD + 0];
			pTreeDataArray[i].pTreeTexcoords[vt + 3] = model.pTexcoordsArray[t2 * TEXCOORD + 1];
			pTreeDataArray[i].pTreeTexcoords[vt + 4] = model.pTexcoordsArray[t3 * TEXCOORD + 0];
			pTreeDataArray[i].pTreeTexcoords[vt + 5] = model.pTexcoordsArray[t3 * TEXCOORD + 1];

			int n1 = pObjectArray[i].pElementNormal[fn * FACE_TUPLE + 0] - 1;
			int n2 = pObjectArray[i].pElementNormal[fn * FACE_TUPLE + 1] - 1;
			int n3 = pObjectArray[i].pElementNormal[fn * FACE_TUPLE + 2] - 1;

			pTreeDataArray[i].pTreeNormals[vn + 0] = model.pNormalsArray[n1 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeNormals[vn + 1] = model.pNormalsArray[n1 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeNormals[vn + 2] = model.pNormalsArray[n1 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeNormals[vn + 3] = model.pNormalsArray[n2 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeNormals[vn + 4] = model.pNormalsArray[n2 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeNormals[vn + 5] = model.pNormalsArray[n2 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeNormals[vn + 6] = model.pNormalsArray[n3 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeNormals[vn + 7] = model.pNormalsArray[n3 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeNormals[vn + 8] = model.pNormalsArray[n3 * VERTEX_IN_3D_PLANE + 2];

			printf("[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeVertices[vi + 0], pTreeDataArray[i].pTreeVertices[vi + 1], pTreeDataArray[i].pTreeVertices[vi + 2]);
			printf("[%f, %f]\n" ,pTreeDataArray[i].pTreeTexcoords[vt + 0], pTreeDataArray[i].pTreeTexcoords[vt + 1]);
			printf("[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeNormals[vn + 0], pTreeDataArray[i].pTreeNormals[vn + 1], pTreeDataArray[i].pTreeNormals[vn + 2]);
			printf("\n");
			printf("[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeVertices[vi + 3], pTreeDataArray[i].pTreeVertices[vi + 4], pTreeDataArray[i].pTreeVertices[vi + 5]);
			printf("[%f, %f]\n" ,pTreeDataArray[i].pTreeTexcoords[vt + 2], pTreeDataArray[i].pTreeTexcoords[vt + 3]);
			printf("[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeNormals[vn + 3], pTreeDataArray[i].pTreeNormals[vn + 4], pTreeDataArray[i].pTreeNormals[vn + 5]);
			printf("\n");
			printf("[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeVertices[vi + 6], pTreeDataArray[i].pTreeVertices[vi + 7], pTreeDataArray[i].pTreeVertices[vi + 8]);
			printf("[%f, %f]\n" ,pTreeDataArray[i].pTreeTexcoords[vt + 4], pTreeDataArray[i].pTreeTexcoords[vt + 5]);
			printf("[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeNormals[vn + 6], pTreeDataArray[i].pTreeNormals[vn + 7], pTreeDataArray[i].pTreeNormals[vn + 8]);
			printf("\n");

			vi += 9;
			vt += 6;
			vn += 9;
		}
	}

	return 0;
}
