#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

#define VERTEX_IN_3D_PLANE 3
#define TEXCOORD 2
#define NORMAL_IN_3D_PLANE 3
#define FACE_TUPLE 3

#define LINE_LENGTH 256
FILE *gpFileObj = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow;
bool gbIsFullScreen;

bool gbIsLightEnable;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum {
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint *vao_tree;
GLuint *vbo_tree_vertex;
GLuint *vbo_tree_normal;

unsigned int gNumVertices;
unsigned int gNumElements;

// UNIFORMS
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint lightAmbientUniform;
GLuint lightDiffuseUniform;
GLuint lightSpecularUniform;
GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShinessUniform;
GLuint lightPositionUniform;
GLuint keyLPressedUniform;

//
// Application Variables Associated With Uniforms
float lightAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition[4] = {0.0f, 10.0f, 100.0f, 1.0f};

float materialAmbient_0[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float materialDiffuse_0[4] = {0.185098f, 0.367059f, 0.040784f, 1.0f};
float materialSpecular_0[4] = {0.185098f, 0.367059f, 0.040784f, 1.0f};
float materialShiness_0 = 19.607843f;  // Also Try 128.0f

float materialAmbient_1[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float materialDiffuse_1[4] = {0.335686f, 0.150588f, 0.018824f, 1.0f};
float materialSpecular_1[4] = {0.335686f, 0.150588f, 0.018824f, 1.0f};
float materialShiness_1 = 19.607843f; // Also Try 128.0f

int lKeyPressed = 0;

mat4 modelMatrix;
mat4 viewMatrix;
mat4 perspectiveProjectionMatrix;
mat4 translateMatrix;

typedef struct _TREE_DATA
{
	int data_count;
	float *pTreeVertices;
	float *pTreeTexcoords;
	float *pTreeNormals;
} TREE_DATA;
TREE_DATA *pTreeDataArray = NULL;

int gObjectCount = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpSzCmdLine, int iCmdShow)
{
  // function prototype
  int Initialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Tree Modeling Loading (Obj)");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("Tree Modeling Loading (Obj)"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, 800, 600, NULL, NULL, hInstance, NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
            "Failed.\nExitting Now... "
            "Successfully\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
            "\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
            "Successful.\n",
            __FILE__, __LINE__, __FUNCTION__);
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message)
      {
        bDone = true;
      }
      else
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } 
    else
    {
      if (gbActiveWindow)
      {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg)
  {
    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;
    case WM_KEYDOWN:
    {
      switch (wParam)
      {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case 'L':
        case 'l':
          if (false == gbIsLightEnable)
          {
            gbIsLightEnable = true;
            lKeyPressed = 1;
          }
          else
          {
            gbIsLightEnable = false;
            lKeyPressed = 0;
          }
          break;
      }
    } break;
  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

void ReSize(int width, int height)
{
  // code
  if (0 == height)
  {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  perspectiveProjectionMatrix =
      perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

float rotation = 0.0f;
void Display(void)
{
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(giShaderProgramObject);

  modelMatrix = mat4::identity();
  viewMatrix = mat4::identity();
  translateMatrix = mat4::identity();

  modelMatrix = translate(0.0f, -5.0f, -30.0f) * rotate (rotation, 0.0f, 1.0f, 0.0f);

  glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
  glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
  glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                     perspectiveProjectionMatrix);

  for (int i = 0; i < gObjectCount; i++)
  {
      if (gbIsLightEnable)
      {
          glUniform1i(keyLPressedUniform, lKeyPressed);
          glUniform3fv(lightAmbientUniform, 1, lightAmbient);
          glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
          glUniform3fv(lightSpecularUniform, 1, lightSpecular);
          glUniform4fv(lightPositionUniform, 1, lightPosition);
          if (8 == i) // trunk of tree
          {
              glUniform3fv(materialAmbientUniform, 1, materialAmbient_1);
              glUniform3fv(materialDiffuseUniform, 1, materialDiffuse_1);
              glUniform3fv(materialSpecularUniform, 1, materialSpecular_1);
              glUniform1f(materialShinessUniform, materialShiness_1);
          }
          else
          {
              glUniform3fv(materialAmbientUniform, 1, materialAmbient_0);
              glUniform3fv(materialDiffuseUniform, 1, materialDiffuse_0);
              glUniform3fv(materialSpecularUniform, 1, materialSpecular_0);
              glUniform1f(materialShinessUniform, materialShiness_0);
          }
         
      }
      else
      {
          glUniform1i(keyLPressedUniform, lKeyPressed);
      }

      glBindVertexArray(vao_tree[i]);
      glDrawArrays(GL_TRIANGLES, 0, pTreeDataArray[i].data_count * FACE_TUPLE);
      glBindVertexArray(0);
  }

  glUseProgram(0);

  rotation += 1.0f;

  SwapBuffers(gHdc);
}

void Uninitialize(void)
{
  // code

  if (vbo_tree_normal) {
    glDeleteBuffers(gObjectCount, vbo_tree_normal);
    vbo_tree_normal = 0;
  }

  if (vbo_tree_vertex)
  {
    glDeleteBuffers(gObjectCount, vbo_tree_vertex);
    vbo_tree_vertex = 0;
  }

  if (vao_tree)
  {
    glDeleteVertexArrays(gObjectCount, vao_tree);
    vao_tree = 0;
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders) {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(
      gpFile,
      "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
      __FILE__, __LINE__, __FUNCTION__);

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  void ReSize(int, int);
  void Uninitialize(void);
  int ModelLoading(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 8;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
          __FILE__, __LINE__, __FUNCTION__);

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index)
  {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd))
  {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc)
  {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc))
  {
    return -4;
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result)
  {
    return -5;
  }

  if (0 != ModelLoading())
  {
	  return -5;
  }
  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : ModelLoading Done\n",
	  __FILE__, __LINE__, __FUNCTION__);

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject)
  {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

    return -6;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

  const GLchar *vertexShaderSourceCode =
      "#version 450 core"
      "\n" \
      "in vec4 vPosition;" \
      "in vec3 vNormal;" \
      "uniform mat4 u_model_matrix;" \
      "uniform mat4 u_view_matrix;" \
      "uniform mat4 u_projection_matrix;" \
      "uniform int u_key_L_pressed;" \
      "uniform vec4 u_light_position;" \
      "out vec3 light_direction;" \
      "out vec3 tranformation_matrix;" \
      "out vec3 viewer_vector;" \
      "void main(void)" \
      "{" \
          "if(1 == u_key_L_pressed)" \
          "{" \
              "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
              "tranformation_matrix = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
              "light_direction = vec3(u_light_position - eye_coordinates);" \
              "viewer_vector = vec3(-eye_coordinates);" \
          "}" \

          "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);
  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -7;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject)
  {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core"
      "\n" \
      "in vec3 light_direction;" \
      "in vec3 tranformation_matrix;" \
      "in vec3 viewer_vector;" \
      "uniform int u_key_L_pressed;" \
      "uniform vec3 u_light_ambient;" \
      "uniform vec3 u_light_diffuse;" \
      "uniform vec3 u_light_specular;" \
      "uniform vec3 u_material_ambient;" \
      "uniform vec3 u_material_diffuse;" \
      "uniform vec3 u_material_specular;" \
      "uniform float u_material_shiness;" \
      "out vec4 FragColor;" \
      "vec3 phong_ads_light;" \
      "void main(void)" \
      "{" \
          "if(1 == u_key_L_pressed)"
          "{" \
              "vec3 light_direction_normalize = normalize(light_direction);" \
              "vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
              "vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \
              "vec3 viewer_vector_normal = normalize(viewer_vector);" \
              "float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \
              "vec3 ambient = u_light_ambient * u_material_ambient;" \
              "vec3 diffuse = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction;" \
              "vec3 specular = u_light_specular * u_material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), u_material_shiness);" \
              "phong_ads_light= ambient + diffuse + specular;" \
          "}" \
          "else" \
          "{" \
              "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
          "}" \
          "FragColor = vec4(phong_ads_light, 1.0);" \
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);
  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (FALSE == iShaderCompileStatus)
  {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -9;
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  // Attributes Binding Before Linking
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                       "vPosition");

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus)
  {
    fprintf(
        gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
        __FILE__, __LINE__, __FUNCTION__);

    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                            szInfoLog);

        fprintf(gpFile,
                "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                "Failed:\n\t%s\n",
                __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -10;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  // Uniform Binding After Linking
  modelMatrixUniform =
      glGetUniformLocation(giShaderProgramObject, "u_model_matrix");
  viewMatrixUniform =
      glGetUniformLocation(giShaderProgramObject, "u_view_matrix");
  projectionMatrixUniform =
      glGetUniformLocation(giShaderProgramObject, "u_projection_matrix");

  lightAmbientUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_ambient");
  lightDiffuseUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_diffuse");
  lightSpecularUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_specular");
  lightPositionUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_position");

  materialAmbientUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_ambient");
  materialDiffuseUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_diffuse");
  materialSpecularUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_specular");
  materialShinessUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_shiness");

  keyLPressedUniform =
      glGetUniformLocation(giShaderProgramObject, "u_key_L_pressed");

  // cube
  vao_tree = (GLuint *)malloc(sizeof(GLuint) * gObjectCount);
  glGenVertexArrays(gObjectCount, vao_tree);

  vbo_tree_vertex = (GLuint *)malloc(sizeof(GLuint) * gObjectCount);
  glGenVertexArrays(gObjectCount, vbo_tree_vertex);

  vbo_tree_normal = (GLuint *)malloc(sizeof(GLuint) * gObjectCount);
  glGenVertexArrays(gObjectCount, vbo_tree_normal);

  for (int i = 0; i < gObjectCount; i++)
  {
      glBindVertexArray(vao_tree[i]);

      glGenBuffers(1, &vbo_tree_vertex[i]);
      glBindBuffer(GL_ARRAY_BUFFER, vbo_tree_vertex[i]);

      glBufferData(
          GL_ARRAY_BUFFER,
          sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * VERTEX_IN_3D_PLANE,
          pTreeDataArray[i].pTreeVertices,
          GL_STATIC_DRAW);

      glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
          NULL);

      glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

      glBindBuffer(GL_ARRAY_BUFFER, 0);

      glGenBuffers(1, &vbo_tree_normal[i]);
      glBindBuffer(GL_ARRAY_BUFFER, vbo_tree_normal[i]);

      glBufferData(
          GL_ARRAY_BUFFER,
          sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * NORMAL_IN_3D_PLANE,
          pTreeDataArray[i].pTreeNormals,
          GL_STATIC_DRAW);

      glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
      glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

      glBindBuffer(GL_ARRAY_BUFFER, 0);

      glBindVertexArray(0);
  }

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  modelMatrix = mat4::identity();
  viewMatrix = mat4::identity();
  perspectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
          __FILE__, __LINE__, __FUNCTION__);
  return 0;
}

int ModelLoading(void)
{
	typedef struct _MODEL
	{
		int object_count;
		//
		// Complete Vertices Data (Not Properly arrange)  -> To Get Proper Faces/Arranged Data Of Each Object Refer Per Object Arrays And Use Elements/Indices To Get Proper Value of Vertices Data In Below Mentioned Arrays
		// 1. Vertex Position
		// 2. Normals
		// 3. TexCoords
		//
		int v_count; // count without vec3 touple
		float *pVerticesArray;

		int vn_count;
		float *pNormalsArray;

		int vt_count;
		float *pTexcoordsArray;

	} MODEL;

	MODEL model;

	typedef struct _OBJECT
	{
		// Face/Indices To Used In  Model Arrays
		int element_count; // count without vec3 touple
		int *pElementVertices;
		int *pElementTexcoord;
		int *pElementNormal;

	} OBJECT;
	OBJECT *pObjectArray = NULL;

	char buf[LINE_LENGTH];

	if (0 != fopen_s(&gpFileObj,"tree.obj", "r"))
	{
		return 1;
	}

	memset(&model, 0, sizeof(MODEL));

	unsigned int line_index = 0;
	unsigned int object_count = 0;
	memset(buf, 0, LINE_LENGTH);
	while (NULL != fgets(buf, LINE_LENGTH, gpFileObj))
	{
		char *context = NULL;
		char *token = strtok_s(buf, " ", &context);

		if (0 == strcmp(token, "o"))
		{
			if (NULL == pObjectArray)
			{
				pObjectArray = (OBJECT *)malloc(sizeof(OBJECT));
				memset(pObjectArray, 0, sizeof(OBJECT));
				model.object_count = 1;
			}
			else
			{
				model.object_count += 1;
				pObjectArray = (OBJECT *)realloc(pObjectArray, model.object_count * sizeof(OBJECT));
				memset(pObjectArray + model.object_count - 1, 0, sizeof(OBJECT));
			}
		}
		else if (0 == strcmp(token, "v"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pVerticesArray)
			{
				model.v_count = 3;
				model.pVerticesArray = (float *)malloc(model.v_count * sizeof(float));
				memset(model.pVerticesArray, 0, sizeof(model.v_count * sizeof(float)));
			}
			else
			{
				model.v_count += 3;
				model.pVerticesArray = (float *)realloc(model.pVerticesArray, model.v_count * sizeof(float));
			}

			model.pVerticesArray[model.v_count - 3] = x;
			model.pVerticesArray[model.v_count - 2] = y;
			model.pVerticesArray[model.v_count - 1] = z;

		}
		else if (0 == strcmp(token, "vn"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pNormalsArray)
			{
				model.vn_count = 3;
				model.pNormalsArray = (float *)malloc(model.vn_count * sizeof(float));
				memset(model.pNormalsArray, 0, sizeof(model.vn_count * sizeof(float)));
			}
			else
			{
				model.vn_count += 3;
				model.pNormalsArray = (float *)realloc(model.pNormalsArray, model.vn_count * sizeof(float));
			}

			model.pNormalsArray[model.vn_count - 3] = x;
			model.pNormalsArray[model.vn_count - 2] = y;
			model.pNormalsArray[model.vn_count - 1] = z;

		}
		else if (0 == strcmp(token, "vt"))
		{
			float x, y;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pTexcoordsArray)
			{
				model.vt_count = 2;
				model.pTexcoordsArray = (float *)malloc(model.vt_count * sizeof(float));
				memset(model.pTexcoordsArray, 0, sizeof(model.vt_count * sizeof(float)));
			}
			else
			{
				model.vt_count += 2;
				model.pTexcoordsArray = (float *)realloc(model.pTexcoordsArray, model.vt_count * sizeof(float));
			}

			model.pTexcoordsArray[model.vt_count - 2] = x;
			model.pTexcoordsArray[model.vt_count - 1] = y;
		}

		else if (0 == strcmp(token, "f"))
		{
			char *first_token = strtok_s(NULL, " ", &context);
			char *second_token = strtok_s(NULL, " ", &context);
			char *third_token = strtok_s(NULL, " ", &context);

			char *first_context = NULL;
			int v1 = (int)atoi(strtok_s(first_token, "/", &first_context));
			int vt1 = (int)atoi(strtok_s(NULL, "/", &first_context));
			int vn1 = (int)atoi(strtok_s(NULL, "/", &first_context));

			char *second_context = NULL;
			int v2 = (int)atoi(strtok_s(second_token, "/", &second_context));
			int vt2 = (int)atoi(strtok_s(NULL, "/", &second_context));
			int vn2 = (int)atoi(strtok_s(NULL, "/", &second_context));

			char *third_context = NULL;
			int v3 = (int)atoi(strtok_s(third_token, "/", &third_context));
			int vt3 = (int)atoi(strtok_s(NULL, "/", &third_context));
			int vn3 = (int)atoi(strtok_s(NULL, "/", &third_context));

			pObjectArray[model.object_count - 1].element_count += 3;
			if (NULL == pObjectArray[model.object_count - 1].pElementVertices)
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementVertices, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)realloc(pObjectArray[model.object_count - 1].pElementVertices, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 3] = v1;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 2] = v2;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 1] = v3;

			if (NULL == pObjectArray[model.object_count - 1].pElementNormal)
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementNormal, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)realloc(pObjectArray[model.object_count - 1].pElementNormal, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 3] = vn1;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 2] = vn2;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 1] = vn3;

			if (NULL == pObjectArray[model.object_count - 1].pElementTexcoord)
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementTexcoord, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)realloc(pObjectArray[model.object_count - 1].pElementTexcoord, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 3] = vt1;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 2] = vt2;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 1] = vt3;
		}

		memset(buf, 0, LINE_LENGTH);
	}
    gObjectCount = model.object_count;
	pTreeDataArray = (TREE_DATA *)malloc(sizeof(TREE_DATA) * model.object_count);
	memset(pTreeDataArray, 0, sizeof(TREE_DATA) * model.object_count);

	for (int i = 0; i < model.object_count; i++)
	{
		pTreeDataArray[i].data_count = pObjectArray[i].element_count / FACE_TUPLE;
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : pTreeDataArray[%d].data_count : %d\n",
            __FILE__, __LINE__, __FUNCTION__, i, pTreeDataArray[i].data_count);

		pTreeDataArray[i].pTreeVertices = (float *)malloc(sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * VERTEX_IN_3D_PLANE);
		memset(pTreeDataArray[i].pTreeVertices, 0, sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * VERTEX_IN_3D_PLANE);

		pTreeDataArray[i].pTreeTexcoords = (float *)malloc(sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * TEXCOORD);
		memset(pTreeDataArray[i].pTreeTexcoords, 0, sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * TEXCOORD);

		pTreeDataArray[i].pTreeNormals = (float *)malloc(sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * NORMAL_IN_3D_PLANE);
		memset(pTreeDataArray[i].pTreeNormals, 0, sizeof(float) * pTreeDataArray[i].data_count * FACE_TUPLE * NORMAL_IN_3D_PLANE);

		int vi = 0;
		int vt = 0;
		int vn = 0;
		for (int fn = 0; fn < pObjectArray[i].element_count / FACE_TUPLE; fn++)
		{
			int v1 = pObjectArray[i].pElementVertices[fn * FACE_TUPLE + 0] - 1; // Index is array start with Zero Hence "-1" From The Element Index Which Is Start From 1
			int v2 = pObjectArray[i].pElementVertices[fn * FACE_TUPLE + 1] - 1;
			int v3 = pObjectArray[i].pElementVertices[fn * FACE_TUPLE + 2] - 1;

			pTreeDataArray[i].pTreeVertices[vi + 0] = model.pVerticesArray[v1 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeVertices[vi + 1] = model.pVerticesArray[v1 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeVertices[vi + 2] = model.pVerticesArray[v1 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeVertices[vi + 3] = model.pVerticesArray[v2 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeVertices[vi + 4] = model.pVerticesArray[v2 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeVertices[vi + 5] = model.pVerticesArray[v2 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeVertices[vi + 6] = model.pVerticesArray[v3 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeVertices[vi + 7] = model.pVerticesArray[v3 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeVertices[vi + 8] = model.pVerticesArray[v3 * VERTEX_IN_3D_PLANE + 2];

			int t1 = pObjectArray[i].pElementTexcoord[fn * FACE_TUPLE + 0] - 1;
			int t2 = pObjectArray[i].pElementTexcoord[fn * FACE_TUPLE + 1] - 1;
			int t3 = pObjectArray[i].pElementTexcoord[fn * FACE_TUPLE + 2] - 1;

			pTreeDataArray[i].pTreeTexcoords[vt + 0] = model.pTexcoordsArray[t1 * TEXCOORD + 0];
			pTreeDataArray[i].pTreeTexcoords[vt + 1] = model.pTexcoordsArray[t1 * TEXCOORD + 1];
			pTreeDataArray[i].pTreeTexcoords[vt + 2] = model.pTexcoordsArray[t2 * TEXCOORD + 0];
			pTreeDataArray[i].pTreeTexcoords[vt + 3] = model.pTexcoordsArray[t2 * TEXCOORD + 1];
			pTreeDataArray[i].pTreeTexcoords[vt + 4] = model.pTexcoordsArray[t3 * TEXCOORD + 0];
			pTreeDataArray[i].pTreeTexcoords[vt + 5] = model.pTexcoordsArray[t3 * TEXCOORD + 1];

			int n1 = pObjectArray[i].pElementNormal[fn * FACE_TUPLE + 0] - 1;
			int n2 = pObjectArray[i].pElementNormal[fn * FACE_TUPLE + 1] - 1;
			int n3 = pObjectArray[i].pElementNormal[fn * FACE_TUPLE + 2] - 1;

			pTreeDataArray[i].pTreeNormals[vn + 0] = model.pNormalsArray[n1 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeNormals[vn + 1] = model.pNormalsArray[n1 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeNormals[vn + 2] = model.pNormalsArray[n1 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeNormals[vn + 3] = model.pNormalsArray[n2 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeNormals[vn + 4] = model.pNormalsArray[n2 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeNormals[vn + 5] = model.pNormalsArray[n2 * VERTEX_IN_3D_PLANE + 2];
			pTreeDataArray[i].pTreeNormals[vn + 6] = model.pNormalsArray[n3 * VERTEX_IN_3D_PLANE + 0];
			pTreeDataArray[i].pTreeNormals[vn + 7] = model.pNormalsArray[n3 * VERTEX_IN_3D_PLANE + 1];
			pTreeDataArray[i].pTreeNormals[vn + 8] = model.pNormalsArray[n3 * VERTEX_IN_3D_PLANE + 2];

			fprintf(gpFile, "[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeVertices[vi + 0], pTreeDataArray[i].pTreeVertices[vi + 1], pTreeDataArray[i].pTreeVertices[vi + 2]);
			fprintf(gpFile, "[%f, %f]\n" ,pTreeDataArray[i].pTreeTexcoords[vt + 0], pTreeDataArray[i].pTreeTexcoords[vt + 1]);
			fprintf(gpFile, "[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeNormals[vn + 0], pTreeDataArray[i].pTreeNormals[vn + 1], pTreeDataArray[i].pTreeNormals[vn + 2]);
			fprintf(gpFile, "\n");
			fprintf(gpFile, "[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeVertices[vi + 3], pTreeDataArray[i].pTreeVertices[vi + 4], pTreeDataArray[i].pTreeVertices[vi + 5]);
			fprintf(gpFile, "[%f, %f]\n" ,pTreeDataArray[i].pTreeTexcoords[vt + 2], pTreeDataArray[i].pTreeTexcoords[vt + 3]);
			fprintf(gpFile, "[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeNormals[vn + 3], pTreeDataArray[i].pTreeNormals[vn + 4], pTreeDataArray[i].pTreeNormals[vn + 5]);
			fprintf(gpFile, "\n");
			fprintf(gpFile, "[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeVertices[vi + 6], pTreeDataArray[i].pTreeVertices[vi + 7], pTreeDataArray[i].pTreeVertices[vi + 8]);
			fprintf(gpFile, "[%f, %f]\n" ,pTreeDataArray[i].pTreeTexcoords[vt + 4], pTreeDataArray[i].pTreeTexcoords[vt + 5]);
			fprintf(gpFile, "[%f, %f, %f]\n" ,pTreeDataArray[i].pTreeNormals[vn + 6], pTreeDataArray[i].pTreeNormals[vn + 7], pTreeDataArray[i].pTreeNormals[vn + 8]);
			fprintf(gpFile, "\n");

			vi += 9;
			vt += 6;
			vn += 9;
		}
	}

	return 0;
}
