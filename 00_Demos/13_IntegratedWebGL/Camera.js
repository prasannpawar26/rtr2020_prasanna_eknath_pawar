
const CameraMovementMacros =
{
	CAMERA_FORWARD:1,
	CAMERA_BACKWARD:2,
	CAMERA_LEFT:3,
	CAMERA_RIGHT:4
};

// Default camera values
const CAMERA_YAW			= -90.0;
const CAMERA_PITCH		= 0.0;
const CAMERA_SPEED		= 2.5;
const CAMERA_SENSITIVITY	= 0.1;
const CAMERA_ZOOM			= 45.0;

// An abstract camera class that processes input and calculates the corresponding Euler Angles, Vectors and Matrices for use in OpenGL

function Camera()
{
	// camera Attributes
	var Camera_Position;
	var Camera_Front;
	var Camera_Up;
	var Camera_Right;
	var Camera_WorldUp;
	var Camera_Matrix;

	// euler Angles
	var Camera_Yaw;
	var Camera_Pitch;

	// camera options
	var Camera_MovementSpeed;
	var Camera_MouseSensitivity;
	var Camera_Zoom;
	
	function Radians(angleInDegrees)
	{
		return (angleInDegrees * Math.PI/180.0);
	}

	function Cross(a, b)
	{
		var result = vec3.create();
		vec3.cross(result, a, b);
		return result;
	}

	function NormalizeOfVector(v)
	{
	  var length = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	  // make sure we don't divide by 0.
	  if (length > 0.00001) {
		return [v[0] / length, v[1] / length, v[2] / length];
	  } else {
		return [0, 0, 0];
	  }
	}

	//
	// Initalize Camera
	//
	this.Camera_Init=function(pos, front, up)
	{
		Camera_Position	= pos;
		Camera_WorldUp		= up;
		Camera_Front = front;
		
		Camera_Yaw			= CAMERA_YAW;
		Camera_Pitch		= CAMERA_PITCH;
		Camera_MovementSpeed		= CAMERA_SPEED;
		Camera_MouseSensitivity	= CAMERA_SENSITIVITY;
		Camera_Zoom				= CAMERA_ZOOM;

		Camera_Matrix = mat4.create();
	
		this.Camera_UpdateVectors();
	}
	
	//
	// calculates the front vector from the Camera's (updated) Euler Angles
	//
	this.Camera_UpdateVectors=function()
	{
		// calculate the new Camera_Front vector
		var front = new Float32Array(3);
		front[0] = Math.cos(Radians(Camera_Yaw)) * Math.cos(Radians(Camera_Pitch));
		front[1] = Math.sin(Radians(Camera_Pitch));
		front[2] = Math.sin(Radians(Camera_Yaw)) * Math.cos(Radians(Camera_Pitch));
		Camera_Front	= NormalizeOfVector(front);

		// also re-calculate the Camera_Right and Camera_Up vector
		Camera_Right	= NormalizeOfVector(Cross(Camera_Front, Camera_WorldUp));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		Camera_Up		= NormalizeOfVector(Cross(Camera_Right, Camera_Front));
	}

	this.Camera_GetViewMatrix=function()
	{
		mat4.identity(Camera_Matrix);
		
		var currentFront = vec3.create();
		vec3.add(currentFront, Camera_Position, Camera_Front);
		
		mat4.lookAt(Camera_Matrix, Camera_Position, currentFront, Camera_Up);

		return Camera_Matrix;
	}
	
	this.Camera_SetPosition=function(position)
	{
		Camera_Position = position;
	}
	
	this.Camera_GetPosition=function()
	{
		return Camera_Position;
	}
	
	this.Camera_SetFront=function(front)
	{
		Camera_Front = front;
	}
	
	this.Camera_GetFront=function()
	{
		return Camera_Front;
	}
	
	this.Camera_SetUp=function(up)
	{
		Camera_Up = up;
	}
	
	this.Camera_GetUp=function()
	{
		return Camera_Up;
	}
	
	this.Camera_SetYaw=function(yaw)
	{
		Camera_Yaw = yaw;
	}
	
	this.Camera_GetYaw=function()
	{
		return Camera_Yaw;
	}
	
	this.Camera_SetPitch=function(pitch)
	{
		Camera_Pitch = pitch;
		
		if (Camera_Pitch > 89.0)
		{
			Camera_Pitch = 89.0;
		}
		
		if (Camera_Pitch < -89.0)
		{
			Camera_Pitch = -89.0;
		}
	}
	
	this.Camera_GetPitch=function()
	{
		return Camera_Pitch;
	}
	
	this.Camera_ProcessKeyBoard=function(direction, deltaTime)
	{
		var velocity = Camera_MovementSpeed * deltaTime;

		if (direction == CameraMovementMacros.CAMERA_FORWARD)
		{
			for (var i=0; i<Camera_Front.length; i++)
			{
				Camera_Position[i] += Camera_Front[i] * velocity;
			}
			//Camera_Position += ResultantFront * velocity;
			// // console.log("Camera Position : " + Camera_Position, "Camera_Front: ", Camera_Front);
		}
		else if (direction == CameraMovementMacros.CAMERA_BACKWARD)
		{
			for (var i=0; i<Camera_Front.length; i++)
			{
				Camera_Position[i] -= Camera_Front[i] * velocity;
			}
			//Camera_Position -= ResultantFront * velocity;
		}
		if (direction == CameraMovementMacros.CAMERA_LEFT)
		{
			for (var i=0; i < Camera_Right.length; i++)
			{
				Camera_Position[i] -= Camera_Right[i] * velocity;
			}
			// // console.log("Camera Position : " + Camera_Position, "Camera_Right: ", Camera_Right);
			//Camera_Position -= Camera_Right * velocity;
		}
		else if (direction == CameraMovementMacros.CAMERA_RIGHT)
		{
			for (var i = 0; i < Camera_Right.length; i++)
			{
				Camera_Position[i] += Camera_Right[i] * velocity;
			}
			// // console.log("Camera Position : " + Camera_Position, "Camera_Right: ", Camera_Right);
			//Camera_Position += Camera_Right * velocity;
		}
	}
}
