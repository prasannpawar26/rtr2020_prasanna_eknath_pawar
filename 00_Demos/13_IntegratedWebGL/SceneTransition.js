function SceneTransition()
{
	var sceneTransition_vs;
	var sceneTransition_fs;
	var sceneTransition_program;

	var sceneTransition_vao;
	var sceneTransition_vbo;
	var sceneTransition_mvpUniform;
	var sceneTransition_alphaUniform;

	var sceneTransition_alpha = 1.0; // blended  // Black To Scene i.e 1.0 to 0.0
	
	this.Init=function()
	{
		// vertex shader
		var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +

		"uniform mat4 u_mvp_matrix;" +

		"void main(void)" +
		"{" +
			"gl_Position = u_mvp_matrix * vPosition;" +
		"}";
		
		sceneTransition_vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(sceneTransition_vs, vertexShaderSourceCode);
		gl.compileShader(sceneTransition_vs);
		if(gl.getShaderParameter(sceneTransition_vs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(sceneTransition_vs);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// // console.log("RTR : sceneTransition_vs Compiled Successfully");
		
		// fragment shader
		var fragmentShaderSourceCode = 
		"#version 300 es" +
		"\n" +
		"precision highp float;"+
		
		"in vec4 color;" +
		"uniform float u_alpha;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = vec4(0.0, 0.0, 0.0, u_alpha);" +
		"}";
		
		sceneTransition_fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(sceneTransition_fs, fragmentShaderSourceCode);
		gl.compileShader(sceneTransition_fs);
		if(gl.getShaderParameter(sceneTransition_fs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(sceneTransition_fs);
			if(erro.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// // console.log("RTR : sceneTransition_fs Compiled Successfully");
		
		// shader program
		sceneTransition_program = gl.createProgram();
		gl.attachShader(sceneTransition_program, sceneTransition_vs);
		gl.attachShader(sceneTransition_program, sceneTransition_fs);
		
		// pre-link binding of shader program object with vertex shader attributes
		gl.bindAttribLocation(sceneTransition_program, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		
		// linking
		gl.linkProgram(sceneTransition_program);
		if(!gl.getProgramParameter(sceneTransition_program, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(sceneTransition_program);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// // console.log("RTR : sceneTransition_program Linked Successfully");
		
		// get MVP uniform location
		sceneTransition_mvpUniform = gl.getUniformLocation(sceneTransition_program, "u_mvp_matrix");
		sceneTransition_alphaUniform = gl.getUniformLocation(sceneTransition_program, "u_alpha");
		
		// vertices, colors, shader attribs, sceneTransition_vbo, sceneTransition_vao initializations
		var SquareVertices = new Float32Array(
			[1.0, 1.0, 0.0,
			-1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0]
		);
		
		sceneTransition_vao = gl.createVertexArray();
		gl.bindVertexArray(sceneTransition_vao);
		
		sceneTransition_vbo = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, sceneTransition_vbo);
		gl.bufferData(gl.ARRAY_BUFFER, SquareVertices, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindVertexArray(null);
		
		gl.clearColor(1.0, 1.0, 1.0, 1.0);
		gl.clearDepth(1.0);
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);

		perspectiveProjectionMatrix = mat4.create();
	}

	this.Draw=function(fade_in, alpha_value)
	{
		if (fade_in)  // 1.0 To 0.0  => Fade Out
		{
			sceneTransition_alpha += alpha_value;
			if (sceneTransition_alpha > 1.0)
			{
				sceneTransition_alpha = 1.0;
				//// console.log(sceneTransition_alpha);
				pep_Global_FadeOut = false;
				
				if(1 == pep_Global_Scene)
				{
					pep_gTitle_Texture++;
					
					if(5 == pep_gTitle_Texture)
					{
						 pep_Global_Scene++; //

						 // scene 2 cam setup
						cameraPosition[0] = initCameraPosition[0];
						cameraPosition[1] = initCameraPosition[1];
						cameraPosition[2] = initCameraPosition[2];
						pep_camera.Camera_SetPosition(cameraPosition);

						pep_camera.Camera_SetYaw(-42.50);
						pep_camera.Camera_UpdateVectors();
					}
				}
				else if(2 == pep_Global_Scene)
				{
					// // console.log(pep_Global_Scene);
					pep_Global_Scene++;

					pep_camera.Camera_SetYaw(-90.0);
					
					// Position
					cameraPosition[0] = -0.70;
					cameraPosition[1] = 0.0;
					cameraPosition[2] = 0.0;
					pep_camera.Camera_SetPosition(cameraPosition);
					pep_camera.Camera_UpdateVectors();
									    
				}
				else if(3 == pep_Global_Scene)
				{
					
					// // console.log(pep_Global_Scene);
					 pep_Global_Scene++; //
				}
				else if(4 == pep_Global_Scene)
				{
					// // console.log(pep_Global_Scene);
					 pep_Global_Scene++; //
				}
				else if(5 == pep_Global_Scene)
				{
					pep_gCredits_Texture++;
					if(17 == pep_gCredits_Texture)
					{
						 pep_Global_Scene++; //
					}
				}
			}
		}
		else // 0.0 To 1.0  => Fade In
		{
			sceneTransition_alpha += alpha_value;
			if (0.0 > sceneTransition_alpha)
			{
				sceneTransition_alpha = 0.0;
				//// console.log(sceneTransition_alpha);
				pep_Global_FadeOut = true;
			}
		}
		
		gl.enable(gl.BLEND);
		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		
		gl.useProgram(sceneTransition_program);
		
		var modelViewMatrix = mat4.create();
		var modelViewProjectionMatrix = mat4.create();
		
		mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -0.15]);
		mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
		
		gl.uniformMatrix4fv(sceneTransition_mvpUniform, false, modelViewProjectionMatrix);
		gl.uniform1f(sceneTransition_alphaUniform, sceneTransition_alpha);
		
		gl.bindVertexArray(sceneTransition_vao);
		
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		
		gl.bindVertexArray(null);
		
		gl.useProgram(null);
		
		gl.disable(gl.BLEND);
	}

	this.Uninit=function()
	{
		// code
		if(sceneTransition_vao)
		{
			gl.deleteVertexArray(sceneTransition_vao);
			sceneTransition_vao = null;
		}

		if(sceneTransition_vbo)
		{
			gl.deleteBuffer(sceneTransition_vbo);
			sceneTransition_vbo = null;
		}
		
		if(sceneTransition_program)
		{
			if(sceneTransition_fs)
			{
				gl.detachShader(sceneTransition_program, sceneTransition_fs);
				gl.deleteShader(sceneTransition_fs);
				sceneTransition_fs = null;
			}
			
			if(sceneTransition_vs)
			{
				gl.detachShader(sceneTransition_program, sceneTransition_vs);
				gl.deleteShader(sceneTransition_vs);
				sceneTransition_vs = null;
			}
			
			gl.deleteProgram(sceneTransition_program);
			sceneTransition_program = null;
		}
	}
}
