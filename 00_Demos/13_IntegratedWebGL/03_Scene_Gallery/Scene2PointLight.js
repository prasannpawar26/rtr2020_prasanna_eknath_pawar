// Class for point light
function Scene2PointLight()
{
  var tsVao;
  var tsVboPosition;
  var tsVboColor;
  var tsVboNormal;
  var tsVboTexCoord;
  
  var tsVaoCube;
  var tsVboPositionCube;
  var tsVboColorCube;
  var tsVboNormalCube;
  
  var tsVertexSO;		// Vertex Shader Object
  var tsFragmentSO;	// Fragment Shader Object
  var tsShaderProgramObject;	// Shader Program Object
  
  var tsModelMatrixUniform;
  var tsViewMatrixUniform;
  var tsProjectionMatrixUniform;
  var tsKeyPressUniform;
  
  var tsLaUniform = new Array(4);
  var tsKaUniform;
  
  var tsLdUniform = new Array(4);
  var tsKdUniform;
  
  var tsLsUniform = new Array(4);
  var tsKsUniform;
  var tsShinyNessUniform;
  
  var tsLightPositionUniform = new Array(4);
  var tsTextureSamplerUniform;
  
  var tsXDir = 0.0;
  var tsYDir = 0.0;
  var tsZDir = -5.0;
  
  var tsXLightDir = -1.0;
  var tsYLightDir = 0.0;
  var tsZLightDir = -3.0;
  
  var tsPerspectiveProjectionMatrix; // Projection Matrix
  
  var tsLight = true;
  var tsCamera_Position;
  var tsStopUpdating = false;
  var tsCountDown = 1000;

  var tsWallTexture;
  var tsTexture;
  var tsFramesTexture = new Array(14);
  
  class tsCLight
  {
  	constructor()
  	{
  	  this.tsAmbient  = new Float32Array(4);
  	  this.tsDiffuse  = new Float32Array(4);
  	  this.tsSpecular = new Float32Array(4);
  	  this.tsPosition = new Float32Array(4);
  	}
  }
  
  var tsCLightObj = new tsCLight(4);
  
  this.tsScene2PointLightKeyDown=function(tsEvent)
  {
  	switch(tsEvent.keyCode)
  	{
  		case 76:
  		case 108:
  			tsLight = (tsLight) ? false : true;
  			break;
  		case 65:	// A
  			tsXLightDir = tsXLightDir + 1.0;
  	  		// console.log("After pressing A value of Lights X: "+tsXLightDir+"\n");
  			break;
  		case 66:	// B
  			tsYLightDir = tsYLightDir + 1.0;
  	  		// console.log("After pressing B value of Lights Y: "+tsYLightDir+"\n");
  			break;
  		case 67:	// C
  			tsZLightDir = tsZLightDir + 1.0;
  	  		// console.log("After pressing C value of Lights Z: "+tsZLightDir+"\n");
  			break;
  		case 68:	// D
  			tsYLightDir = tsYLightDir - 1.0;
  	  		// console.log("After pressing D value of Lights Y: "+tsYLightDir+"\n");
  			break;
  		case 69:	// E
  			tsXLightDir = tsXLightDir - 1.0;
  	  		// console.log("After pressing E value of Lights X: "+tsXLightDir+"\n");
  			break;
  		case 71:	// G
  			tsZLightDir = tsZLightDir - 1.0;
  	  		// console.log("After pressing G value of Lights Z: "+tsZLightDir+"\n");
  			break;
  	}
  }
  
  // Initialize
  this.Init=function()
  {
  	// Code
    	// Step 3 create drawing context from Canvas
    	gl = canvas.getContext("webgl2");	// WebGL version 2 compliant context
  
    	if(!gl)
    	{
    	        // console.log("Obtaining WebGL2 Context failed...\n");
    	}
    	else
    	{
    	        // console.log("Obtaining WebGL2 Context sucess!!!\n");
    	}
  	// Set WebGL specific variables
  	gl.viewWidth  = canvas.width;
  	gl.viewHeight = canvas.height;
  
  	// Create Shaders
  	tsScene2PointLightPerFragmentShader();
  
  	// Create Shader Program
  	tsScene2PointLightShaderProgramLinking();
  
  	// Vertices, colors, shader attirbs, vao, vbo initialization
	/*var tsWallVertices = new Float32Array([
  		 0.0,  2.0, -6.0,
  		 0.0,  2.0,  6.0,		
  		 0.0, -2.0,  6.0,		
  		 0.0, -2.0, -6.0
  	]);
  
  	var tsWallColor = new Float32Array([
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0
  	]);
  	
  	var tsWallTexCoord = new Float32Array([
  		0.0, 0.0,
  		1.0, 0.0,
  		1.0, 1.0,
  		0.0, 1.0
  	]);	
  
  	var tsWallNormal = new Float32Array([
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0
          ]);
  
  	// Start recording for Cube
  	tsWallVao = gl.createVertexArray();
  	gl.bindVertexArray(tsWallVao);
  
  	  // Position
  	  tsWallVboPosition = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsWallVboPosition);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsWallVertices, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
            // Color 
  	  tsWallVboColor = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboWallColor);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsWallColor, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	  // Texture
  	  tsWallVboTexCoord = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsWallVboTexCoord);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsWallTexCoord, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	  // Normal 
  	  tsWallVboNormal = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsWallVboNormal);
  	  gl.bufferData(gl.ARRAY_BUFFER, tsWallNormal, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	gl.bindVertexArray(null);
	*/

  	// Square vertices and color
  	var tsVertices = new Float32Array([
  		 0.0,  2.6, -6.6,
  		 0.0,  2.6,  6.6,		
  		 0.0, -2.6,  6.6,		
  		 0.0, -2.6, -6.6
  	]);
  
  	var tsColor = new Float32Array([
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0
  	]);
  	
  	var tsTexCoord = new Float32Array([
  		0.0, 0.0,
  		1.0, 0.0,
  		1.0, 1.0,
  		0.0, 1.0
  	]);	
  
  	var tsNormal = new Float32Array([
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0
          ]);
  
  	// Start recording for Cube
  	tsVao = gl.createVertexArray();
  	gl.bindVertexArray(tsVao);
  
  	  // Position
  	  tsVboPosition = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboPosition);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsVertices, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
            // Color 
  	  tsVboColor = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboColor);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsColor, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	  // Texture
  	  tsVboTexCoord = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboTexCoord);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsTexCoord, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	  // Normal 
  	  tsVboNormal = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboNormal);
  	  gl.bufferData(gl.ARRAY_BUFFER, tsNormal, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	gl.bindVertexArray(null);
  
  	var tsVerticesCube = new Float32Array([
  		 1.0,  1.0, 1.0,
  		-1.0,  1.0, 1.0,		
  		-1.0, -1.0, 1.0,		
  		 1.0, -1.0, 1.0,
  
      		 1.0,  1.0, -1.0,
      		 1.0,  1.0,  1.0,
      		 1.0, -1.0,  1.0,
      		 1.0, -1.0, -1.0,
  
  		-1.0,  1.0, -1.0,
          	 1.0,  1.0, -1.0,
          	 1.0, -1.0, -1.0,
          	-1.0, -1.0, -1.0,
  
  		-1.0,  1.0,  1.0,
  		-1.0,  1.0, -1.0,
  		-1.0, -1.0, -1.0,
  		-1.0, -1.0,  1.0,
  
  		 1.0, 1.0, -1.0,
                  -1.0, 1.0, -1.0,
                  -1.0, 1.0,  1.0,
                   1.0, 1.0,  1.0,
  
  		  1.0, -1.0, -1.0,
                   -1.0, -1.0, -1.0,
                   -1.0, -1.0,  1.0,
                    1.0, -1.0,  1.0
  	]);
  
  	var tsColorCube = new Float32Array([
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0,
  		1.0, 1.0, 1.0
  	]);
  
  	var tsNormalCube = new Float32Array([
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  		0.0, 0.0, 1.0,
  
  		1.0, 0.0, 0.0,
  		1.0, 0.0, 0.0,
  		1.0, 0.0, 0.0,
  		1.0, 0.0, 0.0,
  
  		0.0, 0.0, -1.0,
  		0.0, 0.0, -1.0,
  		0.0, 0.0, -1.0,
  		0.0, 0.0, -1.0,
  
  		-1.0, 0.0, 0.0,
  		-1.0, 0.0, 0.0,
  		-1.0, 0.0, 0.0,
  		-1.0, 0.0, 0.0,
  
  		0.0, 1.0, 0.0,
  		0.0, 1.0, 0.0,
  		0.0, 1.0, 0.0,
  		0.0, 1.0, 0.0,
  
  		0.0, -1.0, 0.0,
  		0.0, -1.0, 0.0,
  		0.0, -1.0, 0.0,
  		0.0, -1.0, 0.0
  	]);
  
  	// Start recording
  	tsVaoCube = gl.createVertexArray();
  	gl.bindVertexArray(tsVaoCube);
  
  	  // Position
  	  tsVboPositionCube = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboPositionCube);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsVerticesCube, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
            // Color
  	  tsVboColorCube = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboColorCube);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsColorCube, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	  // Normal 
  	  tsVboNormalCube = gl.createBuffer();
  	  gl.bindBuffer(gl.ARRAY_BUFFER, tsVboNormalCube);
  	    gl.bufferData(gl.ARRAY_BUFFER, tsNormalCube, gl.STATIC_DRAW);
  	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
  	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
  	  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
  	gl.bindVertexArray(null);
  
  	// light
  	tsCLightObj[0] = new tsCLight();
  	tsCLightObj[0].tsAmbient  = [1.0, 1.0, 0.0];
  	tsCLightObj[0].tsDiffuse  = [1.0, 1.0, 0.0];
  	tsCLightObj[0].tsSpecular = [1.0, 1.0, 0.0];
  
	tsWallTexture = gl.createTexture();
	tsWallTexture.image = new Image();
	tsWallTexture.image.src = "03_Scene_Gallery/wall.png";
	tsWallTexture.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsWallTexture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsWallTexture.image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[0] = gl.createTexture();
	tsFramesTexture[0].image = new Image();
	tsFramesTexture[0].image.src = "03_Scene_Gallery/1.png";
	tsFramesTexture[0].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[0]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[0].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[1] = gl.createTexture();
	tsFramesTexture[1].image = new Image();
	tsFramesTexture[1].image.src = "03_Scene_Gallery/2.png";
	tsFramesTexture[1].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[1]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[1].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[2] = gl.createTexture();
	tsFramesTexture[2].image = new Image();
	tsFramesTexture[2].image.src = "03_Scene_Gallery/3.png";
	tsFramesTexture[2].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[2]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[2].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[3] = gl.createTexture();
	tsFramesTexture[3].image = new Image();
	tsFramesTexture[3].image.src = "03_Scene_Gallery/4.png";
	tsFramesTexture[3].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[3]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[3].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[5] = gl.createTexture();
	tsFramesTexture[5].image = new Image();
	tsFramesTexture[5].image.src = "03_Scene_Gallery/5.png";
	tsFramesTexture[5].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[5]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[5].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[6] = gl.createTexture();
	tsFramesTexture[6].image = new Image();
	tsFramesTexture[6].image.src = "03_Scene_Gallery/6.png";
	tsFramesTexture[6].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[6]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[6].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[7] = gl.createTexture();
	tsFramesTexture[7].image = new Image();
	tsFramesTexture[7].image.src = "03_Scene_Gallery/7.png";
	tsFramesTexture[7].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[7]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[7].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[8] = gl.createTexture();
	tsFramesTexture[8].image = new Image();
	tsFramesTexture[8].image.src = "03_Scene_Gallery/8.png";
	tsFramesTexture[8].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[8]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[8].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[9] = gl.createTexture();
	tsFramesTexture[9].image = new Image();
	tsFramesTexture[9].image.src = "03_Scene_Gallery/9.png";
	tsFramesTexture[9].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[9]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[9].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[10] = gl.createTexture();
	tsFramesTexture[10].image = new Image();
	tsFramesTexture[10].image.src = "03_Scene_Gallery/10.png";
	tsFramesTexture[10].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[10]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[10].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[11] = gl.createTexture();
	tsFramesTexture[11].image = new Image();
	tsFramesTexture[11].image.src = "03_Scene_Gallery/11.png";
	tsFramesTexture[11].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[11]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[11].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[12] = gl.createTexture();
	tsFramesTexture[12].image = new Image();
	tsFramesTexture[12].image.src = "03_Scene_Gallery/12.png";
	tsFramesTexture[12].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[12]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[12].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

	tsFramesTexture[13] = gl.createTexture();
	tsFramesTexture[13].image = new Image();
	tsFramesTexture[13].image.src = "03_Scene_Gallery/13.png";
	tsFramesTexture[13].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[13]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[13].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	} 

	tsFramesTexture[4] = gl.createTexture();
	tsFramesTexture[4].image = new Image();
	tsFramesTexture[4].image.src = "03_Scene_Gallery/14.png";
	tsFramesTexture[4].image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[4]);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, tsFramesTexture[4].image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
  }
  
  function tsScene2PointLightPerFragmentShader()
  {
  	// Vertex Shader
  	var tsVertexShaderSourceCode = "#version 300 es" +
  		"\n" +
  		"\nprecision highp float;" +
  		"\nprecision highp int;" +
  		"\nin vec4 vTsPosition;" +
  		"\nin vec3 vTsNormal;" +
  		"\nin vec2 vTsTexCoord;" +
  		"\nuniform mat4 u_tsModelMatrix;" +
  		"\nuniform mat4 u_tsViewMatrix;" +
  		"\nuniform mat4 u_tsProjectionMatrix;" +
  		"\nuniform int u_tsKeyPress;" +
  		"\nuniform vec4 u_tsLightPosition[4];" +
  		"\nout vec3 tsTransformNormal;" +
  		"\nout vec3 tsViewVector;" +
  		"\nout vec3 tsLightDirection[4];" +
  		"\nout vec2 outTsTexCoord;" +		
  		"\nvoid main(void)" +
  		"\n{" +
  		"\nif(1 == u_tsKeyPress)" +
  		"\n{" +
  		      "\nvec4 tsEyeCoord = u_tsViewMatrix * u_tsModelMatrix * vTsPosition;" +
  		      "\ntsTransformNormal = mat3(u_tsViewMatrix * u_tsModelMatrix) * vTsNormal;" +
  		      "\ntsViewVector =-tsEyeCoord.xyz;" +
  		      "\nint i;" +
  		     "\nfor(i = 0; i < 4; i++)" +
  		     "\n{" +
  		           "\ntsLightDirection[i] = vec3(u_tsLightPosition[i] - tsEyeCoord);" +
  		     "\n}" +
  		"\n}" +
  		"\ngl_Position = u_tsProjectionMatrix * u_tsViewMatrix * u_tsModelMatrix * vTsPosition;" +
  		"\noutTsTexCoord = vTsTexCoord;" +
  		"\n}";
  
  	tsVertexSO = gl.createShader(gl.VERTEX_SHADER);
  	gl.shaderSource(tsVertexSO, tsVertexShaderSourceCode);
  	gl.compileShader(tsVertexSO);
  	if(false == gl.getShaderParameter(tsVertexSO, gl.COMPILE_STATUS))
  	{
  		var tsError = gl.getShaderInfoLog(tsVertexSO);
  		if(tsError.length > 0)
  		{
  			alert(tsError);
  			tsUninitialize();
  		}
  	}
  
  	// Fragment shader
  	var tsFragmentShaderSourceCode = "#version 300 es" +
  		"\n" +
  		"\nprecision highp float;" +
  		"\nprecision highp int;" +
  		"\nuniform vec3 u_tsLa[4];" +
  		"\nuniform vec3 u_tsKa;" +
  		"\nuniform vec3 u_tsLd[4];" +
  		"\nuniform vec3 u_tsKd;" +
  		"\nuniform vec3 u_tsLs[4];" +
  		"\nuniform vec3 u_tsKs;" +
  		"\nuniform float u_fTsShinyNess;" +
  		"\nuniform int u_tsKeyPress;" +
  		"\nuniform sampler2D u_tsTextureSampler2D;" +
  		"\nin vec3 tsLightDirection[4];" +
  		"\nin vec3 tsTransformNormal;" +
  		"\nin vec3 tsViewVector;" +
  		"\nin vec2 outTsTexCoord;" +		
  		"\nout vec4 tsFragColor;" +
  		"\nvoid main(void)" +
  		"\n{" +
  		"\nvec4 tsColor;" +
  		"\nvec4 tsTexture = texture(u_tsTextureSampler2D, outTsTexCoord);" +
  		"\nif(1 == u_tsKeyPress)" +
  		"\n{" +
  		      "\nvec3 tsPhongAdsLight;" +
  		      "\nvec3 tsNormalizedTransformNormal = normalize(tsTransformNormal);" +
  		      "\nvec3 tsNormalizedViewVector = normalize(tsViewVector);" +
  		     "\nfloat tsAttenuation;" +
  		     "\nfloat tsDistance;" +
  		     "\nint i;" +
  		     "\nfor(i = 0; i < 4; i++)" +
  		     "\n{" +
  		          "\nvec3 tsNormalizedLightDirection = normalize(tsLightDirection[i]);" +
  		          "\nvec3 tsReflectionVector = reflect(-tsNormalizedLightDirection, tsNormalizedTransformNormal);" +
  		          "\ntsDistance = length(tsLightDirection[i]);" +
  		          "\nfloat tsConstantAttenuation = 1.0;" +
  		          "\nfloat tsLinearAttenuation = 0.1;" +
  		          "\nfloat tsQuadraticAttenuation = 0.1;" +
  		          "\ttsAttenuation = 1.0/( 1.0 + (tsConstantAttenuation * tsDistance) + (tsLinearAttenuation * tsDistance * tsDistance) );" +
  		          "\nvec3 tsAmbient = u_tsLa[i] * u_tsKa * tsAttenuation;" +
  		          "\nvec3 tsDiffuse = u_tsLd[i] * u_tsKd * max( dot(tsNormalizedLightDirection, tsNormalizedTransformNormal), 0.0f) * tsAttenuation;" +
  		          "\nvec3 tsSpecular = u_tsLs[i] * u_tsKs * pow( max( dot(tsReflectionVector, tsNormalizedViewVector), 0.0f), u_fTsShinyNess) * tsAttenuation;" +
  		          "\ntsPhongAdsLight = tsPhongAdsLight + tsAmbient + tsDiffuse + tsSpecular;" +
  		     "\n}" +
  		"\ntsColor = vec4(tsPhongAdsLight, 1.0f);" +
  		"\n}" +
  		"\nelse" +
  		"\n{" +
  		"\ntsColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);" +
  		"\n}" +
  		"\ntsFragColor = tsTexture * tsColor;" +
		"\nfloat tsGray = (tsFragColor.r + tsFragColor.g + 0.0) / 3.0;" + 
		"\ntsFragColor = vec4(tsGray, tsGray, tsGray, 1.0);" +
  		"\n}";
  		     //     "\ntsPhongAdsLight = ( (tsAmbient + tsDiffuse) * tsTexture.rgb) + (tsSpecular + tsTexture.rgb);" +
  
  	tsFragmentSO = gl.createShader(gl.FRAGMENT_SHADER);
  	gl.shaderSource(tsFragmentSO, tsFragmentShaderSourceCode);
  	gl.compileShader(tsFragmentSO);
  
  	if(gl.getShaderParameter(tsFragmentSO, gl.COMPILE_STATUS))
  	{
  		var tsError = gl.getShaderInfoLog(tsFragmentSO);
  		if(tsError.length > 0)
  		{
  			alert(tsError);
  			tsUninitialize();
  		}
  	}
  }
  
  function tsScene2PointLightShaderProgramLinking()
  {
  	// Shader Program
  	tsShaderProgramObject = gl.createProgram();
  	gl.attachShader(tsShaderProgramObject, tsVertexSO);
  	gl.attachShader(tsShaderProgramObject, tsFragmentSO);
  
  	// Pre linking attributes
  	tsScene2PointLightPreLinkingAttributes();
  
  	// Linking
  	gl.linkProgram(tsShaderProgramObject);
  	if(false == gl.getProgramParameter(tsShaderProgramObject, gl.LINK_STATUS))
  	{
  		var tsError = gl.getProgramInfoLog(tsShaderProgramObject);
  		if(tsError.length > 0)
  		{
  			alert(tsError);
  			tsUninitialize();
  		}
  	}
  
  	// Post linking attributes
  	tsScene2PointLightPostLinkingAttributes();
  }
  
  function tsScene2PointLightPreLinkingAttributes()
  {
  	// Pre-Link binding of shader program object with vertex shader attributes
  	gl.bindAttribLocation(tsShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vTsPosition");
  	gl.bindAttribLocation(tsShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vTsColor");
  	gl.bindAttribLocation(tsShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vTsNormal");
  	gl.bindAttribLocation(tsShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTsTexCoord");
  }
  
  function tsScene2PointLightPostLinkingAttributes()
  {
  	// Get Model-View-Projection Matrix location
  	tsViewMatrixUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsViewMatrix");
  	tsModelMatrixUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsModelMatrix");
  	tsProjectionMatrixUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsProjectionMatrix");
  
  	tsKeyPressUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsKeyPress");
  
  	tsLaUniform[0] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLa[0]");
  	tsLaUniform[1] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLa[1]");
  	tsLaUniform[2] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLa[2]");
  	tsLaUniform[3] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLa[3]");
  	tsKaUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsKa");
  
  	tsLdUniform[0] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLd[0]");
  	tsLdUniform[1] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLd[1]");
  	tsLdUniform[2] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLd[2]");
  	tsLdUniform[3] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLd[3]");
  	tsKdUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsKd");
  
  	tsLsUniform[0] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLs[0]");
  	tsLsUniform[1] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLs[1]");
  	tsLsUniform[2] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLs[2]");
  	tsLsUniform[3] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLs[3]");
  	tsKsUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsKs");
  	tsShinyNessUniform = gl.getUniformLocation(tsShaderProgramObject, "u_fTsShinyNess");
  
  	tsLightPositionUniform[0] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLightPosition[0]");
  	tsLightPositionUniform[1] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLightPosition[1]");
  	tsLightPositionUniform[2] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLightPosition[2]");
  	tsLightPositionUniform[3] = gl.getUniformLocation(tsShaderProgramObject, "u_tsLightPosition[3]");
  	tsTextureSamplerUniform = gl.getUniformLocation(tsShaderProgramObject, "u_tsTextureSampler2D");
  }
  
  // Draw or Display or Render
  this.Draw=function()
  {
	var tsScaleFactor = 1.0;
  	// Code
  	gl.useProgram(tsShaderProgramObject);
  	
  	  // Add light properties
  	  if (tsLight)
  	  {
  	  	gl.uniform1i(tsKeyPressUniform, 1);				// Enable light in shader
  
  		gl.uniform3fv(tsLaUniform[0], tsCLightObj[0].tsAmbient);		
  		gl.uniform3fv(tsLdUniform[0], tsCLightObj[0].tsDiffuse);		
  		gl.uniform3fv(tsLsUniform[0], tsCLightObj[0].tsSpecular);		
  
		tsCamera_Position = pep_camera.Camera_GetPosition();
		
		if(tsCamera_Position[2] > -80.0)
		{
		  tsCLightObj[0].tsPosition = [tsCamera_Position[0], tsCamera_Position[1], -3.0 , 0.0];
		}
		else
		{
  		  tsCLightObj[0].tsPosition = [tsCamera_Position[0]+0.5, tsCamera_Position[1], -3.0 , 0.0];
		}
		
  		gl.uniform4fv(tsLightPositionUniform[0], tsCLightObj[0].tsPosition);
  
  		// Materials
  	  	gl.uniform3fv(tsKaUniform, [1.0, 1.0, 0.0]);	// Ambient Black Material
  	  	// Diffuse
  	  	gl.uniform3fv(tsKdUniform, [1.0, 1.0, 0.0]);	// White Material
  	  	// Specular
  	  	gl.uniform3fv(tsKsUniform, [1.0, 1.0, 0.0]);	// White Material
  	  	gl.uniform1f(tsShinyNessUniform, 128.0);
  	  }
  	  else
  	  {
  	  	gl.uniform1i(tsKeyPressUniform, 0);	// Disable light in shader
  	  }
  
  	  var tsViewMatrix = mat4.create();
  	  var tsModelMatrix = mat4.create();
  
  	  // Translate
  	  //mat4.translate(tsViewMatrix, tsViewMatrix, [0.0, 0.0, -1.0]);
  	  mat4.copy(tsViewMatrix, cameraMatrix);
  	  gl.uniformMatrix4fv(tsViewMatrixUniform, false, tsViewMatrix);
  	  gl.uniformMatrix4fv(tsProjectionMatrixUniform, false, perspectiveProjectionMatrix);

	  // Draw Wall 
	  for(var i = 0; i < 14; i++)
          {
  	    tsModelMatrix = null;
  	    tsModelMatrix = mat4.create();
  	    mat4.translate(tsModelMatrix, tsModelMatrix, [-2.8, 0.0, tsZDir - i * 6]);
  	    //mat4.scale(tsModelMatrix, tsModelMatrix, [2.0, 2.0, 2.0]);
  	    gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  
  	    // Draw  
            gl.bindVertexArray(tsVao);
  
	      gl.activeTexture(gl.TEXTURE0);
              gl.bindTexture(gl.TEXTURE_2D, tsWallTexture);
              gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	      gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  
  	    gl.bindVertexArray(null);
  
  	    gl.bindTexture(gl.TEXTURE_2D, null);
	  }

  
  	  // Draw Cube
  	  /*tsModelMatrix = null;
  	  tsModelMatrix = mat4.create();
  
  	  // Translate
  	  mat4.translate(tsModelMatrix, tsModelMatrix, [tsXLightDir, tsYLightDir, tsZLightDir]);
  	  mat4.scale(tsModelMatrix, tsModelMatrix, [0.25, 0.25, 0.25]);
  	  //mat4.rotateZ(tsModelMatrix, tsModelMatrix, tsScene2PointLightDegreeToRadian(tsSAngle));
  	  gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  	  gl.uniformMatrix4fv(tsViewMatrixUniform, false, tsViewMatrix);
  	  gl.uniformMatrix4fv(tsProjectionMatrixUniform, false, perspectiveProjectionMatrix);
            gl.bindVertexArray(tsVaoCube);
  
  	    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  	    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
  	    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
  	    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
  	    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
  	    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
  
  	  gl.bindVertexArray(null);
	  */

	  // Draw Picture Frames
	  for(var i = 0; i < 13; i++)
          {
  	    tsModelMatrix = null;
  	    tsModelMatrix = mat4.create();
  
	    // Draw  
	    mat4.translate(tsModelMatrix, tsModelMatrix, [-2.7, 0.0, tsZDir - i * 6]);
  	    mat4.scale(tsModelMatrix, tsModelMatrix, [0.45, 0.45, 0.45]);
  	    gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  	    gl.uniformMatrix4fv(tsViewMatrixUniform, false, tsViewMatrix);
  	    gl.uniformMatrix4fv(tsProjectionMatrixUniform, false, perspectiveProjectionMatrix);
            gl.bindVertexArray(tsVao);
  
	      gl.activeTexture(gl.TEXTURE0);
              gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[i]);
              gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	      gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  
  	    gl.bindVertexArray(null);
  
  	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	// Draw Another Wall 
	for(var i = 0; i < 14; i++)
        {
  	  tsModelMatrix = null;
  	  tsModelMatrix = mat4.create();
  	  mat4.translate(tsModelMatrix, tsModelMatrix, [2.8, 0.0, tsZDir - i * 6]);
  	  mat4.scale(tsModelMatrix, tsModelMatrix, [tsScaleFactor, tsScaleFactor, tsScaleFactor]);
  	  gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  
  	  // Draw  
          gl.bindVertexArray(tsVao);
  
	    gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, tsWallTexture);
            gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  
  	  gl.bindVertexArray(null);
  
  	  gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	// Draw Floor 
	for(var i = 0; i < 14; i++)
        {
  	  tsModelMatrix = null;
  	  tsModelMatrix = mat4.create();
  	  mat4.translate(tsModelMatrix, tsModelMatrix, [0.0, -1.5, tsZDir - i * 6]);
  	  mat4.rotateZ(tsModelMatrix, tsModelMatrix, tsScene2PointLightDegreeToRadian(90.0));
  	  mat4.scale(tsModelMatrix, tsModelMatrix, [tsScaleFactor, tsScaleFactor, tsScaleFactor]);
  	  //mat4.scale(tsModelMatrix, tsModelMatrix, [2.0, 2.0, 2.0]);
  	  gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  
  	  // Draw  
          gl.bindVertexArray(tsVao);
  
	    gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, tsWallTexture);
            gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  
  	  gl.bindVertexArray(null);
  
  	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	// Draw Elevation 
	var index;
	for(index = 0; index < 14; index++)
        {
  	  tsModelMatrix = null;
  	  tsModelMatrix = mat4.create();
  	  mat4.translate(tsModelMatrix, tsModelMatrix, [0.0, 1.5, tsZDir - index * 6]);
  	  mat4.rotateZ(tsModelMatrix, tsModelMatrix, tsScene2PointLightDegreeToRadian(90.0));
  	  mat4.scale(tsModelMatrix, tsModelMatrix, [tsScaleFactor, tsScaleFactor, tsScaleFactor]);
  	  //mat4.scale(tsModelMatrix, tsModelMatrix, [2.0, 2.0, 2.0]);
  	  gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  
  	  // Draw  
          gl.bindVertexArray(tsVao);
  
	    gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, tsWallTexture);
            gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  
  	  gl.bindVertexArray(null);
  
  	  gl.bindTexture(gl.TEXTURE_2D, null);
	}

	// Draw last 
	tsModelMatrix = null;
  	tsModelMatrix = mat4.create();
  	
  	// Draw wall
  	mat4.translate(tsModelMatrix, tsModelMatrix, [0.0, 0.0, tsZDir - index * 6 - 0.1]);
  	mat4.rotateY(tsModelMatrix, tsModelMatrix, tsScene2PointLightDegreeToRadian(90.0));
  	mat4.scale(tsModelMatrix, tsModelMatrix, [tsScaleFactor, tsScaleFactor, tsScaleFactor]);
  	//mat4.scale(tsModelMatrix, tsModelMatrix, [2.0, 2.0, 2.0]);
  	gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  	gl.uniformMatrix4fv(tsViewMatrixUniform, false, tsViewMatrix);
  	gl.uniformMatrix4fv(tsProjectionMatrixUniform, false, perspectiveProjectionMatrix);
  	gl.bindVertexArray(tsVao);
  	
  	  gl.activeTexture(gl.TEXTURE0);
  	  gl.bindTexture(gl.TEXTURE_2D, tsWallTexture);
  	  gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	  gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  	
  	gl.bindVertexArray(null);
  
        gl.bindTexture(gl.TEXTURE_2D, null);
	
	// Draw Frame
	tsModelMatrix = null;
  	tsModelMatrix = mat4.create();
  	
  	// Draw  
  	mat4.translate(tsModelMatrix, tsModelMatrix, [0.0, 0.0, tsZDir - index * 6 - 0.1]);
  	mat4.rotateY(tsModelMatrix, tsModelMatrix, tsScene2PointLightDegreeToRadian(90.0));
  	mat4.scale(tsModelMatrix, tsModelMatrix, [0.5, 0.58, 0.5]);
  	gl.uniformMatrix4fv(tsModelMatrixUniform, false, tsModelMatrix);
  	gl.uniformMatrix4fv(tsViewMatrixUniform, false, tsViewMatrix);
  	gl.uniformMatrix4fv(tsProjectionMatrixUniform, false, perspectiveProjectionMatrix);
  	gl.bindVertexArray(tsVao);
  	
  	  gl.activeTexture(gl.TEXTURE0);
  	  gl.bindTexture(gl.TEXTURE_2D, tsFramesTexture[13]);
  	  gl.uniform1i(tsTextureSamplerUniform, 0);	// 0 => gl.TEXTURE0

  	  gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  	
  	gl.bindVertexArray(null);
  
        gl.bindTexture(gl.TEXTURE_2D, null);
	
  	gl.useProgram(null);
  
  	// Update
	if(false == tsStopUpdating)
	{
  	  tsScene2PointLightUpdate();
	}
	else
	{
		while(tsCountDown--)
		{
			if(0 > tsCountDown)
			{
				break;
			}
			//// console.log("[INFO] tscountdown: " + tsCountDown);
		}
		ts_Global_Scene3Gallery_FadeOut = true;
	}
  }
  
  function tsScene2PointLightUpdate()
  {
  	// Code
	// Increment forward
        pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_FORWARD, 0.01);
	
	// Camera Position: -0.550000011920929,0,-78
	if(tsCamera_Position[2] < -85.0)
	{
		tsStopUpdating = true;
	}
  }
  
  // Degree to Radian
  function tsScene2PointLightDegreeToRadian(tsDegreeAngle)
  {
  	// Return angle in radians
  	return(tsDegreeAngle * Math.PI / 180.0);
  }
  
  this.Uninit=function()
  {
  	// Code
  	if(tsVao)
  	{
  		gl.deleteVertexArray(tsVao);
  		tsVao = null;
  	}
  
  	if(tsVboPosition)
  	{
  		gl.deleteBuffer(tsVboPosition);
  		tsVboPosition = null;
  	}
  
  	if(tsVboColor)
  	{
  		gl.deleteBuffer(tsVboColor);
  		tsVboColor = null;
  	}
  
  	if(tsVboNormal)
  	{
  		gl.deleteBuffer(tsVboNormal);
  		tsVboNormal = null;
  	}
  
  	if(tsVboTexCoord)
  	{
  		gl.deleteBuffer(tsVboTexCoord);
  		tsVboTexCoord = null;
  	}
  
  	// Shader object release
  	if(tsShaderProgramObject)
  	{
  		if(tsFragmentSO)
  		{
  			gl.detachShader(tsShaderProgramObject, tsFragmentSO);
  			gl.deleteShader(tsFragmentSO);
  			tsFragmentSO = null;
  		}
  
  		if(tsVertexSO)
  		{
  			gl.detachShader(tsShaderProgramObject, tsVertexSO);
  			gl.deleteShader(tsVertexSO);
  			tsVertexSO = null;
  		}
  
  		gl.deleteProgram(tsShaderProgramObject);
  		tsShaderProgramObject = null;
  	}
  }
}

