function Skybox() {
    var psm_cubemap_vertexShaderObject;
    var psm_cubemap_fragmentShaderObject;
    var psm_cubemap_shaderProgramObject;

    var psm_cubemap_vao;
    var psm_cubemap_vbo_position;

    var psm_cubemap_mUniform;
    var psm_cubemap_vUniform;
    var psm_cubemap_pUniform;

    var psm_cubemap_samplerUniform;

    var psm_cubemap_texture = 0;

    this.Init = function() {

        var vertexShaderSourceCode_PSM =
            "#version 300 es" +
            "\n" +
            "in vec3 vPosition;\n" +
            "uniform mat4 u_m_matrix;\n" +
            "uniform mat4 u_v_matrix;\n" +
            "uniform mat4 u_p_matrix;\n" +
            "out vec3 out_texcoord;\n" +
            "void main(void)\n" +
            "{\n" +
            "out_texcoord = vPosition;\n" +
            "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vec4(vPosition, 1.0);\n" +
            "}";

        psm_cubemap_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
        gl.shaderSource(psm_cubemap_vertexShaderObject, vertexShaderSourceCode_PSM);
        gl.compileShader(psm_cubemap_vertexShaderObject);

        if (gl.getShaderParameter(psm_cubemap_vertexShaderObject, gl.COMPILE_STATUS) == false) {
            var error = gl.getShaderInfoLog(psm_cubemap_vertexShaderObject);
            if (error.length > 0) {
                alert(error);
                uninitialize();
            }
        }

        var fragmentShaderSourceCode_PSM =
            "#version 300 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 out_texcoord;\n" +
            "uniform highp samplerCube u_texture_sampler;\n" +
            "out vec4 FragColor;\n" +
            "void main(void)\n" +
            "{\n" +
            // "FragColor = texture(u_texture_sampler, out_texcoord);\n" +
            "vec4 tempColor = texture(u_texture_sampler, out_texcoord);" +
			"float gray = (tempColor.x+ tempColor.y + tempColor.z)/3.0;" +
			"FragColor =  vec4(gray , gray , gray,1.0f);" +
            "}";

        psm_cubemap_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
        gl.shaderSource(psm_cubemap_fragmentShaderObject, fragmentShaderSourceCode_PSM);
        gl.compileShader(psm_cubemap_fragmentShaderObject);

        if (gl.getShaderParameter(psm_cubemap_fragmentShaderObject, gl.COMPILE_STATUS) == false) {
            var error = gl.getShaderInfoLog(psm_cubemap_fragmentShaderObject);
            if (error.length > 0) {
                alert(error);
                uninitialize();
            }
        }

        psm_cubemap_shaderProgramObject = gl.createProgram();
        gl.attachShader(psm_cubemap_shaderProgramObject, psm_cubemap_vertexShaderObject);
        gl.attachShader(psm_cubemap_shaderProgramObject, psm_cubemap_fragmentShaderObject);

        gl.bindAttribLocation(psm_cubemap_shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");

        gl.linkProgram(psm_cubemap_shaderProgramObject);

        if (gl.getProgramParameter(psm_cubemap_shaderProgramObject, gl.LINK_STATUS) == false) {
            var error = gl.getProgramInfoLog(psm_cubemap_shaderProgramObject);
            if (error.length > 0) {
                alert(error);
                uninitialize();
            }
        }

        psm_cubemap_mUniform = gl.getUniformLocation(psm_cubemap_shaderProgramObject, "u_m_matrix");
        psm_cubemap_vUniform = gl.getUniformLocation(psm_cubemap_shaderProgramObject, "u_v_matrix");
        psm_cubemap_pUniform = gl.getUniformLocation(psm_cubemap_shaderProgramObject, "u_p_matrix");
        psm_cubemap_samplerUniform = gl.getUniformLocation(psm_cubemap_shaderProgramObject, "u_texture_sampler");
        
        const psm_faceInfos = [
            {
                target: gl.TEXTURE_CUBE_MAP_POSITIVE_X,
                filename: '02_Scene_Pratidnya/Skybox/skybox_textures/right.png',
            },
            {
                target: gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
                filename: '02_Scene_Pratidnya/Skybox/skybox_textures/left.png',
            },
            {
                target: gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
                filename: '02_Scene_Pratidnya/Skybox/skybox_textures/top.png',
            },
            {
                target: gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
                filename: '02_Scene_Pratidnya/Skybox/skybox_textures/bottom.png',
            },
            {
                target: gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
                filename: '02_Scene_Pratidnya/Skybox/skybox_textures/front.png',
            },
            {
                target: gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
                filename: '02_Scene_Pratidnya/Skybox/skybox_textures/back.png',
            },
        ];

        psm_cubemap_texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, psm_cubemap_texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

        psm_faceInfos.forEach((faceInfo) => {
            const level = 0;
            const internalFormat = gl.RGB;
            const format = gl.RGB;
            const type = gl.UNSIGNED_BYTE;

            const image = new Image();
            image.src = faceInfo.filename;
            image.onload = function () {
                // // console.log('processing', image.src)
                gl.texImage2D(faceInfo.target, level, internalFormat, format, type, image);
            }
        });

        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_R, gl.CLAMP_TO_EDGE);

        var cubeVertices_PSM = new Float32Array([
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,

            -1.0, -1.0, 1.0,
            -1.0, -1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            -1.0, -1.0, 1.0,

            1.0, -1.0, -1.0,
            1.0, -1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, 1.0, -1.0,
            1.0, -1.0, -1.0,

            -1.0, -1.0, 1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0,
            -1.0, -1.0, 1.0,

            -1.0, 1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,

            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0
        ]);

        psm_cubemap_vao = gl.createVertexArray();
        gl.bindVertexArray(psm_cubemap_vao);

        psm_cubemap_vbo_position = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, psm_cubemap_vbo_position);
        gl.bufferData(gl.ARRAY_BUFFER, cubeVertices_PSM, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 12, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);

    }

    this.Resize = function() {

    }

    this.Update = function() {

    }

    this.Draw = function() {
        var modelMatrix_PSM = mat4.create();
        var viewMatrix_PSM = mat4.create();
        var scaleMatrix_PSM = mat4.create();

        mat4.copy(viewMatrix_PSM, cameraMatrix);
        // mat4.translate(modelMatrix_PSM, modelMatrix_PSM, [0.0, 0.0, -1.0]);
        mat4.translate(modelMatrix_PSM, modelMatrix_PSM, [59.0, 6.0, -20.0]);
        mat4.scale(scaleMatrix_PSM, scaleMatrix_PSM, [450, 450, 450]);
        mat4.multiply(modelMatrix_PSM, modelMatrix_PSM, scaleMatrix_PSM);
        gl.useProgram(psm_cubemap_shaderProgramObject);

        gl.uniformMatrix4fv(psm_cubemap_mUniform, false, modelMatrix_PSM);
        gl.uniformMatrix4fv(psm_cubemap_vUniform, false, viewMatrix_PSM);
        gl.uniformMatrix4fv(psm_cubemap_pUniform, false, perspectiveProjectionMatrix);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, psm_cubemap_texture);
        gl.uniform1i(psm_cubemap_samplerUniform, 0);

        gl.bindVertexArray(psm_cubemap_vao);
        gl.drawArrays(gl.TRIANGLES, 0, 36);
        gl.bindVertexArray(null);
        gl.useProgram(null);

    }


    function degToRad(degree) {
        return (degree * Math.PI / 180.0);
    }

    this.Uninit = function() {

        if (psm_cubemap_vao) {
            gl.deleteVertexArray(psm_cubemap_vao);
            psm_cubemap_vao = null;
        }

        if (psm_cubemap_vbo_position) {
            gl.deleteBuffer(psm_cubemap_vbo_position);
            psm_cubemap_vbo_position = null;
        }

        if (psm_cubemap_texture) {
            gl.deleteTexture(psm_cubemap_texture);
            psm_cubemap_texture = null;
        }

        if (psm_cubemap_shaderProgramObject) {

            if (psm_cubemap_vertexShaderObject) {
                gl.detachShader(psm_cubemap_shaderProgramObject, psm_cubemap_vertexShaderObject);
                gl.deleteShader(psm_cubemap_vertexShaderObject);
                psm_cubemap_vertexShaderObject = null;
            }

            if (psm_cubemap_fragmentShaderObject) {
                gl.detachShader(psm_cubemap_shaderProgramObject, psm_cubemap_fragmentShaderObject);
                gl.deleteShader(psm_cubemap_fragmentShaderObject);
                psm_cubemap_fragmentShaderObject = null;
            }

            gl.deleteProgram(psm_cubemap_shaderProgramObject);
            psm_cubemap_shaderProgramObject = null;
        }
    }
}
