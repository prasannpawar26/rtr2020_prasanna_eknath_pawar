
function Terrain()
{
	var pep_Terrain_vs;
	var pep_Terrain_fs;
	var pep_Terrain_program;

	var pep_Terrain_vao;
	var pep_Terrain_vbo_position;
	var pep_Terrain_vbo_texcoords;

	var pep_Terrain_modelMatrixUniform;
	var pep_Terrain_viewMatrixUniform;
	var pep_Terrain_projectionMatrixUniform;
	var pep_Terrain_samplerUniform;
	
	var pep_Terrain_texture_1 = 0;
	
	var PEP_TERAIN_SCALE = 800;
	var PEP_TERRAIN_VERTEX_COUNT_X = 257;
	var PEP_TERRAIN_VERTEX_COUNT_Z = 257;

	var pep_Terrain_Vertices = null;
	var pep_Terrain_Normals = null;
	var pep_Terrain_TextureCoords = null;
	var pep_Terrain_Indices = null;
	
	var pep_Terrain_HeightMap;
	var pep_Terrain_heightMapConverted;
	var cameraZPosition = cameraPosition[2];
	var cameraYaw = -42.50;

	Terrain_HeightMap=function()
	{
		var height_canvas = document.createElement('canvas');
		height_canvas.width = 257;
		height_canvas.height = 257;
		var context = height_canvas.getContext('2d');
		context.drawImage(Terrain_Heightmap, 0, 0);
		pep_Terrain_HeightMap = context.getImageData(0, 0, 257, 257).data;

		var float_array_size = pep_Terrain_HeightMap.length / 4;
		float_array_size += pep_Terrain_HeightMap.length % 4 ? 1: 0;
		pep_Terrain_heightMapConverted = new Float32Array(float_array_size);
		
		var max_color_value = 255 * 255 * 255;
		var array_index = 0;
		for(var index = 0; index < pep_Terrain_HeightMap.length; index += 4)
		{
			var temp = (pep_Terrain_HeightMap[index + 2] << 0) |  // left shift by 0
			(pep_Terrain_HeightMap[index + 1] << 8) | // left shift by 8
			(pep_Terrain_HeightMap[index + 0] << 16); // left shift by 16

			pep_Terrain_heightMapConverted[array_index] = parseFloat(temp / max_color_value);
			array_index += 1;
		}
	}
	
	Terrain_Generate=function()
	{
		pep_Terrain_Vertices = new Float32Array(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3);
		pep_Terrain_Normals = new Float32Array(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3);
		pep_Terrain_TextureCoords = new Float32Array(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 2);
		pep_Terrain_Indices = new Uint32Array(6 * (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1));
		
		var  vertexPointer = 0;
		var index = 0;
		for (var i = 0; i < PEP_TERRAIN_VERTEX_COUNT_Z; i++)
		{
			for (var j = 0; j < PEP_TERRAIN_VERTEX_COUNT_X; j++)
			{
				//// console.log(pep_Terrain_heightMapConverted[(j * PEP_TERRAIN_VERTEX_COUNT_Z + i)]);
				pep_Terrain_Vertices[vertexPointer * 3 + 0] = parseFloat(j) * 4.0;
				pep_Terrain_Vertices[vertexPointer * 3 + 1] = pep_Terrain_heightMapConverted[(j * PEP_TERRAIN_VERTEX_COUNT_Z + i) * 3] * 80.0;
				pep_Terrain_Vertices[vertexPointer * 3 + 2] = parseFloat(i) * 4.0;

				//// console.log("[" + pep_Terrain_Vertices[vertexPointer * 3 + 0] + "," + pep_Terrain_Vertices[vertexPointer * 3 + 1] + "," + pep_Terrain_Vertices[vertexPointer * 3 + 2] + "]");
				
				pep_Terrain_Normals[vertexPointer * 3 + 0] = 0.0;
				pep_Terrain_Normals[vertexPointer * 3 + 1] = 1.0;
				pep_Terrain_Normals[vertexPointer * 3 + 2] = 0.0;

				pep_Terrain_TextureCoords[vertexPointer * 2 + 0] =
					parseFloat(j) / parseFloat(PEP_TERRAIN_VERTEX_COUNT_X - 1);
				pep_Terrain_TextureCoords[vertexPointer * 2 + 1] =
					parseFloat(i) / parseFloat(PEP_TERRAIN_VERTEX_COUNT_Z - 1);
				
				vertexPointer += 1;
				index += 4;
			}
		}
		
		var pointer = 0;
		for (var gz = 0; gz < PEP_TERRAIN_VERTEX_COUNT_Z - 1; gz++)
		{
			for (var gx = 0; gx < PEP_TERRAIN_VERTEX_COUNT_X - 1; gx++)
			{
				var vertex_index = gz * PEP_TERRAIN_VERTEX_COUNT_Z + gx;
				pep_Terrain_Indices[pointer + 0] = parseFloat(vertex_index);
				pep_Terrain_Indices[pointer + 1] = parseFloat(vertex_index + PEP_TERRAIN_VERTEX_COUNT_Z + 1);
				pep_Terrain_Indices[pointer + 2] = parseFloat(vertex_index + 1);
				pep_Terrain_Indices[pointer + 3] = parseFloat(vertex_index);
				pep_Terrain_Indices[pointer + 4] = parseFloat(vertex_index + PEP_TERRAIN_VERTEX_COUNT_Z);
				pep_Terrain_Indices[pointer + 5] = parseFloat(vertex_index + PEP_TERRAIN_VERTEX_COUNT_Z + 1);
				pointer += 6;
			}
		}
	}

	this.Init=function()
	{
		Terrain_HeightMap();
		Terrain_Generate();
		
		// vertex shader
		var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTexcoords;" +
		
		"out vec2 texCoords;" +
		
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		
		"float random (in vec2 _st) {" +
			"return fract(sin(dot(_st.xy, vec2(12.9898,78.233))) * 43758.5453123);" +
		"}" +
		
		"void main(void)" +
		"{" +
			//"float y = random(vec2(vPosition.x, vPosition.z));" +
			
			//"vec4 position = vec4(vPosition.x, y, vPosition.z, vPosition.w);" +
			
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"texCoords = vTexcoords;" +
		"}";
		
		pep_Terrain_vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(pep_Terrain_vs, vertexShaderSourceCode);
		gl.compileShader(pep_Terrain_vs);
		if(gl.getShaderParameter(pep_Terrain_vs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Terrain_vs);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// // console.log("RTR : vertexShaderObject Compiled Successfully");
		
		// fragment shader
		var fragmentShaderSourceCode = 
		"#version 300 es" +
		"\n" +
		"precision highp float;"+
		"in vec2 texCoords;" +
		
		"uniform highp sampler2D u_texture0_sampler;" +
		
		"out vec4 FragColor;" +
		

		
		"void main(void)" +
		"{" +
		"vec4 tempColor = texture(u_texture0_sampler, texCoords * 10.0);" +
			"float gray = (tempColor.x+ tempColor.y + tempColor.z)/3.0;" +
			"FragColor =  vec4(gray , gray , gray,1.0f);" +

			// "FragColor = texture(u_texture0_sampler, texCoords * 10.0);" +
			
			//"FragColor = vec4(1.0, 0.0, 0.0, 1.0);" +
		"}";
		
		pep_Terrain_fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(pep_Terrain_fs, fragmentShaderSourceCode);
		gl.compileShader(pep_Terrain_fs);
		if(gl.getShaderParameter(pep_Terrain_fs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Terrain_fs);
			if(erro.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		
		// shader program
		pep_Terrain_program = gl.createProgram();
		gl.attachShader(pep_Terrain_program, pep_Terrain_vs);
		gl.attachShader(pep_Terrain_program, pep_Terrain_fs);
		
		// pre-link binding of shader program object with vertex shader attributes
		gl.bindAttribLocation(pep_Terrain_program, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(pep_Terrain_program, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexcoords");
		
		// linking
		gl.linkProgram(pep_Terrain_program);
		if(!gl.getProgramParameter(pep_Terrain_program, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(pep_Terrain_program);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// // console.log("RTR : shaderProgramObject Linked Successfully");
		
		// get uniform location
		pep_Terrain_modelMatrixUniform = gl.getUniformLocation(pep_Terrain_program, "u_model_matrix");
		pep_Terrain_viewMatrixUniform = gl.getUniformLocation(pep_Terrain_program, "u_view_matrix");
		pep_Terrain_projectionMatrixUniform = gl.getUniformLocation(pep_Terrain_program, "u_projection_matrix");
		pep_Terrain_samplerUniform = gl.getUniformLocation(pep_Terrain_program, "u_texture0_sampler");
		
		// Load Cube Texture
		pep_Terrain_texture_1 = gl.createTexture();
		pep_Terrain_texture_1.image = new Image();
		pep_Terrain_texture_1.image.src = "02_Scene_Pratidnya/Terrain/Terrain_Texture.png";
		pep_Terrain_texture_1.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, pep_Terrain_texture_1);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pep_Terrain_texture_1.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		// pep_Terrain_Vertices, colors, shader attribs, vbo_square_position, vao_square initializations
		pep_Terrain_vao = gl.createVertexArray();
		gl.bindVertexArray(pep_Terrain_vao);
		
		pep_Terrain_vbo_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Terrain_vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, pep_Terrain_Vertices, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		pep_Terrain_vbo_texcoords = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Terrain_vbo_texcoords);
		gl.bufferData(gl.ARRAY_BUFFER, pep_Terrain_TextureCoords, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		pep_Terrain_vbo_indices = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,pep_Terrain_vbo_indices);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, pep_Terrain_Indices, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
		
		gl.bindVertexArray(null);
	} // Init End
	
	this.Uninit=function()
	{
		if(pep_Terrain_vao)
		{
			gl.deleteVertexArray(pep_Terrain_vao);
			pep_Terrain_vao = null;
		}
		
		if(pep_Terrain_vbo_texcoords)
		{
			gl.deleteBuffer(pep_Terrain_vbo_texcoords);
			pep_Terrain_vbo_texcoords = null;
		}
		
		if(pep_Terrain_vbo_position)
		{
			gl.deleteBuffer(pep_Terrain_vbo_position);
			pep_Terrain_vbo_position = null;
		}

		if(pep_Terrain_program)
		{
			if(pep_Terrain_fs)
			{
				gl.detachShader(pep_Terrain_program, pep_Terrain_fs);
				gl.deleteShader(pep_Terrain_fs);
				pep_Terrain_fs = null;
			}
			
			if(pep_Terrain_vs)
			{
				gl.detachShader(pep_Terrain_program, pep_Terrain_vs);
				gl.deleteShader(pep_Terrain_vs);
				pep_Terrain_vs = null;
			}
			
			gl.deleteProgram(pep_Terrain_program);
			pep_Terrain_program = null;
		}
	}
	
	this.Resize=function(width, height)
	{
	}
	
	this.Update=function()
	{
	}
	
	this.Draw=function()
	{
		

		var modelMatrix = mat4.create();
		var viewMatrix = mat4.create();
		var projectionMatrix = mat4.create();

		mat4.identity(modelMatrix);
		mat4.identity(viewMatrix);
		mat4.identity(projectionMatrix);
		
		mat4.translate(modelMatrix, modelMatrix, [-385.0, 0.0, -210.0]);
		mat4.rotateY(modelMatrix, modelMatrix, degtored(45.0));
		
		mat4.copy(projectionMatrix, perspectiveProjectionMatrix);
		pratidnyaCounter = pratidnyaCounter + 1;
		// all voices end at 2379f
		// maharaj voice ends at 924
		if (pratidnyaCounter < 910) { //870f
			//mat4.copy(viewMatrix, cameraMatrix);
			
			// console.log('pratidnyaCounter1: ' + pratidnyaCounter);
			// console.log('cameraPosition1: ' + cameraPosition);
			cameraZPosition = cameraPosition[2];
			
		} else if (pratidnyaCounter < 3240) { // 2379
			// console.log('cameraPosition2: ' + cameraPosition);
			// console.log('pratidnyaCounter2: ' + pratidnyaCounter);
			// -24 < 
			if (cameraZPosition < (cameraPosition_Mid[2] * 1.5)) {
				pep_camera.Camera_SetYaw(-42.50);
				cameraZPosition += 0.12;
				cameraPosition[2] = cameraZPosition;
				pep_camera.Camera_SetPosition(cameraPosition);
			} else {
				cameraYaw = cameraYaw - 0.09;
				pep_camera.Camera_SetYaw(cameraYaw);
			}
			
		} else {
			ts_Global_Scene2Pratidnya_FadeOut = true;
		}
		// console.log('pratidnyaCounter3: ' + pratidnyaCounter);
		pep_camera.Camera_UpdateVectors();
		cameraMatrix = pep_camera.Camera_GetViewMatrix();
		mat4.copy(viewMatrix, cameraMatrix);
		
		gl.useProgram(pep_Terrain_program);
		gl.uniformMatrix4fv(pep_Terrain_modelMatrixUniform, false, modelMatrix);
		gl.uniformMatrix4fv(pep_Terrain_viewMatrixUniform, false, viewMatrix);
		gl.uniformMatrix4fv(pep_Terrain_projectionMatrixUniform, false, projectionMatrix);
		gl.uniform1i(pep_Terrain_samplerUniform, 0);

		var temp = 6 * (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1);
		
		gl.bindTexture(gl.TEXTURE_2D, pep_Terrain_texture_1);
		gl.bindVertexArray(pep_Terrain_vao);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, pep_Terrain_vbo_indices);
        gl.drawElements(gl.TRIANGLES, temp, gl.UNSIGNED_INT, 0);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
		gl.bindVertexArray(null);
		gl.bindTexture(gl.TEXTURE_2D, null);

		gl.useProgram(null);
	}
}
