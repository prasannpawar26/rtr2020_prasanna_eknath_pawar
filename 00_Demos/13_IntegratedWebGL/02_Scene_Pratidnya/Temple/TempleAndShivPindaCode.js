function TempleAndShivPinda() {
    var AJ_Angle = 0.0;

    var AJ_gVertexShaderObject;
    var AJ_gFragmentShaderObject;
    var AJ_gShaderProgramObject;

    var AJ_gVao_TempleBase;
    var AJ_gVao_TemplePillars;
    var AJ_gVao_TemplePillarsBase;
    var AJ_gVao_TempleRoof;
    var AJ_gVao_TempleWalls;
    var AJ_gVao_TempleFlag;

    var AJ_gVbo_Position_TempleBase;
    var AJ_gVbo_Texture_TempleBase;
    var AJ_gVbo_Normals_TempleBase;

    var AJ_gVbo_Position_TemplePillars;
    var AJ_gVbo_Texture_TemplePillars;
    var AJ_gVbo_Normals_TemplePillars;

    var AJ_gVbo_Position_TemplePillarsBase;
    var AJ_gVbo_Texture_TemplePillarsBase;
    var AJ_gVbo_Normals_TemplePillarsBase;

    var AJ_gVbo_Position_TempleRoof;
    var AJ_gVbo_Texture_TempleRoof;
    var AJ_gVbo_Normals_TempleRoof;

    var AJ_gVbo_Position_TempleWalls;
    var AJ_gVbo_Texture_TempleWalls;
    var AJ_gVbo_Normals_TempleWalls;
    var AJ_gVbo_Position_TempleFlag;
    var AJ_gVbo_Texture_TempleFlag;
    var AJ_gVbo_Normals_TempleFlag;

    var AJ_gVao_ShivaBase;
    var AJ_gVao_ShivaBigTamba;
    var AJ_gVao_ShivaLeaves;
    var AJ_gVao_ShivPinda;

    var AJ_gVbo_Position_ShivaBase;
    var AJ_gVbo_Texture_ShivaBase;
    var AJ_gVbo_Normals_ShivaBase;

    var AJ_gVbo_Position_ShivaBigTamba;
    var AJ_gVbo_Texture_ShivaBigTamba;
    var AJ_gVbo_Normals_ShivaBigTamba;

    var AJ_gVbo_Position_ShivaLeaves;
    var AJ_gVbo_Texture_ShivaLeaves;
    var AJ_gVbo_Normals_ShivaLeaves;

    var AJ_gVbo_Position_ShivPinda;
    var AJ_gVbo_Texture_ShivPinda;
    var AJ_gVbo_Normals_ShivPinda;


    var AJ_modelMatrixUniform;
    var AJ_ViewMatrixUniform;
    var AJ_prespectiveProjectionUniform;
    var AJ_lKeyPressedUniform;

    var AJ_lightDiffuseUniform;
    var AJ_lightAmbientUniform;
    var AJ_lightSpecularUniform;
    var AJ_lightPositionUniform;

    var AJ_materialAmbientUniform;
    var AJ_materialDiffuseUniform;
    var AJ_materialSpecularUniform;
    var AJ_materialShininessUniform;

    var AJ_gbLight = true;

    var AJ_TextureSamplerUniform;

    var AJ_TempleBase_Texture;
    var AJ_TemplePillars_Texture;
    var AJ_TemplePillarsBase_Texture;
    var AJ_TempleRoof_Texture;
    var AJ_TempleWalls_Texture;
    var AJ_TempleFlag_Texture;


    var AJ_shivaPinda_Texture;
    var AJ_shivaLeaves_Texture;
    var AJ_shivaBase_Texture;
    var AJ_shivaBigTamba_Texture;

    this.init = function () {
        var vertexShaderSourceCode =
            "#version 300 es\n" +
            "precision highp float;\n" +
            "precision highp int;\n" +
            "in vec4 vPosition;\n" +
            "in vec2 vTexCoord;\n" +
            "in vec3 vNormal;\n" +
            "uniform mat4 u_model_matrix;\n" +
            "uniform mat4 u_view_matrix;\n" +
            "uniform mat4 u_projection_matrix;\n" +
            "uniform int u_lKeyPressed;\n" +
            "uniform vec4 u_light_position;\n" +
            "out vec3 out_transformed_normal;\n" +
            "out vec3 out_light_direction;\n" +
            "out vec3 out_viewVector;\n" +
            "out vec2 out_vTexCoord;\n" +
            "void main(void)\n" +
            "{\n" +
            "if(u_lKeyPressed==1)\n" +
            "{\n" +
            "vec4 eye_coordinate=u_view_matrix*u_model_matrix*vPosition;\n" +
            "out_transformed_normal=mat3(u_view_matrix*u_model_matrix)*vNormal;\n" +
            "out_light_direction=vec3(u_light_position-eye_coordinate);\n" +
            "out_viewVector = -eye_coordinate.xyz;\n" +
            "}\n" +
            "gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;\n" +
            "out_vTexCoord=vTexCoord;\n" +
            "}\n";

        AJ_gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
        gl.shaderSource(AJ_gVertexShaderObject, vertexShaderSourceCode);
        gl.compileShader(AJ_gVertexShaderObject);

        if (gl.getShaderParameter(AJ_gVertexShaderObject, gl.COMPILE_STATUS) == false) {
            var error = gl.getShaderInfoLog(AJ_gVertexShaderObject);
            if (error.length > 0) {
                alert(error);
                uninitialize();
            }
        }

        var fragmentShaderSourceCode =
            "#version 300 es" +
            "\n" +
            "precision highp float;\n" +
            "precision highp int;\n" +
            "in vec3 out_fong_ads_light;\n" +
            "in vec3 out_transformed_normal;\n" +
            "in vec3 out_light_direction;\n" +
            "in vec3 out_viewVector;\n" +
            "in vec2 out_vTexCoord;\n" +
            "uniform int u_lKeyPressed;\n" +
            "uniform vec3 u_light_ambient;\n" +
            "uniform vec3 u_light_diffuse;\n" +
            "uniform vec3 u_light_specular;\n" +
            "uniform vec3 u_material_ambient;\n" +
            "uniform vec3 u_material_diffuse;\n" +
            "uniform vec3 u_material_specular;\n" +
            "uniform float u_material_shininess;\n" +
            "uniform sampler2D uTexture_Sampler;\n" +
            "out vec4 FragColor;\n" +
            "void main(void)" +
            "{" +
            "vec3 fong_ads_light;\n" +
            "if(u_lKeyPressed==1)\n" +
            "{\n" +
            "vec3 normalize_transformed_normal= normalize(out_transformed_normal);\n" +
            "vec3 normalize_light_direction=normalize(out_light_direction);\n" +
            "vec3 normalize_viewVector =normalize(out_viewVector);\n" +
            "vec3 ReflectionVector = vec3(reflect(-normalize_light_direction,normalize_transformed_normal));\n" +
            "vec3 ambient = u_light_ambient*u_material_ambient;\n" +
            "vec3 diffuse = u_light_diffuse*u_material_diffuse*max(dot(normalize_light_direction,normalize_transformed_normal),0.0f);\n" +
            "vec3 specular = u_light_specular*u_material_specular*pow(max(dot(ReflectionVector,normalize_viewVector),0.0f),u_material_shininess);\n" +
            "fong_ads_light=ambient+diffuse+specular;\n" +
            "}\n" +
            "else\n" +
            "{\n" +
            "fong_ads_light=vec3(1.0f,1.0f,1.0f);\n" +
            "}\n" +
            "vec4 tempColor = (vec4(fong_ads_light,1.0f))+(texture(uTexture_Sampler,out_vTexCoord));" +
			"float gray = (tempColor.x+ tempColor.y + tempColor.z)/3.0;" +
			"FragColor =  vec4(gray , gray , gray,1.0f);" +
            // "FragColor=(vec4(fong_ads_light,1.0f))+(texture(uTexture_Sampler,out_vTexCoord));\n" +
            "}";

        AJ_gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
        gl.shaderSource(AJ_gFragmentShaderObject, fragmentShaderSourceCode);
        gl.compileShader(AJ_gFragmentShaderObject);

        if (gl.getShaderParameter(AJ_gFragmentShaderObject, gl.COMPILE_STATUS) == false) {
            var error = gl.getShaderInfoLog(AJ_gFragmentShaderObject);
            if (error.length > 0) {
                alert("Fragment Error - " + error);
                uninitialize();
            }
        }

        AJ_gShaderProgramObject = gl.createProgram();
        gl.attachShader(AJ_gShaderProgramObject, AJ_gVertexShaderObject);
        gl.attachShader(AJ_gShaderProgramObject, AJ_gFragmentShaderObject);

        gl.bindAttribLocation(AJ_gShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
        gl.bindAttribLocation(AJ_gShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");
        gl.bindAttribLocation(AJ_gShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

        gl.linkProgram(AJ_gShaderProgramObject);

        if (!gl.getProgramParameter(AJ_gShaderProgramObject, gl.LINK_STATUS)) {
            var error = gl.getProgramInfoLog(AJ_gShaderProgramObject);

            if (error.length > 0) {
                alert(error);
                uninitialize();
            }
        }


        AJ_TextureSamplerUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "uTexture_Sampler");

        AJ_modelMatrixUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_model_matrix");
        AJ_ViewMatrixUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_view_matrix");
        AJ_prespectiveProjectionUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_projection_matrix");
        AJ_lKeyPressedUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_lKeyPressed");

        //Change
        AJ_lightAmbientUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_light_ambient");
        AJ_lightDiffuseUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_light_diffuse");
        AJ_lightSpecularUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_light_specular");
        AJ_lightPositionUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_light_position");

        //Change
        AJ_materialAmbientUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_material_ambient");
        AJ_materialDiffuseUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_material_diffuse");
        AJ_materialSpecularUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_material_specular");
        AJ_materialShininessUniform = gl.getUniformLocation(AJ_gShaderProgramObject, "u_material_shininess");

        AJ_TempleBase_Texture = loadTexture(AJ_TempleBase_Texture, "02_Scene_Pratidnya/Temple/Temple/Temple_Floor.png");
        AJ_TemplePillars_Texture = loadTexture(AJ_TemplePillars_Texture, "02_Scene_Pratidnya/Temple/Temple/Temple_Piller.png");
        AJ_TemplePillarsBase_Texture = loadTexture(AJ_TemplePillarsBase_Texture, "02_Scene_Pratidnya/Temple/Temple/Temple_Piller_Floor.png");
        AJ_TempleRoof_Texture = loadTexture(AJ_TempleRoof_Texture, "02_Scene_Pratidnya/Temple/Temple/Temple_Roof.png");
        AJ_TempleWalls_Texture = loadTexture(AJ_TempleWalls_Texture, "02_Scene_Pratidnya/Temple/Temple/Walls.png");
        AJ_TempleFlag_Texture = loadTexture(AJ_TempleFlag_Texture, "02_Scene_Pratidnya/Temple/Temple/FlagTexture.png");


        AJ_shivaPinda_Texture = loadTexture(AJ_shivaPinda_Texture, "02_Scene_Pratidnya/Temple/Shiva/shivaPinda.png");
        AJ_shivaLeaves_Texture = loadTexture(AJ_shivaLeaves_Texture, "02_Scene_Pratidnya/Temple/Shiva/leaves.png");
        AJ_shivaBase_Texture = loadTexture(AJ_shivaBase_Texture, "02_Scene_Pratidnya/Temple/Shiva/shivBase.png");
        AJ_shivaBigTamba_Texture = loadTexture(AJ_shivaBigTamba_Texture, "02_Scene_Pratidnya/Temple/Shiva/Copper.png");

        AJ_gVao_TempleBase = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_TempleBase);

        //Temple Base
        AJ_gVbo_Position_TempleBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_TempleBase);
        gl.bufferData(gl.ARRAY_BUFFER, templeBaseVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_TempleBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_TempleBase);
        gl.bufferData(gl.ARRAY_BUFFER, templeBaseTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_TempleBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_TempleBase);
        gl.bufferData(gl.ARRAY_BUFFER, templeBaseNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
        //Temple Base Ends

        //TemplePillars Starts
        AJ_gVao_TemplePillars = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_TemplePillars);
        AJ_gVbo_Position_TemplePillars = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_TemplePillars);
        gl.bufferData(gl.ARRAY_BUFFER, templePillarsVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_TemplePillars = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_TemplePillars);
        gl.bufferData(gl.ARRAY_BUFFER, templePillarsTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_TemplePillars = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_TemplePillars);
        gl.bufferData(gl.ARRAY_BUFFER, templePillarsNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);

        //TemplePillars Ends

        //TemplePillarBase Starts

        AJ_gVao_TemplePillarsBase = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_TemplePillarsBase);

        AJ_gVbo_Position_TemplePillarsBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_TemplePillarsBase);
        gl.bufferData(gl.ARRAY_BUFFER, templePillarsBaseVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_TemplePillarsBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_TemplePillarsBase);
        gl.bufferData(gl.ARRAY_BUFFER, templePillarsBaseTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_TemplePillarsBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_TemplePillarsBase);
        gl.bufferData(gl.ARRAY_BUFFER, templePillarsBaseNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //TemplePillarBase Ends
        gl.bindVertexArray(null);

        AJ_gVao_TempleRoof = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_TempleRoof);

        AJ_gVbo_Position_TempleRoof = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_TempleRoof);
        gl.bufferData(gl.ARRAY_BUFFER, templeRoofVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_TempleRoof = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_TempleRoof);
        gl.bufferData(gl.ARRAY_BUFFER, templeRoofTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_TempleRoof = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_TempleRoof);
        gl.bufferData(gl.ARRAY_BUFFER, templeRoofNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);


        AJ_gVao_TempleWalls = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_TempleWalls);

        AJ_gVbo_Position_TempleWalls = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_TempleWalls);
        gl.bufferData(gl.ARRAY_BUFFER, templeWallsVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_TempleWalls = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_TempleWalls);
        gl.bufferData(gl.ARRAY_BUFFER, templeWallsTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_TempleWalls = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_TempleWalls);
        gl.bufferData(gl.ARRAY_BUFFER, templeWallsNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);


        AJ_gVao_TempleFlag= gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_TempleFlag);

        AJ_gVbo_Position_TempleFlag = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_TempleFlag);
        gl.bufferData(gl.ARRAY_BUFFER, marathaFlagVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_TempleFlag = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_TempleFlag);
        gl.bufferData(gl.ARRAY_BUFFER, marathaFlagTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_TempleFlag = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_TempleFlag);
        gl.bufferData(gl.ARRAY_BUFFER, marathaFlagNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
        //Shiva starts

        //Shiva Pinda Starts
        AJ_gVao_ShivPinda = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_ShivPinda);

        AJ_gVbo_Position_ShivPinda = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_ShivPinda);
        gl.bufferData(gl.ARRAY_BUFFER, shivPindaVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_ShivPinda = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_ShivPinda);
        gl.bufferData(gl.ARRAY_BUFFER, shivPindaTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_ShivPinda = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_ShivPinda);
        gl.bufferData(gl.ARRAY_BUFFER, shivPindaNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
        //Shiva Pinda Ends


        //Shiva Base Starts

        AJ_gVao_ShivaBase = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_ShivaBase);

        AJ_gVbo_Position_ShivaBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_ShivaBase);
        gl.bufferData(gl.ARRAY_BUFFER, shivaBaseVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_ShivaBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_ShivaBase);
        gl.bufferData(gl.ARRAY_BUFFER, shivaBaseTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_ShivaBase = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_ShivaBase);
        gl.bufferData(gl.ARRAY_BUFFER, shivaBaseNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);

        //Shiva Base Ends


        //Shiva leaves starts

        AJ_gVao_ShivaLeaves = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_ShivaLeaves);

        AJ_gVbo_Position_ShivaLeaves = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_ShivaLeaves);
        gl.bufferData(gl.ARRAY_BUFFER, shivaLeavesVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_ShivaLeaves = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_ShivaLeaves);
        gl.bufferData(gl.ARRAY_BUFFER, shivaLeavesTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_ShivaLeaves = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_ShivaLeaves);
        gl.bufferData(gl.ARRAY_BUFFER, shivaLeavesNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);

        //shiva leaves ENds

        //shiva big Tamba Starts

        AJ_gVao_ShivaBigTamba = gl.createVertexArray();
        gl.bindVertexArray(AJ_gVao_ShivaBigTamba);

        AJ_gVbo_Position_ShivaBigTamba = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Position_ShivaBigTamba);
        gl.bufferData(gl.ARRAY_BUFFER, bigTambaVertices, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Texture_ShivaBigTamba = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Texture_ShivaBigTamba);
        gl.bufferData(gl.ARRAY_BUFFER, bigTambaTextures, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        AJ_gVbo_Normals_ShivaBigTamba = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, AJ_gVbo_Normals_ShivaBigTamba);
        gl.bufferData(gl.ARRAY_BUFFER, bigTambaNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);

        //shiva big Tamba Ends


        //Shiva Ends
    }

    function loadTexture(texture, path) {
        texture = gl.createTexture();
        texture.image = new Image();
        texture.image.src = path;

        texture.image.onload = function () {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);

            gl.generateMipmap(gl.TEXTURE_2D);
        };

        return texture;
    }

    this.uninitialize = function () {
        if (AJ_gVao_TempleBase) {
            gl.deleteVertexArray(AJ_gVao_TempleBase);
            AJ_gVao_TempleBase = null;
        }

        if (AJ_gVao_TemplePillars) {
            gl.deleteVertexArray(AJ_gVao_TemplePillars);
            AJ_gVao_TemplePillars = null;
        }
        if (AJ_gVao_TemplePillarsBase) {
            gl.deleteVertexArray(AJ_gVao_TemplePillarsBase);
            AJ_gVao_TemplePillarsBase = null;
        }
        if (AJ_gVao_TempleRoof) {
            gl.deleteVertexArray(AJ_gVao_TempleRoof);
            AJ_gVao_TempleRoof = null;
        }
        if (AJ_gVao_ShivaBase) {
            gl.deleteVertexArray(AJ_gVao_ShivaBase);
            AJ_gVao_ShivaBase = null;
        }
        if (AJ_gVao_ShivaBigTamba) {
            gl.deleteVertexArray(AJ_gVao_ShivaBigTamba);
            AJ_gVao_ShivaBigTamba = null;
        }
        if (AJ_gVao_ShivaLeaves) {
            gl.deleteVertexArray(AJ_gVao_ShivaLeaves);
            AJ_gVao_ShivaLeaves = null;
        }
        if (AJ_gVao_ShivPinda) {
            gl.deleteVertexArray(AJ_gVao_ShivPinda);
            AJ_gVao_ShivPinda = null;
        }

        if (AJ_gVbo_Position_TempleBase) {
            gl.deleteBuffer(AJ_gVbo_Position_TempleBase);
            AJ_gVbo_Position_TempleBase = null;
        }

        if (AJ_gVbo_Texture_TempleBase) {
            gl.deleteBuffer(AJ_gVbo_Texture_TempleBase);
            AJ_gVbo_Texture_TempleBase = null;
        }

        if (AJ_gVbo_Normals_TempleBase) {
            gl.deleteBuffer(AJ_gVbo_Normals_TempleBase);
            AJ_gVbo_Normals_TempleBase = null;
        }

        if (AJ_gVbo_Position_TemplePillars) {
            gl.deleteBuffer(AJ_gVbo_Position_TemplePillars);
            AJ_gVbo_Position_TemplePillars = null;
        }

        if (AJ_gVbo_Texture_TemplePillars) {
            gl.deleteBuffer(AJ_gVbo_Texture_TemplePillars);
            AJ_gVbo_Texture_TemplePillars = null;
        }

        if (AJ_gVbo_Normals_TemplePillars) {
            gl.deleteBuffer(AJ_gVbo_Normals_TemplePillars);
            AJ_gVbo_Normals_TemplePillars = null;
        }

        if (AJ_gVbo_Position_TemplePillarsBase) {
            gl.deleteBuffer(AJ_gVbo_Position_TemplePillarsBase);
            AJ_gVbo_Position_TemplePillarsBase = null;
        }

        if (AJ_gVbo_Texture_TemplePillarsBase) {
            gl.deleteBuffer(AJ_gVbo_Texture_TemplePillarsBase);
            AJ_gVbo_Texture_TemplePillarsBase = null;
        }

        if (AJ_gVbo_Normals_TemplePillarsBase) {
            gl.deleteBuffer(AJ_gVbo_Normals_TemplePillarsBase);
            AJ_gVbo_Normals_TemplePillarsBase = null;
        }

        if (AJ_gVbo_Position_TempleRoof) {
            gl.deleteBuffer(AJ_gVbo_Position_TempleRoof);
            AJ_gVbo_Position_TempleRoof = null;
        }

        if (AJ_gVbo_Texture_TempleRoof) {
            gl.deleteBuffer(AJ_gVbo_Texture_TempleRoof);
            AJ_gVbo_Texture_TempleRoof = null;
        }

        if (AJ_gVbo_Normals_TempleRoof) {
            gl.deleteBuffer(AJ_gVbo_Normals_TempleRoof);
            AJ_gVbo_Normals_TempleRoof = null;
        }

        if (AJ_gVbo_Position_ShivaBase) {
            gl.deleteBuffer(AJ_gVbo_Position_ShivaBase);
            AJ_gVbo_Position_ShivaBase = null;
        }

        if (AJ_gVbo_Texture_ShivaBase) {
            gl.deleteBuffer(AJ_gVbo_Texture_ShivaBase);
            AJ_gVbo_Texture_ShivaBase = null;
        }

        if (AJ_gVbo_Normals_ShivaBase) {
            gl.deleteBuffer(AJ_gVbo_Normals_ShivaBase);
            AJ_gVbo_Normals_ShivaBase = null;
        }

        if (AJ_gVbo_Position_ShivaBigTamba) {
            gl.deleteBuffer(AJ_gVbo_Position_ShivaBigTamba);
            AJ_gVbo_Position_ShivaBigTamba = null;
        }

        if (AJ_gVbo_Texture_ShivaBigTamba) {
            gl.deleteBuffer(AJ_gVbo_Texture_ShivaBigTamba);
            AJ_gVbo_Texture_ShivaBigTamba = null;
        }

        if (AJ_gVbo_Normals_ShivaBigTamba) {
            gl.deleteBuffer(AJ_gVbo_Normals_ShivaBigTamba);
            AJ_gVbo_Normals_ShivaBigTamba = null;
        }

        if (AJ_gVbo_Position_ShivaLeaves) {
            gl.deleteBuffer(AJ_gVbo_Position_ShivaLeaves);
            AJ_gVbo_Position_ShivaLeaves = null;
        }

        if (AJ_gVbo_Texture_ShivaLeaves) {
            gl.deleteBuffer(AJ_gVbo_Texture_ShivaLeaves);
            AJ_gVbo_Texture_ShivaLeaves = null;
        }

        if (AJ_gVbo_Normals_ShivaLeaves) {
            gl.deleteBuffer(AJ_gVbo_Normals_ShivaLeaves);
            AJ_gVbo_Normals_ShivaLeaves = null;
        }

        if (AJ_gVbo_Position_ShivPinda) {
            gl.deleteBuffer(AJ_gVbo_Position_ShivPinda);
            AJ_gVbo_Position_ShivPinda = null;
        }

        if (AJ_gVbo_Texture_ShivPinda) {
            gl.deleteBuffer(AJ_gVbo_Texture_ShivPinda);
            AJ_gVbo_Texture_ShivPinda = null;
        }

        if (AJ_gVbo_Normals_ShivPinda) {
            gl.deleteBuffer(AJ_gVbo_Normals_ShivPinda);
            AJ_gVbo_Normals_ShivPinda = null;
        }


        if (AJ_gShaderProgramObject) {
            if (AJ_gFragmentShaderObject) {
                gl.detachShader(AJ_gShaderProgramObject, AJ_gFragmentShaderObject);
                gl.deleteShader(AJ_gFragmentShaderObject);
                AJ_gFragmentShaderObject = null;
            }

            if (AJ_gVertexShaderObject) {
                gl.detachShader(AJ_gShaderProgramObject, AJ_gVertexShaderObject);
                gl.deleteShader(AJ_gVertexShaderObject);
                AJ_gVertexShaderObject = null;
            }

            gl.deleteProgram(AJ_gShaderProgramObject);
            AJ_gShaderProgramObject = null;
        }
    }

    this.update = function () {
        AJ_Angle = AJ_Angle + 0.5;
    }

    this.draw = function () {
        gl.useProgram(AJ_gShaderProgramObject);

        var translateMatrix = mat4.create();
        var modelMatrix = mat4.create();
        var ViewMatrix = mat4.create();
        var rotationMatrix = mat4.create();
        var scaleMatrix = mat4.create();

        //Change
        if (AJ_gbLight = true) {
            gl.uniform1i(AJ_lKeyPressedUniform, 1);

            var lightAmbient = new Float32Array([0.1, 0.1, 0.1]);
            gl.uniform3fv(AJ_lightAmbientUniform, lightAmbient);

            var lightSpecular = new Float32Array([0.5, 0.5, 0.5]);
            gl.uniform3fv(AJ_lightSpecularUniform, lightSpecular);

            var lightDiffuse = new Float32Array([1.0, 1.0, 1.0]);
            gl.uniform3fv(AJ_lightDiffuseUniform, lightDiffuse);

            var LightPosition = new Float32Array([100.0, 100.0, 100.0, 1.0]);
            gl.uniform4fv(AJ_lightPositionUniform, LightPosition);

            var materialAmbient = new Float32Array([0.0, 0.0, 0.0]);
            gl.uniform3fv(AJ_materialAmbientUniform, materialAmbient);

            var materialDiffuse = new Float32Array([0.2, 0.2, 0.2]);
            gl.uniform3fv(AJ_materialDiffuseUniform, materialDiffuse);

            var materialSpecular = new Float32Array([0.1, 0.1, 0.1]);
            gl.uniform3fv(AJ_materialSpecularUniform, materialSpecular);

            var materialShininess = 10.0;
            gl.uniform1f(AJ_materialShininessUniform, materialShininess);
        }
        else {
            gl.uniform1i(AJ_lKeyPressedUniform, 0);
        }

        mat4.translate(translateMatrix, translateMatrix, [92.0, 25.0, -13.0]);
        mat4.rotateY(rotationMatrix, rotationMatrix, degtored(-45.0));
        mat4.scale(scaleMatrix, scaleMatrix, [0.1, 0.1, 0.1]);
        mat4.multiply(modelMatrix, translateMatrix, scaleMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrix);
        //mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelMatrix);
		pep_camera.Camera_UpdateVectors();
		cameraMatrix = pep_camera.Camera_GetViewMatrix();
		mat4.copy(ViewMatrix, cameraMatrix);
		
        gl.uniformMatrix4fv(AJ_modelMatrixUniform, false, modelMatrix);
        gl.uniformMatrix4fv(AJ_ViewMatrixUniform, false, ViewMatrix);
        gl.uniformMatrix4fv(AJ_prespectiveProjectionUniform, false, perspectiveProjectionMatrix);


        //Temple Base Starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_TempleBase_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_TempleBase);
        gl.drawArrays(gl.TRIANGLES, 0, templeBaseNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Temple Base Ends

        //Temple Pillars Starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_TemplePillars_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_TemplePillars);
        gl.drawArrays(gl.TRIANGLES, 0, templePillarsNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Temple Pillars Ends

        //Temple PillarsBase Starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_TemplePillarsBase_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_TemplePillarsBase);
        gl.drawArrays(gl.TRIANGLES, 0, templePillarsBaseNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Temple PillarsBase Ends

        //Temple Roof Starts

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_TempleRoof_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_TempleRoof);
        gl.drawArrays(gl.TRIANGLES, 0, templeRoofNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);

        
        
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_TempleWalls_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_TempleWalls);
        gl.drawArrays(gl.TRIANGLES, 0, templeWallsNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_TempleFlag_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_TempleFlag);
        gl.drawArrays(gl.TRIANGLES, 0, marathaFlagNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Temple Roof Ends
        //Whole Temple Ends

        //Shiva starts

        //Shiva Pinda starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_shivaPinda_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_ShivPinda);
        gl.drawArrays(gl.TRIANGLES, 0, shivPindaNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Shiva Pinda Ends

        //Shiva Base starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_shivaBase_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_ShivaBase);
        gl.drawArrays(gl.TRIANGLES, 0, shivaBaseNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Shiva Base Ends


        //Shiva Leaves starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_shivaLeaves_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_ShivaLeaves);
        gl.drawArrays(gl.TRIANGLES, 0, shivaLeavesNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Shiva Leaves Ends

        //Shiva Big Tamba starts
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, AJ_shivaBigTamba_Texture);
        gl.uniform1i(AJ_TextureSamplerUniform, 0);

        gl.bindVertexArray(AJ_gVao_ShivaBigTamba);
        gl.drawArrays(gl.TRIANGLES, 0, bigTambaNoOfTotalVerticesInModel);
        gl.bindVertexArray(null);
        //Shiva Big Tamba Ends

        //Shiva Ends

        gl.useProgram(null);
    }
}   

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}