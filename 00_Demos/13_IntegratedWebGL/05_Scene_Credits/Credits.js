
var pep_gCredits_Texture = 1;
var psm_gTitle_x_offset = 0;
var psm_gTitle_bFlicker = false;

var credits_texture = 0;
var current_credit_texture = 0;

function Credits()
{
	var pep_Credits_vs;
	var pep_Credits_fs;
	var pep_Credits_program;

	var pep_Credits_vao;
	var pep_Credits_vbo_position;
	var pep_Credits_vbo_texcoords;

	var pep_Credits_modelMatrixUniform;
	var pep_Credits_viewMatrixUniform;
	var pep_Credits_projectionMatrixUniform;
	var pep_Credits_samplerUniform;
	
	var pep_Credits_texture_1 = 0;
	var pep_Credits_texture_2 = 0;
	var pep_Credits_texture_3 = 0;
	var pep_Credits_texture_4 = 0;
	var pep_Credits_texture_5 = 0;
	var pep_Credits_texture_6 = 0;
	
	this.Init=function()
	{
		// vertex shader
		var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTexcoords;" +
		
		"out vec2 texCoords;" +
		
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		
		"void main(void)" +
		"{" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"texCoords = vTexcoords;" +
		"}";
		
		pep_Credits_vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(pep_Credits_vs, vertexShaderSourceCode);
		gl.compileShader(pep_Credits_vs);
		if(gl.getShaderParameter(pep_Credits_vs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Credits_vs);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// console.log("RTR : vertexShaderObject Compiled Successfully");
		
		// fragment shader
		var fragmentShaderSourceCode = 
		"#version 300 es" +
		"\n" +
		"precision highp float;"+
		"in vec2 texCoords;" +
		
		"uniform highp sampler2D u_texture0_sampler;" +
		
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"vec4 tempColor = texture(u_texture0_sampler, texCoords);" +
			"float gray = (tempColor.x+ tempColor.y + tempColor.z)/3.0;" +
			"FragColor =  vec4(gray , gray , gray,1.0f);" +
		"}";
		
		pep_Credits_fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(pep_Credits_fs, fragmentShaderSourceCode);
		gl.compileShader(pep_Credits_fs);
		if(gl.getShaderParameter(pep_Credits_fs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Credits_fs);
			if(erro.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// console.log("RTR : fragmentShaderObject Compiled Successfully");
		
		// shader program
		pep_Credits_program = gl.createProgram();
		gl.attachShader(pep_Credits_program, pep_Credits_vs);
		gl.attachShader(pep_Credits_program, pep_Credits_fs);
		
		// pre-link binding of shader program object with vertex shader attributes
		gl.bindAttribLocation(pep_Credits_program, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(pep_Credits_program, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexcoords");
		
		// linking
		gl.linkProgram(pep_Credits_program);
		if(!gl.getProgramParameter(pep_Credits_program, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(pep_Credits_program);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		// console.log("RTR : shaderProgramObject Linked Successfully");
		
		// get uniform location
		pep_Credits_modelMatrixUniform = gl.getUniformLocation(pep_Credits_program, "u_model_matrix");
		pep_Credits_viewMatrixUniform = gl.getUniformLocation(pep_Credits_program, "u_view_matrix");
		pep_Credits_projectionMatrixUniform = gl.getUniformLocation(pep_Credits_program, "u_projection_matrix");
		pep_Credits_samplerUniform = gl.getUniformLocation(pep_Credits_program, "u_texture0_sampler");
		
		// Load Cube Texture
		psm_Credits_texture_1 = gl.createTexture();
		psm_Credits_texture_1.image = new Image();
		psm_Credits_texture_1.image.src = "05_Scene_Credits/credit_1.png";
		psm_Credits_texture_1.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_1);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_1.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		psm_Credits_texture_2 = gl.createTexture();
		psm_Credits_texture_2.image = new Image();
		psm_Credits_texture_2.image.src = "05_Scene_Credits/credit_2.png";
		psm_Credits_texture_2.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_2);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_2.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		psm_Credits_texture_3 = gl.createTexture();
		psm_Credits_texture_3.image = new Image();
		psm_Credits_texture_3.image.src = "05_Scene_Credits/credit_3.png";
		psm_Credits_texture_3.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_3);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_3.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		psm_Credits_texture_4 = gl.createTexture();
		psm_Credits_texture_4.image = new Image();
		psm_Credits_texture_4.image.src = "05_Scene_Credits/credit_4.png";
		psm_Credits_texture_4.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_4);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_4.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		// samapta
		psm_Credits_texture_5 = gl.createTexture();
		psm_Credits_texture_5.image = new Image();
		psm_Credits_texture_5.image.src = "05_Scene_Credits/credit_5.png";
		psm_Credits_texture_5.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_5);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_5.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		// dhanyawad
		psm_Credits_texture_6 = gl.createTexture();
		psm_Credits_texture_6.image = new Image();
		psm_Credits_texture_6.image.src = "05_Scene_Credits/credit_6.png";
		psm_Credits_texture_6.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_6);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_6.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_1 = gl.createTexture();
		psm_Credits_member_texture_1.image = new Image();
		psm_Credits_member_texture_1.image.src = "05_Scene_Credits/credit_member1.png";
		psm_Credits_member_texture_1.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_1);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_1.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_2 = gl.createTexture();
		psm_Credits_member_texture_2.image = new Image();
		psm_Credits_member_texture_2.image.src = "05_Scene_Credits/credit_member2.png";
		psm_Credits_member_texture_2.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_2);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_2.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_3 = gl.createTexture();
		psm_Credits_member_texture_3.image = new Image();
		psm_Credits_member_texture_3.image.src = "05_Scene_Credits/credit_member3.png";
		psm_Credits_member_texture_3.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_3);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_3.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_4 = gl.createTexture();
		psm_Credits_member_texture_4.image = new Image();
		psm_Credits_member_texture_4.image.src = "05_Scene_Credits/credit_member4.png";
		psm_Credits_member_texture_4.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_4);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_4.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_5 = gl.createTexture();
		psm_Credits_member_texture_5.image = new Image();
		psm_Credits_member_texture_5.image.src = "05_Scene_Credits/credit_member5.png";
		psm_Credits_member_texture_5.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_5);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_5.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_6 = gl.createTexture();
		psm_Credits_member_texture_6.image = new Image();
		psm_Credits_member_texture_6.image.src = "05_Scene_Credits/credit_member6.png";
		psm_Credits_member_texture_6.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_6);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_6.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_7 = gl.createTexture();
		psm_Credits_member_texture_7.image = new Image();
		psm_Credits_member_texture_7.image.src = "05_Scene_Credits/credit_member7.png";
		psm_Credits_member_texture_7.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_7);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_7.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_8 = gl.createTexture();
		psm_Credits_member_texture_8.image = new Image();
		psm_Credits_member_texture_8.image.src = "05_Scene_Credits/credit_member8.png";
		psm_Credits_member_texture_8.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_8);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_8.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_9 = gl.createTexture();
		psm_Credits_member_texture_9.image = new Image();
		psm_Credits_member_texture_9.image.src = "05_Scene_Credits/credit_member9.png";
		psm_Credits_member_texture_9.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_9);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_9.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		psm_Credits_member_texture_10 = gl.createTexture();
		psm_Credits_member_texture_10.image = new Image();
		psm_Credits_member_texture_10.image.src = "05_Scene_Credits/credit_member10.png";
		psm_Credits_member_texture_10.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_member_texture_10);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_member_texture_10.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		psm_Credits_texture_6 = gl.createTexture();
		psm_Credits_texture_6.image = new Image();
		psm_Credits_texture_6.image.src = "05_Scene_Credits/credit_6.png";
		psm_Credits_texture_6.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, psm_Credits_texture_6);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_Credits_texture_6.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		// vertices, colors, shader attribs, vbo_square_position, vao_square initializations
		var vertices = new Float32Array(
			[
				-2.0, -1.2, 1.0,
				-2.0, 1.2, 1.0,
				2.0, 1.2, 1.0,
				2.0, -1.2, 1.0
			]
		);
		
		pep_Credits_vao = gl.createVertexArray();
		gl.bindVertexArray(pep_Credits_vao);
		
		pep_Credits_vbo_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Credits_vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		var texcoords = new Float32Array([
			//Front
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0
		]);
		
		pep_Credits_vbo_texcoords = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Credits_vbo_texcoords);
		gl.bufferData(gl.ARRAY_BUFFER, texcoords, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		gl.bindVertexArray(null);
	} // Init End
	
	this.Uninit=function()
	{
		if(pep_Credits_vao)
		{
			gl.deleteVertexArray(pep_Credits_vao);
			pep_Credits_vao = null;
		}
		
		if(pep_Credits_vbo_texcoords)
		{
			gl.deleteBuffer(pep_Credits_vbo_texcoords);
			pep_Credits_vbo_texcoords = null;
		}
		
		if(pep_Credits_vbo_position)
		{
			gl.deleteBuffer(pep_Credits_vbo_position);
			pep_Credits_vbo_position = null;
		}

		if(pep_Credits_program)
		{
			if(pep_Credits_fs)
			{
				gl.detachShader(pep_Credits_program, pep_Credits_fs);
				gl.deleteShader(pep_Credits_fs);
				pep_Credits_fs = null;
			}
			
			if(pep_Credits_vs)
			{
				gl.detachShader(pep_Credits_program, pep_Credits_vs);
				gl.deleteShader(pep_Credits_vs);
				pep_Credits_vs = null;
			}
			
			gl.deleteProgram(pep_Credits_program);
			pep_Credits_program = null;
		}
	}
	
	this.Resize=function(width, height)
	{
	}
	
	this.Update=function()
	{
		if (psm_gTitle_bFlicker) {
			psm_gTitle_x_offset += 0.0005;
			if (psm_gTitle_x_offset > 0.03) {
				psm_gTitle_bFlicker = false
			}
		} else {
			psm_gTitle_x_offset -= 0.0005;
			if (psm_gTitle_x_offset < -0.03) {
				psm_gTitle_bFlicker = true
			}
		}
		
	}
	
	this.Draw=function()
	{
		gl.useProgram(pep_Credits_program);
	
		var modelMatrix = mat4.create();
		var viewMatrix = mat4.create();
		var projectionMatrix = mat4.create();

		mat4.identity(modelMatrix);
		mat4.identity(viewMatrix);
		mat4.identity(projectionMatrix);
		mat4.translate(modelMatrix, modelMatrix, [psm_gTitle_x_offset, 0.0, -3.0]);
		mat4.copy(projectionMatrix, perspectiveProjectionMatrix);

		gl.uniformMatrix4fv(pep_Credits_modelMatrixUniform, false, modelMatrix);
		gl.uniformMatrix4fv(pep_Credits_viewMatrixUniform, false, viewMatrix);
		gl.uniformMatrix4fv(pep_Credits_projectionMatrixUniform, false, projectionMatrix);
		gl.uniform1i(pep_Credits_samplerUniform, 0);
		
		
		if(1 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_texture_1;
			current_credit_texture = 1;
		}
		else if(2 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_texture_2;
			current_credit_texture = 2;
		}
		else if(3 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_texture_3;
			current_credit_texture = 3;
		}
		else if(4 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_texture_4;
		}
		else if(5 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_1;
		}
		else if(6 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_2;
		}
		else if(7 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_3;
		}
		else if(8 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_4;
		}
		else if(9 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_5;
		}
		else if(10 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_6;
		}
		else if(11 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_7;
		}
		else if(12 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_8;
		}
		else if(13 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_9;
		}
		else if(14 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_member_texture_10;
		}
		else if(15 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_texture_5;
		}
		else if(16 == pep_gCredits_Texture)
		{
			credits_texture = psm_Credits_texture_6;
		}
		
		gl.bindTexture(gl.TEXTURE_2D, /*pep_Credits_texture_1*/credits_texture);
		gl.bindVertexArray(pep_Credits_vao);
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		gl.bindVertexArray(null);
		gl.bindTexture(gl.TEXTURE_2D, null);

		gl.useProgram(null);
		this.Update();
	}
}

