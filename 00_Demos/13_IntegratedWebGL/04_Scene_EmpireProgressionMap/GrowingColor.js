function GrowingColor_JSS()
{
    var vertexDefaultShaderObject_JSS ;
    var fragmentDefaultShaderObject_JSS ;
    var shaderDefualtProgramObject_JSS ;


    var vertexfbShaderObject_expandingColor_JSS ;
    var fragmentfbShaderObject_expandingColor_JSS ;
    var shaderfbProgramObject_expandingColor_JSS ;

    var vao_JSS ;
    var vbo_Position_JSS  ;
    var vbo_texture_JSS ;
    
    var vao_PSM ;
    var vbo_position_background_PSM;
    var vbo_background_texture_PSM;

    var voa_square_JSS  ;
    var vbo_square_position_JSS ; 
    var vbo_square_texture_JSS ; 
    
    var mvpUniform_1_JSS ;
    var textureSamplerUniform_1_JSS ;

    var modelUniform_2_JSS ;
    var viewUniform_2_JSS ;
    var projectionUniform_2_JSS ;
    var textureSamplerUniform_2_JSS ;
    var mapSamplerUniform_2_JSS;
    var mapSamplerUniform_redmap_JSS ;
    var frameBufferDimentionUniform_JSS ;

    //Texture Code
    var frameUniform_JSS ;
    var timerUniform_JSS ;


    var framebuffer1;
    var textureColorbuffer1;
    var framebuffer2;
    var  textureColorbuffer2;

    var frameFlag = 0 ;

    var map_texture_JSS ;
    var redmap_texture ;
    var psm_bg_texture;

    var  firstCameraMovement = true;
    var  secondCameraMovement = true;


    this.Init=function()
    {
        cameraPosition_JSS = new Float32Array([-0.25, 0.0, -0.3]);

        var vertexShaderSourceCode_JSS =
        "#version 300 es"+
        "\n" +
        "in vec4 vPosition;" +
        "in vec2 vTexCoord;" +
        "uniform mat4 u_modelmatrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "out vec2 out_texcoord;" +
        "void main(void)" +
        "{" +
            "gl_Position  = u_projection_matrix * u_view_matrix * u_modelmatrix  * vPosition;" +
            "out_texcoord = vTexCoord;" +
        "}";

    vertexDefaultShaderObject_JSS = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexDefaultShaderObject_JSS,vertexShaderSourceCode_JSS);
    gl.compileShader(vertexDefaultShaderObject_JSS);
    if(gl.getShaderParameter(vertexDefaultShaderObject_JSS,gl.COMPILE_STATUS) == false)
    {
        var error_JSS =  gl.getShaderInfoLog(vertexDefaultShaderObject_JSS);
        if(error_JSS.length > 0)
        {
            alert(error_JSS);
            uninitialize();
        }
    }

    //Fragment Shader

    fragmentDefaultShaderObject_JSS = gl.createShader(gl.FRAGMENT_SHADER);

    var fragmentShaderSourceCode_JSS = 
        "#version 300 es"+
        "\n"+
        "precision highp float;"+
        "\n" +
        "in vec2 out_texcoord ;" +
        "uniform sampler2D u_texture_sampler;" +
        "uniform sampler2D u_map_sampler;" +
        "uniform sampler2D u_red_map_sampler;" +
        "out vec4 FragColor ;" +
        "void main(void)" +
        "{" +
        "vec4 ecolor = texture(u_texture_sampler,out_texcoord);" +
        "vec4 mapcolor = texture(u_map_sampler,out_texcoord) ;" +
        "vec4 redMapColor = texture(u_red_map_sampler,out_texcoord) ;"+
        "vec4 tempColor;"+
        "if(ecolor == vec4(1.0,0.0,0.0,1.0) && redMapColor == vec4(1.0,0.0,0.0,1.0))"+
        "{"+
			"tempColor = vec4(1.0,0.549,0.0,1.0);"+
        "}"+
        "else"+
        "{"+
            "float gray = (mapcolor.x+ mapcolor.y + mapcolor.z)/3.0;"+
            "tempColor = vec4(gray , gray , gray,1.0f);"+
        "}"+
        "if(tempColor.s < 0.1)"+
        "{"+
            "discard;"+
        "}"+
        "FragColor = tempColor;" +
        "}"
       ;

    gl.shaderSource(fragmentDefaultShaderObject_JSS,fragmentShaderSourceCode_JSS);
    gl.compileShader(fragmentDefaultShaderObject_JSS);

    if(gl.getShaderParameter(fragmentDefaultShaderObject_JSS,gl.COMPILE_STATUS) == false)
    {
        var error_JSS =  gl.getShaderInfoLog(fragmentDefaultShaderObject_JSS);
        if(error_JSS.length > 0)
        {
            alert(error_JSS);
            uninitialize();
        }
    }

    //shader program

    shaderDefualtProgramObject_JSS = gl.createProgram();
    gl.attachShader(shaderDefualtProgramObject_JSS,vertexDefaultShaderObject_JSS);
    gl.attachShader(shaderDefualtProgramObject_JSS,fragmentDefaultShaderObject_JSS);

    gl.bindAttribLocation(shaderDefualtProgramObject_JSS,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(shaderDefualtProgramObject_JSS,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord");
    gl.linkProgram(shaderDefualtProgramObject_JSS);
    if(!gl.getProgramParameter(shaderDefualtProgramObject_JSS,gl.LINK_STATUS))
    {
        var error_JSS =  gl.getProgramInfoLog(shaderDefualtProgramObject_JSS);
        if(error_JSS.length > 0)
        {
            alert(error_JSS);
            uninitialize();
        }
    }

    modelUniform_2_JSS =  gl.getUniformLocation(shaderDefualtProgramObject_JSS,
        "u_modelmatrix");

    viewUniform_2_JSS =  gl.getUniformLocation(shaderDefualtProgramObject_JSS,
            "u_view_matrix");

    projectionUniform_2_JSS =  gl.getUniformLocation(shaderDefualtProgramObject_JSS,
                "u_projection_matrix");
    
    textureSamplerUniform_2_JSS =  gl.getUniformLocation(shaderDefualtProgramObject_JSS,
        "u_texture_sampler");

    mapSamplerUniform_2_JSS = gl.getUniformLocation(shaderDefualtProgramObject_JSS,
            "u_map_sampler");
        
    mapSamplerUniform_redmap_JSS = gl.getUniformLocation(shaderDefualtProgramObject_JSS,
            "u_red_map_sampler");
        


    var squareVertices = new Float32Array([ 1.0,1.0,0.0,
                                            -1.0,1.0,0.0,
                                            -1.0,-1.0,0.0,
                                            1.0,-1.0,0.0]);


    var square_TexCoord_JSS = new Float32Array([1.0,1.0,
                                                0.0,1.0,
                                                0.0,0.0,
                                                1.0,0.0]);                                        

                                            
    voa_square_JSS = gl.createVertexArray();
    gl.bindVertexArray(voa_square_JSS);
    
    vbo_square_position_JSS = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_position_JSS);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,
        false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    vbo_square_texture_JSS = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_square_texture_JSS);
    gl.bufferData(gl.ARRAY_BUFFER,square_TexCoord_JSS,gl.STATIC_DRAW);    
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,
        false,0,0);   
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);    

    gl.bindVertexArray(null);

    //Texture
    map_texture_JSS = gl.createTexture();
    map_texture_JSS.image = new Image();
    map_texture_JSS.image.src = "04_Scene_EmpireProgressionMap/indiamap.png";
    map_texture_JSS.image.onload =  function()
    {
        gl.bindTexture(gl.TEXTURE_2D,map_texture_JSS);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,1);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.LINEAR_MIPMAP_NEAREST);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, map_texture_JSS.image);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D , null);
        
    }; 
    
    redmap_texture = gl.createTexture();
    redmap_texture.image = new Image();
    redmap_texture.image.src = "04_Scene_EmpireProgressionMap/indiamap_red.png";
    redmap_texture.image.onload =  function()
    {
        gl.bindTexture(gl.TEXTURE_2D,redmap_texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,1);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.LINEAR_MIPMAP_NEAREST);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, redmap_texture.image);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D , null);
        
    };

    psm_bg_texture = gl.createTexture();
    psm_bg_texture.image = new Image();
    psm_bg_texture.image.src = "04_Scene_EmpireProgressionMap/bg_texture.png";
    psm_bg_texture.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, psm_bg_texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, psm_bg_texture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    frameBufferShader();
    FrameBufferInitialization();
    
    }

    this.Draw=function()
    {
        var red  = new Float32Array([ 0.0, 0.0, 0.0, 1.0 ]);
        var depth = new Float32Array([1.0]);

        gl.useProgram(shaderfbProgramObject_expandingColor_JSS);
		gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer1);
		gl.clearBufferfv(gl.COLOR, 0,red);
        gl.clearBufferfv(gl.DEPTH, 0,depth);
        RenderToTextureInnerSquare_1();
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.useProgram(null);
		
        gl.bindFramebuffer(gl.READ_FRAMEBUFFER, framebuffer1);
        gl.bindFramebuffer(gl.DRAW_FRAMEBUFFER, framebuffer2);
        gl.blitFramebuffer(0, 0, canvas.width , canvas.height , 0, 0, canvas.width , canvas.height, gl.COLOR_BUFFER_BIT, gl.LINEAR);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		gl.useProgram(null);
        //gl.bindTexture(gl.TEXTURE_2D, null);
		
		RenderMainCube();
    }

    this.Uninit=function()
    {
		if(framebuffe1)
		{
			gl.deleteFramebuffer(framebuffe1);
			framebuffe1 = null;
		}
		
		if(framebuffe2)
		{
			gl.deleteFramebuffer(framebuffe2);
			framebuffe2 = null;
		}
			
		
        if(vbo_Position_JSS != null)
        {
            gl.deleteBuffers(vbo_Position_JSS);
            vbo_Position_JSS = null ;
        }
        
        if(vbo_texture_JSS)
        {
            gl.deleteBuffers(vbo_texture_JSS);
            vbo_texture_JSS = null ;
        }
    
    
        if(voa_square_JSS)
        {
            gl.deleteVertexArray(voa_square_JSS);
            voa_square_JSS = null ;
        }
        if(vbo_square_position_JSS)
        {
            gl.deleteBuffers(vbo_square_position_JSS);
            vbo_square_position_JSS = null ;
        }
        if(vbo_square_texture_JSS)
        {
            gl.deleteBuffers(vbo_square_texture_JSS);
            vbo_square_texture_JSS = null ;
        }

        if(vao_PSM)
        {
            gl.deleteVertexArray(vao_PSM);
            vao_PSM = null ;
        }

        if(vbo_position_background_PSM)
        {
            gl.deleteBuffers(vbo_position_background_PSM);
            vbo_position_background_PSM = null ;
        }

        if(vbo_background_texture_PSM)
        {
            gl.deleteBuffers(vbo_background_texture_PSM);
            vbo_background_texture_PSM = null ;
        }

        if(shaderDefualtProgramObject_JSS)
        {
            if(fragmentDefaultShaderObject_JSS)
            {
                gl.detachShader(shaderDefualtProgramObject_JSS,fragmentDefaultShaderObject_JSS);
                gl.deleteShader(fragmentDefaultShaderObject_JSS);
                fragmentDefaultShaderObject_JSS =  null ;
            }
            if(vertexDefaultShaderObject_JSS)
            {
                gl.detachShader(shaderDefualtProgramObject_JSS,vertexDefaultShaderObject_JSS);
                gl.deleteShader(vertexDefaultShaderObject_JSS);
                vertexDefaultShaderObject_JSS =  null ;
            }
            gl.deleteProgram(shaderDefualtProgramObject_JSS);
            shaderDefualtProgramObject_JSS = null ;
        }

        if(shaderfbProgramObject_expandingColor_JSS)
        {
            if(fragmentfbShaderObject_expandingColor_JSS)
            {
                gl.detachShader(shaderfbProgramObject_expandingColor_JSS,fragmentfbShaderObject_expandingColor_JSS);
                gl.deleteShader(fragmentfbShaderObject_expandingColor_JSS);
                fragmentfbShaderObject_expandingColor_JSS =  null ;
            }
            if(vertexfbShaderObject_expandingColor_JSS)
            {
                gl.detachShader(shaderfbProgramObject_expandingColor_JSS,vertexfbShaderObject_expandingColor_JSS);
                gl.deleteShader(vertexfbShaderObject_expandingColor_JSS);
                vertexfbShaderObject_expandingColor_JSS =  null ;
            }
            gl.deleteProgram(shaderfbProgramObject_expandingColor_JSS);
            shaderfbProgramObject_expandingColor_JSS = null ;
        }

    }

    function frameBufferShader()
    {
        var vertexShaderSourceCode_JSS =
        "#version 300 es"+
         "\n"+
        "in vec4 vPosition;" +
        "in vec2 vTexCoord;" +
        "uniform mat4 u_mvpmatrix;" +
        "out vec2 out_texcoord;" +
        "void main(void)" +
        "{" +
            "gl_Position = u_mvpmatrix * vPosition ;" +
            "out_texcoord = vTexCoord;" +
        "}";
    
        vertexfbShaderObject_expandingColor_JSS = gl.createShader(gl.VERTEX_SHADER);
        gl.shaderSource(vertexfbShaderObject_expandingColor_JSS,vertexShaderSourceCode_JSS);
        gl.compileShader(vertexfbShaderObject_expandingColor_JSS);
        if(gl.getShaderParameter(vertexfbShaderObject_expandingColor_JSS,gl.COMPILE_STATUS) == false)
        {
            var error_JSS =  gl.getShaderInfoLog(vertexfbShaderObject_expandingColor_JSS);
            if(error_JSS.length > 0)
            {
                alert(error_JSS);
                uninitialize();
            }
        }
    
    
        //Fragment Shader
    
        fragmentfbShaderObject_expandingColor_JSS = gl.createShader(gl.FRAGMENT_SHADER);
    
        var fragmentShaderSourceCode_JSS = 
            "#version 300 es"+
            "\n"+
            "precision highp float;\n"+
            "in vec2 out_texcoord;\n"+
        "uniform sampler2D u_texture_sampler;\n"+
        "uniform float u_timer;\n"+
        "uniform int u_frame;\n"+
        "uniform vec2 u_dimention;\n"+
        "out vec4 FragColor;\n"+
        "vec4 hash43(vec3 p)\n"+
        "{\n"+
            "vec4 p4 = fract(vec4(p.xyzx)  * vec4(.1031, .1030, .0973, .1099));\n"+
        
            "p4 += dot(p4, p4.wzxy+33.33);\n"+
        
            "return fract((p4.xxyz+p4.yzzw)*p4.zywx);\n"+
        "}\n"+
        "void test(inout vec4 id, ivec2 x)\n"+
        "{\n"+
            "vec4 next = texelFetch(u_texture_sampler, x, 0);\n"+
            "if (next.x == 0.)\n"+
            "return;\n"+
            "vec3 pos = vec3(x,u_timer);\n"+
            "vec4 blob = hash43(pos);\n"+
            "if (blob.w > 0.2)\n"+
            "return;\n"+
            "id = vec4(1.0,0.0,0.0,1.0);\n"+
        "}\n"+
        "void main(void)\n"+
        "{\n"+
            "if (u_frame == 0)\n"+
            "{\n"+
                "ivec2 xy = ivec2(gl_FragCoord.xy) - ivec2(u_dimention.x/6.5 , u_dimention.y/4.0);\n"+
                "FragColor = vec4(0);\n"+
                "if (xy == ivec2(0,0))\n"+
                    "FragColor = vec4(0.5, 0.5, 0.5, 1);\n"+
                "return;\n"+
            "}\n"+
            "ivec2 x = ivec2(gl_FragCoord);\n"+
      
            "vec4 id = texelFetch(u_texture_sampler, x , 0);\n"+
            "test(id, x+ivec2(0, 1));\n"+
            "test(id, x+ivec2(1, 0));\n"+
            "test(id, x-ivec2(0, 1));\n"+
            "test(id, x-ivec2(1, 0));\n"+
            "FragColor = id;\n"+
        "}"
        ;
    
        gl.shaderSource(fragmentfbShaderObject_expandingColor_JSS,fragmentShaderSourceCode_JSS);
        gl.compileShader(fragmentfbShaderObject_expandingColor_JSS);
    
        if(gl.getShaderParameter(fragmentfbShaderObject_expandingColor_JSS,gl.COMPILE_STATUS) == false)
        {
            var error_JSS =  gl.getShaderInfoLog(fragmentfbShaderObject_expandingColor_JSS);
            if(error_JSS.length > 0)
            {
                alert(error_JSS);
                uninitialize();
            }
        }
    
        //shader program
    
        shaderfbProgramObject_expandingColor_JSS = gl.createProgram();
        gl.attachShader(shaderfbProgramObject_expandingColor_JSS,vertexfbShaderObject_expandingColor_JSS);
        gl.attachShader(shaderfbProgramObject_expandingColor_JSS,fragmentfbShaderObject_expandingColor_JSS);
    
        gl.bindAttribLocation(shaderfbProgramObject_expandingColor_JSS,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
        gl.bindAttribLocation(shaderfbProgramObject_expandingColor_JSS,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord");
        gl.linkProgram(shaderfbProgramObject_expandingColor_JSS);
        if(!gl.getProgramParameter(shaderfbProgramObject_expandingColor_JSS,gl.LINK_STATUS))
        {
            var error_JSS =  gl.getProgramInfoLog(shaderfbProgramObject_expandingColor_JSS);
            if(error_JSS.length > 0)
            {
                alert(error_JSS);
                uninitialize();
            }
        }
    
    
        mvpUniform_1_JSS = gl.getUniformLocation(shaderfbProgramObject_expandingColor_JSS
            ,"u_mvpmatrix");
        
        textureSamplerUniform_1_JSS = gl.getUniformLocation(shaderfbProgramObject_expandingColor_JSS,
            "u_texture_sampler"); 
        
        frameBufferDimentionUniform_JSS = gl.getUniformLocation(shaderfbProgramObject_expandingColor_JSS,
            "u_dimention"); 
        
        timerUniform_JSS = gl.getUniformLocation(shaderfbProgramObject_expandingColor_JSS,
                "u_timer"); 
        
        frameUniform_JSS  = gl.getUniformLocation(shaderfbProgramObject_expandingColor_JSS,
                "u_frame");
    
    
        var squareVertices = new Float32Array([ 2.0,2.0,0.0,
                    -2.0,2.0,0.0,
                    -2.0,-2.0,0.0,
                    2.0,-2.0,0.0]);

        var squareBackgroundVertices_PSM = new Float32Array([ 4.0,4.0,0.0,
                        -4.0,4.0,0.0,
                        -4.0,-4.0,0.0,
                        4.0,-4.0,0.0]);
            
        var square_TexCoord_JSS = new Float32Array([1.0,1.0,
                    0.0,1.0,
                    0.0,0.0,
                    1.0,0.0]);
    
        vao_PSM = gl.createVertexArray();
        gl.bindVertexArray(vao_PSM);
        vbo_position_background_PSM = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_background_PSM);
        gl.bufferData(gl.ARRAY_BUFFER,squareBackgroundVertices_PSM,gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,
        false,0,0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER,null);

        vbo_background_texture_PSM = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,vbo_background_texture_PSM);
        gl.bufferData(gl.ARRAY_BUFFER,square_TexCoord_JSS,gl.STATIC_DRAW);    
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,
            false,0,0);   
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER,null);

        gl.bindVertexArray(null);

        vao_JSS = gl.createVertexArray();
        gl.bindVertexArray(vao_JSS);
        
        vbo_Position_JSS = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Position_JSS);
        gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,
        false,0,0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
        gl.bindBuffer(gl.ARRAY_BUFFER,null);
                
        vbo_texture_JSS = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture_JSS);
        gl.bufferData(gl.ARRAY_BUFFER,square_TexCoord_JSS,gl.STATIC_DRAW);    
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,
            false,0,0);   
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
        gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
        gl.bindVertexArray(null);
                            
    
    }
	
	function degToRad(angle)
	{
		return(angle*Math.PI/180.0);
	}

    function RenderMainCube()
    {
        gl.clearColor(0.8274, 0.8274, 0.8274, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
        gl.useProgram(shaderDefualtProgramObject_JSS);

        cameraMatrix = cameraPositionChangeJSS(); 
    
        var modelMatrix_JSS = mat4.create();
        var translationMatrix_JSS =  mat4.create();
		var rotationMatrix = mat4.create();
        var ProjectionMatrix_JSS = mat4.create();
        mat4.identity(ProjectionMatrix_JSS);
        mat4.identity(translationMatrix_JSS);
        mat4.identity(rotationMatrix);
        var viewMatrix = mat4.create();
        mat4.identity(viewMatrix);
    
    
        mat4.translate(translationMatrix_JSS,translationMatrix_JSS,[0.0,0.1,-0.7]);
		mat4.rotateX(rotationMatrix,rotationMatrix,degToRad(-50));
        mat4.copy(viewMatrix, cameraMatrix);
        mat4.multiply(modelMatrix_JSS,translationMatrix_JSS,translationMatrix_JSS);
		mat4.multiply(modelMatrix_JSS,translationMatrix_JSS,rotationMatrix);
        mat4.copy(ProjectionMatrix_JSS,perspectiveProjectionMatrix);
       
        gl.uniformMatrix4fv(projectionUniform_2_JSS,false,ProjectionMatrix_JSS);

        gl.uniformMatrix4fv(modelUniform_2_JSS, false, modelMatrix_JSS);
	    gl.uniformMatrix4fv(viewUniform_2_JSS, false, viewMatrix);
        
    
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureColorbuffer1);
        gl.uniform1i(textureSamplerUniform_2_JSS, 0);
        
        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, map_texture_JSS);
        gl.uniform1i(mapSamplerUniform_2_JSS, 1);

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D, redmap_texture );
        gl.uniform1i(mapSamplerUniform_redmap_JSS, 2);

        gl.bindVertexArray(voa_square_JSS);    
        gl.drawArrays(gl.TRIANGLE_FAN,0,4);
        gl.bindVertexArray(null);

        gl.bindTexture(gl.TEXTURE_2D, null);
		gl.bindTexture(gl.TEXTURE_2D, null);

        mat4.identity(ProjectionMatrix_JSS);
        mat4.identity(translationMatrix_JSS);
        mat4.identity(rotationMatrix);

        mat4.translate(translationMatrix_JSS,translationMatrix_JSS,[0.0,0.1,-0.705]);
        mat4.rotateX(rotationMatrix,rotationMatrix,degToRad(-50));
        mat4.copy(viewMatrix, cameraMatrix);
        mat4.multiply(modelMatrix_JSS,translationMatrix_JSS,translationMatrix_JSS);
		mat4.multiply(modelMatrix_JSS,translationMatrix_JSS,rotationMatrix);
        mat4.copy(ProjectionMatrix_JSS,perspectiveProjectionMatrix);
       
        gl.uniformMatrix4fv(projectionUniform_2_JSS,false,ProjectionMatrix_JSS);

        gl.uniformMatrix4fv(modelUniform_2_JSS, false, modelMatrix_JSS);
	    gl.uniformMatrix4fv(viewUniform_2_JSS, false, viewMatrix);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, psm_bg_texture);
        gl.uniform1i(mapSamplerUniform_2_JSS, 1);
        

        gl.bindVertexArray(vao_PSM);    
        gl.drawArrays(gl.TRIANGLE_FAN,0,4);
        gl.bindVertexArray(null);

        gl.bindTexture(gl.TEXTURE_2D, null);
		gl.bindTexture(gl.TEXTURE_2D, null);
        gl.useProgram(null);
    
    }

    function cameraPositionChangeJSS()
    {
       
        pep_camera.Camera_SetPosition(cameraPosition_JSS);
       // cameraPosition_JSS[0] = cameraPosition_JSS[0] + 0.001 ;
       if(cameraPosition_JSS[2] < 0.0 && firstCameraMovement == true)
       {
            cameraPosition_JSS[2] = cameraPosition_JSS[2] + 0.000307 ;
       }
       else if(cameraPosition_JSS[1] < 0.5 &&  secondCameraMovement == true )
       {
        firstCameraMovement = false ;
        if(cameraPosition_JSS[2] > - 0.55)
        {
            cameraPosition_JSS[2] = cameraPosition_JSS[2] -0.000907 ;
        }
        cameraPosition_JSS[1] = cameraPosition_JSS[1] + 0.000307 ;
       }
       else if(cameraPosition_JSS[2] < 1.0)
       {
        secondCameraMovement = false ;
        cameraPosition_JSS[1] = cameraPosition_JSS[1] - 0.0001507;
        cameraPosition_JSS[2] = cameraPosition_JSS[2]  + 0.000807;
        cameraPosition_JSS[0] = cameraPosition_JSS[0]  + 0.000107;

       }
        return( pep_camera.Camera_GetViewMatrix());

    }

    function RenderToTextureInnerSquare_1()
    {
      
        var modelViewMatrix_JSS = mat4.create();
        var translationMatrix_JSS =  mat4.create();
        var modelViewProjectionMatrix_JSS = mat4.create();
    
        mat4.translate(translationMatrix_JSS,translationMatrix_JSS,[0.0,0.0,-1.0]);
        mat4.multiply(modelViewMatrix_JSS,translationMatrix_JSS,translationMatrix_JSS);
        mat4.multiply(modelViewProjectionMatrix_JSS,perspectiveProjectionMatrix,modelViewMatrix_JSS);
        gl.uniformMatrix4fv(mvpUniform_1_JSS,false,modelViewProjectionMatrix_JSS);
        gl.uniform2f(frameBufferDimentionUniform_JSS, canvas.width,canvas.height);
        
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureColorbuffer2);
        gl.uniform1i(textureSamplerUniform_1_JSS, 0);
    
    
        if(frameFlag == 0)
        {
            frameFlag = 1 ;
            gl.uniform1i(frameUniform_JSS,0);
        }
        else
        {
            gl.uniform1i(frameUniform_JSS,1);
        }
    
        gl.uniform1f(timerUniform_JSS,jss_Global_GrowingColor_Timer);
    
        gl.bindVertexArray(vao_JSS);    
        gl.drawArrays(gl.TRIANGLE_FAN,0,4);
        gl.bindVertexArray(null);
		
		gl.bindTexture(gl.TEXTURE_2D, null);
        
		if(false == jss_Global_GrowingColor_FadeOut)
		{
			if(jss_Global_GrowingColor_Timer < 0.1)
			{
				jss_Global_GrowingColor_Timer = jss_Global_GrowingColor_Timer + 0.0000307;
				//// console.log(" " + jss_Global_GrowingColor_Timer + " ");
			}
			else
			{
				// console.log("jss_Global_GrowingColor_FadeOut ");
				jss_Global_GrowingColor_FadeOut = true;
			}
		}
    }
    
    function FrameBufferInitialization()
    {
        
        
    
        // define size and format of level 0
        const level = 0;
        const internalFormat = gl.RGBA;
         const border = 0;
        const format = gl.RGBA;
        const type = gl.UNSIGNED_BYTE;
        const data = null;
    
    
        framebuffer1 = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer1);
        // create a color attachment texture
        textureColorbuffer1 = gl.createTexture();   
        gl.bindTexture(gl.TEXTURE_2D, textureColorbuffer1);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
            canvas.width,canvas.height, border,
            format, type, data);
    
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        
        const attachmentPoint = gl.COLOR_ATTACHMENT0;
        gl.framebufferTexture2D(
            gl.FRAMEBUFFER, attachmentPoint, gl.TEXTURE_2D, textureColorbuffer1, level);
    
        
            
        framebuffer2 = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer2);
        
        textureColorbuffer2 = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, textureColorbuffer2);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
            canvas.width,canvas.height, border,
            format, type, data);
        
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);    
        gl.framebufferTexture2D(
            gl.FRAMEBUFFER, attachmentPoint, gl.TEXTURE_2D, textureColorbuffer2, level);
        
        
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }
    
}
