//
// html and js file should be in one directory
//
// onload function

//
// fullscreen:
// every browser has its own elements for fullscreen.
// 

var pep_Global_Scene = 0;
var pep_Global_FadeOut = false;
var jss_Global_GrowingColor_Timer= 0.0 ;
var jss_Global_GrowingColor_FadeOut = false;
var ts_Global_Scene3Gallery_FadeOut = false;
var ts_Global_Scene2Pratidnya_FadeOut = false;

var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;
var audioControl = null;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

var perspectiveProjectionMatrix;

var requestAnimationFrame =
	window.requestAnimationFrame || /*Google Chrome*/
	window.webkitRequestAnimationFrame || /*Apple - Safari*/
	window.mozRequestAnimationFrame || /*Mozilla Firefox*/
	window.oRequestAnimationFrame || /*Opera*/
	window.msRequestAnimationFrame; /*Microsoft - internet explorer*/

var pep_SceneTransition = null;
var pep_Title = null;
var pep_Credits = null;
var jss_GrowingColor = null;
var ts_Scene3Gallery = null;
var pep_Terrain = null;
var templeAndShivPinda;
var pep_camera = null;
var pep_Skybox = null;

var cameraPosition = new Float32Array([105.0, 50.0, 100.0]);
var cameraFront = new Float32Array([0.0, 0.0, 0.0]);
var cameraUp = new Float32Array([0.0, 1.0, 0.0]);
var cameraMatrix;


// var initCameraPosition = new Float32Array([ 85.7978286743164,30,-28.739490509033203]);
var initCameraPosition = new Float32Array([82.0478286743164,30, -4.0/*-24.989490509033203*/]);
var initCameraFront = new Float32Array([ 0.5735764396905743, 0, -0.3191520419506291]);
var initCameraUp = new Float32Array([0.0, 1.0, 0.0]);

var cameraPosition_Final = new Float32Array([80, 50, 27.5]);

var cameraPosition_Mid = new Float32Array([56.401947021484375,30,13.242030143737793]);
var initCameraFront_Mid = new Float32Array([ 0.5735764396905743, 0, -0.8191520419506291]);

var pratidnyaCounter = 0;


function main()
{
	// step 1: Get canvas from DOM (i.e html)
	// var => runtime la type decide hoil right hind side nusar type (Type-Infurance)
	// document => inbuild variable (by DOM)
	audioControl = document.getElementById("audioControl");
	canvas = document.getElementById("PEP_CANVAS");
	
	if(!canvas)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		// // console.log("RTR: Obtaining Canvas Failed\n");
	}
	else
	{
		// // console.log("RTR: Obtaining Canvas Successed\n");
	}

	// step 2: Retrive width and height of canvas for a seak of information
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//
	// window=> is inbuild variable
	//
	
	//register event handlers
	
	// 3rd parameter is false => bubbled propogation
	// capture propogation => sent to our event listener then to other
	// bubbled propogation =>  event sent from bottom to super class
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// toggleFullScreen();
	init();

	//warm-up resize call
	resize(); // till now windows, xwindows, android we have just call resize as warm-up call. but in javascript no repaint event hence need to call warm-up draw call.
	//toggleFullScreen();
	//warm-up draw call
	draw();
}

// browser independ code
function toggleFullScreen()
{
	

	var fullscreen_element =
	document.fullscreenElement || /*Goolge Chrome, Opera*/
	document.webkitFullscreenElement || /*Apple - Safari*/
	document.mozFullScreenElement || /*Mozilla Firefox*/
	document.msFullscreenElement || /* Microsoft - Internet Explore*/
	null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();
		}
		else if(canvas.webkitRequestFullscreen)
		{
			canvas.webkitRequestFullscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		}
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
		audioControl.play();
		pep_Global_Scene += 1;
		// audioControl.play();
	}
	else // already fullscreen
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}
}

function keyDown(event)
{
	switch(event.keyCode)
	{ 
		case 27: // Escape
			uninitialize();
			audioControl.pause();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
		
		case 65: // 'A' CAMERA_LEFT
			pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_LEFT, 0.5);
		break;
		
		case 68: // 'D' CAMERA_RIGHT
			pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_RIGHT, 0.5);
		break;
		
		case 87: // 'W' CAMERA_FORWARD
			pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_FORWARD, 0.5);
		break;
		
		case 83: // 'S' CAMERA_BACKWARD
			pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_BACKWARD, 0.5);
		break;
		
		case 70: //for 'f' or 'F'
			toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	// alert("Mouse Is Clicked");
}

function init()
{
	// step 3: Get Drawing Context From Canvas
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		// // console.log("RTR: Failed To Get The Rendering Context For WebGL\n");
		return;
	}

	// // console.log("RTR: Get The Rendering Context For WebGL\n");

	// following two statements are specific to webgl context not avaliable in windows opengl context nighter xwindows nor android
	gl.viewportWidth = canvas.width; 
	gl.viewportHeight = canvas.height;

	pep_camera = new Camera();
	pep_camera.Camera_Init(cameraPosition, cameraFront, cameraUp);
	
	pep_SceneTransition = new SceneTransition();
	pep_SceneTransition.Init();
	
	pep_Title = new Title();
	pep_Title.Init();
	
	pep_Terrain = new Terrain();
	pep_Terrain.Init();

	pep_Skybox = new Skybox();
	pep_Skybox.Init();
	
	templeAndShivPinda=new TempleAndShivPinda();
	templeAndShivPinda.init();
	
	ts_Scene3Gallery = new Scene2PointLight();
	ts_Scene3Gallery.Init();
	
	jss_GrowingColor = new GrowingColor_JSS();
	jss_GrowingColor.Init();
	
	pep_Credits = new Credits();
	pep_Credits.Init();
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		//
		//
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 1000.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	cameraMatrix = pep_camera.Camera_GetViewMatrix();
	if(0 == pep_Global_Scene) // Dummy Scene
	{
		
	}
	else if(1 == pep_Global_Scene) // Title Scene
	{
		pep_Title.Draw();
		
		if (pep_Global_FadeOut)
		{
			pep_SceneTransition.Draw(true, 0.0065); // Scene To Black
		}
		else
		{
			pep_SceneTransition.Draw(false, -0.0055); // Black To Scene
		}
	}
	else if(2 == pep_Global_Scene)
	{

		pep_Skybox.Draw();
		pep_Terrain.Draw();
		templeAndShivPinda.draw();
		if (ts_Global_Scene2Pratidnya_FadeOut && pep_Global_FadeOut)
		{
			pep_SceneTransition.Draw(true, 0.006); // Scene To Black
		}
		else
		{
			pep_SceneTransition.Draw(false, -0.005); // Black To Scene
		}
	}
	else if(3 == pep_Global_Scene)
	{
		ts_Scene3Gallery.Draw();

		if(true == ts_Global_Scene3Gallery_FadeOut)
		{
		  if (pep_Global_FadeOut)
		  {
		  	pep_SceneTransition.Draw(true, 0.005); // Scene To Black
		  }
		}
		else if(false == ts_Global_Scene3Gallery_FadeOut && false == pep_Global_FadeOut)
		{
			pep_SceneTransition.Draw(false, -0.005); // Black To Scene
		}
	}
	else if(4 == pep_Global_Scene)
	{
		jss_GrowingColor.Draw();
		
		if(false == jss_Global_GrowingColor_FadeOut && false == pep_Global_FadeOut)
		{
			pep_SceneTransition.Draw(false, -0.005); // Black To Scene
		}
		else if(true == jss_Global_GrowingColor_FadeOut)
		{
			if (pep_Global_FadeOut)
			{
				pep_SceneTransition.Draw(true, 0.005); // Scene To Black
			}
		}
	}
	else if(5 == pep_Global_Scene)
	{
		gl.activeTexture(gl.TEXTURE0);
		pep_Credits.Draw();
		
		if (pep_Global_FadeOut)
		{
			//console.log(current_credit_texture);
			if (1 == current_credit_texture || 2 == current_credit_texture)
			{
				pep_SceneTransition.Draw(true, 0.0029); // Scene To Black
				//console.log(current_credit_texture);
			}
			else
			{
				pep_SceneTransition.Draw(true, 0.0051); // Scene To Black
			}
		}
		else
		{
			pep_SceneTransition.Draw(false, -0.0051); // Black To Scene
		}
	} else {
		//audioControl.pause();
	}
	
	

	requestAnimationFrame(draw, canvas); // this function is brower dependent
}

function uninitialize()
{
	if(pep_Credits)
	{
		pep_Credits.Uninit();
		pep_Credits = null;
	}
	
	if(ts_Scene3Gallery)
	{
		ts_Scene3Gallery.Uninit();
	}
	
	if(templeAndShivPinda)
	{
		templeAndShivPinda.uninitialize();
		templeAndShivPinda = null;
	}
	if(pep_Terrain)
	{
		pep_Terrain.Uninit();
		pep_Terrain = null;
	}

	if (pep_Skybox) {
		pep_Skybox.Uninit();
		pep_Skybox = null;
	}
	
	if(pep_Title)
	{
		pep_Title.Uninit();
		pep_Title = null;
	}
	
	if(pep_SceneTransition)
	{
		pep_SceneTransition.Uninit();
		pep_SceneTransition = null;
	}
	
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}
