#pragma once

#include "Common.h"

int Room_Initialize(int width, int height);
void Room_Uninitialize();
void Room_Update();
void Room_Resize(int width, int height);
void Room_Display(void);
