//#ifndef DEFINES_H
//#define DEFINES_H
//
//#define PEP_DIM    1920       // Square size of solver domain
//#define PEP_DOMAINSIZE    (PEP_DIM*PEP_DIM)  // Total domain size
//#define PEP_CPADW (PEP_DIM/2+1)  // Padded width for real->complex in-place FFT
//#define PEP_RPADW (2*(PEP_DIM/2+1))  // Padded width for real->complex in-place FFT
//#define PEP_PADDEDDOMAINSIZE   (PEP_DIM*PEP_CPADW) // Padded total domain size
//
//#define PEP_DELTA_T     0.09f     // Delta T for interative solver
//#define PEP_VISCOSITY_CONSTANT    0.0025f   // Viscosity constant
//#define PEP_FORCE (5.8f*PEP_DIM) // Force scale factor 
//#define PEP_FORCE_UPDATE_RADIUS     4         // Force update radius
//
//#define PEP_TILE_WIDTH 64 // Tile width
//#define PEP_TILE_HEIGHT 64 // Tile height
//#define PEP_TIDS_IN_X 64 // Tids in X
//#define PEP_TIDS_IN_Y 4  // Tids in Y
//
//#endif

/*
* Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
*
* Please refer to the NVIDIA end user license agreement (EULA) associated
* with this source code for terms and conditions that govern your use of
* this software. Any use, reproduction, disclosure, or distribution of
* this software and related documentation outside the terms of the EULA
* is strictly prohibited.
*
*/

#ifndef _DEFINES_H_
#define _DEFINES_H_

typedef unsigned int uint;
typedef unsigned char uchar;

// Using shared to store computed vertices and normals during triangle generation
// improves performance

// The number of threads to use for triangle generation (limited by shared memory size)
#define NTHREADS 32

#endif
