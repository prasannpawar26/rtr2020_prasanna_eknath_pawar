#include "Common.h"
#include "defines.h"
#include "Shadow.h"
#include "Sphere.h"

extern GLuint pep_CubeMarching_vao;
extern uint pep_CubeMarching_totalVerts;
extern bool pep_CubeMarching_wireframe;
extern int pep_Scene;
/*
we render the scene from the light's point of view and everything we see from the light's perspective is lit
and everything we can't see must be in shadow

The first pass requires us to generate a depth map. The depth map is the depth texture as rendered from the light's perspective
that we'll be using for testing for shadows

=====================PASS 1: Light Pass=======================================
Framebuffer ->
	We only need the depth information when rendering the scene from the light's perspective
	so there is no need for a color buffer.

	A framebuffer object however is not complete without a color buffer so we need to explicitly tell OpenGL we're not going to render any color data.
	We do this by setting both the read and draw buffer to GL_NONE with glDrawBuffer and glReadbuffer

Light space transform ->
	Projection Matrix:
	1. all its light rays are parallel -> ortho projection ->used for directional lights
	2. all its light rays are not parallel -> prespective projection -> used spotlights and point lights
	
	View Matrix
	Transform each object so they're visible from the light's point of view. -> use lightposition instead of camera position for lookat

	Combining these two gives us a light space transformation matrix that
	transforms each world-space vector into the space as visible from the light source =>  render the depth map.

Fragment Shader:
	Since we have no color buffer and disabled the draw and read buffers,
	the resulting fragments do not require any processing so we can simply use an empty fragment shader.

	Empty fragment shader does no processing whatsoever, and at the end of its run the depth buffer is updated

=================================PASS 2: =======================================

======
Shadow acne
	Because the shadow map is limited by resolution, multiple fragments can sample the same value from the depth map.

	can solve this issue with a small little hack called a shadow bias where we simply offset the depth of the surface.

Peter panning
	A disadvantage of using a shadow bias is that you're applying an offset to the actual depth of objects.
	This shadow artifact is called peter panning since objects seem slightly detached from their shadows.

	to solve most of the peter panning issue by using front face culling when rendering the depth map => but only for solid

Over sampling

PCF [percentage-closer filtering]
	Because the depth map has a fixed resolution, the depth frequently usually spans more than one fragment per texel.
	As a result, multiple fragments sample the same depth value from the depth map and
	come to the same shadow conclusions, which produces these jagged blocky edges.

	**simple implementation of PCF is to simply sample the surrounding texels of the depth map and average the results**

	By using more samples and/or varying the texelSize variable you can increase the quality of the soft shadows
*/
extern FILE *pep_gpFile;
extern int pep_gWidth;
extern int pep_gHeight;

GLuint pep_Shadow_VertexShader;
GLuint pep_Shadow_FragmentShader;
GLuint pep_Shadow_ShaderProgram;

mat4 pep_Shadow_ProjectionMatrix;
mat4 pep_Shadow_ViewMatrix;

GLuint pep_Shadow_FrameBuffer;
GLuint pep_Shadow_FrameBuffer_ShadowMapTexture;

GLuint pep_Shadow_UniformModelMatrix;
GLuint pep_Shadow_UniformViewMatrix;
GLuint pep_Shadow_UniformProjectionMatrix;
GLuint pep_Shadow_UniformShadowMatrix;
GLuint pep_Shadow_UniformIsLightPass;
GLuint pep_Shadow_UniformShadowMapTexture;
GLuint pep_Shadow_UniformLightPosition;
GLuint pep_Shadow_UniformLightAmbient;
GLuint pep_Shadow_UniformLightDiffuse;
GLuint pep_Shadow_UniformLightSpecular;
GLuint pep_Shadow_UniformMaterialAmbient;
GLuint pep_Shadow_UniformMaterialDiffuse;
GLuint pep_Shadow_UniformMaterialSpecular;
GLuint pep_Shadow_UniformMaterialShiness;

/*************************Matrix Related Variables****************************/
mat4 pep_Shadow_LightViewMatrix;
mat4 pep_Shadow_LightProjectionMatrix;
mat4 pep_Shadow_LightBiasMatrix;
mat4 pep_Shadow_LightBiasProjectionMatrix;
mat4 pep_Shadow_LightShadowMatrix;

GLfloat pep_Shadow_AngleCube;

GLuint pep_Shadow_CubeVao;
GLuint pep_Shadow_CubePosition;
GLuint pep_Shadow_CubeNormal;

GLuint pep_Shadow_GroundVao;
GLuint pep_Shadow_GroundPositionVbo;
GLuint pep_Shadow_GroundNormalVbo;

GLuint pep_Shadow_SphereVao;
GLuint pep_Shadow_SpherePositionVbo;
GLuint pep_Shadow_SphereNormalVbo;
GLuint pep_Shadow_SphereElementVbo;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;
// Sphere Properties
//float sphere_radius = 1.0f;
//int sphere_no_stacks = 180;// = 180; // 20;
//int sphere_no_sectors = 360;//360; // no of vertices per stack ring
//float *sphere_vertices;
//float *sphere_normals;
//int sphere_vertices_count = 
//	sphere_no_sectors * COORDINATE_SYSTEM_3D * sphere_no_stacks * NO_VERTICES_IN_QUAD;


//float lightPosition[4] = {-2.624089f, 3.589553f, 2.286757f, 1.000000f};
//float lightPosition[4] = {2.624089f, 3.589553f, -2.286757f, 1.000000f};
extern float lightPosition[4];
float lightAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};

float materialAmbient_sphere[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float materialDiffuse_sphere[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float materialSpecular_sphere[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float materialShiness_sphere = 50.0f;  // Also Try 128.0f

float materialDiffuse_cube[4] = {0.0f, 0.00f, 1.0f, 1.0f};
float materialSpecular_cube[4] = {0.0f, 0.00f, 1.0f, 1.0f};
float materialShiness_cube = 50.0f;  // Also Try 128.0f

float materialAmbient_marchingcube[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float materialDiffuse_marchingcube[4] = {0.50f, 0.0f, 0.0f, 1.0f};
float materialSpecular_marchingcube[4] = {0.50f, 0.0f, 0.0f, 1.0f};
float materialShiness_marchingcube = 50.0f;  // Also Try 128.0f
int Shadow_InitializeFbo(int width, int height)
{

	glGenFramebuffers(1, &pep_Shadow_FrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, pep_Shadow_FrameBuffer);

	glGenTextures(1, &pep_Shadow_FrameBuffer_ShadowMapTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR /*GL_NEAREST*/);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR/*GL_NEAREST*/);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE/*GL_CLAMP_TO_BORDER*/);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE/*GL_CLAMP_TO_BORDER*/);
	// Remove Over sampling
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	GLfloat border[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32/*GL_DEPTH_COMPONENT24*/, width, height, 0,
		GL_DEPTH_COMPONENT, GL_FLOAT/*GL_UNSIGNED_BYTE*/, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n",
			__FILE__, __LINE__, __FUNCTION__);
	} else {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Complete\n",
			__FILE__, __LINE__, __FUNCTION__);
	}
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	return 0;
}

void Shadow_InitializeModels(void)
{
	// Ground
	glGenVertexArrays(1, &pep_Shadow_GroundVao);

	glBindVertexArray(pep_Shadow_GroundVao);

	// ground-vertex
	const GLfloat groundVertices[] = {
		// BOTTOM
		6.0f, -1.0f,  -6.0f,
		-6.0f, -1.0f, -6.0f,
		-6.0f, -1.0f, 6.0f,
		6.0f, -1.0f,  6.0f,

		//Back
		6.0f, 6.0f,  -6.0f,
		-6.0f, 6.0f, -6.0f,
		-6.0f, -1.0f, -6.0f,
		6.0f, -1.0f,  -6.0f
	};

	glGenBuffers(1, &pep_Shadow_GroundPositionVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_GroundPositionVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(groundVertices), groundVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	const GLfloat groundNormals[] = {
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f
	};

	glGenBuffers(1, &pep_Shadow_GroundNormalVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_GroundNormalVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(groundNormals), groundNormals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Cube
	glGenVertexArrays(1, &pep_Shadow_CubeVao);

	glBindVertexArray(pep_Shadow_CubeVao);

	// cube-vertex
	const GLfloat cubeVertices[] = {
		// TOP
		-1.0f, 1.0f, -1.0f,   // top-left
		-1.0f, 1.0f, 1.0f,    // bottom-left
		1.0f, 1.0f, 1.0f,     // bottom-right
		1.0f, 1.0f, -1.0f,    // top-right
							  // BOTTOM
							  1.0f, -1.0f, -1.0f,   // top-left
							  1.0f, -1.0f, 1.0f,    // bottom-left
							  -1.0f, -1.0f, 1.0f,   // bottom-right
							  -1.0f, -1.0f, -1.0f,  // top-right
													// FRONT
													-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f,
													1.0f,
													// BACK
													1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,
													-1.0f,
													// RIGHT
													1.0f, 1.0f, 1.0f,     // top-left
													1.0f, -1.0f, 1.0f,    // bottom-left
													1.0f, -1.0f, -1.0f,   // bottom-right
													1.0f, 1.0f, -1.0f,    // top-right
																		  // LEFT
																		  -1.0f, 1.0f, -1.0f,   // top-left
																		  -1.0f, -1.0f, -1.0f,  // bottom-left
																		  -1.0f, -1.0f, 1.0f,   // bottom-right
																		  -1.0f, 1.0f, 1.0f     // top-right
	};

	glGenBuffers(1, &pep_Shadow_CubePosition);

	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_CubePosition);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	const GLfloat cubeNormals[] = {0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
		0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

		0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,
		0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,

		0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
		0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,

		0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,
		0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,

		1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
		1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

		-1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,
		-1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f};

	glGenBuffers(1, &pep_Shadow_CubeNormal);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_CubeNormal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Sphere
	//sphere_vertices = (float *)malloc(sphere_vertices_count * sizeof(float));
	//memset(sphere_vertices, 0, sphere_vertices_count * sizeof(float));

	//sphere_normals = (float *)malloc(sphere_vertices_count * sizeof(float));
	//memset(sphere_normals, 0, sphere_vertices_count * sizeof(float));

	//GetSphereData(sphere_radius, sphere_no_stacks, sphere_no_stacks, &sphere_vertices, &sphere_normals);

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
		sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &pep_Shadow_SphereVao);
	glBindVertexArray(pep_Shadow_SphereVao);

	glGenBuffers(1, &pep_Shadow_SpherePositionVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_SpherePositionVbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pep_Shadow_SphereNormalVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_SphereNormalVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &pep_Shadow_SphereElementVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Shadow_SphereElementVbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements),
		sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return;
}

int Shadow_InitializeShaderProgram(void)
{
	const GLchar *vertexShaderSourceCode =
		"#version 450 core"
		"\n"

		"in vec4 vVertex;"
		"in vec3 vNormal;"

		"uniform mat4 uShadow_ModelMatrix;"
		"uniform mat4 uShadow_ViewMatrix;"
		"uniform mat4 uShadow_ProjectionMatrix;"
		"uniform mat4 uShadow_ShadowMatrix;"
		"uniform vec4 uShadow_LightPosition;"

		"out vec3 tranformation_matrix;"  // tranformation_matrix
		"out vec3 light_direction;"
		"out vec4 eye_coordinates;"
		"out vec4 vShadowCoords;"
		"out vec3 viewer_vector;"

		"void main(void)"
		"{"
			//eye space vertex position
			"vec4 eye_coordinates = uShadow_ViewMatrix * uShadow_ModelMatrix *vVertex;"

			//eye space normal
			"tranformation_matrix   = normalize(mat3(uShadow_ViewMatrix * uShadow_ModelMatrix) * vNormal);"

			//the shadow coordinates
			"vShadowCoords = uShadow_ShadowMatrix * ( uShadow_ModelMatrix * vVertex);"

			"light_direction = vec3(uShadow_LightPosition - eye_coordinates);"

			"viewer_vector = vec3(-eye_coordinates);"

			"gl_Position = uShadow_ProjectionMatrix * uShadow_ViewMatrix * uShadow_ModelMatrix * vVertex;"
		"}";

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core"
		"\n"

		"out vec4 vFragColor;"

		"uniform mat4 uShadow_ModelMatrix;"
		"uniform mat4 uShadow_ViewMatrix;"
		"uniform vec4 uShadow_LightPosition;"
		"uniform sampler2DShadow uShadow_ShadowMapTexture;"
		"uniform int uShadow_IsLightPass;"

		"in vec3 tranformation_matrix;"  // tranformation_matrix
		"in vec3 light_direction;"
		"in vec4 eye_coordinates;"
		"in vec4 vShadowCoords;"
		"in vec3 viewer_vector;"

		"const float k0 = 1.0;"
		"const float k1 = 0.0;"
		"const float k2 = 0.0;"

		"uniform vec3 uShadow_LightAmbient;"
		"uniform vec3 uShadow_LightDiffuse;"
		"uniform vec3 uShadow_LightSpecular;"

		"uniform vec3 uShadow_MaterialAmbient;"
		"uniform vec3 uShadow_MaterialDiffuse;"
		"uniform vec3 uShadow_MaterialSpecular;"
		"uniform float uShadow_MaterialShiness;"

		"vec3 phong_ads_light;"

		"float random(vec4 seed) {"
			"float dot_product = dot(seed, vec4(12.9898,78.233,45.164,94.673));"
			"return fract(sin(dot_product) * 43758.5453);"
		"}"

		"void main()"
		"{"
			"if(1 == uShadow_IsLightPass)"
			"{"
				"return;"
			"}"

			"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);"

			"vec3 viewer_vector_normal = normalize(viewer_vector);"

			"vec3 light_direction_normalize = normalize(light_direction);"
			"float d = length(light_direction);"

			"vec3 reflection_vector = reflect(-light_direction_normalize, "
			"tranformation_matrix_normalize);"

			"float t_normal_dot_light_direction = max(dot(light_direction_normalize, "
			"tranformation_matrix_normalize), 0.0f);"

			//"float attenuationAmount = 1.0/(k0 + (k1*d) + (k2*d*d));"
			"float attenuationAmount = 1.0;"

			"float diffuse_1 = t_normal_dot_light_direction * attenuationAmount;"

			"vec3 ambient = uShadow_LightAmbient * uShadow_MaterialAmbient;"

			"vec3 diffuse = uShadow_LightDiffuse * uShadow_MaterialDiffuse * "
			"t_normal_dot_light_direction * attenuationAmount ;"

			"vec3 specular = uShadow_LightSpecular * uShadow_MaterialSpecular * "
			"pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), "
			"uShadow_MaterialShiness);"

			"float shadow;"
			"if(vShadowCoords.w > 1)"
			"{"
				////=============OPTION 1=======================///////
				/*"float bias = max(0.05 * (1.0 - t_normal_dot_light_direction), 0.005);"
				"shadow = textureProj(uShadow_ShadowMapTexture, vShadowCoords);"
				"diffuse_1 = mix(diffuse_1, diffuse_1*shadow, 0.5); "*/

				////=============OPTION 2=======================///////
				/*"float sum = 0;"
				"shadow = 1;"

				"for(int i=0;i<16;i++) {"
					"float indexA = (random(vec4(gl_FragCoord.xyx, i))*0.25);"
					"float indexB = (random(vec4(gl_FragCoord.yxy, i))*0.25); "
					"sum += textureProj(uShadow_ShadowMapTexture, vShadowCoords+vec4(indexA, indexB, 0, 0));"
				"}"
				"shadow = sum/16.0;"
				"diffuse_1 = mix(diffuse_1, diffuse_1*shadow, 0.5);"*/

				////=============OPTION 3: using 3x3 neighborhood=======================///////
				/*"float sum = 0;"
				"float shadow = 1;"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 0));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 2));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 0,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 0, 0));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 0, 2));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 0));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 2));"
				"shadow = sum/9.0;"
				"diffuse_1 = mix(diffuse_1, diffuse_1*shadow, 0.5);"*/

				////=============OPTION 4: using 4x4 neighborhood=======================///////
				"float sum = 0;"
				"float shadow = 1;"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2,-2));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2,-1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1,-1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1,-1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2,-1));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1, 1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1, 1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 1));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1, 2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1, 2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 2));"

				"shadow = sum/16.0;"
				"diffuse_1 = mix(diffuse_1, diffuse_1 * shadow, 0.5);"
			"}"

			"phong_ads_light= ambient + diffuse + specular;"

			"float gamma = 2.2;" \

			"phong_ads_light = pow(phong_ads_light.rgb, vec3(1.0/gamma));" \

			"vFragColor = diffuse_1 * vec4(phong_ads_light, 1);"
		"}";

	pep_Shadow_VertexShader = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_Shadow_VertexShader) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -1;
	}

	glShaderSource(pep_Shadow_VertexShader, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_Shadow_VertexShader);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Shadow_VertexShader, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(pep_Shadow_VertexShader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Shadow_VertexShader, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_Shadow_FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_Shadow_FragmentShader) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -2;
	}

	glShaderSource(pep_Shadow_FragmentShader, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_Shadow_FragmentShader);

	glGetShaderiv(pep_Shadow_FragmentShader, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(pep_Shadow_FragmentShader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Shadow_FragmentShader, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -3;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_Shadow_ShaderProgram = glCreateProgram();

	glAttachShader(pep_Shadow_ShaderProgram, pep_Shadow_VertexShader);
	glAttachShader(pep_Shadow_ShaderProgram, pep_Shadow_FragmentShader);

	glBindAttribLocation(pep_Shadow_ShaderProgram, AMC_ATTRIBUTES_POSITION, "vVertex");
	glBindAttribLocation(pep_Shadow_ShaderProgram, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(pep_Shadow_ShaderProgram);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_Shadow_ShaderProgram, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(pep_Shadow_ShaderProgram, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(pep_Shadow_ShaderProgram, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -4;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	pep_Shadow_UniformModelMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ModelMatrix");
	pep_Shadow_UniformViewMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ViewMatrix");
	pep_Shadow_UniformProjectionMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ProjectionMatrix");
	pep_Shadow_UniformShadowMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ShadowMatrix");
	pep_Shadow_UniformLightPosition = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightPosition");
	pep_Shadow_UniformIsLightPass = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_IsLightPass");
	pep_Shadow_UniformShadowMapTexture = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ShadowMapTexture");

	pep_Shadow_UniformLightAmbient = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightAmbient");
	pep_Shadow_UniformLightDiffuse = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightDiffuse");
	pep_Shadow_UniformLightSpecular = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightSpecular");

	pep_Shadow_UniformMaterialAmbient = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialAmbient");
	pep_Shadow_UniformMaterialDiffuse = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialDiffuse");
	pep_Shadow_UniformMaterialSpecular = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialSpecular");
	pep_Shadow_UniformMaterialShiness = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialShiness");

	return 0;
}

int Shadow_Initialize(int width, int height)
{
	Shadow_InitializeFbo(width, height);

	Shadow_InitializeShaderProgram();

	Shadow_InitializeModels();

	return 0;
}

void Shadow_Uninitialize()
{
	// code
	if (pep_Shadow_FrameBuffer_ShadowMapTexture)
	{
		glDeleteTextures(1, &pep_Shadow_FrameBuffer_ShadowMapTexture);
		pep_Shadow_FrameBuffer_ShadowMapTexture = 0;
	}

	if (pep_Shadow_FrameBuffer)
	{
		glDeleteFramebuffers(1, &pep_Shadow_FrameBuffer);
		pep_Shadow_FrameBuffer = 0;
	}

	if (pep_Shadow_CubePosition)
	{
		glDeleteBuffers(1, &pep_Shadow_CubePosition);
		pep_Shadow_CubePosition = 0;
	}

	if (pep_Shadow_CubeNormal)
	{
		glDeleteBuffers(1, &pep_Shadow_CubeNormal);
		pep_Shadow_CubeNormal = 0;
	}

	if (pep_Shadow_CubeVao)
	{
		glDeleteVertexArrays(1, &pep_Shadow_CubeVao);
		pep_Shadow_CubeVao = 0;
	}

	if (pep_Shadow_GroundPositionVbo)
	{
		glDeleteBuffers(1, &pep_Shadow_GroundPositionVbo);
		pep_Shadow_GroundPositionVbo = 0;
	}

	if (pep_Shadow_GroundNormalVbo)
	{
		glDeleteBuffers(1, &pep_Shadow_GroundNormalVbo);
		pep_Shadow_GroundNormalVbo = 0;
	}

	if (pep_Shadow_GroundVao)
	{
		glDeleteVertexArrays(1, &pep_Shadow_GroundVao);
		pep_Shadow_GroundVao = 0;
	}

	if (pep_Shadow_ShaderProgram) {
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_Shadow_ShaderProgram);

		glGetProgramiv(pep_Shadow_ShaderProgram, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders) {
			glGetAttachedShaders(pep_Shadow_ShaderProgram, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(pep_Shadow_ShaderProgram, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_Shadow_ShaderProgram);
		pep_Shadow_ShaderProgram = 0;

		glUseProgram(0);
	}

}

void Shadow_Update()
{
	if (360.0f < pep_Shadow_AngleCube)
	{
		pep_Shadow_AngleCube = 0.0f;
	}
	//pep_Shadow_AngleCube += 0.40f;
}

void Shadow_Resize(int width, int height)
{
	if (pep_Shadow_FrameBuffer)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pep_Shadow_FrameBuffer);
		glBindTexture(GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32/*GL_DEPTH_COMPONENT24*/, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT/*GL_UNSIGNED_BYTE*/, NULL);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void Shadow_DisplayScene(void)
{
	mat4 T = mat4::identity();
	mat4 R = mat4::identity();
	mat4 l_M = mat4::identity();
	mat4 l_MV = mat4::identity();
	pep_Shadow_ProjectionMatrix = mat4::identity();
	pep_Shadow_ViewMatrix = mat4::identity();

	pep_Shadow_ProjectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / pep_gHeight, 0.1f, 1000.f);
	pep_Shadow_ViewMatrix = lookat(vec3(0.0f, 02.50f, 8.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	glUseProgram(pep_Shadow_ShaderProgram);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture);
	glUniform1f(pep_Shadow_UniformShadowMapTexture, 0);


	//
	// ground
	//
	glUniform1i(pep_Shadow_UniformIsLightPass, 0);

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, mat4::identity());
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, pep_Shadow_ViewMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	glUniform3fv(pep_Shadow_UniformLightAmbient, 1, lightAmbient);
	glUniform3fv(pep_Shadow_UniformLightDiffuse, 1, lightDiffuse);
	glUniform3fv(pep_Shadow_UniformLightSpecular, 1, lightSpecular);
	glUniform4fv(pep_Shadow_UniformLightPosition, 1, lightPosition);

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, vec3(0.0f, 0.0f, 0.0f));
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, vec3(0.20f, 0.20f, 0.20f));
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1,vec3(0.20f, 0.20f, 0.20f));
	glUniform1f(pep_Shadow_UniformMaterialShiness, 50.0f);

	glBindVertexArray(pep_Shadow_GroundVao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glBindVertexArray(0);

	if (2 == pep_Scene)
	{
		//
		// sphere
		//
		glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, materialDiffuse_sphere);
		glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1, materialSpecular_sphere);
		glUniform1f(pep_Shadow_UniformMaterialShiness, materialShiness_sphere);

		T = translate(-1.0f, 0.250f, 0.0f);
		R = rotate(pep_Shadow_AngleCube, 0.0f, 1.0f, 0.0f);
		l_M = T * R;
		l_MV = pep_Shadow_ViewMatrix;

		glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
		glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
		glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
		glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

		glBindVertexArray(pep_Shadow_SphereVao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Shadow_SphereElementVbo);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		//
		// cube
		//
		glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, materialDiffuse_cube);
		glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1, materialSpecular_cube);
		glUniform1f(pep_Shadow_UniformMaterialShiness, materialShiness_cube);

		T = translate(1.0f, 0.250f, 0.0f) * scale(1.0f, 1.0f, 1.0f);
		R = rotate(pep_Shadow_AngleCube, 0.0f, 1.0f, 0.0f);
		l_M = T * R;
		l_MV = pep_Shadow_ViewMatrix;

		glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
		glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
		glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
		glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

		glBindVertexArray(pep_Shadow_SphereVao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Shadow_SphereElementVbo);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		/*glBindVertexArray(pep_Shadow_CubeVao);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
		glBindVertexArray(0);*/

	}
	else if (3 == pep_Scene)
	{
		T = translate(0.0f, 0.250f, 0.0f);
		R = rotate(pep_Shadow_AngleCube, 0.0f, 1.0f, 0.0f);
		l_M = T * R;
		l_MV = pep_Shadow_ViewMatrix;

		glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, materialDiffuse_marchingcube);
		glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1, materialSpecular_marchingcube);
		glUniform1f(pep_Shadow_UniformMaterialShiness, materialShiness_marchingcube);
		glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
		glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
		glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
		glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

		glPolygonMode(GL_FRONT_AND_BACK, pep_CubeMarching_wireframe ? GL_LINE : GL_FILL);
		glBindVertexArray(pep_CubeMarching_vao);
		glDrawArrays(GL_TRIANGLES, 0, pep_CubeMarching_totalVerts);
		glBindVertexArray(0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);

	return;
}

void Shadow_DisplayLightPass(void)
{
	mat4 T = mat4::identity();
	mat4 R = mat4::identity();
	mat4 l_M = mat4::identity();
	mat4 l_MV = mat4::identity();
	mat4 l_MVP = mat4::identity();

	glUseProgram(pep_Shadow_ShaderProgram);

	//
	// ground
	//
	/*glUniform1i(pep_Shadow_UniformIsLightPass, 1);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, pep_Shadow_LightViewMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_LightProjectionMatrix);*/

	//glBindVertexArray(pep_Shadow_GroundVao);
	//glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	//glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	//glBindVertexArray(0);

	if (2 == pep_Scene)
	{
		//
		// sphere
		//
		T = mat4::identity();
		R = mat4::identity();
		T = translate(-1.0f, 0.250f, 0.0f);
		R = rotate(pep_Shadow_AngleCube, 0.0f, 1.0f, 0.0f);
		l_M = T * R;
		l_MV = pep_Shadow_LightViewMatrix;

		glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
		glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
		glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
		glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_LightProjectionMatrix);

		glBindVertexArray(pep_Shadow_SphereVao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Shadow_SphereElementVbo);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		//
		// Cube
		//
		T = mat4::identity();
		R = mat4::identity();
		l_M = mat4::identity();
		l_MV = mat4::identity();
		l_MVP = mat4::identity();

		T = translate(1.0f, 0.250f, 0.0f) * scale(1.0f, 1.0f, 1.0f);
		R = rotate(pep_Shadow_AngleCube, 0.0f, 1.0f, 0.0f);
		l_M = T * R;
		l_MV = pep_Shadow_LightViewMatrix;

		glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
		glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
		glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
		glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_LightProjectionMatrix);

		glBindVertexArray(pep_Shadow_SphereVao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Shadow_SphereElementVbo);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		/*glBindVertexArray(pep_Shadow_CubeVao);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
		glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
		glBindVertexArray(0);*/
	}
	else if (3 == pep_Scene)
	{
		T = mat4::identity();
		R = mat4::identity();
		l_M = mat4::identity();
		l_MV = mat4::identity();

		T = translate(0.0f, 0.250f, 0.0f);
		R = rotate(pep_Shadow_AngleCube, 0.0f, 1.0f, 0.0f);
		l_M = T * R;
		l_MV = pep_Shadow_LightViewMatrix;

		glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
		glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
		glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
		glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_LightProjectionMatrix);

		glPolygonMode(GL_FRONT_AND_BACK, pep_CubeMarching_wireframe ? GL_LINE : GL_FILL);
		glBindVertexArray(pep_CubeMarching_vao);
		glDrawArrays(GL_TRIANGLES, 0, pep_CubeMarching_totalVerts);
		glBindVertexArray(0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	glUseProgram(0);

	return;
}

void Shadow_Display()
{
	pep_Shadow_LightViewMatrix = mat4::identity();
	pep_Shadow_LightViewMatrix = lookat(vec3(lightPosition[0], lightPosition[1], lightPosition[2]),
		vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	pep_Shadow_LightProjectionMatrix = mat4::identity();
	pep_Shadow_LightProjectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / pep_gHeight, 0.1f,
		1000.f); 

	pep_Shadow_LightBiasMatrix = mat4::identity();
	pep_Shadow_LightBiasMatrix = pep_Shadow_LightBiasMatrix * translate(0.5f, 0.5f, 0.5f);
	pep_Shadow_LightBiasMatrix = pep_Shadow_LightBiasMatrix * scale(0.5f, 0.5f, 0.5f);

	pep_Shadow_LightBiasProjectionMatrix = mat4::identity();
	pep_Shadow_LightBiasProjectionMatrix = pep_Shadow_LightBiasMatrix * pep_Shadow_LightProjectionMatrix;

	pep_Shadow_LightShadowMatrix = mat4::identity();
	pep_Shadow_LightShadowMatrix = pep_Shadow_LightBiasProjectionMatrix * pep_Shadow_LightViewMatrix;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// 1) Render scene from the light's POV
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pep_Shadow_FrameBuffer);
		glClear(GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);
		// fix peter panning - cull front faces
		glCullFace(GL_FRONT);
		Shadow_DisplayLightPass();
		glCullFace(GL_BACK); //=> Set to default again
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDrawBuffer(GL_BACK_LEFT);
	}

	// Second pass we render the scene as normal and use the generated depth map to calculate whether fragments are in shadow
	{
		glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Shadow_DisplayScene();
	}

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}
