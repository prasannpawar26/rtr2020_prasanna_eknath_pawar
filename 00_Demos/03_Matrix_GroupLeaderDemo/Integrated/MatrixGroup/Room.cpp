#include "Room.h"
#include "stb_image.h"

extern FILE* pep_gpFile;
extern int pep_gWidth;
extern int pep_gHeight;

GLuint pep_Room_vertexShader;
GLuint pep_Room_fragmentShader;
GLuint pep_Room_programObject;

//Data passing
GLuint pep_Room_vao;
GLuint pep_Room_vboPosition;
GLuint pep_Room_vboNormal;
GLuint pep_Room_vboDiffuseTexcoord;
GLuint pep_Room_vboNormalMapTexcoord;
GLuint pep_Room_vboAoTexcoord;
GLuint pep_Room_vboTangent;
GLuint pep_Room_vboBitangent;

GLuint pep_Room_textureDiffuse;
GLuint pep_Room_textureNormalMap;
GLuint pep_Room_textureAo;

GLuint pep_Room_floorTextureDiffuse;
GLuint pep_Room_floorTextureNormalMap;
GLuint pep_Room_floorTextureAo;

//Uniform
GLuint pep_Room_modelUniform;
GLuint pep_Room_viewUniform;
GLuint pep_Room_projectionUniform;
GLuint pep_Room_samplerDiffuseUniform;
GLuint pep_Room_samplerNormalMapUniform;
GLuint pep_Room_samplerAoUniform;
GLuint pep_Room_PointLight_ambientUniform;
GLuint pep_Room_PointLight_diffuseUniform;
GLuint pep_Room_PointLight_specularUniform;
GLuint pep_Room_PointLight_positionUniform;

GLfloat pep_Room_PointLight_lightAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
GLfloat pep_Room_PointLight_lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat pep_Room_PointLight_lightSpecular[4]= {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat pep_Room_PointLight_lightPosition[4]= {-100.0f, 1.0f, 100.0f, 1.0f};

BOOL Room_LoadTextureSTB(GLuint *texture, const char *filename)
{
    bool success = true;

    int width = 0, height = 0, nrChannels;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    unsigned char* data = 0;// = stbi_load(filename, &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        fprintf(pep_gpFile, "Failed To Load Texture");
        success = false;
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    return success;
}

BOOL Room_LoadTextureSTB_Ao(GLuint *texture, const char *filename)
{
    bool success = true;

    int width = 0, height = 0, nrChannels;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    unsigned char* data = 0;// = stbi_load(filename, &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R, width, height, 0, GL_R, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        fprintf(pep_gpFile, "Failed To Load Texture");
        success = false;
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    return success;
}

int Room_Initialize(int width, int height)
{
    pep_Room_vertexShader = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "in vec2 vTexCoord;" \
        "in vec3 vTangent;" \
        "in vec3 vBitangent;" \

        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_pointlight_position;" \

        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \

        "out vec2 out_texcoord;" \
        "out mat3 TBN;" \
        "void main(void)" \
        "{" \
            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \

            "mat3 normal_matrix = transpose(inverse(mat3(u_model_matrix)));" \

            "vec3 vertexNormal = normalize(normal_matrix * vNormal);"

            "vec3 vertexTangent = normalize(normal_matrix * vTangent);"

            "vertexTangent = normalize(vertexTangent - dot(vertexTangent, vertexNormal) * vertexNormal);" \

            "vec3 vertexBitangent = cross(vertexNormal, vertexTangent);"

            "TBN = transpose(mat3(vertexTangent, vertexBitangent, vertexNormal));"

            "light_direction = TBN * vec3(u_pointlight_position - eye_coordinates);" \

            "viewer_vector = TBN * vec3(-eye_coordinates);" \

            "out_texcoord = vTexCoord;" \

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";

    glShaderSource(pep_Room_vertexShader, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_Room_vertexShader);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Room_vertexShader, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Room_vertexShader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Room_vertexShader, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_Room_fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \
        "in mat3 TBN;" \
        "in vec3 light_direction;" \
        "vec3 tranformation_matrix;" \
        "in vec3 viewer_vector;" \

        "uniform vec3 u_pointlight_ambient;" \
        "uniform vec3 u_pointlight_diffuse;" \
        "uniform vec3 u_pointlight_specular;" \

        "uniform sampler2D u_sampler_diffuse;" \
        "uniform sampler2D u_sampler_normalmap;" \
        "uniform sampler2D u_sampler_ao;" \

        "vec3 phong_ads_light;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "vec4 Diffuse = texture(u_sampler_diffuse, out_texcoord * 10.0);" \

            "vec3 TextureNormal = texture(u_sampler_normalmap, out_texcoord * 10.0).xyz;" \

            "float Ambient = texture(u_sampler_ao, out_texcoord * 10.0).r;" \

            // transform normal vector to range [-1,1]
            "TextureNormal = normalize(TextureNormal * 2.0 - 1.0);" \

            "vec3 light_direction_normalize = normalize(light_direction);" \

            "vec3 tranformation_matrix_normalize = TextureNormal;" \

            "vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \

            "vec3 viewer_vector_normal = normalize(viewer_vector);" \

            "float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \

            "vec3 diffuse = u_pointlight_diffuse * Diffuse.rgb * t_normal_dot_light_direction;" \

            "vec3 specular = u_pointlight_specular * Diffuse.rgb * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), 0.25f * 50.0f);" \

            "vec3 ambient = vec3(0.03) * Diffuse.rgb * Ambient;"

            "phong_ads_light= ambient + diffuse + specular;" \

            "float gamma = 2.2;" \
            "phong_ads_light = pow(phong_ads_light.rgb, vec3(1.0/gamma));" \

            "FragColor = vec4(phong_ads_light, 1.0);" \
        "}";

    glShaderSource(pep_Room_fragmentShader, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_Room_fragmentShader);

    glGetShaderiv(pep_Room_fragmentShader, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Room_fragmentShader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Room_fragmentShader, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Room_programObject = glCreateProgram();
    glAttachShader(pep_Room_programObject, pep_Room_vertexShader);
    glAttachShader(pep_Room_programObject, pep_Room_fragmentShader);

    glBindAttribLocation(pep_Room_programObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_Room_programObject, AMC_ATTRIBUTES_TEXTURE_DIFFUSE_COORD0, "vTexCoord");
    glBindAttribLocation(pep_Room_programObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
    glBindAttribLocation(pep_Room_programObject, AMC_ATTRIBUTES_TANGENT, "vTangent");
    glBindAttribLocation(pep_Room_programObject, AMC_ATTRIBUTES_BITANGENT, "vBitangent");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_Room_programObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Room_programObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_Room_programObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Room_programObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Room_modelUniform = glGetUniformLocation(pep_Room_programObject, "u_model_matrix");
    pep_Room_viewUniform = glGetUniformLocation(pep_Room_programObject, "u_view_matrix");
    pep_Room_projectionUniform = glGetUniformLocation(pep_Room_programObject, "u_projection_matrix");

    pep_Room_samplerDiffuseUniform = glGetUniformLocation(pep_Room_programObject, "u_sampler_diffuse");
    pep_Room_samplerNormalMapUniform = glGetUniformLocation(pep_Room_programObject, "u_sampler_normalmap");
    pep_Room_samplerAoUniform = glGetUniformLocation(pep_Room_programObject, "u_sampler_ao");
    pep_Room_PointLight_ambientUniform = glGetUniformLocation(pep_Room_programObject, "u_pointlight_ambient");
    pep_Room_PointLight_diffuseUniform = glGetUniformLocation(pep_Room_programObject, "u_pointlight_diffuse");
    pep_Room_PointLight_specularUniform = glGetUniformLocation(pep_Room_programObject, "u_pointlight_specular");
    pep_Room_PointLight_positionUniform = glGetUniformLocation(pep_Room_programObject, "u_pointlight_position");

    //
    // Cube
    //
    static float value = 20.0f;

    //GLfloat cubeVertices[] =
    //{
    //    // top
    //    value, value, -value,
    //    -value, value, -value,
    //    -value, value, value,
    //    value, value, value,

    //    // bottom
    //    10.0f, -1.0f,  -12.0f,
    //    -10.0f, -1.0f, -12.0f,
    //    -10.0f, -1.0f, 12.0f,
    //    10.0f,  -1.0f,  12.0f,
    //   /* value, -0.0f, -value,
    //    -value, -0.0f, -value,
    //    -value, -0.0f,  value,
    //    value, -0.0f,  value,*/

    //    // front
    //    value, value, value,
    //    -value, value, value,
    //    -value, -value, value,
    //    value, -value, value,

    //    // back
    //    value, value, -value,
    //    -value, value, -value,
    //    -value, -value, -value,
    //    value, -value, -value,

    //    // left
    //    -value, value, value,
    //    -value, value, -value,
    //    -value, -value, -value,
    //    -value, -value, value,

    //    // right
    //    value, value, -value,
    //    value, value, value,
    //    value, -value, value,
    //    value, -value, -value,
    //};

    GLfloat groundVertices[] =
    {
        /*100.0f, -1.0f,  -100.0f,
        -100.0f, -1.0f, -100.0f,
        -100.0f, -1.0f, 100.0f,
        100.0f,  -1.0f,  100.0f*/
		// BOTTOM
		6.0f, -1.0f,  -6.0f,
		-6.0f, -1.0f, -6.0f,
		-6.0f, -1.0f, 6.0f,
		6.0f, -1.0f,  6.0f,

		//Back
		6.0f, 6.0f,  -6.0f,
		-6.0f, 6.0f, -6.0f,
		-6.0f, -6.0f, -6.0f,
		6.0f, -6.0f,  -6.0f
    };

    GLfloat groundIndices[] =
    {
        3, 1, 0, 3, 1, 2,
		7, 5, 4, 7, 5, 6
    };

    GLfloat groundTexcoords[] =
    {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
    };

    GLfloat groundNormals[] =
    {
        //.0f, 1.0f, 0.0f,  // top-right of bottom
        //0.0f, 1.0f, 0.0f,  // top-left of bottom
        //0.0f, 1.0f, 0.0f,  // bottom-left of bottom
        //0.0f, 1.0f, 0.0f
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f
    };

    //GLuint cubeIndices[] =
    //{
    //    //top
    //    3, 1, 0, 3, 1, 2,
    //    //bottom
    //    7, 5, 4, 7, 5, 6,
    //    //front
    //    11, 9, 8, 11, 9, 10,
    //    //back
    //    15, 13, 12, 15, 13, 14,
    //    //left
    //    19, 17, 16, 19, 17, 18,
    //    //right
    //    23, 21, 20, 23, 21, 22,
    //};

    //GLfloat cubeTexcoords[] =
    //{
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    1.0f, 1.0f,
    //    0.0f, 1.0f,
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    1.0f, 1.0f,
    //    0.0f, 1.0f,
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    1.0f, 1.0f,
    //    0.0f, 1.0f,
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    1.0f, 1.0f,
    //    0.0f, 1.0f,
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    1.0f, 1.0f,
    //    0.0f, 1.0f,
    //    0.0f, 0.0f,
    //    1.0f, 0.0f,
    //    1.0f, 1.0f,
    //    0.0f, 1.0f
    //};

    //GLfloat cubeNormals[] =
    //{
    //    // top surface
    //    0.0f, 1.0f, 0.0f,  // top-right of top
    //    0.0f, 1.0f, 0.0f, // top-left of top
    //    0.0f, 1.0f, 0.0f, // bottom-left of top
    //    0.0f, 1.0f, 0.0f,  // bottom-right of top

    //                       // bottom surface
    //    0.0f, 1.0f, 0.0f,  // top-right of bottom
    //    0.0f, 1.0f, 0.0f,  // top-left of bottom
    //    0.0f, 1.0f, 0.0f,  // bottom-left of bottom
    //    0.0f, 1.0f, 0.0f,   // bottom-right of bottom

    //                         // front surface
    //    0.0f, 0.0f, 1.0f,  // top-right of front
    //    0.0f, 0.0f, 1.0f, // top-left of front
    //    0.0f, 0.0f, 1.0f, // bottom-left of front
    //    0.0f, 0.0f, 1.0f,  // bottom-right of front

    //                       // back surface
    //    0.0f, 0.0f, -1.0f,  // top-right of back
    //    0.0f, 0.0f, -1.0f, // top-left of back
    //    0.0f, 0.0f, -1.0f, // bottom-left of back
    //    0.0f, 0.0f, -1.0f,  // bottom-right of back

    //                        // left surface
    //    -1.0f, 0.0f, 0.0f, // top-right of left
    //    -1.0f, 0.0f, 0.0f, // top-left of left
    //    -1.0f, 0.0f, 0.0f, // bottom-left of left
    //    -1.0f, 0.0f, 0.0f, // bottom-right of left

    //                       // right surface
    //    1.0f, 0.0f, 0.0f,  // top-right of right
    //    1.0f, 0.0f, 0.0f,  // top-left of right
    //    1.0f, 0.0f, 0.0f,  // bottom-left of right
    //    1.0f, 0.0f, 0.0f  // bottom-right of right
    //};

    GLfloat* groundTangent = (GLfloat*)malloc(sizeof(GLfloat) * 8* 3);
    memset(groundTangent, 0, sizeof(GLfloat) * 8 * 3);

    GLfloat* groundBiTangent = (GLfloat*)malloc(sizeof(GLfloat) * 8 * 3);
    memset(groundBiTangent, 0, sizeof(GLfloat) * 8 * 3);

    for (int i = 0; i < 12; i += 3)
    {
        int vertex_0_Index = groundIndices[i + 0];
        int vertex_1_Index = groundIndices[i + 1];
        int vertex_2_Index = groundIndices[i + 2];

        float pos0[3];
        pos0[0] = groundVertices[vertex_0_Index * 3 + 0];
        pos0[1] = groundVertices[vertex_0_Index * 3 + 1];
        pos0[2] = groundVertices[vertex_0_Index * 3 + 2];
        float tex0[2];
        tex0[0] = groundTexcoords[vertex_0_Index * 2 + 0];
        tex0[1] = groundTexcoords[vertex_0_Index * 2 + 1];

        float pos1[3];
        pos1[0] = groundVertices[vertex_1_Index * 3 + 0];
        pos1[1] = groundVertices[vertex_1_Index * 3 + 1];
        pos1[2] = groundVertices[vertex_1_Index * 3 + 2];
        float tex1[2];
        tex1[0] = groundTexcoords[vertex_1_Index * 2 + 0];
        tex1[1] = groundTexcoords[vertex_1_Index * 2 + 1];

        float pos2[3];
        pos2[0] = groundVertices[vertex_2_Index * 3 + 0];
        pos2[1] = groundVertices[vertex_2_Index * 3 + 1];
        pos2[2] = groundVertices[vertex_2_Index * 3 + 2];
        float tex2[2];
        tex2[0] = groundTexcoords[vertex_2_Index * 2 + 0];
        tex2[1] = groundTexcoords[vertex_2_Index * 2 + 1];

        float edge1[3];
        edge1[0] = pos1[0] - pos0[0];
        edge1[1] = pos1[1] - pos0[1];
        edge1[2] = pos1[2] - pos0[2];

        float edge2[3];
        edge2[0] = pos2[0] - pos0[0];
        edge2[1] = pos2[1] - pos0[1];
        edge2[2] = pos2[2] - pos0[2];

        float DeltaU1 = tex1[0] - tex0[0];
        float DeltaV1 = tex1[1] - tex0[1];
        float DeltaU2 = tex2[0] - tex0[0];
        float DeltaV2 = tex2[1] - tex0[1];

        float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

        float Tangent[3];
        Tangent[0] = f * (DeltaV2 * edge1[0] - DeltaV1 * edge2[0]);
        Tangent[1] = f * (DeltaV2 * edge1[1] - DeltaV1 * edge2[1]);
        Tangent[2] = f * (DeltaV2 * edge1[2] - DeltaV1 * edge2[2]);

        groundTangent[vertex_0_Index * 3 + 0] += Tangent[0];
        groundTangent[vertex_0_Index * 3 + 1] += Tangent[1];
        groundTangent[vertex_0_Index * 3 + 2] += Tangent[2];
        groundTangent[vertex_1_Index * 3 + 0] += Tangent[0];
        groundTangent[vertex_1_Index * 3 + 1] += Tangent[1];
        groundTangent[vertex_1_Index * 3 + 2] += Tangent[2];
        groundTangent[vertex_2_Index * 3 + 0] += Tangent[0];
        groundTangent[vertex_2_Index * 3 + 1] += Tangent[1];
        groundTangent[vertex_2_Index * 3 + 2] += Tangent[2];

        float biTangent[3];
        biTangent[0] = f * (-DeltaU2 * edge1[0] + DeltaU1 * edge2[0]);
        biTangent[1] = f * (-DeltaU2 * edge1[1] + DeltaU1 * edge2[1]);
        biTangent[2] = f * (-DeltaU2 * edge1[2] + DeltaU1 * edge2[2]);

        groundBiTangent[vertex_0_Index * 3 + 0] += biTangent[0];
        groundBiTangent[vertex_0_Index * 3 + 1] += biTangent[1];
        groundBiTangent[vertex_0_Index * 3 + 2] += biTangent[2];
        groundBiTangent[vertex_1_Index * 3 + 0] += biTangent[0];
        groundBiTangent[vertex_1_Index * 3 + 1] += biTangent[1];
        groundBiTangent[vertex_1_Index * 3 + 2] += biTangent[2];
        groundBiTangent[vertex_2_Index * 3 + 0] += biTangent[0];
        groundBiTangent[vertex_2_Index * 3 + 1] += biTangent[1];
        groundBiTangent[vertex_2_Index * 3 + 2] += biTangent[2];
    }

    glGenVertexArrays(1, &pep_Room_vao);
    glBindVertexArray(pep_Room_vao);

    glGenBuffers(1, &pep_Room_vboPosition);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Room_vboPosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(groundVertices), groundVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Room_vboNormal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Room_vboNormal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(groundNormals), groundNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Room_vboDiffuseTexcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Room_vboDiffuseTexcoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(groundTexcoords), groundTexcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXTURE_DIFFUSE_COORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXTURE_DIFFUSE_COORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Room_vboTangent);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Room_vboTangent);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 4 * 3, groundTangent, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TANGENT);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Room_vboBitangent);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Room_vboBitangent);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 4 * 3, groundBiTangent, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_BITANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_BITANGENT);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    if (groundTangent)
    {
        free(groundTangent);
    }

    if (groundBiTangent)
    {
        free(groundBiTangent);
    }

    /*Room_LoadTextureSTB(&pep_Room_textureDiffuse, ".\\Room\\StoneBrickWall_Diffuse.jpg");
    Room_LoadTextureSTB(&pep_Room_textureNormalMap, ".\\Room\\StoneBrickWall_NormalMap.jpg");
    Room_LoadTextureSTB_Ao(&pep_Room_textureAo, ".\\Room\\StoneBrickWall_Ao.jpg");*/

    Room_LoadTextureSTB(&pep_Room_textureDiffuse, ".\\OldPlanks_Diffuse.jpg");
    Room_LoadTextureSTB(&pep_Room_textureNormalMap, ".\\OldPlanks_NormalMap.jpg");
    Room_LoadTextureSTB_Ao(&pep_Room_textureAo, ".\\OldPlanks_Ao.jpg");

    //Room_LoadTextureSTB(&pep_Room_textureDiffuse, ".\\Room\\BrickWall_Diffuse.jpg");
    //Room_LoadTextureSTB(&pep_Room_textureNormalMap, ".\\Room\\BrickWall_NormalMap.jpg");
    //Room_LoadTextureSTB_Ao(&pep_Room_textureAo, ".\\Room\\BrickWall_Ao.jpg");

    //Room_LoadTextureSTB(&pep_Room_floorTextureDiffuse, ".\\Room\\ConcreteFloorPainted_Diffuse.jpg");
    //Room_LoadTextureSTB(&pep_Room_floorTextureNormalMap, ".\\Room\\ConcreteFloorPainted_NormalMap.jpg");
    //Room_LoadTextureSTB_Ao(&pep_Room_floorTextureAo, ".\\Room\\ConcreteFloorPainted_Ao.jpg");

	return 0;
}

void Room_Uninitialize()
{
    if (pep_Room_textureAo)
    {
        glDeleteBuffers(1, &pep_Room_textureAo);
        pep_Room_textureAo = 0;
    }
    if (pep_Room_textureNormalMap)
    {
        glDeleteBuffers(1, &pep_Room_textureNormalMap);
        pep_Room_textureNormalMap = 0;
    }
    if (pep_Room_textureDiffuse)
    {
        glDeleteBuffers(1, &pep_Room_textureDiffuse);
        pep_Room_textureDiffuse = 0;
    }

    if (pep_Room_vboBitangent)
    {
        glDeleteBuffers(1, &pep_Room_vboBitangent);
        pep_Room_vboBitangent = 0;
    }
    if (pep_Room_vboTangent)
    {
        glDeleteBuffers(1, &pep_Room_vboTangent);
        pep_Room_vboTangent = 0;
    }
    if (pep_Room_vboAoTexcoord)
    {
        glDeleteBuffers(1, &pep_Room_vboAoTexcoord);
        pep_Room_vboAoTexcoord = 0;
    }
    if (pep_Room_vboNormalMapTexcoord)
    {
        glDeleteBuffers(1, &pep_Room_vboNormalMapTexcoord);
        pep_Room_vboNormalMapTexcoord = 0;
    }
    if (pep_Room_vboDiffuseTexcoord)
    {
        glDeleteBuffers(1, &pep_Room_vboDiffuseTexcoord);
        pep_Room_vboDiffuseTexcoord = 0;
    }
    if (pep_Room_vboNormal)
    {
        glDeleteBuffers(1, &pep_Room_vboNormal);
        pep_Room_vboNormal = 0;
    }
    if (pep_Room_vboPosition)
    {
        glDeleteBuffers(1, &pep_Room_vboPosition);
        pep_Room_vboPosition = 0;
    }
    if (pep_Room_vao)
    {
        glDeleteBuffers(1, &pep_Room_vao);
        pep_Room_vao = 0;
    }

    if (pep_Room_programObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Room_programObject);

        glGetProgramiv(pep_Room_programObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Room_programObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Room_programObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Room_programObject);
        pep_Room_programObject = 0;

        glUseProgram(0);
    }
}

void Room_Update()
{

}

void Room_Resize(int width, int height)
{

}

void Room_Display(void)
{
    mat4 modelMatrix = mat4::identity();
    mat4 viewMatrix = mat4::identity();
    mat4 projectionMatrix = mat4::identity();

    modelMatrix = translate(0.0f, 0.0f, 0.0f); /*= scale(1000.0f, 1000.0f, 1000.0f)*/;
    viewMatrix = lookat(vec3(0.0f, 0.0f, 10.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
    projectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 5000.0f);

    glClearDepth(1.0f);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glDepthFunc(GL_LEQUAL);

    glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(pep_Room_programObject);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_Room_samplerDiffuseUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_Room_textureDiffuse);

    glActiveTexture(GL_TEXTURE1);
    glUniform1i(pep_Room_samplerNormalMapUniform, 1);
    glBindTexture(GL_TEXTURE_2D, pep_Room_textureNormalMap);

    glActiveTexture(GL_TEXTURE2);
    glUniform1i(pep_Room_samplerAoUniform, 2);
    glBindTexture(GL_TEXTURE_2D, pep_Room_textureAo);

    glUniformMatrix4fv(pep_Room_modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_Room_viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_Room_projectionUniform, 1, GL_FALSE, projectionMatrix);

    glUniform3fv(pep_Room_PointLight_ambientUniform, 1, pep_Room_PointLight_lightAmbient);
    glUniform3fv(pep_Room_PointLight_diffuseUniform, 1, pep_Room_PointLight_lightDiffuse);
    glUniform3fv(pep_Room_PointLight_specularUniform, 1, pep_Room_PointLight_lightSpecular);
    glUniform4fv(pep_Room_PointLight_positionUniform, 1, pep_Room_PointLight_lightPosition);

    glBindVertexArray(pep_Room_vao);
    //glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    //glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    //glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    //glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    //glActiveTexture(GL_TEXTURE0);
    //glUniform1i(pep_Room_samplerDiffuseUniform, 0);
    //glBindTexture(GL_TEXTURE_2D, pep_Room_floorTextureDiffuse);

    //glActiveTexture(GL_TEXTURE1);
    //glUniform1i(pep_Room_samplerNormalMapUniform, 1);
    //glBindTexture(GL_TEXTURE_2D, pep_Room_floorTextureNormalMap);

    //glActiveTexture(GL_TEXTURE2);
    //glUniform1i(pep_Room_samplerAoUniform, 2);
    //glBindTexture(GL_TEXTURE_2D, pep_Room_floorTextureAo);

    //glBindVertexArray(pep_Room_vao);
    //glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    //glBindVertexArray(pep_Room_vao);

    glUseProgram(0);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_TEXTURE_2D);

}
