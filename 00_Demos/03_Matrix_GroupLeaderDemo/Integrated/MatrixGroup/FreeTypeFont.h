#pragma once

#include "Common.h"

int FreeTypeFont_Initialize();
void FreeTypeFont_Uninitialize();
void FreeTypeFont_Update();
void FreeTypeFont_Resize(int width, int height);
void FreeTypeFont_Display(const char *text, float positionX, float positionY, float positionZ, float alpha);
