﻿#include "Common.h"
#include "Shadow.h"
#include "CubeMarching.h"
#include "FreeTypeFont.h"
#include "SceneTransition.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "freetyped.lib")
#pragma comment(lib, "Winmm.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

float lightPosition[4] = {-0.699999332f, 5.88960028f, 14.0132608f, 1.00000000f}/*{0.0f, 3.589553f, -2.286757f, 1.000000f}*/;
//0x00007ff7ef845698 {12.2000113, 5.88960028, 14.0132608, 1.00000000}
HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND gHwnd = NULL;
DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
mat4 pep_perspectiveProjectionMatrix;
bool pep_bFadeOut;
bool gbIsFullScreen;
int pep_gWidth = 800;
int pep_gHeight = 600;
bool pep_CubeMarching_wireframe = true;

int pep_Scene = 0;

extern float gFontColor[3];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow)
{
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);
	void ToggledFullScreen(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("MatrixGroup");

	// code
	if (0 != fopen_s(&pep_gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("MatrixGroup"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, pep_gWidth, pep_gHeight, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (0 != iRet)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	if (!PlaySoundW(L"Peder_B_Helland_Always_Final.wav", NULL,
		SND_NODEFAULT | SND_ASYNC | SND_FILENAME/* | SND_LOOP*/)) {
		fprintf(pep_gpFile, "PlaySound FAILED\n");
	}

	ToggledFullScreen();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// code
	switch (iMsg)
	{
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
		case WM_ERASEBKGND:
			return 0;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			ReSize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;

			case 'F':
			case 'f':
				ToggledFullScreen();
				break;

			case 'w':
			case 'W':
				if (3 == pep_Scene)
				{
					pep_CubeMarching_wireframe = pep_CubeMarching_wireframe ? false : true;
				}
				break;

			case 'S':
			case 's':
				pep_Scene++;
				break;
			case VK_LEFT:
				lightPosition[0] -= 0.1f;
				break;
			case VK_RIGHT:
				lightPosition[0] += 0.1f;
				break;

			case VK_UP:
				lightPosition[1] -= 0.1f;
				break;
			case VK_DOWN:
				lightPosition[1] += 0.1f;
				break;

			case 'Z':
			case 'z':
				lightPosition[2] -= 0.1f;
				break;

			case 'X':
			case 'x':
				lightPosition[2] += 0.1f;
				break;
			}
		} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void Display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (0 == pep_Scene)
	{
		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;

		float yT = 1.0f;
		float xT = -3.250f;
		float zT = -8.0f;
		FreeTypeFont_Display("ASTROMEDiCOMP", xT, yT, zT, 1.0f);

		gFontColor[0] = 1.0f; 
		gFontColor[1] = 1.0f;
		gFontColor[2] = 1.0f;
		yT = 0.0f;
		xT = -3.15f;
		zT = -9.0f;
		FreeTypeFont_Display("(MATRIX GROUP)", xT, yT, zT, 1.0f);

		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;
		yT = -1.0f;
		xT = -2.0f;
		zT = -9.0f;
		FreeTypeFont_Display("PRESENTS", xT, yT, zT, 1.0f);

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}

	}
	else if (1 == pep_Scene)
	{
		float yT = 1.0f;
		float xT = -3.250f;
		float zT = -8.0f;

		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;

		yT = 0.0f;
		xT = -1.850f;
		zT = -9.0f;
		FreeTypeFont_Display("SHADOW", xT, yT, zT, 1.0f);

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (2 == pep_Scene)
	{
		static bool pep_scene_part1 = false;
		static bool pep_scene_part2 = false;
		static bool pep_scene_part3 = false;
		static bool pep_scene_part4 = false;
		static bool pep_scene_part5 = false;
		Shadow_Display();

		if (pep_bFadeOut)
		{
			if (!pep_scene_part1)
			{
				if (lightPosition[0] < 12.2000113f)
				{
					lightPosition[0] += 0.053;
				}
				else
				{
					pep_scene_part1 = true;
				}
			}
			else if (!pep_scene_part2)
			{
				if (lightPosition[2] > -7.68675232f)
				{
					lightPosition[2] -= 0.053;
				}
				else
				{
					pep_scene_part2 = true;
				}
			}
			else if (!pep_scene_part3)
			{
				if (lightPosition[0] > -8.79991913f)
				{
					lightPosition[0] -= 0.053;
				}
				else
				{
					pep_scene_part3 = true;
				}
			}
			else if (!pep_scene_part4)
			{
				if (lightPosition[2] < 13.5131483f)
				{
					lightPosition[2] += 0.053;
				}
				else
				{
					pep_scene_part4 = true;
				}
			}
			else if (!pep_scene_part5)
			{
				if (lightPosition[0] < -0.699999332f)
				{
					lightPosition[0] += 0.053;
				}
				else
				{
					pep_scene_part5 = true;
				}
			}

			if (pep_scene_part5)
			{
				SceneTransition_Display(true, 0.004f); // 0 to 1 => 
			}
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (3 == pep_Scene)
	{
		static bool pep_scene_part1 = false;
		static bool pep_scene_part2 = false;
		static bool pep_scene_part3 = false;
		static bool pep_scene_part4 = false;
		static bool pep_scene_part5 = false;

		Shadow_Display();

		if (pep_bFadeOut)
		{
			if (!pep_scene_part1)
			{
				if (lightPosition[0] < 12.2000113f)
				{
					lightPosition[0] += 0.053;
				}
				else
				{
					pep_scene_part1 = true;
				}
			}
			else if (!pep_scene_part2)
			{
				if (lightPosition[2] > -7.68675232f)
				{
					lightPosition[2] -= 0.053;
				}
				else
				{
					pep_scene_part2 = true;
				}
			}
			else if (!pep_scene_part3)
			{
				if (lightPosition[0] > -8.79991913f)
				{
					lightPosition[0] -= 0.053;
				}
				else
				{
					pep_scene_part3 = true;
				}
			}
			else if (!pep_scene_part4)
			{
				if (lightPosition[2] < 13.5131483f)
				{
					lightPosition[2] += 0.053;
				}
				else
				{
					pep_scene_part4 = true;
				}
			}
			else if (!pep_scene_part5)
			{
				if (lightPosition[0] < -0.699999332f)
				{
					lightPosition[0] += 0.053;
				}
				else
				{
					pep_scene_part5 = true;
				}
			}

			if (pep_scene_part5)
			{
				SceneTransition_Display(true, 0.004f); // 0 to 1 => 
			}
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (4 == pep_Scene)
	{
		float yT = 4.250f;
		float xT = -3.5f;
		float zT = -13.0f;

		gFontColor[0] = 1.0f; 
		gFontColor[1] = 1.0f;
		gFontColor[2] = 1.0f;
		FreeTypeFont_Display("CONCEPTS USED", xT, yT, zT, 1.0f);

		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;
		yT = 4.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("ADS LIGHTS", xT, yT, zT, 1.0f);

		yT = 3.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("TEXTURES", xT, yT, zT, 1.0f);

		yT = 2.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("FRAME BUFFERS", xT, yT, zT, 1.0f);

		yT = 1.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("CUDA INTROP", xT, yT, zT, 1.0f);

		yT = 0.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("MARCHING CUBES ALGORITHM", xT, yT, zT, 1.0f);

		gFontColor[0] = 1.0f; 
		gFontColor[1] = 1.0f;
		gFontColor[2] = 1.0f;
		yT = -1.750f;
		xT = -3.5f;
		zT = -13.0f;
		FreeTypeFont_Display("REFERENCES", xT, yT, zT, 1.0f);

		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;
		yT = -3.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("RED BOOK 8TH EDITION", xT, yT, zT, 1.0f);

		yT = -4.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("SUPERBIBLE BOOK 7TH EDITION", xT, yT, zT, 1.0f);

		yT = -5.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("CUDA SAMPLES 10.1", xT, yT, zT, 1.0f);

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (5 == pep_Scene)
	{
		gFontColor[0] = 1.0f; 
		gFontColor[1] = 1.0f;
		gFontColor[2] = 1.0f;
		float yT = 4.250f;
		float xT = -3.5f;
		float zT = -13.0f;

		
		FreeTypeFont_Display("TECHNOLOGY USED", xT, yT, zT, 1.0f);

		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;

		yT = 4.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("RENDERING:     OPENGL", xT, yT, zT, 1.0f);

		yT = 3.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("HPP:                     CUDA", xT, yT, zT, 1.0f);

		yT = 2.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("LANGUAGE:       C", xT, yT, zT, 1.0f);

		yT = 1.0f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("OS:                        WINDOWS 10", xT, yT, zT, 1.0f);

		gFontColor[0] = 1.0f; 
		gFontColor[1] = 1.0f;
		gFontColor[2] = 1.0f;
		yT = -0.750f;
		xT = -3.5f;
		zT = -13.0f;
		FreeTypeFont_Display("BACKGROUND MUSIC", xT, yT, zT, 1.0f);

		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;
		yT = -2.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("TRACK:        ALWAYS", xT, yT, zT, 1.0f);

		yT = -3.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("BY:                 PEDER B. HELLAND", xT, yT, zT, 1.0f);

		yT = -4.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("SOURCE:      YOUTUBE", xT, yT, zT, 1.0f);

		/*
		yT = -5.250f;
		xT = -3.750f;
		zT = -17.0f;
		FreeTypeFont_Display("                              KISHORE KUMAR", xT, yT, zT, 1.0f);*/

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (6 == pep_Scene)
	{
		gFontColor[0] = 0.529f; 
		gFontColor[1] = 0.807f;
		gFontColor[2] = 0.921f;
		float yT = 1.0f;
		float xT = -3.250f;
		float zT = -8.0f;

		yT = 0.0f;
		xT = -2.50f;
		zT = -9.0f;
		FreeTypeFont_Display("THANK YOU", xT, yT, zT, 1.0f);

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}

	SwapBuffers(gHdc);

	return;
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}
	pep_gWidth = width;
	pep_gHeight = height;

	CubeMarching_Resize(width, height);
	Shadow_Resize(width, height);
	SceneTransition_ReSize(width, height);

	return;
}

void Uninitialize(void)
{
	// code
	SceneTransition_Uninitialize();
	CubeMarching_Uninitialize();
	Shadow_Uninitialize();

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}

	return;
}

void Update(void)
{
	CubeMarching_Update();
	Shadow_Update();
	SceneTransition_Update();
}

int Initialize(void)
{
	// variable declarations
	int pep_CubeMarching_index;
	PIXELFORMATDESCRIPTOR pep_CubeMarching_pfd;

	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pep_CubeMarching_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_CubeMarching_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_CubeMarching_pfd.nVersion = 1;
	pep_CubeMarching_pfd.cColorBits = 32;
	pep_CubeMarching_pfd.cDepthBits = 8;
	pep_CubeMarching_pfd.cRedBits = 8;
	pep_CubeMarching_pfd.cGreenBits = 8;
	pep_CubeMarching_pfd.cBlueBits = 8;
	pep_CubeMarching_pfd.cAlphaBits = 8;
	pep_CubeMarching_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	pep_CubeMarching_index = ChoosePixelFormat(gHdc, &pep_CubeMarching_pfd);
	if (0 == pep_CubeMarching_index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, pep_CubeMarching_index, &pep_CubeMarching_pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum pep_CubeMarching_result;
	pep_CubeMarching_result = glewInit();
	if (GLEW_OK != pep_CubeMarching_result)
	{
		return -5;
	}

	Shadow_Initialize(pep_gWidth, pep_gHeight);
	CubeMarching_Initialize();
	FreeTypeFont_Initialize();
	SceneTransition_Initialize();

	//glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	pep_perspectiveProjectionMatrix = mat4::identity();

	ReSize(pep_gWidth, pep_gHeight);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}
