#pragma once

//#define CIRCLE_DEGREE 360
//#define NO_VERTICES_IN_QUAD 4
//#define COORDINATE_SYSTEM_3D 3
//#define PI 3.14159
//
//void GetSphereData(float sphere_radius, int sphere_no_stacks, int sphere_no_sectors, float **sphere_vertices, float **sphere_normals);
//

extern "C" void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);

extern "C" unsigned int getNumberOfSphereVertices(void);

extern "C" unsigned int getNumberOfSphereElements(void);
