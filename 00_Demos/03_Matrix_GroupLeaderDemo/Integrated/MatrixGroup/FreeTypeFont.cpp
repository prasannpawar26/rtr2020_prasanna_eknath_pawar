
#include "FreeTypeFont.h"

#include <ft2build.h>
#include FT_FREETYPE_H

extern FILE* pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;
extern int pep_gWidth;
extern int pep_gHeight;

//
#define NUM_CHARS 128
typedef struct _CHARACTER_ {
	GLuint TextureID;  // ID handle of the glyph texture
	vmath::uvec2 Size;         // Size of glyph
	vmath::uvec2 Bearing;      // Offset from baseline to left/top of glyph
	GLuint Advance;    // Horizontal offset to advance to next glyph
}CHARACTER;

CHARACTER gFontCharacter[NUM_CHARS];

//float gFontColor[3] = {0.5f, 0.8f, 0.2f};
float gFontColor[3] = {0.529f, 0.807f, 0.921f};

//
// Font Shader Related Variables
//
GLuint pep_Font_vertexShaderObject;
GLuint pep_Font_fragmentShaderObject;
GLuint pep_Font_shaderProgramObject;

//
// UNIFORMS
//
GLuint pep_Font_modelMatrixUniform;
GLuint pep_Font_viewMatrixUniform;
GLuint pep_Font_projectionMatrixUniform;
GLuint pep_Font_samplerUniform;
GLuint pep_Font_textColorUniform;
GLuint pep_Font_alphaUniform;

GLuint pep_Font_vao;
GLuint pep_Font_vbo;

char pep_Font_RenterText[3][255];

int LoadFreeType(void)
{
	//
	// Code
	//
	FT_Library fontLibrary;
	if (FT_Init_FreeType(&fontLibrary))
	{
		fprintf(pep_gpFile, "ERROR::FREETYPE: Could not init FreeType Library\n");
		return -1;
	}

	FT_Face face;
	if (FT_New_Face(fontLibrary, "./Fonts_Files/timesbd.ttf", 0, &face))
		/*if (FT_New_Face(fontLibrary, "./Fonts_Files/Cinzel-Regular.ttf", 0, &face))*/
	{
		fprintf(pep_gpFile, "ERROR::FREETYPE: Failed to load font\n");
		FT_Done_FreeType(fontLibrary);
		return -1;
	}

	FT_Set_Pixel_Sizes(face, 0, 50);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); 

	// load first 128 characters of ASCII set
	for (unsigned int c = 0; c < NUM_CHARS; c++)
	{
		// Load character glyph
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			fprintf(pep_gpFile, "ERROR::FREETYTPE: Failed to load Glyph\n");
			continue;
		}

		// Generate Texture
		GLuint char_texture;
		glGenTextures(1, &char_texture);

		glBindTexture(GL_TEXTURE_2D, char_texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width,
			face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		gFontCharacter[c].TextureID = char_texture;
		gFontCharacter[c].Size = vmath::uvec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);
		gFontCharacter[c].Bearing =	vmath::uvec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
		gFontCharacter[c].Advance = face->glyph->advance.x;

	} // End Of For Loop
	glBindTexture(GL_TEXTURE_2D, 0);
	FT_Done_Face(face);
	FT_Done_FreeType(fontLibrary);

	return 0;
}

int FreeTypeFont_Initialize()
{
	LoadFreeType();

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \
		"out vec2 out_textureCoord;" \

		"uniform mat4 model_matrix;" \
		"uniform mat4 view_matrix;" \
		"uniform mat4 projection_matrix;" \

		"void main(void)" \
		"{" \
		"gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vPosition.xy, 0.0, 1.0);" \
		"out_textureCoord = vPosition.zw;" \
		"}";

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec2 out_textureCoord;" \
		"out vec4 FragColor;" \

		"uniform sampler2D texture0_sampler_u;" \
		"uniform vec3 textColor_u;" \
		"uniform float u_alpha;"

		"void main(void)" \
		"{" \
		"vec4 sampled = vec4(1.0,1.0,1.0,texture(texture0_sampler_u, out_textureCoord).r);" \
		"FragColor = vec4(textColor_u,1.0)* sampled * vec4(u_alpha);" \
		"}";

	pep_Font_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_Font_vertexShaderObject) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -1;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(pep_Font_vertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_Font_vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Font_vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(pep_Font_vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Font_vertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -2;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_Font_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_Font_fragmentShaderObject) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -3;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(pep_Font_fragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_Font_fragmentShaderObject);

	glGetShaderiv(pep_Font_fragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(pep_Font_fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Font_fragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -4;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_Font_shaderProgramObject = glCreateProgram();

	glAttachShader(pep_Font_shaderProgramObject, pep_Font_vertexShaderObject);
	glAttachShader(pep_Font_shaderProgramObject, pep_Font_fragmentShaderObject);

	// Attributes Binding Before Linking
	glBindAttribLocation(pep_Font_shaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(pep_Font_shaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_Font_shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(pep_Font_shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(pep_Font_shaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -5;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	pep_Font_modelMatrixUniform =
		glGetUniformLocation(pep_Font_shaderProgramObject, "model_matrix");
	pep_Font_viewMatrixUniform =
		glGetUniformLocation(pep_Font_shaderProgramObject, "view_matrix");
	pep_Font_projectionMatrixUniform =
		glGetUniformLocation(pep_Font_shaderProgramObject, "projection_matrix");

	pep_Font_samplerUniform =
		glGetUniformLocation(pep_Font_shaderProgramObject, "texture0_sampler_u");
	pep_Font_textColorUniform =
		glGetUniformLocation(pep_Font_shaderProgramObject, "textColor_u");

	pep_Font_alphaUniform =
		glGetUniformLocation(pep_Font_shaderProgramObject, "u_alpha");

	glGenVertexArrays(1, &pep_Font_vao);
	glBindVertexArray(pep_Font_vao);
	glGenBuffers(1, &pep_Font_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Font_vbo);

	// The 2D quad requires 6 vertices of 4 floats each so we reserve 6 * 4 floats
	// of memory. 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE,
		4 * sizeof(GLfloat), 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	for (int i = 0; i < 3; i++)
	{
		memset(pep_Font_RenterText[i], 0, 255);
	}

	return 0;
}

void FreeTypeFont_Uninitialize()
{
	//
	// Code
	//
	for (unsigned char c = 0; c < NUM_CHARS; c++)
	{
		glDeleteTextures(1, &gFontCharacter[(int)c].TextureID);
	}

	if (pep_Font_shaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_Font_shaderProgramObject);

		glGetProgramiv(pep_Font_shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_Font_shaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_Font_shaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_Font_shaderProgramObject);
		pep_Font_shaderProgramObject = 0;

		glUseProgram(0);
	}

	return;
}

void FreeTypeFont_Update()
{

}

void FreeTypeFont_Resize(int width, int height)
{

}

void FreeTypeFont_Display(const char *text, float positionX, float positionY, float positionZ, float alpha)
{
	//
	//Code
	//
	GLfloat x = 0.0f;
	GLfloat y = 0.0f;
	GLfloat scale = 0.015f;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glActiveTexture(GL_TEXTURE0);
	glUseProgram(pep_Font_shaderProgramObject);

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	modelMatrix = modelMatrix * translate(positionX, positionY, positionZ);
	pep_perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 1000.0f);
	glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

	glUniformMatrix4fv(pep_Font_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_Font_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_Font_projectionMatrixUniform, 1, GL_FALSE,
		pep_perspectiveProjectionMatrix);

	glUniform1i(pep_Font_samplerUniform, 0);
	glUniform3f(pep_Font_textColorUniform, gFontColor[0], gFontColor[1], gFontColor[2]);
	glUniform1f(pep_Font_alphaUniform, alpha);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(pep_Font_vao);

	for (unsigned int i = 0; i < strlen(text); i++)
	{
		char c = text[i];
		CHARACTER fc = gFontCharacter[c];

		GLfloat xpos = x + fc.Bearing[0] * scale;
		GLfloat ypos = y - (fc.Size[1] - fc.Bearing[1]) * scale;

		GLfloat w = fc.Size[0] * scale;
		GLfloat h = fc.Size[1] * scale;

		GLfloat vertices[6][4] =
		{
			{ xpos,     ypos + h,   0.0, 0.0 },
		{ xpos,     ypos,       0.0, 1.0 },
		{ xpos + w, ypos,       1.0, 1.0 },

		{ xpos,     ypos + h,   0.0, 0.0 },
		{ xpos + w, ypos,       1.0, 1.0 },
		{ xpos + w, ypos + h,   1.0, 0.0 }
		};

		glBindTexture(GL_TEXTURE_2D, fc.TextureID);

		glBindBuffer(GL_ARRAY_BUFFER, pep_Font_vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices),vertices); 
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindTexture(GL_TEXTURE_2D, 0);

		x += (fc.Advance >> 6) * scale;
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glDisable(GL_BLEND);

	glUseProgram(0);
}
