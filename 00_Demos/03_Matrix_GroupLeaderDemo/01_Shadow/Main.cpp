#include "Common.h"
#include "Shadow.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

bool gbIsShadowEnable = true;

//float lightPosition[4] = {2.624089f, 3.589553f, -2.286757f, 1.000000f};
float lightPosition[4] = {0.0f, 3.589553f, -2.286757f, 1.000000f};
const int WIDTH = 800;
const int HEIGHT = 600;

int pep_gWidth;
int pep_gHeight;

HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;

DWORD pep_dwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool pep_gbActiveWindow;
bool pep_gbIsFullScreen = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpSzCmdLine, int iCmdShow)
{
  // function prototype
  int Initialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("RenderToTexture");

  // code
  if (0 != fopen_s(&pep_gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }
  fprintf(pep_gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("RenderToTexture"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, WIDTH, HEIGHT, NULL, NULL, hInstance, NULL);
  pep_gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (0 != iRet)
  {
    fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  }
  else
  {
    fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
            "Successful.\n",
            __FILE__, __LINE__, __FUNCTION__);
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
      if (WM_QUIT == msg.message)
	  {
        bDone = true;
      }
	  else
	  {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
	else
	{
      if (pep_gbActiveWindow)
	  {
        Update();
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  POINT pt;

  static int old_x_pos;
  static int old_y_pos;

  static int new_x_pos;
  static int new_y_pos;

  static int x_offset;
  static int y_offset;

  // code
  switch (iMsg) {
  case WM_CREATE:
      break;
    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_KILLFOCUS:
      pep_gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      pep_gbActiveWindow = true;
      break;
    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
	  case VK_LEFT:
		  lightPosition[0] -= 0.1f;
		  break;
	  case VK_RIGHT:
		  lightPosition[0] += 0.1f;
		  break;

	  case VK_UP:
		  lightPosition[1] -= 0.1f;
		  break;
	  case VK_DOWN:
		  lightPosition[1] += 0.1f;
		  break;

      case 'Z':
	  case 'z':
		  lightPosition[2] -= 0.1f;
		  break;

	  case 'X':
	  case 'x':
		  lightPosition[2] += 0.1f;
		  break;

        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;
      }
    } break;
  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ReSize(int width, int height)
{
  // code
  if (0 == height)
  {
    height = 1;
  }

  pep_gWidth = width;
  pep_gHeight = height;

  Shadow_Resize(pep_gWidth, pep_gHeight);

  return;
}

void Display(void)
{
	Shadow_Display();
    //Room_Display();

	SwapBuffers(pep_gHdc);
}

void Uninitialize(void)
{
	Shadow_Uninitialize();

	if (wglGetCurrentContext() == pep_gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
	}

	fprintf(
		pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		  fclose(pep_gpFile);
	}

  return;
}

void Update(void)
{
	Shadow_Update();
    //Room_Update();
}

int Initialize(void)
{
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
			__FILE__, __LINE__, __FUNCTION__);

	pep_gHdc = GetDC(pep_gHwnd);

	index = ChoosePixelFormat(pep_gHdc, &pfd);
	if (0 == index)
	{
		return -1;
	}

	if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd))
	{
		return -1;
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		return -1;
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc)) {
		return -1;
	}

	fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
			__FILE__, __LINE__, __FUNCTION__);

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result)
	{
		return -1;
	}

	Shadow_Initialize(WIDTH, HEIGHT);
    //Room_Initialize(WIDTH, HEIGHT);

	ReSize(WIDTH, HEIGHT);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
			__FILE__, __LINE__, __FUNCTION__);
	return 0;
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == pep_gbIsFullScreen)
  {
    pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
	{
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY),
                         &mi))
	  {
        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    pep_gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
    SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    pep_gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}


