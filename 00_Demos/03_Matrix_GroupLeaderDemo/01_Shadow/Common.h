#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

#include <stdio.h>
#include <windows.h>
#include <Windowsx.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"
#include "Sphere.h"

#define STB_IMAGE_IMPLEMENTATION

using namespace std;
using namespace vmath;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXTURE_DIFFUSE_COORD0,
	AMC_ATTRIBUTES_TANGENT,
	AMC_ATTRIBUTES_BITANGENT,
};
