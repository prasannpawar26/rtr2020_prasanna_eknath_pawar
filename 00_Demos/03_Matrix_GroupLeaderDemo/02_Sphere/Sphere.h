#pragma once

#pragma once

int Sphere_Intialize(void);
void Sphere_Unintialize(void);
void Sphere_Display(void);
void Sphere_Resize(int, int);
void Sphere_Update();
