
#include "Common.h"
#include "Sphere.h"

#define CIRCLE_DEGREE 360
#define NO_VERTICES_IN_QUAD 4
#define COORDINATE_SYSTEM_3D 3
#define PI 3.14159

extern mat4 perspectiveProjectionMatrix;
extern FILE *gpFile;
extern int pep_gWidth;
extern int pep_gHeight;

GLuint pep_Sphere_vertexShaderObject;
GLuint pep_Sphere_fragmentShaderObject;
GLuint pep_Sphere_shaderProgramObject;
GLuint pep_Sphere_modelMatrixUniform;
GLuint pep_Sphere_viewMatrixUniform;
GLuint pep_Sphere_projectionMatrixUniform;
GLuint pep_Sphere_lightDiffuseUniform;
GLuint pep_Sphere_materialDiffuseUniform;
GLuint pep_Sphere_lightPositionUniform;

GLfloat rotation_angle_cube;

// Sphere Properties
float sphere_radius = 1.0f;
int sphere_no_stacks = 20;// = 180; // 20;
int sphere_no_sectors = 60;//360; // no of vertices per stack ring
float *sphere_vertices;
float *sphere_normals;

GLuint sphere_vao;
GLuint sphere_vbo_vertex;
GLuint sphere_vbo_normal;

// TODO: If One Of Radius Is Zero Then Lighting Will Not Work. Need To Consider Normal Calculation Again
int no_of_surface = 360;
float top_circle_radius = 1.0f;
float bottom_circle_radius = 1.0f;
float height = 2.0f;
GLfloat *vertices;
GLfloat *normals;

float DegToRad(const float& deg)
{
	return deg * (M_PI / 180.0f);
}

void getSphereData(float sphere_radius, int sphere_no_stacks, int sphere_no_sectors, float **sphere_vertices, float **sphere_normals)
{
	float stack_angle = 89.99999f; // range from 90 to -90   // Range From +Y To -Y
	float stack_angle_factor = 180.0f / sphere_no_stacks;
	float sector_angle = 0.0f; // range from 0 to 360;
	float sector_angle_step = CIRCLE_DEGREE / sphere_no_sectors;

	int no_of_sphere_vertices = sphere_no_sectors * COORDINATE_SYSTEM_3D * (sphere_no_stacks + 1);
	int size_of_sphere_vertices_array = no_of_sphere_vertices  *  sizeof(float);

	float *temp_vertices = (float *)malloc(size_of_sphere_vertices_array);
	memset(temp_vertices, 0, size_of_sphere_vertices_array);

	for (int stack_i = 0; stack_i <=  sphere_no_stacks; stack_i++) // Need To Correct
	{
		if (stack_i == sphere_no_stacks)
		{
			stack_angle = -89.99999f;
		}

		sector_angle = 0.0f;
		for (int sector_i = 0; sector_i < sphere_no_sectors * COORDINATE_SYSTEM_3D; sector_i +=3)
		{
			temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + sector_i + 0] = (sphere_radius * cos(DegToRad(stack_angle))) * cos(DegToRad(sector_angle));
			temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + sector_i + 1] = (sphere_radius * sin(DegToRad(stack_angle)));
			temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + sector_i + 2] =  (sphere_radius * cos(DegToRad(stack_angle))) * sin(DegToRad(sector_angle));

			sector_angle += sector_angle_step;
		}
		stack_angle -= stack_angle_factor;
	}

	int index = 0;

	int stack_next_i = 0;
	for (int stack_i = 0 ; stack_i < sphere_no_stacks; stack_i++)
	{
		stack_next_i = stack_i + 1;
		int stride_i = 0;
		for (int sector_i = 0; sector_i < sphere_no_sectors; sector_i++) // i.e. No. Of Vertices In Each Sector
		{
			if (stride_i + 3 == sphere_no_sectors * COORDINATE_SYSTEM_3D)
			{
				// Counter-Clockwise Order
				(*sphere_vertices)[index + 0] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + 0];
				(*sphere_vertices)[index + 1] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + 1];
				(*sphere_vertices)[index + 2] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + 2];
				(*sphere_vertices)[index + 3] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 0];
				(*sphere_vertices)[index + 4] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 1];
				(*sphere_vertices)[index + 5] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 2];

				(*sphere_vertices)[index + 6] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 0];
				(*sphere_vertices)[index + 7] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 1];
				(*sphere_vertices)[index + 8] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 2];
				(*sphere_vertices)[index + 9] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + 0];
				(*sphere_vertices)[index + 10] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + 1];
				(*sphere_vertices)[index + 11] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + 2];

			}
			else
			{
				// Counter-Clockwise Order
				(*sphere_vertices)[index + 0] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 3];
				(*sphere_vertices)[index + 1] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 4];
				(*sphere_vertices)[index + 2] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 5];
				(*sphere_vertices)[index + 3] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 0];
				(*sphere_vertices)[index + 4] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 1];
				(*sphere_vertices)[index + 5] = temp_vertices[stack_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 2];

				(*sphere_vertices)[index + 6] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 0];
				(*sphere_vertices)[index + 7] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 1];
				(*sphere_vertices)[index + 8] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 2];
				(*sphere_vertices)[index + 9] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 3];
				(*sphere_vertices)[index + 10] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 4];
				(*sphere_vertices)[index + 11] = temp_vertices[stack_next_i * sphere_no_sectors * COORDINATE_SYSTEM_3D + stride_i + 5];
			}


			// Normal Calculations
			vec3 v0((*sphere_vertices)[index + 0], (*sphere_vertices)[index + 1], (*sphere_vertices)[index + 2]);
			vec3 v1((*sphere_vertices)[index + 3], (*sphere_vertices)[index + 4], (*sphere_vertices)[index + 5]);
			vec3 v2((*sphere_vertices)[index + 6], (*sphere_vertices)[index + 7], (*sphere_vertices)[index + 8]);
			vec3 normal = normalize(cross(v2 - v0, v1 - v0));
			(*sphere_normals)[index + 0] = normal[0];
			(*sphere_normals)[index + 1] = normal[1];
			(*sphere_normals)[index + 2] = normal[2];

			v0 = vec3((*sphere_vertices)[index + 3], (*sphere_vertices)[index + 4], (*sphere_vertices)[index + 5]);
			v1 = vec3((*sphere_vertices)[index + 6], (*sphere_vertices)[index + 7], (*sphere_vertices)[index + 8]);
			v2 = vec3((*sphere_vertices)[index + 9], (*sphere_vertices)[index + 10], (*sphere_vertices)[index + 11]);
			normal = normalize(cross(v2 - v0, v1 - v0));
			(*sphere_normals)[index + 3] = normal[0];
			(*sphere_normals)[index + 4] = normal[1];
			(*sphere_normals)[index + 5] = normal[2];

			v0 = vec3((*sphere_vertices)[index + 6], (*sphere_vertices)[index + 7], (*sphere_vertices)[index + 8]);
			v1 = vec3((*sphere_vertices)[index + 9], (*sphere_vertices)[index + 10], (*sphere_vertices)[index + 11]);
			v2 = vec3((*sphere_vertices)[index + 0], (*sphere_vertices)[index + 1], (*sphere_vertices)[index + 2]);
			normal = normalize(cross(v2 - v0, v1 - v0));
			(*sphere_normals)[index + 6] = normal[0];
			(*sphere_normals)[index + 7] = normal[1];
			(*sphere_normals)[index + 8] = normal[2];

			v0 = vec3((*sphere_vertices)[index + 9], (*sphere_vertices)[index + 10], (*sphere_vertices)[index + 11]);
			v1 = vec3((*sphere_vertices)[index + 0], (*sphere_vertices)[index + 1], (*sphere_vertices)[index + 2]);
			v2 = vec3((*sphere_vertices)[index + 3], (*sphere_vertices)[index + 4], (*sphere_vertices)[index + 5]);
			normal = normalize(cross(v2 - v0, v1 - v0));
			(*sphere_normals)[index + 9] = normal[0];
			(*sphere_normals)[index + 10] = normal[1];
			(*sphere_normals)[index + 11] = normal[2];

			stride_i += 3;
			index += 12;
		}
	}

	free(temp_vertices);

	return;
}

int Sphere_Intialize(void)
{
	pep_Sphere_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_Sphere_vertexShaderObject)
	{

		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec3 u_light_diffuse;" \
		"uniform vec3 u_material_diffuse;" \
		"uniform vec4 u_light_position;" \
		"out vec3 diffuse_color;" \
		"void main(void)" \
		"{" \
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
			"mat3 normalMatrix = mat3(transpose(inverse(u_view_matrix * u_model_matrix)));" \
			"vec3 t_normal = normalize(normalMatrix * vNormal);" \
			"vec3 lightSource = vec3(vec3(u_light_position) - eye_coordinates.xyz);" \
			"diffuse_color = u_light_diffuse * u_material_diffuse * dot(lightSource, t_normal);" \

			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(pep_Sphere_vertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_Sphere_vertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Sphere_vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(pep_Sphere_vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Sphere_vertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -7;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_Sphere_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_Sphere_fragmentShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 diffuse_color;" \
		"out vec4 FragColor;" \

		"void main(void)" \
		"{" \
			"FragColor = vec4(diffuse_color, 1.0);" \
		"}";

	glShaderSource(pep_Sphere_fragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_Sphere_fragmentShaderObject);

	glGetShaderiv(pep_Sphere_fragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(pep_Sphere_fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_Sphere_fragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -9;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_Sphere_shaderProgramObject = glCreateProgram();

	glAttachShader(pep_Sphere_shaderProgramObject, pep_Sphere_vertexShaderObject);
	glAttachShader(pep_Sphere_shaderProgramObject, pep_Sphere_fragmentShaderObject);

	glBindAttribLocation(pep_Sphere_shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glBindAttribLocation(pep_Sphere_shaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(pep_Sphere_shaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_Sphere_shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(pep_Sphere_shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_Sphere_shaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -10;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_Sphere_modelMatrixUniform =
		glGetUniformLocation(pep_Sphere_shaderProgramObject, "u_model_matrix");
	pep_Sphere_viewMatrixUniform =
		glGetUniformLocation(pep_Sphere_shaderProgramObject, "u_view_matrix");
	pep_Sphere_projectionMatrixUniform =
		glGetUniformLocation(pep_Sphere_shaderProgramObject, "u_projection_matrix");
	pep_Sphere_lightDiffuseUniform =
		glGetUniformLocation(pep_Sphere_shaderProgramObject, "u_light_diffuse");
	pep_Sphere_materialDiffuseUniform =
		glGetUniformLocation(pep_Sphere_shaderProgramObject, "u_material_diffuse");
	pep_Sphere_lightPositionUniform =
		glGetUniformLocation(pep_Sphere_shaderProgramObject, "u_light_position");

	//sphere_vertices
	int size_of_sphere_vertices_array = sphere_no_sectors * COORDINATE_SYSTEM_3D * sphere_no_stacks * NO_VERTICES_IN_QUAD * sizeof(float);

	sphere_vertices = (float *)malloc(size_of_sphere_vertices_array);
	memset(sphere_vertices, 0, size_of_sphere_vertices_array);

	sphere_normals = (float *)malloc(size_of_sphere_vertices_array);
	memset(sphere_normals, 0, size_of_sphere_vertices_array);

	getSphereData(sphere_radius, sphere_no_stacks, sphere_no_stacks, &sphere_vertices, &sphere_normals);
	//cube
	glGenVertexArrays(1, &sphere_vao);

	glBindVertexArray(sphere_vao);

	glGenBuffers(1, &sphere_vbo_vertex);

	glBindBuffer(GL_ARRAY_BUFFER, sphere_vbo_vertex);

	glBufferData(GL_ARRAY_BUFFER, size_of_sphere_vertices_array, sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &sphere_vbo_normal);
	glBindBuffer(GL_ARRAY_BUFFER, sphere_vbo_normal);
	glBufferData(GL_ARRAY_BUFFER, size_of_sphere_vertices_array, sphere_normals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return 0;
}

void Sphere_Unintialize(void)
{

	if (sphere_vbo_normal)
	{
		glDeleteBuffers(1, &sphere_vbo_normal);
		sphere_vbo_normal = 0;
	}

	if (sphere_vbo_vertex)
	{
		glDeleteBuffers(1, &sphere_vbo_vertex);
		sphere_vbo_vertex = 0;
	}

	if (sphere_vao)
	{
		glDeleteVertexArrays(1, &sphere_vao);
		sphere_vao = 0;
	}

	if (pep_Sphere_shaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_Sphere_shaderProgramObject);

		glGetProgramiv(pep_Sphere_shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_Sphere_shaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_Sphere_shaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_Sphere_shaderProgramObject);
		pep_Sphere_shaderProgramObject = 0;

		glUseProgram(0);
	}
}

void Sphere_Display(void)
{
	glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

	perspectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 100.0f);

	glUseProgram(pep_Sphere_shaderProgramObject);

	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 translateMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	// Sphere
	translateMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translateMatrix = translate(0.0f, 0.0f, -5.0f);
	rotationMatrix = rotate(rotation_angle_cube, 1.0f, 0.0f, 0.0f);
	modelMatrix = translateMatrix * rotationMatrix * scaleMatrix;

	glUniformMatrix4fv(pep_Sphere_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_Sphere_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_Sphere_projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	glUniform3f(pep_Sphere_lightDiffuseUniform, 0.0f, 0.0f, 1.0f);
	glUniform3f(pep_Sphere_materialDiffuseUniform, 0.0f, 0.0f, 0.10f);
	glUniform4f(pep_Sphere_lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);

	glBindVertexArray(sphere_vao);

	for (int i = 0; i < sphere_no_sectors * sphere_no_stacks; i++)
	{
		glDrawArrays(GL_TRIANGLE_FAN,  i * 4, 4);
	}

	glBindVertexArray(0);

	glUseProgram(0);

}

void Sphere_Resize(int width, int height)
{

}

void Sphere_Update()
{
	if (360.0f < rotation_angle_cube)
	{
		rotation_angle_cube = 0.0f;
	}
	rotation_angle_cube += 1.0f;
}
