
#include "Common.h"
#include "Sphere.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbActiveWindow;
bool gbIsFullScreen;
mat4 perspectiveProjectionMatrix;
int pep_gWidth = 800;
int pep_gHeight = 600;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow)
{
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	bool bDone = false;
	MSG msg = { 0 };
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("Sphere");

	// code
	if (0 != fopen_s(&gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("Sphere"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, pep_gWidth, pep_gHeight, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (0 != iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// code
	switch (iMsg)
	{
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
		case WM_ERASEBKGND:
			return 0;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_SIZE:
			ReSize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;

				case 'F':
				case 'f':
					ToggledFullScreen();
					break;
			}
		} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}

	pep_gWidth = width;
	pep_gHeight = height;

	Sphere_Resize(pep_gWidth, pep_gHeight);

	return;
}

void Display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Sphere_Display();

	SwapBuffers(gHdc);
}

void Uninitialize(void)
{
	// code

	Sphere_Unintialize();

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc) {
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != gpFile)
	{
		fclose(gpFile);
	}

	return;
}

void Update(void)
{
	Sphere_Update();
	return;
}

int Initialize(void)
{
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index)
	{
		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : ChoosePixelFormat Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	if (FALSE == SetPixelFormat(gHdc, index, &pfd))
	{
		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : SetPixelFormat Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	if (FALSE == wglMakeCurrent(gHdc, gHglrc))
	{
		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result)
	{
		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	if (0 != Sphere_Intialize())
	{
		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Sphere_Intialize Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	ReSize(800, 600);

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}
