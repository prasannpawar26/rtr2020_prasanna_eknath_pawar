﻿#include <stdio.h>
#include <string.h>
#include <helper_cuda.h>    // includes for helper CUDA functions
#include <helper_math.h>
#include <cuda_runtime_api.h>

#include "defines.h"
#include "tables.h"

//
// Global Declarations For Cuda Kernel
//
texture<uint, 1, cudaReadModeElementType> pep_CubeMarching_triangle_table_texture; // textures containing look-up tables
texture<uint, 1, cudaReadModeElementType> pep_CubeMarching_number_of_vertices_texture; // textures containing look-up tables
texture<uchar, 1, cudaReadModeNormalizedFloat> pep_CubeMarching_volume_data_texture; // volume data

//
// Kernel Function Declarations
//
__device__ float SampleVolume(uint3 p, uint3 gridSize);

__device__ uint3 Convert1DIndexInto3DIndex(uint index);

__global__ void Kernel_ClassifyVoxel(uint *voxelVerts, uint *voxelOccupied,
	uchar *volume, uint3 gridSize, uint numVoxels,
	float3 voxelSize, float isoValue);

__global__ void Kernel_CompactVoxels(uint *compactedVoxelArray, uint *voxelOccupied,
	uint *voxelOccupiedScan, uint numVoxels);

__device__ float3 vertexInterp(float isolevel, float3 p0, float3 p1, float f0,
	float f1);

__device__ float3 calcNormal(float3 *v0, float3 *v1, float3 *v2);

__device__ float SampleVolume(uint3 p, uint3 gridSize) {
	p.x = min(p.x, gridSize.x - 1);
	p.y = min(p.y, gridSize.y - 1);
	p.z = min(p.z, gridSize.z - 1);

	uint i = (p.z * gridSize.x * gridSize.y) + (p.y * gridSize.x) + p.x;

	return tex1Dfetch(pep_CubeMarching_volume_data_texture, i);
}

//
// compute position in 3d grid from 1d index
// only works for power of 2 sizes
//
__device__ uint3 Convert1DIndexInto3DIndex(uint index) {
	uint3 gridPos;
	gridPos.x = index % 32;
	gridPos.y = (index / 32) % 32;
	gridPos.z = index / (32 * 32);

	return gridPos;
}

//
// classify voxel based on number of vertices it will generate
// one thread per voxel
//
__global__ void Kernel_ClassifyVoxel(uint *voxelVerts, uint *voxelOccupied,
	uchar *volume, uint3 gridSize, uint numVoxels,
	float3 voxelSize, float isoValue) {
	//
	// blockDim.x => block dimensions i.e thread per block.
	// blockIdx.x, blockIdx.y => Block Index In Grid
	// threadIdx -> thread index in block
	// gridDim.x  => grid dimensions
	//
	uint blockId = __mul24(blockIdx.y, gridDim.x) + blockIdx.x;
	uint i = __mul24(blockId, blockDim.x) + threadIdx.x;

	uint3 gridPos = Convert1DIndexInto3DIndex(i);

	// read field values at neighbouring grid vertices
	float field[8];
	field[0] = SampleVolume(gridPos + make_uint3(0, 0, 0), gridSize);
	field[1] = SampleVolume(gridPos + make_uint3(1, 0, 0), gridSize);
	field[2] = SampleVolume(gridPos + make_uint3(1, 1, 0), gridSize);
	field[3] = SampleVolume(gridPos + make_uint3(0, 1, 0), gridSize);
	field[4] = SampleVolume(gridPos + make_uint3(0, 0, 1), gridSize);
	field[5] = SampleVolume(gridPos + make_uint3(1, 0, 1), gridSize);
	field[6] = SampleVolume(gridPos + make_uint3(1, 1, 1), gridSize);
	field[7] = SampleVolume(gridPos + make_uint3(0, 1, 1), gridSize);

	// calculate flag indicating if each vertex is inside or outside isosurface
	uint cubeindex;
	cubeindex = uint(field[0] < isoValue);
	cubeindex += uint(field[1] < isoValue) * 2;
	cubeindex += uint(field[2] < isoValue) * 4;
	cubeindex += uint(field[3] < isoValue) * 8;
	cubeindex += uint(field[4] < isoValue) * 16;
	cubeindex += uint(field[5] < isoValue) * 32;
	cubeindex += uint(field[6] < isoValue) * 64;
	cubeindex += uint(field[7] < isoValue) * 128;

	uint numVerts = tex1Dfetch(pep_CubeMarching_number_of_vertices_texture, cubeindex);

	if (i < numVoxels) {
		voxelVerts[i] = numVerts;
		voxelOccupied[i] = (numVerts > 0);
	}

	return;
}

//
// compact voxel array
//
__global__ void Kernel_CompactVoxels(uint *compactedVoxelArray, uint *voxelOccupied,
	uint *voxelOccupiedScan, uint numVoxels) {
	uint blockId = __mul24(blockIdx.y, gridDim.x) + blockIdx.x;
	uint i = __mul24(blockId, blockDim.x) + threadIdx.x;

	if (voxelOccupied[i] && (i < numVoxels)) {
		compactedVoxelArray[voxelOccupiedScan[i]] = i;
	}

	return;
}

//
// compute interpolated vertex along an edge
//
__device__ float3 vertexInterp(float isolevel, float3 p0, float3 p1, float f0,
	float f1) {

	float t = (isolevel - f0) / (f1 - f0);

	return lerp(p0, p1, t);
}

//
// calculate triangle normal
//
__device__ float3 calcNormal(float3 *v0, float3 *v1, float3 *v2) {

	float3 edge0 = *v1 - *v0;
	float3 edge1 = *v2 - *v0;
	// note - it's faster to perform normalization in vertex shader rather than
	// here
	return cross(edge0, edge1);
}

//
// version that calculates flat surface normal for each triangle
//
__global__ void generateTriangles2(float4 *pos, float4 *norm,
	uint *compactedVoxelArray,
	uint *numVertsScanned, uchar *volume,
	uint3 gridSize, float3 voxelSize,
	float isoValue, uint activeVoxels,
	uint maxVerts) {
	uint blockId = __mul24(blockIdx.y, gridDim.x) + blockIdx.x;
	uint i = __mul24(blockId, blockDim.x) + threadIdx.x;

	if (i > activeVoxels - 1) {
		i = activeVoxels - 1;
	}

	uint voxel = compactedVoxelArray[i];

	// compute position in 3d grid
	uint3 gridPos = Convert1DIndexInto3DIndex(voxel);

	float3 p;
	p.x = -1.0f + (gridPos.x * voxelSize.x);
	p.y = -1.0f + (gridPos.y * voxelSize.y);
	p.z = -1.0f + (gridPos.z * voxelSize.z);

	// calculate voxel's vertices positions
	float3 v[8];
	v[0] = p;
	v[1] = p + make_float3(voxelSize.x, 0, 0);
	v[2] = p + make_float3(voxelSize.x, voxelSize.y, 0);
	v[3] = p + make_float3(0, voxelSize.y, 0);
	v[4] = p + make_float3(0, 0, voxelSize.z);
	v[5] = p + make_float3(voxelSize.x, 0, voxelSize.z);
	v[6] = p + make_float3(voxelSize.x, voxelSize.y, voxelSize.z);
	v[7] = p + make_float3(0, voxelSize.y, voxelSize.z);

	float field[8];
	field[0] = SampleVolume(gridPos + make_uint3(0, 0, 0), gridSize);
	field[1] = SampleVolume(gridPos + make_uint3(1, 0, 0), gridSize);
	field[2] = SampleVolume(gridPos + make_uint3(1, 1, 0), gridSize);
	field[3] = SampleVolume(gridPos + make_uint3(0, 1, 0), gridSize);
	field[4] = SampleVolume(gridPos + make_uint3(0, 0, 1), gridSize);
	field[5] = SampleVolume(gridPos + make_uint3(1, 0, 1), gridSize);
	field[6] = SampleVolume(gridPos + make_uint3(1, 1, 1), gridSize);
	field[7] = SampleVolume(gridPos + make_uint3(0, 1, 1), gridSize);

	// recalculate flag
	uint cubeindex;
	cubeindex = uint(field[0] < isoValue);
	cubeindex += uint(field[1] < isoValue) * 2;
	cubeindex += uint(field[2] < isoValue) * 4;
	cubeindex += uint(field[3] < isoValue) * 8;
	cubeindex += uint(field[4] < isoValue) * 16;
	cubeindex += uint(field[5] < isoValue) * 32;
	cubeindex += uint(field[6] < isoValue) * 64;
	cubeindex += uint(field[7] < isoValue) * 128;

	// find the vertices where the surface intersects the cube

	// use shared memory to avoid using local
	__shared__ float3 vertlist[12 * NTHREADS];

	vertlist[(NTHREADS * 0) + threadIdx.x] =
		vertexInterp(isoValue, v[0], v[1], field[0], field[1]);
	vertlist[(NTHREADS * 1) + threadIdx.x] =
		vertexInterp(isoValue, v[1], v[2], field[1], field[2]);
	vertlist[(NTHREADS * 2) + threadIdx.x] =
		vertexInterp(isoValue, v[2], v[3], field[2], field[3]);
	vertlist[(NTHREADS * 3) + threadIdx.x] =
		vertexInterp(isoValue, v[3], v[0], field[3], field[0]);
	vertlist[(NTHREADS * 4) + threadIdx.x] =
		vertexInterp(isoValue, v[4], v[5], field[4], field[5]);
	vertlist[(NTHREADS * 5) + threadIdx.x] =
		vertexInterp(isoValue, v[5], v[6], field[5], field[6]);
	vertlist[(NTHREADS * 6) + threadIdx.x] =
		vertexInterp(isoValue, v[6], v[7], field[6], field[7]);
	vertlist[(NTHREADS * 7) + threadIdx.x] =
		vertexInterp(isoValue, v[7], v[4], field[7], field[4]);
	vertlist[(NTHREADS * 8) + threadIdx.x] =
		vertexInterp(isoValue, v[0], v[4], field[0], field[4]);
	vertlist[(NTHREADS * 9) + threadIdx.x] =
		vertexInterp(isoValue, v[1], v[5], field[1], field[5]);
	vertlist[(NTHREADS * 10) + threadIdx.x] =
		vertexInterp(isoValue, v[2], v[6], field[2], field[6]);
	vertlist[(NTHREADS * 11) + threadIdx.x] =
		vertexInterp(isoValue, v[3], v[7], field[3], field[7]);
	__syncthreads();

	// output triangle vertices
	uint numVerts = tex1Dfetch(pep_CubeMarching_number_of_vertices_texture, cubeindex);

	for (int i = 0; i < numVerts; i += 3) {
		uint index = numVertsScanned[voxel] + i;

		float3 *v[3];
		uint edge;
		edge = tex1Dfetch(pep_CubeMarching_triangle_table_texture, (cubeindex * 16) + i);
		v[0] = &vertlist[(edge * NTHREADS) + threadIdx.x];

		edge = tex1Dfetch(pep_CubeMarching_triangle_table_texture, (cubeindex * 16) + i + 1);
		v[1] = &vertlist[(edge * NTHREADS) + threadIdx.x];

		edge = tex1Dfetch(pep_CubeMarching_triangle_table_texture, (cubeindex * 16) + i + 2);
		v[2] = &vertlist[(edge * NTHREADS) + threadIdx.x];

		// calculate triangle surface normal
		float3 n = calcNormal(v[0], v[1], v[2]);

		if (index < (maxVerts - 3)) {
			pos[index] = make_float4(*v[0], 1.0f);
			norm[index] = make_float4(n, 0.0f);

			pos[index + 1] = make_float4(*v[1], 1.0f);
			norm[index + 1] = make_float4(n, 0.0f);

			pos[index + 2] = make_float4(*v[2], 1.0f);
			norm[index + 2] = make_float4(n, 0.0f);
		}
	}

	return;
}






extern "C" void ThrustScanWrapperHost(unsigned int *output, unsigned int *input,
	unsigned int numElements) {
	output[0] = 0;

	for (unsigned int i = 0; i < numElements - 1; i++) {
		output[i + 1] = output[i] + input[i];
	}
}


//
// Function Definations Of Cuda Kernel
//
extern "C" void AllocateTextureMemory(
	uint **d_edgeTable,
	uint **d_triTable,
	uint **d_numVertsTable)
{
	cudaChannelFormatDesc channelDesc =
		cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindUnsigned);

	checkCudaErrors(cudaMalloc((void **)d_triTable, 256 * 16 * sizeof(uint)));

	checkCudaErrors(cudaMemcpy((void *)*d_triTable, (void *)triTable,
		256 * 16 * sizeof(uint), cudaMemcpyHostToDevice));

	checkCudaErrors(
		cudaBindTexture(0, pep_CubeMarching_triangle_table_texture, *d_triTable, channelDesc));

	checkCudaErrors(cudaMalloc((void **)d_numVertsTable, 256 * sizeof(uint)));

	checkCudaErrors(cudaMemcpy((void *)*d_numVertsTable, (void *)numVertsTable,
		256 * sizeof(uint), cudaMemcpyHostToDevice));

	checkCudaErrors(cudaBindTexture(0, pep_CubeMarching_number_of_vertices_texture,
		*d_numVertsTable, channelDesc));

	return;
}

extern "C" void BindVolumeTexture(uchar *d_volume) {
	// bind to linear texture
	checkCudaErrors(cudaBindTexture(
		0, pep_CubeMarching_volume_data_texture, d_volume,
		cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned)));

	return;
}

// sample volume data set at a point
__device__ float sampleVolume(uint3 p, uint3 gridSize) {
	p.x = min(p.x, gridSize.x - 1);
	p.y = min(p.y, gridSize.y - 1);
	p.z = min(p.z, gridSize.z - 1);

	uint i = (p.z * gridSize.x * gridSize.y) + (p.y * gridSize.x) + p.x;

	return tex1Dfetch(pep_CubeMarching_volume_data_texture, i);
}

extern "C" void LaunchCompactVoxels(dim3 grid, dim3 threads,
	uint *compactedVoxelArray,
	uint *voxelOccupied,
	uint *voxelOccupiedScan, uint numVoxels) {

	Kernel_CompactVoxels<<<grid, threads>>>(compactedVoxelArray, voxelOccupied,
		voxelOccupiedScan, numVoxels);

	getLastCudaError("compactVoxels failed");

	return;
}

extern "C" void LaunchClassifyVoxel(dim3 grid, dim3 threads, uint *voxelVerts,
	uint *voxelOccupied, uchar *volume,
	uint3 gridSize, uint numVoxels,
	float3 voxelSize, float isoValue) {
	// calculate number of vertices need per voxel
	Kernel_ClassifyVoxel<<<grid, threads>>>(voxelVerts, voxelOccupied, volume, gridSize,
		numVoxels, voxelSize, isoValue);

	getLastCudaError("classifyVoxel failed");

	return;
}

extern "C" void LaunchGenerateTriangles(dim3 grid, dim3 threads, float4 *pos, float4 *norm,
	uint *compactedVoxelArray, uint *numVertsScanned,
	uchar *volume, uint3 gridSize, float3 voxelSize,
	float isoValue, uint activeVoxels, uint maxVerts) {

	generateTriangles2<<<grid, NTHREADS>>>(
		pos, norm, compactedVoxelArray, numVertsScanned, volume, gridSize,
		voxelSize, isoValue, activeVoxels, maxVerts);

	getLastCudaError("generateTriangles2 failed");

	return;
}
