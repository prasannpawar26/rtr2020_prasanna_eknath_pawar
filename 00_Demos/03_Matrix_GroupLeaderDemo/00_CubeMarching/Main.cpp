﻿#include "Common.h"
#include "CubeMarching.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "glew32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND gHwnd = NULL;
DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
mat4 pep_perspectiveProjectionMatrix;
bool gbIsFullScreen;
int pep_gWidth = 800;
int pep_gHeight = 600;
bool pep_CubeMarching_wireframe = true;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow)
{
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("CubeMarching");

	// code
	if (0 != fopen_s(&pep_gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("CubeMarching"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, pep_gWidth, pep_gHeight, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (0 != iRet)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// code
	switch (iMsg)
	{
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
		case WM_ERASEBKGND:
			return 0;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			ReSize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;

			case 'F':
			case 'f':
				ToggledFullScreen();
				break;

			case 'w':
			case 'W':
				pep_CubeMarching_wireframe = pep_CubeMarching_wireframe ? false : true;
				break;
			}
		} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void Display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	CubeMarching_Display();

	SwapBuffers(gHdc);

	return;
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}
	pep_gWidth = width;
	pep_gHeight = height;

	CubeMarching_Resize(width, height);

	return;
}

void Uninitialize(void)
{
	// code
	CubeMarching_Uninitialize();

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}

	return;
}

void Update(void)
{
	CubeMarching_Update();
}

int Initialize(void)
{
	// variable declarations
	int pep_CubeMarching_index;
	PIXELFORMATDESCRIPTOR pep_CubeMarching_pfd;

	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pep_CubeMarching_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_CubeMarching_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_CubeMarching_pfd.nVersion = 1;
	pep_CubeMarching_pfd.cColorBits = 32;
	pep_CubeMarching_pfd.cDepthBits = 8;
	pep_CubeMarching_pfd.cRedBits = 8;
	pep_CubeMarching_pfd.cGreenBits = 8;
	pep_CubeMarching_pfd.cBlueBits = 8;
	pep_CubeMarching_pfd.cAlphaBits = 8;
	pep_CubeMarching_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	pep_CubeMarching_index = ChoosePixelFormat(gHdc, &pep_CubeMarching_pfd);
	if (0 == pep_CubeMarching_index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, pep_CubeMarching_index, &pep_CubeMarching_pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum pep_CubeMarching_result;
	pep_CubeMarching_result = glewInit();
	if (GLEW_OK != pep_CubeMarching_result)
	{
		return -5;
	}

	CubeMarching_Initialize();

	glClearColor(0.1f, 0.2f, 0.3f, 1.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	pep_perspectiveProjectionMatrix = mat4::identity();

	ReSize(pep_gWidth, pep_gHeight);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}
