#pragma once

#include <GL/glew.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

enum {
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0
};