﻿
#include "Common.h"

#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <helper_gl.h>
#include <vector_functions.h>
#include <vector_types.h>

#include <helper_cuda.h>  // includes cuda.h and cuda_runtime_api.h
#include <helper_functions.h>

#include "defines.h"

extern FILE *pep_gpFile;
extern int pep_gWidth;
extern int pep_gHeight;
extern bool pep_CubeMarching_wireframe;

GLuint pep_CubeMarching_giVertexShaderObject;
GLuint pep_CubeMarching_giFragmentShaderObject;
GLuint pep_CubeMarching_giShaderProgramObject;

GLuint pep_CubeMarching_modelMatrixUniform;
GLuint pep_CubeMarching_viewMatrixUniform;
GLuint pep_CubeMarching_projectionMatrixUniform;

extern mat4 pep_perspectiveProjectionMatrix;

// device data
GLuint pep_CubeMarching_vao;
GLuint pep_CubeMarching_posVbo;
GLuint pep_CubeMarching_normalVbo;

// handles OpenGL-CUDA exchange
struct cudaGraphicsResource *pep_CubeMarching_cuda_posvbo_resource, *pep_CubeMarching_cuda_normalvbo_resource;

float4 *pep_CubeMarching_d_pos = 0, *pep_CubeMarching_d_normal = 0;

uchar *pep_CubeMarching_d_volume = 0;
uint *pep_CubeMarching_d_voxelVerts = 0;
uint *pep_CubeMarching_d_voxelVertsScan = 0;

uint *pep_CubeMarching_h_voxelVerts = 0;
uint *pep_CubeMarching_h_voxelVertsScan = 0;

uint *pep_CubeMarching_d_voxelOccupied = 0;
uint *pep_CubeMarching_d_voxelOccupiedScan = 0;

uint *pep_CubeMarching_h_voxelOccupied = 0;
uint *pep_CubeMarching_h_voxelOccupiedScan = 0;

uint *pep_CubeMarching_d_compVoxelArray;

// tables
uint *pep_CubeMarching_d_numVertsTable = 0;
uint *pep_CubeMarching_d_edgeTable = 0;
uint *pep_CubeMarching_d_triTable = 0;

// 3D Scalar Field
uint3 pep_CubeMarching_volume_3d_gridSize = make_uint3(32, 32, 32);

float3 pep_CubeMarching_voxelSize;
uint pep_CubeMarching_numVoxels = 0;
uint pep_CubeMarching_maxVerts = 0;
uint pep_CubeMarching_activeVoxels = 0;
uint pep_CubeMarching_totalVerts = 0;

float pep_CubeMarching_isoValue = 0.02f;
float pep_CubeMarching_dIsoValue = 0.005f;

//
// CPU Function Declarations
//
extern "C" void LaunchCompactVoxels(dim3 grid, dim3 threads,
	uint *compactedVoxelArray,
	uint *voxelOccupied,
	uint *voxelOccupiedScan, uint numVoxels);

extern "C" void LaunchClassifyVoxel(dim3 grid, dim3 threads, uint *voxelVerts,
	uint *voxelOccupied, uchar *volume,
	uint3 gridSize, uint numVoxels,
	float3 voxelSize, float isoValue);

extern "C" void LaunchGenerateTriangles(dim3 grid, dim3 threads, float4 *pos, float4 *norm,
	uint *compactedVoxelArray, uint *numVertsScanned,
	uchar *volume, uint3 gridSize, float3 voxelSize,
	float isoValue, uint activeVoxels, uint maxVerts);

extern "C" void AllocateTextureMemory(uint **d_edgeTable, uint **d_triTable,
	uint **d_numVertsTable);

extern "C" void BindVolumeTexture(uchar *d_volume);

extern "C" void ThrustScanWrapperHost(unsigned int *output, unsigned int *input,
	unsigned int numElements);

//! Run the Cuda part of the computation
void ComputeIsosurface()
{
	int threads = 128;
	dim3 cuda_grid(pep_CubeMarching_numVoxels / threads, 1, 1);

	// get around maximum grid size of 65535 in each dimension
	if (cuda_grid.x > 65535) {
		cuda_grid.y = cuda_grid.x / 32768;
		cuda_grid.x = 32768;
	}

	// calculate number of vertices need per voxel
	LaunchClassifyVoxel(cuda_grid, threads, pep_CubeMarching_d_voxelVerts, pep_CubeMarching_d_voxelOccupied,
		pep_CubeMarching_d_volume, pep_CubeMarching_volume_3d_gridSize, pep_CubeMarching_numVoxels, pep_CubeMarching_voxelSize,
		pep_CubeMarching_isoValue);

	// Dump Data to debugg

	cudaMemcpy(pep_CubeMarching_h_voxelOccupied, pep_CubeMarching_d_voxelOccupied, pep_CubeMarching_numVoxels * sizeof(uint),
		cudaMemcpyDeviceToHost);

	// scan voxel occupied array
	ThrustScanWrapperHost(pep_CubeMarching_h_voxelOccupiedScan, pep_CubeMarching_h_voxelOccupied, pep_CubeMarching_numVoxels);

	cudaMemcpy(pep_CubeMarching_d_voxelOccupiedScan, pep_CubeMarching_h_voxelOccupiedScan, pep_CubeMarching_numVoxels * sizeof(uint),
		cudaMemcpyHostToDevice);

	// Dump Data to debugg

	// read back values to calculate total number of non-empty voxels
	// since we are using an exclusive scan, the total is the last value of
	// the scan result plus the last value in the input array
	uint lastElement = 0;
	uint lastScanElement = 0;
	checkCudaErrors(cudaMemcpy((void *)&lastElement,
		(void *)(pep_CubeMarching_d_voxelOccupied + pep_CubeMarching_numVoxels - 1),
		sizeof(uint), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy((void *)&lastScanElement,
		(void *)(pep_CubeMarching_d_voxelOccupiedScan + pep_CubeMarching_numVoxels - 1),
		sizeof(uint), cudaMemcpyDeviceToHost));
	pep_CubeMarching_activeVoxels = lastElement + lastScanElement;

	if (pep_CubeMarching_activeVoxels == 0)
	{
		// return if there are no full voxels
		pep_CubeMarching_totalVerts = 0;
		return;
	}

	// compact voxel index array
	LaunchCompactVoxels(cuda_grid, threads, pep_CubeMarching_d_compVoxelArray, pep_CubeMarching_d_voxelOccupied,
		pep_CubeMarching_d_voxelOccupiedScan, pep_CubeMarching_numVoxels);
	getLastCudaError("compactVoxels failed");

	cudaMemcpy(pep_CubeMarching_h_voxelVerts, pep_CubeMarching_d_voxelVerts, pep_CubeMarching_numVoxels * sizeof(uint),
		cudaMemcpyDeviceToHost);

	// Dump Data to debugg

	// scan voxel occupied array
	ThrustScanWrapperHost(pep_CubeMarching_h_voxelVertsScan, pep_CubeMarching_h_voxelVerts, pep_CubeMarching_numVoxels);

	cudaMemcpy(pep_CubeMarching_d_voxelVertsScan, pep_CubeMarching_h_voxelVertsScan, pep_CubeMarching_numVoxels * sizeof(uint),
		cudaMemcpyHostToDevice);

	// Dump Data to debugg

	lastElement = 0;
	lastScanElement = 0;
	checkCudaErrors(cudaMemcpy((void *)&lastElement,
		(void *)(pep_CubeMarching_d_voxelVerts + pep_CubeMarching_numVoxels - 1),
		sizeof(uint), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy((void *)&lastScanElement,
		(void *)(pep_CubeMarching_d_voxelVertsScan + pep_CubeMarching_numVoxels - 1),
		sizeof(uint), cudaMemcpyDeviceToHost));
	pep_CubeMarching_totalVerts = lastElement + lastScanElement;

	// generate triangles, writing to vertex buffers
	size_t pep_CubeMarching_num_bytes;
	checkCudaErrors(cudaGraphicsMapResources(1, &pep_CubeMarching_cuda_posvbo_resource, 0));
	checkCudaErrors(cudaGraphicsResourceGetMappedPointer(
		(void **)&pep_CubeMarching_d_pos, &pep_CubeMarching_num_bytes, pep_CubeMarching_cuda_posvbo_resource));

	checkCudaErrors(cudaGraphicsMapResources(1, &pep_CubeMarching_cuda_normalvbo_resource, 0));
	checkCudaErrors(cudaGraphicsResourceGetMappedPointer(
		(void **)&pep_CubeMarching_d_normal, &pep_CubeMarching_num_bytes, pep_CubeMarching_cuda_normalvbo_resource));

	dim3 grid2((int)ceil(pep_CubeMarching_activeVoxels / (float)NTHREADS), 1, 1);

	while (grid2.x > 65535)
	{
		grid2.x /= 2;
		grid2.y *= 2;
	}

	LaunchGenerateTriangles(grid2, NTHREADS, pep_CubeMarching_d_pos, pep_CubeMarching_d_normal, pep_CubeMarching_d_compVoxelArray,
		pep_CubeMarching_d_voxelVertsScan, pep_CubeMarching_d_volume, pep_CubeMarching_volume_3d_gridSize,
		pep_CubeMarching_voxelSize, pep_CubeMarching_isoValue, pep_CubeMarching_activeVoxels, pep_CubeMarching_maxVerts);
	// Dump Data to debugg

	checkCudaErrors(cudaGraphicsUnmapResources(1, &pep_CubeMarching_cuda_normalvbo_resource, 0));
	checkCudaErrors(cudaGraphicsUnmapResources(1, &pep_CubeMarching_cuda_posvbo_resource, 0));
}

// Load raw data from disk
uchar *loadRawFile(char *filename, int size)
{
	FILE *fp = fopen(filename, "rb");

	if (!fp) {
		fprintf(stderr, "Error opening file '%s'\n", filename);
		return 0;
	}

	uchar *data = (uchar *)malloc(size);
	size_t read = fread(data, 1, size, fp);
	fclose(fp);

	printf("Read '%s', %d bytes\n", filename, (int)read);

	return data;
}

int CubeMarching_Initialize()
{
	pep_CubeMarching_giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_CubeMarching_giVertexShaderObject)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	const GLchar *pep_CubeMarching_vertexShaderSourceCode =
		"#version 450 core"
		"\n"
		"in vec4 vPosition;"
		"in vec4 vNormal;"

		"uniform mat4 u_model_matrix;"
		"uniform mat4 u_view_matrix;"
		"uniform mat4 u_projection_matrix;"

		"void main(void)"
		"{"
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
		"}";

	glShaderSource(pep_CubeMarching_giVertexShaderObject, 1,
		(const GLchar **)&pep_CubeMarching_vertexShaderSourceCode, NULL);
	glCompileShader(pep_CubeMarching_giVertexShaderObject);

	GLint pep_CubeMarching_iShaderCompileStatus = 0;
	GLint pep_CubeMarching_iInfoLogLength = 0;
	GLchar *pep_CubeMarching_szInfoLog = NULL;

	glGetShaderiv(pep_CubeMarching_giVertexShaderObject, GL_COMPILE_STATUS, &pep_CubeMarching_iShaderCompileStatus);
	if (GL_FALSE == pep_CubeMarching_iShaderCompileStatus)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(pep_CubeMarching_giVertexShaderObject, GL_INFO_LOG_LENGTH, &pep_CubeMarching_iInfoLogLength);
		if (0 < pep_CubeMarching_iInfoLogLength)
		{
			pep_CubeMarching_szInfoLog = (GLchar *)malloc(pep_CubeMarching_iInfoLogLength);
			if (NULL != pep_CubeMarching_szInfoLog) {
				GLsizei pep_CubeMarching_written;

				glGetShaderInfoLog(pep_CubeMarching_giVertexShaderObject, pep_CubeMarching_iInfoLogLength, &pep_CubeMarching_written,
					pep_CubeMarching_szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, pep_CubeMarching_szInfoLog);

				free(pep_CubeMarching_szInfoLog);
			}
		}

		return -7;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_CubeMarching_iShaderCompileStatus = 0;
	pep_CubeMarching_iInfoLogLength = 0;
	pep_CubeMarching_szInfoLog = NULL;

	pep_CubeMarching_giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_CubeMarching_giFragmentShaderObject)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, pep_CubeMarching_szInfoLog);

		return -8;
	}

	const GLchar *pep_CubeMarching_fragmentShaderSourceCode =
		"#version 450 core"
		"\n"
		"out vec4 FragColor;"

		"void main(void)"
		"{"
			"FragColor = vec4(1.0, 1.0, 1.0, 1.0);"
		"}";

	glShaderSource(pep_CubeMarching_giFragmentShaderObject, 1,
		(const GLchar **)&pep_CubeMarching_fragmentShaderSourceCode, NULL);
	glCompileShader(pep_CubeMarching_giFragmentShaderObject);

	glGetShaderiv(pep_CubeMarching_giFragmentShaderObject, GL_COMPILE_STATUS,
		&pep_CubeMarching_iShaderCompileStatus);
	if (FALSE == pep_CubeMarching_iShaderCompileStatus)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(pep_CubeMarching_giFragmentShaderObject, GL_INFO_LOG_LENGTH, &pep_CubeMarching_iInfoLogLength);
		if (0 < pep_CubeMarching_iInfoLogLength) {
			pep_CubeMarching_szInfoLog = (GLchar *)malloc(pep_CubeMarching_iInfoLogLength);
			if (NULL != pep_CubeMarching_szInfoLog) {
				GLsizei pep_CubeMarching_written;

				glGetShaderInfoLog(pep_CubeMarching_giFragmentShaderObject, pep_CubeMarching_iInfoLogLength, &pep_CubeMarching_written,
					pep_CubeMarching_szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, pep_CubeMarching_szInfoLog);

				free(pep_CubeMarching_szInfoLog);
			}
		}

		return -9;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_CubeMarching_giShaderProgramObject = glCreateProgram();

	glAttachShader(pep_CubeMarching_giShaderProgramObject, pep_CubeMarching_giVertexShaderObject);
	glAttachShader(pep_CubeMarching_giShaderProgramObject, pep_CubeMarching_giFragmentShaderObject);

	// Attributes Binding Before Linking
	glBindAttribLocation(pep_CubeMarching_giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glBindAttribLocation(pep_CubeMarching_giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(pep_CubeMarching_giShaderProgramObject);

	GLint pep_CubeMarching_iProgramLinkStatus = 0;
	pep_CubeMarching_iInfoLogLength = 0;
	pep_CubeMarching_szInfoLog = NULL;

	glGetProgramiv(pep_CubeMarching_giShaderProgramObject, GL_LINK_STATUS, &pep_CubeMarching_iProgramLinkStatus);
	if (GL_FALSE == pep_CubeMarching_iProgramLinkStatus)
	{
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(pep_CubeMarching_giShaderProgramObject, GL_INFO_LOG_LENGTH, &pep_CubeMarching_iInfoLogLength);
		if (0 < pep_CubeMarching_iInfoLogLength)
		{
			pep_CubeMarching_szInfoLog = (GLchar *)malloc(pep_CubeMarching_iInfoLogLength);
			if (NULL != pep_CubeMarching_szInfoLog)
			{
				GLsizei pep_CubeMarching_written;

				glGetProgramInfoLog(pep_CubeMarching_giShaderProgramObject, pep_CubeMarching_iInfoLogLength, &pep_CubeMarching_written,
					pep_CubeMarching_szInfoLog);

				fprintf(pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, pep_CubeMarching_szInfoLog);

				free(pep_CubeMarching_szInfoLog);
			}
		}

		return -10;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	pep_CubeMarching_modelMatrixUniform =
		glGetUniformLocation(pep_CubeMarching_giShaderProgramObject, "u_model_matrix");
	pep_CubeMarching_viewMatrixUniform =
		glGetUniformLocation(pep_CubeMarching_giShaderProgramObject, "u_view_matrix");
	pep_CubeMarching_projectionMatrixUniform =
		glGetUniformLocation(pep_CubeMarching_giShaderProgramObject, "u_projection_matrix");

	// Initialize CUDA buffers for Marching Cubes
	pep_CubeMarching_numVoxels =
		pep_CubeMarching_volume_3d_gridSize.x * pep_CubeMarching_volume_3d_gridSize.y * pep_CubeMarching_volume_3d_gridSize.z;

	// voxel(cube) by the pixel values at the eight corners of the cube�
	pep_CubeMarching_voxelSize =
		make_float3(2.0f / pep_CubeMarching_volume_3d_gridSize.x, 2.0f / pep_CubeMarching_volume_3d_gridSize.y,
			2.0f / pep_CubeMarching_volume_3d_gridSize.z);

	pep_CubeMarching_maxVerts = pep_CubeMarching_volume_3d_gridSize.x * pep_CubeMarching_volume_3d_gridSize.y * 100;

	int volume_3d_size = pep_CubeMarching_volume_3d_gridSize.x * pep_CubeMarching_volume_3d_gridSize.y *
		pep_CubeMarching_volume_3d_gridSize.z * sizeof(uchar);

	uchar *volume = loadRawFile("./data/Bucky.raw", volume_3d_size);

	checkCudaErrors(cudaMalloc((void **)&pep_CubeMarching_d_volume, volume_3d_size));

	checkCudaErrors(
		cudaMemcpy(pep_CubeMarching_d_volume, volume, volume_3d_size, cudaMemcpyHostToDevice));

	free(volume);

	BindVolumeTexture(pep_CubeMarching_d_volume);

	glGenVertexArrays(1, &pep_CubeMarching_vao);

	glBindVertexArray(pep_CubeMarching_vao);

	glGenBuffers(1, &pep_CubeMarching_posVbo);

	glBindBuffer(GL_ARRAY_BUFFER, pep_CubeMarching_posVbo);
	glBufferData(GL_ARRAY_BUFFER, pep_CubeMarching_maxVerts * sizeof(float) * 4, NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pep_CubeMarching_normalVbo);

	glBindBuffer(GL_ARRAY_BUFFER, pep_CubeMarching_normalVbo);
	glBufferData(GL_ARRAY_BUFFER, pep_CubeMarching_maxVerts * sizeof(float) * 4, NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	checkCudaErrors(cudaGraphicsGLRegisterBuffer(
		&pep_CubeMarching_cuda_posvbo_resource, pep_CubeMarching_posVbo, cudaGraphicsMapFlagsWriteDiscard));

	checkCudaErrors(cudaGraphicsGLRegisterBuffer(
		&pep_CubeMarching_cuda_normalvbo_resource, pep_CubeMarching_normalVbo, cudaGraphicsMapFlagsWriteDiscard));

	// allocate textures
	AllocateTextureMemory(&pep_CubeMarching_d_edgeTable, &pep_CubeMarching_d_triTable, &pep_CubeMarching_d_numVertsTable);

	// allocate device memory
	unsigned int memSize = sizeof(uint) * pep_CubeMarching_numVoxels;
	checkCudaErrors(cudaMalloc((void **)&pep_CubeMarching_d_voxelVerts, memSize));
	checkCudaErrors(cudaMalloc((void **)&pep_CubeMarching_d_voxelVertsScan, memSize));
	checkCudaErrors(cudaMalloc((void **)&pep_CubeMarching_d_voxelOccupied, memSize));
	checkCudaErrors(cudaMalloc((void **)&pep_CubeMarching_d_voxelOccupiedScan, memSize));
	checkCudaErrors(cudaMalloc((void **)&pep_CubeMarching_d_compVoxelArray, memSize));

	pep_CubeMarching_h_voxelVerts = (uint *)malloc(memSize);
	memset(pep_CubeMarching_h_voxelVerts, 0, memSize);

	pep_CubeMarching_h_voxelVertsScan = (uint *)malloc(memSize);
	memset(pep_CubeMarching_h_voxelVertsScan, 0, memSize);

	pep_CubeMarching_h_voxelOccupied = (uint *)malloc(memSize);
	memset(pep_CubeMarching_h_voxelOccupied, 0, memSize);

	pep_CubeMarching_h_voxelOccupiedScan = (uint *)malloc(memSize);
	memset(pep_CubeMarching_h_voxelOccupiedScan, 0, memSize);

	return 0;
}

void CubeMarching_Uninitialize()
{
	glDeleteBuffers(1, &pep_CubeMarching_posVbo);
	pep_CubeMarching_posVbo = 0;
	cudaGraphicsUnregisterResource(pep_CubeMarching_cuda_posvbo_resource);
	glDeleteBuffers(1, &pep_CubeMarching_normalVbo);
	pep_CubeMarching_normalVbo = 0;
	cudaGraphicsUnregisterResource(pep_CubeMarching_cuda_normalvbo_resource);

	checkCudaErrors(cudaFree(pep_CubeMarching_d_edgeTable));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_triTable));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_numVertsTable));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_voxelVerts));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_voxelVertsScan));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_voxelOccupied));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_voxelOccupiedScan));
	checkCudaErrors(cudaFree(pep_CubeMarching_d_compVoxelArray));

	if (pep_CubeMarching_d_volume)
	{
		checkCudaErrors(cudaFree(pep_CubeMarching_d_volume));
	}

	if (pep_CubeMarching_giShaderProgramObject)
	{
		GLsizei pep_CubeMarching_shaderCount;
		GLsizei pep_CubeMarching_shaderNumber;

		glUseProgram(pep_CubeMarching_giShaderProgramObject);

		glGetProgramiv(pep_CubeMarching_giShaderProgramObject, GL_ATTACHED_SHADERS, &pep_CubeMarching_shaderCount);

		GLuint *pep_CubeMarching_pShaders = (GLuint *)malloc(sizeof(GLuint) * pep_CubeMarching_shaderCount);
		if (pep_CubeMarching_pShaders) {
			glGetAttachedShaders(pep_CubeMarching_giShaderProgramObject, pep_CubeMarching_shaderCount, &pep_CubeMarching_shaderCount,
				pep_CubeMarching_pShaders);

			for (pep_CubeMarching_shaderNumber = 0; pep_CubeMarching_shaderNumber < pep_CubeMarching_shaderCount; pep_CubeMarching_shaderNumber++) {
				glDetachShader(pep_CubeMarching_giShaderProgramObject, pep_CubeMarching_pShaders[pep_CubeMarching_shaderNumber]);
				glDeleteShader(pep_CubeMarching_pShaders[pep_CubeMarching_shaderNumber]);
				pep_CubeMarching_pShaders[pep_CubeMarching_shaderNumber] = 0;
			}

			free(pep_CubeMarching_pShaders);
		}

		glDeleteProgram(pep_CubeMarching_giShaderProgramObject);
		pep_CubeMarching_giShaderProgramObject = 0;

		glUseProgram(0);
	}

}

void CubeMarching_Resize(int width, int height)
{
}

void CubeMarching_Update()
{
	ComputeIsosurface();

	// code
	pep_CubeMarching_isoValue += pep_CubeMarching_dIsoValue;

	if (pep_CubeMarching_isoValue < 0.1f)
	{
		pep_CubeMarching_isoValue = 0.1f;
		pep_CubeMarching_dIsoValue *= -1.0f;
	}
	else if (pep_CubeMarching_isoValue > 0.9f)
	{
		pep_CubeMarching_isoValue = 0.9f;
		pep_CubeMarching_dIsoValue *= -1.0f;
	}
}

void CubeMarching_Display()
{
	glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

	pep_perspectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 100.0f);

	glUseProgram(pep_CubeMarching_giShaderProgramObject);
	glPolygonMode(GL_FRONT_AND_BACK, pep_CubeMarching_wireframe ? GL_LINE : GL_FILL); // Can Be Commented
	mat4 pep_CubeMarching_modelMatrix;
	mat4 pep_CubeMarching_viewMatrix;
	mat4 pep_CubeMarching_translateMatrix;
	mat4 pep_CubeMarching_rotationMatrix;
	mat4 pep_CubeMarching_scaleMatrix;

	pep_CubeMarching_translateMatrix = mat4::identity();
	pep_CubeMarching_rotationMatrix = mat4::identity();
	pep_CubeMarching_scaleMatrix = mat4::identity();
	pep_CubeMarching_modelMatrix = mat4::identity();
	pep_CubeMarching_viewMatrix = mat4::identity();

	pep_CubeMarching_translateMatrix = translate(0.0f, 0.0f, -3.0f);

	pep_CubeMarching_modelMatrix = pep_CubeMarching_translateMatrix * pep_CubeMarching_rotationMatrix;
	glUniformMatrix4fv(pep_CubeMarching_modelMatrixUniform, 1, GL_FALSE, pep_CubeMarching_modelMatrix);
	glUniformMatrix4fv(pep_CubeMarching_viewMatrixUniform, 1, GL_FALSE, pep_CubeMarching_viewMatrix);
	glUniformMatrix4fv(pep_CubeMarching_projectionMatrixUniform, 1, GL_FALSE, pep_perspectiveProjectionMatrix);

	glBindVertexArray(pep_CubeMarching_vao);

	glDrawArrays(GL_TRIANGLES, 0, pep_CubeMarching_totalVerts);

	glBindVertexArray(0);

	glUseProgram(0);
}
