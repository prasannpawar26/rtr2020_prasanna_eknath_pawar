	// Headers
#import <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#include "Sphere.h"
#import "vmath.h"

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTCOORD0
};

// C Style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

int Initialize_Shaders(const GLchar *vertexShaderSourceCode,
                       const GLchar *fragmentShaderSourceCode);
// Global Variables
FILE *gpFile=NULL;

BOOL isLightEnable = NO;
BOOL isAnimation = NO;

// Shaders
const GLchar *gc_PerVertex_vertexShaderSourceCode =
"#version 410 core" \
"\n" \

"in vec4 vPosition;" \
"in vec3 vNormal;" \

"struct matrices {" \
    "mat4 model_matrix;" \
    "mat4 view_matrix;" \
    "mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \

"struct light_configuration {" \
    "vec3 light_ambient;" \
    "vec3 light_diffuse;" \
    "vec3 light_specular;" \
    "vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration0;" \

"struct material_configuration {" \
    "vec3 material_ambient;" \
    "vec3 material_diffuse;" \
    "vec3 material_specular;" \
    "float material_shiness;" \
"};" \
"uniform material_configuration u_material_configuration;" \

"uniform int u_key_L_pressed;" \
"out vec3 phong_ads_light;" \

"void main(void)" \
"{" \
    "if(1 == u_key_L_pressed)" \
    "{" \
        "vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
        "vec3 t_normal = normalize(mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal);" \
        "vec3 viewer_vector = normalize(vec3(-eye_coordinates));"\

        "vec3 light_direction0 = normalize(vec3(u_light_configuration0.light_position - eye_coordinates));" \
        "float t_normal_dot_light_direction0 = max(dot(light_direction0, t_normal), 0.0f);" \
        "vec3 reflection_vector0 = reflect(-light_direction0, t_normal);" \
        "vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" \
        "vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" \
        "vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector), 0.0f), u_material_configuration.material_shiness);" \

        "phong_ads_light = ambient0 + diffuse0 + specular0;" \

    "}" \
    "else" \
    "{" \
        "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
    "}" \

    "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
"}";

const GLchar *gc_PerVertex_fragmentShaderSourceCode =
"#version 410 core" \
"\n" \

"in vec3 phong_ads_light;" \
"uniform int u_key_L_pressed;" \
"out vec4 FragColor;" \

"void main(void)" \
"{" \
    "FragColor = vec4(phong_ads_light, 1.0);" \
"}"; \

const GLchar *gc_PerFragment_vertexShaderSourceCode =
"#version 410 core" \
"\n" \

"in vec4 vPosition;" \
"in vec3 vNormal;" \

"struct matrices {" \
    "mat4 model_matrix;" \
    "mat4 view_matrix;" \
    "mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \
"uniform int u_key_L_pressed;" \

"struct light_configuration {" \
    "vec3 light_ambient;" \
    "vec3 light_diffuse;" \
    "vec3 light_specular;" \
    "vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration0;" \

"out vec3 light_direction0;" \

"out vec3 tranformation_matrix;" \
"out vec3 viewer_vector;" \

"void main(void)" \
"{" \
    "if(1 == u_key_L_pressed)" \
    "{" \
        "vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \

        "tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" \
        "viewer_vector = vec3(-eye_coordinates);" \

        "light_direction0 = vec3(u_light_configuration0.light_position - eye_coordinates);" \

    "}" \

    "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
"}";

const GLchar *gc_PerFragment_fragmentShaderSourceCode =
"#version 410 core" \
"\n" \

"in vec3 light_direction0;" \
"in vec3 tranformation_matrix;" \
"in vec3 viewer_vector;" \

"uniform int u_key_L_pressed;" \
"struct light_configuration {" \
    "vec3 light_ambient;" \
    "vec3 light_diffuse;" \
    "vec3 light_specular;" \
    "vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration0;" \

"struct material_configuration {"
    "vec3 material_ambient;" \
    "vec3 material_diffuse;" \
    "vec3 material_specular;" \
    "float material_shiness;" \
"};" \
"uniform material_configuration u_material_configuration;" \

"out vec4 FragColor;" \
"vec3 phong_ads_light;" \

"void main(void)" \
"{" \
    "if(1 == u_key_L_pressed)" \
    "{" \
    "vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
    "vec3 viewer_vector_normal = normalize(viewer_vector);" \

    "vec3 light_direction_normalize0 = normalize(light_direction0);" \
    "vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" \
    "float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" \
    "vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" \
    "vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" \
    "vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" \

    "phong_ads_light = ambient0 + diffuse0 + specular0;" \
    "}" \
    "else" \
    "{" \
        "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
    "}" \

        "FragColor = vec4(phong_ads_light, 1.0);" \
"}";

// Interface Declarations
@interface AppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

// NSOpenGLView => Because of this we donot have to take care of framebuffer(FBO) management. In iOS View is getting derived frim UIView Only, and UIView want us to take care of FBO.
//
@interface GLView : NSOpenGLView 
@end

// Enter-Point Function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// Interface Implementation
@implementation AppDelegate
{
    @private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // log file
    NSBundle *mainBundle=[NSBundle mainBundle];
    
    NSString *appDirName=[mainBundle bundlePath];
    
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile=fopen(pszLogFileNameWithPath, "w");
    if(gpFile==NULL)
    {
        printf("Can Not Create Log File.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
    
    
    // window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0, 0.0, 800.0, 600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];
    
    [window setTitle:@"macOS OpenGL 24Spheres"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile, "Program Is Terminated Successfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
	
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;
    
    GLuint vao_sphere;
    GLuint vbo_sphere_vertex;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    unsigned int gNumVertices;
    unsigned int gNumElements;
    
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 perspectiveProjectionMatrix;
    vmath::mat4 translateMatrix;
    
    // Unifroms
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint keyLPressedUniform;
    
    struct light_configuration_uniform {
        GLuint lightAmbient;
        GLuint lightDiffuse;
        GLuint lightSpecular;
        GLuint lightPosition;
    };
    struct light_configuration_uniform light_configuration_uniform0;
    
    struct material_configuration_uniform {
        GLuint materialAmbient;
        GLuint materialDiffuse;
        GLuint materialSpecular;
        GLuint materialShiness;
    };
    struct material_configuration_uniform material_configuration_uniform;
    
    struct light_configuration {
        float lightAmbient[4];
        float lightDiffuse[4];
        float lightSpecular[4];
        float lightPosition[4];
    };
    struct light_configuration light0;
    
    struct material_configuration {
        float materialAmbient[4];
        float materialDiffuse[4];
        float materialSpecular[4];
        float materialShiness;
    };
    struct material_configuration material[6][4];
    
    int g_width;
    int g_height;
    
    GLfloat gLightROtationAroundXAxis;
    GLfloat gLightROtationAroundYAxis;
    GLfloat gLightROtationAroundZAxis;
    int keyPress;
}

-(id)initWithFrame:(NSRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify The Display ID To Associate The GL Context With (Main Display For Now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAAccelerated,
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer, 0
        }; //last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(nil==pixelFormat)
        {
            fprintf(gpFile, "No Valid OpeGL Pixel Format Is Available. Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext]; // it automatically release the older context, if present, and sets the newer one
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // Code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // Code
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    gLightROtationAroundXAxis = 0.0f;
    gLightROtationAroundYAxis = 0.0f;
    gLightROtationAroundZAxis = 0.0f;
    keyPress = 0;
    
    // This Values Will be Common to All Viewport
    light0.lightAmbient[0] = 0.0f;
    light0.lightAmbient[1] = 0.0f;
    light0.lightAmbient[2] = 0.0f;
    light0.lightAmbient[3] = 0.0f;
    
    light0.lightDiffuse[0] = 1.0f;
    light0.lightDiffuse[1] = 1.0f;
    light0.lightDiffuse[2] = 1.0f;
    light0.lightDiffuse[3] = 1.0f;
    
    light0.lightSpecular[0] = 1.0f;
    light0.lightSpecular[1] = 1.0f;
    light0.lightSpecular[2] = 1.0f;
    light0.lightSpecular[3] = 1.0f;
    
    light0.lightPosition[0] = 0.0f;
    light0.lightPosition[1] = 0.0f;
    light0.lightPosition[2] = 0.0f;
    light0.lightPosition[3] = 1.0f;
    
    // Material values will be unique per sphere hence declare an 2d-array of it
    // 1st Colume, Gems
    // 1st Sphere -> Emerald
    material[0][0].materialAmbient[0] = 0.0215f;
    material[0][0].materialAmbient[1] = 0.1745f;
    material[0][0].materialAmbient[2] = 0.0215f;
    material[0][0].materialAmbient[3] = 1.0f;
    material[0][0].materialDiffuse[0] = 0.07568f;
    material[0][0].materialDiffuse[1] = 0.61424f;
    material[0][0].materialDiffuse[2] = 0.07568f;
    material[0][0].materialDiffuse[3] = 1.0f;
    material[0][0].materialSpecular[0] = 0.633f;
    material[0][0].materialSpecular[1] = 0.727811f;
    material[0][0].materialSpecular[2] = 0.633f;
    material[0][0].materialSpecular[3] = 1.0f;
    material[0][0].materialShiness = 0.6f * 128;
    // 2nd Sphere -> Emerald
    material[1][0].materialAmbient[0] = 0.135f;
    material[1][0].materialAmbient[1] = 0.2225f;
    material[1][0].materialAmbient[2] = 0.1575f;
    material[1][0].materialAmbient[3] = 1.0f;
    material[1][0].materialDiffuse[0] = 0.54f;
    material[1][0].materialDiffuse[1] = 0.89f;
    material[1][0].materialDiffuse[2] = 0.63f;
    material[1][0].materialDiffuse[3] = 1.0f;
    material[1][0].materialSpecular[0] = 0.316228f;
    material[1][0].materialSpecular[1] = 0.316228f;
    material[1][0].materialSpecular[2] = 0.316228f;
    material[1][0].materialSpecular[3] = 1.0f;
    material[1][0].materialShiness = 0.1f * 128;
    // 3rd Sphere -> Obsidian
    material[2][0].materialAmbient[0] = 0.05375f;
    material[2][0].materialAmbient[1] = 0.05f;
    material[2][0].materialAmbient[2] = 0.06625f;
    material[2][0].materialAmbient[3] = 1.0f;
    material[2][0].materialDiffuse[0] = 0.18275f;
    material[2][0].materialDiffuse[1] = 0.17f;
    material[2][0].materialDiffuse[2] = 0.22525f;
    material[2][0].materialDiffuse[3] = 1.0f;
    material[2][0].materialSpecular[0] = 0.332741f;
    material[2][0].materialSpecular[1] = 0.328634f;
    material[2][0].materialSpecular[2] = 0.346435f;
    material[2][0].materialSpecular[3] = 1.0f;
    material[2][0].materialShiness = 0.3f * 128;
    //4th Sphere -> Pearl
    material[3][0].materialAmbient[0] = 0.25f;
    material[3][0].materialAmbient[1] = 0.20725f;
    material[3][0].materialAmbient[2] = 0.20725f;
    material[3][0].materialAmbient[3] = 1.0f;
    material[3][0].materialDiffuse[0] = 1.0f;
    material[3][0].materialDiffuse[1] = 0.829f;
    material[3][0].materialDiffuse[2] = 0.829f;
    material[3][0].materialDiffuse[3] = 1.0f;
    material[3][0].materialSpecular[0] = 0.296648f;
    material[3][0].materialSpecular[1] = 0.296648f;
    material[3][0].materialSpecular[2] = 0.296648f;
    material[3][0].materialSpecular[3] = 1.0f;
    material[3][0].materialShiness = 0.088f * 128;
    //5th Sphere->Ruby
    material[4][0].materialAmbient[0] = 0.1745f;
    material[4][0].materialAmbient[1] = 0.01175f;
    material[4][0].materialAmbient[2] = 0.01175f;
    material[4][0].materialAmbient[3] = 1.0f;
    material[4][0].materialDiffuse[0] = 0.61424f;
    material[4][0].materialDiffuse[1] = 0.04136f;
    material[4][0].materialDiffuse[2] = 0.04136f;
    material[4][0].materialDiffuse[3] = 1.0f;
    material[4][0].materialSpecular[0] = 0.727811f;
    material[4][0].materialSpecular[1] = 0.626959f;
    material[4][0].materialSpecular[2] = 0.626959f;
    material[4][0].materialSpecular[3] = 1.0f;
    material[4][0].materialShiness = 0.6f * 128;
    //6th Sphere->Turquoise
    material[5][0].materialAmbient[0] = 0.1f;
    material[5][0].materialAmbient[1] = 0.18725f;
    material[5][0].materialAmbient[2] = 0.1745f;
    material[5][0].materialAmbient[3] = 1.0f;
    material[5][0].materialDiffuse[0] = 0.396f;
    material[5][0].materialDiffuse[1] = 0.74151f;
    material[5][0].materialDiffuse[2] = 0.69102f;
    material[5][0].materialDiffuse[3] = 1.0f;
    material[5][0].materialSpecular[0] = 0.297254f;
    material[5][0].materialSpecular[1] = 0.30829f;
    material[5][0].materialSpecular[2] = 0.306678f;
    material[5][0].materialSpecular[3] = 1.0f;
    material[5][0].materialShiness = 0.1f * 128;
    // 2nd Column : Metal
    // 1st Sphere-> Brass
    material[0][1].materialAmbient[0] = 0.329412f;
    material[0][1].materialAmbient[1] = 0.223529f;
    material[0][1].materialAmbient[2] = 0.027451f;
    material[0][1].materialAmbient[3] = 1.0f;
    material[0][1].materialDiffuse[0] = 0.780392f;
    material[0][1].materialDiffuse[1] = 0.568627f;
    material[0][1].materialDiffuse[2] = 0.113725f;
    material[0][1].materialDiffuse[3] = 1.0f;
    material[0][1].materialSpecular[0] = 0.992157f;
    material[0][1].materialSpecular[1] = 0.941176f;
    material[0][1].materialSpecular[2] = 0.807843f;
    material[0][1].materialSpecular[3] = 1.0f;
    material[0][1].materialShiness = 0.21794872f * 128;
    // 2nd Sphere, Bronze
    material[1][1].materialAmbient[0] = 0.2125f;
    material[1][1].materialAmbient[1] = 0.1275f;
    material[1][1].materialAmbient[2] = 0.054f;
    material[1][1].materialAmbient[3] = 1.0f;
    material[1][1].materialDiffuse[0] = 0.714f;
    material[1][1].materialDiffuse[1] = 0.4284f;
    material[1][1].materialDiffuse[2] = 0.18144f;
    material[1][1].materialDiffuse[3] = 1.0f;
    material[1][1].materialSpecular[0] = 0.393548f;
    material[1][1].materialSpecular[1] = 0.271906f;
    material[1][1].materialSpecular[2] = 0.166721f;
    material[1][1].materialSpecular[3] = 1.0f;
    material[1][1].materialShiness = 0.2f * 128;
    // 3rd Sphere, Chrome
    material[2][1].materialAmbient[0] = 0.25f;
    material[2][1].materialAmbient[1] = 0.25f;
    material[2][1].materialAmbient[2] = 0.25f;
    material[2][1].materialAmbient[3] = 1.0f;
    material[2][1].materialDiffuse[0] = 0.4f;
    material[2][1].materialDiffuse[1] = 0.4f;
    material[2][1].materialDiffuse[2] = 0.4f;
    material[2][1].materialDiffuse[3] = 1.0f;
    material[2][1].materialSpecular[0] = 0.774597f;
    material[2][1].materialSpecular[1] = 0.774597f;
    material[2][1].materialSpecular[2] = 0.774597f;
    material[2][1].materialSpecular[3] = 1.0f;
    material[2][1].materialShiness = 0.6f * 128;
    // 4th Sphere, Copper
    material[3][1].materialAmbient[0] = 0.19125f;
    material[3][1].materialAmbient[1] = 0.0735f;
    material[3][1].materialAmbient[2] = 0.0225f;
    material[3][1].materialAmbient[3] = 1.0f;
    material[3][1].materialDiffuse[0] = 0.7038f;
    material[3][1].materialDiffuse[1] = 0.27048f;
    material[3][1].materialDiffuse[2] = 0.0828f;
    material[3][1].materialDiffuse[3] = 1.0f;
    material[3][1].materialSpecular[0] = 0.256777f;
    material[3][1].materialSpecular[1] = 0.137622f;
    material[3][1].materialSpecular[2] = 0.086014f;
    material[3][1].materialSpecular[3] = 1.0f;
    material[3][1].materialShiness = 0.1f * 128;
    // 5th Sphere, Gold
    material[4][1].materialAmbient[0] = 0.24725f;
    material[4][1].materialAmbient[1] = 0.1995f;
    material[4][1].materialAmbient[2] = 0.0745f;
    material[4][1].materialAmbient[3] = 1.0f;
    material[4][1].materialDiffuse[0] = 0.75164f;
    material[4][1].materialDiffuse[1] = 0.60648f;
    material[4][1].materialDiffuse[2] = 0.22648f;
    material[4][1].materialDiffuse[3] = 1.0f;
    material[4][1].materialSpecular[0] = 0.628281f;
    material[4][1].materialSpecular[1] = 0.555802f;
    material[4][1].materialSpecular[2] = 0.366065f;
    material[4][1].materialSpecular[3] = 1.0f;
    material[4][1].materialShiness = 0.4f * 128;
    // 6th Sphere, Silver
    material[5][1].materialAmbient[0] = 0.19225f;
    material[5][1].materialAmbient[1] = 0.19225f;
    material[5][1].materialAmbient[2] = 0.19225f;
    material[5][1].materialAmbient[3] = 1.0f;
    material[5][1].materialDiffuse[0] = 0.50754f;
    material[5][1].materialDiffuse[1] = 0.50754f;
    material[5][1].materialDiffuse[2] = 0.50754f;
    material[5][1].materialDiffuse[3] = 1.0f;
    material[5][1].materialSpecular[0] = 0.508273f;
    material[5][1].materialSpecular[1] = 0.508273f;
    material[5][1].materialSpecular[2] = 0.508273f;
    material[5][1].materialSpecular[3] = 1.0f;
    material[5][1].materialShiness = 0.4f * 128;
    // 3rd Column : Plastic
    // 1st Sphere, Black
    material[0][2].materialAmbient[0] = 0.0f;
    material[0][2].materialAmbient[1] = 0.0f;
    material[0][2].materialAmbient[2] = 0.0f;
    material[0][2].materialAmbient[3] = 1.0f;
    material[0][2].materialDiffuse[0] = 0.01f;
    material[0][2].materialDiffuse[1] = 0.01f;
    material[0][2].materialDiffuse[2] = 0.01f;
    material[0][2].materialDiffuse[3] = 1.0f;
    material[0][2].materialSpecular[0] = 0.50f;
    material[0][2].materialSpecular[1] = 0.50f;
    material[0][2].materialSpecular[2] = 0.50f;
    material[0][2].materialSpecular[3] = 1.0f;
    material[0][2].materialShiness = 0.25f * 128;
    // 2nd Sphere, Cyan */
    material[1][2].materialAmbient[0] = 0.0f;
    material[1][2].materialAmbient[1] = 0.1f;
    material[1][2].materialAmbient[2] = 0.06f;
    material[1][2].materialAmbient[3] = 1.0f;
    material[1][2].materialDiffuse[0] = 0.0f;
    material[1][2].materialDiffuse[1] = 0.50980392f;
    material[1][2].materialDiffuse[2] = 0.50980392f;
    material[1][2].materialDiffuse[3] = 1.0f;
    material[1][2].materialSpecular[0] = 0.50196078f;
    material[1][2].materialSpecular[1] = 0.50196078f;
    material[1][2].materialSpecular[2] = 0.50196078f;
    material[1][2].materialSpecular[3] = 1.0f;
    material[1][2].materialShiness = 0.25f * 128;
    // 3rd Sphere, Green
    material[2][2].materialAmbient[0] = 0.0f;
    material[2][2].materialAmbient[1] = 0.0f;
    material[2][2].materialAmbient[2] = 0.0f;
    material[2][2].materialAmbient[3] = 1.0f;
    material[2][2].materialDiffuse[0] = 0.1f;
    material[2][2].materialDiffuse[1] = 0.35f;
    material[2][2].materialDiffuse[2] = 0.1f;
    material[2][2].materialDiffuse[3] = 1.0f;
    material[2][2].materialSpecular[0] = 0.45f;
    material[2][2].materialSpecular[1] = 0.55f;
    material[2][2].materialSpecular[2] = 0.45f;
    material[2][2].materialSpecular[3] = 1.0f;
    material[2][2].materialShiness = 0.25f * 128;
    // 4th Sphere, Red
    material[3][2].materialAmbient[0] = 0.0f;
    material[3][2].materialAmbient[1] = 0.0f;
    material[3][2].materialAmbient[2] = 0.0f;
    material[3][2].materialAmbient[3] = 1.0f;
    material[3][2].materialDiffuse[0] = 0.5f;
    material[3][2].materialDiffuse[1] = 0.0f;
    material[3][2].materialDiffuse[2] = 0.0f;
    material[3][2].materialDiffuse[3] = 1.0f;
    material[3][2].materialSpecular[0] = 0.7f;
    material[3][2].materialSpecular[1] = 0.6f;
    material[3][2].materialSpecular[2] = 0.6f;
    material[3][2].materialSpecular[3] = 1.0f;
    material[3][2].materialShiness = 0.25f * 128;
    // 5th Sphere, White
    material[4][2].materialAmbient[0] = 0.0f;
    material[4][2].materialAmbient[1] = 0.0f;
    material[4][2].materialAmbient[2] = 0.0f;
    material[4][2].materialAmbient[3] = 1.0f;
    material[4][2].materialDiffuse[0] = 0.55f;
    material[4][2].materialDiffuse[1] = 0.55f;
    material[4][2].materialDiffuse[2] = 0.55f;
    material[4][2].materialDiffuse[3] = 1.0f;
    material[4][2].materialSpecular[0] = 0.70f;
    material[4][2].materialSpecular[1] = 0.70f;
    material[4][2].materialSpecular[2] = 0.70f;
    material[4][2].materialSpecular[3] = 1.0f;
    material[4][2].materialShiness = 0.25f * 128;
    // 6th Sphere, Yellow
    material[5][2].materialAmbient[0] = 0.0f;
    material[5][2].materialAmbient[1] = 0.0f;
    material[5][2].materialAmbient[2] = 0.0f;
    material[5][2].materialAmbient[3] = 1.0f;
    material[5][2].materialDiffuse[0] = 0.5f;
    material[5][2].materialDiffuse[1] = 0.5f;
    material[5][2].materialDiffuse[2] = 0.0f;
    material[5][2].materialDiffuse[3] = 1.0f;
    material[5][2].materialSpecular[0] = 0.60f;
    material[5][2].materialSpecular[1] = 0.60f;
    material[5][2].materialSpecular[2] = 0.50f;
    material[5][2].materialSpecular[3] = 1.0f;
    material[5][2].materialShiness = 0.25f * 128;
    //4th Column : Rubber
    // 1st Sphere, Black
    material[0][3].materialAmbient[0] = 0.02f;
    material[0][3].materialAmbient[1] = 0.02f;
    material[0][3].materialAmbient[2] = 0.02f;
    material[0][3].materialAmbient[3] = 1.0f;
    material[0][3].materialDiffuse[0] = 0.01f;
    material[0][3].materialDiffuse[1] = 0.01f;
    material[0][3].materialDiffuse[2] = 0.01f;
    material[0][3].materialDiffuse[3] = 1.0f;
    material[0][3].materialSpecular[0] = 0.40f;
    material[0][3].materialSpecular[1] = 0.40f;
    material[0][3].materialSpecular[2] = 0.40f;
    material[0][3].materialSpecular[3] = 1.0f;
    material[0][3].materialShiness = 0.078125f * 128;
    // 2nd Sphere, Cyan
    material[1][3].materialAmbient[0] = 0.0f;
    material[1][3].materialAmbient[1] = 0.05f;
    material[1][3].materialAmbient[2] = 0.05f;
    material[1][3].materialAmbient[3] = 1.0f;
    material[1][3].materialDiffuse[0] = 0.4f;
    material[1][3].materialDiffuse[1] = 0.5f;
    material[1][3].materialDiffuse[2] = 0.5f;
    material[1][3].materialDiffuse[3] = 1.0f;
    material[1][3].materialSpecular[0] = 0.04f;
    material[1][3].materialSpecular[1] = 0.7f;
    material[1][3].materialSpecular[2] = 0.7f;
    material[1][3].materialSpecular[3] = 1.0f;
    material[1][3].materialShiness = 0.078125f * 128;
    // 3rd Sphere, Green
    material[2][3].materialAmbient[0] = 0.0f;
    material[2][3].materialAmbient[1] = 0.05f;
    material[2][3].materialAmbient[2] = 0.0f;
    material[2][3].materialAmbient[3] = 1.0f;
    material[2][3].materialDiffuse[0] = 0.4f;
    material[2][3].materialDiffuse[1] = 0.5f;
    material[2][3].materialDiffuse[2] = 0.4f;
    material[2][3].materialDiffuse[3] = 1.0f;
    material[2][3].materialSpecular[0] = 0.04f;
    material[2][3].materialSpecular[1] = 0.7f;
    material[2][3].materialSpecular[2] = 0.04f;
    material[2][3].materialSpecular[3] = 1.0f;
    material[2][3].materialShiness = 0.078125f * 128;
    // 4th Sphere, Red
    material[3][3].materialAmbient[0] = 0.05f;
    material[3][3].materialAmbient[1] = 0.0f;
    material[3][3].materialAmbient[2] = 0.0f;
    material[3][3].materialAmbient[3] = 1.0f;
    material[3][3].materialDiffuse[0] = 0.5f;
    material[3][3].materialDiffuse[1] = 0.4f;
    material[3][3].materialDiffuse[2] = 0.4f;
    material[3][3].materialDiffuse[3] = 1.0f;
    material[3][3].materialSpecular[0] = 0.7f;
    material[3][3].materialSpecular[1] = 0.04f;
    material[3][3].materialSpecular[2] = 0.04f;
    material[3][3].materialSpecular[3] = 1.0f;
    material[3][3].materialShiness = 0.078125f * 128;
    // 5th Sphere, White
    material[4][3].materialAmbient[0] = 0.05f;
    material[4][3].materialAmbient[1] = 0.05f;
    material[4][3].materialAmbient[2] = 0.05f;
    material[4][3].materialAmbient[3] = 1.0f;
    material[4][3].materialDiffuse[0] = 0.5f;
    material[4][3].materialDiffuse[1] = 0.5f;
    material[4][3].materialDiffuse[2] = 0.5f;
    material[4][3].materialDiffuse[3] = 1.0f;
    material[4][3].materialSpecular[0] = 0.7f;
    material[4][3].materialSpecular[1] = 0.7f;
    material[4][3].materialSpecular[2] = 0.7f;
    material[4][3].materialSpecular[3] = 1.0f;
    material[4][3].materialShiness = 0.078125f * 128;
    // 6th Sphere, Yellow
    material[5][3].materialAmbient[0] = 0.05f;
    material[5][3].materialAmbient[1] = 0.05f;
    material[5][3].materialAmbient[2] = 0.0f;
    material[5][3].materialAmbient[3] = 1.0f;
    material[5][3].materialDiffuse[0] = 0.5f;
    material[5][3].materialDiffuse[1] = 0.5f;
    material[5][3].materialDiffuse[2] = 0.4f;
    material[5][3].materialDiffuse[3] = 1.0f;
    material[5][3].materialSpecular[0] = 0.70f;
    material[5][3].materialSpecular[1] = 0.70f;
    material[5][3].materialSpecular[2] = 0.04f;
    material[5][3].materialSpecular[3] = 1.0f;
    material[5][3].materialShiness = 0.078125f * 128;

    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
                        sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();
    
    [self initialize_shaders:gc_PerVertex_vertexShaderSourceCode :gc_PerVertex_fragmentShaderSourceCode];
    
    // Sphere
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);
    
    glGenBuffers(1, &vbo_sphere_vertex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_vertex);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // element vbo
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements),
                 sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
	glClearDepth(1.0f);
	
	glEnable(GL_DEPTH_TEST);
	
	glDepthFunc(GL_LEQUAL);
	
	//glEnable(GL_CULL_FACE);
	
    // set background color
    glClearColor(0.50f, 0.50f, 0.50f, 0.50f);
	
	// set projection matrix to identity matrix
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    isLightEnable = NO;
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    // Code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    g_width = width;
    g_height = height;
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    [self drawView]; // dummy call
}

-(void)draw24sphere
{
    //1st Colume, 1st Sphere
    // ViewPort setting
    int height = g_height;
    int width = 0;
    
    for (int colume = 0; colume < 4; colume++)
    {
        height -= (g_height/6);
        
        for (int row = 0; row < 6; row++)
        {
            glViewport(width, height, (GLsizei)g_width / 4,
                       (GLsizei)g_height / 6);
            // Prepsective Setting
            perspectiveProjectionMatrix = vmath::mat4::identity();
            perspectiveProjectionMatrix = vmath::perspective(45.0f, ((GLfloat)g_width / 4.0f) / ((GLfloat)g_height / 6.0f), 0.1f,                                                    100.0f);
            
            // Identity matrices
            modelMatrix = vmath::mat4::identity();
            viewMatrix = vmath::mat4::identity();
            translateMatrix = vmath::mat4::identity();
            
            // Setting matirces - uniform values
            viewMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
            glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
            glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
            glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                               perspectiveProjectionMatrix);
            
            // setting light and material values
            if (isLightEnable == YES)
            {
                glUniform1i(keyLPressedUniform, 1);
                
                if (1 == keyPress)
                {
                    light0.lightPosition[1] =
                    cos(gLightROtationAroundXAxis) + sin(gLightROtationAroundXAxis);
                    light0.lightPosition[2] =
                    cos(gLightROtationAroundXAxis) - sin(gLightROtationAroundXAxis);
                } else if (2 == keyPress)
                {
                    light0.lightPosition[0] =
                    cos(gLightROtationAroundYAxis) - sin(gLightROtationAroundYAxis);
                    light0.lightPosition[2] =
                    cos(gLightROtationAroundYAxis) + sin(gLightROtationAroundYAxis);
                } else if (3 == keyPress)
                {
                    light0.lightPosition[0] =
                    cos(gLightROtationAroundZAxis) - sin(gLightROtationAroundZAxis);
                    light0.lightPosition[1] =
                    cos(gLightROtationAroundZAxis) + sin(gLightROtationAroundZAxis);
                }
                
                glUniform3fv(light_configuration_uniform0.lightAmbient, 1,
                             light0.lightAmbient);
                glUniform3fv(light_configuration_uniform0.lightDiffuse, 1,
                             light0.lightDiffuse);
                glUniform3fv(light_configuration_uniform0.lightSpecular, 1,
                             light0.lightSpecular);
                glUniform4fv(light_configuration_uniform0.lightPosition, 1,
                             light0.lightPosition);
                
                glUniform3fv(material_configuration_uniform.materialAmbient, 1,
                             material[row][colume].materialAmbient);
                
                glUniform3fv(material_configuration_uniform.materialDiffuse, 1,
                             material[row][colume].materialDiffuse);
                
                glUniform3fv(material_configuration_uniform.materialSpecular, 1,
                             material[row][colume].materialSpecular);
                
                glUniform1f(material_configuration_uniform.materialShiness,
                            material[row][colume].materialShiness);
                
            } else
            {
                glUniform1i(keyLPressedUniform, 0);
            }
            
            glBindVertexArray(vao_sphere);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
            // *** draw, either by glDrawTriangles() or glDrawArrays() or
            // glDrawElements()
            glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
            
            glBindVertexArray(0);
            
            height -= (g_height / 6);
        }
        
        width += (g_width / 4);
        height = g_height;
    }
    
    return;
}


- (void)drawView
{
    // code
    
    // update code
    if (1 == keyPress)
    {
        if (gLightROtationAroundXAxis >= 360.0f)
        {
            gLightROtationAroundXAxis = 0.0f;
        }
        gLightROtationAroundXAxis += 0.05f;
        
    } else if (2 == keyPress)
    {
        if (gLightROtationAroundYAxis >= 360.0f)
        {
            gLightROtationAroundYAxis = 0.0f;
        }
        gLightROtationAroundYAxis += 0.05f;
        
    } else if (3 == keyPress)
    {
        if (gLightROtationAroundZAxis >= 360.0f)
        {
            gLightROtationAroundZAxis = 0.0f;
        }
        gLightROtationAroundZAxis += 0.05f;
    }
    
    // rendering code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	glUseProgram(shaderProgramObject);
	// Opengl Drawing
    [self draw24sphere];
	glUseProgram(0);
	
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]); // multi-threaded swap buffer
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    fprintf(gpFile, "InSide Function 'acceptsFirstResonder'\n");
    [[self window]makeFirstResponder:self];
    
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 'e':
        case 'E':
        {
            fprintf(gpFile, "InSide Function 'keyDown: Escape Key Is Pressed'\n");
            [self release];
            [NSApp terminate:self];
        }   break;
        
        case 'F':
        case 'f':
        {
            [self uninitialize_shaders];
            [self initialize_shaders:gc_PerFragment_vertexShaderSourceCode :gc_PerFragment_fragmentShaderSourceCode];
        }   break;
            
        case 'V':
        case 'v':
        {
            [self uninitialize_shaders];
            [self initialize_shaders:gc_PerVertex_vertexShaderSourceCode :gc_PerVertex_fragmentShaderSourceCode];
        }   break;
        
        case 'L':
        case 'l':
        {
            if(isLightEnable == NO)
            {
                isLightEnable = YES;
            }
            else
            {
                isLightEnable = NO;
            }
        }   break;
            
        case 'A':
        case 'a':
        {
            if(isAnimation == NO)
            {
                isAnimation = YES;
            }
            else
            {
                isAnimation = NO;
            }
        }   break;
            
        case 'X':
        case 'x':
        {
            keyPress = 1;
            gLightROtationAroundXAxis = 0.0f;
        } break;
            
        case 'Y':
        case 'y':
        {
            keyPress = 2;
            gLightROtationAroundYAxis = 0.0f;
        } break;
            
        case 'Z':
        case 'z':
        {
            keyPress = 3;
            gLightROtationAroundZAxis = 0.0f;
        } break;
            
        case 27:
        {
            fprintf(gpFile, "InSide Function 'keyDown: F Key Is Pressed'\n");
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
        }   break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void) dealloc
{
    if(vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    
    if (vbo_sphere_vertex)
    {
        glDeleteBuffers(1, &vbo_sphere_vertex);
        vbo_sphere_vertex = 0;
    }
    
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    
    [self uninitialize_shaders];
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

-(void) uninitialize_shaders
{
    if (shaderProgramObject) {
        GLsizei shaderCount;
        GLsizei shaderNumber;
        
        glUseProgram(shaderProgramObject);
        
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders) {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount,
                                 pShaders);
            
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            
            free(pShaders);
        }
        
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        
        glUseProgram(0);
    }
    
    return;
    
}

-(int) initialize_shaders:(const GLchar *)vertexShaderSourceCode :(const GLchar *)fragmentShaderSourceCode
{
    
    // code
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == vertexShaderObject) {
        fprintf(gpFile, "Error : glCreateShader Failed\n");
        return -1;
    }
    
    fprintf(gpFile, "Vertex Shader glCreateShader Successful\n");
    
    glShaderSource(vertexShaderObject, 1,(const GLchar **)&vertexShaderSourceCode, NULL);
    glCompileShader(vertexShaderObject);
    
    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus) {
        fprintf(gpFile, "Error : Vertex Shader Compilation Failed.\n");
        
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Error : Shader Compilation Failed:\n\t%s\n", szInfoLog);
                free(szInfoLog);
            }
        }
        
        return -2;
    }
    fprintf(gpFile, "Vertex Shader Compiled Successfully\n");
    
    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == fragmentShaderObject) {
        fprintf(gpFile,"glCreateShader Failed:\n\t%s\n", szInfoLog);
        return -3;
    }
    fprintf(gpFile,"Fragment Shader Object Created Succesful");
    
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    glCompileShader(fragmentShaderObject);
    
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS,
                  &iShaderCompileStatus);
    if (FALSE == iShaderCompileStatus) {
        fprintf(gpFile, "Error : Fragment Shader Compilation Failed.\n");
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile,"Error : Shader Compilation Failed:\n\t%s\n", szInfoLog);
                free(szInfoLog);
            }
        }
        
        return -4;
    }
    fprintf(gpFile, "Fragment Shader Compiled Successfully\n");
    
    shaderProgramObject = glCreateProgram();
    
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // Attributes Binding Before Linking
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
    
    glLinkProgram(shaderProgramObject);
    
    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;
    
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus) {
            
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program(s) Linked: %s\n", szInfoLog);
                free(szInfoLog);
            }
        }
        
        return -5;
    }
        fprintf(gpFile, "Shader Program(s) Linked Successfully\n");
    
        // Uniform Binding After Linking
        modelMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_matrices.model_matrix");
        viewMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_matrices.view_matrix");
        projectionMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_matrices.projection_matrix");
    
        light_configuration_uniform0.lightAmbient = glGetUniformLocation(shaderProgramObject, "u_light_configuration0.light_ambient");
        light_configuration_uniform0.lightDiffuse = glGetUniformLocation(shaderProgramObject, "u_light_configuration0.light_diffuse");
        light_configuration_uniform0.lightSpecular = glGetUniformLocation(shaderProgramObject, "u_light_configuration0.light_specular");
        light_configuration_uniform0.lightPosition = glGetUniformLocation(shaderProgramObject, "u_light_configuration0.light_position");
    
        material_configuration_uniform.materialAmbient = glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_ambient");
        material_configuration_uniform.materialDiffuse  = glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_diffuse");
        material_configuration_uniform.materialSpecular = glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_specular");
        material_configuration_uniform.materialShiness = glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_shiness");
        keyLPressedUniform = glGetUniformLocation(shaderProgramObject, "u_key_L_pressed");
    
        return 0;
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayRef, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    
    return(result);
}



                


















