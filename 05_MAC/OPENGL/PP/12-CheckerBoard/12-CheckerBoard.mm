// Headers
#import <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTCOORD0
};

// C Style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile=NULL;

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

GLubyte check_image[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];

// Interface Declarations
@interface AppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

// NSOpenGLView => Because of this we donot have to take care of framebuffer(FBO) management. In iOS View is getting derived frim UIView Only, and UIView want us to take care of FBO.
//
@interface GLView : NSOpenGLView
@end

// Enter-Point Function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// Interface Implementation
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // log file
    NSBundle *mainBundle=[NSBundle mainBundle];
    
    NSString *appDirName=[mainBundle bundlePath];
    
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile=fopen(pszLogFileNameWithPath, "w");
    if(gpFile==NULL)
    {
        printf("Can Not Create Log File.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
    
    
    // window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0, 0.0, 800.0, 600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];
    
    [window setTitle:@"macOS OpenGL Static Smiley"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile, "Program Is Terminated Successfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_rectangle_1;
    GLuint vbo_position_1;
    GLuint vbo_texture_1;
    
    GLuint vao_rectangle_2;
    GLuint vbo_position_2;
    GLuint vbo_texture_2;
    
    GLuint texture_math;
    
    GLuint sampler_uniform;
    
    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify The Display ID To Associate The GL Context With (Main Display For Now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAAccelerated,
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer, 0
        }; //last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(nil==pixelFormat)
        {
            fprintf(gpFile, "No Valid OpeGL Pixel Format Is Available. Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext]; // it automatically release the older context, if present, and sets the newer one
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // Code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // Code
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // VertexShader
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    const GLchar *vertexShaderSourceCode =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec2 vTexCoord;" \
    "out vec2 out_textcoord;" \
    "uniform mat4 u_mvp_matrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvp_matrix * vPosition;" \
    "out_textcoord = vTexCoord;" \
    "}";
    
    glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(vertexShaderObject);
    
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if(iShaderCompiledStatus == GL_COMPILE_STATUS)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                
                free(szInfoLog);
                
                [self release];
                
                [NSApp terminate:self];
                
            }
        }
    }
    fprintf(gpFile, "Vertex Shader Compile Successfully\n");
    
    // Fragment Shader
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    
    // create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide shader code to shader
    const GLchar *fragmentShaderSourceCode =
    "#version 410" \
    "\n" \
    "uniform sampler2D u_sampler;" \
    "in vec2 out_textcoord;" \
    "out vec4 FragColor;" \
    " void main(void)" \
    "{" \
    "FragColor = texture(u_sampler, out_textcoord);" \
    "}";
    
    glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if(iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
                
                free(szInfoLog);
                
                [self release];
                
                [NSApp terminate:self];
            }
            
        }
    }
    fprintf(gpFile, "Fragement Shader Compile Successfully\n");
    
    // SHADER Program
    //create
    shaderProgramObject = glCreateProgram();
    
    // attach
    glAttachShader(shaderProgramObject, vertexShaderObject);
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_TEXTCOORD0,
                         "vTexCoord");
    
    // link
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if( iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if(iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                
                fprintf(gpFile, "Shader Program LinkLog: %s\n", szInfoLog);
                
                free(szInfoLog);
                
                [self release];
                
                [NSApp terminate:self];
            }
        }
    }
    fprintf(gpFile, "Shader Program Link Successfully\n");
    //get MVP uniform location
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    sampler_uniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
    
    // load textures
    texture_math = [self loadTexture];
    
    // RECTANGLE_1
   
    glGenVertexArrays(1, &vao_rectangle_1);
    
    glBindVertexArray(vao_rectangle_1);
    
    // vertex
    glGenBuffers(1, &vbo_position_1);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_1);
    
    glBufferData(GL_ARRAY_BUFFER, 12, NULL, GL_DYNAMIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //texture
    const GLfloat textureCoords_1[] =
    {0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f, 1.0f};
    
    glGenBuffers(1, &vbo_texture_1);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_1);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords_1), textureCoords_1,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXTCOORD0, 2, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXTCOORD0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    //
    
    // RECTANGLE_2
    
    glGenVertexArrays(1, &vao_rectangle_2);
    
    glBindVertexArray(vao_rectangle_2);
    
    // vertex
    glGenBuffers(1, &vbo_position_2);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_2);
    
    glBufferData(GL_ARRAY_BUFFER, 12, NULL, GL_DYNAMIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //texture
    const GLfloat textureCoords_2[] =
    {0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f, 1.0f};
    
    glGenBuffers(1, &vbo_texture_2);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_2);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords_2), textureCoords_2,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXTCOORD0, 2, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXTCOORD0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    //
    
    glClearDepth(1.0f);
    
    glEnable(GL_TEXTURE_2D);
    
    glEnable(GL_DEPTH_TEST);
    
    glDepthFunc(GL_LEQUAL);
    
    //glEnable(GL_CULL_FACE);
    
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
    
    // set projection matrix to identity matrix
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)makeCheckerImage
{
    // Variable Delcarations
    int i, j, c;
    
    for (i = 0; i < CHECK_IMAGE_HEIGHT; i++) {
        for (j = 0; j < CHECK_IMAGE_WIDTH; j++) {
            c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            check_image[i][j][0] = (GLubyte)c;
            check_image[i][j][1] = (GLubyte)c;
            check_image[i][j][2] = (GLubyte)c;
            check_image[i][j][3] = 255;
        }
    }
    
    return;
}

-(GLuint)loadTexture
{
    GLuint bmpTexture;
    
    [self makeCheckerImage];
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glGenTextures(1, &bmpTexture);
    
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMAGE_WIDTH, CHECK_IMAGE_HEIGHT,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, check_image);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return(bmpTexture);
}

-(void)reshape
{
    // Code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self  bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
        height=1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    // perspectiveProjectionMatrix projection setting
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    [self drawView]; // dummy call
}

- (void)drawView
{
    // code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    // Opengl Drawing
    // set modelView & modelViewProjection Matrices to identity
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1i(sampler_uniform, 0);
    
    // Rectangle 1
    
    glBindTexture(GL_TEXTURE_2D, texture_math);
    
    glBindVertexArray(vao_rectangle_1);
    
    const GLfloat rectangleVertices_1[] =
    {
        0.0f, 1.0f,  0.0f,
        -2.0f, 1.0f,  0.0f,
        -2.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f
    };
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_1);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices_1), rectangleVertices_1, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_1);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    // Rectangle 2
    
    glBindTexture(GL_TEXTURE_2D, texture_math);
    
    glBindVertexArray(vao_rectangle_2);
    
    const GLfloat rectangleVertices_2[] =
    {
        // Right-Top
        2.41421f, 1.0f, -1.41421f,
        // Left-Top
        1.0f, 1.0f, 0.0f,
        // Left-Bottom
        1.0f, -1.0f, 0.0f,
        // Right-Bottom
        2.41421f, -1.0f, -1.41421f
    };
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_2);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices_2), rectangleVertices_2, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_2);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]); // multi-threaded swap buffer
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    fprintf(gpFile, "InSide Function 'acceptsFirstResonder'\n");
    [[self window]makeFirstResponder:self];
    
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            fprintf(gpFile, "InSide Function 'keyDown: Escape Key Is Pressed'\n");
            [self release];
            [NSApp terminate:self];
            break;
            
        case 'F':
        case 'f':
            fprintf(gpFile, "InSide Function 'keyDown: F Key Is Pressed'\n");
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
            break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void) dealloc
{
    
    if (vbo_texture_1)
    {
        glDeleteBuffers(1, &vbo_texture_1);
        vbo_texture_1 = 0;
    }
    
    if (vbo_position_1)
    {
        glDeleteBuffers(1, &vbo_position_1);
        vbo_position_1 = 0;
    }
    
    if (vao_rectangle_1)
    {
        glDeleteVertexArrays(1, &vao_rectangle_1);
        vao_rectangle_1 = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayRef, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    
    return(result);
}




















