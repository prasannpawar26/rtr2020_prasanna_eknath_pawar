	// Headers
#import <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#include "Sphere.h"
#import "vmath.h"

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTCOORD0
};

// C Style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile=NULL;

BOOL isLightEnable = NO;

// Interface Declarations
@interface AppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

// NSOpenGLView => Because of this we donot have to take care of framebuffer(FBO) management. In iOS View is getting derived frim UIView Only, and UIView want us to take care of FBO.
//
@interface GLView : NSOpenGLView 
@end

// Enter-Point Function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// Interface Implementation
@implementation AppDelegate
{
    @private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // log file
    NSBundle *mainBundle=[NSBundle mainBundle];
    
    NSString *appDirName=[mainBundle bundlePath];
    
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile=fopen(pszLogFileNameWithPath, "w");
    if(gpFile==NULL)
    {
        printf("Can Not Create Log File.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
    
    
    // window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0, 0.0, 800.0, 600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];
    
    [window setTitle:@"macOS OpenGL 3D Shapes Animation"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile, "Program Is Terminated Successfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
	
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;
    
    GLuint vao_sphere;
    GLuint vbo_sphere_vertex;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    unsigned int gNumVertices;
    unsigned int gNumElements;
    
    // UNIFORMS
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint lightAmbientUniform;
    GLuint lightDiffuseUniform;
    GLuint lightSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShinessUniform;
    GLuint lightPositionUniform;
    GLuint keyLPressedUniform;
    
    // Application Variables Associated With Uniforms
    float lightAmbient[4];
    float lightDiffuse[4];
    float lightSpecular[4];
    float lightPosition[4];
    
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShiness;
    
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 perspectiveProjectionMatrix;
    vmath::mat4 translateMatrix;
    
}

-(id)initWithFrame:(NSRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify The Display ID To Associate The GL Context With (Main Display For Now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAAccelerated,
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer, 0
        }; //last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(nil==pixelFormat)
        {
            fprintf(gpFile, "No Valid OpeGL Pixel Format Is Available. Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext]; // it automatically release the older context, if present, and sets the newer one
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // Code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // Code
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 0.0f;
    
    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    
    lightPosition[0] = 100.0f;
    lightPosition[1] = 100.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;
    
    
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f;
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 0.0f;
    
    materialDiffuse[0] = 1.0f;
    materialDiffuse[1] = 1.0f;
    materialDiffuse[2] = 1.0f;
    materialDiffuse[3] = 1.0f;
    
    materialSpecular[0] =1.0f;
    materialSpecular[1] = 1.0f;
    materialSpecular[2] = 1.0f;
    materialSpecular[3] = 1.0f;

    materialShiness = 50.0f;
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
                        sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();
    
	// VertexShader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar *vertexShaderSourceCode = 
	"#version 410" \
	"\n" \
    
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform vec3 u_light_ambient;" \
    "uniform vec3 u_light_diffuse;" \
    "uniform vec3 u_light_specular;" \
    "uniform vec4 u_light_position;" \
    "uniform vec3 u_material_ambient;" \
    "uniform vec3 u_material_diffuse;" \
    "uniform vec3 u_material_specular;" \
    "uniform float u_material_shiness;" \
    "uniform int u_key_L_pressed;" \
    "out vec3 phong_ads_light;" \
    "void main(void)" \
    "{" \
        "if(1 == u_key_L_pressed)" \
        "{" \
            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
            "vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * "
            "vNormal);" \
            "vec3 light_direction = normalize(vec3(u_light_position - "
            "eye_coordinates));" \
            "float t_normal_dot_light_direction = max(dot(light_direction, "
            "t_normal), 0.0f);" \
            "vec3 reflection_vector = reflect(-light_direction, t_normal);"
            "vec3 viewer_vector = normalize(vec3(-eye_coordinates));" \
            "vec3 ambient = u_light_ambient * u_material_ambient;" \
            "vec3 diffuse = u_light_diffuse * u_material_diffuse * "
            "t_normal_dot_light_direction;" \
            "vec3 specular = u_light_specular * u_material_specular * "
            "pow(max(dot(reflection_vector, viewer_vector), 0.0f), "
            "u_material_shiness);" \
    
            "phong_ads_light = ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
            "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
        "}" \
    
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * "
        "vPosition;" \
    "}";
	
	glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// compile shader
	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if(iShaderCompiledStatus == GL_COMPILE_STATUS)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				
				free(szInfoLog);
				
				[self release];
				
				[NSApp terminate:self];
				
			}
		}
	}
	fprintf(gpFile, "Vertex Shader Compile Successfully\n");
    
    // Fragment Shader
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	// create shader
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	// provide shader code to shader
	const GLchar *fragmentShaderSourceCode = 
	"#version 410" \
	"\n" \
    "in vec3 phong_ads_light;" \
    "uniform int u_key_L_pressed;" \
    "out vec4 FragColor;" \
    
    "void main(void)" \
    "{" \
        " FragColor = vec4(phong_ads_light, 1.0);" \
    "}";
    
	glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// compile shader
	glCompileShader(fragmentShaderObject);
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				
				free(szInfoLog);
				
				[self release];
				
				[NSApp terminate:self];
			}
			
		}
	}
    fprintf(gpFile, "Fragement Shader Compile Successfully\n");
    
    // SHADER Program
	//create
	shaderProgramObject = glCreateProgram();
	
	// attach
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);
	
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
	
	// link
	glLinkProgram(shaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if( iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				
				fprintf(gpFile, "Shader Program LinkLog: %s\n", szInfoLog);
				
				free(szInfoLog);
				
				[self release];
				
				[NSApp terminate:self];
			}
		}
	}
	fprintf(gpFile, "Shader Program Link Successfully\n");
   
    // Uniform Binding After Linking
    modelMatrixUniform =
    glGetUniformLocation(shaderProgramObject, "u_model_matrix");
    viewMatrixUniform =
    glGetUniformLocation(shaderProgramObject, "u_view_matrix");
    projectionMatrixUniform =
    glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
    
    lightAmbientUniform =
    glGetUniformLocation(shaderProgramObject, "u_light_ambient");
    lightDiffuseUniform =
    glGetUniformLocation(shaderProgramObject, "u_light_diffuse");
    lightSpecularUniform =
    glGetUniformLocation(shaderProgramObject, "u_light_specular");
    lightPositionUniform =
    glGetUniformLocation(shaderProgramObject, "u_light_position");
    
    materialAmbientUniform =
    glGetUniformLocation(shaderProgramObject, "u_material_ambient");
    materialDiffuseUniform =
    glGetUniformLocation(shaderProgramObject, "u_material_diffuse");
    materialSpecularUniform =
    glGetUniformLocation(shaderProgramObject, "u_material_specular");
    materialShinessUniform =
    glGetUniformLocation(shaderProgramObject, "u_material_shiness");
    
    keyLPressedUniform =
    glGetUniformLocation(shaderProgramObject, "u_key_L_pressed");
   
    // Sphere
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);
    
    glGenBuffers(1, &vbo_sphere_vertex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_vertex);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // element vbo
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements),
                 sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    //
    
	glClearDepth(1.0f);
	
	glEnable(GL_DEPTH_TEST);
	
	glDepthFunc(GL_LEQUAL);
	
	//glEnable(GL_CULL_FACE);
	
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
	
	// set projection matrix to identity matrix
	perspectiveProjectionMatrix = vmath::mat4::identity();
	
    isLightEnable = NO;
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    // Code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
        height=1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    [self drawView]; // dummy call
}

- (void)drawView
{
    // code
    
    // update code
    
    // rendering code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	glUseProgram(shaderProgramObject);
	
	// Opengl Drawing
    modelMatrix = vmath::mat4::identity();
    viewMatrix =vmath::mat4::identity();
    translateMatrix = vmath::mat4::identity();
    
    viewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                       perspectiveProjectionMatrix);

    if (isLightEnable == YES) {
        glUniform1i(keyLPressedUniform, 1);
        glUniform3fv(lightAmbientUniform, 1, lightAmbient);
        glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
        glUniform3fv(lightSpecularUniform, 1, lightSpecular);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        glUniform3fv(materialAmbientUniform, 1, materialAmbient);
        glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
        glUniform3fv(materialSpecularUniform, 1, materialSpecular);
        glUniform1f(materialShinessUniform, materialShiness);
    } else {
        glUniform1i(keyLPressedUniform, 0);
    }
    
    glBindVertexArray(vao_sphere);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    glBindVertexArray(0);
    
	glUseProgram(0);
	
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]); // multi-threaded swap buffer
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    fprintf(gpFile, "InSide Function 'acceptsFirstResonder'\n");
    [[self window]makeFirstResponder:self];
    
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            fprintf(gpFile, "InSide Function 'keyDown: Escape Key Is Pressed'\n");
            [self release];
            [NSApp terminate:self];
            break;
            
        case 'L':
        case 'l':
            if(isLightEnable == NO)
            {
                isLightEnable = YES;
            }
            else
            {
                isLightEnable = NO;
            }
            break;
            
        case 'F':
        case 'f':
            fprintf(gpFile, "InSide Function 'keyDown: F Key Is Pressed'\n");
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
            break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void) dealloc
{
	
    if(vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    
    if (vbo_sphere_vertex)
    {
        glDeleteBuffers(1, &vbo_sphere_vertex);
        vbo_sphere_vertex = 0;
    }
    
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    
    if (shaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;
        
        glUseProgram(shaderProgramObject);
        
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
        
        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(shaderProgramObject, shaderCount, &shaderCount,
                                 pShaders);
            
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(shaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            
            free(pShaders);
        }
        
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        
        glUseProgram(0);
    }
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayRef, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    
    return(result);
}





















