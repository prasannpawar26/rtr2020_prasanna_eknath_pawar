	// Headers
#import <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>
#import "vmath.h"

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTCOORD0
};

// C Style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

// Global Variables
FILE *gpFile=NULL;

// Interface Declarations
@interface AppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

// NSOpenGLView => Because of this we donot have to take care of framebuffer(FBO) management. In iOS View is getting derived frim UIView Only, and UIView want us to take care of FBO.
//
@interface GLView : NSOpenGLView 
@end

// Enter-Point Function
int main(int argc, const char *argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// Interface Implementation
@implementation AppDelegate
{
    @private
    NSWindow *window;
    GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // log file
    NSBundle *mainBundle=[NSBundle mainBundle];
    
    NSString *appDirName=[mainBundle bundlePath];
    
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
    
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile=fopen(pszLogFileNameWithPath, "w");
    if(gpFile==NULL)
    {
        printf("Can Not Create Log File.\nExitting ...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile, "Program Is Started Successfully\n");
    
    
    // window
    NSRect win_rect;
    win_rect=NSMakeRect(0.0, 0.0, 800.0, 600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable backing:NSBackingStoreBuffered defer:NO];
    
    [window setTitle:@"macOS OpenGL 2D Shapes Animation"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile, "Program Is Terminated Successfully\n");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
	
	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;
	
    GLuint vao_triangle;
    GLuint vbo_triangle_position;
    GLuint vbo_triangle_color;
    GLfloat rotation_triangle_angle;
    
    GLuint vao_rectangle;
    GLuint vbo_rectangle_position;
    //GLuint vbo_rectangle_color;
    GLfloat rotation_rectangle_angle;
    
    GLuint mvpUniform;
	
	vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            //Specify The Display ID To Associate The GL Context With (Main Display For Now)
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAAccelerated,
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer, 0
        }; //last 0 is must
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        if(nil==pixelFormat)
        {
            fprintf(gpFile, "No Valid OpeGL Pixel Format Is Available. Exitting...\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext]; // it automatically release the older context, if present, and sets the newer one
    }
    
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // Code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    //[self update];
    [self drawView];
    
    [pool release];
    
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    // Code
    fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
	// VertexShader
	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar *vertexShaderSourceCode = 
	"#version 410" \
	"\n" \
	"in vec4 vPosition;" \
    "in vec4 vColor;" \
    
    "out vec4 out_color;" \
    
	"uniform mat4 u_mvp_matrix;" \
	"void main(void)" \
	"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
    "out_color = vColor;" \
	"}";
	
	glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	
	// compile shader
	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if(iShaderCompiledStatus == GL_COMPILE_STATUS)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				
				glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
				
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				
				free(szInfoLog);
				
				[self release];
				
				[NSApp terminate:self];
				
			}
		}
	}
	fprintf(gpFile, "Vertex Shader Compile Successfully\n");
    
    // Fragment Shader
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	// create shader
	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	
	// provide shader code to shader
	const GLchar *fragmentShaderSourceCode = 
	"#version 410" \
	"\n" \
    
    "in vec4 out_color;" \
    
	"out vec4 FragColor;" \
	" void main(void)" \
	
    "{" \
        "FragColor = out_color;" \
    "}";
	
	glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	
	// compile shader
	glCompileShader(fragmentShaderObject);
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				
				free(szInfoLog);
				
				[self release];
				
				[NSApp terminate:self];
			}
			
		}
	}
    fprintf(gpFile, "Fragement Shader Compile Successfully\n");
    
    // SHADER Program
	//create
	shaderProgramObject = glCreateProgram();
	
	// attach
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);
	
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");
	
	// link
	glLinkProgram(shaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if( iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
				
				fprintf(gpFile, "Shader Program LinkLog: %s\n", szInfoLog);
				
				free(szInfoLog);
				
				[self release];
				
				[NSApp terminate:self];
			}
		}
	}
	fprintf(gpFile, "Shader Program Link Successfully\n");
    //get MVP uniform location
	mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
    // Triangle
	// triangle-vertices
	const GLfloat triangleVertices[] =
	{
		0.0f, 1.0f, 0.0f, // apex
		-1.0f, -1.0f, 0.0f, // left bottom
		1.0f, -1.0f, 0.0f // right-bottom
    };
    
    const GLfloat triangleColor[] = {1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
        0.0f, 0.0f, 0.0f, 1.0f};
    
    glGenVertexArrays(1, &vao_triangle);
    
    glBindVertexArray(vao_triangle);
    
    glGenBuffers(1, &vbo_triangle_position);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_position);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_triangle_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle_color);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    //
    // RECTANGLE
    const GLfloat rectangleVertices[] =
    {
        1.0f, 1.0f,  0.0f,
        -1.0f, 1.0f,  0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f
    };
    
    glGenVertexArrays(1, &vao_rectangle);
    
    glBindVertexArray(vao_rectangle);
    
    glGenBuffers(1, &vbo_rectangle_position);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle_position);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices,
                 GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                          NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glVertexAttrib3f(AMC_ATTRIBUTES_COLOR, 0.0f, 0.0f, 1.0f);
    
    glBindVertexArray(0);
    //
    
	glClearDepth(1.0f);
	
	glEnable(GL_DEPTH_TEST);
	
	glDepthFunc(GL_LEQUAL);
	
	//glEnable(GL_CULL_FACE);
	
    // set background color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
	
	// set projection matrix to identity matrix
	perspectiveProjectionMatrix = vmath::mat4::identity();
	
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    // Code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    
    if(height==0)
        height=1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	// perspectiveProjectionMatrix projection setting
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, width/height, 0.1f, 100.f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    [self drawView]; // dummy call
}

- (void)drawView
{
    // code
    
    // update code
    if (360.0f < rotation_triangle_angle)
    {
        rotation_triangle_angle = 0.0f;
    }
    rotation_triangle_angle += 1.0f;
    
    if (360.0f < rotation_rectangle_angle) {
        rotation_rectangle_angle = 0.0f;
    }
    rotation_rectangle_angle += 1.0f;
    
    // rendering code
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	glUseProgram(shaderProgramObject);
	
	// Opengl Drawing
	// set modelView & modelViewProjection Matrices to identity
	vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(rotation_triangle_angle, 0.0f, 1.0f, 0.0f);
    modelViewMatrix = translateMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // order is important
	
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	
    glBindVertexArray(vao_triangle);
    
    glDrawArrays(GL_TRIANGLES, 0, 3);
    
    glBindVertexArray(0);
    
    // Rectangle
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    translateMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    
    rotationMatrix = vmath::rotate(rotation_rectangle_angle, 1.0f, 0.0f, 0.0f);
    
    modelViewMatrix = translateMatrix * rotationMatrix;
    
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    glBindVertexArray(vao_rectangle);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
	glUseProgram(0);
	
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]); // multi-threaded swap buffer
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    fprintf(gpFile, "InSide Function 'acceptsFirstResonder'\n");
    [[self window]makeFirstResponder:self];
    
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            fprintf(gpFile, "InSide Function 'keyDown: Escape Key Is Pressed'\n");
            [self release];
            [NSApp terminate:self];
            break;
            
        case 'F':
        case 'f':
            fprintf(gpFile, "InSide Function 'keyDown: F Key Is Pressed'\n");
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
            break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    //code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    //code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

-(void) dealloc
{
	
    if (vbo_rectangle_position)
    {
        glDeleteBuffers(1, &vbo_rectangle_position);
        vbo_rectangle_position = 0;
    }
    
    if (vao_rectangle)
    {
        glDeleteVertexArrays(1, &vao_rectangle);
        vao_rectangle = 0;
    }
    
    if (vbo_triangle_position) {
        glDeleteBuffers(1, &vbo_triangle_position);
        vbo_triangle_position = 0;
    }
    
    if (vao_triangle) {
        glDeleteVertexArrays(1, &vao_triangle);
        vao_triangle = 0;
    }
    
	glDetachShader(shaderProgramObject, vertexShaderObject);
	glDetachShader(shaderProgramObject, fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject = 0;
	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject = 0;
	glDeleteProgram(shaderProgramObject);
	shaderProgramObject = 0;
	
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}

/*

-(void)update
{
    // code
    if (360.0f < rotation_triangle_angle)
    {
        rotation_triangle_angle = 0.0f;
    }
    rotation_triangle_angle += 1.0f;
    
    if (360.0f < rotation_rectangle_angle) {
        rotation_rectangle_angle = 0.0f;
    }
    rotation_rectangle_angle += 1.0f;
}
*/
 
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayRef, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    
    return(result);
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	
	return(result);
}






















