#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  // Variable Declarations
  FILE *pFile;

  // Code
  if (0 != fopen_s(&pFile, "LogFile.txt", "w"))
  {
    printf(" %s: %s : %d : Failed To Create/Open Log file\n", __FILE__, __FUNCTION__, __LINE__);
    exit(0);
  }

  fprintf(pFile, "India Is My Country!!!\n");

  fclose(pFile);

  return 0;
}

/*

int g_i;
int insert()
{
  g_i++;
}

= function frame =
stack => i

= function framework end


*/