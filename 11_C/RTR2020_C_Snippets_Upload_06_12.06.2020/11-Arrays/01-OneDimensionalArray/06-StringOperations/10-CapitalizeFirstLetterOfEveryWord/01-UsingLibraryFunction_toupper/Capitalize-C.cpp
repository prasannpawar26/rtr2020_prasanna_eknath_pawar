#include <stdio.h>
#include <ctype.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Prototype
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  char pep_chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH];
  int pep_iStringLength;
  int pep_i, pep_j;

  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  pep_iStringLength = MyStrlen(pep_chArray);

  pep_j = 0;

  for (pep_i = 0; pep_i < pep_iStringLength; pep_i++)
  {
    if (0 == pep_i)
    {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] =
          toupper(pep_chArray[pep_i]);
    }
    else if (' ' == pep_chArray[pep_i])
    {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] = pep_chArray[pep_i];
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j + 1] =
          toupper(pep_chArray[pep_i + 1]);
      pep_i++;
      pep_j++;
    }
    else
    {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] = pep_chArray[pep_i];
    }
    pep_j++;
  }
  pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] = '\0';

  printf("\n\n");
  printf("String Enterd By You Is : \n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  printf("String After Capitalizing First Letter Of Every Word : \n\n");
  printf("%s\n", pep_chArray_CapitalizedFirstLetterOfEveryWord);

  return 0;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_legnth = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    } else
    {
      pep_string_legnth++;
    }
  }

  return pep_string_legnth;
}
