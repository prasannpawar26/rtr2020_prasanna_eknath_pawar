#include <ctype.h>
#include <stdio.h>

#define MAX_STRING_LENGTH 512
#define SPACE ' '
#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void) {
  // Function Prototype
  int MyStrlen(char[]);
  char MyToUpper(char);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  char pep_chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH];
  int pep_iStringLength;
  int pep_i, pep_j;

  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  pep_iStringLength = MyStrlen(pep_chArray);

  pep_j = 0;

  for (pep_i = 0; pep_i < pep_iStringLength; pep_i++) {
    if (pep_i == 0) {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] =
          MyToUpper(pep_chArray[pep_i]);
    }
    else if (SPACE == pep_chArray[pep_i])
    {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] =
          pep_chArray[pep_i];
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j + 1] =
          MyToUpper(pep_chArray[pep_i + 1]);

      pep_j++;
      pep_i++;
    } else if (FULLSTOP == pep_chArray[pep_i] ||
               COMMA == pep_chArray[pep_i] ||
               EXCLAMATION == pep_chArray[pep_i] ||
               QUESTION_MARK == pep_chArray[pep_i])
    {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] = pep_chArray[pep_i];
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j + 1] = SPACE;
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j + 2] =
          MyToUpper(pep_chArray[pep_i + 1]);

      pep_j = pep_j + 2;
      pep_i++;
    }
    else
    {
      pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] = pep_chArray[pep_i];
    }

    pep_j++;
  }

  pep_chArray_CapitalizedFirstLetterOfEveryWord[pep_j] = '\0';

  printf("\n\n");
  printf("String Enterd By You Is : \n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  printf("String After Capitalizing First Letter Of Every Word : \n\n");
  printf("%s\n", pep_chArray_CapitalizedFirstLetterOfEveryWord);

  return 0;
}

int MyStrlen(char pep_str[]) {
  // Variable Declarations
  int pep_j;
  int pep_string_legnth = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++) {
    if ('\0' == pep_str[pep_j]) {
      break;
    } else {
      pep_string_legnth++;
    }
  }

  return pep_string_legnth;
}

char MyToUpper(char pep_ch) {
  // Variable Declarations
  int pep_num;
  int pep_c;

  // ASCII VLAUES OF 'a' To 'z' => 97 to 122
  // ASCII VLAUES OF 'A' To 'Z' => 65 to 90
  pep_num = 'a' - 'A';

  if ((int)pep_ch >= 97 && (int)pep_ch <= 122) {
    pep_c = (int)pep_ch - pep_num;

    return ((char)pep_c);
  } else {
    return (pep_ch);
  }
}
