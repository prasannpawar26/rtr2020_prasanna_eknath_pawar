#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  int pep_iString_Length;
  int pep_i;
  int pep_word_count = 0, pep_Space_count = 0;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray);

  pep_iString_Length = MyStrlen(pep_chArray);

  for (pep_i = 0; pep_i < pep_iString_Length; pep_i++)
  {
    switch (pep_chArray[pep_i])
    {
      case 32:
      {
        pep_Space_count++;
      } break;

      default:
        break;
    }
  }

  pep_word_count = pep_Space_count + 1;

  printf("\n\n");
  printf("Number Of Spaces In The Input String = %d\n\n", pep_Space_count);
  printf("Number Of Words In The Input String = %d\n\n", pep_word_count);

  return 0;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_legnth = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    } else
    {
      pep_string_legnth++;
    }
  }

  return pep_string_legnth;
}
