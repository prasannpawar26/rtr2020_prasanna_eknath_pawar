#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  void MyStrcpy(char[], char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  char pep_chArray_copy[MAX_STRING_LENGTH];

  int pep_iStringLength = 0;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  MyStrcpy(pep_chArray_copy, pep_chArray);

  printf("\n\n");
  printf("Original String Entered By You Is :\n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  printf("Copied String Is :\n\n");
  printf("%s\n", pep_chArray_copy);

  return 0;
}

void MyStrcpy(char str_destination[], char str_source[])
{
  // Function Prototype
  int MyStrlen(char[]);

  // Variable Declarations
  int pep_iStringLength = 0;
  int pep_j;

  // Code
  pep_iStringLength = MyStrlen(str_source);

  for (pep_j = 0; pep_j < pep_iStringLength; pep_j++)
  {
    str_destination[pep_j] = str_source[pep_j];
  }
  str_destination[pep_j] = '\0';

  return;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}