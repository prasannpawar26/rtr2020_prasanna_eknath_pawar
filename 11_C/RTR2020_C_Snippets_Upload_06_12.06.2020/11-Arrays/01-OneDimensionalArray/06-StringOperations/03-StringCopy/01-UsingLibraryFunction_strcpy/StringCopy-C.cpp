#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  char pep_chArray_copy[MAX_STRING_LENGTH];

  int pep_iStringLength = 0;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  strcpy(pep_chArray_copy, pep_chArray);

  printf("\n\n");
  printf("Original String Entered By You Is :\n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  printf("Copied String Is :\n\n");
  printf("%s\n", pep_chArray_copy);

  return 0;
}