#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  int pep_iStringLength = 0;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered By You Is :\n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  pep_iStringLength = strlen(pep_chArray);
  printf("Length Of String Is = %d Characters !!!\n\n", pep_iStringLength);

  return 0;
}
