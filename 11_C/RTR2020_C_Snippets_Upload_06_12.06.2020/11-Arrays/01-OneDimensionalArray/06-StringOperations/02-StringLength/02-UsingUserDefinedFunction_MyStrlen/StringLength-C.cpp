#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  int pep_iStringLength = 0;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered By You Is :\n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  pep_iStringLength = MyStrlen(pep_chArray);
  printf("Length Of String Is = %d Characters !!!\n\n", pep_iStringLength);

  return 0;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  printf("pep_string_length = %d\n", pep_string_length);
  return pep_string_length;
}
