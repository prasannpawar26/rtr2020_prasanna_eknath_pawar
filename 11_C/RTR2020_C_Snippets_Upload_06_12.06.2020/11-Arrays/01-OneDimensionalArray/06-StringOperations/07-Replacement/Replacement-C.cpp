#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char[]);
  void MyStrcpy(char[], char[]);

  // Variable Declarations
  char pep_chArray_Original[MAX_STRING_LENGTH];
  char pep_chArray_VowelsReplaced[MAX_STRING_LENGTH];
  int pep_iString_Length;
  int pep_i;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray_Original, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_Original);

  MyStrcpy(pep_chArray_VowelsReplaced, pep_chArray_Original);

  pep_iString_Length = MyStrlen(pep_chArray_VowelsReplaced);

  for (pep_i = 0; pep_i < pep_iString_Length; pep_i++)
  {
    switch (pep_chArray_VowelsReplaced[pep_i])
    {
      case 'A':
      case 'a':
      case 'E':
      case 'e':
      case 'I':
      case 'i':
      case 'O':
      case 'o':
      case 'U':
      case 'u':
      {
        pep_chArray_VowelsReplaced[pep_i] = '*';
      }
        break;

      default:
      {

      } break;
    }
  }

  

  printf("\n\n");
  printf("String After Replacement Of Vowels By * Is : \n\n");
  printf("%s\n\n", pep_chArray_VowelsReplaced);

  return 0;
}

void MyStrcpy(char str_destination[], char str_source[])
{
  // Function Declarations
  int MyStrlen(char[]);

  int pep_iStringLength = 0;
  int pep_j = 0;

  // Code
  pep_iStringLength = MyStrlen(str_source);

  for (pep_j = 0; pep_j < pep_iStringLength; pep_j++)
  {
    str_destination[pep_j] = str_source[pep_j];
  }

  str_destination[pep_j] = '\0';

  return;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_legnth = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    } else
    {
      pep_string_legnth++;
    }
  }

  return pep_string_legnth;
}
