#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("Original String Entered By You Is :\n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  printf("Reverse String Is :\n\n");
  printf("%s\n", strrev(pep_chArray));

  return 0;
}