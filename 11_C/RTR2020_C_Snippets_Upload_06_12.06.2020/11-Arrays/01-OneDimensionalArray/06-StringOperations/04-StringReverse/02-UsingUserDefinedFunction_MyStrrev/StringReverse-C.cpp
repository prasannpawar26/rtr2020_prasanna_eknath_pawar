#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declearations
  void MyStrrev(char[], char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH], pep_chArray_reversed[MAX_STRING_LENGTH];

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  MyStrrev(pep_chArray_reversed, pep_chArray);

  printf("\n\n");
  printf("Original String Entered By You Is :\n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  printf("Reverse String Is :\n\n");
  printf("%s\n", pep_chArray_reversed);

  return 0;
}

void MyStrrev(char str_destination[], char str_source[])
{
  // Function Declrations
  int MyStrlen(char[]);

  // Variable Declarations
  int pep_i, pep_j, pep_len;
  int pep_iStringLength = 0;

  pep_iStringLength = MyStrlen(str_source);

  pep_len = pep_iStringLength - 1; // Array Index Start With 0, Hence Last Element Will Be (Length - 1)

  for (pep_i = 0, pep_j = pep_len; pep_i < pep_iStringLength, pep_j >= 0; pep_i++, pep_j--)
  {
    str_destination[pep_i] = str_source[pep_j];
  }

  str_destination[pep_i] = '\0';

  return;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}
