#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  char pep_chArray_removespaces[MAX_STRING_LENGTH];
  int pep_iString_Length;
  int pep_i, pep_j;
 
  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray);

  pep_iString_Length = MyStrlen(pep_chArray);

  pep_j = 0;
  for (pep_i = 0; pep_i < pep_iString_Length; pep_i++)
  {
    if (' ' == pep_chArray[pep_i])
    {
      continue;
    }
    else
    {
      pep_chArray_removespaces[pep_j] = pep_chArray[pep_i];
      pep_j++;
    }
  }
  pep_chArray_removespaces[pep_j] = '\0';

  printf("\n\n");
  printf("String After Removal Of Spaces Is :\n\n");
  printf("%s\n\n", pep_chArray_removespaces);

  return 0;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_legnth = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    } else
    {
      pep_string_legnth++;
    }
  }

  return pep_string_legnth;
}
