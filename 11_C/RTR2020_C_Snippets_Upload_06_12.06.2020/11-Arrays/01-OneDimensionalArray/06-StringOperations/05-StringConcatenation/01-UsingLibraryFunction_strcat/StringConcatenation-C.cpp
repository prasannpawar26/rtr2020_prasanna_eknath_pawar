#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Variable Declarations
  char pep_chArray_one[MAX_STRING_LENGTH];
  char pep_chArray_two[MAX_STRING_LENGTH];

  // Code
  printf("\n\n");
  printf("Enter First String : \n\n");
  gets_s(pep_chArray_one, MAX_STRING_LENGTH);

  printf("Enter Second String : \n\n");
  gets_s(pep_chArray_two, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("Original First String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_one);
  printf("Original Second String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_two);

  strcat(pep_chArray_one, pep_chArray_two);

  printf("\n\n");
  printf("After Concatenation First String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_one);
  printf("After Concatenation Second String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_two);

  return 0;
}