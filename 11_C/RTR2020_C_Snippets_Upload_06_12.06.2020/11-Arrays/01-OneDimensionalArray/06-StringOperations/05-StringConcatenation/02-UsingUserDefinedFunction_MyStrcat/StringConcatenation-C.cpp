#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  void MyStrcat(char[], char[]);

  // Variable Declarations
  char pep_chArray_one[MAX_STRING_LENGTH];
  char pep_chArray_two[MAX_STRING_LENGTH];

  // Code
  printf("\n\n");
  printf("Enter First String : \n\n");
  gets_s(pep_chArray_one, MAX_STRING_LENGTH);

  printf("Enter Second String : \n\n");
  gets_s(pep_chArray_two, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("Original First String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_one);
  printf("Original Second String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_two);

  MyStrcat(pep_chArray_one, pep_chArray_two);

  printf("\n\n");
  printf("After Concatenation First String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_one);
  printf("After Concatenation Second String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray_two);

  return 0;
}

void MyStrcat(char str_destination[], char str_source[])
{
  // Function Declarations
  int MyStrlen(char[]);

  // Variable Declarations
  int pep_iStringLength_Source = 0, pep_iStringLength_Destination = 0;
  int pep_i, pep_j;

  // Code
  pep_iStringLength_Source = MyStrlen(str_source);
  pep_iStringLength_Destination = MyStrlen(str_destination);

  for (pep_i = pep_iStringLength_Destination, pep_j = 0; pep_j < pep_iStringLength_Source; pep_i++, pep_j++)
  {
    str_destination[pep_i] = str_source[pep_j];
  }
  str_destination[pep_i] = '\0';

  return;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return (pep_string_length);
}
