#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_chArray[MAX_STRING_LENGTH];
  int pep_iString_Length;

  int pep_count_A = 0, pep_count_E = 0, pep_count_I = 0, pep_count_O = 0,
      pep_count_U = 0;

  int pep_i;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_chArray, MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered By You Is :\n\n");
  printf("%s\n\n", pep_chArray);

  pep_iString_Length = MyStrlen(pep_chArray);

  for (pep_i = 0; pep_i < pep_iString_Length; pep_i++)
  {
    switch (pep_chArray[pep_i])
    {
      case 'A':
      case 'a':
      {
        pep_count_A++;
      }
        break;

      case 'E':
      case 'e':
      {
        pep_count_E++;
      }
        break;

      case 'I':
      case 'i':
      {
        pep_count_I++;
      }
        break;

      case 'O':
      case 'o':
      {
        pep_count_O++;
      }
        break;

      case 'U':
      case 'u':
      {
        pep_count_U++;
      }
        break;

      default:
      {

      } break;
    }
  }

  printf("\n\n");
  printf("In The String Entered By You, The Vowels And The Number Of Their Occurences Are As Follows : \n\n");
  printf("'A' Has Occured = %d Times !!! \n\n", pep_count_A);
  printf("'E' Has Occured = %d Times !!! \n\n", pep_count_E);
  printf("'I' Has Occured = %d Times !!! \n\n", pep_count_I);
  printf("'O' Has Occured = %d Times !!! \n\n", pep_count_O);
  printf("'U' Has Occured = %d Times !!! \n\n", pep_count_U);

  return 0;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_legnth = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_legnth++;
    }
  }

  return pep_string_legnth;
}

