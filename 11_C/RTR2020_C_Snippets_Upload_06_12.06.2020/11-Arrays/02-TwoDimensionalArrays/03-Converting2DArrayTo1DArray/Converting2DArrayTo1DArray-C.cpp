#include <stdio.h>
#include <string.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3

int main(void)
{
  // Variable Declarations
  int pep_iArray_2D[NUMBER_ROWS][NUMBER_COLUMNS];
  int pep_iArray_1D[NUMBER_ROWS * NUMBER_COLUMNS];

  int pep_i, pep_j;
  int pep_num;

  // Code
  printf("Enter Elements Of Your Choice T o Fill Up The Integer 2D Array : \n\n");

  for (pep_i = 0; pep_i < NUMBER_ROWS; pep_i++)
  {
    printf("For ROW NUMBER %d : \n", (pep_i + 1));
    for (pep_j = 0; pep_j < NUMBER_COLUMNS; pep_j++)
    {
      printf("Enter Element Number %d : \n", (pep_j + 1));
      scanf("%d", &pep_num);
      pep_iArray_2D[pep_i][pep_j] = pep_num;
    }
    printf("\n\n");
  }

  printf(
      "Two-Dimensional (2D) Array of Integers : \n\n");
  for (pep_i = 0; pep_i < NUMBER_ROWS; pep_i++)
  {
    printf("******** ROW NUMBER %d ************ \n", (pep_i + 1));
    for (pep_j = 0; pep_j < NUMBER_COLUMNS; pep_j++)
    {
      printf("pep_iArray_2D[%d][%d] =  %d \n", pep_i,
             pep_j, pep_iArray_2D[pep_i][pep_j]);
    }
    printf("\n\n");
  }

  // Converting 2D To 1D Array
  for (pep_i = 0; pep_i < NUMBER_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < NUMBER_COLUMNS; pep_j++)
    {
      // Current_Row * No_Of_Columns_In_Each_Row * Current_Column
      pep_iArray_1D[pep_i * NUMBER_COLUMNS + pep_j] = pep_iArray_2D[pep_i][pep_j];
    }
  }

  for (pep_i = 0; pep_i < NUMBER_ROWS * NUMBER_COLUMNS; pep_i++)
  {
    printf("pep_iArray_1D[%d] =  %d \n", pep_i, pep_iArray_1D[pep_i]);
  }
  printf("\n\n");

  return 0;
}

