#include <stdio.h>
#include <string.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3
#define NUMBER_DEPTHS 2

int main(void)
{
  // Variable Declarations
  int pep_iArray_3D[NUMBER_ROWS][NUMBER_COLUMNS][NUMBER_DEPTHS] =
  {
    {
      {9, 18}, {27, 36}, {45, 54}
    },
    {
      {8, 16}, {24, 32}, {40, 48}
    },
    {
      {7, 14}, {21, 28}, {35, 42}
    },
    {
      {11, 22}, {33, 44}, {55, 66}
    },
    {
      {1, 2}, {3, 4}, {5, 6}
    }    
  };
  
  int pep_width, pep_height, pep_depth;
  int pep_int_size;
  int pep_array_size;

  printf("\n\n");

  pep_int_size = sizeof(int);

  pep_array_size = sizeof(pep_iArray_3D);
  printf("Size Of Three Dimentional (3D) Interger Array Is = %d\n\n", pep_array_size);

  // sizeof(pep_iArray_3D[0] => Number Of Columes In Each Row
  pep_width = pep_array_size / sizeof(pep_iArray_3D[0]);
  printf("Numer Of Rows In Three Dimentional (3D) Interger Array Is = %d\n\n",
         pep_width);

  // pep_iArray_3D[0][0] => Number Of Depth In Each Column
  pep_height = sizeof(pep_iArray_3D[0]) / sizeof(pep_iArray_3D[0][0]);
  printf("Numer Of Columns In Three Dimentional (3D) Interger Array Is = %d\n\n",
         pep_height);

  pep_depth = sizeof(pep_iArray_3D[0][0]) / pep_int_size;
  printf(
      "Numer Of Depths In Three Dimentional (3D) Interger Array Is = %d\n\n",
      pep_depth);

  printf("\n\n");
  printf("Elements In Integer 3D Array : \n\n");

  printf(" **************** ROW 1 *********************\n");
  printf("\t **************** COLUME 1 *********************\n");
  printf("\tpep_iArray_3D[0][0][0] = %d \n", pep_iArray_3D[0][0][0]);
  printf("\tpep_iArray_3D[0][0][1] = %d \n", pep_iArray_3D[0][0][1]);
  printf("\n");
  printf("\t **************** COLUME 2 *********************\n");
  printf("\tpep_iArray_3D[0][1][0] = %d \n", pep_iArray_3D[0][1][0]);
  printf("\tpep_iArray_3D[0][1][1] = %d \n", pep_iArray_3D[0][1][1]);
  printf("\n");
  printf("\t **************** COLUME 3 *********************\n");
  printf("\tpep_iArray_3D[0][2][0] = %d \n", pep_iArray_3D[0][2][0]);
  printf("\tpep_iArray_3D[0][2][1] = %d \n", pep_iArray_3D[0][2][1]);
  printf("\n\n");

  printf(" **************** ROW 2 *********************\n");
  printf("\t **************** COLUME 1 *********************\n");
  printf("\tpep_iArray_3D[1][0][0] = %d \n", pep_iArray_3D[1][0][0]);
  printf("\tpep_iArray_3D[1][0][1] = %d \n", pep_iArray_3D[1][0][1]);
  printf("\n");
  printf("\t **************** COLUME 2 *********************\n");
  printf("\tpep_iArray_3D[1][1][0] = %d \n", pep_iArray_3D[1][1][0]);
  printf("\tpep_iArray_3D[1][1][1] = %d \n", pep_iArray_3D[1][1][1]);
  printf("\n");
  printf("\t **************** COLUME 3 *********************\n");
  printf("\tpep_iArray_3D[2][2][0] = %d \n", pep_iArray_3D[1][2][0]);
  printf("\tpep_iArray_3D[2][2][1] = %d \n", pep_iArray_3D[1][2][1]);
  printf("\n\n");

  printf(" **************** ROW 3 *********************\n");
  printf("\t **************** COLUME 1 *********************\n");
  printf("\tpep_iArray_3D[2][0][0] = %d \n", pep_iArray_3D[2][0][0]);
  printf("\tpep_iArray_3D[2][0][1] = %d \n", pep_iArray_3D[2][0][1]);
  printf("\n");
  printf("\t **************** COLUME 2 *********************\n");
  printf("\tpep_iArray_3D[2][1][0] = %d \n", pep_iArray_3D[2][1][0]);
  printf("\tpep_iArray_3D[2][1][1] = %d \n", pep_iArray_3D[2][1][1]);
  printf("\n");
  printf("\t **************** COLUME 3 *********************\n");
  printf("\tpep_iArray_3D[2][2][0] = %d \n", pep_iArray_3D[2][2][0]);
  printf("\tpep_iArray_3D[2][2][1] = %d \n", pep_iArray_3D[2][2][1]);
  printf("\n\n");

  printf(" **************** ROW 4 *********************\n");
  printf("\t **************** COLUME 1 *********************\n");
  printf("\tpep_iArray_3D[3][0][0] = %d \n", pep_iArray_3D[3][0][0]);
  printf("\tpep_iArray_3D[3][0][1] = %d \n", pep_iArray_3D[3][0][1]);
  printf("\n");
  printf("\t **************** COLUME 2 *********************\n");
  printf("\tpep_iArray_3D[3][1][0] = %d \n", pep_iArray_3D[3][1][0]);
  printf("\tpep_iArray_3D[3][1][1] = %d \n", pep_iArray_3D[3][1][1]);
  printf("\n");
  printf("\t **************** COLUME 3 *********************\n");
  printf("\tpep_iArray_3D[3][2][0] = %d \n", pep_iArray_3D[3][2][0]);
  printf("\tpep_iArray_3D[3][2][1] = %d \n", pep_iArray_3D[3][2][1]);
  printf("\n\n");

  printf(" **************** ROW 5 *********************\n");
  printf("\t **************** COLUME 1 *********************\n");
  printf("\tpep_iArray_3D[4][0][0] = %d \n", pep_iArray_3D[4][0][0]);
  printf("\tpep_iArray_3D[4][0][1] = %d \n", pep_iArray_3D[4][0][1]);
  printf("\n");
  printf("\t **************** COLUME 2 *********************\n");
  printf("\tpep_iArray_3D[4][1][0] = %d \n", pep_iArray_3D[4][1][0]);
  printf("\tpep_iArray_3D[4][1][1] = %d \n", pep_iArray_3D[4][1][1]);
  printf("\n");
  printf("\t **************** COLUME 3 *********************\n");
  printf("\tpep_iArray_3D[4][2][0] = %d \n", pep_iArray_3D[4][2][0]);
  printf("\tpep_iArray_3D[4][2][1] = %d \n", pep_iArray_3D[4][2][1]);
  printf("\n\n");

  return 0;
}

