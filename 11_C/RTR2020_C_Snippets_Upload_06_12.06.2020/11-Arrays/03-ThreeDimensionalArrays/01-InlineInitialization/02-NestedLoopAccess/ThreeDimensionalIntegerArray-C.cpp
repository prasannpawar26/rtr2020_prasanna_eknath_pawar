#include <stdio.h>
#include <string.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3
#define NUMBER_DEPTHS 2

int main(void)
{
  // Variable Declarations
  int pep_iArray_3D[NUMBER_ROWS][NUMBER_COLUMNS][NUMBER_DEPTHS] =
  {
    {
      {9, 18}, {27, 36}, {45, 54}
    },
    {
      {8, 16}, {24, 32}, {40, 48}
    },
    {
      {7, 14}, {21, 28}, {35, 42}
    },
    {
      {11, 22}, {33, 44}, {55, 66}
    },
    {
      {1, 2}, {3, 4}, {5, 6}
    }    
  };
  
  int pep_width, pep_height, pep_depth, pep_num_elements;
  int pep_int_size;
  int pep_array_size;
  int pep_i, pep_j, pep_k;

  printf("\n\n");

  pep_int_size = sizeof(int);

  pep_array_size = sizeof(pep_iArray_3D);
  printf("Size Of Three Dimentional (3D) Interger Array Is = %d\n\n", pep_array_size);

  // sizeof(pep_iArray_3D[0] => Number Of Columes In Each Row
  pep_width = pep_array_size / sizeof(pep_iArray_3D[0]);
  printf("Numer Of Rows In Three Dimentional (3D) Interger Array Is = %d\n\n",
         pep_width);

  // pep_iArray_3D[0][0] => Number Of Depth In Each Column
  pep_height = sizeof(pep_iArray_3D[0]) / sizeof(pep_iArray_3D[0][0]);
  printf("Numer Of Columns In Three Dimentional (3D) Interger Array Is = %d\n\n",
         pep_height);

  pep_depth = sizeof(pep_iArray_3D[0][0]) / pep_int_size;
  printf(
      "Numer Of Depths In Three Dimentional (3D) Interger Array Is = %d\n\n",
      pep_depth);

  pep_num_elements = pep_width * pep_height * pep_depth;
  printf(
      "Numer Of Elements In Three Dimentional (3D) Interger Array Is = %d\n\n",
      pep_num_elements);

  printf("\n\n");
  printf("Elements In Integer 3D Array : \n\n");

  for (pep_i = 0; pep_i < pep_width; pep_i++)
  {
    printf("***** Row %d ******\n", (pep_i + 1));
    for (pep_j = 0; pep_j < pep_height; pep_j++)
    {
      printf("***** Column %d ******\n", (pep_j + 1));
      for (pep_k = 0; pep_k < pep_depth; pep_k++)
      {
        printf("pep_iArray_3D[%d][%d][%d]  = %d\n",pep_i, pep_j, pep_k, pep_iArray_3D[pep_i][pep_j][pep_k]);
      }
      printf("\n");
    }
    printf("\n\n");
  }
  
  

  return 0;
}

