#include <stdio.h>
#include <string.h>

#define NUMBER_ROWS 5
#define NUMBER_COLUMNS 3
#define NUMBER_DEPTHS 2

int main(void) {
  // Variable Declarations
  int pep_iArray_3D[NUMBER_ROWS][NUMBER_COLUMNS][NUMBER_DEPTHS] = {
      {{9, 18}, {27, 36}, {45, 54}},
      {{8, 16}, {24, 32}, {40, 48}},
      {{7, 14}, {21, 28}, {35, 42}},
      {{11, 22}, {33, 44}, {55, 66}},
      {{1, 2}, {3, 4}, {5, 6}}};

  int pep_iArray_1D[NUMBER_ROWS * NUMBER_COLUMNS * NUMBER_DEPTHS];

  int pep_width, pep_height, pep_depth, pep_num_elements;
  int pep_i, pep_j, pep_k;

  printf("\n\n");
  printf("Elements In Integer 3D Array : \n\n");

  for (pep_i = 0; pep_i < NUMBER_ROWS; pep_i++)
  {
    printf("***** Row %d ******\n", (pep_i + 1));
    for (pep_j = 0; pep_j < NUMBER_COLUMNS; pep_j++)
    {
      printf("***** Column %d ******\n", (pep_j + 1));
      for (pep_k = 0; pep_k < NUMBER_DEPTHS; pep_k++)
      {
        printf("pep_iArray_3D[%d][%d][%d]  = %d\n", pep_i, pep_j, pep_k,
               pep_iArray_3D[pep_i][pep_j][pep_k]);
      }
      printf("\n");
    }
    printf("\n\n");
  }

  for (pep_i = 0; pep_i < NUMBER_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < NUMBER_COLUMNS; pep_j++)
    {
      for (pep_k = 0; pep_k < NUMBER_DEPTHS; pep_k++)
      {
        pep_iArray_1D[(pep_i * NUMBER_COLUMNS * NUMBER_DEPTHS) + (pep_j * NUMBER_DEPTHS) + pep_k] = pep_iArray_3D[pep_i][pep_j][pep_k];
      }
    }
  }

  for (pep_i = 0; pep_i < NUMBER_ROWS * NUMBER_COLUMNS  * NUMBER_DEPTHS;
       pep_i++)
  {
    printf("pep_iArray_1D[%d] = %d\n", pep_i, pep_iArray_1D[pep_i]);
  }

  return 0;
}
