#include <stdio.h>
#include <stdlib.h>

struct MyData {
  int pep_i;
  float pep_f;
  double pep_d;
};

int main(void) {
  // Function Prototypes
  void ChangeValues(struct MyData *);

  // Variable Delcarations
  struct MyData *pep_pData = NULL;

  // Code
  printf("\n\n");

  pep_pData = (struct MyData *)malloc(sizeof(struct MyData));
  if (NULL == pep_pData) {
    printf(
        "Failed To Allocate Memory To 'struct MyData' !!! Exitting Now...\n\n");
    exit(0);
  } else {
    printf("Successfully Allocated Memory To 'struct MyData' !!! \n\n");
  }

  pep_pData->pep_i = 56;
  pep_pData->pep_f = 5.6f;
  pep_pData->pep_d = 53.67898f;

  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are :\n\n");
  printf("i = %d\n", pep_pData->pep_i);
  printf("f = %f\n", pep_pData->pep_f);
  printf("d = %lf\n", pep_pData->pep_d);

  ChangeValues(pep_pData);

  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are :\n\n");
  printf("i = %d\n", pep_pData->pep_i);
  printf("f = %f\n", pep_pData->pep_f);
  printf("d = %lf\n", pep_pData->pep_d);

  if (NULL != pep_pData) {
    free(pep_pData);
    pep_pData = NULL;
    printf(
        "Memory Allocated To 'struct MyData' Has Been Successfully Freed "
        "!!!\n\n");
  }

  return 0;
}

void ChangeValues(struct MyData *pep_pParam)
{
  // Code
  pep_pParam->pep_i = 10;
  pep_pParam->pep_f = 10.567f;
  pep_pParam->pep_d = 10.121234;

  return;

}
