#include <stdio.h>
#include <stdlib.h>

struct MyData
{
  int pep_i;
  float pep_f;
  double pep_d;
};

int main(void)
{
  // Variable Declarations
  int pep_i_size;
  int pep_f_size;
  int pep_d_size;
  int pep_struct_MyData_size;
  int pep_pointer_to_struct_MyData_size;

  struct MyData *pep_pData = NULL;

  pep_pData = (struct MyData *)malloc(sizeof(struct MyData));
  if (NULL == pep_pData)
  {
    printf("Failed To Allocate Memory To 'struct MyData' !!! Exitting Now...\n\n");
    exit(0);
  }
  else
  {
    printf("SWuccessfully Allcoated Memory To 'struct MyData' !!! \n\n");
  }

  (*pep_pData).pep_i = 30;
  (*pep_pData).pep_f = 3.40;
  (*pep_pData).pep_d = 4.566730;

  printf("Data Members Of 'struct MyData' Are : \n\n");
  printf("i = %d\n", (*pep_pData).pep_i);
  printf("f = %f\n", (*pep_pData).pep_f);
  printf("d = %lf\n", (*pep_pData).pep_d);

  pep_i_size = sizeof((*pep_pData).pep_i);
  pep_f_size = sizeof((*pep_pData).pep_f);
  pep_d_size = sizeof((*pep_pData).pep_d);

  printf("Size Of Data Members Of 'struct MyData' Are : \n\n");
  printf("Size Of i = %d bytes\n", pep_i_size);
  printf("Size Of f = %d bytes\n", pep_f_size);
  printf("Size Of d = %d bytes\n", pep_d_size);

  pep_struct_MyData_size = sizeof(struct MyData);
  pep_pointer_to_struct_MyData_size = sizeof(struct MyData *);

  printf("\n\n");
  printf("Size Of 'struct MyData' : %d bytes\n\n", pep_struct_MyData_size);
  printf("Size Of pointer to 'struct MyData' : %d bytes\n\n", pep_pointer_to_struct_MyData_size);

  if (NULL == pep_pData)
  {
    free(pep_pData);
    pep_pData = NULL;
    printf("Memory Allcoated To 'struct MyData' Has Been Successfully Freed !!!\n\n");
  }

  return 0;
}
