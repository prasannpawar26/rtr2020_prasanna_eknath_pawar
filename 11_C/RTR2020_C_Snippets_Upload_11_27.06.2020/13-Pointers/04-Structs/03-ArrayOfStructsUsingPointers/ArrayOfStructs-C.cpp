#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>

#define PEP_NAME_LENGTH 100
#define PEP_MARITAL_STATUS 10

struct Employee
{
  char pep_name[PEP_NAME_LENGTH];
  int pep_age;
  char pep_sex;
  float pep_salary;
  char pep_marital_status;
};

int main(void)
{
  // Function Declarations
  void MyGetString(char[], int);

  // Variable Delcarations
  struct Employee *pep_pEmployeeRecord = NULL;
  int pep_num_employees, pep_i;

  // Code
  printf("\n\n");
  printf("Enter Number Of Employees Whose Details You Want To Record : ");
  scanf("%d", &pep_num_employees);

  printf("\n\n");
  pep_pEmployeeRecord =
      (struct Employee *)malloc(sizeof(struct Employee) * pep_num_employees);
  if (NULL == pep_pEmployeeRecord)
  {
    printf("Faile To Allocate Memory For %d Employess !!! Exitting Now...\n\n",
           pep_num_employees);
    exit(0);
  }
  else
  {
    printf("Successfully Allocated Memory For %d Employees !!!\n\n",
           pep_num_employees);
  }

  for (pep_i = 0; pep_i < pep_num_employees; pep_i++)
  {
    printf("\n\n\n\n");
    printf("********** Data Entery For Employee Number %d ************\n", (pep_i + 1));

    printf("\n\n");
    printf("Enter Employee Name : ");
    MyGetString(pep_pEmployeeRecord[pep_i].pep_name, PEP_NAME_LENGTH);
    
    printf("\n\n");
    printf("Enter Employee's Age (int Years) : ");
    scanf("%d", &pep_pEmployeeRecord[pep_i].pep_age);
    
    printf("\n\n");
    printf("Enter Employee's Sex (M/m For Male, F/f For Female) : ");
    pep_pEmployeeRecord[pep_i].pep_sex = getch();
    printf("%c", pep_pEmployeeRecord[pep_i].pep_sex);
    pep_pEmployeeRecord[pep_i].pep_sex =
        toupper(pep_pEmployeeRecord[pep_i].pep_sex);

    printf("\n\n");
    printf("Enter Employee's Salary (In Indian Rupees) : ");
    scanf("%f", &pep_pEmployeeRecord[pep_i].pep_salary);

    printf("\n\n");
    printf("Is The Employee Married? (Y/y For Yes, N/n For No) : ");
    pep_pEmployeeRecord[pep_i].pep_marital_status = getch();
    printf("%c", pep_pEmployeeRecord[pep_i].pep_marital_status);
    pep_pEmployeeRecord[pep_i].pep_marital_status =
        toupper(pep_pEmployeeRecord[pep_i].pep_marital_status);

  }

  printf("\n\n\n\n");
  for (pep_i = 0; pep_i < pep_num_employees; pep_i++)
  {
    printf("****************** Employee Number %d ***************************\n\n", (pep_i + 1));
    printf("Name\t\t\t\t: %s\n", pep_pEmployeeRecord[pep_i].pep_name);
    printf("Age\t\t\t\t: %d years\n", pep_pEmployeeRecord[pep_i].pep_age);
    
    if ('M' == pep_pEmployeeRecord[pep_i].pep_sex)
    {
      printf("Sex\t\t\t\t: Male\n");
    }
    else if ('F' == pep_pEmployeeRecord[pep_i].pep_sex)
    {
      printf("Sex\t\t\t\t: Female\n");
    }
    else
    {
      printf(
          "Sex\t\t\t\t: Invalid Data Entered\n");
    }

    printf("Salary\t\t\t\t: %f Rupees\n",
           pep_pEmployeeRecord[pep_i].pep_salary);

    if ('Y' == pep_pEmployeeRecord[pep_i].pep_marital_status)
    {
      printf("Marital Status\t\t\t\t: Married\n");
    }
    else if ('N' == pep_pEmployeeRecord[pep_i].pep_marital_status)
    {
      printf(
          "Marital Status\t\t\t\t: Unmarried\n");
    }
    else
    {
      printf(
          "Marital Status\t\t\t\t: "
          "Invalud Data Entered\n");
    }

    printf("\n\n");
  }

  if (NULL != pep_pEmployeeRecord)
  {
    free(pep_pEmployeeRecord);
    pep_pEmployeeRecord = NULL;
    printf(
        "Memory Allocated To %d Employees Has Been Successfully Freed !!!\n\n",
        pep_num_employees);
  }

  return 0;
}

void MyGetString(char pep_str[], int pep_str_size) {
  // Variable Declarations
  int pep_i;
  char pep_ch = '\0';

  // Code
  pep_i = 0;

  do
  {
    pep_ch = getch();
    pep_str[pep_i] = pep_ch;
    printf("%c", pep_str[pep_i]);
    pep_i++;
  } while ((pep_ch != '\r') && (pep_i < pep_str_size));

  if (pep_i == pep_str_size)
  {
    pep_str[pep_i - 1] = '\0';
  }
  else
  {
    pep_str[pep_i] = '\0';
  }

  return;
}
