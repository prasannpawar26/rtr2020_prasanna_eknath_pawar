#include <stdio.h>
#include <stdlib.h>

struct MyData {
  int *pep_ptr_i;
  int pep_i;

  float *pep_ptr_f;
  float pep_f;

  double *pep_ptr_d;
  double pep_d;
};

int main(void) {
  // Variable Declarations
  struct MyData *pep_pData = NULL;

  // Code
  printf("\n\n");
  pep_pData = (struct MyData *)malloc(sizeof(struct MyData));
  if (NULL == pep_pData)
  {
    printf("Failed To Allocate Memory To 'struct MyData' !!! Exitting Now ...\n\n");
    exit(0);
  }
  else
  {
    printf("Successfully Allocated Memory To 'struct MyData' !!!\n\n");
  }

  (*pep_pData).pep_i = 17;
  (*pep_pData).pep_ptr_i = &(*pep_pData).pep_i;

  (*pep_pData).pep_f = 4.45;
  (*pep_pData).pep_ptr_f = &(*pep_pData).pep_f;

  (*pep_pData).pep_d = 745.45678;
  (*pep_pData).pep_ptr_d = &(*pep_pData).pep_d;

  printf("\n\n");
  printf("i = %d\n", *((*pep_pData).pep_ptr_i));
  printf("Address Of i = %p\n", (*pep_pData).pep_ptr_i);

  printf("\n\n");
  printf("f = %f\n", *((*pep_pData).pep_ptr_f));
  printf("Address Of f = %p\n", (*pep_pData).pep_ptr_f);

  printf("\n\n");
  printf("i = %lf\n", *((*pep_pData).pep_ptr_d));
  printf("Address Of d = %p\n", (*pep_pData).pep_ptr_d);

  printf("\n\n");

  if (pep_pData)
  {
    free(pep_pData);
    pep_pData = NULL;
    printf(
        "Memory Allocated To 'struct MyData' Has Been Successfully Freed "
        "!!!\n\n");
  }
  return 0;
}