#include <stdio.h>

struct MyData {
  int *pep_ptr_i;
  int pep_i;

  float *pep_ptr_f;
  float pep_f;

  double *pep_ptr_d;
  double pep_d;
};

int main(void) {
  // Variable Declarations
  struct MyData pep_data;

  // Code
  pep_data.pep_i = 7;
  pep_data.pep_ptr_i = &pep_data.pep_i;

  pep_data.pep_f = 7.45;
  pep_data.pep_ptr_f = &pep_data.pep_f;

  pep_data.pep_d = 7.45678;
  pep_data.pep_ptr_d = &pep_data.pep_d;

  printf("\n\n");
  printf("i = %d\n", *(pep_data.pep_ptr_i));
  printf("Address Of i = %p\n", pep_data.pep_ptr_i);

  printf("\n\n");
  printf("f = %f\n", *(pep_data.pep_ptr_f));
  printf("Address Of f = %p\n", pep_data.pep_ptr_f);

  printf("\n\n");
  printf("i = %lf\n", *(pep_data.pep_ptr_d));
  printf("Address Of d = %p\n", pep_data.pep_ptr_d);

  return 0;
}