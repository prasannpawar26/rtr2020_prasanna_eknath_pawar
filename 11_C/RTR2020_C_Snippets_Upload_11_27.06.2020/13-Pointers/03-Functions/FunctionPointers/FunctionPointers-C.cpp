#include <stdio.h>

int main(void)
{
  // Function Declarations
  int AddIntegers(int, int);
  int SubtractIntegers(int, int);
  float AddFloats(float, float);

  // Variable Declarations
  typedef int (*AddIntsFnPtr)(int, int);
  AddIntsFnPtr pep_ptrAddTwoIntegers = NULL;
  AddIntsFnPtr pep_ptrFunc = NULL;

  typedef float (*AddFloatsFnPtr)(float, float);
  AddFloatsFnPtr pep_ptrAddTwoFloats = NULL;

  int pep_iAnswer = 0;
  float pep_fAnswer = 0.0f;
  
  // Code
  pep_ptrAddTwoIntegers = AddIntegers;
  pep_iAnswer = pep_ptrAddTwoIntegers(87, 45);
  printf("\n\n");
  printf("Sum Of Integers = %d\n\n", pep_iAnswer);

  pep_ptrFunc = SubtractIntegers;
  pep_iAnswer = pep_ptrFunc(87, 45);
  printf("\n\n");
  printf("Subtraction Of Integers = %d\n\n", pep_iAnswer);

  pep_ptrAddTwoFloats = AddFloats;
  pep_fAnswer = pep_ptrAddTwoFloats(11.45f, 8.2f);
  printf("\n\n");
  printf("Sum Of Floating-Point Numbers = %f\n\n", pep_fAnswer);

  return 0;
}

int AddIntegers(int pep_a, int pep_b)
{
  // Variable Declarations
  int pep_c;

  // Code
  pep_c = pep_a + pep_b;

  return pep_c;
}

int SubtractIntegers(int pep_a, int pep_b)
{
  // Variable Declarations
  int pep_c;

  // Code
  if (pep_a > pep_b)
  {
    pep_c = pep_a - pep_b;
  }
  else
  {
    pep_c = pep_b - pep_a;
  }

  return pep_c;
}

float AddFloats(float pep_num1, float pep_num2)
{
  // Variable Declarations
  float pep_ans;

  // Code
  pep_ans = pep_num1 + pep_num2;

  return pep_ans;
}
