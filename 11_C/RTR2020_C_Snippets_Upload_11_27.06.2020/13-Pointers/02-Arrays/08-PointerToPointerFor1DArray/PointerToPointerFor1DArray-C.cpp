#include <stdio.h>
#include <stdlib.h>

int main(void)
{

  // Function Declarations
  void MyAlloc(int **, unsigned int);

  // Variable Declarations
  int *pep_piArray = NULL;
  unsigned int pep_num_elements;
  int pep_i;

  printf("\n\n");
  printf("How Many Elements You Want In Integer Array ? \n\n");
  scanf("%u", &pep_num_elements);

  printf("\n\n");
  MyAlloc(&pep_piArray, pep_num_elements);

  printf("Enter %u Elements To Fill Up Your Integer Array : \n\n", pep_num_elements);

  for (pep_i = 0; pep_i < pep_num_elements; pep_i++)
  {
    scanf("%d", &pep_piArray[pep_i]);
  }

  printf("\n\n");
  printf("The %u Elements Entered By You In The Integer Array : \n\n", pep_num_elements);

  for (pep_i = 0; pep_i < pep_num_elements; pep_i++)
  {
    printf("%d\n", pep_piArray[pep_i]);
  }

  printf("\n\n");
  if (NULL != pep_piArray)
  {
    free(pep_piArray);
    pep_piArray = NULL;
    printf("Memory Allocated Has Now Been Successfully Freed !!!\n\n");
  }

  return 0;
}

void MyAlloc(int **pep_ptr, unsigned int pep_numberOfElements)
{
  // Code
  *pep_ptr = (int *)malloc(pep_numberOfElements * sizeof(int));
  if (NULL == *pep_ptr)
  {
    printf("Could Not Allocate Memory !!! Exitting Now...\n\n");
    exit(0);
  }

  printf("MyAlloc() Has SWuccessfully Allocated %zu Bytes For For Integer Array !!! \n\n", (pep_numberOfElements * sizeof(int)));

  return;
}
