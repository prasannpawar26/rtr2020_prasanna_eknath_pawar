#include <stdio.h>
#include <stdarg.h>

#define PEP_NUM_TO_BE_FOUND 3
#define PEP_NUM_ELEMENTS 10

int main(void)
{
  // Function Prototypes
  void FindNumber(int, int, ...);

  // Code
  printf("\n\n");

  FindNumber(PEP_NUM_TO_BE_FOUND, PEP_NUM_ELEMENTS, 4, 3, 2, 1, 0, 1, 2, 3, 5,
             7);

  return 0;
}

void FindNumber(int pep_num_to_be_found, int pep_num, ...)
{
  // Variable Declarations
  int pep_count = 0;
  int pep_n;

  va_list pep_numbers_list;

  // Code
  va_start(pep_numbers_list, pep_num);

  while (pep_num)
  {
    pep_n = va_arg(pep_numbers_list, int);

    if (pep_n == pep_num_to_be_found)
    {
      pep_count++;
    }

    pep_num--;
  }

  if (0 == pep_count)
  {
    printf("Number %d Could Not Be Found !!! \n\n", pep_num_to_be_found);
  }
  else
  {
    printf("Number %d Found %d Times!!! \n\n", pep_num_to_be_found, pep_count);
  }

  va_end(pep_numbers_list);

  return;
}