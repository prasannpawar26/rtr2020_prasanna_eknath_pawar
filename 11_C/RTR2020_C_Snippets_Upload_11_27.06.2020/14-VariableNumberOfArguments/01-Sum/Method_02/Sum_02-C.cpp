#include <stdio.h>
#include <stdarg.h>

int main(void)
{
  // Function Prototypes
  int CalculateSum(int, ...);

  // Variable Declarations
  int pep_answer;

  // Code 
  printf("\n\n");

  pep_answer = CalculateSum(5, 10, 15, 20, 25, 45);
  printf("Answer = %d\n\n", pep_answer);

  pep_answer = CalculateSum(7, 1, 5, 1, 5, 1, 5, 1);
  printf("Answer = %d\n\n", pep_answer);

  pep_answer = CalculateSum(0);
  printf("Answer = %d\n\n", pep_answer);

  return 0;
}

// Variadic Functions
int CalculateSum(int pep_num, ...)
{
  // Function Declarations
  int Va_CalculateSum(int, va_list);

  // Variable Declarations
  int pep_sum_total = 0;

  va_list pep_number_list;

  va_start(pep_number_list, pep_num);

  pep_sum_total = Va_CalculateSum(pep_num, pep_number_list);

  va_end(pep_number_list);

  return pep_sum_total;
}

int Va_CalculateSum(int pep_num, va_list pep_number_list)
{
  // Variable Declarations
  int pep_sum_total = 0;
  int pep_n;

  while (pep_num)
  {
    pep_n = va_arg(pep_number_list, int);
    pep_sum_total = pep_sum_total + pep_n;
    pep_num--;
  }

  return pep_sum_total;
}
