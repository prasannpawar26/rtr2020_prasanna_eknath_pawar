#include <stdio.h>

int main(void)
{
  // variable declarations
  int pep_a;
  int pep_b;
  int pep_c;
  int pep_result;

  // Code
  printf("\n\n");
  printf("Enter One Integer :");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Second Integer :");
  scanf("%d", &pep_b);

  printf("\n\n");
  printf("Enter Third Integer :");
  scanf("%d", &pep_c);

  printf("\n\n");
  printf("If Answer = 0, It Is 'FALSE'. \n");
  printf("If Answer = 1, It Is 'TRUE'. \n");

  pep_result = (pep_a <= pep_b) && (pep_b != pep_c);
  printf("LOGICAL AND(&&) :  Answer Is TRUE (1) If And Only BOTH Conditions Are True. The Answer Is FALSE (0), If Any One Or Both Conditions Are FALSE\n\n");
  printf("A = %d Is Less Than Or Equal To B = %d AND B = %d Is NOT Equal To C = %d                \t Answer = %d\n\n", pep_a, pep_b, pep_b, pep_c, pep_result);

  pep_result = (pep_b >= pep_a) || (pep_a == pep_c);
  printf("LOGICAL OR(||) :  Answer Is FALSE (0) If And Only BOTH Conditions Are False. The Answer Is TRUE (1), If Any One Or Both Conditions Are True\n\n");
  printf("Either B = %d Is Greater Than Or Equal To A = %d OR A = %d Is Equal To C = %d           \t Answer = %d\n\n", pep_b, pep_a, pep_a, pep_c, pep_result);

  pep_result = !pep_a;
  printf("A = %d And Using Logical Not (!) Operator On A Gives Result = %d\n\n", pep_a, pep_result);

  pep_result = !pep_b;
  printf("B = %d And Using Logical Not (!) Operator On B Gives Result = %d\n\n", pep_b, pep_result);

  pep_result = !pep_c;
  printf("C = %d And Using Logical Not (!) Operator On C Gives Result = %d\n\n", pep_c, pep_result);


  pep_result = (!(pep_a <= pep_b) && !(pep_b !=pep_c));
  printf("Using Logical NOT (!) On (a <= b) And Also On (b != c) And Then ANDing Them Afterwards Gives Result = %d\n", pep_result);

  pep_result = !((pep_b >= pep_a) || (pep_a == pep_c));
  printf("Using Logical NOT (!) On Entire Logical Expression (b >= a) || (a == c) Gives Result = %d\n", pep_result);

  printf("\n\n");

  return 0;
}
