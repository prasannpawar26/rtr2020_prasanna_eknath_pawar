#include <stdio.h>

int main(void)
{
  // function prototype
  void PrintBinaryFormOfNumber(unsigned int);

  // variable declarations
  unsigned int pep_a;
  unsigned int pep_result;

  // Code
  printf("\n\n");
  printf("Enter One Integer :");
  scanf("%d", &pep_a);

  printf("\n\n\n\n");

  pep_result = ~pep_a;
  printf("Bitwise COMPLEMENTING Of \nA = %d (Decimal) Gives Result %d (Decimal). \n\n", pep_a, pep_result);

  PrintBinaryFormOfNumber(pep_a);
  PrintBinaryFormOfNumber(pep_result);

  return 0;
}

void PrintBinaryFormOfNumber(unsigned int pep_decimal_number)
{
  // variable declarations
  unsigned int pep_quotient, pep_reminder;
  unsigned int pep_num;
  unsigned int pep_binary_array[8];
  int pep_i;

  // Code
  for (pep_i = 0; pep_i < 8; pep_i++)
  {
    pep_binary_array[pep_i] = 0;
  }
  printf("The Binary Form Of The Original Integer %d is \t=\t", pep_decimal_number);
  pep_num = pep_decimal_number;

  pep_i = 7;
  while (pep_num != 0)
  {
    pep_quotient = pep_num / 2;
    pep_reminder = pep_num % 2;
    pep_binary_array[pep_i] = pep_reminder;
    pep_num = pep_quotient;
    pep_i--;
  }

  for (pep_i = 0; pep_i < 8; pep_i++)
  {
    printf("%u", pep_binary_array[pep_i]);
  }

  printf("\n\n");

  return;
}
