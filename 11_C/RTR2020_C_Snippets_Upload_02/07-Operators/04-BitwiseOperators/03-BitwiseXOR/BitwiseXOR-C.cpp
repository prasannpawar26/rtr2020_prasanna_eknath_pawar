#include <stdio.h>

int main(void)
{
  // function prototype
  void PrintBinaryFormOfNumber(unsigned int );

  // variable declarations
  int pep_a;
  int pep_b;
  int pep_result;

  // Code
  printf("\n\n");
  printf("Enter One Integer :");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Second Integer :");
  scanf("%d", &pep_b);

  printf("\n\n\n\n");

  pep_result = pep_a ^ pep_b;
  printf("Bitwise XOR-ing Of \nA = %d (Decimal) And B = %d (Decimal) Gives Result %d (Decimal). \n\n", pep_a, pep_b, pep_result);

  PrintBinaryFormOfNumber(pep_a);
  PrintBinaryFormOfNumber(pep_b);
  PrintBinaryFormOfNumber(pep_result);

  return 0;
}

void PrintBinaryFormOfNumber(unsigned int pep_decimal_number)
{
  // variable declarations
  unsigned int pep_quotient, pep_reminder;
  unsigned int pep_num;
  unsigned int pep_binary_array[8];
  int pep_i;

  // Code
  for (pep_i = 0; pep_i < 8; pep_i++)
  {
    pep_binary_array[pep_i] = 0;
  }
  printf("The Binary Form Of The Original Integer %d is \t=\t", pep_decimal_number);
  pep_num = pep_decimal_number;

  pep_i = 7;
  while (pep_num != 0)
  {
    pep_quotient = pep_num / 2;
    pep_reminder = pep_num % 2;
    pep_binary_array[pep_i] = pep_reminder;
    pep_num = pep_quotient;
    pep_i--;
  }

  for (pep_i = 0; pep_i < 8; pep_i++)
  {
    printf("%u", pep_binary_array[pep_i]);
  }

  printf("\n\n");

  return;
}
