#include <stdio.h>

int main(void)
{
  // variable declarations
  int pep_a;
  int pep_b;
  int pep_result;

  // Code
  printf("\n\n");
  printf("Enter One Integer :");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Another Integer :");
  scanf("%d", &pep_b);

  printf("\n\n");
  printf("If Answer = 0, It Is 'FALSE'. \n");
  printf("If Answer = 1, It Is 'TRUE'. \n");

  pep_result = (pep_a < pep_b);
  printf("(pep_a < pep_b) A = %d Is Less Than B = %d                \t Answer = %d\n", pep_a, pep_b, pep_result);

  pep_result = (pep_a > pep_b);
  printf("(pep_a > pep_b) A = %d Is Greater Than B = %d             \t Answer = %d\n", pep_a, pep_b, pep_result);

  pep_result = (pep_a <= pep_b);
  printf("(pep_a <= pep_b) A = %d Is Less Than Or Equal To B = %d   \t Answer = %d\n", pep_a, pep_b, pep_result);

  pep_result = (pep_a >= pep_b);
  printf("(pep_a >= pep_b) A = %d Is Greater Than Or Equal To B = %d\t Answer = %d\n", pep_a, pep_b, pep_result);

  pep_result = (pep_a == pep_b);
  printf("(pep_a == pep_b) A = %d Is Equal To B = %d                \t Answer = %d\n", pep_a, pep_b, pep_result);

  pep_result = (pep_a != pep_b);
  printf("(pep_a != pep_b) A = %d Is Not Equal To B = %d            \t Answer = %d\n", pep_a, pep_b, pep_result);

  return 0;
}
