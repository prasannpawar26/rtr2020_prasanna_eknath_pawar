#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_num;
  int *pep_ptr = NULL;
  int *pep_copy_ptr = NULL;

  // Code
  pep_num = 5;
  pep_ptr = &pep_num;

  printf("\n\n");
  printf("***** Before copy_ptr = ptr *****\n\n");
  printf("  num                     %d\n", pep_num);
  printf("  &num                    %p\n", &pep_num);
  printf("  *(&num)                 %d\n", *(&pep_num));
  printf("  ptr                     %p\n", pep_ptr);
  printf("  *ptr                    %d\n", *pep_ptr);

  pep_copy_ptr = pep_ptr;

  printf("\n\n");
  printf("***** After copy_ptr = ptr *****\n\n");
  printf("  num                     %d\n", pep_num);
  printf("  &num                    %p\n", &pep_num);
  printf("  *(&num)                 %d\n", *(&pep_num));
  printf("  ptr                     %p\n", pep_ptr);
  printf("  *ptr                    %d\n", *pep_ptr);
  printf("  copy_ptr                %p\n", pep_copy_ptr);
  printf("  *copy_ptr               %d\n", *pep_copy_ptr);

  return 0;
}
