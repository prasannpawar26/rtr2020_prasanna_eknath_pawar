#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_num;
  int *pep_ptr = NULL;
  int pep_ans;

  // Code
  pep_num = 5;
  pep_ptr = &pep_num;

  printf("\n\n");
  printf("***** Before copy_ptr = ptr *****\n\n");
  printf("  num                     %d\n", pep_num);
  printf("  &num                    %p\n", &pep_num);
  printf("  *(&num)                 %d\n", *(&pep_num));
  printf("  ptr                     %p\n", pep_ptr);
  printf("  *ptr                    %d\n", *pep_ptr);

 printf("\n\n");

 printf("Answer Of (ptr + 10) = %p\n", (pep_ptr + 10));
 printf("Answer Of *(ptr + 10) = %d\n", *(pep_ptr + 10));
 printf("Answer Of (*ptr + 10) = %d\n", (*pep_ptr + 10));
 ++*pep_ptr;
 printf("Answer Of ++*ptr = %d\n", *pep_ptr);
 *pep_ptr++;
 printf("Answer Of *ptr++ = %d\n", *pep_ptr);

 pep_ptr = &pep_num;
 (*pep_ptr)++;
 printf("Answer Of (*ptr)++ = %d\n", *pep_ptr);
 return 0;

}
