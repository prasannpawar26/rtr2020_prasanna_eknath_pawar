#include <stdio.h>

struct Employee
{
  char name[100];
  int age;
  float salary;
  char sex;
  char marital_status;
};

int main(void)
{

  // Code
  printf("\n\n");
  printf("SIZES OF DATA TYPES AND POINTERS TO THOSE REPSECITVE DATA TYPES ARE : \n\n");

  printf(
      "Size of (int)                        :%zd \t\t\t Size Of Pointer To int (int*)                         "
      "  :%zd\n\n",
      sizeof(int), sizeof(int*));
  printf(
      "Size of (float)                      :%zd \t\t\t Size Of Pointer To float (float*)                     "
      "  :%zd\n\n",
      sizeof(float), sizeof(float*));
  printf(
      "Size of (double)                     :%zd \t\t\t Size Of Pointer To double (double*)                   "
      "  :%zd\n\n",
      sizeof(double), sizeof(double*));
  printf(
      "Size of (char)                       :%zd \t\t\t Size Of Pointer To char (char*)                       "
      "  :%zd\n\n",
      sizeof(int), sizeof(int*));
  printf(
      "Size of (struct Employee)            :%zd \t\t\t Size Of Pointer To char (struct Employee*)            "
      "  :%zd\n\n",
      sizeof(struct Employee), sizeof(struct Employee*));

  return 0;
}
