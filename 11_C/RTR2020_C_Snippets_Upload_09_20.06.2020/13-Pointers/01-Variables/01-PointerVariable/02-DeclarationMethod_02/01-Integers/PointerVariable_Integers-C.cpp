#include <stdio.h>

int main(void)
{

  // Variable Declarations
  int pep_num;
  int* pep_ptr = NULL; // ptr is variable of int*

  // Code
  pep_num = 10;

  printf("\n\n");
  printf("******* BEFORE ptr = &num ******\n\n");
  printf("Value Of 'num'                    = %d\n\n", pep_num);
  printf("Address Of 'num'                  = %p\n\n", &pep_num);
  printf("Value At Adress Of 'num'          = %d\n\n", *(&pep_num));

  pep_ptr = &pep_num;

  printf("\n\n");
  printf("******* AFTER ptr = &num ******\n\n");
  printf("Value Of 'num'                    = %d\n\n", pep_num);
  printf("Address Of 'num'                  = %p\n\n", pep_ptr);
  printf("Value At Adress Of 'num'          = %d\n\n", *pep_ptr);

  return 0;
}
