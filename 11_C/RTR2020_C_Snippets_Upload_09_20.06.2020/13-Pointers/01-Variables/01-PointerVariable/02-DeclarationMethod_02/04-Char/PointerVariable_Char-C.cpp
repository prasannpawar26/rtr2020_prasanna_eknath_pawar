#include <stdio.h>

int main(void)
{

  // Variable Declarations
  char pep_ch;
  char* pep_ptr = NULL; // pep_ptr is of type char*

  // Code
  pep_ch = 'A';

  printf("\n\n");
  printf("******* Before ptr = &ch ******\n\n");
  printf("Value Of 'num'                    = %c\n\n", pep_ch);
  printf("Address Of 'num'                  = %p\n\n", &pep_ch);
  printf("Value At Adress Of 'num'          = %c\n\n", *(&pep_ch));

  pep_ptr = &pep_ch;

  printf("\n\n");
  printf("******* After ptr = &ch ******\n\n");
  printf("Value Of 'num'                    = %c\n\n", pep_ch);
  printf("Address Of 'num'                  = %p\n\n", pep_ptr);
  printf("Value At Adress Of 'num'          = %c\n\n", *pep_ptr);

  return 0;
}
