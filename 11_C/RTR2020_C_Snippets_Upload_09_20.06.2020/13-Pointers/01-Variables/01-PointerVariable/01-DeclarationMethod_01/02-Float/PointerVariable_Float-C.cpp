#include <stdio.h>

int main(void)
{

  // Variable Declarations
  float pep_num;
  float *pep_ptr = NULL;

  // Code
  pep_num = 9.6f;

  printf("\n\n");
  printf("******* BEFORE ptr = &num ******\n\n");
  printf("Value Of 'num'                    = %f\n\n", pep_num);
  printf("Address Of 'num'                  = %p\n\n", &pep_num);
  printf("Value At Adress Of 'num'          = %f\n\n", *(&pep_num));

  pep_ptr = &pep_num;

  printf("\n\n");
  printf("******* AFTER ptr = &num ******\n\n");
  printf("Value Of 'num'                    = %f\n\n", pep_num);
  printf("Address Of 'num'                  = %p\n\n", pep_ptr);
  printf("Value At Adress Of 'num'          = %f\n\n", *pep_ptr);

  return 0;
}
