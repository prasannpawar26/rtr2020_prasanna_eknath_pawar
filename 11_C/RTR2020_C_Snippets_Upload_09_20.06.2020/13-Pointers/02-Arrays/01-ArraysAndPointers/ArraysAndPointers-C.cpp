#include <stdio.h>

int main(void) {
  // Variable Declarations
  int pep_iArray[] = {13, 26, 39, 52, 65, 78, 91, 1004, 117, 130};
  float pep_fArray[] = {9.8f, 8.7f, 7.6f, 6.5f, 5.4f};
  double pep_dArray[] = {1.222222, 2.333333, 4.555555};
  char pep_cArray[] = {'A', 'S', 'T', 'R', 'O', 'M', 'E',
                   'D', 'I', 'C', 'O', 'M', 'P', '\0'};

  // Code
  printf("\n\n");
  printf("Integer Array Elements And The Addresses They Occupy Are As Follows : \n\n");
  printf("iArray[0] = %d \t At Address = %p\n", *(pep_iArray + 0),
         (pep_iArray + 0));
  printf("iArray[1] = %d \t At Address = %p\n", *(pep_iArray + 1),
         (pep_iArray + 1));
  printf("iArray[2] = %d \t At Address = %p\n", *(pep_iArray + 2),
         (pep_iArray + 2));
  printf("iArray[3] = %d \t At Address = %p\n", *(pep_iArray + 3),
         (pep_iArray + 3));
  printf("iArray[4] = %d \t At Address = %p\n", *(pep_iArray + 4),
         (pep_iArray + 4));
  printf("iArray[5] = %d \t At Address = %p\n", *(pep_iArray + 5),
         (pep_iArray + 5));
  printf("iArray[6] = %d \t At Address = %p\n", *(pep_iArray + 6),
         (pep_iArray + 6));
  printf("iArray[7] = %d \t At Address = %p\n", *(pep_iArray + 7),
         (pep_iArray + 7));
  printf("iArray[8] = %d \t At Address = %p\n", *(pep_iArray + 8),
         (pep_iArray + 8));
  printf("iArray[9] = %d \t At Address = %p\n", *(pep_iArray + 9),
         (pep_iArray + 9));

  printf("\n\n");
  printf(
      "Floating-Point Array Elements And The Addresses They Occupy Are As "
      "Follows : \n\n");
  printf("fArray[0] = %f \t At Address = %p\n", *(pep_fArray + 0),
         (pep_fArray + 0));
  printf("fArray[1] = %f \t At Address = %p\n", *(pep_fArray + 1),
         (pep_fArray + 1));
  printf("fArray[2] = %f \t At Address = %p\n", *(pep_fArray + 2),
         (pep_fArray + 2));
  printf("fArray[3] = %f \t At Address = %p\n", *(pep_fArray + 3),
         (pep_fArray + 3));
  printf("fArray[4] = %f \t At Address = %p\n", *(pep_fArray + 4),
         (pep_fArray + 4));

  printf("\n\n");
  printf(
      "Double Array Elements And The Addresses They Occupy Are As "
      "Follows : \n\n");
  printf("dArray[0] = %lf \t At Address = %p\n", *(pep_dArray + 0),
         (pep_dArray + 0));
  printf("dArray[1] = %lf \t At Address = %p\n", *(pep_dArray + 1),
         (pep_dArray + 1));
  printf("dArray[2] = %lf \t At Address = %p\n", *(pep_dArray + 2),
         (pep_dArray + 2));

  printf("\n\n");
  printf(
      "Character Array Elements And The Addresses They Occupy Are As "
      "Follows : \n\n");
  printf("cArray[0] = %c \t At Address = %p\n", *(pep_cArray + 0),
         (pep_cArray + 0));
  printf("cArray[1] = %c \t At Address = %p\n", *(pep_cArray + 1),
         (pep_cArray + 1));
  printf("cArray[2] = %c \t At Address = %p\n", *(pep_cArray + 2),
         (pep_cArray + 2));
  printf("cArray[3] = %c \t At Address = %p\n", *(pep_cArray + 3),
         (pep_cArray + 3));
  printf("cArray[4] = %c \t At Address = %p\n", *(pep_cArray + 4),
         (pep_cArray + 4));
  printf("cArray[5] = %c \t At Address = %p\n", *(pep_cArray + 5),
         (pep_cArray + 5));
  printf("cArray[6] = %c \t At Address = %p\n", *(pep_cArray + 6),
         (pep_cArray + 6));
  printf("cArray[7] = %c \t At Address = %p\n", *(pep_cArray + 7),
         (pep_cArray + 7));
  printf("cArray[8] = %c \t At Address = %p\n", *(pep_cArray + 8),
         (pep_cArray + 8));
  printf("cArray[9] = %c \t At Address = %p\n", *(pep_cArray + 9),
         (pep_cArray + 9));
  printf("cArray[10] = %c \t At Address = %p\n", *(pep_cArray + 10),
         (pep_cArray + 10));
  printf("cArray[11] = %c \t At Address = %p\n", *(pep_cArray + 11),
         (pep_cArray + 11));
  printf("cArray[12] = %c \t At Address = %p\n", *(pep_cArray + 12),
         (pep_cArray + 12));
  return 0;
}
