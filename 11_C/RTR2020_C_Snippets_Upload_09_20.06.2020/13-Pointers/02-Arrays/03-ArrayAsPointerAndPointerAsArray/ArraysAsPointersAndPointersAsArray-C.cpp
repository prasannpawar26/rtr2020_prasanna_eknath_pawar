#include <stdio.h>

int main(void) {
  // Variable Declarations
  int pep_iArray[] = {13, 26, 39, 52, 65, 78, 91, 104, 117, 130};
  int *pep_ptr_iArray = NULL;

  // Code
  printf("\n\n");
  printf("Integer Array Elements And Their Addresses By Array Name : \n\n");
  printf("iArray[0] = %d \t\t At Address = %p\n", *(pep_iArray + 0),
         (pep_iArray + 0));
  printf("iArray[1] = %d \t\t At Address = %p\n", *(pep_iArray + 1),
         (pep_iArray + 1));
  printf("iArray[2] = %d \t\t At Address = %p\n", *(pep_iArray + 2),
         (pep_iArray + 2));
  printf("iArray[3] = %d \t\t At Address = %p\n", *(pep_iArray + 3),
         (pep_iArray + 3));
  printf("iArray[4] = %d \t\t At Address = %p\n", *(pep_iArray + 4),
         (pep_iArray + 4));
  printf("iArray[5] = %d \t\t At Address = %p\n", *(pep_iArray + 5),
         (pep_iArray + 5));
  printf("iArray[6] = %d \t\t At Address = %p\n", *(pep_iArray + 6),
         (pep_iArray + 6));
  printf("iArray[7] = %d \t\t At Address = %p\n", *(pep_iArray + 7),
         (pep_iArray + 7));
  printf("iArray[8] = %d \t\t At Address = %p\n", *(pep_iArray + 8),
         (pep_iArray + 8));
  printf("iArray[9] = %d \t\t At Address = %p\n", *(pep_iArray + 9),
         (pep_iArray + 9));

  pep_ptr_iArray = pep_iArray;

  printf("\n\n");
  printf("Integer Array Elements And Their Addresses By Using Pointer : \n\n");
  printf("iArray[0] = %d \t\t At Address = %p\n", pep_ptr_iArray[0],
         &pep_ptr_iArray[0]);
  printf("iArray[1] = %d \t\t At Address = %p\n", pep_ptr_iArray[1],
         &pep_ptr_iArray[1]);
  printf("iArray[2] = %d \t\t At Address = %p\n", pep_ptr_iArray[2],
         &pep_ptr_iArray[2]);
  printf("iArray[3] = %d \t\t At Address = %p\n", pep_ptr_iArray[3],
         &pep_ptr_iArray[3]);
  printf("iArray[4] = %d \t\t At Address = %p\n", pep_ptr_iArray[4],
         &pep_ptr_iArray[4]);
  printf("iArray[5] = %d \t\t At Address = %p\n", pep_ptr_iArray[5],
         &pep_ptr_iArray[5]);
  printf("iArray[6] = %d \t\t At Address = %p\n", pep_ptr_iArray[6],
         &pep_ptr_iArray[6]);
  printf("iArray[7] = %d \t\t At Address = %p\n", pep_ptr_iArray[7],
         &pep_ptr_iArray[7]);
  printf("iArray[8] = %d \t\t At Address = %p\n", pep_ptr_iArray[8],
         &pep_ptr_iArray[8]);
  printf("iArray[9] = %d \t\t At Address = %p\n", pep_ptr_iArray[9],
         &pep_ptr_iArray[9]);
  
  return 0;
}
