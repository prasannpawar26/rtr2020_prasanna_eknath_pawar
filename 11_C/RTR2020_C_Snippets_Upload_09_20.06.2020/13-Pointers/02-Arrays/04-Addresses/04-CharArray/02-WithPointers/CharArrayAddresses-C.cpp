#include <stdio.h>

int main(void)
{
  // Variable Declarations
  char pep_cArray[10];
  int pep_i;
  char *pep_ptr_cArray;

  // Code

  pep_ptr_cArray = pep_cArray;

  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_cArray[pep_i] = (char)(pep_i + 65);
    
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("cArray[%d] = %c\n", pep_i, *(pep_ptr_cArray + pep_i));
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("cArray[%d] = %c \t\t Address = %p\n", pep_i,
           *(pep_ptr_cArray + pep_i),
           (pep_ptr_cArray + pep_i));
  }

  printf("\n\n");

  return 0;
}
