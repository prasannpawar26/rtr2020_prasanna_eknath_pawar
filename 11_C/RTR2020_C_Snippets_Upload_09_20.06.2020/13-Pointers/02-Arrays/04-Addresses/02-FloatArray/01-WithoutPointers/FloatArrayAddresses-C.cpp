#include <stdio.h>

int main(void)
{
  // Variable Declarations
  float pep_fArray[10];
  int pep_i;

  // Code
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_fArray[pep_i] = (float)(pep_i + 1) * 1.5f;
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("fArray[%d] = %f\n", pep_i, pep_fArray[pep_i]);
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("fArray[%d] = %f \t\t Address = %p\n", pep_i, pep_fArray[pep_i], &pep_fArray[pep_i]);
  }

  printf("\n\n");

  return 0;
}
