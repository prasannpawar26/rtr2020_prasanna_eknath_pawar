#include <stdio.h>

int main(void)
{
  // Variable Declarations
  double pep_dArray[10];
  int pep_i;
  double *pep_ptr_dArray;

  // Code

  pep_ptr_dArray = pep_dArray;

  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_dArray[pep_i] = (double)(pep_i + 1) * 3.51f;
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("dArray[%d] = %lf\n", pep_i, *(pep_ptr_dArray + pep_i));
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("dArray[%d] = %f \t\t Address = %p\n", pep_i,
           *(pep_ptr_dArray + pep_i),
           (pep_ptr_dArray + pep_i));
  }

  printf("\n\n");

  return 0;
}
