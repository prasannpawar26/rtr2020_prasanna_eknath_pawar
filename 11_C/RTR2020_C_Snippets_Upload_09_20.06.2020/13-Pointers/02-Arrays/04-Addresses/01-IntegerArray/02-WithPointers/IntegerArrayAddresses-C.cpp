#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_iArray[10];
  int pep_i;
  int *pep_ptr_iArray = NULL;

  // Code
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_iArray[pep_i] = (pep_i + 1) * 3;
  }

  pep_ptr_iArray = pep_iArray;

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("iArray[%d] = %d\n", pep_i, *(pep_ptr_iArray + pep_i));
  }

  printf("\n\n");
  printf("Elements Of The Integer Array : \n\n");
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("iArray[%d] = %d \t\t Address = %p\n", pep_i,
           *(pep_ptr_iArray + pep_i), (pep_ptr_iArray + pep_i));
  }

  printf("\n\n");

  return 0;
}
