#include <stdio.h>

int main(void)
{
  // Function Declarations
  void MathematicalOperations(int, int, int *, int *, int *, int *, int *);

  // Variable Declarations
  int pep_a;
  int pep_b;
  int pep_sum;
  int pep_difference;
  int pep_product;
  int pep_quotient;
  int pep_reminder;

  // Code
  printf("Enter Value For 'A' : ");
  scanf("%d", &pep_a);

  printf("\n\n");

  printf("Enter Value For 'B' : ");
  scanf("%d", &pep_b);

  MathematicalOperations(pep_a, pep_b, &pep_sum, &pep_difference, &pep_product,
                         &pep_quotient, &pep_reminder);
 
  printf("\n\n");
  printf("******Results*******\n");
  printf("\tSum =         %d\n", pep_sum);
  printf("\tDifference =  %d\n", pep_difference);
  printf("\tProduct =     %d\n", pep_product);
  printf("\tQuotient =    %d\n", pep_quotient);
  printf("\tReminder =    %d\n", pep_reminder);

  return 0;
}

void MathematicalOperations(int pep_a, int pep_b, int *pep_sum,
                            int *pep_difference, int *pep_product,
                            int *pep_quotient, int *pep_reminder)
{
  // Code
  *pep_sum = pep_a + pep_b;
  *pep_difference = pep_a - pep_b;
  *pep_product = pep_a * pep_b;
  *pep_quotient = pep_a / pep_b;
  *pep_reminder = pep_a % pep_b;

  return;
}
