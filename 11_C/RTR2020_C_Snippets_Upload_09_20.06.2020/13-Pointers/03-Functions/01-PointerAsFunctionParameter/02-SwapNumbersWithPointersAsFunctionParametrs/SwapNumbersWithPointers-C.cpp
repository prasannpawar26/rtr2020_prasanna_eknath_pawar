#include <stdio.h>

int main(void)
{
  // Function Declarations
  void SwapNumbers(int *, int *);

  // Variable Declarations
  int pep_a;
  int pep_b;

  // Code
  printf("Enter Value For 'A' : ");
  scanf("%d", &pep_a);

  printf("\n\n");

  printf("Enter Value For 'B' : ");
  scanf("%d", &pep_b);

  printf("\n\n");
  printf("****** Before Swapping ******\n\n");
  printf("value Of 'A' = %d\n\n", pep_a);
  printf("value Of 'B' = %d\n\n", pep_b);

  SwapNumbers(&pep_a, &pep_b);

  printf("\n\n");
  printf("****** After Swapping ******\n\n");
  printf("value Of 'A' = %d\n\n", pep_a);
  printf("value Of 'B' = %d\n\n", pep_b);

  printf("\n\n");

  return 0;
}

void SwapNumbers(int *pep_x, int *pep_y)
{
  // Variable Declarations
  int pep_temp;

  
  // Code
  printf("\n\n");
  printf("****** Before Swapping ******\n\n");
  printf("value Of 'X' = %d\n\n", *pep_x);
  printf("value Of 'Y' = %d\n\n", *pep_y);

  pep_temp = *pep_x;
  *pep_x = *pep_y;
  *pep_y = pep_temp;

  printf("\n\n");
  printf("****** After Swapping ******\n\n");
  printf("value Of 'X' = %d\n\n", *pep_x);
  printf("value Of 'Y' = %d\n\n", *pep_y);

  return;
}
