#include <stdio.h>

enum
{
  NEGATIVE = -1,
  ZERO,
  POSITIVE = 1,
};
int main(void)
{
  // Function Declarations
  int Difference(int, int, int *);

  // Variable Declarations
  int pep_a;
  int pep_b;
  int pep_answer, pep_ret;

  // Code
  printf("\n\n");
  printf("Enter Value Of 'A' : ");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Value Of 'B' : ");
  scanf("%d", &pep_b);

  printf("\n\n");

  pep_ret = Difference(pep_a, pep_b, &pep_answer);

  printf("\n\n");
  printf("Difference Of %d And %d = %d\n\n", pep_a, pep_b, pep_answer);

  if (pep_ret == POSITIVE)
  {
    printf("Difference Of %d And %d Is Positive !!!\n\n", pep_a, pep_b);
  }
  else if (pep_ret == NEGATIVE)
  {
    printf("Difference Of %d And %d Is Negative !!!\n\n", pep_a, pep_b);
  }
  else
  {
    printf("Difference Of %d And %d Is Zero !!!\n\n", pep_a, pep_b);
  }

  return 0;
}

int Difference(int pep_a, int pep_b, int *pep_difference)
{
  // Code
  *pep_difference = pep_a - pep_b;

  if (*pep_difference > 0)
  {
    return POSITIVE;
  }
  else if (*pep_difference < 0)
  {
    return NEGATIVE;
  }
  else
  {
    return ZERO;
  }
}
