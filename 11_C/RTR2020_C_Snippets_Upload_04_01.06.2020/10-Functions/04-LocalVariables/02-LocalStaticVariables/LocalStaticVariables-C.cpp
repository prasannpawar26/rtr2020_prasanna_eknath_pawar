#include <stdio.h>

int main(void)
{
  // function declarations
  void change_count(void);

  // Variable Declarations
  int pep_a = 67;

  // Code
  printf("\n\n");
  printf("A = %d\n", pep_a);

  change_count();

  change_count();

  change_count();

  printf("\n\n");

  return 0;
}

void change_count(void) {

  // Local Scope STARTS Here

  // Variable Delcarations
  static int pep_local_count = 0; // local to change_count() and it will retain its value between calls to change_count()

  // Code
  pep_local_count = pep_local_count + 10;
  printf("change_count() Value Of pep_local_count = %d\n", pep_local_count);

  // Local Scope ENDS Here
}
