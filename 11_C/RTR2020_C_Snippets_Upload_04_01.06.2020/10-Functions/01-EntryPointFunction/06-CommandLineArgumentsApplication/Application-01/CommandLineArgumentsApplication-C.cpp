#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *envp[])
{
  // Variable Declarations
  int pep_i;
  int pep_num;
  int pep_sum = 0;

  // Code
  if (argc == 1)
  {
    printf("\n\n");
    printf("No Number given For Addition !!! Exitting Now...\n\n");
    printf("Usage : CommandLineArgumentsApplication-C <1st number> <2nd number> ...\n\n");
    exit(0);
  }

  printf("\n\n");
  printf("Sum Of All Integer Number Command Line  Arguments Is : \n\n");
  for (int i = 1; i < argc; i++)
  {
    pep_num = atoi(argv[i]);
    pep_sum = pep_sum + pep_num;
  }

  printf("Sum = %d\n\n", pep_sum);

  return 0;
}
