#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *envp[])
{
  // Variable Declarations
  
  // Code
  if (argc != 4)
  {
    printf("\n\n");
    printf("Invalid Usage !!! Exitting Now...\n\n");
    printf("Usage : CommandLineArgumentsApplication-C <First Name> <Middle Name> <Last Name>\n\n");
    exit(0);
  }

  printf("\n\n");
  printf("Your Full Name Is : \n\n");
  for (int i = 1; i < argc; i++)
  {
    printf("%s ", argv[i]);
  }

  printf("\n\n");

  return 0;
}
