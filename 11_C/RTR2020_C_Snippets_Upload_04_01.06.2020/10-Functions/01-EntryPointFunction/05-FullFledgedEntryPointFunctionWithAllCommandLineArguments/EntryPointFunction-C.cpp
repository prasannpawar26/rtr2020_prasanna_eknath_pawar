#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // Code
  printf("\n\n");
  printf("Hello World !!! \n\n"); // Library Function 
  printf("Number Of Command Line Arguments = %d\n\n", argc);

  printf("Command Line Arguments Are: \n\n");
  for (int i = 0; i < argc; i++)
  {
    printf("Command Line Argument %d = %s\n", i, argv[i]);
  }
  printf("\n\n");

  printf("First 20 Environmental Variables Passed To This Program Are : \n\n");
  for (int i = 0; i < 70; i++)
  {
    printf("Environmental Variable Number %d = %s \n", (i + 1), envp[i]);
  }
  printf("\n\n");

  return 0;
}
