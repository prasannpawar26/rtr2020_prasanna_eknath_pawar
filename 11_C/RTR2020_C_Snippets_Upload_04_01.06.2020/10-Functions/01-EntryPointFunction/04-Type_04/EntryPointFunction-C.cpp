#include <stdio.h>

int main(int argc, char *argv[])
{
  // Code
  printf("\n\n");
  printf("Hello World !!! \n\n"); // Library Function 
  printf("Number Of Command Line Arguments = %d\n\n", argc);

  printf("Command Line Arguments Are: \n\n");
  for (int i = 0; i < argc; i++)
  {
    printf("Command Line Argument %d = %s\n", i, argv[i]);
  }

  printf("\n\n");

  return 0;
}
