#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // function declarations
  void recursive(unsigned int);

  // variable declarations
  unsigned int pep_num;

  printf("\n\n");
  printf("Enter Any Numner : ");
  scanf("%u", &pep_num);

  printf("\n\n");
  printf("Output Of Recursive Function : \n\n");

  recursive(pep_num);

  printf("\n\n");

  return 0;
}

void recursive(unsigned int pep_num)
{
  // Code
  printf("%d\n", pep_num);

  if (pep_num > 0)
  {
    recursive(pep_num - 1);
  }
}

