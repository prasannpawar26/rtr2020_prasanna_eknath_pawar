#include <stdio.h>

#define MAX_NUMBER(pep_a, pep_b) ((pep_a > pep_b) ? pep_a : pep_b)

int main(int argc, char *argv[], char *envp[])
{
  // function declarations
  void recursive(unsigned int);

  // variable declarations
  int pep_i_01;
  int pep_i_02;
  int pep_i_result;

  float pep_f_01;
  float pep_f_02;
  float pep_f_result;

  printf("\n\n");
  printf("Enter Integer Number : ");
  scanf("%d", &pep_i_01);

  printf("\n\n");
  printf("Enter Another Integer Number : ");
  scanf("%d", &pep_i_02);

  pep_i_result = MAX_NUMBER(pep_i_01, pep_i_02);
    printf("\n\n");
  printf("Result Of Macro Function MAX_NUMBER() = %d \n", pep_i_result);

  printf("\n\n");
  printf("Enter Floating Number : ");
  scanf("%f", &pep_f_01);

  printf("\n\n");
  printf("Enter Another Floating Number : ");
  scanf("%f", &pep_f_02);

  pep_f_result = MAX_NUMBER(pep_f_01, pep_f_02);
  printf("\n\n");
  printf("Result Of Macro Function MAX_NUMBER() = %f \n", pep_f_result);

  printf("\n\n");

  return 0;
}
