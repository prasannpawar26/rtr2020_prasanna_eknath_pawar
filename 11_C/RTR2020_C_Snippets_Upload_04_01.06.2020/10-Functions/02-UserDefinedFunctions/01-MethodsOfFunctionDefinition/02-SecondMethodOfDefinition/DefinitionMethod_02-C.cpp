#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // function prototype / declaration / signature
  int  MyAddition(void);

  // variable declaration
  int pep_result;

  // Code
  pep_result = MyAddition();

  printf("\n\n");
  printf("Sum  = %d\n\n", pep_result);
  return 0;
}

int MyAddition(void)
{
  // Variable Declarations
  int pep_a, pep_b, pep_sum;

  // Code
  printf("\n\n");
  printf("Enter Interger A:");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Interger B:");
  scanf("%d", &pep_b);

  pep_sum = pep_a + pep_b;

  return pep_sum;
}
