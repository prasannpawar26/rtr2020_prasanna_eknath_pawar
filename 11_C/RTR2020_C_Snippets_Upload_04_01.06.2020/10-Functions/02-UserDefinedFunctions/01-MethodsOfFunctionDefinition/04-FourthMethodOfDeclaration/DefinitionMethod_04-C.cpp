#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // function prototype / declaration / signature
  int MyAddition(int, int);

  // variable declarations
  int pep_a, pep_b, pep_result;

  // Code
  printf("\n\n");
  printf("Enter Interger A:");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Interger B:");
  scanf("%d", &pep_b);

  pep_result = MyAddition(pep_a, pep_b);

  printf("\n\n");
  printf("Sum Of %d And %d = %d\n\n", pep_a, pep_b, pep_result);

  return 0;
}

int MyAddition(int pep_a, int pep_b)
{
  // Variable Declarations
  int pep_sum;

  // Code
  pep_sum = pep_a + pep_b;

  return pep_sum;
}
