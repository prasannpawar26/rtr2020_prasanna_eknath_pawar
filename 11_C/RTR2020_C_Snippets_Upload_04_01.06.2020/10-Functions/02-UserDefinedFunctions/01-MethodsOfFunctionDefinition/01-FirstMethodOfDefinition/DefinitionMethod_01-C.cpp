#include <stdio.h>

int main(void)
{
  // function prototype / declaration / signature
  void MyAddition(void);

  // Code
  MyAddition();

  return 0;
}

void MyAddition(void)
{
  // Variable Declarations
  int pep_a, pep_b, pep_sum;

  // Code
  printf("\n\n");
  printf("Enter Interger A:");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Interger B:");
  scanf("%d", &pep_b);

  pep_sum = pep_a + pep_b;

  printf("\n\n");
  printf("Sum Of %d And %d = %d\n\n", pep_a, pep_b, pep_sum);

}
