#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // function prototype / declaration / signature
  void MyAddition(int, int);

  // Variable Declarations
  int pep_a, pep_b;

  // Code
  // Code
  printf("\n\n");
  printf("Enter Interger A:");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Interger B:");
  scanf("%d", &pep_b);


  MyAddition(pep_a, pep_b);

  return 0;
}

void MyAddition(int pep_a, int pep_b)
{
  int pep_sum;

  pep_sum = pep_a + pep_b;

  printf("\n\n");
  printf("Sum Of %d And %d = %d\n\n", pep_a, pep_b, pep_sum);

}
