#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // Function Declarations
  void MyAddition(void);
  int MySubtraction(void);
  void MyMutiplication(int, int);
  int MyDivision(int, int);

  // Variable Declarations
  int pep_result_subraction;
  int pep_a_multiplication, pep_b_multiplication;
  int pep_a_divison, pep_b_divison, pep_result_divison;

  // Code
  MyAddition();
  pep_result_subraction = MySubtraction();
  printf("\n\n");
  printf("Subraction Result : %d\n", pep_result_subraction);

  printf("\n\n");
  printf("Enter A For Multiplication : ");
  scanf("%d", &pep_a_multiplication);

  printf("\n\n");
  printf("Enter B For Multiplication : ");
  scanf("%d", &pep_b_multiplication);

  MyMutiplication(pep_a_multiplication, pep_b_multiplication);

  printf("\n\n");
  printf("Enter A For Division : ");
  scanf("%d", &pep_a_divison);

  printf("\n\n");
  printf("Enter B For Division : ");
  scanf("%d", &pep_b_divison);

  pep_result_divison = MyDivision(pep_a_divison, pep_b_divison);
  printf("\n\n");
  printf("Division Of %d And %d Gives = %d Quotient\n",
         pep_a_divison, pep_b_divison,
         pep_result_divison);
  return 0;
}

void MyAddition(void)
{

  // Variable Declarations
  int pep_a;
  int pep_b;
  int pep_sum;

  printf("\n\n");
  printf("Enter A For Addition : ");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter B For Addition : ");
  scanf("%d", &pep_b);

  pep_sum = pep_a + pep_b;

  printf("\n\n");
  printf("Addition Of %d And %d = %d\n\n", pep_a, pep_b, pep_sum);
}

int MySubtraction(void)
{
  int pep_a;
  int pep_b;
  int pep_sub;

  printf("\n\n");
  printf("Enter A For Subraction : ");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter B For Subraction : ");
  scanf("%d", &pep_b);

  pep_sub = pep_a -  pep_b;

  return pep_sub;
}

void MyMutiplication(int pep_a, int pep_b)
{
  // Variable Declarations
  int pep_multiplication;

  pep_multiplication = pep_a * pep_b;

  printf("\n\n");
  printf("Mutipliction Of %d And %d = %d\n\n", pep_a, pep_b,
         pep_multiplication);
}

int MyDivision(int pep_a, int pep_b)
{
  // variable declarations
  int pep_division;

  // code
  if (pep_a > pep_b)
  {
    pep_division = pep_a / pep_b;
  }
  else
  {
    pep_division = pep_b / pep_a;
  }

  return pep_division;
}
