#include <stdio.h>

int main(int argc, char *argv[], char *envp[]) {
  // function declarations
  void function_country(void);

  // Code
  function_country();

  return 0;
}

void function_country() {
  // Function Declarations
  void function_of_amc();

  // Code
  function_of_amc();

  printf("\n\n");
  printf("I Live In India.");
  printf("\n\n");
}

void function_of_amc() {
  // Function Declarations
  void function_last_name();

  // Code
  function_last_name();

  printf("\n\n");
  printf("Of ASTROMEDICOMP");
}

void function_last_name() {
  // Function Declarations
  void function_middle_name();

  // Code
  function_middle_name();

  printf("\n\n");
  printf("Pawar");
}

void function_middle_name() {
  // Function Declarations
  void function_first_name();

  // Code
  function_first_name();

  printf("\n\n");
  printf("Eknath");
}

void function_first_name() {
  // Function Declarations
  void function_is();

  // Code
  function_is();

  printf("\n\n");
  printf("Prasanna");
}

void function_is() {
  // Function Declarations
  void function_name();

  // Code
  function_name();

  printf("\n\n");
  printf("Is");
}

void function_name() {
  // Function Declarations
  void function_my();

  // Code
  function_my();

  printf("\n\n");
  printf("Name");
}

void function_my() {
  // Code
  printf("\n\n");
  printf("My");
}
