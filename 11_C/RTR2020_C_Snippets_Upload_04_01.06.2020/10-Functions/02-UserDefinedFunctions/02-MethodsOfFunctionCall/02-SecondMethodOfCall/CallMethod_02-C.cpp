#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
  // function declarations
  void display_information(void);
  void function_country(void);

  // Code
  display_information();
  function_country();

  return 0;
}

void display_information()
{
  // function declarations
  void function_my();
  void function_name();
  void function_is();
  void function_first_name();
  void function_middle_name();
  void function_last_name();
  void function_of_amc();

  // Code
  function_my();
  function_name();
  function_is();
  function_first_name();
  function_middle_name();
  function_last_name();
  function_of_amc();
}

void function_my() {
  // Code
  printf("\n\n");
  printf("My");
}

void function_name() {
  // Code
  printf("\n\n");
  printf("Name");
}

void function_is() {
  // Code
  printf("\n\n");
  printf("Is");
}

void function_first_name() {
  // Code
  printf("\n\n");
  printf("Prasanna");
}

void function_middle_name() {
  // Code
  printf("\n\n");
  printf("Eknath");
}

void function_last_name() {
  // Code
  printf("\n\n");
  printf("Pawar");
}

void function_of_amc() {
  // Code
  printf("\n\n");
  printf("Of ASTROMeDICOMP");
}

void function_country()
{
  // Code
  printf("\n\n");
  printf("I Live In India.");
  printf("\n\n");
}

