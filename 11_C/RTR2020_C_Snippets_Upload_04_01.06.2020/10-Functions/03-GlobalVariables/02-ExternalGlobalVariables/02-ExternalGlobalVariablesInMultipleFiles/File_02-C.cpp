#include <stdio.h>


void change_count_two(void) {

  // Variable Declarations
  extern int pep_global_count;

  // Code
  pep_global_count = pep_global_count + 10;
  printf("File_01-C: Value Of global_count in change_count_two() = %d\n",
         pep_global_count);
}