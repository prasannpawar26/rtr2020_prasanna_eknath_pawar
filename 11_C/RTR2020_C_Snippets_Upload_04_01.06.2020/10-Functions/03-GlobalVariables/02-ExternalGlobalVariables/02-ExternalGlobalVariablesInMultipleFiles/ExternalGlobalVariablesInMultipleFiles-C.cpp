#include <stdio.h>

// Global Scope
int pep_global_count;

int main(void)
{
  // function declarations
  void change_count(void);
  void change_count_one(void);
  void change_count_two(void);

  // Code
  printf("\n\n");
  change_count();
  change_count_one();
  change_count_two();

  return 0;
}

void change_count(void) {
  // Code
  pep_global_count =  10;
  printf("global_count in change_count() = %d\n", pep_global_count);
}
