#include <stdio.h>



int main(void)
{
  // function declarations
  void change_count(void);

  // Variable Declarations
  extern int pep_global_count;

  // Code
  printf("\n\n");
  printf("Value Of global_count before calling change_count() = %d\n", pep_global_count);
  change_count();
  printf("Value Of global_count after calling change_count() = %d\n", pep_global_count);
  printf("\n\n");

  return 0;
}

int pep_global_count = 0;

void change_count(void) {
  // Code
  pep_global_count =  10;
  printf("Value Of global_count in change_count() = %d\n", pep_global_count);
}
