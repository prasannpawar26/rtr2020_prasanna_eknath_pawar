#include <stdio.h>

int pep_global_count = 0;

int main(void)
{
  // function declarations
  void change_count_one(void);
  void change_count_two(void);
  void change_count_three(void);

  // Code
  printf("\n\n");
  printf("main() Value Of global_count = %d\n", pep_global_count);

  change_count_one();
  change_count_two();
  change_count_three();

  printf("\n\n");

  return 0;
}

void change_count_one(void)
{
  // Code
  pep_global_count = 100;
  printf("change_count_one() Value Of global_count = %d\n", pep_global_count);
}

void change_count_two(void) {
  // Code
  pep_global_count = pep_global_count + 1;
  printf("change_count_two() Value Of global_count = %d\n", pep_global_count);
}

void change_count_three(void) {
  // Code
  pep_global_count = pep_global_count + 10;
  printf("change_count_three() Value Of global_count = %d\n", pep_global_count);
}
