﻿#include <stdio.h>
#include <conio.h>

/*
Equations:
	Pc(t)=P1∗k1+P2∗k2+P3∗k3+P4∗k4

	k1(t)= (1 − t)∗(1 − t)∗(1 − t);
	k2(t)= 3 * (1 − t)*(1 - t) ∗ t;
	k3(t)= 3 * (1 − t )∗ t * t;
	k4(t)= t * t * t;

	Where t range from [0, 1]
*/

void EvaluateBezierCurve(float ControlPoint[4][3], float v, float *point)
{
	float b0 = (1 - v) * (1 - v) * (1 - v);
	float b1 = 3 * v * (1 - v) * (1 - v);
	float b2 = 3 * v * v * (1 - v);
	float b3 = v * v * v;

	(point)[0] = ControlPoint[0][0] * b0;
	(point)[1] = ControlPoint[0][1] * b0;
	(point)[2] = ControlPoint[0][2] * b0;

	(point)[0] += ControlPoint[1][0] * b1;
	(point)[1] += ControlPoint[1][1] * b1;
	(point)[2] += ControlPoint[1][2] * b1;

	(point)[0] += ControlPoint[2][0] * b2;
	(point)[1] += ControlPoint[2][1] * b2;
	(point)[2] += ControlPoint[2][2] * b2;

	(point)[0] += ControlPoint[3][0] * b3;
	(point)[1] += ControlPoint[3][1] * b3;
	(point)[2] += ControlPoint[3][2] * b3;

	return;
}

void EvaluateBezierSurface(float ControlPoint[16][3], float u, float v, float *point)
{
	float Pu[4][3];
	// compute 4 control points along u direction
	for (int i = 0; i < 4; ++i)
	{
		float curveP[4][3];

		curveP[0][0] = ControlPoint[i * 4 + 0][0];
		curveP[0][1] = ControlPoint[i * 4 + 0][1];
		curveP[0][2] = ControlPoint[i * 4 + 0][2];

		curveP[1][0] = ControlPoint[i * 4 + 1][0];
		curveP[1][1] = ControlPoint[i * 4 + 1][1];
		curveP[1][2] = ControlPoint[i * 4 + 1][2];

		curveP[2][0] = ControlPoint[i * 4 + 2][0];
		curveP[2][1] = ControlPoint[i * 4 + 2][1];
		curveP[2][2] = ControlPoint[i * 4 + 2][2];

		curveP[3][0] = ControlPoint[i * 4 + 3][0];
		curveP[3][1] = ControlPoint[i * 4 + 3][1];
		curveP[3][2] = ControlPoint[i * 4 + 3][2];

		float Temp[3];
		
		EvaluateBezierCurve(curveP, u, Temp);

		Pu[i][0] = Temp[0];
		Pu[i][1] = Temp[1];
		Pu[i][2] = Temp[2];
	}

	float Temp[3];
	EvaluateBezierCurve(Pu, v, Temp);

	point[0] = Temp[0];
	point[1] = Temp[1];
	point[2] = Temp[2];

	return;
}

float ControlPoints[16][3] =
{
	{-1.0f, 1.0f, 0.0f},
	{-0.33f, 1.0f, 0.0f},
	{0.33f, 1.0f, 0.0f},
	{1.0f, 1.0f, 0.0f},

	{-1.0f, 0.33f, 0.0f},
	{-0.33f, 0.33f, 0.0f},
	{0.33f, 0.33f, 0.0f},
	{1.0f, 0.33f, 0.0f},

	{-1.0f, -0.33f, 0.0f},
	{-0.33f, -0.33f, 0.0f},
	{0.33f, -0.33f, 0.0f},
	{1.0f, -0.33f, 0.0f},

	{-1.0f, -1.0f, 0.0f},
	{-0.33f, -1.0f, 0.0f},
	{0.33f, -1.0f, 0.0f},
	{1.0f, -1.0f, 0.0f}
};

int main(void)
{
	float time_u = 0.0f;
	float time_v = 0.0f;
	float time_interval = 0.1f;

	float ResultantPoint[3] = {0.0f, 0.0f, 0.0f};
	while (time_u <= 1.100000f)
	{
		printf("time_u : %f\n\n", time_u);
		time_v = 0.0f;

		while (time_v <= 1.100000f)
		{
			EvaluateBezierSurface(ControlPoints, time_u, time_v, ResultantPoint);

			printf("\tResultantPoint (%f, %f, %f)\n", ResultantPoint[0], ResultantPoint[1], ResultantPoint[2]);

			time_v += time_interval;
		}

		time_u += time_interval;
	}

	return 0;
}
