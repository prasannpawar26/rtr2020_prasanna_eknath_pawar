﻿#include <stdio.h>
#include <conio.h>

/*
Equations:
	Pc(t)=P1∗k1+P2∗k2+P3∗k3+P4∗k4

	k1(t)= (1 − t)∗(1 − t)∗(1 − t);
	k2(t)= 3 * (1 − t)*(1 - t) ∗ t;
	k3(t)= 3 * (1 − t )∗ t * t;
	k4(t)= t * t * t;

	Where t range from [0, 1]
*/

#define K1(T) ((1 - T) * (1 - T) * (1 - T))
#define K2(T) (3 * (1 - T) * (1 - T) * T)
#define K3(T) (3 * (1 - T) * T * T)
#define K4(T) (T * T * T)

float ControlPoints[4][3] =
{
	{-0.5f, 0.0f, 0.0f},
	{-0.25f, 1.0f, 0.0f},
	{-0.0f, 1.0f, 0.0f},
	{1.0f, 0.0f, 0.0f}
};

int main(void)
{
	float time = 0.0f;
	float time_interval = 0.1f;

	float ResultantPoint[3] = {0.0f, 0.0f, 0.0f};
	while (time <= 1.0f)
	{
		ResultantPoint[0] = ControlPoints[0][0] * K1(time);
		ResultantPoint[1] = ControlPoints[0][1] * K1(time);
		ResultantPoint[2] = ControlPoints[0][2] * K1(time);

		ResultantPoint[0] += ControlPoints[1][0] * K2(time);
		ResultantPoint[1] += ControlPoints[1][1] * K2(time);
		ResultantPoint[2] += ControlPoints[1][2] * K2(time);

		ResultantPoint[0] += ControlPoints[2][0] * K3(time);
		ResultantPoint[1] += ControlPoints[2][1] * K3(time);
		ResultantPoint[2] += ControlPoints[2][2] * K3(time);

		ResultantPoint[0] += ControlPoints[3][0] * K4(time);
		ResultantPoint[1] += ControlPoints[3][1] * K4(time);
		ResultantPoint[2] += ControlPoints[3][2] * K4(time);

		printf("ResultantPoint (%f, %f, %f)\n", ResultantPoint[0], ResultantPoint[1], ResultantPoint[2]);

		time += time_interval;
	}

	return 0;
}
