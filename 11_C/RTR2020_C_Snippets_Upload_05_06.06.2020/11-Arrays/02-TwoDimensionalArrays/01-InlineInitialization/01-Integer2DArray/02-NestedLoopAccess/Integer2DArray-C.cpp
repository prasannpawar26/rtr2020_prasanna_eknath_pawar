#include <stdio.h>

int main(void)
{
 
  // Variable Declarations
  int pep_iArray[5][3] = { // In-Line Initialization
    {1, 2, 3},
    {2, 4, 6},
    {3, 6, 9},
    {4, 8, 12},
    {5, 10, 15}
  };

  int pep_int_size;
  int pep_iArray_size;
  int pep_iArray_num_elements, pep_iArray_num_rows, pep_iArray_num_columns;
  int pep_i, pep_j;


  // Code
  printf("\n\n");

  pep_int_size = sizeof(int);

  pep_iArray_size = sizeof(pep_iArray);
  printf("Size Of Two Dimensional (2D) Integer Array Is = %d\n\n", pep_iArray_size);

  pep_iArray_num_rows = pep_iArray_size / sizeof(pep_iArray[0]);
  printf("Number Of Rows In Two Dimensional (2D) Integer Array Is = %d\n\n",
         pep_iArray_num_rows);

  pep_iArray_num_columns = sizeof(pep_iArray[0]) / pep_int_size;
  printf("Number Of Columns In Two Dimensional (2D) Integer Array Is = %d\n\n",
         pep_iArray_num_columns);

  pep_iArray_num_elements = pep_iArray_num_rows * pep_iArray_num_columns;
  printf("Number Of Elements In Two Dimensional (2D) Integer Array Is = %d\n\n",
         pep_iArray_num_elements);

  printf("\n\n");
  printf("Elements In The 2D Array : \n\n");

  for (pep_i = 0; pep_i < pep_iArray_num_rows; pep_i++)
  {
    printf("******** ROWS %d********\n", (pep_i + 1));
    for (pep_j = 0; pep_j < pep_iArray_num_columns; pep_j++)
    {
      printf("iArray[%d][%d] = %d\n", pep_i, pep_j, pep_iArray[pep_i][pep_j]);
    }
    printf("\n\n");
  }

  return 0;
}
