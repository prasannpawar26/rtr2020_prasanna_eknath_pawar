#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_iArray[5][3] = {
    {1, 2, 3},
    {2, 4, 6},
    {3, 6, 9},
    {4, 8, 12},
    {5, 10, 15}
  };

  int pep_int_size;
  int pep_iArray_size;
  int pep_iArray_num_elements, pep_iArray_num_rows, pep_iArray_num_columns;

  // Code
  printf("\n\n");

  pep_int_size = sizeof(int);

  pep_iArray_size = sizeof(pep_iArray);
  printf("Size Of Two Dimensional (2D) Integer Array Is = %d\n\n", pep_iArray_size);

  pep_iArray_num_rows = pep_iArray_size / sizeof(pep_iArray[0]);
  printf("Number Of Rows In Two Dimensional (2D) Integer Array Is = %d \n\n",
         pep_iArray_num_rows);

  pep_iArray_num_columns = sizeof(pep_iArray[0]) / pep_int_size;
  printf("Number Of Columns In Two Dimensional (2D) Integer Array Is = %d \n\n",
         pep_iArray_num_columns);

  pep_iArray_num_elements = pep_iArray_num_rows * pep_iArray_num_columns;
  printf("Number Of Elements In Two Dimensional (2D) Integer Array Are = %d \n\n",
         pep_iArray_num_elements);

  printf("\n\n");
  printf("Elements In The 2D Array : \n\n");

  // ROW 1
  printf("***************Row 1*****************\n");
  printf("pep_iArray[0][0] : %d\n",
         pep_iArray[0][0]);  // Column 1 (0th Element)
  printf("pep_iArray[0][1] : %d\n",
         pep_iArray[0][1]);  // Column 2 (1st Element)
  printf("pep_iArray[0][2] : %d\n",
         pep_iArray[0][2]);  // Column 3 (2nd Element)

  // ROW 2
  printf("***************Row 2*****************\n");
  printf("pep_iArray[1][0] : %d\n",
         pep_iArray[1][0]);  // Column 1 (0th Element)
  printf("pep_iArray[1][1] : %d\n",
         pep_iArray[1][1]);  // Column 2 (1st Element)
  printf("pep_iArray[1][2] : %d\n",
         pep_iArray[1][2]);  // Column 3 (2nd Element)

  // ROW 3
  printf("***************Row 3*****************\n");
  printf("pep_iArray[2][0] : %d\n",
         pep_iArray[2][0]);  // Column 1 (0th Element)
  printf("pep_iArray[2][1] : %d\n",
         pep_iArray[2][1]);  // Column 2 (1st Element)
  printf("pep_iArray[2][2] : %d\n",
         pep_iArray[2][2]);  // Column 3 (2nd Element)

  // ROW 4
  printf("***************Row 4*****************\n");
  printf("pep_iArray[3][0] : %d\n",
         pep_iArray[3][0]);  // Column 1 (0th Element)
  printf("pep_iArray[3][1] : %d\n",
         pep_iArray[3][1]);  // Column 2 (1st Element)
  printf("pep_iArray[3][2] : %d\n",
         pep_iArray[3][2]);  // Column 3 (2nd Element)

  // ROW 5
  printf("***************Row 5*****************\n");
  printf("pep_iArray[4][0] : %d\n",
         pep_iArray[4][0]);  // Column 1 (0th Element)
  printf("pep_iArray[4][1] : %d\n",
         pep_iArray[4][1]);  // Column 2 (1st Element)
  printf("pep_iArray[4][2] : %d\n",
         pep_iArray[4][2]);  // Column 3 (2nd Element)

  printf("\n\n");

  return 0;
}
