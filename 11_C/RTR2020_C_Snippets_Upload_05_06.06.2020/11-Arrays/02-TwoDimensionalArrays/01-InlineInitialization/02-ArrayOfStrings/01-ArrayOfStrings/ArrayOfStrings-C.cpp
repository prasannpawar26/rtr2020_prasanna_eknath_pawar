#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // function declarations
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_strArray[10][15] = {"Hello!", "Welcome",       "To",    "Real",
                               "Time",   "Rendering",     "Batch", "(2020-21)",
                               "Of",     "ASTROMEDICOMP."};
  
  int pep_char_size;
  int pep_strArray_size;
  int pep_strArray_num_elements, pep_strArray_num_rows, pep_strArray_num_columns;
  int pep_strActual_num_chars = 0;
  int pep_i;

  // Code
  printf("\n\n");

  pep_char_size = sizeof(char);
  pep_strArray_size = sizeof(pep_strArray);

  printf(
      "Size Of Two Dimensional (2D) Character Array (String Array) Is = %d\n\n",
      pep_strArray_size);

  pep_strArray_num_rows = pep_strArray_size / sizeof(pep_strArray[0]);
  printf(
      "Number Of Rows In Two Dimensional (2D) Character Array (String Array) "
      "Is = %d \n\n",
      pep_strArray_num_rows);

  pep_strArray_num_columns = sizeof(pep_strArray[0]) / pep_char_size;
  printf(
      "Number Of Columns In Two Dimensional (2D) Character Array (String "
      "Array) Is = %d \n\n",
      pep_strArray_num_columns);

  pep_strArray_num_elements = pep_strArray_num_rows * pep_strArray_num_columns;
  printf(
      "Number Of Elements In Two Dimensional (2D) Character Array (String "
      "Array) Are = %d \n\n",
      pep_strArray_num_elements);

  printf("\n\n");
  
  for (pep_i = 0; pep_i < pep_strArray_num_rows; pep_i++)
  {
    pep_strActual_num_chars =
        pep_strActual_num_chars + MyStrlen(pep_strArray[pep_i]);
  }
  printf(
      "Actual Of Elements (Characters) In Two Dimensional (2D) Character Array (String "
      "Array) Are = %d \n\n",
      pep_strActual_num_chars);

  printf("\n\n");
  printf("Strings In The 2D Array : \n");

  printf("%s ", pep_strArray[0]);
  printf("%s ", pep_strArray[1]);
  printf("%s ", pep_strArray[2]);
  printf("%s ", pep_strArray[3]);
  printf("%s ", pep_strArray[4]);
  printf("%s ", pep_strArray[5]);
  printf("%s ", pep_strArray[6]);
  printf("%s ", pep_strArray[7]);
  printf("%s ", pep_strArray[8]);
  printf("%s ", pep_strArray[9]);

  printf("\n\n");

  return 0;
}

int MyStrlen(char pep_str[])
{
  // variable declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}
