#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
  // function declarations
  int MyStrlen(char[]);

  // Variable Declarations
  char pep_strArray[10][15] = {"Hello!", "Welcome",       "To",    "Real",
                               "Time",   "Rendering",     "Batch", "(2020-21)",
                               "Of",     "ASTROMEDICOMP."};
  
  int pep_iStrLengths[10]; // Store Length Of Each String At Corressponding Indices In "pep_strArray[]"
  int pep_strArray_size;
  int pep_strArray_num_rows;
  int pep_i, pep_j;

  // Code
  pep_strArray_size = sizeof(pep_strArray);
  pep_strArray_num_rows = pep_strArray_size / sizeof(pep_strArray[0]);

  // Storing In Lengths Of All The Strings
  for (pep_i = 0; pep_i < pep_strArray_num_rows; pep_i++)
  {
    pep_iStrLengths[pep_i] = MyStrlen(pep_strArray[pep_i]);
  }

  printf("\n\n");
  printf("The Entire String Array : \n\n");
  for (pep_i = 0; pep_i < pep_strArray_num_rows; pep_i++)
  {
    printf("%s\n",pep_strArray[pep_i]);
  }

  printf("\n\n");
  printf("String In The 2D Array : \n\n");

  for (pep_i = 0; pep_i < pep_strArray_num_rows; pep_i++)
  {
    printf("String Number %d => %s\n\n", (pep_i +1), pep_strArray[pep_i]);
    for (pep_j = 0; pep_j < pep_iStrLengths[pep_i]; pep_j++)
    {
      printf("Character %d = %c\n", (pep_j + 1), pep_strArray[pep_i][pep_j]);
    }
    printf("\n\n");
  }
  
  return 0;
}

int MyStrlen(char pep_str[])
{
  // variable declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}
