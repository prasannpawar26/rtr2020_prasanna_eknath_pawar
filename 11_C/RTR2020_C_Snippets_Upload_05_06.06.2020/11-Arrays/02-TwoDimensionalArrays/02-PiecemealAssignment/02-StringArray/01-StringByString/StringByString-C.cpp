#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void) {
  // Function Prototype
  void MyStrcpy(char[], char[]);

  // Variable Declarations
  char pep_strArray[6][10];
  int pep_char_size;
  int pep_strArray_size;
  int pep_strArray_num_elements, pep_strArray_num_rows,
      pep_strArray_num_columns;
  int pep_i;

  // Code
  printf("\n\n");

  pep_char_size = sizeof(char);

  pep_strArray_size = sizeof(pep_strArray);
  printf(
      "Size Of Two Dimentional (2D) Character Array (String Array) Is = %d "
      "\n\n",
      pep_strArray_size);

  pep_strArray_num_rows = pep_strArray_size / sizeof(pep_strArray[0]);
  printf(
      "Number Of Rows (Strings) In Two Dimentional (2D) Character Array "
      "(String "
      "Array) Is = %d "
      "\n\n",
      pep_strArray_num_rows);

  pep_strArray_num_columns = sizeof(pep_strArray[0]) / pep_char_size;
  printf(
      "Number Of Columns (Strings) In Two Dimentional (2D) Character Array "
      "(String "
      "Array) Is = %d "
      "\n\n",
      pep_strArray_num_columns);

  pep_strArray_num_elements = pep_strArray_num_rows * pep_strArray_num_columns;
  printf(
      "Maximum Number Of Elements (Strings) In Two Dimentional (2D) Character "
      "Array "
      "(String "
      "Array) Is = %d "
      "\n\n",
      pep_strArray_num_elements);

  // PIECE-MEAL ASSIGNMENT
  MyStrcpy(pep_strArray[0], "My");
  MyStrcpy(pep_strArray[1], "Name");
  MyStrcpy(pep_strArray[2], "Is");
  MyStrcpy(pep_strArray[3], "Prasanna");
  MyStrcpy(pep_strArray[4], "Eknath");
  MyStrcpy(pep_strArray[5], "Pawar");

  printf("\n\n");
  printf("The Strings In The 2D Character Array Are : \n\n");

  for (pep_i = 0; pep_i < pep_strArray_num_rows; pep_i++) {
    printf("%s ", pep_strArray[pep_i]);
  }

  printf("\n\n");

  return 0;
}

void MyStrcpy(char str_destination[], char str_source[]) {
  // function prototype
  int MyStrlen(char[]);

  // variable declarations
  int pep_iStringLength = 0;
  int pep_j;

  // Code
  pep_iStringLength = MyStrlen(str_source);

  for (pep_j = 0; pep_j < pep_iStringLength; pep_j++) {
    str_destination[pep_j] = str_source[pep_j];
  }

  str_destination[pep_j] = '\0';

  return;
}

int MyStrlen(char pep_str[])
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  for (pep_j = 0; pep_j < MAX_STRING_LENGTH; pep_j++)
  {
    if ('\0' == pep_str[pep_j])
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;

}
