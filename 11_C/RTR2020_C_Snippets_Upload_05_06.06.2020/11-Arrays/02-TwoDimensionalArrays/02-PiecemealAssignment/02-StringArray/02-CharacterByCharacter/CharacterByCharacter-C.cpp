#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void) {
  // Function Prototype
  void MyStrcpy(char[], char[]);

  // Variable Declarations
  char pep_strArray[6][10];
  int pep_char_size;
  int pep_strArray_size;
  int pep_strArray_num_elements, pep_strArray_num_rows,
      pep_strArray_num_columns;
  int pep_i;

  // Code
  printf("\n\n");

  pep_char_size = sizeof(char);

  pep_strArray_size = sizeof(pep_strArray);
  printf(
      "Size Of Two Dimentional (2D) Character Array (String Array) Is = %d "
      "\n\n",
      pep_strArray_size);

  pep_strArray_num_rows = pep_strArray_size / sizeof(pep_strArray[0]);
  printf(
      "Number Of Rows (Strings) In Two Dimentional (2D) Character Array "
      "(String "
      "Array) Is = %d "
      "\n\n",
      pep_strArray_num_rows);

  pep_strArray_num_columns = sizeof(pep_strArray[0]) / pep_char_size;
  printf(
      "Number Of Columns (Strings) In Two Dimentional (2D) Character Array "
      "(String "
      "Array) Is = %d "
      "\n\n",
      pep_strArray_num_columns);

  pep_strArray_num_elements = pep_strArray_num_rows * pep_strArray_num_columns;
  printf(
      "Maximum Number Of Elements (Strings) In Two Dimentional (2D) Character "
      "Array "
      "(String "
      "Array) Is = %d "
      "\n\n",
      pep_strArray_num_elements);

  // PIECE-MEAL ASSIGNMENT
  // ****** ROW 1 / STRING 1 ***** 
  pep_strArray[0][0] = 'M';
  pep_strArray[0][1] = 'y';
  pep_strArray[0][2] = '\0';

  // ****** ROW 2 / STRING 2 *****
  pep_strArray[1][0] = 'N';
  pep_strArray[1][1] = 'a';
  pep_strArray[1][2] = 'm';
  pep_strArray[1][3] = 'e';
  pep_strArray[1][4] = '\0';

  // ****** ROW 3 / STRING 3 *****
  pep_strArray[2][0] = 'I';
  pep_strArray[2][1] = 's';
  pep_strArray[2][2] = '\0';

  // ****** ROW 4 / STRING 4 *****
  pep_strArray[3][0] = 'P';
  pep_strArray[3][1] = 'r';
  pep_strArray[3][2] = 'a';
  pep_strArray[3][3] = 's';
  pep_strArray[3][4] = 'a';
  pep_strArray[3][5] = 'n';
  pep_strArray[3][6] = 'n';
  pep_strArray[3][7] = 'a';
  pep_strArray[3][8] = '\0';

  // ****** ROW 5 / STRING 5 *****
  pep_strArray[4][0] = 'E';
  pep_strArray[4][1] = 'k';
  pep_strArray[4][2] = 'n';
  pep_strArray[4][3] = 'a';
  pep_strArray[4][4] = 't';
  pep_strArray[4][5] = 'h';
  pep_strArray[4][6] = '\0';

  // ****** ROW 6 / STRING 6 *****");
  pep_strArray[5][0] = 'P';
  pep_strArray[5][1] = 'a';
  pep_strArray[5][2] = 'w';
  pep_strArray[5][3] = 'a';
  pep_strArray[5][4] = 'r';
  pep_strArray[5][5] = '.';
  pep_strArray[5][6] = '\0';

  printf("\n\n");
  printf("The Strings In The 2D Character Array Are : \n\n");

  for (pep_i = 0; pep_i < pep_strArray_num_rows; pep_i++) {
    printf("%s ", pep_strArray[pep_i]);
  }

  printf("\n\n");

  return 0;
}

