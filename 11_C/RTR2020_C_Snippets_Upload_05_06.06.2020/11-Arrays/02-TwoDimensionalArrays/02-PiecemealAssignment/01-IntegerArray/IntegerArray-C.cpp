#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_iArray[3][5];

  int pep_int_size;
  int pep_iArray_size;
  int pep_iArray_num_elements, pep_iArray_num_rows, pep_iArray_num_columns;
  int pep_i, pep_j;

  // Code
  printf("\n\n");

  pep_int_size = sizeof(int);

  pep_iArray_size = sizeof(pep_iArray);
  printf("Size Of Two Dimensional (2D) Integer Array Is = %d\n\n", pep_iArray_size);

  pep_iArray_num_rows = pep_iArray_size / sizeof(pep_iArray[0]);
  printf("Number Of Rows In Two Dimensional (2D) Integer Array Is = %d \n\n",
         pep_iArray_num_rows);

  pep_iArray_num_columns = sizeof(pep_iArray[0]) / pep_int_size;
  printf("Number Of Columns In Two Dimensional (2D) Integer Array Is = %d \n\n",
         pep_iArray_num_columns);

  pep_iArray_num_elements = pep_iArray_num_rows * pep_iArray_num_columns;
  printf("Number Of Elements In Two Dimensional (2D) Integer Array Are = %d \n\n",
         pep_iArray_num_elements);

  printf("\n\n");
  printf("Elements In The 2D Array : \n\n");

  // ROW 1
  pep_iArray[0][0] = 11;
  pep_iArray[0][1] = 22;
  pep_iArray[0][2] = 33;
  pep_iArray[0][3] = 44;
  pep_iArray[0][4] = 55;

  // ROW 2
  pep_iArray[1][0] = 1;
  pep_iArray[1][1] = 2;
  pep_iArray[1][2] = 3;
  pep_iArray[1][3] = 4;
  pep_iArray[1][4] = 5;

  // ROW 3
  pep_iArray[2][0] = 101;
  pep_iArray[2][1] = 102;
  pep_iArray[2][2] = 103;
  pep_iArray[2][3] = 104;
  pep_iArray[2][4] = 105;

  // Display
  for (pep_i = 0; pep_i < pep_iArray_num_rows; pep_i++)
  {
    printf("***************Row %d*****************\n", (pep_i +1));

    for (pep_j = 0; pep_j < pep_iArray_num_columns; pep_j++)
    {
      printf("pep_iArray[%d][%d] : %d\n", pep_i,
             pep_j, pep_iArray[pep_i][pep_j]);  // Column 1 (0th Element)
    }
    printf("\n\n");
  }

  return 0;
}
