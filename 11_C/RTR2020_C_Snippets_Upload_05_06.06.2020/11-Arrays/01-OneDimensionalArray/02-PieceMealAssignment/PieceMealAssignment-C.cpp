#include <stdio.h>

int main(void)
{
 
  // Variable Declarations
  int pep_iArrayOne[10];
  int pep_iArrayTwo[10];

  // Code
  pep_iArrayOne[0] = 4;
  pep_iArrayOne[1] = 8;
  pep_iArrayOne[2] = 12;
  pep_iArrayOne[3] = 16;
  pep_iArrayOne[4] = 20;
  pep_iArrayOne[5] = 24;
  pep_iArrayOne[6] = 28;
  pep_iArrayOne[7] = 32;
  pep_iArrayOne[8] = 36;
  pep_iArrayOne[9] = 40;

  printf("\n\n");
  printf("Piece-Meal (hard-Coded) Assignment And Display Of Elements To Array 'iArrayOne[]' : \n\n");

  printf(
      "1st Element Of 'iArrayOne[]' Or Element At 0th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[0]);
  printf(
      "2nd Element Of 'iArrayOne[]' Or Element At 1st Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[1]);
  printf(
      "3rd Element Of 'iArrayOne[]' Or Element At 2nd Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[2]);
  printf(
      "4th Element Of 'iArrayOne[]' Or Element At 3rd Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[3]);
  printf(
      "5th Element Of 'iArrayOne[]' Or Element At 4th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[4]);
  printf(
      "6th Element Of 'iArrayOne[]' Or Element At 5th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[5]);
  printf(
      "7th Element Of 'iArrayOne[]' Or Element At 6th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[6]);
  printf(
      "8th Element Of 'iArrayOne[]' Or Element At 7th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[7]);
  printf(
      "9th Element Of 'iArrayOne[]' Or Element At 8th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[8]);
  printf(
      "10th Element Of 'iArrayOne[]' Or Element At 9th Index Of 'iArrayOne[]' = "
      "%d\n",
      pep_iArrayOne[9]);

  printf("\n\n");
  printf("Enter 1st Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[0]);
  printf("Enter 2nd Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[1]);
  printf("Enter 3rd Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[2]);
  printf("Enter 4th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[3]);
  printf("Enter 5th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[4]);
  printf("Enter 6th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[5]);
  printf("Enter 7th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[6]);
  printf("Enter 8th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[7]);
  printf("Enter 9th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[8]);
  printf("Enter 10th Element Of Array 'iArrayTwo[]' : ");
  scanf("%d", &pep_iArrayTwo[9]);

  printf("\n\n");
  printf(
      "Piece-Meal (User Input) Assignment And Display Of Elements To Array "
      "'iArrayTwo[]' : \n\n");

  printf(
      "1st Element Of 'iArrayTwo[]' Or Element At 0th Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[0]);
  printf(
      "2nd Element Of 'iArrayTwo[]' Or Element At 1st Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[1]);
  printf(
      "3rd Element Of 'iArrayTwo[]' Or Element At 2nd Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[2]);
  printf(
      "4th Element Of 'iArrayTwo[]' Or Element At 3rd Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[3]);
  printf(
      "5th Element Of 'iArrayTwo[]' Or Element At 4th Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[4]);
  printf(
      "6th Element Of 'iArrayTwo[]' Or Element At 5th Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[5]);
  printf(
      "7th Element Of 'iArrayTwo[]' Or Element At 6th Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[6]);
  printf(
      "8th Element Of 'iArrayTwo[]' Or Element At 7th Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[7]);
  printf(
      "9th Element Of 'iArrayTwo[]' Or Element At 8th Index Of 'iArrayTwo[]' = "
      "%d\n",
      pep_iArrayTwo[8]);
  printf(
      "10th Element Of 'iArrayTwo[]' Or Element At 9th Index Of 'iArrayTwo[]' "
      "= "
      "%d\n",
      pep_iArrayTwo[9]);
  return 0;
}
