#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
 
  // Variable Declarations
  int pep_iArray[NUM_ELEMENTS];
  int pep_i, pep_num, pep_sum = 0;

  // Code
  printf("\n\n");
  printf("Enter Interger Elements For Array : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    scanf("%d", &pep_num);
    pep_iArray[pep_i] = pep_num;
  }

  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    pep_sum = pep_sum + pep_iArray[pep_i];
  }

  printf("\n\n");
  printf("Sum Of All Elements Of Array = %d\n\n", pep_sum);

  return 0;
}
