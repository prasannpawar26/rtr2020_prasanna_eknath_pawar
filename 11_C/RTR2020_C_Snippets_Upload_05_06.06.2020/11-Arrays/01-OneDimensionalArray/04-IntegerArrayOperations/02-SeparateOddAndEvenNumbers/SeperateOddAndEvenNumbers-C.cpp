#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
 
  // Variable Declarations
  int pep_iArray[NUM_ELEMENTS];
  int pep_i, pep_num, pep_sum = 0;

  // Code
  printf("\n\n");
  printf("Enter Interger Elements For Array : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    scanf("%d", &pep_num);
    pep_iArray[pep_i] = pep_num;
  }

  printf("\n\n");
  printf("Even Numbers Amongst The Array Elements Are : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    if ((pep_iArray[pep_i] % 2) == 0)
    {
      printf("%d\n", pep_iArray[pep_i]);
    }
  }

  printf("\n\n");
  printf("Odd Numbers Amongst The Array Elements Are : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++) {
    if ((pep_iArray[pep_i] % 2) != 0)
    {
      printf("%d\n", pep_iArray[pep_i]);
    }
  }

  return 0;
}
