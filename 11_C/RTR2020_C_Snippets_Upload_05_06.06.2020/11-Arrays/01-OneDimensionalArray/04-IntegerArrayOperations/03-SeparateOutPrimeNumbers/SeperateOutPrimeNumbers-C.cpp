#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
 
  // Variable Declarations
  int pep_iArray[NUM_ELEMENTS];
  int pep_i, pep_j, pep_num, pep_count = 0;

  // Code
  printf("\n\n");
  printf("Enter Interger Elements For Array : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    scanf("%d", &pep_num);

    // Convert Negative Number To Its Positive
    if (pep_num < 0)
    {
      pep_num = -1 * pep_num;
    }
    pep_iArray[pep_i] = pep_num;
  }

  printf("\n\n");
  printf("The Array Elements Are : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    printf("%d\n", pep_iArray[pep_i]);
  }

  printf("\n\n");
  printf("Prime Numbers Amongst The Array Elements Are : \n\n");
  for (pep_i = 0; pep_i < NUM_ELEMENTS; pep_i++)
  {
    for (pep_j = 1; pep_j <= pep_iArray[pep_i]; pep_j++)
    {
      // IF A NUMBER IS PRIME, IT IS ONLY DIVISIBLE BY 1 AND ITSELF. 
      if ((pep_iArray[pep_i] % pep_j) == 0) 
      {
        pep_count++;
      }
    }

    if (pep_count == 2)
    {
      // IF A NUMBER IS PRIME, IT IS ONLY DIVISIBLE BY 1 AND ITSELF.  HENCE
      // Count == 2 Checking Here
      printf("%d\n", pep_iArray[pep_i]);
    }

    pep_count = 0;
  }

  return 0;
}
