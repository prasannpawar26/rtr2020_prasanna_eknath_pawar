#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_iArray[] = {34, 45, 67 ,78 ,90, 12, 32, 31, 51, 76};
  int pep_int_size;
  int pep_iArray_size;
  int pep_iArray_num_elements;

  float pep_fArray[] = {3.4f, 4.5f, 6.7f, 7.8f, 9.01f, 1.2f, 3.2f, 3.1f, 5.1f, 7.6f};
  int pep_float_size;
  int pep_fArray_size;
  int pep_fArray_num_elements;

  char pep_cArray[] = {'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P'};
  int pep_char_size;
  int pep_cArray_size;
  int pep_cArray_num_elements;

  // Code
  printf("***************iArray **************************\n\n");
  printf("Inline Initialization And Piece-Meal Display Of Elements Of Array 'pep_iArray[]' : \n\n");
  printf("iArray[0] (1st Element) =%d\n", pep_iArray[0]);
  printf("iArray[1] (2nd Element) =%d\n", pep_iArray[1]);
  printf("iArray[2] (3rd Element) =%d\n", pep_iArray[2]);
  printf("iArray[3] (4th Element) =%d\n", pep_iArray[3]);
  printf("iArray[4] (5th Element) =%d\n", pep_iArray[4]);
  printf("iArray[5] (6th Element) =%d\n", pep_iArray[5]);
  printf("iArray[6] (7th Element) =%d\n", pep_iArray[6]);
  printf("iArray[7] (8th Element) =%d\n", pep_iArray[7]);
  printf("iArray[8] (9th Element) =%d\n", pep_iArray[8]);
  printf("iArray[9] (10th Element) =%d\n", pep_iArray[9]);

  pep_int_size = sizeof(int);
  pep_iArray_size = sizeof(pep_iArray);
  pep_iArray_num_elements = pep_iArray_size / pep_int_size;

  printf("Sizeof DataType 'int' = %d bytes \n", pep_int_size);
  printf("Number Of Elements In 'int' Array 'iArray[]' = %d Elements\n", pep_iArray_num_elements);
  printf("Sizeof Array 'iArray[]' (%d Elements * %d Bytes) = %d Bytes\n",
         pep_iArray_num_elements, pep_int_size, pep_iArray_size);

  printf("***************fArray **************************\n\n");
  printf("Inline Initialization And Piece-Meal Display Of Elements Of Array 'pep_fArray[]' : \n\n");
  printf("fArray[0] (1st Element) =%f\n", pep_fArray[0]);
  printf("fArray[1] (2nd Element) =%f\n", pep_fArray[1]);
  printf("fArray[2] (3rd Element) =%f\n", pep_fArray[2]);
  printf("fArray[3] (4th Element) =%f\n", pep_fArray[3]);
  printf("fArray[4] (5th Element) =%f\n", pep_fArray[4]);
  printf("fArray[5] (6th Element) =%f\n", pep_fArray[5]);
  printf("fArray[6] (7th Element) =%f\n", pep_fArray[6]);
  printf("fArray[7] (8th Element) =%f\n", pep_fArray[7]);
  printf("fArray[8] (9th Element) =%f\n", pep_fArray[8]);
  printf("fArray[9] (10th Element) =%f\n", pep_fArray[9]);

  pep_float_size = sizeof(float);
  pep_fArray_size = sizeof(pep_fArray);
  pep_fArray_num_elements = pep_fArray_size / pep_float_size;

  printf("Sizeof DataType 'float' = %d bytes \n", pep_float_size);
  printf("Number Of Elements In 'float' Array 'fArray[]' = %d Elements\n",
         pep_fArray_num_elements);
  printf("Sizeof Array 'fArray[]' (%d Elements * %d Bytes) = %d Bytes\n",
         pep_fArray_num_elements, pep_float_size, pep_fArray_size);

  printf("***************cArray **************************\n\n");
  printf("Inline Initialization And Piece-Meal Display Of Elements Of Array 'pep_cArray[]' : \n\n");
  printf("cArray[0] (1st Element) =%c\n", pep_cArray[0]);
  printf("cArray[1] (2nd Element) =%c\n", pep_cArray[1]);
  printf("cArray[2] (3rd Element) =%c\n", pep_cArray[2]);
  printf("cArray[3] (4th Element) =%c\n", pep_cArray[3]);
  printf("cArray[4] (5th Element) =%c\n", pep_cArray[4]);
  printf("cArray[5] (6th Element) =%c\n", pep_cArray[5]);
  printf("cArray[6] (7th Element) =%c\n", pep_cArray[6]);
  printf("cArray[7] (8th Element) =%c\n", pep_cArray[7]);
  printf("cArray[8] (9th Element) =%c\n", pep_cArray[8]);
  printf("cArray[9] (10th Element) =%c\n", pep_cArray[9]);
  printf("cArray[10] (11th Element) =%c\n", pep_cArray[10]);
  printf("cArray[11] (12th Element) =%c\n", pep_cArray[11]);
  printf("cArray[12] (13th Element) =%c\n", pep_cArray[12]);
  printf("cArray[13] (14th Element) =%c\n", pep_cArray[13]);

  pep_char_size = sizeof(char);
  pep_cArray_size = sizeof(pep_cArray);
  pep_cArray_num_elements = pep_cArray_size / pep_char_size;

  printf("Sizeof DataType 'char' = %d bytes \n", pep_char_size);
  printf("Number Of Elements In 'char' Array 'cArray[]' = %d Elements\n",
         pep_cArray_num_elements);
  printf("Sizeof Array 'cArray[]' (%d Elements * %d Bytes) = %d Bytes\n",
         pep_cArray_num_elements, pep_char_size, pep_cArray_size);

  return 0;
}
