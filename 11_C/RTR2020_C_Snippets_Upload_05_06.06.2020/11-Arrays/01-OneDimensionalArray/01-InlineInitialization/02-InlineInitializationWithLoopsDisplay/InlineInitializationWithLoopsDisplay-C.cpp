#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_iArray[] = {34, 45, 67 ,78 ,90, 12, 32, 31, 51, 76};
  int pep_int_size;
  int pep_iArray_size;
  int pep_iArray_num_elements;

  float pep_fArray[] = {1.2f, 2.3f, 3.4f, 5.6f, 6.7f, 7.8f, 9.1f, 10.11f, 11.12f, 12.13f};
  int pep_float_size;
  int pep_fArray_size;
  int pep_fArray_num_elements;

  char pep_cArray[] = {'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P'};
  int pep_char_size;
  int pep_cArray_size;
  int pep_cArray_num_elements;

  // Code
  printf("***************iArray **************************\n\n");
  printf("Inline Initialization And Piece-Meal Display Of Elements Of Array 'pep_iArray[]' : \n\n");
  
  pep_int_size = sizeof(int);
  pep_iArray_size = sizeof(pep_iArray);
  pep_iArray_num_elements = pep_iArray_size / pep_int_size;

  for (int pep_i = 0; pep_i < pep_iArray_num_elements; pep_i++)
  {
    printf("iArray[%d] (Element %d) = %d\n", pep_i, (pep_i + 1), pep_iArray[pep_i]);
  }

  printf("Sizeof DataType 'int' = %d bytes \n", pep_int_size);
  printf("Number Of Elements In 'int' Array 'iArray[]' = %d Elements\n", pep_iArray_num_elements);
  printf("Sizeof Array 'iArray[]' (%d Elements * %d Bytes) = %d Bytes\n",
         pep_iArray_num_elements, pep_int_size, pep_iArray_size);

  printf("***************fArray **************************\n\n");
  printf("Inline Initialization And Piece-Meal Display Of Elements Of Array 'pep_fArray[]' : \n\n");
  

  pep_float_size = sizeof(float);
  pep_fArray_size = sizeof(pep_fArray);
  pep_fArray_num_elements = pep_fArray_size / pep_float_size;

  for (int pep_i = 0; pep_i < pep_fArray_num_elements; pep_i++)
  {
    printf("fArray[%d] (Element %d) = %f\n", pep_i, (pep_i + 1),
           pep_fArray[pep_i]);
  }

  printf("Sizeof DataType 'float' = %d bytes \n", pep_float_size);
  printf("Number Of Elements In 'float' Array 'fArray[]' = %d Elements\n",
         pep_fArray_num_elements);
  printf("Sizeof Array 'fArray[]' (%d Elements * %d Bytes) = %d Bytes\n",
         pep_fArray_num_elements, pep_float_size, pep_fArray_size);

  printf("***************cArray **************************\n\n");
  printf("Inline Initialization And Piece-Meal Display Of Elements Of Array 'pep_cArray[]' : \n\n");
  

  pep_char_size = sizeof(char);
  pep_cArray_size = sizeof(pep_cArray);
  pep_cArray_num_elements = pep_cArray_size / pep_char_size;

  for (int pep_i = 0; pep_i < pep_cArray_num_elements; pep_i++)
  {
    printf("cArray[%d] (Element %d) = %c\n", pep_i, (pep_i + 1),
           pep_cArray[pep_i]);
  }

  printf("Sizeof DataType 'char' = %d bytes \n", pep_char_size);
  printf("Number Of Elements In 'char' Array 'cArray[]' = %d Elements\n",
         pep_cArray_num_elements);
  printf("Sizeof Array 'cArray[]' (%d Elements * %d Bytes) = %d Bytes\n",
         pep_cArray_num_elements, pep_char_size, pep_cArray_size);

  return 0;
}
