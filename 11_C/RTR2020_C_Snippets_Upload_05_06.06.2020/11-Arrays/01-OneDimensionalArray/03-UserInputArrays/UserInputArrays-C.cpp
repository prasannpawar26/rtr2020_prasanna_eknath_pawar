#include <stdio.h>
#include <conio.h>

#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 6
#define CHAR_ARRAY_NUM_ELEMENTS 8

int main(void)
{
 
  // Variable Declarations
  int pep_iArray[INT_ARRAY_NUM_ELEMENTS];
  float pep_fArray[INT_ARRAY_NUM_ELEMENTS];
  char pep_cArray[INT_ARRAY_NUM_ELEMENTS];

  int pep_i, pep_num;

  printf("\n\n");
  printf("Enter Elements For Integer Array : \n");
  for (pep_i = 0; pep_i < INT_ARRAY_NUM_ELEMENTS; pep_i++)
  {
    scanf("%d", &pep_iArray[pep_i]);
  }

  printf("\n\n");
  printf("Enter Elements For Float Array : \n");
  for (pep_i = 0; pep_i < FLOAT_ARRAY_NUM_ELEMENTS; pep_i++) {
    scanf("%f", &pep_fArray[pep_i]);
  }

  printf("\n\n");
  printf("Enter Elements For Char Array : \n");
  for (pep_i = 0; pep_i < CHAR_ARRAY_NUM_ELEMENTS; pep_i++) {
    pep_cArray[pep_i] = getch();
    printf("%c\n", pep_cArray[pep_i]);
  }

  printf("\n\n");
  printf(" Integer Array Elements Enter By You : \n");
  for (pep_i = 0; pep_i < INT_ARRAY_NUM_ELEMENTS; pep_i++) {
    printf("%d\n", pep_iArray[pep_i]);
  }

  printf("\n\n");
  printf(" Floating Array Elements Enter By You : \n");
  for (pep_i = 0; pep_i < FLOAT_ARRAY_NUM_ELEMENTS; pep_i++) {
    printf("%f\n", pep_fArray[pep_i]);
  }

  printf("\n\n");
  printf(" Char Array Elements Enter By You : \n");
  for (pep_i = 0; pep_i < CHAR_ARRAY_NUM_ELEMENTS; pep_i++) {
    printf("%c\n", pep_cArray[pep_i]);
  }

  return 0;
}
