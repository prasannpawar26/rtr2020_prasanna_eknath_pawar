#include <stdio.h>

int main(void)
{
 
  // Variable Declarations
  char pep_chArray_01[] = {'A', 'S', 'T', 'R', 'O', 'M', 'E',
                           'D', 'I', 'C', 'O', 'M', 'P', '\0'
  };

  char pep_chArray_02[] = {'W', 'E', 'L', 'C', 'O', 'M', 'E', 'S', '\0'};

  char pep_chArray_03[] = {'Y', 'O', 'U', '\0'};

  char pep_chArray_04[] = "To"; //\0 Is Assumed, Size Is Given As 3

  char pep_chArray_05[] = "REAL TIME RENDERING BATCH 2020-21"; //\0 Is Assumed

  char pep_cArray_WithoutNullTerminator[] = {'H', 'e', 'l', 'l', 'o'};

  // Code
  printf("\n\n");

  printf("Size Of chArray_01 : %zu \n\n", sizeof(pep_chArray_01));
  printf("Size Of chArray_02 : %zu \n\n", sizeof(pep_chArray_02));
  printf("Size Of chArray_03 : %zu \n\n", sizeof(pep_chArray_03));
  printf("Size Of chArray_04 : %zu \n\n", sizeof(pep_chArray_04));
  printf("Size Of chArray_05 : %zu \n\n", sizeof(pep_chArray_05));

  printf("\n\n");
  printf("The String Are : \n\n");
  printf("chArray_01 : %s \n\n", pep_chArray_01);
  printf("chArray_02 : %s \n\n", pep_chArray_02);
  printf("chArray_03 : %s \n\n", pep_chArray_03);
  printf("chArray_04 : %s \n\n", pep_chArray_04);
  printf("chArray_05 : %s \n\n", pep_chArray_05);

  printf("\n\n");
  printf("Size Of pep_cArray_WithoutNullTerminator : %zu \n\n",
         sizeof(pep_cArray_WithoutNullTerminator));
  printf("pep_cArray_WithoutNullTerminator : %s \n\n",
         pep_cArray_WithoutNullTerminator);

  return 0;
}
