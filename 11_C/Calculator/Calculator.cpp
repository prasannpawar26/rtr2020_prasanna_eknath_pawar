#include <stdio.h>

enum
{
  EXIT = 0,
  STANDARD = 1,
  PROGRAMMER,
  SCIENTIFIC
};

enum
{
  STANDARD_EXIT = 0,
  STANDARD_ADDITION = 1,
  STANDARD_SUBTRACTION,
  STANDARD_DIVISION,
  STANDARD_REMINDER,
  STANDARD_MULTIPLICATION
};

enum {
  PROGRAMMER_EXIT = 0,
  PROGRAMMER_AND,
  PROGRAMMER_OR,
  PROGRAMMER_NOT,
  PROGRAMMER_XOR,
  PROGRAMMER_LEFT_SHIFT,
  PROGRAMMER_RIGHT_SHIFT
};

enum
{
  SCIENTIFIC_EXIT = 0,
  SCIENTIFIC_SIN,
  SCIENTIFIC_COS,
  SCIENTIFIC_SEC,
  SCIENTIFIC_COSEC,
  SCIENTIFIC_TAN,
  SCIENTIFIC_COT
};

int main(void)
{
  // Varaiable Declarations
  int pep_mode;

  do
  {
    printf("Select Mode : \n\n");
    printf("0. For Exit\n");
    printf("1. Standard\n");
    printf("2. Programmer\n");
    printf("3. Scientific\n");
    printf("4. Date Calculation\n");

    scanf("%d", &pep_mode);

    if (EXIT == pep_mode)
    {
      break;
    }

    switch (pep_mode)
    {
      case STANDARD:
      {
        int pep_operation;

        do
        {
          printf("Select Operation : \n\n");
          printf("0. For Exit\n");
          printf("1. Addition\n");
          printf("2. Subtraction\n");
          printf("3. Division\n");
          printf("4. Reminder\n");
          printf("5. Multiplication\n");

          scanf("%d", &pep_operation);

          printf("\n\n");

          if (STANDARD_EXIT == pep_operation)
          {
            break;
          }
          else if (STANDARD_ADDITION == pep_operation)
          {
            printf("STANDARD_ADDITION\n\n");
          }
          else if (STANDARD_SUBTRACTION == pep_operation)
          {
            printf("STANDARD_SUBTRACTION\n\n");
          }
          else if (STANDARD_DIVISION == pep_operation)
          {
            printf("STANDARD_DIVISION\n\n");
          }
          else if (STANDARD_REMINDER == pep_operation)
          {
            printf("STANDARD_REMINDER\n\n");
          }
          else if (STANDARD_MULTIPLICATION == pep_operation)
          {
            printf("STANDARD_MULTIPLICATION\n\n");
          }
          else
          {
            printf("Invalid Operation !!! Enter Valid Operation Code\n\n");
          }

        } while (pep_operation != 0);
      } // End Of STANDARD Case
        break;

      case PROGRAMMER:
      {
        int pep_operation;

        printf("Select Operation : \n\n");
        printf("0. For Exit\n");
        printf("1. AND\n");
        printf("2. OR\n");
        printf("3. XOR\n");
        printf("4. NOT\n");
        printf("5. LEFT SHIFT\n");
        printf("6. RIGHT SHIFT\n");

        scanf("%d", &pep_operation);

        printf("\n\n");

        if (PROGRAMMER_EXIT == pep_operation)
        {
          break;
        }
        else if (PROGRAMMER_AND == pep_operation)
        {
          printf("PROGRAMMER_AND\n\n");
        }
        else if (PROGRAMMER_OR == pep_operation)
        {
          printf("PROGRAMMER_OR\n\n");
        }
        else if (PROGRAMMER_XOR == pep_operation)
        {
          printf("PROGRAMMER_XOR\n\n");
        }
        else if (PROGRAMMER_NOT == pep_operation)
        {
          printf("PROGRAMMER_NOT\n\n");
        }
        else if (PROGRAMMER_LEFT_SHIFT == pep_operation)
        {
          printf("PROGRAMMER_LEFT_SHIFT\n\n");
        }
        else if (PROGRAMMER_RIGHT_SHIFT == pep_operation)
        {
          printf("PROGRAMMER_RIGHT_SHIFT\n\n");
        }
        else
        {
          printf("Invalid Operation !!! Enter Valid Operation Code\n\n");
        }
      }
        break;

      case SCIENTIFIC:
      {
        int pep_scientific;

        printf("Select Operation : \n\n");
        printf("0. For Exit Mode\n");
        printf("1. SIN\n");
        printf("2. COS\n");
        printf("3. SEC\n");
        printf("4. COSEC\n");
        printf("5. TAN\n");
        printf("6. COT\n");

        scanf("%d", &pep_scientific);

        printf("\n\n");

        if (SCIENTIFIC_EXIT == pep_scientific)
        {
          break;
        }
        else if (SCIENTIFIC_SIN == pep_scientific)
        {
          printf("SCIENTIFIC_SIN\n\n");
        }
        else if (SCIENTIFIC_COS == pep_scientific)
        {
          printf("SCIENTIFIC_COS\n\n");
        }
        else if (SCIENTIFIC_SEC == pep_scientific)
        {
          printf("SCIENTIFIC_SEC\n\n");
        }
        else if (SCIENTIFIC_COSEC == pep_scientific)
        {
          printf("SCIENTIFIC_COSEC\n\n");
        }
        else if (SCIENTIFIC_TAN == pep_scientific)
        {
          printf("SCIENTIFIC_TAN\n\n");
        }
        else if (SCIENTIFIC_COT == pep_scientific)
        {
          printf("SCIENTIFIC_COT\n\n");
        }
        else
        {
          printf("Invalid Operation !!! Enter Valid Operation Code\n\n");
        }

      }
        break;

      default:
      {
        printf("Invalid Input !!! Enter Valid Mode Code\n\n");
      }
        break;
        
    }

  } while (pep_mode != 0);

  

  return 0;
}