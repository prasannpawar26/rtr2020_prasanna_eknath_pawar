#include <stdio.h>

int main(void)
{
  // Function Declarations
  int SumOfTwoIntegers(int, int);

  // Variable Declarations
  int pep_sum;
  
  pep_sum = SumOfTwoIntegers(75, 25);

  printf("\n\n");
  printf("SumOfTwoIntegers 75 And 25 Is %d\n\n", pep_sum);
  printf("SumOfTwoIntegers 175 And 125 Is %d\n\n", SumOfTwoIntegers(175, 125));

  SumOfTwoIntegers(275, 225);

  return 0;
}

int SumOfTwoIntegers(int pep_a, int pep_b)
{
  // Variable
  int pep_sum;
  // Code

  pep_sum = pep_a - pep_b;

  return pep_sum;
}
