#include <stdio.h>

int main(void)
{
  // varible declrations
  int pep_num;

  // Code
  printf("\n\n");
  printf("Enter Num : ");
  scanf("%d", &pep_num);

  printf("\n\n");

  if (pep_num < 0)
  {
    printf("Num = %d Is Less Than 0 (NEGATIVE) !!! \n\n", pep_num);
  }
  else if ((pep_num > 0) && (pep_num <= 100))
  {
     printf("Num = %d Is between 0 And 100 !!! \n\n", pep_num);
  }
  else if ((pep_num > 100) && (pep_num <= 200))
  {
    printf("Num = %d Is between 100 And 200 !!! \n\n", pep_num);
  }
  else if ((pep_num > 200) && (pep_num <= 300))
  {
    printf("Num = %d Is between 2000 And 300 !!! \n\n", pep_num);
  }
  else if ((pep_num > 300) && (pep_num <= 400))
  {
    printf("Num = %d Is between 300 And 400 !!! \n\n", pep_num);
  }
  else if ((pep_num > 400) && (pep_num <= 500))
  {
    printf("Num = %d Is between 400 And 500 !!! \n\n", pep_num);
  }
  else if(pep_num > 500)
  {
    printf("Num = %d Is Greater Than 500 !!!\n\n", pep_num);
  }

  return 0;
}
