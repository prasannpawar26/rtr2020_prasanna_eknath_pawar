#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i;

  // Code
  printf("\n\n");

  printf("Printing Digits 10 To 1 : \n\n");

  pep_i = 10;
  while (pep_i >= 1)
  {
    printf("\t%d\n", pep_i);
    pep_i--;
  }
  
  printf("\n\n");

  return 0;
}
