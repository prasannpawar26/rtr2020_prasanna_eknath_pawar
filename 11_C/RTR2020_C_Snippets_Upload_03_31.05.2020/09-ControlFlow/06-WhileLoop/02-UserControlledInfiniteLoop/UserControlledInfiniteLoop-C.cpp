#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  char pep_option, pep_ch = '\0';

  // Code
  printf("\n\n");
  printf("Once Infinite Loop Begins, Enters 'Q' or 'q' To  Quit The Infinite For Loop : \n\n");
  printf("Enter 'Y' or 'y' To Initiate User Controlled Infinite Loop :");
  printf("\n\n");

  pep_option = getch();
  if (pep_option == 'Y' || pep_option == 'y')
  {
    while (1) // Infinte Loop
    {
      printf("In Loop...\n");
      pep_ch = getch();

      if (pep_ch == 'Q' || pep_ch == 'q')
      {
        break; // User Controlled Exitting From Infinite Loop
      }
    }

    printf("\n\n");
    printf("EXITTING USER CONTROLLED INFINTE LOOP...");
    printf("\n\n");
  }
  else
  {
    printf("You Must Press 'Y' or 'y' To Initiate The User Controlled Infinite Loop...Please try Again...\n\n");
  }

  return 0;
}
