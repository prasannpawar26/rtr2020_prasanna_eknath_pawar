#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j, pep_c;

  // Code
  printf("\n\n");

  pep_i = 0;
  while (pep_i < 64)
  {
    pep_j = 0;
    while (pep_j < 64)
    {
      pep_c = ((pep_i & 0x8) == 0) ^ ((pep_j & 0x8) == 0);

      if (pep_c == 0)
      {
        printf(" ");
      }

      if (pep_c == 1)
      {
        printf("* ");
      }

      pep_j++;
    }

    printf("\n\n");
    pep_i++;

  }
  return 0;
}
