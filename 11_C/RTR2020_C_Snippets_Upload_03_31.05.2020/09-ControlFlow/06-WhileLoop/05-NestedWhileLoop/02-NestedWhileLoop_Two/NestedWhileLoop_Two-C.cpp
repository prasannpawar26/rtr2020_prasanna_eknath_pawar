#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j, pep_k;

  // Code
  printf("\n\n");

  pep_i = 1;
  while (pep_i <= 5)
  {
    printf("i = %d\n", pep_i);
    printf("-------\n");

    pep_j = 1;
    while (pep_j <=3)
    {
      printf("\t j = %d\n", pep_j);
      printf("\t-------\n");

      pep_k = 1;
      while (pep_k <= 2)
      {
        printf("\t\t k = %d\n", pep_k);
        pep_k++;
      }

      pep_j++;
      printf("\n\n");
    }

    pep_i++;
    printf("\n\n");
  }

  return 0;
}
