#include <conio.h>
#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j;

  // Code
  printf("\n\n");

  pep_i = 1;
  do
  {
    printf(" i = %d\n", pep_i);
    printf("-------\n\n");

    pep_j = 1;
    do
    {
      printf("\t j = %d\n", pep_j);
      pep_j++;
    } while (pep_j <= 5);

    pep_i++;
  } while (pep_i <= 10);
  printf("\n\n");

  return 0;
}
