#include <conio.h>
#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j, pep_k;

  printf("\n\n");

  pep_i = 1;
  do
  {
    printf("i = %d\n", pep_i);
    printf("-----\n\n");

    pep_j = 1;
    do
    {
      printf("\tj = %d\n", pep_j);
      printf("\t-----\n\n");

      pep_k = 1;
      do
      {
        printf("\t\tk = %d\n", pep_k);
        pep_k++;
      } while (pep_k <= 2);
      printf("\n\n");
      pep_j++;
    } while (pep_j <= 5);
    printf("\n\n");

    pep_i++;
  } while (pep_i <= 5);

  return 0;
}
