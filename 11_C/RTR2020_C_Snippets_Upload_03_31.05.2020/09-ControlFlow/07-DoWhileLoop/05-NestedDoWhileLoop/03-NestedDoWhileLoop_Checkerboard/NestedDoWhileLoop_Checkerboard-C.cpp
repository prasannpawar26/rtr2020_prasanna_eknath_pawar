#include <conio.h>
#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j, pep_c;

  // Code
  printf("\n\n");

  pep_i = 0;
  do
  {

    pep_j = 0;
    do
    {
      pep_c = ((pep_i & 0x8) == 0) ^ ((pep_j & 0x8) == 0);

      if (pep_c == 0)
      {
        printf(" ");
      }

      if (pep_c == 1)
      {
        printf("* ");
      }

      pep_j++;
    } while (pep_j < 64);
    printf("\n\n");

    pep_i++;
  } while (pep_i < 64);

  printf("\n\n");

  return 0;
}
