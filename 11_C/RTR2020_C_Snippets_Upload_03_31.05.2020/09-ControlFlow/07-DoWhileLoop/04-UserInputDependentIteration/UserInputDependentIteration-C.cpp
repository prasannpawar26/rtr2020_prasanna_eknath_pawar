#include <conio.h>
#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i_num, pep_num, pep_i;

  // Code
  printf("\n\n");

  printf("Enter An Integer Value From Which Iteration To Start :  ");
  scanf("%d", &pep_i_num);

  printf("How May Digits To Print From %d ? ", pep_i_num);
  scanf("%d", &pep_num);

  printf("Printing Digits %d To %d : \n\n", pep_i_num , (pep_i_num + pep_num));

  pep_i = pep_i_num;
  do
  {
    printf("\t%d\n", pep_i);
    pep_i++;
  } while (pep_i <= (pep_i_num+ pep_num));

  printf("\n\n");

  return 0;
}
