#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j;

  // Code
  printf("\n\n");

  printf("Printing Digits 10 To 1 And 100 To 10 : \n\n");

  pep_i = 10;
  pep_j = 100;

  do
  {
    printf("\t %d \t %d\n", pep_i, pep_j);

    pep_i--;
    pep_j = pep_j - 10;

  } while (pep_i >= 1 , pep_j >= 10);

  return 0;
}
