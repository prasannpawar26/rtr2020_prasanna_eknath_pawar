#include <conio.h>
#include <stdio.h>

int main(void) {
  // Variable Declarations
  float pep_f;
  float pep_f_num = 4.5f;

  printf("\n\n");
  printf("Printing Numbers %f To %f : \n\n", pep_f_num, (pep_f_num * 10.0f));

  pep_f = pep_f_num;
  do
  {
    printf("\t%f\n", pep_f);
    pep_f = pep_f + pep_f_num;
  } while (pep_f <= (pep_f_num * 10.0f));

  printf("\n\n");

  return 0;
}
