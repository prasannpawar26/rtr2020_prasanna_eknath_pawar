#include <conio.h>
#include <stdio.h>

int main(void) {
  // Variable Declarations
  char pep_option, pep_ch = '\0';

  // Code
  printf("\n\n");

  printf(
      "Once The Infinte Loop Begins, Enter 'Q' or 'q' To Quit The Infinte For "
      "Loop : \n\n");

  do
  {
    do
    {
      printf("In Loop...\n");
      pep_ch = getch();
    } while (pep_ch != 'Q' && pep_ch != 'q');

    printf("\n\n");
    printf("EXITTING USER CONTROLLED INFINTE LOOP...");

    printf("\n\n");
    printf("DO YOU WANT TO BEGIN USER CONTROLLED INFINTE LOOP AGAIN...(Y/y Yes, Any Other Key - No) : ");
    pep_option = getch();
  } while (pep_option == 'Y' || pep_option == 'y');

  return 0;
}
