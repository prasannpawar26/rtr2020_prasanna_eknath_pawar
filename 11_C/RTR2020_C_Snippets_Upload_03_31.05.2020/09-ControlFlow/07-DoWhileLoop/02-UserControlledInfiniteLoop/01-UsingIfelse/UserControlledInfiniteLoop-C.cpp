#include <conio.h>
#include <stdio.h>

int main(void) {
  // Variable Declarations
  char pep_option, pep_ch = '\0';

  // Code
  printf("\n\n");

  printf(
      "Once The Infinte Loop Begins, Enter 'Q' or 'q' To Quit The Infinte For "
      "Loop : \n\n");
  printf("Enter 'Y' or 'y' To Infinte User Controlled Infinte Loop : ");
  printf("\n\n");

  pep_option = getch();
  if (pep_option == 'Y' || pep_option == 'y')
  {
    do
    {
      printf("In Loop...\n");
      pep_ch = getche();

      if (pep_ch == 'Q' || pep_ch == 'q')
      {
        break;
      }
    } while (1);

    printf("\n\n");
    printf("EXITTING USER CONTROLLED INFINTE LOOP...");
    printf("\n\n");
  }
  else
  {
    printf("You Must Press 'Y' or 'y' To Initiate The User Controlled Infinite Loop...Please Try Again...\n\n");
  }

  return 0;
}
