#include <stdio.h>

int main(void) {
  // Variable Declarations
  int pep_i, pep_j;

  // Code
  printf("\n\n");

  printf("Outer Loop Print Odd Number From 0 To 100: \n\n");
  printf("Inner Loop Print Even Number From 0 To 100 For Every Odd Number Printed By Outer Loop : \n\n");

  for (pep_i = 0; pep_i <= 10; pep_i++)
  {
    if (pep_i % 2 != 0)
    {
      printf("i = %d\n", pep_i);
      printf("-------\n");

      for (pep_j = 0; pep_j <= 10; pep_j++)
      {
        if (pep_j % 2 == 0)
        {
          printf("\t j = %d\n", pep_j);
        } 
        else
        {
          continue;
        }
      }
      printf("\n\n");
    }
    else
    {
      continue;
    }
  }

  printf("\n\n");

  return 0;
}
