#include <stdio.h>

int main(void) {
  // Variable Declarations
  int pep_i;

  // Code
  printf("\n\n");

  printf("Print Even Number From 0 To 100: \n\n");

  for (pep_i = 0; pep_i <= 100; pep_i++)
  {
    if (pep_i % 2 != 0)
    {
      continue;
    }
    else
    {
      printf("\t%d\n", pep_i);
    }
  }

  printf("\n\n");

  return 0;
}
