#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  int pep_i_num, pep_num, pep_i;

  // Code
  printf("\n\n");

  printf("Enter An Integer Value From Which Iteration Must Begin : ");
  scanf("%d", &pep_i_num);

  printf("How Many Digits Do You Want To Print From %d Onwords ? : ", pep_i_num);
  scanf("%d", &pep_num);

  printf("Printing Digits %d To %d : \n\n", pep_i_num, (pep_i_num + pep_num));

  for (pep_i = pep_i_num; pep_i <= (pep_i_num + pep_num); pep_i++)
  {
    printf("\t%d\n", pep_i);
  }

  printf("\n\n");

  return 0;
}
