#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  float pep_f;
  float pep_f_num = 12.7f; 

  // Code
  printf("\n\n");

  printf("Printing Numbers %f To %f : \n\n", pep_f_num, (pep_f_num * 10.0f));

  for (pep_f = pep_f_num; pep_f <= (pep_f_num * 10.0f); pep_f = pep_f + pep_f_num)
  {
    printf("\t %f\n", pep_f);
  }

  printf("\n\n");

  return 0;
}
