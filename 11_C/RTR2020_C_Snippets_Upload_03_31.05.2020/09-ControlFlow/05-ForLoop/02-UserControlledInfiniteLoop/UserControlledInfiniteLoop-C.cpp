#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  char pep_option, pep_ch = '\0';

  // Code
  printf("\n\n");
  printf("Once The Infinte Loop Begins , Enter 'Q' Or 'q'  To Quit The Infinite For Loop : \n\n");
  printf("Enter 'Y' or 'y' To Initiate User Controlled Infinite Loop : ");
  printf("\n\n");

  pep_option = getch();
  if (pep_option == 'Y' || pep_option == 'y')
  {
    for (;;) // Infinite Loop
    {
      printf("In Loop...\n");
      pep_ch = getch();

      if (pep_ch == 'Q' || pep_ch == 'q')
      {
        break; // User Controlled Exitting From Infinite Loop
      }
    }
  }

  printf("\n\n");
  printf("EXITTING USER CONTROLLED INFINTE LOOP...");
  printf("\n\n");

  return 0;
}
