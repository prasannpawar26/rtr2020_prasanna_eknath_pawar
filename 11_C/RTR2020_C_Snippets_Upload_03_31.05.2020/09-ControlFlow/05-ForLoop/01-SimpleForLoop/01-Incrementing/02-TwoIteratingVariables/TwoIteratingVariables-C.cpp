#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j;

  // Code
  printf("\n\n");

  printf("Printing Digits 1 To 10 And 10 To 100 : \n\n");

  for(pep_i = 1, pep_j = 10; pep_i <= 10, pep_j <=100; pep_i++, pep_j = pep_j + 10)
  {
    printf("\t %d \t %d\n", pep_i, pep_j);
  }

  printf("\n\n");

  return 0;
}
