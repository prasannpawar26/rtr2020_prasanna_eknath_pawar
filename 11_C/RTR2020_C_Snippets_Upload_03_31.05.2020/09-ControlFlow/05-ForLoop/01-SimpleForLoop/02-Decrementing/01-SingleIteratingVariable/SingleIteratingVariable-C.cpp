#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i;

  // Code
  printf("\n\n");

  printf("Printing Digits 1 To 10 : \n\n");

  for(pep_i = 10; pep_i >= 1; pep_i--)
  {
    printf("\t%d\n", pep_i);
  }

  printf("\n\n");

  return 0;
}
