#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j;

  // Code
  printf("\n\n");

  for (pep_i = 1; pep_i <= 5; pep_i++)
  {
    printf("pep_i = %d\n", pep_i);
    printf("------------\n\n");

    for (pep_j = 1; pep_j <=3; pep_j++)
    {
      printf("\t pep_j = %d\n", pep_j);
    }

    printf("\n\n");
  }

  return 0;
}
