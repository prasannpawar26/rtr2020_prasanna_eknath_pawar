#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j, pep_c;

  // Code
  printf("\n\n");

  for (pep_i = 0; pep_i < 64; pep_i++)
  {
    for (pep_j = 0; pep_j < 64; pep_j++)
    {
      pep_c = ((pep_i & 0x8) == 0) ^ ((pep_j & 0x8) == 0);

      if (pep_c == 0)
      {
        printf(" ");
      }

      if (pep_c == 1)
      {
        printf("* ");
      }
    } // end of inner for-loop

    printf("\n\n");

  } // end of outer for-loop

  return 0;
}
