#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_i, pep_j, pep_k;

  // Code
  printf("\n");

  for (pep_i = 1; pep_i <= 3; pep_i++)
  {
    printf("pep_i = %d\n", pep_i);
    printf("------------\n");

    for (pep_j = 1; pep_j <=3; pep_j++)
    {
      printf("\t pep_j = %d\n", pep_j);
      printf("\t------------\n");

      for (pep_k = 1; pep_k <=2; pep_k++)
      {
        printf("\t\t pep_k = %d\n", pep_k);
      }

      printf("\n");
    }

    printf("\n\n");
  }

  return 0;
}
