#include <stdio.h>
#include <conio.h>

int main(void) {
  // Variable Declarations
  int pep_i;
  char pep_ch;

  // Code
  printf("\n\n");

  printf("Print Number From 1 To 100 For Every user Input. Exitting The Loop When User Enters 'Q' and 'q': \n\n");

  for (pep_i = 0; pep_i <= 100; pep_i++)
  {
    printf("\t%d\n", pep_i);
    pep_ch = getch();

    if (pep_ch == 'Q' || pep_ch == 'q')
    {
      break;
    }
  }

  printf("\n\n");
  printf("EXITTING LOOP...");
  printf("\n\n");

  return 0;
}
