#include <stdio.h>
#include <conio.h>

int main(void) {
  // Variable Declarations
  int pep_i;
  int pep_j;

  printf("\n\n");

  for (pep_i = 1; pep_i <= 20; pep_i++)
  {
    for (pep_j = 1; pep_j <= 20; pep_j++)
    {
      if (pep_j > pep_i)
      {
        break;
      }
      else
      {
        printf("* ");
      }
    }

    printf("\n");
  }

  printf("\n\n");

  return 0;
}
