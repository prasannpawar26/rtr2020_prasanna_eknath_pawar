#include <stdio.h>

int main(void)
{
  // varible declrations
  int pep_a, pep_b, pep_p;

  // Code
  pep_a = 87;
  pep_b = 333;
  pep_p = 333;

  printf("\n\n");

  if (pep_a < pep_b)
  {
    printf("Entering First if-block...\n\n");
    printf("A Is Less Than B !!!\n\n");
  }
  else
  {
    printf("Entering First else-block...\n\n");
    printf("A Is Not Less Than B !!!\n\n");
  }
  printf("First if-else Pair Done !!!\n\n");

  // Second if-else Pair
  if (pep_a != pep_p)
  {
    printf("Entering Second if-block...\n\n");
    printf("B Is NOT Equal To P !!!\n\n");
  }
  else
  {
    printf("Entering Second else-block...\n\n");
    printf("B Is Equal To P !!!\n\n");
  }
  printf("Second if-else Pair Done !!!\n\n");

  return 0;
}
