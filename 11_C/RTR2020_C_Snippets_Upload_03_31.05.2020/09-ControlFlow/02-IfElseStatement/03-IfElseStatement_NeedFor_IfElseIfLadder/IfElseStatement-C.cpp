#include <stdio.h>

int main(void)
{
  // varible declrations
  int pep_num;

  // Code
  printf("\n\n");
  printf("Enter Num : ");
  scanf("%d", &pep_num);

  printf("\n\n");

  if (pep_num < 0) // 'if' - 01
  {
    printf("Num = %d Is Less Than 0 (NEGATIVE) !!! \n\n", pep_num);
  }
  else // 'else' - 01
  {
    if ((pep_num > 0) && (pep_num <= 100)) // 'if' - 02
    {
      printf("Num = %d Is between 0 And 100 !!! \n\n", pep_num);
    }
    else // 'else' - 02
    {
      if ((pep_num > 100) && (pep_num <= 200))  // 'if' - 03
      {
        printf("Num = %d Is between 100 And 200 !!! \n\n", pep_num);
      }
      else  // 'else' - 03
      {
        if ((pep_num > 200) && (pep_num <= 300))  // 'if' - 04
        {
          printf("Num = %d Is between 2000 And 300 !!! \n\n", pep_num);
        }
        else  // 'else' - 04
        {
          if ((pep_num > 300) && (pep_num <= 400))  // 'if' - 05
          {
            printf("Num = %d Is between 300 And 400 !!! \n\n", pep_num);
          }
          else  // 'else' - 05
          {
            if ((pep_num > 400) && (pep_num <= 500))  // 'if' - 06
            {
              printf("Num = %d Is between 400 And 500 !!! \n\n", pep_num);
            }
            else  // 'else' - 06
            {
              printf("Num = %d Is Greater Than 500 !!!\n\n", pep_num);
            } // end of 'else' - 06
          } // end of 'else' - 05
        } // end of 'else' - 04
      } // end of 'else' - 03
    } // end of 'else' - 02
  } // end of 'else' - 01

  return 0;
}
