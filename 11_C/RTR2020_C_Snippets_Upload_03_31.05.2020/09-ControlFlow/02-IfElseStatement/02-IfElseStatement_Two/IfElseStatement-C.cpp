#include <stdio.h>

int main(void)
{
  // varible declrations
  int pep_age;

  // Code
  printf("\n\n");
  printf("Enter Age : ");
  scanf("%d", &pep_age);

  printf("\n\n");

  if (pep_age >= 18)
  {
    printf("Entering if-block...\n\n");
    printf("You Are Eligible For Voting !!!\n\n");
  }
  else
  {
    printf("Entering else-block...\n\n");
    printf("You Are NOT Eligible For Voting !!!\n\n");
  }
  printf("Bye !!!\n\n");

  return 0;
}
