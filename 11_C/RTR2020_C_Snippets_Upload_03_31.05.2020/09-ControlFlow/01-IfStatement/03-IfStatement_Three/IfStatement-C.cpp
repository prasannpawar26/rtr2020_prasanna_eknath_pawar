#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_num;

  // Code
  printf("\n\n");
  printf("Enter Value For 'num' : ");
  scanf("%d", &pep_num);

  if (pep_num < 0)
  {
    printf("Num = %d Is Less Than 0 (NEGATIVE).\n\n", pep_num);
  }

  if ((pep_num > 0) && (pep_num <= 100))
  {
    printf("Num = %d Is Between 0 And 100.\n\n", pep_num);
  }

  if ((pep_num > 100) && (pep_num <= 200))
  {
    printf("Num = %d Is Between 100 And 200.\n\n", pep_num);
  }

  if ((pep_num > 200) && (pep_num <= 300))
  {
    printf("Num = %d Is Between 200 And 300.\n\n", pep_num);
  }

  if ((pep_num > 300) && (pep_num <= 400))
  {
    printf("Num = %d Is Between 300 And 400.\n\n", pep_num);
  }

  if ((pep_num > 400) && (pep_num <= 500))
  {
    printf("Num = %d Is Between 400 And 500.\n\n", pep_num);
  }

  if (pep_num > 500)
  {
    printf("Num = %d Is Greater 500.\n\n", pep_num);
  }

  return 0;
}
