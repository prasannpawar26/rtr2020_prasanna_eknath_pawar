#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_a, pep_b, pep_p;

  // Code
  pep_a = 91;
  pep_b = 310;
  pep_p = 310;

  printf("\n\n");

  if (pep_a < pep_b) {
    printf("A Is Less Than B !!!\n\n");
  }

  if (pep_b != pep_p) {
    printf("B Is NOT Equal To P !!!\n\n");
  }

  printf("Both Comparisons Have Been Done !!!\n\n");

  return 0;
}
