#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_a;

  // Code
  printf("\n\n");
  
  pep_a = 5;
  if (pep_a) // Non-Zero Positive Value
  {
    printf("if-block 1 : 'A' Exist And Has Value = %d !!!\n\n", pep_a);
  }

  pep_a = -5;
  if (pep_a)  // Non-Zero Negative Value
  {
    printf("if-block 2 : 'A' Exist And Has Value = %d !!!\n\n", pep_a);
  }

  pep_a = 0;
  if (pep_a)  // Zero Value
  {
    printf("if-block 3 : 'A' Exist And Has Value = %d !!!\n\n", pep_a);
  }

  printf("All Three if-statements Are Done !!! \n\n");

  return 0;
}
