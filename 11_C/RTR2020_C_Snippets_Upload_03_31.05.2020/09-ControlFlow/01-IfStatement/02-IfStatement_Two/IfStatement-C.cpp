#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_age;

  // Code
  printf("\n\n");
  printf("Enter Age : ");
  scanf("%d", &pep_age);

  if (pep_age >= 18)
  {
    printf("You Are Eligible For Voting !!!\n\n");
  }

  return 0;
}
