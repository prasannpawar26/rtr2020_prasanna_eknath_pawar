#include <stdio.h>

int main(void)
{
  // varible declrations
  int pep_num_month;

  // Code
  printf("\n\n");

  printf("Enter Number Of Month ( 1 To 12 ) :");
  scanf("%d", &pep_num_month);

  printf("\n\n");

  switch (pep_num_month)
  {
    case 1: // like 'if'
    {
      printf("Month Number %d Is JANUARY !!!\n\n", pep_num_month);
    } break;

    case 2: // like 'else if'
    {
      printf("Month Number %d Is FEBRUARY !!!\n\n", pep_num_month);
    } break;

    case 3: // like 'else if'
    {
      printf("Month Number %d Is MARCH !!!\n\n", pep_num_month);
    } break;

    case 4: // like 'else if'
    {
      printf("Month Number %d Is APRIL !!!\n\n", pep_num_month);
    } break;

    case 5: // like 'else if'
    {
      printf("Month Number %d Is MAY !!!\n\n", pep_num_month);
    } break;

    case 6: // like 'else if'
    {
      printf("Month Number %d Is JUNE !!!\n\n", pep_num_month);
    } break;

    case 7: // like 'else if'
    {
      printf("Month Number %d Is JULY !!!\n\n", pep_num_month);
    } break;

    case 8: // like 'else if'
    {
      printf("Month Number %d Is AUGUST !!!\n\n", pep_num_month);
    } break;

    case 9: // like 'else if'
    {
      printf("Month Number %d Is SEPTEMBER !!!\n\n", pep_num_month);
    } break;

    case 10: // like 'else if'
    {
      printf("Month Number %d Is OCTOBER !!!\n\n", pep_num_month);
    } break;

    case 11: // like 'else if'
    {
      printf("Month Number %d Is NOVEMBER !!!\n\n", pep_num_month);
    } break;

    case 12: // like 'else if'
    {
      printf("Month Number %d Is DECEMBER !!!\n\n", pep_num_month);
    } break;

    default:  // Like Ending Optional 'else'
    {
      printf("Invalid Month Number %d Entered !!! Please try Again...\n\n", pep_num_month);
    } break;

    printf("Switch Case Block Completed !!!\n");

  }

  return 0;
}
