#include <stdio.h>

int main(void)
{
  // varible declrations
  int pep_num_month;

  // Code
  printf("\n\n");

  printf("Enter Number Of Month ( 1 To 12 ) :");
  scanf("%d", &pep_num_month);

  printf("\n\n");

  if (1 == pep_num_month) // like 'case 1'
  {
    printf("Month Number %d Is JANUARY !!!\n\n", pep_num_month);
  }
  else if (2 == pep_num_month) // like 'case 2'
  {
    printf("Month Number %d Is FEBRUARY !!!\n\n", pep_num_month);
  } else if (3 == pep_num_month) // like 'case 3'
  {
    printf("Month Number %d Is MARCH !!!\n\n", pep_num_month);
  } else if (4 == pep_num_month) // like 'case 4'
  {
    printf("Month Number %d Is APRIL !!!\n\n", pep_num_month);
  } else if (5 == pep_num_month) // like 'case 5'
  {
    printf("Month Number %d Is MAY !!!\n\n", pep_num_month);
  } else if (6 == pep_num_month) // like 'case 6'
  {
    printf("Month Number %d Is JUNE !!!\n\n", pep_num_month);
  } else if (7 == pep_num_month) // like 'case 7'
  {
    printf("Month Number %d Is JULY !!!\n\n", pep_num_month);
  } else if (8 == pep_num_month) // like 'case 8'
  {
    printf("Month Number %d Is AUGUST !!!\n\n", pep_num_month);
  } else if (9 == pep_num_month) // like 'case 9'
  {
    printf("Month Number %d Is SEPTEMBER !!!\n\n", pep_num_month);
  } else if (10 == pep_num_month) // like 'case 10'
  {
    printf("Month Number %d Is OCTOBER !!!\n\n", pep_num_month);
  } else if (11 == pep_num_month) // like 'case 11'
  {
    printf("Month Number %d Is NOVEMBER !!!\n\n", pep_num_month);
  } else if (12 == pep_num_month) // like 'case 12'
  {
    printf("Month Number %d Is DECEMBER !!!\n\n", pep_num_month);
  }
  else // like 'default'
  {
    printf("Invalid Month Number %d Entered !!! Please Try Again \n\n", pep_num_month);
  }

  printf("If-Else If -Else Ladder Complete !!! \n\n");

  return 0;
}
