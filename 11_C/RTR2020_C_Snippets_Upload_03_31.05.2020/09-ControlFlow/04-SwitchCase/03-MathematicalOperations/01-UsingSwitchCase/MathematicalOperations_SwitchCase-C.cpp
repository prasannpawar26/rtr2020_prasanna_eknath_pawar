#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Delcarations
  int pep_a, pep_b;
  int pep_result;

  char pep_option, pep_option_division;

  // Code
  printf("\n\n");
  printf("Enter Value For 'A' : ");
  scanf("%d", &pep_a);
  printf("\n\n");
  printf("Enter Value For 'B' : ");
  scanf("%d", &pep_b);

  printf("Enter Option In Character : \n\n");
  printf("'A' Or 'a' For Addition : \n");
  printf("'S' Or 's' For Subtraction : \n");
  printf("'M' Or 'm' For Mutiplication : \n");
  printf("'D' Or 'd' For Division : \n");

  printf("Enter Option : ");
  pep_option = getch();

  printf("\n\n");

  switch (pep_option)
  {
    case 'A':
    case 'a':
    {
      pep_result = pep_a + pep_b;
      printf("Addition Of A = %d And B = %d Gives Result %d !!!\n\n", pep_a,
             pep_b, pep_result);
    } break;

    case 'S':
    case 's':
    {
      pep_result = pep_a - pep_b;
      printf("Subraction Of A = %d And B = %d Gives Result %d !!!\n\n", pep_a,
             pep_b, pep_result);
    } break;

    case 'M':
    case 'm':
    {
      pep_result = pep_a * pep_b;
      printf("Multiplication Of A = %d And B = %d Gives Result %d !!!\n\n", pep_a,
             pep_b, pep_result);
    } break;

    case 'D':
    case 'd':
    {
      printf("Enter Option In Character : \n\n");
      printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
      printf("'R' or 'r' or '%%' For Reminder Upon Division : \n");

      printf("Enter Option :");
      pep_option_division = getch();

      printf("\n\n");

      switch (pep_option_division)
      {
        case 'Q':
        case 'q':
        case '/':
        {
          if (pep_a >= pep_b)
          {
            pep_result = pep_a / pep_b;
            printf(
                "Division Of A = %d And B = %d Gives Quotient %d !!!\n\n",
                pep_a, pep_b, pep_result);
          }
          else
          {
            pep_result = pep_b / pep_a;
            printf("Division Of B = %d And A = %d Gives Quotient %d !!!\n\n",
                   pep_b, pep_a, pep_result);
          }

        } break;

        case 'R':
        case 'r':
        case '%':
        {
          if (pep_a >= pep_b)
          {
            pep_result = pep_a % pep_b;
            printf("Division Of A = %d And B = %d Gives Reminder %d !!!\n\n",
                   pep_a, pep_b, pep_result);
          }
          else
          {
            pep_result = pep_b % pep_a;
            printf("Division Of B = %d And A = %d Gives Reminder %d !!!\n\n",
                   pep_b, pep_a, pep_result);
          }

        } break;

        default:
        {
          printf("Invalid Character %c Entered !!! Please Try Again... \n\n", pep_option_division);
        } break;
      }
    } break;

    default:
    {
      printf("Invalid Character %c Entered !!! Please Try Again... \n\n",
             pep_option);
    } break;
  }
  printf("Switch Case Block Complete !!!\n\n");

  return 0;
}
