#include <stdio.h>
#include <conio.h>

int main(void)
{
  // Variable Declarations
  int pep_a, pep_b;
  int pep_result;

  char pep_option, pep_option_division;

  // Code
  printf("\n\n");
  printf("Enter Value For 'A' : ");
  scanf("%d", &pep_a);
  printf("\n\n");
  printf("Enter Value For 'B' : ");
  scanf("%d", &pep_b);

  printf("Enter Option In Character : \n\n");
  printf("'A' or 'a' For Addition : \n");
  printf("'S' or 's' For Subtraction : \n");
  printf("'M' or 'm' For Mutiplication : \n");
  printf("'D' or 'd' For Division : \n");

  printf("Enter Option In Character : ");
  pep_option = getch();

  printf("\n\n");

  if (pep_option == 'A' || pep_option == 'a')
  {
    pep_result = pep_a + pep_b;
    printf("Addition Of A = %d And B = %d Gives Result = %d !!!\n\n", pep_a,
           pep_b, pep_result);
    
  }
  else if (pep_option == 'S' || pep_option == 's')
  {
    pep_result = pep_a - pep_b;
    printf("Subtraction Of A = %d And B = %d Gives Result = %d !!!\n\n", pep_a,
           pep_b, pep_result);
  }
  else if (pep_option == 'M' || pep_option == 'm')
  {
    pep_result = pep_a * pep_b;
    printf("Mutiplication Of A = %d And B = %d Gives Result = %d !!!\n\n", pep_a,
           pep_b, pep_result);
  }
  else if (pep_option == 'D' || pep_option == 'd')
  {
    printf("Enter Option In Character : \n\n");
    printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
    printf("'R' or 'r' or '%%' For Reminder Upon Division : \n");

    printf("Enter Option : ");
    pep_option_division = getch();

    printf("\n\n");

    if (pep_option_division == 'Q' || pep_option_division == 'q' ||
        pep_option_division == '/')
    {
      if (pep_a >= pep_b)
      {
        pep_result = pep_a / pep_b;
        printf("Division Of A = %d And B = %d Gives Quotient = %d !!!\n\n",
               pep_a, pep_b, pep_result);
      }
      else
      {
        pep_result = pep_a / pep_b;
        printf("Division Of B = %d And A = %d Gives Quotient = %d !!!\n\n",
               pep_b, pep_a, pep_result);
      }
    } else if (pep_option_division == 'R' || pep_option_division == 'r' ||
               pep_option_division == '%')
    {
      if (pep_a >= pep_b)
      {
        pep_result = pep_a % pep_b;
        printf("Division Of A = %d And B = %d Gives Reminder = %d !!!\n\n",
               pep_a, pep_b, pep_result);
      }
      else
      {
        pep_result = pep_a % pep_b;
        printf("Division Of B = %d And A = %d Gives Reminder = %d !!!\n\n",
               pep_b, pep_a, pep_result);
      }
    }
    else
    {
      printf(
          "Invalid Character %c Entered For Division !!! Please Try Again "
          "...\n\n",
          pep_option_division);
    }
  }
  else
  {
    printf(
        "Invalid Character %c Entered For Division !!! Please Try Again "
        "...\n\n",
        pep_option);
  }

  printf("If Else If Ladder Complete !!!\n");

  return 0;
}
