#include <stdio.h>
#include <conio.h>

// ASCII Values For 'A' to 'Z' => 65 To 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING 90

// ASCII Values For 'A' to 'Z' => 97 To 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING 122

// ASCII Values For '0' To '9' => 48 To 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main(void)
{
  // Variable Declarations
  char pep_ch;
  char pep_ch_value;

  // Code
  printf("\n\n");

  printf("Enter Character : ");

  pep_ch = getch();

  printf("\n\n");

  switch (pep_ch)
  {
    // Fall Throught Condition
    case 'A':
    case 'a':
    case 'E':
    case 'e':
    case 'I':
    case 'i':
    case 'O':
    case 'o':
    case 'U':
    case 'u':
    {
      printf("Character \'%c\' Entered By You, Is A VOWEL CHARCTER From The English Alphabet !!!\n\n", pep_ch);
    }
      break;

   default:
   { //
     pep_ch_value = (int)pep_ch;

     if ((pep_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING &&
       pep_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDING) ||
       (pep_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING &&
         pep_ch_value <= CHAR_ALPHABET_LOWER_CASE_ENDING))
     {
       printf("Character \'%c\' Is CONSONANT !!!\n\n", pep_ch_value);
     }
     else if (pep_ch_value >= CHAR_DIGIT_BEGINNING && pep_ch_value <= CHAR_DIGIT_ENDING)
     {
       printf("Character \'%c\' Is DIGIT !!!\n\n", pep_ch_value);
     }
     else
     {
       printf("Character \'%c\' Is SPECIAL character !!!\n\n", pep_ch_value);
     }
   } break;
  }

  printf("Switch Case Block Complete !!!\n");
  return 0;
}
