#include <stdio.h>
#include <conio.h>

#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING 90

#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING 122

#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main(void)
{
  // Variable Declarations
  char pep_ch;
  int pep_ch_value;

  // Code
  printf("\n\n");

  printf("Enter Character :");
  pep_ch = getch();

  printf("\n\n");

  if ((pep_ch == 'A') || (pep_ch == 'E') || (pep_ch == 'I') ||
    (pep_ch == 'O') || (pep_ch == 'U') || (pep_ch == 'a') ||
    (pep_ch == 'e') || (pep_ch == 'i') || (pep_ch == 'o') || (pep_ch == 'u'))
  {
    printf("Character \'%c\' Is VOWEL !!!\n\n", pep_ch);
  }
  else
  {
    pep_ch_value = (int)pep_ch;
    if ((pep_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING &&
      pep_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDING) ||
      (pep_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING &&
        pep_ch_value <= CHAR_ALPHABET_LOWER_CASE_ENDING))
    {
      printf("Character \'%c\' Is CONSONANT !!!\n\n", pep_ch);
    }
    else if (pep_ch_value >= CHAR_DIGIT_BEGINNING &&
               pep_ch_value <= CHAR_DIGIT_ENDING)
    {
      printf("Character \'%c\' Is DIGIT !!!\n\n", pep_ch);
    }
    else
    {
      printf("Character \'%c\' Is SPECIAL Character !!!\n\n", pep_ch);
    }
  }
  return 0;
}
