#include <stdio.h>

int main(void) {
  // Variable Declarations
  int pep_a, pep_b;
  int pep_p, pep_q;

  char pep_ch_result_01, pep_ch_result_02;
  int pep_i_result_01, pep_i_result_02;

  // Code
  printf("\n\n");

  pep_a = 71;
  pep_b = 105;

  pep_ch_result_01 = (pep_a > pep_b) ? 'A' : 'B';
  pep_i_result_01 = (pep_a > pep_b) ? pep_a : pep_b;
  printf("Ternary Operator Answer 1 ----- %c and %d.\n\n", pep_ch_result_01, pep_i_result_01);

  pep_p = 130;
  pep_q = 130;
  pep_ch_result_02 = (pep_p != pep_q) ? 'P' : 'Q';
  pep_i_result_02 = (pep_p != pep_q) ? pep_p : pep_q;
  printf("Ternary Operator Answer 2 ----- %c And %d.\n\n", pep_ch_result_02, pep_i_result_02);

  printf("\n\n");

  return 0;
}
