#include <stdio.h>
#include <stdlib.h>

#define PEP_MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char *);

  // Variable Declarations
  char *pep_chArray = NULL;

  int pep_iStringLength = 0;

  // Code
  printf("\n\n");
  pep_chArray = (char *)malloc(PEP_MAX_STRING_LENGTH * sizeof(char));
  if (NULL == pep_chArray)
  {
    printf("Memory Allocation To Character Array Failed !!! Exitting Now...\n\n");
    exit(0);
  }

  printf("Enter A String : \n\n");
  gets_s(pep_chArray, PEP_MAX_STRING_LENGTH);

  printf("\n\n");
  printf("String Entered BY You Is : \n\n");
  printf("%s\n", pep_chArray);

  printf("\n\n");
  pep_iStringLength = MyStrlen(pep_chArray);
  printf("Length Of string Is = %d Characters !!!\n\n", pep_iStringLength);

  if (pep_chArray)
  {
    free(pep_chArray);
    pep_chArray = NULL;
  }

  return 0;
}

int MyStrlen(char *pep_str)
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < PEP_MAX_STRING_LENGTH; pep_j++)
  {
    if (*(pep_str + pep_j) == '\0')
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}
