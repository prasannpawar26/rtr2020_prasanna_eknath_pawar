#include <stdio.h>
#include <stdlib.h>

#define PEP_MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char *);
  void MyStrrev(char *, char *);

  // Variable Declarations
  char *pep_chArray_Original = NULL, *pep_chArray_Rev = NULL;
  int pep_original_string_length;

  // Code
  printf("\n\n");
  pep_chArray_Original = (char *)malloc(PEP_MAX_STRING_LENGTH * sizeof(char));
  if (NULL == pep_chArray_Original)
  {
    printf("Memory Allocation For Original String Failed !!! Exitting Now...\n\n");
    exit(0);
  }

  printf("Enter A String : \n\n");
  gets_s(pep_chArray_Original, PEP_MAX_STRING_LENGTH);

  pep_original_string_length = MyStrlen(pep_chArray_Original);

  pep_chArray_Rev = (char *)malloc(pep_original_string_length * sizeof(char));
  if (NULL == pep_chArray_Rev)
  {
    printf("Memory Allocation For Reverse String Failed !!! Exitting Now...\n\n");
    exit(0);
  }

  MyStrrev(pep_chArray_Rev, pep_chArray_Original);

  printf("\n\n");
  printf("The Original String Entered By You (i.e. 'chArray_Original') Is : \n\n");
  printf("%s\n", pep_chArray_Original);

  printf("\n\n");
  printf(
      "The Reverse String Entered By You (i.e. 'chArray_Rev') Is : \n\n");
  printf("%s\n", pep_chArray_Rev);

  printf("\n\n");

  if (NULL != pep_chArray_Rev)
  {
    free(pep_chArray_Rev);
    pep_chArray_Rev = NULL;
    printf("Memory Allocated For Reverse String Has Been Successfully Freed !!!\n\n");
  }

  if (NULL != pep_chArray_Original)
  {
    free(pep_chArray_Original);
    pep_chArray_Original = NULL;
    printf("Memory Allocated For Original String Has Been Successfully Freed !!!\n\n");
  }

  return 0;
}

void MyStrrev(char *pep_str_destiantion, char *pep_str_source)
{
  // Function Declarations
  int MyStrlen(char *);

  // Variable Declarations
  int pep_iStringLength = 0;
  int pep_i, pep_j, pep_len;

  // Code
  pep_iStringLength = MyStrlen(pep_str_source);

  pep_len = pep_iStringLength - 1;

  for (pep_i = 0, pep_j = pep_len; pep_i < pep_iStringLength, pep_j >= 0; pep_i++, pep_j--)
  {
    *(pep_str_destiantion + pep_i) = *(pep_str_source + pep_j);
  }

  *(pep_str_destiantion + pep_i) = '\0';

  return;
}

int MyStrlen(char *pep_str)
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < PEP_MAX_STRING_LENGTH; pep_j++)
  {
    if (*(pep_str + pep_j) == '\0')
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}
