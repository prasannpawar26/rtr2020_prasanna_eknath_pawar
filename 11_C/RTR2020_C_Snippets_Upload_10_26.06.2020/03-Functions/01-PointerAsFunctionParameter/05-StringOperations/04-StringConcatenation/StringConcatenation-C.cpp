#include <stdio.h>
#include <stdlib.h>

#define PEP_MAX_STRING_LENGTH 512

int main(void)
{
  // Function Declarations
  int MyStrlen(char *);
  void MyStrcat(char *, char *);

  // Variable Declarations
  char *pep_chArray_Original_One = NULL, *pep_chArray_Original_Two = NULL;

  // Code
  printf("\n\n");
  pep_chArray_Original_One =
      (char *)malloc(PEP_MAX_STRING_LENGTH * sizeof(char));
  if (NULL == pep_chArray_Original_One)
  {
    printf("Memory Allocation For Original One String Failed !!! Exitting Now...\n\n");
    exit(0);
  }

  printf("Enter A Original One String : \n\n");
  gets_s(pep_chArray_Original_One, PEP_MAX_STRING_LENGTH);

  printf("\n\n");

  pep_chArray_Original_Two =
      (char *)malloc(PEP_MAX_STRING_LENGTH * sizeof(char));
  if (NULL == pep_chArray_Original_Two)
  {
    printf("Memory Allocation For Original Two String Failed !!! Exitting Now...\n\n");
    exit(0);
  }
  printf("Enter A Original Two String : \n\n");
  gets_s(pep_chArray_Original_Two, PEP_MAX_STRING_LENGTH);

  printf("\n\n");
  printf("The Original One String Entered By You (i.e. 'chArray_Original_one') Is : \n\n");
  printf("%s\n", pep_chArray_Original_One);

  printf("\n\n");
  printf(
      "The Original Two String Entered By You (i.e. 'chArray_Original_Two') Is : "
      "\n\n");
  printf("%s\n", pep_chArray_Original_Two);

  MyStrcat(pep_chArray_Original_One, pep_chArray_Original_Two);

  printf("\n\n");
  printf("****** After Concatenation **********\n\n");

  printf("\n\n");
  printf(
      "The Original One String Entered By You (i.e. 'chArray_Original_one') Is "
      ": \n\n");
  printf("%s\n", pep_chArray_Original_One);

  printf("\n\n");
  printf(
      "The Original Two String Entered By You (i.e. 'chArray_Original_Two') Is "
      ": "
      "\n\n");
  printf("%s\n", pep_chArray_Original_Two);

  printf("\n\n");

  if (NULL != pep_chArray_Original_Two)
  {
    free(pep_chArray_Original_Two);
    pep_chArray_Original_Two = NULL;
    printf("Memory Allocated For Original Two String Has Been Successfully Freed !!!\n\n");
  }

  if (NULL != pep_chArray_Original_One)
  {
    free(pep_chArray_Original_One);
    pep_chArray_Original_One = NULL;
    printf("Memory Allocated For Original  One String Has Been Successfully Freed !!!\n\n");
  }

  return 0;
}

void MyStrcat(char *pep_str_destiantion, char *pep_str_source)
{
  // Function Declarations
  int MyStrlen(char *);

  // Variable Declarations
  int pep_iStringLength_Source = 0;
  int pep_iStringLength_Destination = 0;
  int pep_i, pep_j;

  // Code
  pep_iStringLength_Source = MyStrlen(pep_str_source);
  pep_iStringLength_Destination = MyStrlen(pep_str_destiantion);

  for (pep_i = pep_iStringLength_Destination, pep_j = 0;
       pep_j < pep_iStringLength_Source; pep_i++, pep_j++)
  {
    *(pep_str_destiantion + pep_i) = *(pep_str_source + pep_j);
  }

  *(pep_str_destiantion + pep_i) = '\0';

  return;
}

int MyStrlen(char *pep_str)
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < PEP_MAX_STRING_LENGTH; pep_j++)
  {
    if (*(pep_str + pep_j) == '\0')
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;
}
