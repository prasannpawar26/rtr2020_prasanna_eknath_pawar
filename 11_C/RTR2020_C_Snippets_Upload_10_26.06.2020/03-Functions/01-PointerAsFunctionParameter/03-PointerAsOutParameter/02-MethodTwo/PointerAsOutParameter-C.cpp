#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  // Function Declarations
  void MathematicalOperations(int, int, int *, int *, int *, int *, int *);

  // Variable Declarations
  int pep_a;
  int pep_b;

  int *pep_answer_sum = NULL;
  int *pep_answer_difference = NULL;
  int *pep_answer_product = NULL;
  int *pep_answer_reminder = NULL;
  int *pep_answer_quotient = NULL;

  // Code
  printf("\n\n");
  printf("Enter Value Of 'A' : ");
  scanf("%d", &pep_a);

  printf("\n\n");
  printf("Enter Value Of 'B' : ");
  scanf("%d", &pep_b);

  pep_answer_sum = (int *)malloc(1 * sizeof(int));
  if (NULL == pep_answer_sum)
  {
    printf("Could Not Allocate Memory For 'answer_sum' . Exitting Now...\n\n");
    exit(0);
  }
  printf("Allocate Memory For 'answer_sum' Successfully \n\n");

  pep_answer_difference = (int *)malloc(1 * sizeof(int));
  if (NULL == pep_answer_difference) {
    printf("Could Not Allocate Memory For 'answer_difference' . Exitting Now...\n\n");
    exit(0);
  }
  printf("Allocate Memory For 'answer_difference' Successfully \n\n");

  pep_answer_product = (int *)malloc(1 * sizeof(int));
  if (NULL == pep_answer_product) {
    printf(
        "Could Not Allocate Memory For 'answer_product' . Exitting "
        "Now...\n\n");
    exit(0);
  }
  printf("Allocate Memory For 'answer_product' Successfully \n\n");

  pep_answer_quotient = (int *)malloc(1 * sizeof(int));
  if (NULL == pep_answer_quotient) {
    printf(
        "Could Not Allocate Memory For 'answer_quotient' . Exitting "
        "Now...\n\n");
    exit(0);
  }
  printf("Allocate Memory For 'answer_quotient' Successfully \n\n");

  pep_answer_reminder = (int *)malloc(1 * sizeof(int));
  if (NULL == pep_answer_reminder) {
    printf(
        "Could Not Allocate Memory For 'answer_reminder' . Exitting "
        "Now...\n\n");
    exit(0);
  }
  printf("Allocate Memory For 'answer_reminder' Successfully \n\n");

  MathematicalOperations(pep_a, pep_b, pep_answer_sum, pep_answer_difference,
                         pep_answer_product, pep_answer_quotient,
                         pep_answer_reminder);

  printf("\n\n");

  printf("****** RESULTS ******\n\n");
  printf("Sum = %d\n", *pep_answer_sum);
  printf("Difference = %d\n", *pep_answer_difference);
  printf("Product = %d\n", *pep_answer_product);
  printf("Quotient = %d\n", *pep_answer_quotient);
  printf("Reminder = %d\n", *pep_answer_reminder);

  printf("\n\n");

  if (NULL != pep_answer_reminder) {
    free(pep_answer_reminder);
    pep_answer_reminder = NULL;
    printf("Memory Allocated For 'answer_reminder' Successfully Freed !!!\n\n");
  }

  if (NULL != pep_answer_quotient) {
    free(pep_answer_quotient);
    pep_answer_quotient = NULL;
    printf("Memory Allocated For 'answer_quotient' Successfully Freed !!!\n\n");
  }

  if (NULL != pep_answer_product) {
    free(pep_answer_product);
    pep_answer_product = NULL;
    printf("Memory Allocated For 'answer_product' Successfully Freed !!!\n\n");
  }

  if (NULL != pep_answer_difference) {
    free(pep_answer_difference);
    pep_answer_difference = NULL;
    printf("Memory Allocated For 'answer_difference' Successfully Freed !!!\n\n");
  }

  if (NULL != pep_answer_sum)
  {
    free(pep_answer_sum);
    pep_answer_sum = NULL;
    printf("Memory Allocated For 'answer_sum' Successfully Freed !!!\n\n");
  }

  return 0;
}

void MathematicalOperations(int pep_x, int pep_y, int *pep_sum, int *pep_difference,
  int *pep_product, int *pep_quotient, int *pep_reminder)
{
  // Code
  *pep_sum = pep_x + pep_y;
  *pep_difference = pep_x - pep_y;
  *pep_product = pep_x * pep_y;
  *pep_quotient = pep_x / pep_y;
  *pep_reminder = pep_x % pep_y;

  return;
}