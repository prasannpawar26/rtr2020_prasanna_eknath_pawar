#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  // Function Declarations
  void MultiplyArrayElementsByNumber(int *, int, int);

  // Variable Declarations
  int *pep_iArray = NULL;
  int pep_num_elements;
  int pep_i, pep_num;

  // Code
  printf("\n\n");
  printf("Enter How Many Elements You Want In The Integer Array : ");
  scanf("%d", &pep_num_elements);

  pep_iArray = (int *)malloc(pep_num_elements * sizeof(int));
  if (pep_iArray == NULL)
  {
    printf("Memory Allocation To iArray Has Failed !!! Exitting Now...\n\n");
    exit(0);
  }

  printf("\n\n");
  printf("Enter %d Elements For The Integer Array : \n\n", pep_num_elements);
  for (pep_i = 0; pep_i < pep_num_elements; pep_i++)
  {
    scanf("%d", &pep_iArray[pep_i]);
  }

  printf("\n\n");
  printf("Enter The NUmber By Which You Want To Multiply Each Integer Array Element : ");
  scanf("%d", &pep_num);

  MultiplyArrayElementsByNumber(pep_iArray, pep_num_elements, pep_num);

  printf("\n\n");
  printf("Array Returned By Function MultiplyArrayElementsByNumber() : \n\n");
  for (pep_i = 0; pep_i < pep_num_elements; pep_i++)
  {
    printf("iArray[%d] = %d\n",pep_i, pep_iArray[pep_i]);
  }

  if (NULL != pep_iArray)
  {
    free(pep_iArray);
    pep_iArray = NULL;
    printf("\n\n");
    printf("Memory Allocated To iArray Has Been Successfully Freed !!!\n\n");
  }

  return 0;

}

void MultiplyArrayElementsByNumber(int *pep_arr, int pep_iNumElements, int pep_n)
{
  // Variable Declarations
  int pep_i;

  // Code
  for (pep_i = 0; pep_i < pep_iNumElements; pep_i++)
  {
    pep_arr[pep_i] = pep_arr[pep_i] * pep_n;
  }

  return;
}
