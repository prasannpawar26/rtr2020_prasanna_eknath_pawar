#include <stdio.h>
#include <stdlib.h>

#define PEP_MAX_STRING_LENGTH 512

int main(void) {
  // Function Delcarations
  char *ReplacedVowelsWithHashSymbol(char *);

  // Variable Declarations
  char pep_string[PEP_MAX_STRING_LENGTH];
  char *pep_replaced_string = NULL;

  // Code
  printf("\n\n");
  printf("Enter String : ");
  gets_s(pep_string, PEP_MAX_STRING_LENGTH);

  pep_replaced_string = ReplacedVowelsWithHashSymbol(pep_string);
  if (NULL == pep_replaced_string) {
    printf(
        "ReplaceVowelsWithHashSymbol() Function Has Failed !!! Exitting "
        "Now...\n\n");
    exit(0);
  }

  printf("\n\n");
  printf("Replaced String Is : \n\n");
  printf("%s\n\n", pep_replaced_string);

  if (pep_replaced_string) {
    free(pep_replaced_string);
    pep_replaced_string = NULL;
  }

  return 0;
}

char *ReplacedVowelsWithHashSymbol(char *pep_s) {
  // Function Prototype
  void MyStrcpy(char *, char *);
  int MyStrlen(char *);

  // Variable Declarations
  char *pep_new_string = NULL;
  int pep_i;

  // Code
  pep_new_string = (char *)malloc(MyStrlen(pep_s) * sizeof(char));
  if (NULL == pep_new_string) {
    printf("Could Not Allocate Memory For New String !!! \n\n");
    return NULL;
  }

  MyStrcpy(pep_new_string, pep_s);

  for (pep_i = 0; pep_i < MyStrlen(pep_new_string); pep_i++) {
    switch (pep_new_string[pep_i])
    {
      case 'A':
      case 'a':
      case 'E':
      case 'e':
      case 'I':
      case 'i':
      case 'O':
      case 'o':
      case 'U':
      case 'u':
      {
        pep_new_string[pep_i] = '#';
      }
        break;

      default:
        break;
    }
  }

  return pep_new_string;
}

void MyStrcpy(char *pep_str_destination, char *pep_str_source)
{
  // Function Declarations
  int MyStrlen(char *);

  // Variable Delcarations
  int pep_iStringLength = 0;
  int pep_j;

  // Code
  pep_iStringLength = MyStrlen(pep_str_source);

  for (pep_j = 0; pep_j < pep_iStringLength; pep_j++)
  {
    *(pep_str_destination + pep_j) = *(pep_str_source + pep_j);
  }

  *(pep_str_destination + pep_j) = '\0';
}

int MyStrlen(char *pep_str)
{
  // Variable Declarations
  int pep_j;
  int pep_string_length = 0;

  // Code
  for (pep_j = 0; pep_j < PEP_MAX_STRING_LENGTH; pep_j++)
  {
    if (pep_str[pep_j] == '\0')
    {
      break;
    }
    else
    {
      pep_string_length++;
    }
  }

  return pep_string_length;

}