#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  // Variable Declarations
  int *pep_ptr_iArray = NULL;
  unsigned int pep_intArrayLength = 0;
  int pep_i;

  // Code
  printf("\n\n");
  printf("Enter The Number Of Elements You Want In Your Integer Array : \n\n");
  scanf("%d", &pep_intArrayLength);

  pep_ptr_iArray = (int *)malloc(sizeof(int) * pep_intArrayLength);
  if (NULL == pep_ptr_iArray)
  {
    printf("\n\n");
    printf(
        "Memory Allocation For Integer Array Has Failed !!! Exitting Now "
        "...\n\n");
    exit(0);
  }
  else
  {
    printf("Memory Allocation For Integer Array Has Succeeded !!!\n\n");
    printf(
        "Memory Address From %p To %p Have Been Allocated To Integer Array "
        "!!!\n\n",
        pep_ptr_iArray, (pep_ptr_iArray + (pep_intArrayLength - 1)));
  }

  printf("");
  printf("Enter %d Elements For The Integer Array : \n\n", pep_intArrayLength);

  for (pep_i = 0; pep_i < pep_intArrayLength; pep_i++)
  {
    scanf("%d", (pep_ptr_iArray + pep_i));
  }

  printf("\n\n");
  printf("The Integer Array Entered By You, Consisting Of %d Elements : \n\n",
         pep_intArrayLength);

  printf("\n\n");
  for (pep_i = 0; pep_i < pep_intArrayLength; pep_i++)
  {
    printf(" ptr_iArray[%d] = %d \t \t At Address &ptr_iArray[%d] = %p\n", pep_i,
           pep_ptr_iArray[pep_i], pep_i, &pep_ptr_iArray[pep_i]);
  }

  printf("\n\n");
  for (pep_i = 0; pep_i < pep_intArrayLength; pep_i++) {
    printf(" *(ptr_iArray + %d) = %d \t \t At Address (ptr_iArray + %d) = %p\n", pep_i,
           *(pep_ptr_iArray + pep_i), pep_i, (pep_ptr_iArray+ pep_i));
  }

  if (pep_ptr_iArray)
  {
    free(pep_ptr_iArray);
    pep_ptr_iArray = NULL;

    printf("\n\n");
    printf("Memory Allocated For Integer Array Has Been Successfully Freed !!! \n\n");
  }

  return 0;
}
