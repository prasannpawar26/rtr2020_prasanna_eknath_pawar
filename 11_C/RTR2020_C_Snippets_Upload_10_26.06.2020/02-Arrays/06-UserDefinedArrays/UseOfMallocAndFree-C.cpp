#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define PEP_INT_SIZE sizeof(int)
#define PEP_FLOAT_SIZE sizeof(float)
#define PEP_DOUBLE_SIZE sizeof(double)
#define PEP_CHAR_SIZE sizeof(char)

int main(void)
{
  // Variable Declarations
  int *pep_ptr_iArray = NULL;
  unsigned int pep_intArrayLength = 0;

  float *pep_ptr_fArray = NULL;
  unsigned int pep_floatArrayLength = 0;

  double *pep_ptr_dArray = NULL;
  unsigned int pep_doubleArrayLength = 0;

  char *pep_ptr_cArray = NULL;
  unsigned int pep_charArrayLength = 0;

  int pep_i;

  printf("\n\n");
  printf("Enter The Number Of Elements You Want In The Integer Array : ");
  scanf("%d", &pep_intArrayLength);

  pep_ptr_iArray = (int *)malloc(PEP_INT_SIZE * pep_intArrayLength);
  if (NULL == pep_ptr_iArray)
  {
    printf("\n\n");
    printf("Memory Allocation For Integer Array Failed !!! Exitting Now...\n\n");
    exit(0);
  }
  else
  {
    printf("\n\n");
    printf("Memory Allocation For Integer Array Succeeded !!!\n\n");
  }

  printf("\n\n");
  printf("Enter The %d Integer Elements To Fill Up The Integer Array : \n\n", pep_intArrayLength);
  for (pep_i = 0; pep_i < pep_intArrayLength; pep_i++)
  {
    scanf("%d", (pep_ptr_iArray + pep_i));
  }

  printf("\n\n");
  printf("Enter The Number Of Elements You Want In The Float Array : ");
  scanf("%d", &pep_floatArrayLength);

  pep_ptr_fArray = (float *)malloc(PEP_FLOAT_SIZE * pep_floatArrayLength);
  if (NULL == pep_ptr_fArray)
  {
    printf("\n\n");
    printf(
        "Memory Allocation For Float Array Failed !!! Exitting Now...\n\n");
    exit(0);
  }
  else
  {
    printf("\n\n");
    printf("Memory Allocation For Float Array Succeeded !!!\n\n");
  }
  printf("\n\n");
  printf("Enter The %d Floating-Point Elements To Fill Up The Floating-Point Array : \n\n",
         pep_floatArrayLength);
  for (pep_i = 0; pep_i < pep_floatArrayLength; pep_i++)
  {
    scanf("%f", (pep_ptr_fArray + pep_i));
  }

  printf("\n\n");
  printf("Enter The Number Of Elements You Want In The Double Array : ");
  scanf("%d", &pep_doubleArrayLength);

  pep_ptr_dArray = (double *)malloc(PEP_DOUBLE_SIZE * pep_doubleArrayLength);
  if (NULL == pep_ptr_dArray)
  {
    printf("\n\n");
    printf("Memory Allocation For Double Array Failed !!! Exitting Now...\n\n");
    exit(0);
  }
  else
  {
    printf("\n\n");
    printf("Memory Allocation For Double Array Succeeded !!!\n\n");
  }

  printf("\n\n");
  printf(
      "Enter The %d Double Elements To Fill Up The Double Array : "
      "\n\n",
      pep_doubleArrayLength);
  for (pep_i = 0; pep_i < pep_doubleArrayLength; pep_i++)
  {
    scanf("%lf", (pep_ptr_dArray + pep_i));
  }

  printf("\n\n");
  printf("Enter The Number Of Elements You Want In The Char Array : ");
  scanf("%d", &pep_charArrayLength);

  pep_ptr_cArray = (char *)malloc(PEP_CHAR_SIZE * pep_charArrayLength);
  if (NULL == pep_ptr_cArray)
  {
    printf("\n\n");
    printf("Memory Allocation For Char Array Failed !!! Exitting Now...\n\n");
    exit(0);
  }
  else
  {
    printf("\n\n");
    printf("Memory Allocation For Char Array Succeeded !!!\n\n");
  }

  printf("\n\n");
  printf(
      "Enter The %d Char Elements To Fill Up The Char Array : "
      "\n\n",
      pep_charArrayLength);
  for (pep_i = 0; pep_i < pep_charArrayLength; pep_i++)
  {
    *(pep_ptr_cArray + pep_i) = getch();
    printf("%c\n", *(pep_ptr_cArray + pep_i));
  }

  printf("\n\n");
  printf("The Integer Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", pep_intArrayLength);
  for (pep_i = 0; pep_i < pep_intArrayLength; pep_i++)
  {
    printf(" %d \t\t\t At Address : %p \n", *(pep_ptr_iArray + pep_i),
           (pep_ptr_iArray + pep_i));
  }

  printf("\n\n");
  printf(
      "The Floating-Point Array Entered By You And Consisting Of %d Elements Is As "
      "Follows : \n\n",
      pep_floatArrayLength);
  for (pep_i = 0; pep_i < pep_floatArrayLength; pep_i++)
  {
    printf(" %f \t\t\t At Address : %p \n", *(pep_ptr_fArray + pep_i),
           (pep_ptr_fArray + pep_i));
  }

  printf("\n\n");
  printf(
      "The Double Array Entered By You And Consisting Of %d Elements "
      "Is As "
      "Follows : \n\n",
      pep_doubleArrayLength);
  for (pep_i = 0; pep_i < pep_doubleArrayLength; pep_i++)
  {
    printf(" %lf \t\t\t At Address : %p \n", *(pep_ptr_dArray + pep_i),
           (pep_ptr_dArray + pep_i));
  }

  printf("\n\n");
  printf(
      "The Chat Array Entered By You And Consisting Of %d Elements "
      "Is As "
      "Follows : \n\n",
      pep_charArrayLength);
  for (pep_i = 0; pep_i < pep_charArrayLength; pep_i++)
  {
    printf(" %c \t\t\t At Address : %p \n", *(pep_ptr_cArray + pep_i),
           (pep_ptr_cArray + pep_i));
  }

  if (NULL != pep_ptr_cArray)
  {
    free(pep_ptr_cArray);
    pep_ptr_cArray = NULL;
    printf(
        "Memory Allocated By CHAR Array Has Been Successfully Freed "
        "!!!\n\n");
  }

  if (NULL != pep_ptr_dArray)
  {
    free(pep_ptr_dArray);
    pep_ptr_dArray = NULL;
    printf(
        "Memory Allocated By DOUBLE Array Has Been Successfully Freed "
        "!!!\n\n");
  }

  if (NULL != pep_ptr_fArray)
  {
    free(pep_ptr_fArray);
    pep_ptr_fArray = NULL;
    printf(
        "Memory Allocated By FLOAT Array Has Been Successfully Freed "
        "!!!\n\n");
  }

  if (NULL != pep_ptr_iArray)
  {
    free(pep_ptr_iArray);
    pep_ptr_iArray = NULL;
    printf("Memory Allocated By INTEGER Array Has Been Successfully Freed !!!\n\n");
  }

  return 0;
}