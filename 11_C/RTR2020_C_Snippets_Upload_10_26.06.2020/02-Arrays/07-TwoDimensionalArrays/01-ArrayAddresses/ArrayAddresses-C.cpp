#include <stdio.h>
#include <stdlib.h>

#define PEP_NUM_ROWS 5
#define PEP_NUM_COLUMNS 3

int main(void)
{
  // Variable Declarations
  int pep_iArray[PEP_NUM_ROWS][PEP_NUM_COLUMNS];
  int pep_i, pep_j;

  // Code
  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMNS; pep_j++)
    {
      pep_iArray[pep_i][pep_j] = (pep_i + 1) * (pep_j + 1);
    }
  }

  printf("\n\n");
  printf("2D Integer Array Elements Along With Addresses : \n\n");

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMNS; pep_j++)
    {
      printf("iArray[%d][%d] = %d \t \t At Address : %p \n", pep_i, pep_j,
             pep_iArray[pep_i][pep_j], &pep_iArray[pep_i][pep_j]);
    }

    printf("\n\n");
  }

  return 0;
}
