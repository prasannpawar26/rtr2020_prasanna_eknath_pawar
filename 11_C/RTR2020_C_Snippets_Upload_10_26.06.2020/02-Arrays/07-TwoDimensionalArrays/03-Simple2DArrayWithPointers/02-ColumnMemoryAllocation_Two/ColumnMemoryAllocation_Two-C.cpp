#include <stdio.h>
#include <stdlib.h>

#define PEP_NUM_ROWS 5
#define PEP_NUM_COLUMN_ONE 3
#define PEP_NUM_COLUMN_TWO 8

int main(void)
{
  // Variable Declarations
  int *pep_iArray[PEP_NUM_ROWS];
  int pep_i, pep_j;

  printf("\n\n");
  printf("==================== First Memory Allocation To 2D Integer Array ====================\n\n");
  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    pep_iArray[pep_i] = (int *)malloc(PEP_NUM_COLUMN_ONE * sizeof(int));
    if (NULL == pep_iArray[pep_i])
    {
      printf("Failed To Allocated Memory For Row %d Of 2D Array !!! Exitting Now...\n\n", pep_i);
      exit(0);
    }
    else
    {
      printf("Allocated Memory For Row %d Of 2D Array Successfully !!!\n\n",
             pep_i);
    }
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMN_ONE; pep_j++)
    {
      pep_iArray[pep_i][pep_j] = (pep_i + 1) * (pep_j + 1);
    }
  }

  printf("Displaying 2D Array Elements\n\n");
  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMN_ONE; pep_j++)
    {
      printf("pep_iArray[%d][%d]) = %d\n", pep_i,
             pep_j,  pep_iArray[pep_i][pep_j]);
    }
    printf("\n\n");
  }
  printf("\n\n");

  for (pep_i = PEP_NUM_ROWS - 1; pep_i >= 0; pep_i--) 
  {
    free(pep_iArray[pep_i]);
    pep_iArray[pep_i] = NULL;
    printf("Memory Allocated For Row %d Is Freed Successfully \n\n", pep_i);
  }

  printf("\n\n");
  printf(
      "==================== Sencond Memory Allocation To 2D Integer Array "
      "====================\n\n");
  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    pep_iArray[pep_i] = (int *)malloc(PEP_NUM_COLUMN_TWO * sizeof(int));
    if (NULL == pep_iArray[pep_i])
    {
      printf(
          "Failed To Allocated Memory For Row %d Of 2D Array !!! Exitting "
          "Now...\n\n",
          pep_i);
      exit(0);
    }
    else
    {
      printf("Allocated Memory For Row %d Of 2D Array Successfully !!!\n\n",
             pep_i);
    }
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMN_TWO; pep_j++)
    {
      pep_iArray[pep_i][pep_j] = (pep_i + 1) * (pep_j + 1);
    }
  }

  printf("Displaying 2D Array Elements\n\n");
  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMN_TWO; pep_j++)
    {
      printf("pep_iArray[%d][%d]) = %d\n", pep_i, pep_j,
             pep_iArray[pep_i][pep_j]);
    }
    printf("\n\n");
  }
  printf("\n\n");

  for (pep_i = PEP_NUM_ROWS - 1; pep_i >= 0; pep_i--)
  {
    free(pep_iArray[pep_i]);
    pep_iArray[pep_i] = NULL;
    printf("Memory Allocated For Row %d Is Freed Successfully \n\n", pep_i);
  }
}