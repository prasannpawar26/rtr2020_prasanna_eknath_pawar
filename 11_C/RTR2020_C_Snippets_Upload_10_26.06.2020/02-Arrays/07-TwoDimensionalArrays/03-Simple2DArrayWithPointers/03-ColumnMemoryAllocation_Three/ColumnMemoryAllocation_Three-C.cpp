#include <stdio.h>
#include <stdlib.h>

#define PEP_NUM_ROWS 5
#define PEP_NUM_COLUMNS 5

int main(void)
{
  // Variable Declarations
  int *pep_iArray[PEP_NUM_ROWS];
  int pep_i, pep_j;

  // Code
  printf("\n\n");
  printf("******** Memory Allocation To 2D  Integer Array *********\n");
  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    pep_iArray[pep_i] = (int *)malloc((PEP_NUM_COLUMNS - 1) * sizeof(int));
    if (NULL == pep_iArray[pep_i])
    {
      printf("Failed To Allocate Memory To Row %d Of 2D Integer Array !!! Exitting Now ...\n\n", pep_i);
      exit(0);
    }
    else
    {
      printf("Memory Allocation To Row %d Of 2D Integer Array Succeeded !!!\n\n", pep_i);
    }
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < (PEP_NUM_COLUMNS - pep_i); pep_j++)
    {
      pep_iArray[pep_i][pep_j] = (pep_i * 1) + (pep_j * 1);
    }
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < (PEP_NUM_COLUMNS - pep_i); pep_j++)
    {
      printf("iArray[%d][%d] = %d \t At Address : %p\n", pep_i, pep_j,
             pep_iArray[pep_i][pep_j], &pep_iArray[pep_i][pep_j]);
    }
    printf("\n");
  }

  for (pep_i = PEP_NUM_ROWS - 1; pep_i >= 0; pep_i--)
  {
    if (NULL != pep_iArray[pep_i])
    {
      free(pep_iArray[pep_i]);
      pep_iArray[pep_i] = NULL;
      printf("Memory Allocated To Row %d Has Been Succesfully Freed !!!\n\n", pep_i);
    }
  }

  return 0;
}