#include <stdio.h>
#include <stdlib.h>

#define PEP_NUM_ROWS 5
#define PEP_NUM_COLUMNS 3

int main(void)
{
  // Variable Declarations
  int **pep_ptr_iArray;
  int pep_i, pep_j;

  // Code

  printf("\n\n");

  pep_ptr_iArray = (int **)malloc(PEP_NUM_ROWS * sizeof(int *)); // No Of Rows.
  if (NULL == pep_ptr_iArray)
  {
    printf("Malloc Failed!!! Exitting Now...\n\n");
    exit(0);
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    pep_ptr_iArray[pep_i] = (int *)malloc(PEP_NUM_COLUMNS * sizeof(int));
    if (NULL == pep_ptr_iArray[pep_i])
    {
      printf("Malloc Failed!!! Exitting Now...\n\n");
      exit(0);
    }
    printf("Memory Allocated Successufully");
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMNS; pep_j++)
    {
      *(*(pep_ptr_iArray + pep_i) + pep_j) = (pep_i + 1) * (pep_j + 1);
    }
  }

  printf("\n\n");
  printf("2D Integer Array Elements Along With Addresses : \n\n");

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    for (pep_j = 0; pep_j < PEP_NUM_COLUMNS; pep_j++)
    {
      printf(
          "*(*(pep_ptr_iArray + %d) + %d) = %d \t \t At Address &pep_ptr_iArray[%d][%d] : "
          "%p \n",
          pep_i, pep_j, pep_ptr_iArray[pep_i][pep_j], pep_i, pep_j,
          &pep_ptr_iArray[pep_i][pep_j]);
    }

    printf("\n\n");
  }

  for (pep_i = 0; pep_i < PEP_NUM_ROWS; pep_i++)
  {
    if (*(pep_ptr_iArray + pep_i))
    {
      free(*(pep_ptr_iArray + pep_i));
      *(pep_ptr_iArray + pep_i) = NULL;
      printf("Memory  Of %d Row Freed Successuflly\n", pep_i);
    }
  }

  if (pep_ptr_iArray)
  {
    free(pep_ptr_iArray);
    pep_ptr_iArray = NULL;
    printf("Memory Freed Successuflly\n\n");
  }

  return 0;
}
