#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  // Variable Declarations
  int **pep_iArray = NULL;
  int pep_i, pep_j;
  int pep_num_rows, pep_num_columns;

  // Code
  printf("\n\n");
  printf("Enter Number Of Rows : ");
  scanf("%d", &pep_num_rows);

  printf("\n\n");
  printf("Enter Number Of Columns : ");
  scanf("%d", &pep_num_columns);

  printf("\n\n");
  printf("================ Memory Allocation To 2D Integer Array =================\n\n");
  pep_iArray = (int **)malloc(pep_num_rows * sizeof(int *));
  if (NULL == pep_iArray)
  {
    printf(
        "Failed To Allocate Memory To %d Rows Of 2D Interger Array !!! "
        "Exitting Now...\n\n",
        pep_num_rows);
    exit(0);
  }
  else
  {
    printf(
        "Memory Allocation To %d Rows Of 2D Integer Array Succeeded !!! \n\n",
        pep_num_rows);
  }

  for (pep_i = 0; pep_i < pep_num_rows; pep_i++)
  {
    pep_iArray[pep_i] = (int *)malloc(pep_num_columns * sizeof(int));
    if (NULL == pep_iArray[pep_i])
    {
      printf(
          "Failed To Allocate Memory To Columns Of Row %d Of 2D Interger Array !!! "
          "Exitting Now...\n\n",
          pep_i);
      exit(0);
    }
    else
    {
      printf(
          " Allocate Memory To Columns Of Row %d Of 2D Interger Array Succeeded !!! \n\n",
          pep_i);
    }
  }

  for (pep_i = 0; pep_i < pep_num_rows; pep_i++)
  {
    for (pep_j = 0; pep_j < pep_num_columns; pep_j++)
    {
      pep_iArray[pep_i][pep_j] = (pep_i * 1) + (pep_j * 1);
    }
  }

  for (pep_i = 0; pep_i < pep_num_rows; pep_i++)
  {
    printf("Base Address Of Row %d : iArray[%d] = %p \t At Address : %p\n",
           pep_i, pep_i, pep_iArray[pep_i], &pep_iArray[pep_i]);
  }

  printf("\n\n");

  for (pep_i = 0; pep_i < pep_num_rows; pep_i++)
  {
    for (pep_j = 0; pep_j < pep_num_columns; pep_j++)
    {
      printf("iArray[%d][%d] = %d \t At Address : %p\n", pep_i, pep_j,
             pep_iArray[pep_i][pep_j], &pep_iArray[pep_i][pep_j]);
    }
    printf("\n");
  }

  for (pep_i = pep_num_rows - 1; pep_i >=0; pep_i--)
  {
    if (NULL != pep_iArray[pep_i])
    {
      free(pep_iArray[pep_i]);
      pep_iArray[pep_i] = NULL;
      printf("Memory Allocated To Row %d Has Been Succesasfully Freed !!!\n\n", pep_i);
    }
  }

  if (NULL != pep_iArray)
  {
    free(pep_iArray);
    pep_iArray = NULL;
    printf("Memory Allocated To iArray Has Been Succesasfully Freed !!!\n\n");
  }

  return 0;
}
