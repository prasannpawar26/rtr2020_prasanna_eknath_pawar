#include <stdio.h>


int main(void)
{
  // Variable Declarations
  int pep_num;
  int *pep_ptr = NULL;
  int **pep_pptr = NULL;

  // Code
  pep_num = 10;

  printf("\n\n");

  printf("******* Before ptr = &num *******\n");
  printf("Value Of 'num'                      = %d\n\n", pep_num);
  printf("Address Of 'num'                    = %p\n\n", &pep_num);
  printf("Value At Address Of 'num'           = %d\n\n", *(&pep_num));

  pep_ptr = &pep_num;

  printf("\n\n");

  printf("******* After ptr = &num *******\n");
  printf("Value Of 'num'                      = %d\n\n", pep_num);
  printf("Address Of 'num'                    = %p\n\n", pep_ptr);
  printf("Value At Address Of 'num'           = %d\n\n", *pep_ptr);

  pep_pptr = &pep_ptr;

  printf("\n\n");

  printf("******* After pptr = &ptr *******\n");
  printf("Value Of 'num'                      = %d\n\n", pep_num);
  printf("Address Of 'num' (ptr)              = %p\n\n", pep_ptr);
  printf("Address Of 'ptr' (pptr)             = %p\n\n", pep_pptr);
  printf("Value At Address Of 'ptr' (*pptr)   = %p\n\n", *pep_pptr);
  printf("Value At Address Of 'num' (*ptr)    = %d\n\n", **pep_pptr);

  return 0;
}
