#include <stdio.h>


struct MyEmployee
{
  char name[100];
  int age;
  int salary;
  char sex;
  char marital_status;
};

int main(void)
{
  // Code

  printf("\n\n");
  printf("SIZES OF DATA TYPES AND POINTERS TO THOSE RESPECTIVE DATA TYPES ARE : \n\n");

  printf("Int : \n\n");
  printf(
      "\t Size of (int) : %zd \t Size of (int*) : %zd \t Size of (int**) : %zd \n\n",
      sizeof(int), sizeof(int*), sizeof(int**));

  printf("Float : \n\n");
  printf(
      "\t Size of (float) : %zd \t Size of (float*) : %zd \t Size of (float**) : %zd \n\n",
      sizeof(float), sizeof(float*), sizeof(float**));

  printf("Double : \n\n");
  printf(
      "\t Size of (double) : %zd \t Size of (double*) : %zd \t Size of (double**) : %zd \n\n",
      sizeof(double), sizeof(double*), sizeof(double**));

  printf("Char : \n\n");
  printf(
      "\t Size of (char) : %zd \t Size of (char*) : %zd \t Size of (char**) : %zd \n\n",
      sizeof(char), sizeof(char*), sizeof(char**));

  printf("struct MyEmployee : \n\n");
  printf(
      "\t Size of (struct MyEmployee) : %zd \t Size of (struct MyEmployee*) : %zd \t Size of (struct MyEmployee**) : %zd \n\n",
      sizeof(struct MyEmployee), sizeof(struct MyEmployee*),
      sizeof(struct MyEmployee**));

  return 0;
}
