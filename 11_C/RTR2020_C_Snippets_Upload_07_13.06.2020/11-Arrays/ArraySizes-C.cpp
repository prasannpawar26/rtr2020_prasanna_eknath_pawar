#include <stdio.h>

int main(void) {
  // Variable Declarations
  int pep_iArray_One[5];
  int pep_iArray_Two[5][3];
  int pep_iArray_Three[100][100][5];

  int pep_num_rows_2D;
  int pep_num_columns_2D;

  int pep_num_rows_3D;
  int pep_num_columns_3D;
  int pep_num_depth_3D;

  // Code
  printf("\n\n");
  printf("Size Of 1D Integer Array = %zu\n", sizeof(pep_iArray_One));
  printf("Number Of Elements In 1D Integer Array = %zu\n",
         sizeof(pep_iArray_One) / sizeof(int));

  printf("\n\n");
  printf("Size Of 2D Integer Array = %zu\n", sizeof(pep_iArray_Two));

  printf("Number Of Rows In 2D Integer Array = %zu\n",
         sizeof(pep_iArray_Two) / sizeof(pep_iArray_Two[0]));
  pep_num_rows_2D = sizeof(pep_iArray_Two) / sizeof(pep_iArray_Two[0]);

  printf("Number Of Columns In 2D Integer Array = %zu\n",
         sizeof(pep_iArray_Two[0]) / sizeof(pep_iArray_Two[0][0]));
  pep_num_columns_2D = sizeof(pep_iArray_Two[0]) / sizeof(pep_iArray_Two[0][0]);

  printf("Number Of Elements In 2D Array Is = %u\n",
         pep_num_rows_2D * pep_num_columns_2D);

  printf("\n\n");

  printf("Size Of 3D Integer Array = %zu\n", sizeof(pep_iArray_Three));

  printf("Number Of Rows In 3D Integer Array = %zu\n",
         sizeof(pep_iArray_Three) / sizeof(pep_iArray_Three[0]));
  pep_num_rows_3D = sizeof(pep_iArray_Three) / sizeof(pep_iArray_Three[0]);

  printf("Number Of Columns In 3D Integer Array = %zu\n",
         sizeof(pep_iArray_Three[0]) / sizeof(pep_iArray_Three[0][0]));
  pep_num_columns_3D =
      sizeof(pep_iArray_Three[0]) / sizeof(pep_iArray_Three[0][0]);

  printf("Number Of Depth In 3D Integer Array = %zu",
         sizeof(pep_iArray_Three[0][0]) / sizeof(pep_iArray_Three[0][0][0]));
  pep_num_depth_3D =
      sizeof(pep_iArray_Three[0][0]) / sizeof(pep_iArray_Three[0][0][0]);

  printf("Number Of Elements In 3D Array Is = %u\n", pep_num_rows_3D * pep_num_columns_3D * pep_num_depth_3D);

  printf("\n\n");
  return 0;
}
