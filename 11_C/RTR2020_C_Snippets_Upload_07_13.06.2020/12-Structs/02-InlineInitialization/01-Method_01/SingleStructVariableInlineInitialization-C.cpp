#include <stdio.h>

// Defining Struct
struct MyData 
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
} pep_data = {12, 4.5f, 11.32434, 'C'}; // Inline Initialization

int main(void)
{
  // Code
  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are : \n\n");
  printf(" i = %d\n", pep_data.pep_i);
  printf(" f = %f\n", pep_data.pep_f);
  printf(" d = %lf\n", pep_data.pep_d);
  printf(" c = %c\n", pep_data.pep_c);

  return 0;
}
