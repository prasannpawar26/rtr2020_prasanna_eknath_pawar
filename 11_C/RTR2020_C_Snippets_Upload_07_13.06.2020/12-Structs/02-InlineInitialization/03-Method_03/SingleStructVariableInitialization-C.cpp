#include <stdio.h>

int main(void)
{
  // Defining Struct
  struct MyData
  {
    int pep_i;
    float pep_f;
    double pep_d;
    char pep_c;
  } pep_data = {11, 21.5f, 1.3838348, 'B'};

  // Code
  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are : \n\n");
  printf(" i = %d\n", pep_data.pep_i);
  printf(" f = %f\n", pep_data.pep_f);
  printf(" d = %lf\n", pep_data.pep_d);
  printf(" c = %c\n", pep_data.pep_c);

  return 0;
}
