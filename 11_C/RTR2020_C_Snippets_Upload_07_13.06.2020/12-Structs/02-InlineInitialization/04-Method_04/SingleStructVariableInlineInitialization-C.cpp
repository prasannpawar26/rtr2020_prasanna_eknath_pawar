#include <stdio.h>

// Defining Struct
struct MyData {
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};
int main(void)
{
  struct MyData pep_data_one = {11, 21.5f, 1.33481234, 'B'};
  struct MyData pep_data_two = {'P', 6.5f, 12.3838348, 68};
  struct MyData pep_data_three = {36, 'R'};
  struct MyData pep_data_four = {78};

  // Code
  printf("\n\n");
  printf("Data Members Of 'struct MyData data_one' Are : \n\n");
  printf(" i = %d\n", pep_data_one.pep_i);
  printf(" f = %f\n", pep_data_one.pep_f);
  printf(" d = %lf\n", pep_data_one.pep_d);
  printf(" c = %c\n", pep_data_one.pep_c);

  printf("\n\n");
  printf("Data Members Of 'struct MyData data_two' Are : \n\n");
  printf(" i = %d\n", pep_data_two.pep_i);
  printf(" f = %f\n", pep_data_two.pep_f);
  printf(" d = %lf\n", pep_data_two.pep_d);
  printf(" c = %c\n", pep_data_two.pep_c);

  printf("\n\n");
  printf("Data Members Of 'struct MyData data_three' Are : \n\n");
  printf(" i = %d\n", pep_data_three.pep_i);
  printf(" f = %f\n", pep_data_three.pep_f);
  printf(" d = %lf\n", pep_data_three.pep_d);
  printf(" c = %c\n", pep_data_three.pep_c);

  printf("\n\n");
  printf("Data Members Of 'struct MyData data_four' Are : \n\n");
  printf(" i = %d\n", pep_data_four.pep_i);
  printf(" f = %f\n", pep_data_four.pep_f);
  printf(" d = %lf\n", pep_data_four.pep_d);
  printf(" c = %c\n", pep_data_four.pep_c);

  return 0;
}
