#include <stdio.h>

// Defining Struct
struct MyPoint
{
  int pep_x;
  int pep_y;
};

int main(void)
{
  // Variable Declarations
  struct MyPoint pep_point_A, pep_point_B, pep_point_C, pep_point_D,
      pep_point_E;

  // Code
  printf("\n\n");
  printf("Enter X-Coordinate For Point 'A' : ");
  scanf("%d", &pep_point_A.pep_x);
  printf("Enter Y-Coordinate For Point 'A' : ");
  scanf("%d", &pep_point_A.pep_y);
  printf("\n\n");
  printf("Enter X-Coordinate For Point 'B' : ");
  scanf("%d", &pep_point_B.pep_x);
  printf("Enter Y-Coordinate For Point 'B' : ");
  scanf("%d", &pep_point_B.pep_y);
  printf("\n\n");
  printf("Enter X-Coordinate For Point 'C' : ");
  scanf("%d", &pep_point_C.pep_x);
  printf("Enter Y-Coordinate For Point 'C' : ");
  scanf("%d", &pep_point_C.pep_y);
  printf("\n\n");
  printf("Enter X-Coordinate For Point 'D' : ");
  scanf("%d", &pep_point_D.pep_x);
  printf("Enter Y-Coordinate For Point 'D' : ");
  scanf("%d", &pep_point_D.pep_y);
  printf("\n\n");
  printf("Enter X-Coordinate For Point 'E' : ");
  scanf("%d", &pep_point_E.pep_x);
  printf("Enter Y-Coordinate For Point 'E' : ");
  scanf("%d", &pep_point_E.pep_y);

  printf("\n\n");
  printf("Co-ordinates (x, y) Of Point 'A' Are : (%d, %d) \n\n",
         pep_point_A.pep_x, pep_point_A.pep_y);
  printf("Co-ordinates (x, y) Of Point 'B' Are : (%d, %d) \n\n",
         pep_point_B.pep_x, pep_point_B.pep_y);
  printf("Co-ordinates (x, y) Of Point 'C' Are : (%d, %d) \n\n",
         pep_point_C.pep_x, pep_point_C.pep_y);
  printf("Co-ordinates (x, y) Of Point 'D' Are : (%d, %d) \n\n",
         pep_point_D.pep_x, pep_point_D.pep_y);
  printf("Co-ordinates (x, y) Of Point 'E' Are : (%d, %d) \n\n",
         pep_point_E.pep_x, pep_point_E.pep_y);

  return 0;
}
