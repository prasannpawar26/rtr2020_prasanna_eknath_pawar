#include <stdio.h>
#include <conio.h>

// Defining Struct
struct MyData 
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Variable Declarations
  struct MyData pep_data;

  printf("\n\n");
  printf("Enter Integer Value For Data Member 'i' Of 'struct MyData' : \n");
  scanf("%d", &pep_data.pep_i);
  printf("Enter Float Value For Data Member 'f' Of 'struct MyData' : \n");
  scanf("%f", &pep_data.pep_f);
  printf("Enter Double Value For Data Member 'd' Of 'struct MyData' : \n");
  scanf("%lf", &pep_data.pep_d);
  printf("Enter Char Value For Data Member 'c' Of 'struct MyData' : \n");
  pep_data.pep_c = getch();

  // Code
  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are : \n\n");
  printf(" i = %d\n", pep_data.pep_i);
  printf(" f = %f\n", pep_data.pep_f);
  printf(" d = %lf\n", pep_data.pep_d);
  printf(" c = %c\n", pep_data.pep_c);

  return 0;
}
