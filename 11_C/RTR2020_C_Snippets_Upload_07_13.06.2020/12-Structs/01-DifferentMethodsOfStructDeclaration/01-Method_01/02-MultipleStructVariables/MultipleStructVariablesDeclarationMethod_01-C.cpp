#include <stdio.h>

// Defining Struct
struct MyPoint
{
  int x;
  int y;
} pep_point_A, pep_point_B, pep_point_C, pep_point_D,
    pep_point_E;

int main(void)
{
 
  pep_point_A.x = 1;
  pep_point_A.y = 0;

  pep_point_B.x = 1;
  pep_point_B.y = 2;

  pep_point_C.x = 9;
  pep_point_C.y = 6;

  pep_point_D.x = 8;
  pep_point_D.y = 2;

  pep_point_E.x = 11;
  pep_point_E.y = 8;

  printf("\n\n");
  printf("Co-ordinates (x, y) Of Points 'A' Are : (%d, %d) \n\n", pep_point_A.x,
         pep_point_A.y);
  printf("Co-ordinates (x, y) Of Points 'B' Are : (%d, %d) \n\n", pep_point_B.x,
         pep_point_B.y);
  printf("Co-ordinates (x, y) Of Points 'C' Are : (%d, %d) \n\n", pep_point_C.x,
         pep_point_C.y);
  printf("Co-ordinates (x, y) Of Points 'D' Are : (%d, %d) \n\n", pep_point_D.x,
         pep_point_D.y);
  printf("Co-ordinates (x, y) Of Points 'E' Are : (%d, %d) \n\n", pep_point_E.x,
         pep_point_E.y);
 
  return 0;
}
