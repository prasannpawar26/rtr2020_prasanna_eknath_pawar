#include <stdio.h>

// Defining Struct
struct MyPoint
{
  int x;
  int y;
};

struct MyPoint pep_point_A, pep_point_B, pep_point_C, pep_point_D, pep_point_E;

int main(void)
{
 
  pep_point_A.x = 12;
  pep_point_A.y = 1;

  pep_point_B.x = 0;
  pep_point_B.y = 0;

  pep_point_C.x = 19;
  pep_point_C.y = 61;

  pep_point_D.x = 3;
  pep_point_D.y = 7;

  pep_point_E.x = 1;
  pep_point_E.y = 9;

  printf("\n\n");
  printf("Co-ordinates (x, y) Of Points 'A' Are : (%d, %d) \n\n", pep_point_A.x,
         pep_point_A.y);
  printf("Co-ordinates (x, y) Of Points 'B' Are : (%d, %d) \n\n", pep_point_B.x,
         pep_point_B.y);
  printf("Co-ordinates (x, y) Of Points 'C' Are : (%d, %d) \n\n", pep_point_C.x,
         pep_point_C.y);
  printf("Co-ordinates (x, y) Of Points 'D' Are : (%d, %d) \n\n", pep_point_D.x,
         pep_point_D.y);
  printf("Co-ordinates (x, y) Of Points 'E' Are : (%d, %d) \n\n", pep_point_E.x,
         pep_point_E.y);
 
  return 0;
}
