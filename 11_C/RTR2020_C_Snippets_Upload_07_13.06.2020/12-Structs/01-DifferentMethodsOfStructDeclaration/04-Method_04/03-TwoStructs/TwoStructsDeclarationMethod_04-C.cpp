#include <stdio.h>
#include <string.h>

// Defining Struct
struct MyPoint
{
  int x;
  int y;
};

struct MyPointProperties
{
  int pep_quadrant;
  char pep_axis_location[10];
};

int main(void)
{
  // Variable Declarations
  struct MyPoint pep_point;
  struct MyPointProperties pep_point_properties;

  // Code
  printf("\n\n");
  printf("Enter X-Coordinate For A Point : ");
  scanf("%d", &pep_point.x);
  printf("Enter Y-Coordinate For A Point : ");
  scanf("%d", &pep_point.y);

  printf("\n\n");
  printf("Point Co-ordinate (x, y) Are : (%d, %d) !!!\n\n", pep_point.x, pep_point.y);

  if (0 == pep_point.x && 0 == pep_point.y)
  {
    printf("The Point Is The Origin (%d, %d) !!!\n", pep_point.x, pep_point.y);
  }
  else
  {
    if (0 == pep_point.x)
    {
      if (0 > pep_point.y)
      {
        strcpy(pep_point_properties.pep_axis_location, "Negative Y");
      }
      else if (0 < pep_point.y)
      {
        strcpy(pep_point_properties.pep_axis_location, "Positive Y");
      }

      pep_point_properties.pep_quadrant = 0; // Point Lying On Any Of The Axes Is Not Part Of Any Quadrant

      printf("The Point Lies On The %s Axis\n\n",
             pep_point_properties.pep_axis_location);
    }
    else if (0 == pep_point.y)
    {
      if (0 > pep_point.x)
      {
        strcpy(pep_point_properties.pep_axis_location, "Negative X");
      }
      else if (0 < pep_point.x)
      {
        strcpy(pep_point_properties.pep_axis_location, "Positive X");
      }

      pep_point_properties.pep_quadrant =
          0;  // Point Lying On Any Of The Axes Is Not Part Of Any Quadrant

      printf("The Point Lies On The %s Axis\n\n",
             pep_point_properties.pep_axis_location);
    }
    else
    {
      // Both Axes
      pep_point_properties.pep_axis_location[0] = '\0';

      if (pep_point.x > 0 && pep_point.y > 0)
      {
        pep_point_properties.pep_quadrant = 1;
      }
      else if (pep_point.x < 0 && pep_point.y > 0)
      {
        pep_point_properties.pep_quadrant = 2;
      }
      else if (pep_point.x < 0 && pep_point.y < 0)
      {
        pep_point_properties.pep_quadrant = 3;
      
      }
      else
      {
        pep_point_properties.pep_quadrant = 4;
      }

      printf("The Point Lies In Quadrent Number %d !!!\n\n", pep_point_properties.pep_quadrant);
    }
  }
  return 0;
}
