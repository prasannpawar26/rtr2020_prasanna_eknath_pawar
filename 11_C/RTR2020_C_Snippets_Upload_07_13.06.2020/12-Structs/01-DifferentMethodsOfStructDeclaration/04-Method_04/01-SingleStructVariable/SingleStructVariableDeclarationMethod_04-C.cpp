#include <stdio.h>

// Defining Struct
struct MyData
{
  int i;
  float f;
  double d;
  char c;
};

int main(void)
{
  // variable declarations

  // Declaring A Single struct variable of type 'struct MyData' locally
  struct MyData pep_data;

  int pep_i_size;
  int pep_f_size;
  int pep_d_size;
  int pep_c_size;
  int pep_struct_mydata_size;

  pep_data.i = 3;
  pep_data.f = 4.45f;
  pep_data.d = 5.2995;
  pep_data.c = 'F';

  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are : \n\n");
  printf("i = %d\n", pep_data.i);
  printf("f = %f\n", pep_data.f);
  printf("d = %lf\n", pep_data.d);
  printf("i = %c\n", pep_data.c);

  pep_i_size = sizeof(pep_data.i);
  pep_f_size = sizeof(pep_data.f);
  pep_d_size = sizeof(pep_data.d);
  pep_c_size = sizeof(pep_data.c);

  printf("\n\n");
  printf("Sizes (in bytes) Of Data Members Of 'struct MyData' Are : \n\n");
  printf("'Size Of i' = %d\n", pep_i_size);
  printf("'Size Of f' = %d\n", pep_f_size);
  printf("'Size Of d' = %d\n", pep_d_size);
  printf("'Size Of c' = %d\n", pep_c_size);

  pep_struct_mydata_size = sizeof(struct MyData);

  printf("\n\n");
  printf("Size of 'struct MyData' : %d bytes \n\n", pep_struct_mydata_size);


  return 0;
}
