#include <stdio.h>

#define MAX_NAME_LENGTH 100

struct Employee
{
  char pep_name[MAX_NAME_LENGTH];
  unsigned int pep_age;
  char pep_gender;
  double pep_salary;
};

struct MyData
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Typedefs
  typedef struct Employee MY_EMPLOYEE_TYPE;
  typedef struct MyData MY_DATA_TYPE;

  // Variable Declarations
  struct Employee pep_emp = {"Funny", 25 , 'M', 23456.789};
  MY_EMPLOYEE_TYPE pep_emp_typedef = {"Bunny", 35, 'F', 31456.86};

  struct MyData pep_md = {34, 45.678f, 3.145678, 'X'};
  MY_DATA_TYPE pep_md_typedef;

  pep_md_typedef.pep_i = 56;
  pep_md_typedef.pep_f = 5.6f;
  pep_md_typedef.pep_d = 5.678966;
  pep_md_typedef.pep_c = 'G';

  printf("\n\n");
  printf("struct Employee : \n\n");
  printf("name = %s \n", pep_emp.pep_name);
  printf("age = %d \n", pep_emp.pep_age);
  printf("gender = %c \n", pep_emp.pep_gender);
  printf("salary = %lf \n", pep_emp.pep_salary);

  printf("\n\n");
  printf("MY_EMPLOYEE_TYPE : \n\n");
  printf("name = %s \n", pep_emp_typedef.pep_name);
  printf("age = %d \n", pep_emp_typedef.pep_age);
  printf("gender = %c \n", pep_emp_typedef.pep_gender);
  printf("salary = %lf \n", pep_emp_typedef.pep_salary);

  printf("\n\n");
  printf("struct MyData : \n\n");
  printf("i = %d \n", pep_md.pep_i);
  printf("f = %f \n", pep_md.pep_f);
  printf("d = %lf \n", pep_md.pep_d);
  printf("c = %c \n", pep_md.pep_c);

  printf("\n\n");
  printf("MY_DATA_TYPE: \n\n");
  printf("i = %d \n", pep_md_typedef.pep_i);
  printf("f = %f \n", pep_md_typedef.pep_f);
  printf("d = %lf \n", pep_md_typedef.pep_d);
  printf("c = %c \n", pep_md_typedef.pep_c);

  printf("\n\n");

  return 0;
}
