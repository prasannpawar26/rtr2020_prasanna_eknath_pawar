#include <stdio.h>

// Global Typedef
typedef int MY_INT;

int main(void)
{
  // Function Declarations
  MY_INT Add(MY_INT, MY_INT);

  // Typedefs
  typedef int MY_INT;
  typedef float PVG_FLOAT;
  typedef char CHARACTER;
  typedef double MY_DOUBLE;

  // Just Like Win32SDK
  typedef unsigned int UINT;
  typedef UINT HANDLE;
  typedef HANDLE HWND;
  typedef HANDLE HINSTANCE;

  // Variable Declarations
  MY_INT pep_a = 10, pep_i;
  MY_INT pep_iArray[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

  PVG_FLOAT pep_f_pvg = 34.7f;

  const PVG_FLOAT pep_f_pi = 3.14f;

  CHARACTER pep_ch = '*';
  CHARACTER pep_chArray_one[] = "Hello";
  CHARACTER pep_chArray_two[][10] = {"RTR", "Batch", "2020-2021"};

  MY_DOUBLE pep_d = 8.00434567;

  UINT pep_uint = 3456;
  HANDLE pep_handle = 9871;
  HWND pep_hwnd = 98715;
  HINSTANCE pep_hInstance = 44355;

  // Code
  printf("\n\n");
  printf("Type MY_INT Variable a = %d\n", pep_a);

  printf("\n\n");
  for (pep_i = 0; pep_i < sizeof(pep_iArray) / sizeof(pep_iArray[0]); pep_i++)
  {
    printf("Type MY_INT Array Variable iArray[%d] = %d\n", pep_i,
           pep_iArray[pep_i]);
  }

  printf("\n\n");
  printf("Type PVG_FLOAT variable f : %f\n", pep_f_pvg);
  printf("Type PVG_FLOAT constant f_pi : %f\n", pep_f_pi);

  printf("\n\n");
  printf("Type MY_DOUBLE Variable d = %lf\n", pep_d);

  printf("\n\n");
  printf("Type CHARACTER Variable ch = %c\n", pep_ch);

  printf("\n\n");
  printf("Type CHARACTER Array Variable  chArray_one = %s\n", pep_chArray_one);

  printf("\n\n");
  for (pep_i = 0; pep_i < (sizeof(pep_chArray_two) / sizeof(pep_chArray_two[0])); pep_i++)
  {
    printf("%s\t", pep_chArray_two[pep_i]);
  }

  printf("\n\n");
  printf("Type UINT Variable = %u\n", pep_uint);
  printf("Type HANDLE Variable = %u\n", pep_handle);
  printf("Type HWND Variable = %u\n", pep_hwnd);
  printf("Type HINSTANCE Variable = %u\n", pep_hInstance);
  printf("\n\n");

  MY_INT pep_x = 90;
  MY_INT pep_y = 34;
  MY_INT pep_ret;

  pep_ret = Add(pep_x, pep_y);
  printf("ret = %d\n\n", pep_ret);

  return 0;
}

MY_INT Add(MY_INT pep_a, MY_INT pep_b)
{
  // Code
  MY_INT pep_c;

  pep_c = pep_a + pep_b;

  return pep_c;
}
