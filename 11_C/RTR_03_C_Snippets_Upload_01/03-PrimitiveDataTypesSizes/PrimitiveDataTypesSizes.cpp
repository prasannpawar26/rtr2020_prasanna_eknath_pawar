#include <stdio.h>

int main(void)
{
  // Code
  printf("\n\n");

  printf("Size Of 'int'             = %zd bytes\n", sizeof(int));
  printf("Size Of 'unsigned int'    = %zd bytes\n", sizeof(unsigned int));
  printf("Size Of 'long'            = %zd bytes\n", sizeof(long));
  printf("Size Of 'long long'       = %zd bytes\n", sizeof(long long));

  printf("Size Of 'float'           = %zd bytes\n", sizeof(float));
  printf("Size Of 'double'          = %zd bytes\n", sizeof(double));
  printf("Size Of 'long double'     = %zd bytes\n", sizeof(long double));

  printf("\n\n");

  return 0;
}
