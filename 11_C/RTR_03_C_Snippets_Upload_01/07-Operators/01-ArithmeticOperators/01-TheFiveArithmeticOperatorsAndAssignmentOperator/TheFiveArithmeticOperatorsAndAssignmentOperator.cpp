#include <stdio.h>

int main(void)
{
  // Varaiable Declarations
  int a;
  int b;
  int result;

  // Code 
  printf("\n\n");
  printf("Enter A Number : ");
  scanf("%d", &a);

  printf("\n\n");
  printf("Enter B Number : ");
  scanf("%d", &b);

  printf("\n\n");
  // The Following Are The 5 Arithmetic Operators +, -, *, / and %

  result = a + b;
  printf("Addition Of A = %d And B = %d Gives %d.\n", a, b, result);

  result = a - b;
  printf("Subtraction Of A = %d And B = %d Gives %d.\n", a, b, result);

  result = a * b;
  printf("Mutiplication Of A = %d And B = %d Gives %d.\n", a, b, result);

  result = a / b;
  printf("Division Of A = %d And B = %d Gives %d.\n", a, b, result);

  result = a % b;
  printf("Division Of A = %d And B = %d Gives Remainder %d.\n", a, b, result);

  printf("\n\n");

  return 0;
}