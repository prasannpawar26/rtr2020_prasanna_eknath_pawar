#include <stdio.h>

int main(void)
{
  // Variable Declarations

  int a;
  int b;
  int x;

  // Code
  printf("\n\n");

  printf("Enter A Number :");
  scanf("%d", &a);

  printf("Enter B Number :");
  scanf("%d", &b);

  printf("\n\n");

  x = a;
  a += b; // a = a + b;
  printf("Addition Of A = %d Amd B = %d Gives %d.\n", x, b, a);

  x = a;
  a -= b; // a = a-b;
  printf("Subraction Of A = %d Amd B = %d Gives %d.\n", x, b, a);

  x = a;
  a *= b; // a = a * b;
  printf("Multiplication Of A = %d Amd B = %d Gives %d.\n", x, b, a);

  x = a;
  a /= b; // a = a / b;
  printf("Division Of A = %d Amd B = %d Gives %d.\n", x, b, a);

  x = a;
  a %= b; // a = a % b
  printf("Division Of A = %d Amd B = %d Gives Reminder %d.\n", x, b, a);

  printf("\n\n");

  return 0;
}
