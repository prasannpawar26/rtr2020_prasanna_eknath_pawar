#include <stdio.h>
#include <string.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20

#define ALPHABET_BEGINNING 65

struct MyDataOne {
  int iArray[INT_ARRAY_SIZE];
  float fArray[FLOAT_ARRAY_SIZE];
};

struct MyDataTwo {
  char cArray[CHAR_ARRAY_SIZE];
  char strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main(void) {
  // Variable Declarations
  struct MyDataOne pep_data_one;
  struct MyDataTwo pep_data_two;
  int pep_i;

  pep_data_one.fArray[0] = 0.1f;
  pep_data_one.fArray[1] = 0.2f;
  pep_data_one.fArray[2] = 0.3f;
  pep_data_one.fArray[3] = 0.4f;
  pep_data_one.fArray[4] = 0.5f;

  printf("\n\n");
  printf("Enter %d Integers : \n\n", INT_ARRAY_SIZE);
  for (pep_i = 0; pep_i < INT_ARRAY_SIZE; pep_i++) {
    scanf("%d", &pep_data_one.iArray[pep_i]);
  }

  for (pep_i = 0; pep_i < CHAR_ARRAY_SIZE; pep_i++) {
    pep_data_two.cArray[pep_i] = (char)(pep_i + ALPHABET_BEGINNING);
  }

  strcpy(pep_data_two.strArray[0], "Welcome !!!");
  strcpy(pep_data_two.strArray[1], "This");
  strcpy(pep_data_two.strArray[2], "Is");
  strcpy(pep_data_two.strArray[3], "ASTROMEDICOMP");
  strcpy(pep_data_two.strArray[4], "Real");
  strcpy(pep_data_two.strArray[5], "Time");
  strcpy(pep_data_two.strArray[6], "Rendering");
  strcpy(pep_data_two.strArray[7], "Batch");
  strcpy(pep_data_two.strArray[8], "Of");
  strcpy(pep_data_two.strArray[9], "2020-2021 !!!");

  printf("\n\n");
  printf(
      "Members Of 'struct DataOne' AlongWith Their Assigned Values Are : \n\n");

  printf("\n\n");
  printf("Integer Array (data_one.iArray[]) : \n\n");
  for (pep_i = 0; pep_i < INT_ARRAY_SIZE; pep_i++)
  {
    printf("data_one.iArray[%d] = %d\n", pep_i, pep_data_one.iArray[pep_i]);
  }

  printf("\n\n");
  printf("Float-Point Array (data_one.fArray[]) : \n\n");
  for (pep_i = 0; pep_i < FLOAT_ARRAY_SIZE; pep_i++)
  {
    printf("data_one.fArray[%d] = %f\n", pep_i, pep_data_one.fArray[pep_i]);
  }

  printf("\n\n");
  printf(
      "Members Of 'struct DataTwo' AlongWith Their Assigned Values Are : \n\n");

  printf("\n\n");
  printf("Char Array (data_two.cArray[]) : \n\n");
  for (pep_i = 0; pep_i < CHAR_ARRAY_SIZE; pep_i++)
  {
    printf("data_two.cArray[%d] = %c\n", pep_i, pep_data_two.cArray[pep_i]);
  }

  printf("\n\n");
  printf("String Array (data_two.strArray[]) : \n\n");
  for (pep_i = 0; pep_i < NUM_STRINGS; pep_i++)
  {
    printf("%s ", pep_data_two.strArray[pep_i]);
  }

  printf("\n\n");

  return 0;
}
