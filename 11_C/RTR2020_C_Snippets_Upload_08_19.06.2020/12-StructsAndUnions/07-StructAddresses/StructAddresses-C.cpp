#include <stdio.h>

struct MyData
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Variable Declartations
  struct MyData pep_data;

  // Code
  pep_data.pep_i = 35;
  pep_data.pep_f = 344.34f;
  pep_data.pep_d = 1.4577;
  pep_data.pep_c = 'G';

  printf("\n\n");
  printf("Data Members Of 'struct MyData' Are : \n\n");
  printf("i = %d\n", pep_data.pep_i);
  printf("f = %f\n", pep_data.pep_f);
  printf("d = %lf\n", pep_data.pep_d);
  printf("c = %c\n", pep_data.pep_c);

  printf("\n\n");
  printf("Adddrss Of Data Members Of 'struct MyData' Are : \n\n");
  printf("&i = %p\n", &pep_data.pep_i);
  printf("&f = %p\n", &pep_data.pep_f);
  printf("&d = %p\n", &pep_data.pep_d);
  printf("&c = %p\n\n", &pep_data.pep_c);

  printf("Starting Address Of 'struct MyData' Variable 'data' = %p\n\n", &pep_data);


}