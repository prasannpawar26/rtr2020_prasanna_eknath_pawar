#include <stdio.h>

int main(void)
{
  // Variable Declarations
  int pep_length, pep_breadth, pep_area;

  struct MyRectangle
  {
    struct MyPoint
    {
      int pep_x;
      int pep_y;
    } pep_point_01, pep_point_02;
  } pep_rect = {{34, 1}, {12, 23}};

  // Code
  pep_length = pep_rect.pep_point_02.pep_y - pep_rect.pep_point_01.pep_y;
  if (pep_length < 0)
  {
    pep_length = pep_length * -1;
  }

  pep_breadth = pep_rect.pep_point_02.pep_x - pep_rect.pep_point_01.pep_x;
  if (pep_breadth < 0)
  {
    pep_breadth = pep_breadth * -1;
  }

  pep_area = pep_length * pep_breadth;

  printf("\n\n");
  printf("Length Of Rectangle = %d\n\n", pep_length);
  printf("Breadth Of Rectangle = %d\n\n", pep_breadth);
  printf("Area Of Rectangle = %d\n\n", pep_area);

  return 0;
}
