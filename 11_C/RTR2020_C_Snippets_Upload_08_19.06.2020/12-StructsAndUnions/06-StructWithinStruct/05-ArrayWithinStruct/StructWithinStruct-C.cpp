#include <stdio.h>

struct MyNumber
{
  int pep_num;
  int pep_num_table[10];
};

struct MyNumTable
{
  struct MyNumber pep_a;
  struct MyNumber pep_b;
  struct MyNumber pep_c;
};

int main(void)
{
  // Variable Declarations
  struct MyNumTable pep_tables;
  int pep_i;
  
  // Code
  pep_tables.pep_a.pep_num = 3;
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_tables.pep_a.pep_num_table[pep_i] = pep_tables.pep_a.pep_num * (pep_i + 1);
  }

  printf("\n\n");
  printf("Table Of %d : \n\n", pep_tables.pep_a.pep_num);
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("%d * %d = %d \n\n", pep_tables.pep_a.pep_num, (pep_i + 1),
           pep_tables.pep_a.pep_num_table[pep_i]);
  }

  pep_tables.pep_b.pep_num = 4;
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_tables.pep_b.pep_num_table[pep_i] =
        pep_tables.pep_b.pep_num * (pep_i + 1);
  }

  printf("\n\n");
  printf("Table Of %d : \n\n", pep_tables.pep_b.pep_num);
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("%d * %d = %d \n\n", pep_tables.pep_b.pep_num, (pep_i + 1),
           pep_tables.pep_b.pep_num_table[pep_i]);
  }

  pep_tables.pep_c.pep_num = 5;
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_tables.pep_c.pep_num_table[pep_i] =
        pep_tables.pep_c.pep_num * (pep_i + 1);
  }

  printf("\n\n");
  printf("Table Of %d : \n\n", pep_tables.pep_c.pep_num);
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("%d * %d = %d \n\n", pep_tables.pep_c.pep_num, (pep_i + 1),
           pep_tables.pep_c.pep_num_table[pep_i]);
  }

  return 0;
}
