#include <stdio.h>

struct MyNumber
{
  int pep_num;
  int pep_num_table[10];
};

struct MyNumTable
{
  struct MyNumber pep_n;
};

int main(void)
{
  // Variable Declarations
  struct MyNumTable pep_tables[10];
  int pep_i;
  int pep_j;
  
  // Code
  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    pep_tables[pep_i].pep_n.pep_num = (pep_i + 1);
  }

  for (pep_i = 0; pep_i < 10; pep_i++)
  {
    printf("\n\n");
    printf("Table Of %d : \n\n", pep_tables[pep_i].pep_n.pep_num);
    for (pep_j = 0; pep_j < 10; pep_j++)
    {
      pep_tables[pep_i].pep_n.pep_num_table[pep_j] =
          pep_tables[pep_i].pep_n.pep_num * (pep_j + 1);

      printf("%d * %d = %d \n\n", pep_tables[pep_i].pep_n.pep_num, (pep_j + 1),
             pep_tables[pep_i].pep_n.pep_num_table[pep_j]);
    }

  }

  return 0;
}
