#include <stdio.h>
#include <conio.h>

struct MyStruct
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Function Declarations
  struct MyStruct AddStructMembers(struct MyStruct, struct MyStruct,
                                   struct MyStruct);

  // Variable Declarations
  struct MyStruct pep_data1, pep_data2, pep_data3, pep_answer;

  // Code
  printf("\n\n\n\n");
  printf("****************** DATA 1 ******************\n\n");
  printf("Enter Integer Value Of i : ");
  scanf("%d", &pep_data1.pep_i);
  printf("\n\n");
  printf("Enter Floating-Point Value Of f : ");
  scanf("%f", &pep_data1.pep_f);
  printf("\n\n");
  printf("Enter Double Value Of d : ");
  scanf("%lf", &pep_data1.pep_d);
  printf("\n\n");
  printf("Enter Character Value Of c : ");
  pep_data1.pep_c = getch();
  printf("%c", pep_data1.pep_c);

  printf("\n\n\n\n");
  printf("****************** DATA 2 ******************\n\n");
  printf("Enter Integer Value Of i : ");
  scanf("%d", &pep_data2.pep_i);
  printf("\n\n");
  printf("Enter Floating-Point Value Of f : ");
  scanf("%f", &pep_data2.pep_f);
  printf("\n\n");
  printf("Enter Double Value Of d : ");
  scanf("%lf", &pep_data2.pep_d);
  printf("\n\n");
  printf("Enter Character Value Of c : ");
  pep_data2.pep_c = getch();
  printf("%c", pep_data2.pep_c);

  printf("\n\n\n\n");
  printf("****************** DATA 3 ******************\n\n");
  printf("Enter Integer Value Of i : ");
  scanf("%d", &pep_data3.pep_i);
  printf("\n\n");
  printf("Enter Floating-Point Value Of f : ");
  scanf("%f", &pep_data3.pep_f);
  printf("\n\n");
  printf("Enter Double Value Of d : ");
  scanf("%lf", &pep_data3.pep_d);
  printf("\n\n");
  printf("Enter Character Value Of c : ");
  pep_data3.pep_c = getch();
  printf("%c", pep_data3.pep_c);
  
  pep_answer = AddStructMembers(pep_data1, pep_data2, pep_data3);

  printf("\n\n\n\n");
  printf("****************** ANSWER ******************\n\n");
  printf("answer.i : %d\n", pep_answer.pep_i);
  printf("answer.f : %f\n", pep_answer.pep_f);
  printf("answer.d : %lf\n\n", pep_answer.pep_d);

  pep_answer.pep_c = pep_data1.pep_c;
  printf("answer.c (from data1): %c\n\n", pep_data1.pep_c);

  pep_answer.pep_c = pep_data2.pep_c;
  printf("answer.c (from data2): %c\n\n", pep_data2.pep_c);

  pep_answer.pep_c = pep_data3.pep_c;
  printf("answer.c (from data3): %c\n\n", pep_data3.pep_c);

  printf("\n\n");
  return 0;
}

struct MyStruct AddStructMembers(struct MyStruct pep_one,
                                 struct MyStruct pep_two,
                                 struct MyStruct pep_three)
{
  // Variable Declarations
  struct MyStruct pep_answer;

  // Code
  pep_answer.pep_i = pep_one.pep_i + pep_two.pep_i + pep_three.pep_i;
  pep_answer.pep_f = pep_one.pep_f + pep_two.pep_f + pep_three.pep_f;
  pep_answer.pep_d = pep_one.pep_d + pep_two.pep_d + pep_three.pep_d;

  return pep_answer;
}
