#include <stdio.h>

union MyUnion
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Variable Declarations
  union MyUnion pep_u1, pep_u2;

  // Code
  printf("\n\n");
  printf("Members Of Union u1 Are : \n");

  pep_u1.pep_i = 5;
  pep_u1.pep_f = 1.3f;
  pep_u1.pep_d = 3.789894;
  pep_u1.pep_c = 'D';

  printf("u1.i = %d\n\n", pep_u1.pep_i);
  printf("u1.f = %f\n\n", pep_u1.pep_f);
  printf("u1.d = %lf\n\n", pep_u1.pep_d);
  printf("u1.c = %c\n\n", pep_u1.pep_c);

  printf("\n\n");
  printf("Address Of Members Of Union u1 Are : \n");

  printf("&u1.i = %p\n\n", &pep_u1.pep_i);
  printf("&u1.f = %p\n\n", &pep_u1.pep_f);
  printf("&u1.d = %p\n\n", &pep_u1.pep_d);
  printf("&u1.c = %p\n\n", &pep_u1.pep_c);

  printf("Address Of &u1 = %p\n\n", &pep_u1);

  printf("\n\n");

  printf("Members Of Union u2 Are : \n");

  pep_u2.pep_i = 4;
  printf("u2.i = %d\n\n", pep_u2.pep_i);

  pep_u2.pep_f = 3.3f;
  printf("u2.f = %f\n\n", pep_u2.pep_f);

  pep_u2.pep_d = 7.887834;
  printf("u2.d = %lf\n\n", pep_u2.pep_d);

  pep_u2.pep_c = 'W';
  printf("u2.c = %c\n\n", pep_u2.pep_c);

  printf("\n\n");
  printf("Address Of Members Of Union u2 Are : \n");

  printf("&u2.i = %p\n\n", &pep_u2.pep_i);
  printf("&u2.f = %p\n\n", &pep_u2.pep_f);
  printf("&u2.d = %p\n\n", &pep_u2.pep_d);
  printf("&u2.c = %p\n\n", &pep_u2.pep_c);

  printf("Address Of &u2 = %p\n\n", &pep_u2);

  return 0;
}
