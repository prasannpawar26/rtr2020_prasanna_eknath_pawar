#include <stdio.h>

struct MyStruct
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

union MyUnion
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Variable Declarations
  struct MyStruct pep_s;
  union MyUnion pep_u;

  // Code
  printf("\n\n");
  printf("Sizeof MyStruct = %zu\n", sizeof(pep_s));
  printf("\n\n");
  printf("Sizeof MyUnion = %zu\n", sizeof(pep_u));

  return 0;
}
