#include <stdio.h>

struct MyStruct
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

union MyUnion
{
  int pep_i;
  float pep_f;
  double pep_d;
  char pep_c;
};

int main(void)
{
  // Variable Declarations
  struct MyStruct pep_s;
  union MyUnion pep_u;

  // Code
  printf("\n\n");
  printf("Members Of MyStruct Are : \n");

  pep_s.pep_i = 5;
  pep_s.pep_f = 1.3f;
  pep_s.pep_d = 3.789894;
  pep_s.pep_c = 'D';

  printf("s.i = %d\n\n", pep_s.pep_i);
  printf("s.f = %f\n\n", pep_s.pep_f);
  printf("s.d = %lf\n\n", pep_s.pep_d);
  printf("s.c = %c\n\n", pep_s.pep_c);

  printf("\n\n");
  printf("Address Of Members Of MyStruct s Are : \n");

  printf("&s.i = %p\n\n", &pep_s.pep_i);
  printf("&s.f = %p\n\n", &pep_s.pep_f);
  printf("&s.d = %p\n\n", &pep_s.pep_d);
  printf("&s.c = %p\n\n", &pep_s.pep_c);

  printf("Address Of &s = %p\n\n", &pep_s);

  printf("\n\n");

  printf("Members Of Union u Are : \n");

  pep_u.pep_i = 4;
  printf("u.i = %d\n\n", pep_u.pep_i);

  pep_u.pep_f = 3.3f;
  printf("u.f = %f\n\n", pep_u.pep_f);

  pep_u.pep_d = 7.887834;
  printf("u.d = %lf\n\n", pep_u.pep_d);

  pep_u.pep_c = 'W';
  printf("u.c = %c\n\n", pep_u.pep_c);

  printf("\n\n");
  printf("Address Of Members Of Union u Are : \n");

  printf("&u.i = %p\n\n", &pep_u.pep_i);
  printf("&u.f = %p\n\n", &pep_u.pep_f);
  printf("&u.d = %p\n\n", &pep_u.pep_d);
  printf("&u.c = %p\n\n", &pep_u.pep_c);

  printf("Address Of &u = %p\n\n", &pep_u);

  return 0;
}
