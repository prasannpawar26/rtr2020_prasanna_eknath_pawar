#include <stdio.h>
#include <string.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
  char name[NAME_LENGTH];
  int age;
  float salary;
  char sex;
  char marital_status[MARITAL_STATUS];
};

int main(void)
{
  // Variable Declarations
  struct Employee pep_EmployeeRecord[5];

  char employee_prasanna[] = "Prasanna";
  char employee_nilesh[] = "Nilesh";
  char employee_nasir[] = "Nasir";
  char employee_jack[] = "Jack";
  char employee_kiran[] = "Kiran";

  int pep_i;

  strcpy(pep_EmployeeRecord[0].name, employee_prasanna);
  pep_EmployeeRecord[0].age = 32;
  pep_EmployeeRecord[0].sex = 'M';
  pep_EmployeeRecord[0].salary = 155555.0f;
  strcpy(pep_EmployeeRecord[0].marital_status, "Married");

  strcpy(pep_EmployeeRecord[1].name, employee_nilesh);
  pep_EmployeeRecord[1].age = 34;
  pep_EmployeeRecord[1].sex = 'M';
  pep_EmployeeRecord[1].salary = 1105555.0f;
  strcpy(pep_EmployeeRecord[1].marital_status, "UnMarried");

  strcpy(pep_EmployeeRecord[2].name, employee_nasir);
  pep_EmployeeRecord[2].age = 62;
  pep_EmployeeRecord[2].sex = 'M';
  pep_EmployeeRecord[2].salary = 412445.0f;
  strcpy(pep_EmployeeRecord[2].marital_status, "Married");

  strcpy(pep_EmployeeRecord[3].name, employee_jack);
  pep_EmployeeRecord[3].age = 53;
  pep_EmployeeRecord[3].sex = 'M';
  pep_EmployeeRecord[3].salary = 157425.0f;
  strcpy(pep_EmployeeRecord[3].marital_status, "UnMarried");

  strcpy(pep_EmployeeRecord[4].name, employee_kiran);
  pep_EmployeeRecord[4].age = 40;
  pep_EmployeeRecord[4].sex = 'M';
  pep_EmployeeRecord[4].salary = 105555.0f;
  strcpy(pep_EmployeeRecord[4].marital_status, "Married");

  printf("\n\n");
  printf(" ******* Display Employee Records *******\n\n");
  for (pep_i = 0; pep_i < 5; pep_i++)
  {
    printf(" \t****** Employee Number %d ******\n", (pep_i + 1));
    printf(" \tName                : %s\n", pep_EmployeeRecord[pep_i].name);
    printf(" \tAge                 : %d Years\n", pep_EmployeeRecord[pep_i].age);
    
    if ('M' == pep_EmployeeRecord[pep_i].sex ||
      'm' == pep_EmployeeRecord[pep_i].sex)
    {
      printf(" \tSex                 : Male\n");
    }
    else
    {
      printf(" \tSex                 : Female\n");
    }

    printf(" \tSalary              : Rs. %f\n",
           pep_EmployeeRecord[pep_i].salary);
    printf(" \tMarital Status      : %s\n",
           pep_EmployeeRecord[pep_i].marital_status);
    printf("\n");
  }

  return 0;
}
