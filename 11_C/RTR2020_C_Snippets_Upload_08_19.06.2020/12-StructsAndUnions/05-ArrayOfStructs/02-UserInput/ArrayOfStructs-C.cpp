#include <stdio.h>
#include <conio.h>
#include <ctype.h>

#define NUM_EMPLOYEES 5
#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
  char name[NAME_LENGTH];
  int age;
  float salary;
  char sex;
  char marital_status;
};

int main(void)
{
  // function declarations
  void MyGetString(char[], int);

  // Variable Declarations
  struct Employee pep_EmployeeRecord[NUM_EMPLOYEES];

  int pep_i;

  for (pep_i = 0; pep_i < NUM_EMPLOYEES; pep_i++)
  {
    printf("\n\n\n\n");
    printf("******* Data Entry For Employee Number %d *******\n", (pep_i));

    printf("\n\n");
    printf("Enter Employee Name : ");
    MyGetString(pep_EmployeeRecord[pep_i].name, NAME_LENGTH);

    printf("\n\n");
    printf("Enter Employee's Age (In Years) : ");
    scanf("%d", &pep_EmployeeRecord[pep_i].age);

    printf("\n\n");
    printf("Enter Employee's Sex (m/M For Male, f/F for Female) : ");
    pep_EmployeeRecord[pep_i].sex = getch();
    printf("%c\n", pep_EmployeeRecord[pep_i].sex);
 
    printf("\n\n");
    printf("Enter Employee's Salary (In Indian Rupees) : ");
    scanf("%f", &pep_EmployeeRecord[pep_i].salary);

    printf("\n\n");
    printf("Enter Employee Married (y/Y For Yes, n/N for No) : ");
    pep_EmployeeRecord[pep_i].marital_status = getch();
    printf("%c\n", pep_EmployeeRecord[pep_i].marital_status);
    pep_EmployeeRecord[pep_i].marital_status =
        toupper(pep_EmployeeRecord[pep_i].marital_status);

  }
 
  printf("\n\n");
  printf(" ******* Display Employee Records *******\n\n");
  for (pep_i = 0; pep_i < 5; pep_i++)
  {
    printf(" \t****** Employee Number %d ******\n", (pep_i + 1));
    printf(" \tName                : %s\n", pep_EmployeeRecord[pep_i].name);
    printf(" \tAge                 : %d Years\n", pep_EmployeeRecord[pep_i].age);
    
    if ('M' == pep_EmployeeRecord[pep_i].sex ||
      'm' == pep_EmployeeRecord[pep_i].sex)
    {
      printf(" \tSex                 : Male\n");
    }
    else
    {
      printf(" \tSex                 : Female\n");
    }

    printf(" \tSalary              : Rs. %f\n",
           pep_EmployeeRecord[pep_i].salary);

    if ('Y' == pep_EmployeeRecord[pep_i].marital_status ||
        'y' == pep_EmployeeRecord[pep_i].marital_status)
    {
      printf(" \tMarital Status      : Married\n");

    } else if ('N' == pep_EmployeeRecord[pep_i].marital_status ||
               'n' == pep_EmployeeRecord[pep_i].marital_status)
    {
      printf(" \tMarital Status      : Unmarried\n");
    }
    printf("\n");
  }

  return 0;
}

void MyGetString(char pep_str[], int pep_str_size)
{
  // variable declarations
  int pep_i;
  char pep_ch = '\0';

  // code
  pep_i = 0;

  do
  {
    pep_ch = getch();
    pep_str[pep_i] = pep_ch;

    printf("%c", pep_str[pep_i]);
    pep_i++;

  } while ((pep_ch != '\r') && (pep_i < pep_str_size));

  if (pep_i == pep_str_size)
  {
    pep_str[pep_i - 1] = '\0';
  }
  else
  {
    pep_str[pep_i] = '\0';
  }

  return;
}
