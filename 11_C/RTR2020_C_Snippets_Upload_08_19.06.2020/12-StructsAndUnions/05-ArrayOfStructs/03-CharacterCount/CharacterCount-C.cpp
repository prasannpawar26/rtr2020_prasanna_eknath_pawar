#include <stdio.h>
#include <conio.h>
#include <ctype.h>
#include <string.h>

#define MAX_STRING_LENGTH 1024

struct CharacterCount
{
  char pep_ch;
  int pep_ch_count;
} pep_character_and_count[] = {
    {'A', 0},
    {'B', 0},
    {'C', 0},
    {'D', 0},
    {'E', 0},
    {'F', 0},
    {'G', 0},
    {'H', 0},
    {'I', 0},
    {'J', 0},
    {'K', 0},
    {'L', 0},
    {'M', 0},
    {'N', 0},
    {'O', 0},
    {'P', 0},
    {'Q', 0},
    {'R', 0},
    {'S', 0},
    {'T', 0},
    {'U', 0},
    {'V', 0},
    {'W', 0},
    {'X', 0},
    {'Y', 0},
    {'Z', 0}
};

#define SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS sizeof(pep_character_and_count)
#define SIZE_OF_STRUCT_FROM_THE_ARRAY_OF_STRUCTS sizeof(pep_character_and_count[0])
#define NUM_ELEMENTS_IN_ARRAY (SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS / SIZE_OF_STRUCT_FROM_THE_ARRAY_OF_STRUCTS)

int main(void)
{
  // variable declarations
  char pep_str[MAX_STRING_LENGTH];
  int pep_i, pep_j, pep_actual_string_length = 0;

  // Code
  printf("\n\n");
  printf("Enter A String : \n\n");
  gets_s(pep_str, MAX_STRING_LENGTH);

  pep_actual_string_length = strlen(pep_str);

  printf("\n\n");
  printf("The String You Have Entered Is : \n");
  printf("%s\n\n", pep_str);

  for(pep_i = 0; pep_i < pep_actual_string_length; pep_i++)
  {
    for (pep_j = 0; pep_j < NUM_ELEMENTS_IN_ARRAY; pep_j++)
    {
      pep_str[pep_i] = toupper(pep_str[pep_i]);

      if (pep_str[pep_i] == pep_character_and_count[pep_j].pep_ch)
      {
        pep_character_and_count[pep_j].pep_ch_count++;
      }
    }
  }

  printf("\n\n");
  printf("The Number Of Occurances Of All Chracters From The Alphabet Are As Follows : \n\n");
  for (pep_j = 0; pep_j < NUM_ELEMENTS_IN_ARRAY; pep_j++)
  {
   
   printf("Character %c = %d\n", pep_character_and_count[pep_j].pep_ch, pep_character_and_count[pep_j].pep_ch_count);
  }

  printf("\n\n");

  return 0;
}
