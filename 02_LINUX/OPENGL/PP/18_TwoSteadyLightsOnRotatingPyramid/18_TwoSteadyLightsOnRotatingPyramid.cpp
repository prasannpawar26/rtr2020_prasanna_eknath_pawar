#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXTCOORD0
};

bool pep_bFullScreen = false;

Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;
Colormap pep_gColormap;
Window pep_gWindow;

int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;

static GLXContext pep_gGlxContext;

//PP Related Code
GLuint pep_TwoSteadyLights_gVertexShaderObject;
GLuint pep_TwoSteadyLights_gFragmentShaderObject;
GLuint pep_TwoSteadyLights_gShaderProgramObject;

GLuint pep_TwoSteadyLights_vao;
GLuint pep_TwoSteadyLights_vbo_position;
GLuint pep_TwoSteadyLights_vbo_normal;
GLfloat pep_TwoSteadyLights_PyramidRotation;

// UNIFORMS
GLuint pep_TwoSteadyLights_modelMatrixUniform;
GLuint pep_TwoSteadyLights_viewMatrixUniform;
GLuint pep_TwoSteadyLights_projectionMatrixUniform;

GLuint pep_TwoSteadyLights_lightAmbient0Uniform0;
GLuint pep_TwoSteadyLights_lightDiffuse0Uniform0;
GLuint pep_TwoSteadyLights_lightSpecular0Uniform0;
GLuint pep_TwoSteadyLights_lightPosition0Uniform0;

GLuint pep_TwoSteadyLights_lightAmbient0Uniform1;
GLuint pep_TwoSteadyLights_lightDiffuse0Uniform1;
GLuint pep_TwoSteadyLights_lightSpecular0Uniform1;
GLuint pep_TwoSteadyLights_lightPosition0Uniform1;


GLuint pep_TwoSteadyLights_materialAmbientUniform;
GLuint pep_TwoSteadyLights_materialDiffuseUniform;
GLuint pep_TwoSteadyLights_materialSpecularUniform;
GLuint pep_TwoSteadyLights_materialShinessUniform;

GLuint pep_TwoSteadyLights_keyLPressedUniform;

bool pep_TwoSteadyLights_bIsLightEnable = true;
bool pep_TwoSteadyLights_bIsAnimation = true;

//
// Application Variables Associated With Uniforms
float pep_TwoSteadyLights_lightAmbient0[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_TwoSteadyLights_lightDiffuse0[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float pep_TwoSteadyLights_lightSpecular0[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float pep_TwoSteadyLights_lightPosition0[4] = {-2.0f, 0.0f, 0.0f, 1.0f};

float pep_TwoSteadyLights_lightAmbient1[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_TwoSteadyLights_lightDiffuse1[4] = {0.0f, 0.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_lightSpecular1[4] = {0.0f, 0.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_lightPosition1[4] = {2.0f, 0.0f, 0.0f, 1.0f};

float pep_TwoSteadyLights_materialAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_TwoSteadyLights_materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_materialShiness = 50.0f;  // Also Try 128.0f


mat4 pep_perspectiveProjectionMatrix;

// Changes Related To Get FBConfig
typedef GLXContext (*procpep_glXCreateContextAttribsARBProc)
						(Display *,
						GLXFBConfig,
						GLXContext,
						Bool,
						const int *);

procpep_glXCreateContextAttribsARBProc pep_glXCreateContextAttribsARBProc;

GLXFBConfig pep_gGlxFbConfig;

int main()
{
	//function protoypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Initialize(void);
	void ReSize(int, int);
	void Draw(void);
    void Update(void);

	// variable declarations
	bool bDone = false;

	int winWidth = pep_giWindowWidth;
	int winHeight = pep_giWindowHeight;

	// code
	CreateWindow();
	Initialize();

	// Game Loop
	XEvent event;
	KeySym keysym;

	while(false == bDone)
	{
		while(XPending(pep_gpDisplay))
		{
			XNextEvent(pep_gpDisplay, &event);

			switch(event.type)
			{
				case MapNotify:
				break;

				case KeyPress:
				{
					keysym = XkbKeycodeToKeysym(pep_gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
						bDone = true;
						break;

						case XK_F:
						case XK_f:
						{
							if(false == pep_bFullScreen)
							{
								ToggleFullScreen();
								pep_bFullScreen = true;
							}
							else
							{
								ToggleFullScreen();
								pep_bFullScreen = false;
							}
						}
						break;
                        
                        case XK_L:
                        case XK_l:
                        {
                            if (false == pep_TwoSteadyLights_bIsLightEnable)
                            {
                                pep_TwoSteadyLights_bIsLightEnable = true;
                            }
                            else
                            {
                                pep_TwoSteadyLights_bIsLightEnable = false;
                            }
                            break;
                                
                        }
                        break;
                        
                        case XK_A:
                        case XK_a:
                        {
                            if (false == pep_TwoSteadyLights_bIsAnimation)
                            {
                                pep_TwoSteadyLights_bIsAnimation = true;
                            }
                            else
                            {
                                pep_TwoSteadyLights_bIsAnimation = false;
                            }
                            break;
                                
                        }
                        break;

						default:
						break;
					} // Switch Block End
				} // Case KeyPress
				break;

				case ButtonPress:
				{
					switch(event.xbutton.button)
					{
						case 1:
						break;

						case 2:
						break;

						case 3:
						break;

						case 4:
						break;

						case 5:
						break;
					}
				} // Case ButtonPress
				break;

				case MotionNotify:
				break;

				case ConfigureNotify:
				{
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;

					ReSize(winWidth, winHeight);
				}
				break;

				case Expose:
				break;

				case DestroyNotify:
				break;

				case 33:
				bDone = true;
				break;

				default:
				break;

			} // Switch Block End
		} // Inner While Loop End

		Update();
		Draw();
	} // Outer While Loop End

	Uninitialize();

	return 0;
}

void CreateWindow(void)
{
	// function declarations
	void Uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	// FBConfig Related Changes
	GLXFBConfig *pGlxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFbConfigs = 0;

	// Initialize Framebuffers Attributes
	static int frameBufferAttributes[] =
	{
		GLX_X_RENDERABLE, True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_STENCIL_SIZE, 8,
		None
	};

	pep_gpDisplay = XOpenDisplay(NULL);
	if(NULL == pep_gpDisplay)
	{
		printf("Error : XOpenDisplay Failed.\nExitting Now...\n\n");
		Uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(pep_gpDisplay);

	//	Algorithm To Get FBConfig
	//  
	// 1. Get Array Of All Available FBConfigs : glXChooseFbConfig
	// 2. Select Best From All : Based On Sampler and SampleBuffer
	// 3. Define High Quality H/W Rendering Context Frame Buffer To Get
	// 4. Get XVisualInfo From Best FBConfig : glXGetVisualFromConfig
	//
	// FBConfig Related Changes
	// Retrieve All FBConfigs Driver Has.
	// pGlxFbConfigs : This Is An Array Of FBConfig(s)
	// iNumberOfFbConfigs : If Multiple FBConfig(s) Present, To Get The Number We Are Using This 
	pGlxFbConfigs = glXChooseFBConfig(pep_gpDisplay, defaultScreen, frameBufferAttributes, &iNumberOfFbConfigs);

	int sampleBuffers, samples;
	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples = 999;

	for (int i = 0; i < iNumberOfFbConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(pep_gpDisplay, pGlxFbConfigs[i]);

		// For Each Optained frame FBConfig, Get temp VisualInfo. Its Use To Just Check The Capability Of NExt Call Can Be Made Or Not.
		if(NULL != pTempXVisualInfo)
		{
			// Get Number of Sample Buffers From Respective FBConfig
			glXGetFBConfigAttrib(pep_gpDisplay, pGlxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			// Get Number Of Samples From Respective FBConfig
			// Sampler => Breaking "Pixel" Into Small Parts 
			glXGetFBConfigAttrib(pep_gpDisplay, pGlxFbConfigs[i], GLX_SAMPLES, &samples);

			// More The Number Of Samples And Sample Buffers More The Eligible FBConfig, So Do The Comparision
			// Ge best FBConfig
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			// Get Worst
			if(worstFrameBufferConfig < 999 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		} // If End

		XFree(pTempXVisualInfo);
	} // For End

	// Now Assigned Beest FBConfig
	bestGlxFbConfig = pGlxFbConfigs[bestFrameBufferConfig];

	// Assign The Best FBConfig To Global Variable
	pep_gGlxFbConfig = bestGlxFbConfig;

	// Free The Obtain GLXFBConfig Array - Not Required

	// Get XVisualInfo From Best FBConfig
	pep_gpXVisualInfo = glXGetVisualFromFBConfig(pep_gpDisplay, bestGlxFbConfig);
	if(NULL == pep_gpXVisualInfo)
	{
		printf("Error : GLXGetVisualFromFBConfig Failed.\n Exitting Now...\n\n");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;

	winAttribs.colormap = XCreateColormap(pep_gpDisplay,
											RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
											pep_gpXVisualInfo->visual,
											AllocNone);
	pep_gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(pep_gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	pep_gWindow = XCreateWindow(pep_gpDisplay,
							RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
							0,
							0,
							pep_giWindowWidth, pep_giWindowHeight,
							0,
							pep_gpXVisualInfo->depth,
							InputOutput,
							pep_gpXVisualInfo->visual,
							styleMask,
							&winAttribs);
	if(!pep_gWindow)
	{
		printf("Error : XCreateWindow Failed.\nExitting Now");
		Uninitialize();
		exit(1);
	}

	XStoreName(pep_gpDisplay, pep_gWindow, "PP - Two Steady Lights");
	Atom windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(pep_gpDisplay, pep_gWindow, &windowManagerDelete, 1);

	XMapWindow(pep_gpDisplay, pep_gWindow);

	return;
}

void Initialize(void)
{
	// function declarations
	void ReSize(int, int);
	void Uninitialize(void);

	// code

	pep_glXCreateContextAttribsARBProc = (procpep_glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	if(NULL == pep_glXCreateContextAttribsARBProc)
	{
		Uninitialize();
		exit(1);
	}

	GLint attribs[] = 
	{
		// Latest OpenGL Context 4.5
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	pep_gGlxContext = pep_glXCreateContextAttribsARBProc(
		pep_gpDisplay,
		pep_gGlxFbConfig,
		0, // If Multiple Monitor Forms One Screen, Then Graphics Context Has To Be Shared. As We Are Not Using Multiple Monitor Hence Specifiing Zero.
		True, // We Are Interested In H/W Rendering Context. Hence True
			  // If Linux Installed In VM. Then We Are Interested In S/W Rendering Context. For S/W Context => Specify False
		attribs);
	if(NULL == pep_gGlxContext)
	{
		// If Not Obtained Highest Context, Specify The Lowest One => it Will Give/ Return The Highest One Which Is known To Grpahics Driver
		printf("%s : %s : %d = Failed To Obtained Highest H/W Rendering Context.\nAsking For Nearest Highest Context\n", __FILE__, __FUNCTION__, __LINE__);

		// Not Full H/W But To Get Nearest High Context
		GLint attribs[] = 
		{
			// OpenGL Context 1.0
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			None
		};

		pep_gGlxContext = pep_glXCreateContextAttribsARBProc(pep_gpDisplay, pep_gGlxFbConfig, 0, True, attribs);
	}

	// Check Obtained Context Is Really H/W Rendering Context Or Not
	if(!glXIsDirect(pep_gpDisplay, pep_gGlxContext))
	{
		printf("%s : %s : %d = Obtained Context Is Not H/W Rendering Context\n", __FILE__, __FUNCTION__, __LINE__);
	
	}
	else
	{
		printf("%s : %s : %d = Obtained Context Is H/W Rendering Context\n", __FILE__, __FUNCTION__, __LINE__);	
	}

	// Make Obtain Context As Current OpenGL Context
	glXMakeCurrent(pep_gpDisplay, pep_gWindow, pep_gGlxContext);

	GLenum result;

	// Enable Extensions
	result = glewInit();
	if(GLEW_OK != result)
	{
		Uninitialize();
		exit(1);
	}

	const GLfloat pyramidVertices[] = {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };
    const GLfloat pyramidNormals[] = {
        0.0f, 0.447214f, 0.894427f,// front-top
        0.0f, 0.447214f, 0.894427f,// front-left
        0.0f, 0.447214f, 0.894427f,// front-right

        0.894427f, 0.447214f, 0.0f, // right-top
        0.894427f, 0.447214f, 0.0f, // right-left
        0.894427f, 0.447214f, 0.0f, // right-right

        0.0f, 0.447214f, -0.894427f, // back-top
        0.0f, 0.447214f, -0.894427f, // back-left
        0.0f, 0.447214f, -0.894427f, // back-right

        -0.894427f, 0.447214f, 0.0f, // left-top
        -0.894427f, 0.447214f, 0.0f, // left-left
        -0.894427f, 0.447214f, 0.0f // left-right
    };

    pep_TwoSteadyLights_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"

        "uniform vec3 u_light_ambient0;"
        "uniform vec3 u_light_diffuse0;"
        "uniform vec3 u_light_specular0;"
        "uniform vec4 u_light_position0;"

        "uniform vec3 u_light_ambient1;"
        "uniform vec3 u_light_diffuse1;"
        "uniform vec3 u_light_specular1;"
        "uniform vec4 u_light_position1;"

        "uniform vec3 u_material_ambient;"
        "uniform vec3 u_material_diffuse;"
        "uniform vec3 u_material_specular;"
        "uniform float u_material_shiness;"

        "uniform int u_key_L_pressed;"

        "out vec3 phong_ads_light;"

        "void main(void)"
        "{"
            "if(1 == u_key_L_pressed)"
            "{"
                "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
                "vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
                "vec3 viewer_vector = normalize(vec3(-eye_coordinates));"

                "vec3 light_direction0 = normalize(vec3(u_light_position0 - eye_coordinates));"
                "float t_normal_dot_light_direction0 = max(dot(light_direction0, t_normal), 0.0f);"
                "vec3 reflection_vector0 = reflect(-light_direction0, t_normal);"

                "vec3 light_direction1 = normalize(vec3(u_light_position1 - eye_coordinates));"
                "float t_normal_dot_light_direction1 = max(dot(light_direction1, t_normal), 0.0f);"
                "vec3 reflection_vector1 = reflect(-light_direction1, t_normal);"

                "vec3 ambient;"
                "vec3 diffuse;"
                "vec3 specular;"

                "ambient = u_light_ambient0 * u_material_ambient;"
                "diffuse = u_light_diffuse0 * u_material_diffuse * t_normal_dot_light_direction0;"
                "specular = u_light_specular0 * u_material_specular * pow(max(dot(reflection_vector0, viewer_vector), 0.0f), u_material_shiness);"

                "ambient += u_light_ambient1 * u_material_ambient;"
                "diffuse += u_light_diffuse1 * u_material_diffuse * t_normal_dot_light_direction1;"
                "specular += u_light_specular1 * u_material_specular * pow(max(dot(reflection_vector1, viewer_vector), 0.0f), u_material_shiness);"

                "phong_ads_light = ambient + diffuse + specular;"
            "}"
            "else"
            "{"
               "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
            "}"

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

    glShaderSource(pep_TwoSteadyLights_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_TwoSteadyLights_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_TwoSteadyLights_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TwoSteadyLights_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TwoSteadyLights_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return;
            }
        }
    }
    printf("%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    printf("%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_TwoSteadyLights_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec3 phong_ads_light;"
        "uniform int u_key_L_pressed;"
        "out vec4 FragColor;"

        "void main(void)"
        "{"
            "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

    glShaderSource(pep_TwoSteadyLights_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_TwoSteadyLights_gFragmentShaderObject);

    glGetShaderiv(pep_TwoSteadyLights_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TwoSteadyLights_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TwoSteadyLights_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                printf("%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
               return;
            }
        }
    }
    printf("%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    printf("%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_TwoSteadyLights_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_TwoSteadyLights_gShaderProgramObject, pep_TwoSteadyLights_gVertexShaderObject);
    glAttachShader(pep_TwoSteadyLights_gShaderProgramObject, pep_TwoSteadyLights_gFragmentShaderObject);

    glBindAttribLocation(pep_TwoSteadyLights_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_TwoSteadyLights_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

    printf("%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_TwoSteadyLights_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_TwoSteadyLights_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_TwoSteadyLights_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_TwoSteadyLights_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                printf("%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return;
            }
        }
    }

    printf("%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    //pep_TwoSteadyLights_mvpUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_mvp_matrix");
    pep_TwoSteadyLights_modelMatrixUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_model_matrix");
    pep_TwoSteadyLights_viewMatrixUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_view_matrix");
    pep_TwoSteadyLights_projectionMatrixUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_projection_matrix");

    pep_TwoSteadyLights_lightAmbient0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_ambient0");
    pep_TwoSteadyLights_lightDiffuse0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_diffuse0");
    pep_TwoSteadyLights_lightSpecular0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_specular0");
    pep_TwoSteadyLights_lightPosition0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_position0");

    pep_TwoSteadyLights_lightAmbient0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_ambient1");
    pep_TwoSteadyLights_lightDiffuse0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_diffuse1");
    pep_TwoSteadyLights_lightSpecular0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_specular1");
    pep_TwoSteadyLights_lightPosition0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_position1");

    pep_TwoSteadyLights_materialAmbientUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_ambient");
    pep_TwoSteadyLights_materialDiffuseUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_diffuse");
    pep_TwoSteadyLights_materialSpecularUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_specular");
    pep_TwoSteadyLights_materialShinessUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_shiness");

    pep_TwoSteadyLights_keyLPressedUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_key_L_pressed");

    glGenVertexArrays(1, &pep_TwoSteadyLights_vao);
    glBindVertexArray(pep_TwoSteadyLights_vao);

    glGenBuffers(1, &pep_TwoSteadyLights_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoSteadyLights_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_TwoSteadyLights_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoSteadyLights_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

    pep_perspectiveProjectionMatrix = mat4::identity();
    
	ReSize(pep_giWindowWidth, pep_giWindowHeight);

    printf("%s : %d : %s = Iniitialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
	return;
}

void ReSize(int width, int height)
{
	if(0 == height)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    pep_perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
	return;
}

void Draw(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(pep_TwoSteadyLights_gShaderProgramObject);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;

    //
    // Cube
    //
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    viewMatrix = translate(0.0f, 0.0f, -6.0f) * rotate(pep_TwoSteadyLights_PyramidRotation, 0.0f, 1.0f, 0.0f);;
    projectionMatrix = pep_perspectiveProjectionMatrix;

    glUniformMatrix4fv(pep_TwoSteadyLights_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_TwoSteadyLights_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_TwoSteadyLights_projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);
 
    if (pep_TwoSteadyLights_bIsLightEnable)
    {
        glUniform1i(pep_TwoSteadyLights_keyLPressedUniform, 1);
        glUniform3fv(pep_TwoSteadyLights_lightAmbient0Uniform0, 1, pep_TwoSteadyLights_lightAmbient0);
        glUniform3fv(pep_TwoSteadyLights_lightDiffuse0Uniform0, 1, pep_TwoSteadyLights_lightDiffuse0);
        glUniform3fv(pep_TwoSteadyLights_lightSpecular0Uniform0, 1, pep_TwoSteadyLights_lightSpecular0);
        glUniform4fv(pep_TwoSteadyLights_lightPosition0Uniform0, 1, pep_TwoSteadyLights_lightPosition0);

        glUniform3fv(pep_TwoSteadyLights_lightAmbient0Uniform1, 1, pep_TwoSteadyLights_lightAmbient1);
        glUniform3fv(pep_TwoSteadyLights_lightDiffuse0Uniform1, 1, pep_TwoSteadyLights_lightDiffuse1);
        glUniform3fv(pep_TwoSteadyLights_lightSpecular0Uniform1, 1, pep_TwoSteadyLights_lightSpecular1);
        glUniform4fv(pep_TwoSteadyLights_lightPosition0Uniform1, 1, pep_TwoSteadyLights_lightPosition1);

        glUniform3fv(pep_TwoSteadyLights_materialAmbientUniform, 1, pep_TwoSteadyLights_materialAmbient);
        glUniform3fv(pep_TwoSteadyLights_materialDiffuseUniform, 1, pep_TwoSteadyLights_materialDiffuse);
        glUniform3fv(pep_TwoSteadyLights_materialSpecularUniform, 1, pep_TwoSteadyLights_materialSpecular);
        glUniform1f(pep_TwoSteadyLights_materialShinessUniform, pep_TwoSteadyLights_materialShiness);
    }
    else
    {
        glUniform1i(pep_TwoSteadyLights_keyLPressedUniform, 0);
    }

    glBindVertexArray(pep_TwoSteadyLights_vao);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    glUseProgram(0);

	glXSwapBuffers(pep_gpDisplay, pep_gWindow);

	return;
}

void Uninitialize(void)
{
	
	if (pep_TwoSteadyLights_vbo_normal)
    {
        glDeleteBuffers(1, &pep_TwoSteadyLights_vbo_normal);
        pep_TwoSteadyLights_vbo_normal = 0;
    }

    if (pep_TwoSteadyLights_vbo_position)
    {
        glDeleteBuffers(1, &pep_TwoSteadyLights_vbo_position);
        pep_TwoSteadyLights_vbo_position = 0;
    }

    if (pep_TwoSteadyLights_vao)
    {
        glDeleteVertexArrays(1, &pep_TwoSteadyLights_vao);
        pep_TwoSteadyLights_vao = 0;
    }

    if (pep_TwoSteadyLights_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_TwoSteadyLights_gShaderProgramObject);

        glGetProgramiv(pep_TwoSteadyLights_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_TwoSteadyLights_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                printf("%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_TwoSteadyLights_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        printf("%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_TwoSteadyLights_gShaderProgramObject);
        pep_TwoSteadyLights_gShaderProgramObject = 0;

        glUseProgram(0);
    }
	
	GLXContext currentGlxContext = glXGetCurrentContext();
	if((NULL != currentGlxContext) && (currentGlxContext == pep_gGlxContext))
	{
		glXMakeCurrent(pep_gpDisplay, 0, 0);
	}

	if(pep_gGlxContext)
	{
		glXDestroyContext(pep_gpDisplay, pep_gGlxContext);
	}

	if(pep_gWindow)
	{
		XDestroyWindow(pep_gpDisplay, pep_gWindow);
	}

	if(pep_gpXVisualInfo)
	{
		free(pep_gpXVisualInfo);
		pep_gpXVisualInfo = NULL;
	}

	if(pep_gpDisplay)
	{
		XCloseDisplay(pep_gpDisplay);
		pep_gpDisplay = NULL;
	}

	return;
}

void ToggleFullScreen(void)
{
	// variable
	Atom wm_state;
	Atom fullScreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = pep_gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.data.l[0] = pep_bFullScreen ? 0 : 1;
	xev.xclient.format = 32;

	fullScreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullScreen;

	XSendEvent(pep_gpDisplay,
				RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);

	return;
}

void Update(void)
{
    if(false == pep_TwoSteadyLights_bIsAnimation)
    {
        return;
    }
    
    pep_TwoSteadyLights_PyramidRotation += 0.5f;
    if (pep_TwoSteadyLights_PyramidRotation > 360.0f)
    {
        pep_TwoSteadyLights_PyramidRotation = 0.0f;
    }
    
    return;
}
