#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "vmath.h"
#include "Sphere.h"

using namespace std;
using namespace vmath;

bool pep_bFullScreen = false;

Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;
Colormap pep_gColormap;
Window pep_gWindow;

int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;

static GLXContext pep_gGlxContext;

//PP Related Code
GLuint pep_PerVertexLighting_gVertexShaderObject;
GLuint pep_PerVertexLighting_gShaderProgramObject;
GLuint pep_PerVertexLighting_gFragmentShaderObject;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXTCOORD0
};

GLuint vao_sphere;
GLuint vbo_sphere_vertex;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_element;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;

// UNIFORMS
GLuint pep_PerVertexLighting_modelMatrixUniform;
GLuint pep_PerVertexLighting_viewMatrixUniform;
GLuint pep_PerVertexLighting_projectionMatrixUniform;
GLuint pep_PerVertexLighting_lightAmbientUniform;
GLuint pep_PerVertexLighting_lightDiffuseUniform;
GLuint pep_PerVertexLighting_lightSpecularUniform;
GLuint pep_PerVertexLighting_materialAmbientUniform;
GLuint pep_PerVertexLighting_materialDiffuseUniform;
GLuint pep_PerVertexLighting_materialSpecularUniform;
GLuint pep_PerVertexLighting_materialShinessUniform;
GLuint pep_PerVertexLighting_lightPositionUniform;
GLuint pep_PerVertexLighting_keyLPressedUniform;

//
// Application Variables Associated With Uniforms
float pep_PerVertexLighting_lightAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_PerVertexLighting_lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_lightPosition[4] = {100.0f, 100.0f, 100.0f, 1.0f};

float pep_PerVertexLighting_materialAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_PerVertexLighting_materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_materialShiness = 50.0f;  // Also Try 128.0f

bool pep_PerVertexLighting_bIsLightEnable = true;

mat4 pep_perspectiveProjectionMatrix;

// Changes Related To Get FBConfig
typedef GLXContext (*procpep_glXCreateContextAttribsARBProc)
						(Display *,
						GLXFBConfig,
						GLXContext,
						Bool,
						const int *);

procpep_glXCreateContextAttribsARBProc pep_glXCreateContextAttribsARBProc;

GLXFBConfig pep_gGlxFbConfig;

int main()
{
	//function protoypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Initialize(void);
	void ReSize(int, int);
	void Draw(void);
    void Update(void);

	// variable declarations
	bool bDone = false;

	int winWidth = pep_giWindowWidth;
	int winHeight = pep_giWindowHeight;

	// code
	CreateWindow();
	Initialize();

	// Game Loop
	XEvent event;
	KeySym keysym;

	while(false == bDone)
	{
		while(XPending(pep_gpDisplay))
		{
			XNextEvent(pep_gpDisplay, &event);

			switch(event.type)
			{
				case MapNotify:
				break;

				case KeyPress:
				{
					keysym = XkbKeycodeToKeysym(pep_gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
						bDone = true;
						break;

						case XK_F:
						case XK_f:
						{
							if(false == pep_bFullScreen)
							{
								ToggleFullScreen();
								pep_bFullScreen = true;
							}
							else
							{
								ToggleFullScreen();
								pep_bFullScreen = false;
							}
						}
						break;
                        
                        case XK_L:
                        case XK_l:
                        {
                            if(false == pep_PerVertexLighting_bIsLightEnable)
                            {
                                pep_PerVertexLighting_bIsLightEnable = true;
                            }
                            else
                            {
                                pep_PerVertexLighting_bIsLightEnable = false;
                            }
                                
                        }
                        break;

						default:
						break;
					} // Switch Block End
				} // Case KeyPress
				break;

				case ButtonPress:
				{
					switch(event.xbutton.button)
					{
						case 1:
						break;

						case 2:
						break;

						case 3:
						break;

						case 4:
						break;

						case 5:
						break;
					}
				} // Case ButtonPress
				break;

				case MotionNotify:
				break;

				case ConfigureNotify:
				{
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;

					ReSize(winWidth, winHeight);
				}
				break;

				case Expose:
				break;

				case DestroyNotify:
				break;

				case 33:
				bDone = true;
				break;

				default:
				break;

			} // Switch Block End
		} // Inner While Loop End

		Update();
		Draw();
	} // Outer While Loop End

	Uninitialize();

	return 0;
}

void CreateWindow(void)
{
	// function declarations
	void Uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	// FBConfig Related Changes
	GLXFBConfig *pGlxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFbConfigs = 0;

	// Initialize Framebuffers Attributes
	static int frameBufferAttributes[] =
	{
		GLX_X_RENDERABLE, True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_STENCIL_SIZE, 8,
		None
	};

	pep_gpDisplay = XOpenDisplay(NULL);
	if(NULL == pep_gpDisplay)
	{
		printf("Error : XOpenDisplay Failed.\nExitting Now...\n\n");
		Uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(pep_gpDisplay);

	//	Algorithm To Get FBConfig
	//  
	// 1. Get Array Of All Available FBConfigs : glXChooseFbConfig
	// 2. Select Best From All : Based On Sampler and SampleBuffer
	// 3. Define High Quality H/W Rendering Context Frame Buffer To Get
	// 4. Get XVisualInfo From Best FBConfig : glXGetVisualFromConfig
	//
	// FBConfig Related Changes
	// Retrieve All FBConfigs Driver Has.
	// pGlxFbConfigs : This Is An Array Of FBConfig(s)
	// iNumberOfFbConfigs : If Multiple FBConfig(s) Present, To Get The Number We Are Using This 
	pGlxFbConfigs = glXChooseFBConfig(pep_gpDisplay, defaultScreen, frameBufferAttributes, &iNumberOfFbConfigs);

	int sampleBuffers, samples;
	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples = 999;

	for (int i = 0; i < iNumberOfFbConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(pep_gpDisplay, pGlxFbConfigs[i]);

		// For Each Optained frame FBConfig, Get temp VisualInfo. Its Use To Just Check The Capability Of NExt Call Can Be Made Or Not.
		if(NULL != pTempXVisualInfo)
		{
			// Get Number of Sample Buffers From Respective FBConfig
			glXGetFBConfigAttrib(pep_gpDisplay, pGlxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			// Get Number Of Samples From Respective FBConfig
			// Sampler => Breaking "Pixel" Into Small Parts 
			glXGetFBConfigAttrib(pep_gpDisplay, pGlxFbConfigs[i], GLX_SAMPLES, &samples);

			// More The Number Of Samples And Sample Buffers More The Eligible FBConfig, So Do The Comparision
			// Ge best FBConfig
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			// Get Worst
			if(worstFrameBufferConfig < 999 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		} // If End

		XFree(pTempXVisualInfo);
	} // For End

	// Now Assigned Beest FBConfig
	bestGlxFbConfig = pGlxFbConfigs[bestFrameBufferConfig];

	// Assign The Best FBConfig To Global Variable
	pep_gGlxFbConfig = bestGlxFbConfig;

	// Free The Obtain GLXFBConfig Array - Not Required

	// Get XVisualInfo From Best FBConfig
	pep_gpXVisualInfo = glXGetVisualFromFBConfig(pep_gpDisplay, bestGlxFbConfig);
	if(NULL == pep_gpXVisualInfo)
	{
		printf("Error : GLXGetVisualFromFBConfig Failed.\n Exitting Now...\n\n");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;

	winAttribs.colormap = XCreateColormap(pep_gpDisplay,
											RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
											pep_gpXVisualInfo->visual,
											AllocNone);
	pep_gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(pep_gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	pep_gWindow = XCreateWindow(pep_gpDisplay,
							RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
							0,
							0,
							pep_giWindowWidth, pep_giWindowHeight,
							0,
							pep_gpXVisualInfo->depth,
							InputOutput,
							pep_gpXVisualInfo->visual,
							styleMask,
							&winAttribs);
	if(!pep_gWindow)
	{
		printf("Error : XCreateWindow Failed.\nExitting Now");
		Uninitialize();
		exit(1);
	}

	XStoreName(pep_gpDisplay, pep_gWindow, "PP - Per Vertex Lighting");
	Atom windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(pep_gpDisplay, pep_gWindow, &windowManagerDelete, 1);

	XMapWindow(pep_gpDisplay, pep_gWindow);

	return;
}

void Initialize(void)
{
	// function declarations
	void ReSize(int, int);
	void Uninitialize(void);

	// code

	pep_glXCreateContextAttribsARBProc = (procpep_glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	if(NULL == pep_glXCreateContextAttribsARBProc)
	{
		Uninitialize();
		exit(1);
	}

	GLint attribs[] = 
	{
		// Latest OpenGL Context 4.5
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	pep_gGlxContext = pep_glXCreateContextAttribsARBProc(
		pep_gpDisplay,
		pep_gGlxFbConfig,
		0, // If Multiple Monitor Forms One Screen, Then Graphics Context Has To Be Shared. As We Are Not Using Multiple Monitor Hence Specifiing Zero.
		True, // We Are Interested In H/W Rendering Context. Hence True
			  // If Linux Installed In VM. Then We Are Interested In S/W Rendering Context. For S/W Context => Specify False
		attribs);
	if(NULL == pep_gGlxContext)
	{
		// If Not Obtained Highest Context, Specify The Lowest One => it Will Give/ Return The Highest One Which Is known To Grpahics Driver
		printf("%s : %s : %d = Failed To Obtained Highest H/W Rendering Context.\nAsking For Nearest Highest Context\n", __FILE__, __FUNCTION__, __LINE__);

		// Not Full H/W But To Get Nearest High Context
		GLint attribs[] = 
		{
			// OpenGL Context 1.0
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			None
		};

		pep_gGlxContext = pep_glXCreateContextAttribsARBProc(pep_gpDisplay, pep_gGlxFbConfig, 0, True, attribs);
	}

	// Check Obtained Context Is Really H/W Rendering Context Or Not
	if(!glXIsDirect(pep_gpDisplay, pep_gGlxContext))
	{
		printf("%s : %s : %d = Obtained Context Is Not H/W Rendering Context\n", __FILE__, __FUNCTION__, __LINE__);
	
	}
	else
	{
		printf("%s : %s : %d = Obtained Context Is H/W Rendering Context\n", __FILE__, __FUNCTION__, __LINE__);	
	}

	// Make Obtain Context As Current OpenGL Context
	glXMakeCurrent(pep_gpDisplay, pep_gWindow, pep_gGlxContext);

	GLenum result;

	// Enable Extensions
	result = glewInit();
	if(GLEW_OK != result)
	{
		Uninitialize();
		exit(1);
	}

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
                      sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();
    
	// Create Shader Object
	pep_PerVertexLighting_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if(0 == pep_PerVertexLighting_gVertexShaderObject)
    {
        printf("\n glCreateShader:VertexShader Failed \n");
		Uninitialize();
		exit(1);
    }
    
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec3 u_light_ambient;"
        "uniform vec3 u_light_diffuse;"
        "uniform vec3 u_light_specular;"
        "uniform vec4 u_light_position;"
        "uniform vec3 u_material_ambient;"
        "uniform vec3 u_material_diffuse;"
        "uniform vec3 u_material_specular;"
        "uniform float u_material_shiness;"
        "uniform int u_key_L_pressed;"

        "out vec3 phong_ads_light;"

        "void main(void)"
        "{"
            "if(1 == u_key_L_pressed)"
            "{"
                "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
                "vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
                "vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));"
                "float t_normal_dot_light_direction = max(dot(light_direction, t_normal), 0.0f);"
                "vec3 reflection_vector = reflect(-light_direction, t_normal);"
                "vec3 viewer_vector = normalize(vec3(-eye_coordinates));"
                "vec3 ambient = u_light_ambient * u_material_ambient;"
                "vec3 diffuse = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction;"
                "vec3 specular = u_light_specular * u_material_specular * pow(max(dot(reflection_vector, viewer_vector), 0.0f), u_material_shiness);"

                "phong_ads_light = ambient + diffuse + specular;"
            "}"
            "else"
            "{"
               "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
            "}"

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

	// Specify Above Source Code To Vertex Shader Object
	glShaderSource(pep_PerVertexLighting_gVertexShaderObject,
					1,
					(const GLchar **)&vertexShaderSourceCode,
					NULL);

	// Compile The Vertex Shader
	glCompileShader(pep_PerVertexLighting_gVertexShaderObject);
    
    GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	// Error Checking
	glGetShaderiv(pep_PerVertexLighting_gVertexShaderObject,	GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_PerVertexLighting_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if(NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_PerVertexLighting_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Error: Vertex shader - \n\t%s\n", szInfoLog);
				free(szInfoLog);
			}
		}

		Uninitialize();
		exit(1);
	}
	printf("%s : %d : %s = glCompileShader Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
    iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_PerVertexLighting_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if(0 == pep_PerVertexLighting_gFragmentShaderObject)
    {
        printf("\n glCreateShader:Fragment Failed \n");
		Uninitialize();
		exit(1);
    }
    
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core"
        "\n"
        "in vec3 phong_ads_light;"
        "uniform int u_key_L_pressed;"
        "out vec4 FragColor;"

        "void main(void)"
        "{"
            "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

	// Specify Above Source Code To Vertex Shader Object
	glShaderSource(pep_PerVertexLighting_gFragmentShaderObject,
					1,
					(const GLchar **)&fragmentShaderSourceCode,
					NULL);

	// Compile The Vertex Shader
	glCompileShader(pep_PerVertexLighting_gFragmentShaderObject);
    
    glGetShaderiv(pep_PerVertexLighting_gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_PerVertexLighting_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if(NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_PerVertexLighting_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Error: Fragment Shader: \n\t%s\n", szInfoLog);

				free(szInfoLog);
			}
		}

		Uninitialize();
		exit(1);
	}
	printf("%s : %d : %s = glCompileShader Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	// Create Shader Program Object
	pep_PerVertexLighting_gShaderProgramObject = glCreateProgram();

	// Attach Vertex Shader To Shader Program
	glAttachShader(pep_PerVertexLighting_gShaderProgramObject, pep_PerVertexLighting_gVertexShaderObject);
	glAttachShader(pep_PerVertexLighting_gShaderProgramObject, pep_PerVertexLighting_gFragmentShaderObject);

    glBindAttribLocation(pep_PerVertexLighting_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_PerVertexLighting_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
    
	// Link The Shader Program
	glLinkProgram(pep_PerVertexLighting_gShaderProgramObject);
    
    GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_PerVertexLighting_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_PerVertexLighting_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *) malloc(iInfoLogLength);
			if(NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_PerVertexLighting_gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				free(szInfoLog);
			}
		}

		Uninitialize();
		exit(1);
	}
	printf("%s : %d : %s = glLinkProgram Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
    pep_PerVertexLighting_modelMatrixUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_model_matrix");
    pep_PerVertexLighting_viewMatrixUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_view_matrix");
    pep_PerVertexLighting_projectionMatrixUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_projection_matrix");

    pep_PerVertexLighting_lightAmbientUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_ambient");
    pep_PerVertexLighting_lightDiffuseUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_diffuse");
    pep_PerVertexLighting_lightSpecularUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_specular");
    pep_PerVertexLighting_lightPositionUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_position");

    pep_PerVertexLighting_materialAmbientUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_ambient");
    pep_PerVertexLighting_materialDiffuseUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_diffuse");
    pep_PerVertexLighting_materialSpecularUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_specular");
    pep_PerVertexLighting_materialShinessUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_shiness");

    pep_PerVertexLighting_keyLPressedUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_key_L_pressed");

	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);
    
    glGenBuffers(1, &vbo_sphere_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_vertex);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_sphere_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

    pep_perspectiveProjectionMatrix = mat4::identity();
    
	ReSize(pep_giWindowWidth, pep_giWindowHeight);

    printf("%s : %d : %s = Iniitialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
	return;
}

void ReSize(int width, int height)
{
	if(0 == height)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    pep_perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
	return;
}

void Draw(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(pep_PerVertexLighting_gShaderProgramObject);
    
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;

    //
    // Cube
    //
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    viewMatrix = translate(0.0f, 0.0f, -3.0f);
    projectionMatrix = pep_perspectiveProjectionMatrix;

    glUniformMatrix4fv(pep_PerVertexLighting_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_PerVertexLighting_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_PerVertexLighting_projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);
 
    if (pep_PerVertexLighting_bIsLightEnable)
    {
        glUniform1i(pep_PerVertexLighting_keyLPressedUniform, 1);
        glUniform3fv(pep_PerVertexLighting_lightAmbientUniform, 1, pep_PerVertexLighting_lightAmbient);
        glUniform3fv(pep_PerVertexLighting_lightDiffuseUniform, 1, pep_PerVertexLighting_lightDiffuse);
        glUniform3fv(pep_PerVertexLighting_lightSpecularUniform, 1, pep_PerVertexLighting_lightSpecular);
        glUniform4fv(pep_PerVertexLighting_lightPositionUniform, 1, pep_PerVertexLighting_lightPosition);
        glUniform3fv(pep_PerVertexLighting_materialAmbientUniform, 1, pep_PerVertexLighting_materialAmbient);
        glUniform3fv(pep_PerVertexLighting_materialDiffuseUniform, 1, pep_PerVertexLighting_materialDiffuse);
        glUniform3fv(pep_PerVertexLighting_materialSpecularUniform, 1, pep_PerVertexLighting_materialSpecular);
        glUniform1f(pep_PerVertexLighting_materialShinessUniform, pep_PerVertexLighting_materialShiness);
    }
    else
    {
        glUniform1i(pep_PerVertexLighting_keyLPressedUniform, 0);
    }

    glBindVertexArray(vao_sphere);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);

    glUseProgram(0);

	glXSwapBuffers(pep_gpDisplay, pep_gWindow);

	return;
}

void Uninitialize(void)
{
	
	if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    if (vbo_sphere_vertex)
    {
        glDeleteBuffers(1, &vbo_sphere_vertex);
        vbo_sphere_vertex = 0;
    }

    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

	if(pep_PerVertexLighting_gShaderProgramObject)
	{
		GLsizei shaderNumber;
		GLsizei shaderCount;

		glUseProgram(pep_PerVertexLighting_gShaderProgramObject);

		glGetProgramiv(pep_PerVertexLighting_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if(pShaders)
		{
			glGetAttachedShaders(pep_PerVertexLighting_gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_PerVertexLighting_gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_PerVertexLighting_gShaderProgramObject);
		pep_PerVertexLighting_gShaderProgramObject = 0;

		glUseProgram(0);
	}
	
	GLXContext currentGlxContext = glXGetCurrentContext();
	if((NULL != currentGlxContext) && (currentGlxContext == pep_gGlxContext))
	{
		glXMakeCurrent(pep_gpDisplay, 0, 0);
	}

	if(pep_gGlxContext)
	{
		glXDestroyContext(pep_gpDisplay, pep_gGlxContext);
	}

	if(pep_gWindow)
	{
		XDestroyWindow(pep_gpDisplay, pep_gWindow);
	}

	if(pep_gpXVisualInfo)
	{
		free(pep_gpXVisualInfo);
		pep_gpXVisualInfo = NULL;
	}

	if(pep_gpDisplay)
	{
		XCloseDisplay(pep_gpDisplay);
		pep_gpDisplay = NULL;
	}

	return;
}

void ToggleFullScreen(void)
{
	// variable
	Atom wm_state;
	Atom fullScreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = pep_gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.data.l[0] = pep_bFullScreen ? 0 : 1;
	xev.xclient.format = 32;

	fullScreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullScreen;

	XSendEvent(pep_gpDisplay,
				RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);

	return;
}

void Update(void)
{
    return;
}
