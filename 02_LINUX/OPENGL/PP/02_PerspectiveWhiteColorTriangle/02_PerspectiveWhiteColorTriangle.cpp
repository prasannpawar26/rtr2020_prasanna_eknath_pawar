#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

bool pep_bFullScreen = false;

Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;

static GLXContext pep_gGlxContext;

//PP Related Code
GLuint pep_gVertexShaderObject;
GLuint pep_gShaderProgramObject;
GLuint pep_gFragmentShaderObject;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXTCOORD0
};

GLuint vao;
GLuint vbo;
GLuint mvpUniform;

mat4 perspectiveProjectionMatrix;

// Changes Related To Get FBConfig
typedef GLXContext (*procpep_glXCreateContextAttribsARBProc)
						(Display *,
						GLXFBConfig,
						GLXContext,
						Bool,
						const int *);

procpep_glXCreateContextAttribsARBProc pep_glXCreateContextAttribsARBProc;

GLXFBConfig pep_gGlxFbConfig;

int main()
{
	//function protoypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void Uninitialize(void);
	void Initialize(void);
	void ReSize(int, int);
	void Draw(void);

	// variable declarations
	bool bDone = false;

	int winWidth = pep_giWindowWidth;
	int winHeight = pep_giWindowHeight;

	// code
	CreateWindow();
	Initialize();

	// Game Loop
	XEvent event;
	KeySym keysym;

	while(false == bDone)
	{
		while(XPending(pep_gpDisplay))
		{
			XNextEvent(pep_gpDisplay, &event);

			switch(event.type)
			{
				case MapNotify:
				break;

				case KeyPress:
				{
					keysym = XkbKeycodeToKeysym(pep_gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
						bDone = true;
						break;

						case XK_F:
						case XK_f:
						{
							if(false == pep_bFullScreen)
							{
								ToggleFullScreen();
								pep_bFullScreen = true;
							}
							else
							{
								ToggleFullScreen();
								pep_bFullScreen = false;
							}
						}
						break;

						default:
						break;
					} // Switch Block End
				} // Case KeyPress
				break;

				case ButtonPress:
				{
					switch(event.xbutton.button)
					{
						case 1:
						break;

						case 2:
						break;

						case 3:
						break;

						case 4:
						break;

						case 5:
						break;
					}
				} // Case ButtonPress
				break;

				case MotionNotify:
				break;

				case ConfigureNotify:
				{
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;

					ReSize(winWidth, winHeight);
				}
				break;

				case Expose:
				break;

				case DestroyNotify:
				break;

				case 33:
				bDone = true;
				break;

				default:
				break;

			} // Switch Block End
		} // Inner While Loop End

		Draw();
	} // Outer While Loop End

	Uninitialize();

	return 0;
}

void CreateWindow(void)
{
	// function declarations
	void Uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	// FBConfig Related Changes
	GLXFBConfig *pGlxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumberOfFbConfigs = 0;

	// Initialize Framebuffers Attributes
	static int frameBufferAttributes[] =
	{
		GLX_X_RENDERABLE, True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_STENCIL_SIZE, 8,
		None
	};

	pep_gpDisplay = XOpenDisplay(NULL);
	if(NULL == pep_gpDisplay)
	{
		printf("Error : XOpenDisplay Failed.\nExitting Now...\n\n");
		Uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(pep_gpDisplay);

	//	Algorithm To Get FBConfig
	//  
	// 1. Get Array Of All Available FBConfigs : glXChooseFbConfig
	// 2. Select Best From All : Based On Sampler and SampleBuffer
	// 3. Define High Quality H/W Rendering Context Frame Buffer To Get
	// 4. Get XVisualInfo From Best FBConfig : glXGetVisualFromConfig
	//
	// FBConfig Related Changes
	// Retrieve All FBConfigs Driver Has.
	// pGlxFbConfigs : This Is An Array Of FBConfig(s)
	// iNumberOfFbConfigs : If Multiple FBConfig(s) Present, To Get The Number We Are Using This 
	pGlxFbConfigs = glXChooseFBConfig(pep_gpDisplay, defaultScreen, frameBufferAttributes, &iNumberOfFbConfigs);

	int sampleBuffers, samples;
	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int worstFrameBufferConfig = -1;
	int worstNumberOfSamples = 999;

	for (int i = 0; i < iNumberOfFbConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(pep_gpDisplay, pGlxFbConfigs[i]);

		// For Each Optained frame FBConfig, Get temp VisualInfo. Its Use To Just Check The Capability Of NExt Call Can Be Made Or Not.
		if(NULL != pTempXVisualInfo)
		{
			// Get Number of Sample Buffers From Respective FBConfig
			glXGetFBConfigAttrib(pep_gpDisplay, pGlxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);

			// Get Number Of Samples From Respective FBConfig
			// Sampler => Breaking "Pixel" Into Small Parts 
			glXGetFBConfigAttrib(pep_gpDisplay, pGlxFbConfigs[i], GLX_SAMPLES, &samples);

			// More The Number Of Samples And Sample Buffers More The Eligible FBConfig, So Do The Comparision
			// Ge best FBConfig
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			// Get Worst
			if(worstFrameBufferConfig < 999 || !sampleBuffers || samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = samples;
			}
		} // If End

		XFree(pTempXVisualInfo);
	} // For End

	// Now Assigned Beest FBConfig
	bestGlxFbConfig = pGlxFbConfigs[bestFrameBufferConfig];

	// Assign The Best FBConfig To Global Variable
	pep_gGlxFbConfig = bestGlxFbConfig;

	// Free The Obtain GLXFBConfig Array - Not Required

	// Get XVisualInfo From Best FBConfig
	pep_gpXVisualInfo = glXGetVisualFromFBConfig(pep_gpDisplay, bestGlxFbConfig);
	if(NULL == pep_gpXVisualInfo)
	{
		printf("Error : GLXGetVisualFromFBConfig Failed.\n Exitting Now...\n\n");
		Uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;

	winAttribs.colormap = XCreateColormap(pep_gpDisplay,
											RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
											pep_gpXVisualInfo->visual,
											AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(pep_gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(pep_gpDisplay,
							RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
							0,
							0,
							pep_giWindowWidth, pep_giWindowHeight,
							0,
							pep_gpXVisualInfo->depth,
							InputOutput,
							pep_gpXVisualInfo->visual,
							styleMask,
							&winAttribs);
	if(!gWindow)
	{
		printf("Error : XCreateWindow Failed.\nExitting Now");
		Uninitialize();
		exit(1);
	}

	XStoreName(pep_gpDisplay, gWindow, "PP - Perspective");
	Atom windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(pep_gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(pep_gpDisplay, gWindow);

	return;
}

void Initialize(void)
{
	// function declarations
	void ReSize(int, int);
	void Uninitialize(void);

	// code

	pep_glXCreateContextAttribsARBProc = (procpep_glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	if(NULL == pep_glXCreateContextAttribsARBProc)
	{
		Uninitialize();
		exit(1);
	}

	GLint attribs[] = 
	{
		// Latest OpenGL Context 4.5
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	pep_gGlxContext = pep_glXCreateContextAttribsARBProc(
		pep_gpDisplay,
		pep_gGlxFbConfig,
		0, // If Multiple Monitor Forms One Screen, Then Graphics Context Has To Be Shared. As We Are Not Using Multiple Monitor Hence Specifiing Zero.
		True, // We Are Interested In H/W Rendering Context. Hence True
			  // If Linux Installed In VM. Then We Are Interested In S/W Rendering Context. For S/W Context => Specify False
		attribs);
	if(NULL == pep_gGlxContext)
	{
		// If Not Obtained Highest Context, Specify The Lowest One => it Will Give/ Return The Highest One Which Is known To Grpahics Driver
		printf("%s : %s : %d = Failed To Obtained Highest H/W Rendering Context.\nAsking For Nearest Highest Context\n", __FILE__, __FUNCTION__, __LINE__);

		// Not Full H/W But To Get Nearest High Context
		GLint attribs[] = 
		{
			// OpenGL Context 1.0
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			None
		};

		pep_gGlxContext = pep_glXCreateContextAttribsARBProc(pep_gpDisplay, pep_gGlxFbConfig, 0, True, attribs);
	}

	// Check Obtained Context Is Really H/W Rendering Context Or Not
	if(!glXIsDirect(pep_gpDisplay, pep_gGlxContext))
	{
		printf("%s : %s : %d = Obtained Context Is Not H/W Rendering Context\n", __FILE__, __FUNCTION__, __LINE__);
	
	}
	else
	{
		printf("%s : %s : %d = Obtained Context Is H/W Rendering Context\n", __FILE__, __FUNCTION__, __LINE__);	
	}

	// Make Obtain Context As Current OpenGL Context
	glXMakeCurrent(pep_gpDisplay, gWindow, pep_gGlxContext);

	GLenum result;

	// Enable Extensions
	result = glewInit();
	if(GLEW_OK != result)
	{
		Uninitialize();
		exit(1);
	}

	// Create Shader Object
	pep_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if(0 == pep_gVertexShaderObject)
    {
        printf("\n glCreateShader:VertexShader Failed \n");
		Uninitialize();
		exit(1);
    }
    
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		
		"void main(void)" \
		"{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	// Specify Above Source Code To Vertex Shader Object
	glShaderSource(pep_gVertexShaderObject,
					1,
					(const GLchar **)&vertexShaderSourceCode,
					NULL);

	// Compile The Vertex Shader
	glCompileShader(pep_gVertexShaderObject);
    
    GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	// Error Checking
	glGetShaderiv(pep_gVertexShaderObject,	GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if(NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Error: Vertex shader - \n\t%s\n", szInfoLog);
				free(szInfoLog);
			}
		}

		Uninitialize();
		exit(1);
	}
	printf("%s : %d : %s = glCompileShader Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
    iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if(0 == pep_gFragmentShaderObject)
    {
        printf("\n glCreateShader:Fragment Failed \n");
		Uninitialize();
		exit(1);
    }
    
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		
		"out vec4 FragColor;" \
		
		"void main(void)" \
		"{" \
            "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}";

	// Specify Above Source Code To Vertex Shader Object
	glShaderSource(pep_gFragmentShaderObject,
					1,
					(const GLchar **)&fragmentShaderSourceCode,
					NULL);

	// Compile The Vertex Shader
	glCompileShader(pep_gFragmentShaderObject);
    
    glGetShaderiv(pep_gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if(GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if(NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Error: Fragment Shader: \n\t%s\n", szInfoLog);

				free(szInfoLog);
			}
		}

		Uninitialize();
		exit(1);
	}
	printf("%s : %d : %s = glCompileShader Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	// Create Shader Program Object
	pep_gShaderProgramObject = glCreateProgram();

	// Attach Vertex Shader To Shader Program
	glAttachShader(pep_gShaderProgramObject, pep_gVertexShaderObject);
	glAttachShader(pep_gShaderProgramObject, pep_gFragmentShaderObject);

    glBindAttribLocation(pep_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    
	// Link The Shader Program
	glLinkProgram(pep_gShaderProgramObject);
    
    GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if(GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *) malloc(iInfoLogLength);
			if(NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				free(szInfoLog);
			}
		}

		Uninitialize();
		exit(1);
	}
	printf("%s : %d : %s = glLinkProgram Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
    mvpUniform = glGetUniformLocation(pep_gShaderProgramObject, "u_mvp_matrix");

    const GLfloat triangleVertices[] = 
    {   0.0f, 1.0f, 0.0f, 
        -1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
    };

	glGenVertexArrays(1, &vao);

	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
    

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

    perspectiveProjectionMatrix = mat4::identity();
    
	ReSize(pep_giWindowWidth, pep_giWindowHeight);

    printf("%s : %d : %s = Iniitialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);
    
	return;
}

void ReSize(int width, int height)
{
	if(0 == height)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
	return;
}

void Draw(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(pep_gShaderProgramObject);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(pep_gpDisplay, gWindow);

	return;
}

void Uninitialize(void)
{
    if(vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	if(vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if(pep_gShaderProgramObject)
	{
		GLsizei shaderNumber;
		GLsizei shaderCount;

		glUseProgram(pep_gShaderProgramObject);

		glGetProgramiv(pep_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if(pShaders)
		{
			glGetAttachedShaders(pep_gShaderProgramObject, shaderCount, &shaderCount, pShaders);

			for(shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_gShaderProgramObject);
		pep_gShaderProgramObject = 0;

		glUseProgram(0);
	}
	
	GLXContext currentGlxContext = glXGetCurrentContext();
	if((NULL != currentGlxContext) && (currentGlxContext == pep_gGlxContext))
	{
		glXMakeCurrent(pep_gpDisplay, 0, 0);
	}

	if(pep_gGlxContext)
	{
		glXDestroyContext(pep_gpDisplay, pep_gGlxContext);
	}

	if(gWindow)
	{
		XDestroyWindow(pep_gpDisplay, gWindow);
	}

	if(pep_gpXVisualInfo)
	{
		free(pep_gpXVisualInfo);
		pep_gpXVisualInfo = NULL;
	}

	if(pep_gpDisplay)
	{
		XCloseDisplay(pep_gpDisplay);
		pep_gpDisplay = NULL;
	}

	return;
}

void ToggleFullScreen(void)
{
	// variable
	Atom wm_state;
	Atom fullScreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.data.l[0] = pep_bFullScreen ? 0 : 1;
	xev.xclient.format = 32;

	fullScreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullScreen;

	XSendEvent(pep_gpDisplay,
				RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);

	return;
}
