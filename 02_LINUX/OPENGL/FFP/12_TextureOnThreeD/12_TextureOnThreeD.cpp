#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h> // #1st change for OpenGL
#include <GL/glu.h>
#include <GL/glx.h> // #2nd change for OpenGL -> bridging api

#include <SOIL/SOIL.h>

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;

//3rd change
GLXContext gGlxContext;

Colormap pep_gColormap;
Window pep_gWindow;
int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;

GLfloat pep_gPyramidVertices[4][3][3];
GLfloat pep_gPyramidTexCoords[4][3][2];
GLfloat pep_gPyramidRotationAngle = 0.0f;

GLfloat pep_gCubeVertices[6][4][3];
GLfloat pep_gCubeTexCoords[6][4][2];
GLfloat pep_gCubeRotationAngle = 0.0f;

// texture related variables
GLuint pep_texture_stone;
GLuint pep_texture_kundali;


int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    //4th change
    void Initialize(void);
    void Draw(void);
    void Resize(int, int);
    void Update(void);
    
    // Variable Declarations
    int pep_winWidth = pep_giWindowWidth;
    int pep_winHeight = pep_giWindowHeight;
    
    //5th change
    bool bDone = false;
    
    // Code
    
    CreateWindow();
    
    //6th change
    Initialize();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(bDone == false)
    {
        while(XPending(pep_gpDisplay))
        {
            XNextEvent(pep_gpDisplay, &pep_event);
        
            switch(pep_event.type)
            {
                case MapNotify:
                break;
            
                case KeyPress:
                {
                    pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                    
                    switch(pep_keysym)
                    {
                        case XK_Escape:
                        {
                            bDone = true;
                        }
                        break;
                            
                        case XK_F:
                        case XK_f:
                        {
                            if(false == pep_bFullScreen)
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = false;
                            }
                        }
                        break;
                        
                        default:
                            break;
                    } // End Of Switch-Case
                } // End Of Case KeyPress
                break;
            
                case ButtonPress:
                {
                    switch(pep_event.xbutton.button)
                    {
                        case 1:
                            break;
                            
                        case 2:
                            break;
                            
                        case 3:
                            break;
                            
                        default:
                            break;
                    } // End Of Switch
                }
                break;
                
                case MotionNotify:
                    break;
                
                case ConfigureNotify:
                {
                    pep_winWidth = pep_event.xconfigure.width;
                    pep_winHeight = pep_event.xconfigure.height;
                    
                    Resize(pep_winWidth, pep_winHeight);
                }
                break;
                
                case Expose:
                    break;
                    
                case DestroyNotify:
                    break;
                    
                case 33:
                {
                    bDone = true;
                }
                break;
                
                default:
                    break;
                            
            } // End Of Switch
        } // End Of While
        
        Update();
        Draw();
        
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    //int pep_defaultDepth;
    int pep_styleMask;
    
    // convensionally static not must
    // properties of visual we want
    static int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        GLX_RGBA, // PIXEL TYPE
        GLX_RED_SIZE, 8, // pixel madhe red chi size
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None        
    };
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    // mala asa visual/pfd de, je ki opengl sarkh readering karel visual pahije
    pep_gpXVisualInfo = glXChooseVisual(pep_gpDisplay, pep_defaultScreen, frameBufferAttributes);
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_giWindowWidth,
        pep_giWindowHeight,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "Texture On 3D Object");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
    
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    
    if(pep_texture_kundali)
    {
        glDeleteTextures(1, &pep_texture_kundali);
        pep_texture_kundali = 0;
    }
    
    if(pep_texture_stone)
    {
        glDeleteTextures(1, &pep_texture_stone);
        pep_texture_stone = 0;
    }
    
    GLXContext currentGlxContext = glXGetCurrentContext();
    if((NULL != currentGlxContext) && (currentGlxContext == gGlxContext))
    {
        glXMakeCurrent(pep_gpDisplay, 0, 0);
    }
    
    if(gGlxContext)
    {
        glXDestroyContext(pep_gpDisplay, gGlxContext);
    }
    
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}

void Initialize(void)
{
    void Resize(int, int);
    GLuint LoadBitmapAsTexture(const char*);
    
    //
	// Pyramid Vertices
	//

	// FrontSide-Vertices
	pep_gPyramidVertices[0][0][0] = 0.0f;
	pep_gPyramidVertices[0][0][1] = 1.0f;
	pep_gPyramidVertices[0][0][2] = 0.0f;

	pep_gPyramidVertices[0][1][0] = -1.0f;
	pep_gPyramidVertices[0][1][1] = -1.0f;
	pep_gPyramidVertices[0][1][2] = 1.0f;

	pep_gPyramidVertices[0][2][0] = 1.0f;
	pep_gPyramidVertices[0][2][1] = -1.0f;
	pep_gPyramidVertices[0][2][2] = 1.0f;

	// RightSide-Vertices
	pep_gPyramidVertices[1][0][0] = 0.0f;
	pep_gPyramidVertices[1][0][1] = 1.0f;
	pep_gPyramidVertices[1][0][2] = 0.0f;

	pep_gPyramidVertices[1][1][0] = 1.0f;
	pep_gPyramidVertices[1][1][1] = -1.0f;
	pep_gPyramidVertices[1][1][2] = 1.0f;

	pep_gPyramidVertices[1][2][0] = 1.0f;
	pep_gPyramidVertices[1][2][1] = -1.0f;
	pep_gPyramidVertices[1][2][2] = -1.0f;

	// BackSide-Vertices
	pep_gPyramidVertices[2][0][0] = 0.0f;
	pep_gPyramidVertices[2][0][1] = 1.0f;
	pep_gPyramidVertices[2][0][2] = 0.0f;

	pep_gPyramidVertices[2][1][0] = 1.0f;
	pep_gPyramidVertices[2][1][1] = -1.0f;
	pep_gPyramidVertices[2][1][2] = -1.0f;

	pep_gPyramidVertices[2][2][0] = -1.0f;
	pep_gPyramidVertices[2][2][1] = -1.0f;
	pep_gPyramidVertices[2][2][2] = -1.0f;

	// LeftSide-Vertices
	pep_gPyramidVertices[3][0][0] = 0.0f;
	pep_gPyramidVertices[3][0][1] = 1.0f;
	pep_gPyramidVertices[3][0][2] = 0.0f;

	pep_gPyramidVertices[3][1][0] = -1.0f;
	pep_gPyramidVertices[3][1][1] = -1.0f;
	pep_gPyramidVertices[3][1][2] = -1.0f;

	pep_gPyramidVertices[3][2][0] = -1.0f;
	pep_gPyramidVertices[3][2][1] = -1.0f;
	pep_gPyramidVertices[3][2][2] = 1.0f;


	//
	// TexCoord
	//

	// FrontSide-TexCoord
	pep_gPyramidTexCoords[0][0][0] = 0.50f;
	pep_gPyramidTexCoords[0][0][1] = 1.0f;

	pep_gPyramidTexCoords[0][1][0] = 0.0f;
	pep_gPyramidTexCoords[0][1][1] = 0.0f;

	pep_gPyramidTexCoords[0][2][0] = 1.0f;
	pep_gPyramidTexCoords[0][2][1] = 0.0f;

	// RightSide-TexCoord
	pep_gPyramidTexCoords[1][0][0] = 0.50f;
	pep_gPyramidTexCoords[1][0][1] = 1.0f;

	pep_gPyramidTexCoords[1][1][0] = 0.0f;
	pep_gPyramidTexCoords[1][1][1] = 0.0f;

	pep_gPyramidTexCoords[1][2][0] = 1.0f;
	pep_gPyramidTexCoords[1][2][1] = 0.0f;

	// BackSide-TexCoord
	pep_gPyramidTexCoords[2][0][0] = 0.50f;
	pep_gPyramidTexCoords[2][0][1] = 1.0f;

	pep_gPyramidTexCoords[2][1][0] = 0.0f;
	pep_gPyramidTexCoords[2][1][1] = 0.0f;

	pep_gPyramidTexCoords[2][2][0] = 1.0f;
	pep_gPyramidTexCoords[2][2][1] = 0.0f;

	// LeftSide-TexCoord
	pep_gPyramidTexCoords[3][0][0] = 0.50f;
	pep_gPyramidTexCoords[3][0][1] = 1.0f;

	pep_gPyramidTexCoords[3][1][0] = 0.0f;
	pep_gPyramidTexCoords[3][1][1] = 0.0f;

	pep_gPyramidTexCoords[3][2][0] = 1.0f;
	pep_gPyramidTexCoords[3][2][1] = 0.0f;

	//
	// Cube Vertices
	//
	
	// FrontSide-Vertices
	pep_gCubeVertices[0][0][0] = 1.0f;
	pep_gCubeVertices[0][0][1] = 1.0f;
	pep_gCubeVertices[0][0][2] = 1.0f;

	pep_gCubeVertices[0][1][0] = -1.0f;
	pep_gCubeVertices[0][1][1] = 1.0f;
	pep_gCubeVertices[0][1][2] = 1.0f;

	pep_gCubeVertices[0][2][0] = -1.0f;
	pep_gCubeVertices[0][2][1] = -1.0f;
	pep_gCubeVertices[0][2][2] = 1.0f;

	pep_gCubeVertices[0][3][0] = 1.0f;
	pep_gCubeVertices[0][3][1] = -1.0f;
	pep_gCubeVertices[0][3][2] = 1.0f;

	// RightSide-Vertices
	pep_gCubeVertices[1][0][0] = 1.0f;
	pep_gCubeVertices[1][0][1] = 1.0f;
	pep_gCubeVertices[1][0][2] = -1.0f;

	pep_gCubeVertices[1][1][0] = 1.0f;
	pep_gCubeVertices[1][1][1] = 1.0f;
	pep_gCubeVertices[1][1][2] = 1.0f;

	pep_gCubeVertices[1][2][0] = 1.0f;
	pep_gCubeVertices[1][2][1] = -1.0f;
	pep_gCubeVertices[1][2][2] = 1.0f;

	pep_gCubeVertices[1][3][0] = 1.0f;
	pep_gCubeVertices[1][3][1] = -1.0f;
	pep_gCubeVertices[1][3][2] = -1.0f;

	// BackSide-Vertices
	pep_gCubeVertices[2][0][0] = -1.0f;
	pep_gCubeVertices[2][0][1] = 1.0f;
	pep_gCubeVertices[2][0][2] = -1.0f;

	pep_gCubeVertices[2][1][0] = 1.0f;
	pep_gCubeVertices[2][1][1] = 1.0f;
	pep_gCubeVertices[2][1][2] = -1.0f;

	pep_gCubeVertices[2][2][0] = 1.0f;
	pep_gCubeVertices[2][2][1] = -1.0f;
	pep_gCubeVertices[2][2][2] = -1.0f;

	pep_gCubeVertices[2][3][0] = -1.0f;
	pep_gCubeVertices[2][3][1] = -1.0f;
	pep_gCubeVertices[2][3][2] = -1.0f;
	
	// LeftSide-Vertices
	pep_gCubeVertices[3][0][0] = -1.0f;
	pep_gCubeVertices[3][0][1] = 1.0f;
	pep_gCubeVertices[3][0][2] = 1.0f;

	pep_gCubeVertices[3][1][0] = -1.0f;
	pep_gCubeVertices[3][1][1] = 1.0f;
	pep_gCubeVertices[3][1][2] = -1.0f;

	pep_gCubeVertices[3][2][0] = -1.0f;
	pep_gCubeVertices[3][2][1] = -1.0f;
	pep_gCubeVertices[3][2][2] = -1.0f;

	pep_gCubeVertices[3][3][0] = -1.0f;
	pep_gCubeVertices[3][3][1] = -1.0f;
	pep_gCubeVertices[3][3][2] = 1.0f;

	// TopSide-Vertices
	pep_gCubeVertices[4][0][0] = 1.0f;
	pep_gCubeVertices[4][0][1] = 1.0f;
	pep_gCubeVertices[4][0][2] = -1.0f;

	pep_gCubeVertices[4][1][0] = -1.0f;
	pep_gCubeVertices[4][1][1] = 1.0f;
	pep_gCubeVertices[4][1][2] = -1.0f;

	pep_gCubeVertices[4][2][0] = -1.0f;
	pep_gCubeVertices[4][2][1] = 1.0f;
	pep_gCubeVertices[4][2][2] = 1.0f;

	pep_gCubeVertices[4][3][0] = 1.0f;
	pep_gCubeVertices[4][3][1] = 1.0f;
	pep_gCubeVertices[4][3][2] = 1.0f;

	// BottomSide-Vertices
	pep_gCubeVertices[5][0][0] = 1.0f;
	pep_gCubeVertices[5][0][1] = -1.0f;
	pep_gCubeVertices[5][0][2] = -1.0f;

	pep_gCubeVertices[5][1][0] = -1.0f;
	pep_gCubeVertices[5][1][1] = -1.0f;
	pep_gCubeVertices[5][1][2] = -1.0f;

	pep_gCubeVertices[5][2][0] = -1.0f;
	pep_gCubeVertices[5][2][1] = -1.0f;
	pep_gCubeVertices[5][2][2] = 1.0f;

	pep_gCubeVertices[5][3][0] = 1.0f;
	pep_gCubeVertices[5][3][1] = -1.0f;
	pep_gCubeVertices[5][3][2] = 1.0f;

	//
	// TexCoord
	//

	// FrontSide-TexCoord
	pep_gCubeTexCoords[0][0][0] = 0.0f;
	pep_gCubeTexCoords[0][0][1] = 0.0f;

	pep_gCubeTexCoords[0][1][0] = 1.0f;
	pep_gCubeTexCoords[0][1][1] = 0.0f;

	pep_gCubeTexCoords[0][2][0] = 1.0f;
	pep_gCubeTexCoords[0][2][1] = 1.0f;

	pep_gCubeTexCoords[0][3][0] = 0.0f;
	pep_gCubeTexCoords[0][3][1] = 1.0f;

	// RightSide-TexCoord
	pep_gCubeTexCoords[1][0][0] = 0.0f;
	pep_gCubeTexCoords[1][0][1] = 0.0f;

	pep_gCubeTexCoords[1][1][0] = 1.0f;
	pep_gCubeTexCoords[1][1][1] = 0.0f;

	pep_gCubeTexCoords[1][2][0] = 1.0f;
	pep_gCubeTexCoords[1][2][1] = 1.0f;

	pep_gCubeTexCoords[1][3][0] = 0.0f;
	pep_gCubeTexCoords[1][3][1] = 1.0f;

	// BackSide-TexCoord
	pep_gCubeTexCoords[2][0][0] = 0.0f;
	pep_gCubeTexCoords[2][0][1] = 0.0f;

	pep_gCubeTexCoords[2][1][0] = 1.0f;
	pep_gCubeTexCoords[2][1][1] = 0.0f;

	pep_gCubeTexCoords[2][2][0] = 1.0f;
	pep_gCubeTexCoords[2][2][1] = 1.0f;

	pep_gCubeTexCoords[2][3][0] = 0.0f;
	pep_gCubeTexCoords[2][3][1] = 1.0f;

	// LeftSide-TexCoord
	pep_gCubeTexCoords[3][0][0] = 0.0f;
	pep_gCubeTexCoords[3][0][1] = 0.0f;

	pep_gCubeTexCoords[3][1][0] = 1.0f;
	pep_gCubeTexCoords[3][1][1] = 0.0f;

	pep_gCubeTexCoords[3][2][0] = 1.0f;
	pep_gCubeTexCoords[3][2][1] = 1.0f;

	pep_gCubeTexCoords[3][3][0] = 0.0f;
	pep_gCubeTexCoords[3][3][1] = 1.0f;

	// TopSide-TexCoord
	pep_gCubeTexCoords[4][0][0] = 0.0f;
	pep_gCubeTexCoords[4][0][1] = 0.0f;

	pep_gCubeTexCoords[4][1][0] = 1.0f;
	pep_gCubeTexCoords[4][1][1] = 0.0f;

	pep_gCubeTexCoords[4][2][0] = 1.0f;
	pep_gCubeTexCoords[4][2][1] = 1.0f;

	pep_gCubeTexCoords[4][3][0] = 0.0f;
	pep_gCubeTexCoords[4][3][1] = 1.0f;

	// BottomSide-TexCoord
	pep_gCubeTexCoords[5][0][0] = 0.0f;
	pep_gCubeTexCoords[5][0][1] = 0.0f;

	pep_gCubeTexCoords[5][1][0] = 1.0f;
	pep_gCubeTexCoords[5][1][1] = 0.0f;

	pep_gCubeTexCoords[5][2][0] = 1.0f;
	pep_gCubeTexCoords[5][2][1] = 1.0f;

	pep_gCubeTexCoords[5][3][0] = 0.0f;
	pep_gCubeTexCoords[5][3][1] = 1.0f;
    
    // opengl rendering context creation
    gGlxContext = glXCreateContext(
        pep_gpDisplay, //
        pep_gpXVisualInfo, //
        NULL, // do not want sharable context hence NULL (in mutiple monitor setup we can share context of other monitor)
        GL_TRUE // (actual graphics card)hardware rendering context. GL_FALSE -> software rendering (novavu/mesa)
    );
    
    glXMakeCurrent(pep_gpDisplay, pep_gWindow, gGlxContext);
    
    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    glEnable(GL_TEXTURE_2D); 
    
    pep_texture_stone = LoadBitmapAsTexture("Stone.bmp");
    pep_texture_kundali = LoadBitmapAsTexture("Vijay_Kundali.bmp");
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    Resize(pep_giWindowWidth, pep_giWindowHeight);
    
    return;
}

void Resize(int pep_width, int pep_height)
{
    if(0 == pep_height)
    {
        pep_height = 1;
    }
    
    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);
	
    return;
}

void Draw(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-2.0f, 0.0f, -8.0f);
	glRotatef(pep_gPyramidRotationAngle, 0.0f, 1.0f, 0.0f);

	glBindTexture(
		GL_TEXTURE_2D,
		pep_texture_stone
	);

	for (int i = 0; i < 4; i++)
	{
		glBegin(GL_TRIANGLES);
		glTexCoord2f(pep_gPyramidTexCoords[i][0][0], pep_gPyramidTexCoords[i][0][1]);
		glVertex3f(pep_gPyramidVertices[i][0][0], pep_gPyramidVertices[i][0][1], pep_gPyramidVertices[i][0][2]);

		glTexCoord2f(pep_gPyramidTexCoords[i][1][0], pep_gPyramidTexCoords[i][1][1]);
		glVertex3f(pep_gPyramidVertices[i][1][0], pep_gPyramidVertices[i][1][1], pep_gPyramidVertices[i][1][2]);

		glTexCoord2f(pep_gPyramidTexCoords[i][2][0], pep_gPyramidTexCoords[i][2][1]);
		glVertex3f(pep_gPyramidVertices[i][2][0], pep_gPyramidVertices[i][2][1], pep_gPyramidVertices[i][2][2]);
		glEnd();
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(2.0f, 0.0f, -8.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(pep_gCubeRotationAngle, 1.0f, 1.0f, 1.0f);

	glBindTexture(
		GL_TEXTURE_2D,
		pep_texture_kundali
	);

	for (int i = 0; i < 6; i++)
	{
		glBegin(GL_QUADS);

		glTexCoord2f(pep_gCubeTexCoords[i][0][0], pep_gCubeTexCoords[i][0][1]);
		glVertex3f(pep_gCubeVertices[i][0][0], pep_gCubeVertices[i][0][1], pep_gCubeVertices[i][0][2]);

		glTexCoord2f(pep_gCubeTexCoords[i][1][0], pep_gCubeTexCoords[i][1][1]);
		glVertex3f(pep_gCubeVertices[i][1][0], pep_gCubeVertices[i][1][1], pep_gCubeVertices[i][1][2]);

		glTexCoord2f(pep_gCubeTexCoords[i][2][0], pep_gCubeTexCoords[i][2][1]);
		glVertex3f(pep_gCubeVertices[i][2][0], pep_gCubeVertices[i][2][1], pep_gCubeVertices[i][2][2]);

		glTexCoord2f(pep_gCubeTexCoords[i][3][0], pep_gCubeTexCoords[i][3][1]);
		glVertex3f(pep_gCubeVertices[i][3][0], pep_gCubeVertices[i][3][1], pep_gCubeVertices[i][3][2]);
		glEnd();
	}

    glXSwapBuffers(pep_gpDisplay, pep_gWindow);

    return;
}

GLuint LoadBitmapAsTexture(const char* bitmap_file_path)
{
    // Variable Declarations
    GLuint textureID;
    
    int width, height;
    unsigned char* pImageData = NULL;
    
    // Code
    pImageData = SOIL_load_image(
        bitmap_file_path,
        &width,
        &height,
        NULL,
        SOIL_LOAD_RGB // which channels we are interested in RGB (Red, Green, Blue). Bitmap file one texel generally 24bits 1 Byte for Red, 1 Byte for Green and 1 Byte for Blue
    );

    glPixelStorei(
    GL_UNPACK_ALIGNMENT, // unpacked
    4 // rgba - per pixel -> alignment is depend of how much data we are using for 1 pixel
    );
    
    glGenTextures(
		1, // no. of buffers
		&textureID // name of target point which will be point to gpu texture memory
	);
    
    glBindTexture(
		GL_TEXTURE_2D,
		textureID
	);
    
    glTexParameteri(
		GL_TEXTURE_2D, // target 
		GL_TEXTURE_MAG_FILTER, // (which parameter to set) -> maginfication (near to far from audience)
		GL_LINEAR // quality (weighted avrage)
	);
    
    glTexParameteri(
		GL_TEXTURE_2D,
		GL_TEXTURE_MIN_FILTER, // minification
		GL_LINEAR_MIPMAP_LINEAR // decrease image size + quality by weighted avarage
	);
    
    gluBuild2DMipmaps(
		GL_TEXTURE_2D,
		3, // no. of colors (rgb) -> internal format
		width,
		height,
		GL_RGB, // windows os native bitmap format given to opengl
		GL_UNSIGNED_BYTE,
		pImageData // image data
	);
    
    SOIL_free_image_data(pImageData);
    
    return textureID;
}

void Update(void)
{
    pep_gPyramidRotationAngle += 0.2f;
    if(pep_gPyramidRotationAngle > 360.0f)
    {
        pep_gPyramidRotationAngle = 0.0f;
    }
    
    pep_gCubeRotationAngle += 0.2f;
    if(pep_gCubeRotationAngle > 360.0f)
    {
        pep_gCubeRotationAngle = 0.0f;
    }
}
