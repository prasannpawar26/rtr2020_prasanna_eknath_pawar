#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h> // #1st change for OpenGL
#include <GL/glu.h>
#include <GL/glx.h> // #2nd change for OpenGL -> bridging api

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;

//3rd change
GLXContext gGlxContext;

Colormap pep_gColormap;
Window pep_gWindow;

int pep_width = 800, pep_height = 600;

float pep_gOuterCircleOrigin[2];
float pep_gOuterCircleRadius;
float pep_gOuterCircleAngle;
int pep_gOuterCirclePoints;

float pep_gInnerRectangleCoordinate_A[2];
float pep_gInnerRectangleCoordinate_B[2];
float pep_gInnerRectangleCoordinate_C[2];
float pep_gInnerRectangleCoordinate_D[2];

float pep_gInnerTriangleCoordinate_A[2] = {0.0f, 0.0f};
float pep_gInnerTriangleCoordinate_B[2] = {0.0f, 0.0f};
float pep_gInnerTriangleCoordinate_C[2] = {0.0f, 0.0f};

float pep_gDistanceBetweenInnerTriangleSide_BC = 0.0f;
float pep_gDistanceBetweenInnerTriangleSide_AC = 0.0f;
float pep_gDistanceBetweenInnerTriangleSide_AB = 0.0f;

float pep_gInnerTriangleSemiPerimeter = 0.0f;
float pep_gInnerCircleRadius = 0.0f;
float pep_gInnerCirclOriginCoordinates[2] = {0.0f, 0.0f};
const int pep_gInnerCirclePoints = 1000;
float pep_gInnerCircleAngle = 0.0f;

int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    //4th change
    void Initialize(void);
    void Draw(void);
    void Resize(int, int);
    
    // Variable Declarations
    int pep_winWidth = pep_width;
    int pep_winHeight = pep_height;
    
    //5th change
    bool bDone = false;
    
    // Code
    
    CreateWindow();
    
    //6th change
    Initialize();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(bDone == false)
    {
        while(XPending(pep_gpDisplay))
        {
            XNextEvent(pep_gpDisplay, &pep_event);
        
            switch(pep_event.type)
            {
                case MapNotify:
                break;
            
                case KeyPress:
                {
                    pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                    
                    switch(pep_keysym)
                    {
                        case XK_Escape:
                        {
                            bDone = true;
                        }
                        break;
                            
                        case XK_F:
                        case XK_f:
                        {
                            if(false == pep_bFullScreen)
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = false;
                            }
                        }
                        break;
                        
                        
                        default:
                            break;
                    } // End Of Switch-Case
                } // End Of Case KeyPress
                break;
            
                case ButtonPress:
                {
                    switch(pep_event.xbutton.button)
                    {
                        case 1:
                            break;
                            
                        case 2:
                            break;
                            
                        case 3:
                            break;
                            
                        default:
                            break;
                    } // End Of Switch
                }
                break;
                
                case MotionNotify:
                    break;
                
                case ConfigureNotify:
                {
                    pep_winWidth = pep_event.xconfigure.width;
                    pep_winHeight = pep_event.xconfigure.height;
                    
                    Resize(pep_winWidth, pep_winHeight);
                }
                break;
                
                case Expose:
                    break;
                    
                case DestroyNotify:
                    break;
                    
                case 33:
                {
                    bDone = true;
                }
                break;
                
                default:
                    break;
                            
            } // End Of Switch
        } // End Of While
        
        Draw();
        
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    //int pep_defaultDepth;
    int pep_styleMask;
    
    // convensionally static not must
    // properties of visual we want
    static int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        GLX_RGBA, // PIXEL TYPE
        GLX_RED_SIZE, 8, // pixel madhe red chi size
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None        
    };
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    // mala asa visual/pfd de, je ki opengl sarkh readering karel visual pahije
    pep_gpXVisualInfo = glXChooseVisual(pep_gpDisplay, pep_defaultScreen, frameBufferAttributes);
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
       
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_width,
        pep_height,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "Graph Paper");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
    
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    
    GLXContext currentGlxContext = glXGetCurrentContext();
    if((NULL != currentGlxContext) && (currentGlxContext == gGlxContext))
    {
        glXMakeCurrent(pep_gpDisplay, 0, 0);
    }
    
    if(gGlxContext)
    {
        glXDestroyContext(pep_gpDisplay, gGlxContext);
    }
    
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}

void Initialize(void)
{
    void Resize(int, int);
    void Initialize_OuterCircle(void);
	void Initialize_InnerRectangle(void);
	void Initialize_InnerTriangle(void);
	void Initialize_InnerCircle(void);
	    
    Initialize_OuterCircle();
	Initialize_InnerRectangle();
	Initialize_InnerTriangle();
	Initialize_InnerCircle();
    
    // opengl rendering context creation
    gGlxContext = glXCreateContext(
        pep_gpDisplay, //
        pep_gpXVisualInfo, //
        NULL, // do not want sharable context hence NULL (in mutiple monitor setup we can share context of other monitor)
        GL_TRUE // (actual graphics card)hardware rendering context. GL_FALSE -> software rendering (novavu/mesa)
    );
    
    glXMakeCurrent(pep_gpDisplay, pep_gWindow, gGlxContext);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    Resize(pep_width, pep_height);
    
    return;
}

void Resize(int width, int height)
{
    if(0 == height)
    {
        height = 1;
    }
    
    pep_width = width;
    pep_height = height;
    
    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);
	
    return;
}

void Draw(void)
{

    //
	//Function Declarations
    //
	void Display_GraphPaper(void);
	void Display_OuterCircle(void);
	void Display_InnerRectangle(void);
	void Display_InnerTriangle(void);
	void Display_InnerCircle(void);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    
	glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -2.50f);
	Display_GraphPaper();
    
    glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.50f);

	// Circle
	Display_OuterCircle();
	// Rectangle In-Side Circle
	Display_InnerRectangle();
	// Triangle In-Side Rectangle
	Display_InnerTriangle();
	// Inner Circle
	Display_InnerCircle();
    
    glXSwapBuffers(pep_gpDisplay, pep_gWindow);

    return;
}

void Display_GraphPaper(void)
{
	// Code

	// Horizontal Lines
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (float y_axis = -1.0f; y_axis <= 1.0f; y_axis = y_axis + 0.02f) {
		glVertex3f(-1.0f, y_axis, 0);
		glVertex3f(1.0f, y_axis, 0);
	}
	glEnd();

	// Vertical Lines
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (float x_axis = -1.0f; x_axis <= 1.0f; x_axis = x_axis + 0.02f) {
		glVertex3f(x_axis, -1.0f, 0.0f);
		glVertex3f(x_axis, 1.0f, 0.0f);
	}
	glEnd();

	// Horizontal Line in Center
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0, 0);
	glVertex3f(1.0f, 0, 0);
	glEnd();

	// Vertical Line in Center
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0, -1.0f, 0);
	glVertex3f(0, 1.0f, 0);
	glEnd();

	return;
}

void Display_OuterCircle(void)
{
	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 0.0f);

	for (int i = 0; i < pep_gOuterCirclePoints; i++) {
		pep_gOuterCircleAngle = (float)(2.0f * M_PI * i) / pep_gOuterCirclePoints;
		glVertex3f((GLfloat)(pep_gOuterCircleOrigin[0] +
			cos(pep_gOuterCircleAngle) * pep_gOuterCircleRadius),
			(GLfloat)(pep_gOuterCircleOrigin[1] +
				sin(pep_gOuterCircleAngle) * pep_gOuterCircleRadius),
			0.0f);
	}

	glEnd();

	return;
}

void Display_InnerRectangle(void)
{

	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(pep_gInnerRectangleCoordinate_A[0], pep_gInnerRectangleCoordinate_A[1],
		0.0f);
	glVertex3f(pep_gInnerRectangleCoordinate_B[0], pep_gInnerRectangleCoordinate_B[1],
		0.0f);
	glVertex3f(pep_gInnerRectangleCoordinate_C[0], pep_gInnerRectangleCoordinate_C[1],
		0.0f);
	glVertex3f(pep_gInnerRectangleCoordinate_D[0], pep_gInnerRectangleCoordinate_D[1],
		0.0f);
	glEnd();

	return;
}

void Display_InnerTriangle(void) {

	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);

	// Co-Ordinate A
	glVertex3f(pep_gInnerTriangleCoordinate_A[0], pep_gInnerTriangleCoordinate_A[1],
		0.0f);
	// Co-Ordinate B
	glVertex3f(pep_gInnerTriangleCoordinate_B[0], pep_gInnerTriangleCoordinate_B[1],
		0.0f);
	// Co-Ordinate C
	glVertex3f(pep_gInnerTriangleCoordinate_C[0], pep_gInnerTriangleCoordinate_C[1],
		0.0f);

	glEnd();

	return;
}

void Display_InnerCircle(void) {

	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < pep_gInnerCirclePoints; i++) {
		pep_gInnerCircleAngle = (float)(2.0f * M_PI * i) / pep_gInnerCirclePoints;
		glVertex3f((GLfloat)(pep_gInnerCirclOriginCoordinates[0] +
			cos(pep_gInnerCircleAngle) * pep_gInnerCircleRadius),
			(GLfloat)(pep_gInnerCirclOriginCoordinates[1] +
				sin(pep_gInnerCircleAngle) * pep_gInnerCircleRadius),
			0.0f);
	}
	glEnd();

	return;
}

void Initialize_OuterCircle(void) {

	// Origin Of Outer Circle = (0.0f, 0.0f)
	pep_gOuterCircleOrigin[0] = 0.0f;
	pep_gOuterCircleOrigin[1] = 0.0f;

	// Radius Of Outer Circle = 1.0f
	pep_gOuterCircleRadius = 0.90f;

	// Starting Angle Of Outer Circle
	pep_gOuterCircleAngle = 0.0f;

	// Number Of Points To Draw To Draw Outer Circle
	pep_gOuterCirclePoints = 1000;

	return;
}

void Initialize_InnerRectangle(void) {

	// Calculating Inner Rectangle Starting Co-Ordinates. Taking HarCoded Angle
	// Value 15.0f.
	// Note: All The Co-Ordinates Of Inner Rectangle Should Touches The Outer
	// Circle Hence For Calulating Starting Co-ordinate Of Inner(In-Side Outer
	// Circle) Rectangle Using Radius Of Outer Circle

	pep_gInnerRectangleCoordinate_A[0] =
		(float)(pep_gOuterCircleOrigin[0] + cos(15.0f) * pep_gOuterCircleRadius);
	pep_gInnerRectangleCoordinate_A[1] =
		(float)(pep_gOuterCircleOrigin[1] + sin(15.0f) * pep_gOuterCircleRadius);

	// Picking 2nd Co-Ordinate Of Rectangle In (-X, Y) / 2nd Quadrant.
	// 2nd Co-Ordinate will be 2X = width distance from 1st Co-Ordinate. Hence
	// Assigning negative Of X of 1st To 2nd Co-Ordinate
	pep_gInnerRectangleCoordinate_B[0] = -pep_gInnerRectangleCoordinate_A[0];
	pep_gInnerRectangleCoordinate_B[1] = pep_gInnerRectangleCoordinate_A[1];

	// Picking 2nd Co-Ordinate Of Rectangle In (-X, -Y) / 3rd Quadrant.
	// 3rd Co-Ordinate will be 2Y = height distance from 2nd Co-Ordinate.
	pep_gInnerRectangleCoordinate_C[0] = -pep_gInnerRectangleCoordinate_A[0];
	pep_gInnerRectangleCoordinate_C[1] = -pep_gInnerRectangleCoordinate_A[1];

	// Picking 2nd Co-Ordinate Of Rectangle In (X, -Y) / 4th Quadrant.
	// 3rd Co-Ordinate will be 2X = width distance from 4th Co-Ordinate. (I.E 4th
	// Co-Ordinate Will be 2Y Distance From 1st Co-Ordinate). Hence Assigning
	// negative Of -Y of 1st To 4th Co-Ordinate
	pep_gInnerRectangleCoordinate_D[0] = pep_gInnerRectangleCoordinate_A[0];
	pep_gInnerRectangleCoordinate_D[1] = -pep_gInnerRectangleCoordinate_A[1];

	return;
}

void Initialize_InnerTriangle(void) {

	// Calculating Top "A" Co-Ordinate Of Triangle
	// Rectangles Co-Ordinate A And B Lies In 1st And 2nd Quadrant Respsectively
	pep_gInnerTriangleCoordinate_A[0] = (float)((pep_gInnerRectangleCoordinate_B[0]) +
		pep_gInnerRectangleCoordinate_A[0]) /
		2.0f;
	pep_gInnerTriangleCoordinate_A[1] =
		(float)(pep_gInnerRectangleCoordinate_B[1] + pep_gInnerRectangleCoordinate_A[1]) /
		2.0f;

	// Base Co-Ordinates Of Inner Triangle  Will Be Similar To Rectangles 3rd And
	// 4th Co-Ordinates Hence Directly Initializing With Them
	pep_gInnerTriangleCoordinate_B[0] = pep_gInnerRectangleCoordinate_C[0];
	pep_gInnerTriangleCoordinate_B[1] = pep_gInnerRectangleCoordinate_C[1];

	pep_gInnerTriangleCoordinate_C[0] = pep_gInnerRectangleCoordinate_D[0];
	pep_gInnerTriangleCoordinate_C[1] = pep_gInnerRectangleCoordinate_D[1];

	return;
}

void Initialize_InnerCircle(void) {

	// 1. Calculate Sides Of Triangle
	// 2. Calculate semi- perimeter
	// 3. Calculate radius
	// 4. Calculate Origin Coordinates Of Inner Circle

	// Distance BetWeen Triangles B And C Co-Ordinates = Called As Distance 'A'
	pep_gDistanceBetweenInnerTriangleSide_BC = (float)sqrt(
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_B[0]) *
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_B[0]) +
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_B[1]) *
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_B[1]));

	// Distance BetWeen Triangles A And C Co-Ordinates = Called As Distance 'B'
	pep_gDistanceBetweenInnerTriangleSide_AC = (float)sqrt(
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_A[0]) *
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_A[0]) +
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_A[1]) *
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_A[1]));

	// Distance BetWeen Triangles A And B Co-Ordinates = Called As Distance 'C'
	pep_gDistanceBetweenInnerTriangleSide_AB = (float)sqrt(
		(pep_gInnerTriangleCoordinate_B[0] - pep_gInnerTriangleCoordinate_A[0]) *
		(pep_gInnerTriangleCoordinate_B[0] - pep_gInnerTriangleCoordinate_A[0]) +
		(pep_gInnerTriangleCoordinate_B[1] - pep_gInnerTriangleCoordinate_A[1]) *
		(pep_gInnerTriangleCoordinate_B[1] - pep_gInnerTriangleCoordinate_A[1]));

	pep_gInnerTriangleSemiPerimeter = (float)(pep_gDistanceBetweenInnerTriangleSide_BC +
		pep_gDistanceBetweenInnerTriangleSide_AC +
		pep_gDistanceBetweenInnerTriangleSide_AB) /
		2;

	pep_gInnerCircleRadius =
		(float)sqrt(
			pep_gInnerTriangleSemiPerimeter *
			(pep_gInnerTriangleSemiPerimeter - pep_gDistanceBetweenInnerTriangleSide_BC) *
			(pep_gInnerTriangleSemiPerimeter - pep_gDistanceBetweenInnerTriangleSide_AC) *
			(pep_gInnerTriangleSemiPerimeter -
				pep_gDistanceBetweenInnerTriangleSide_AB)) /
		pep_gInnerTriangleSemiPerimeter;

	pep_gInnerCirclOriginCoordinates[0] =
		(float)((pep_gDistanceBetweenInnerTriangleSide_BC *
			pep_gInnerTriangleCoordinate_A[0]) +
			(pep_gDistanceBetweenInnerTriangleSide_AC *
				pep_gInnerTriangleCoordinate_B[0]) +
				(pep_gDistanceBetweenInnerTriangleSide_AB *
					pep_gInnerTriangleCoordinate_C[0])) /
					(pep_gDistanceBetweenInnerTriangleSide_BC +
						pep_gDistanceBetweenInnerTriangleSide_AC +
						pep_gDistanceBetweenInnerTriangleSide_AB);

	pep_gInnerCirclOriginCoordinates[1] =
		(float)((pep_gDistanceBetweenInnerTriangleSide_BC *
			pep_gInnerTriangleCoordinate_A[1]) +
			(pep_gDistanceBetweenInnerTriangleSide_AC *
				pep_gInnerTriangleCoordinate_B[1]) +
				(pep_gDistanceBetweenInnerTriangleSide_AB *
					pep_gInnerTriangleCoordinate_C[1])) /
					(pep_gDistanceBetweenInnerTriangleSide_BC +
						pep_gDistanceBetweenInnerTriangleSide_AC +
						pep_gDistanceBetweenInnerTriangleSide_AB);

	return;
}

