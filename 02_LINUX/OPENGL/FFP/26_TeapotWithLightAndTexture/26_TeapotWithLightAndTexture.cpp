#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h> // #1st change for OpenGL
#include <GL/glu.h>
#include <GL/glx.h> // #2nd change for OpenGL -> bridging api

#include <SOIL/SOIL.h>

#include "OGL.h"

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;

//3rd change
GLXContext gGlxContext;

Colormap pep_gColormap;
Window pep_gWindow;
int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;

GLfloat pep_LightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat pep_LightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat pep_LightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat pep_LightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f}; // positional light

GLfloat pep_MaterialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat pep_MaterialDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat pep_MaterialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat pep_MaterialShininess = 128.0f;

GLfloat gRotationAngle = 0.0f;

bool pep_bLight = false;
bool pep_bAnimation = false;
bool pep_bTexture = false;

GLuint pep_marble;

int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    //4th change
    void Initialize(void);
    void Update(void);
    void Draw(void);
    void Resize(int, int);
    
    // Variable Declarations
    int pep_winWidth = pep_giWindowWidth;
    int pep_winHeight = pep_giWindowHeight;
    
    //5th change
    bool bDone = false;
    
    // Code
    
    CreateWindow();
    
    //6th change
    Initialize();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(bDone == false)
    {
        while(XPending(pep_gpDisplay))
        {
            XNextEvent(pep_gpDisplay, &pep_event);
        
            switch(pep_event.type)
            {
                case MapNotify:
                break;
            
                case KeyPress:
                {
                    pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                    
                    switch(pep_keysym)
                    {
                        case XK_Escape:
                        {
                            bDone = true;
                        }
                        break;
                            
                        case XK_F:
                        case XK_f:
                        {
                            if(false == pep_bFullScreen)
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = false;
                            }
                        }
                        break;
                                                               
                        case XK_l:
                        case XK_L:
                        {
                            if (true == pep_bLight)
                            {
                                glDisable(GL_LIGHTING); // global light switch

                                pep_bLight = false;
                            }
                            else
                            {
                                glEnable(GL_LIGHTING); // global light switch
                                pep_bLight = true;
                            }
                        }
                        break;
                
                        case XK_T:
                        case XK_t:
                        {
                            if (true == pep_bTexture)
                            {
                                glDisable(GL_TEXTURE_2D);
                                glBindTexture(
                                    GL_TEXTURE_2D,
                                    0
                                );
                                pep_bTexture = false;
                            }
                            else
                            {
                                glEnable(GL_TEXTURE_2D);
                                glBindTexture(
                                    GL_TEXTURE_2D,
                                    pep_marble
                                );
                                pep_bTexture = true;
                            }
                        }
                        break;
                        
                        case XK_a:
                        case XK_A:
                        {
                            if (true == pep_bAnimation)
                            {
                                pep_bAnimation = false;
                            }
                            else
                            {
                                pep_bAnimation = true;
                            }
                        }
                        break;
                        
                        default:
                            break;
                    } // End Of Switch-Case
                } // End Of Case KeyPress
                break;
            
                case ButtonPress:
                {
                    switch(pep_event.xbutton.button)
                    {
                        case 1:
                            break;
                            
                        case 2:
                            break;
                            
                        case 3:
                            break;
                            
                        default:
                            break;
                    } // End Of Switch
                }
                break;
                
                case MotionNotify:
                    break;
                
                case ConfigureNotify:
                {
                    pep_winWidth = pep_event.xconfigure.width;
                    pep_winHeight = pep_event.xconfigure.height;
                    
                    Resize(pep_winWidth, pep_winHeight);
                }
                break;
                
                case Expose:
                    break;
                    
                case DestroyNotify:
                    break;
                    
                case 33:
                {
                    bDone = true;
                }
                break;
                
                default:
                    break;
                            
            } // End Of Switch
        } // End Of While
        
        Update();
        
        Draw();
        
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    //int pep_defaultDepth;
    int pep_styleMask;
    
    // convensionally static not must
    // properties of visual we want
    static int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        GLX_RGBA, // PIXEL TYPE
        GLX_RED_SIZE, 8, // pixel madhe red chi size
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None        
    };
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    // mala asa visual/pfd de, je ki opengl sarkh readering karel visual pahije
    pep_gpXVisualInfo = glXChooseVisual(pep_gpDisplay, pep_defaultScreen, frameBufferAttributes);
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_giWindowWidth,
        pep_giWindowHeight,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "Teapot with Light And Texture");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
    
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    
    GLXContext currentGlxContext = glXGetCurrentContext();
    if((NULL != currentGlxContext) && (currentGlxContext == gGlxContext))
    {
        glXMakeCurrent(pep_gpDisplay, 0, 0);
    }
    
    if(gGlxContext)
    {
        glXDestroyContext(pep_gpDisplay, gGlxContext);
    }
    
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}

void Initialize(void)
{
    void Resize(int, int);
    bool LoadTexture(GLuint*, const char*);
    
    // opengl rendering context creation
    gGlxContext = glXCreateContext(
        pep_gpDisplay, //
        pep_gpXVisualInfo, //
        NULL, // do not want sharable context hence NULL (in mutiple monitor setup we can share context of other monitor)
        GL_TRUE // (actual graphics card)hardware rendering context. GL_FALSE -> software rendering (novavu/mesa)
    );
    
    glXMakeCurrent(pep_gpDisplay, pep_gWindow, gGlxContext);
    
   
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    glLightfv(GL_LIGHT1, GL_AMBIENT, pep_LightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, pep_LightDiffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, pep_LightSpecular);
	glLightfv(GL_LIGHT1, GL_POSITION, pep_LightPosition);
	glEnable(GL_LIGHT1);

	glMaterialfv(GL_FRONT, GL_AMBIENT, pep_MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, pep_MaterialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, pep_MaterialShininess);
    
    LoadTexture(&pep_marble, "marble.bmp");
    
    Resize(pep_giWindowWidth, pep_giWindowHeight);
    
    return;
}

void Resize(int pep_width, int pep_height)
{
    if(0 == pep_height)
    {
        pep_height = 1;
    }
    
    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);
	
    return;
}

void Draw(void)
{
    // Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -1.0f);
	glRotatef(gRotationAngle, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	for (int i = 0; i < sizeof(face_indicies) / sizeof(face_indicies[0]); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int vi = face_indicies[i][j + 0];
			int ni = face_indicies[i][j + 3];
			int ti = face_indicies[i][j + 6];
            
            glTexCoord2f(textures[ti][0], textures[ti][1]);
			glNormal3f(normals[ni][0], normals[ni][1], normals[ni][2]);
			glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);
		}
	}

	glEnd();

    glXSwapBuffers(pep_gpDisplay, pep_gWindow);

    return;
}

void Update(void)
{  
    if(false != pep_bAnimation)
    {
        return;
    }
        
    if (360.0f < gRotationAngle)
	{
		gRotationAngle = 0.0f;
	}

	gRotationAngle += 1.0f;
	
}

bool LoadTexture(GLuint *texture, const char *path)
{
	// variable declarations
	bool bResult = false;
	int width;
	int height;

	unsigned char *imageData = NULL;

	imageData = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGB);
	if(NULL == imageData)
	{
		bResult = false;
		printf("Error : SOIL_load_image Failed\nExitting Now...\n");
		return bResult;
	}

	bResult = true;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glGenTextures(1, texture);

	glBindTexture(GL_TEXTURE_2D, *texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_BGR, GL_UNSIGNED_BYTE, imageData);

	glBindTexture(GL_TEXTURE_2D, 0);
	
	SOIL_free_image_data(imageData);

	return bResult;
}
