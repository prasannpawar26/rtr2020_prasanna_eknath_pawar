#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h> // #1st change for OpenGL
#include <GL/glu.h>
#include <GL/glx.h> // #2nd change for OpenGL -> bridging api

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;

//3rd change
GLXContext gGlxContext;

Colormap pep_gColormap;
Window pep_gWindow;

int pep_width = 800, pep_height = 600;

GLfloat pep_gTriangleVertices[3][3] =
{
	{0.0f, 1.0f, 0.0f},
	{-1.0f, -1.0f, 0.0f},
	{1.0f, -1.0f, 0.0f}
};
 
struct Circle
{
	GLfloat center[3];
	GLfloat radius;
	GLfloat angle;
	GLfloat points;
};

struct Circle pep_gInnerCircle;

GLfloat pep_gMidPoint[3];

bool pep_gbIsTrianglePlaced = false;
bool pep_gbIsCirclePlaced = false;
bool pep_gbIsLinePlaced = false;

// Used For Tirangle Model
float pep_gTriangle_transformation_x = 3.5f;
float pep_gTriangle_transformation_y = -3.5f;
float pep_gTriangle_rotation_angle = 0.0f;

// Used For Circle Model
float pep_gCircle_transformation_x = -4.5f;
float pep_gCircle_transformation_y = -4.5f;
float pep_gCircle_rotation_angle = 0.0f;

// Used For Line Model
float pep_gLine_transformation_y = 3.5f;

// Used For All Models
float pep_gTransformation_z = -3.0f;

int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    //4th change
    void Initialize(void);
    void Draw(void);
    void Resize(int, int);
    
    // Variable Declarations
    int pep_winWidth = pep_width;
    int pep_winHeight = pep_height;
    
    //5th change
    bool bDone = false;
    
    // Code
    
    CreateWindow();
    
    //6th change
    Initialize();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(bDone == false)
    {
        while(XPending(pep_gpDisplay))
        {
            XNextEvent(pep_gpDisplay, &pep_event);
        
            switch(pep_event.type)
            {
                case MapNotify:
                break;
            
                case KeyPress:
                {
                    pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                    
                    switch(pep_keysym)
                    {
                        case XK_Escape:
                        {
                            bDone = true;
                        }
                        break;
                            
                        case XK_F:
                        case XK_f:
                        {
                            if(false == pep_bFullScreen)
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = false;
                            }
                        }
                        break;
                        
                        
                        default:
                            break;
                    } // End Of Switch-Case
                } // End Of Case KeyPress
                break;
            
                case ButtonPress:
                {
                    switch(pep_event.xbutton.button)
                    {
                        case 1:
                            break;
                            
                        case 2:
                            break;
                            
                        case 3:
                            break;
                            
                        default:
                            break;
                    } // End Of Switch
                }
                break;
                
                case MotionNotify:
                    break;
                
                case ConfigureNotify:
                {
                    pep_winWidth = pep_event.xconfigure.width;
                    pep_winHeight = pep_event.xconfigure.height;
                    
                    Resize(pep_winWidth, pep_winHeight);
                }
                break;
                
                case Expose:
                    break;
                    
                case DestroyNotify:
                    break;
                    
                case 33:
                {
                    bDone = true;
                }
                break;
                
                default:
                    break;
                            
            } // End Of Switch
        } // End Of While
        
        Draw();
        
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    //int pep_defaultDepth;
    int pep_styleMask;
    
    // convensionally static not must
    // properties of visual we want
    static int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        GLX_RGBA, // PIXEL TYPE
        GLX_RED_SIZE, 8, // pixel madhe red chi size
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None        
    };
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    // mala asa visual/pfd de, je ki opengl sarkh readering karel visual pahije
    pep_gpXVisualInfo = glXChooseVisual(pep_gpDisplay, pep_defaultScreen, frameBufferAttributes);
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
       
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_width,
        pep_height,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "Deathly Hallow");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
    
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    
    GLXContext currentGlxContext = glXGetCurrentContext();
    if((NULL != currentGlxContext) && (currentGlxContext == gGlxContext))
    {
        glXMakeCurrent(pep_gpDisplay, 0, 0);
    }
    
    if(gGlxContext)
    {
        glXDestroyContext(pep_gpDisplay, gGlxContext);
    }
    
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}

void Initialize(void)
{
    void Resize(int, int);
    void Initialize_InnerCircle(void);
    
    // opengl rendering context creation
    gGlxContext = glXCreateContext(
        pep_gpDisplay, //
        pep_gpXVisualInfo, //
        NULL, // do not want sharable context hence NULL (in mutiple monitor setup we can share context of other monitor)
        GL_TRUE // (actual graphics card)hardware rendering context. GL_FALSE -> software rendering (novavu/mesa)
    );
    
    glXMakeCurrent(pep_gpDisplay, pep_gWindow, gGlxContext);
    
    Initialize_InnerCircle();

	pep_gMidPoint[0] = (GLfloat)(pep_gTriangleVertices[1][0] + pep_gTriangleVertices[2][0]) / 2.0f;
	pep_gMidPoint[1] = (GLfloat)(pep_gTriangleVertices[1][1] + pep_gTriangleVertices[2][1]) / 2.0f;
	pep_gMidPoint[2] = (GLfloat)(pep_gTriangleVertices[1][2] + pep_gTriangleVertices[2][2]) / 2.0f;
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    Resize(pep_width, pep_height);
    
    return;
}

void Resize(int width, int height)
{
    if(0 == height)
    {
        height = 1;
    }
    
    pep_width = width;
    pep_height = height;
    
    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);
	
    return;
}

void Draw(void)
{
    void SetTransformation_ForAll(void);
    void SetTransformation_Triangle(void);
    void SetTransformation_Circle(void);
    void SetTransformation_Line(void);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glLineWidth(3.0f);
    glColor3f(1.0f, 1.0f, 0.0f);
    
    SetTransformation_ForAll();
    
    //
    // Triangle
    //
    SetTransformation_Triangle();
	glTranslatef(pep_gTriangle_transformation_x, pep_gTriangle_transformation_y, pep_gTransformation_z);
    glRotatef(pep_gTriangle_rotation_angle, 0.0f, 1.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(pep_gTriangleVertices[0][0], pep_gTriangleVertices[0][1], pep_gTriangleVertices[0][2]);
	glVertex3f(pep_gTriangleVertices[1][0], pep_gTriangleVertices[1][1], pep_gTriangleVertices[1][2]);
	glVertex3f(pep_gTriangleVertices[2][0], pep_gTriangleVertices[2][1], pep_gTriangleVertices[2][2]);
	glEnd();

    //
    // Circle
    //
    if (!pep_gbIsTrianglePlaced)
    {
        goto Exit;
    }
    
    SetTransformation_Circle();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    glTranslatef(pep_gCircle_transformation_x, pep_gCircle_transformation_y, pep_gTransformation_z);
    glRotatef(pep_gCircle_rotation_angle, 0.0f, 1.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
	for(int i = 0; i < pep_gInnerCircle.points; i++)
	{
		pep_gInnerCircle.angle = (GLfloat)(2.0f * M_PI * i) /pep_gInnerCircle.points;

		glVertex3f((GLfloat)(pep_gInnerCircle.center[0] + cos(pep_gInnerCircle.angle) * pep_gInnerCircle.radius),
					(GLfloat)(pep_gInnerCircle.center[1] + sin(pep_gInnerCircle.angle) * pep_gInnerCircle.radius),
					0.0f);
	}
	glEnd();

    //
    // Line
    //
    if (!pep_gbIsTrianglePlaced || !pep_gbIsCirclePlaced)
    {
        goto Exit;
    }
    
    SetTransformation_Line();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    glTranslatef(0.0f, pep_gLine_transformation_y, pep_gTransformation_z);
	glBegin(GL_LINE_LOOP);
	glVertex3f(pep_gTriangleVertices[0][0], pep_gTriangleVertices[0][1], pep_gTriangleVertices[0][2]);
	glVertex3f(pep_gMidPoint[0], pep_gMidPoint[1], pep_gMidPoint[2]);
	glEnd();

Exit:
    glXSwapBuffers(pep_gpDisplay, pep_gWindow);

    return;
}

void Initialize_InnerCircle(void)
{
	// 1. Calculate Sides Of Triangle
  	// 2. Calculate semi- perimeter
  	// 3. Calculate radius
  	// 4. Calculate Origin Coordinates Of Inner Circle

	GLfloat triangleCoordinateDistance[3] = 
	{
		// A Distance: Distance Between 2nd And 3rd Corodinate Of Inner Triangle
		(GLfloat)sqrt(
      	(pep_gTriangleVertices[2][0] - pep_gTriangleVertices[1][0]) * (pep_gTriangleVertices[2][0] - pep_gTriangleVertices[1][0]) +
      	(pep_gTriangleVertices[2][1] - pep_gTriangleVertices[1][1]) * (pep_gTriangleVertices[2][1] - pep_gTriangleVertices[1][1])),

		// B Distance: Distance Between 1st And 3rd Corodinate Of Inner Triangle
		(GLfloat)sqrt(
      	(pep_gTriangleVertices[2][0] - pep_gTriangleVertices[0][0]) * (pep_gTriangleVertices[2][0] - pep_gTriangleVertices[0][0]) +
      	(pep_gTriangleVertices[2][1] - pep_gTriangleVertices[0][1]) * (pep_gTriangleVertices[2][1] - pep_gTriangleVertices[0][1])),

		// C Distance: Distance Between 2nd And 1st Corodinate Of Inner Triangle
		(GLfloat)sqrt(
      	(pep_gTriangleVertices[1][0] - pep_gTriangleVertices[0][0]) * (pep_gTriangleVertices[1][0] - pep_gTriangleVertices[0][0]) +
      	(pep_gTriangleVertices[1][1] - pep_gTriangleVertices[0][1]) * (pep_gTriangleVertices[1][1] - pep_gTriangleVertices[0][1]))
	};

	GLfloat semiPerimeter = (GLfloat)(triangleCoordinateDistance[0] + triangleCoordinateDistance[1] + triangleCoordinateDistance[2]) / 2.0f;

	pep_gInnerCircle.radius = (GLfloat)sqrt(
						semiPerimeter *
						(semiPerimeter - triangleCoordinateDistance[0]) *
						(semiPerimeter - triangleCoordinateDistance[1]) *
						(semiPerimeter - triangleCoordinateDistance[2])) / semiPerimeter;

	pep_gInnerCircle.center[0] =
		(GLfloat)((triangleCoordinateDistance[0] * pep_gTriangleVertices[0][0]) +
				  (triangleCoordinateDistance[1] * pep_gTriangleVertices[1][0]) + 
				  (triangleCoordinateDistance[2] * pep_gTriangleVertices[2][0])) / (semiPerimeter * 2.0f);

	pep_gInnerCircle.center[1] =
		(GLfloat)((triangleCoordinateDistance[0] * pep_gTriangleVertices[0][1]) +
				  (triangleCoordinateDistance[1] * pep_gTriangleVertices[1][1]) + 
				  (triangleCoordinateDistance[2] * pep_gTriangleVertices[2][1])) / (semiPerimeter * 2.0f);

	pep_gInnerCircle.points = 1000;
	pep_gInnerCircle.angle = 0.0f;

	return;
}

void SetTransformation_Triangle(void)
{

  // Code

  if (false == pep_gbIsTrianglePlaced) {
    pep_gTriangle_rotation_angle += 1.0f;
    if (pep_gTriangle_rotation_angle == 360.0f) {
      pep_gTriangle_rotation_angle = 0.0f;
    }

    pep_gTriangle_transformation_x = pep_gTriangle_transformation_x - 0.0050f;
    pep_gTriangle_transformation_y = pep_gTriangle_transformation_y + 0.0050f;

    if (0.0f >= pep_gTriangle_transformation_x) {
      pep_gbIsTrianglePlaced = true;
      pep_gTriangle_rotation_angle = 0.0f;
      pep_gTriangle_transformation_x = 0.0f;
      pep_gTriangle_transformation_y = 0.0f;
    }
  }

  return;
}

void SetTransformation_Circle(void)
{

  // Code
  if (false == pep_gbIsCirclePlaced) {
    pep_gCircle_rotation_angle += 1.0f;
    if (pep_gCircle_rotation_angle == 360.0f) {
      pep_gCircle_rotation_angle = 0.0f;
    }

    pep_gCircle_transformation_x = pep_gCircle_transformation_x + 0.0050f;
    pep_gCircle_transformation_y = pep_gCircle_transformation_y + 0.0050f;

    if (0.0f <= pep_gCircle_transformation_x) {
      pep_gbIsCirclePlaced = true;
      pep_gCircle_rotation_angle = 0.0f;
      pep_gCircle_transformation_x = 0.0f;
      pep_gCircle_transformation_y = 0.0f;
    }
  }

  return;
}

void SetTransformation_Line(void)
{
  // Code

  if (false == pep_gbIsLinePlaced) {
    pep_gLine_transformation_y = pep_gLine_transformation_y - 0.0050f;

    if (0.0f >= pep_gLine_transformation_y) {
      pep_gbIsLinePlaced = true;
      pep_gLine_transformation_y = 0.0f;
    }
  }

  return;
}

void SetTransformation_ForAll(void)
{

  // Code

  // For All Models
  if (pep_gbIsCirclePlaced && pep_gbIsLinePlaced && pep_gbIsTrianglePlaced) {
    if (pep_gTransformation_z >= -6.0f) {
      pep_gTransformation_z = pep_gTransformation_z - 0.00100f;
    }

    pep_gTriangle_rotation_angle += 1.0f;
    if (pep_gTriangle_rotation_angle == 360.0f) {
      pep_gTriangle_rotation_angle = 0.0f;
    }

    pep_gCircle_rotation_angle += 1.0f;
    if (pep_gCircle_rotation_angle == 360.0f) {
      pep_gCircle_rotation_angle = 0.0f;
    }
  }

  return;
}
