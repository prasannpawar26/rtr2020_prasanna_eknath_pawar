#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h> // #1st change for OpenGL
#include <GL/glu.h>
#include <GL/glx.h> // #2nd change for OpenGL -> bridging api

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;

//3rd change
GLXContext gGlxContext;

Colormap pep_gColormap;
Window pep_gWindow;
int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;


bool pep_gbAnimation = true;
// Light Configuration
bool pep_gbLight = false;

GLfloat pep_gLightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat pep_gLightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
// W Component Of Light Source Is 1.0f, It Menas It Is Positional Light
GLfloat pep_gLightPosition[] = {0.0f, 0.0f, 0.0f, 1.0f};

GLfloat pep_gLightModelAmbient[] = {0.2f, 0.2f, 0.2f, 1.0f};
GLfloat pep_gLightModelLocalViewer[] = {0.0f};

GLfloat pep_gAngleXRotation = 0.0f;
GLfloat pep_gAngleYRotation = 0.0f;
GLfloat pep_gAngleZRotation = 0.0f;

GLint pep_gKeyPress = 0;

GLUquadric *pep_gpQuadric[24];

GLfloat pep_gPositionOnXAxis = 0.0f;
GLfloat pep_gDisplacementOnXAxis = 0.0f;

int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    //4th change
    void Initialize(void);
    void Update(void);
    void Draw(void);
    void Resize(int, int);
    
    // Variable Declarations
    int pep_winWidth = pep_giWindowWidth;
    int pep_winHeight = pep_giWindowHeight;
    
    //5th change
    bool bDone = false;
    
    // Code
    
    CreateWindow();
    
    //6th change
    Initialize();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(bDone == false)
    {
        while(XPending(pep_gpDisplay))
        {
            XNextEvent(pep_gpDisplay, &pep_event);
        
            switch(pep_event.type)
            {
                case MapNotify:
                break;
            
                case KeyPress:
                {
                    pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                    
                    switch(pep_keysym)
                    {
                        case XK_Escape:
                        {
                            bDone = true;
                        }
                        break;
                            
                        case XK_F:
                        case XK_f:
                        {
                            if(false == pep_bFullScreen)
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = true;
                                pep_gDisplacementOnXAxis = 7.5f;
                            }
                            else
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = false;
                                pep_gDisplacementOnXAxis = 5.0f;
                            }
                        }
                        break;
                        
                        case XK_l:
                        case XK_L:
                        {
                            if(false == pep_gbLight)
                            {
                                pep_gbLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                pep_gbLight = false;
                                glDisable(GL_LIGHTING);
                            }
                        }
                        break;

                        case XK_a:
                        case XK_A:
                        {
                            if(false == pep_gbAnimation)
                            {
                                pep_gbAnimation = true;
                            }
                            else
                            {
                                pep_gbAnimation = false;
                            }
                        }
                        break;
                        
                        case XK_x:
                        case XK_X:
                        {
                            pep_gKeyPress = 1;
                            pep_gAngleXRotation = 0.0f;
                            printf("X key pressed\n");
                        }
                        break;
                        
                        case XK_y:
                        case XK_Y:
                        {
                            pep_gKeyPress = 2;
                            pep_gAngleYRotation = 0.0f;
                            printf("Y key pressed\n");
                        }
                        break;
                        
                        case XK_z:
                        case XK_Z:
                        {
                            pep_gKeyPress = 3;
                            pep_gAngleZRotation = 0.0f;
                            printf("Z key pressed\n");
                        }
                        break;
                        
                        
                        default:
                            break;
                    } // End Of Switch-Case
                } // End Of Case KeyPress
                break;
            
                case ButtonPress:
                {
                    switch(pep_event.xbutton.button)
                    {
                        case 1:
                            break;
                            
                        case 2:
                            break;
                            
                        case 3:
                            break;
                            
                        default:
                            break;
                    } // End Of Switch
                }
                break;
                
                case MotionNotify:
                    break;
                
                case ConfigureNotify:
                {
                    pep_winWidth = pep_event.xconfigure.width;
                    pep_winHeight = pep_event.xconfigure.height;
                    
                    Resize(pep_winWidth, pep_winHeight);
                }
                break;
                
                case Expose:
                    break;
                    
                case DestroyNotify:
                    break;
                    
                case 33:
                {
                    bDone = true;
                }
                break;
                
                default:
                    break;
                            
            } // End Of Switch
        } // End Of While
        
        Update();
        
        Draw();
        
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    //int pep_defaultDepth;
    int pep_styleMask;
    
    // convensionally static not must
    // properties of visual we want
    static int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        GLX_RGBA, // PIXEL TYPE
        GLX_RED_SIZE, 8, // pixel madhe red chi size
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None        
    };
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    // mala asa visual/pfd de, je ki opengl sarkh readering karel visual pahije
    pep_gpXVisualInfo = glXChooseVisual(pep_gpDisplay, pep_defaultScreen, frameBufferAttributes);
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_giWindowWidth,
        pep_giWindowHeight,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "24 Sphere");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
        
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    
    for (int i = 0; i < 24; i++)
    {
        gluDeleteQuadric(pep_gpQuadric[i]);
    }
	
    GLXContext currentGlxContext = glXGetCurrentContext();
    if((NULL != currentGlxContext) && (currentGlxContext == gGlxContext))
    {
        glXMakeCurrent(pep_gpDisplay, 0, 0);
    }
    
    if(gGlxContext)
    {
        glXDestroyContext(pep_gpDisplay, gGlxContext);
    }
    
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}

void Initialize(void)
{
    void Resize(int, int);
    
    
    // opengl rendering context creation
    gGlxContext = glXCreateContext(
        pep_gpDisplay, //
        pep_gpXVisualInfo, //
        NULL, // do not want sharable context hence NULL (in mutiple monitor setup we can share context of other monitor)
        GL_TRUE // (actual graphics card)hardware rendering context. GL_FALSE -> software rendering (novavu/mesa)
    );
    
    glXMakeCurrent(pep_gpDisplay, pep_gWindow, gGlxContext);
    
    pep_gPositionOnXAxis = 2.5f;
    pep_gDisplacementOnXAxis = 5.0f;

    for (int i = 0; i < 24; i++)
    {
        pep_gpQuadric[i] = gluNewQuadric();
    }
    
    // ADS - light initialization
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

    // ADS - light initialization
    // LIGHT
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);

    glLightfv(GL_LIGHT0, GL_AMBIENT , pep_gLightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, pep_gLightDiffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, pep_gLightPosition);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, pep_gLightModelAmbient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, pep_gLightModelLocalViewer);

    glEnable(GL_LIGHT0);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    Resize(pep_giWindowWidth, pep_giWindowHeight);
    
    return;
}

void Resize(int pep_width, int pep_height)
{
    if(0 == pep_height)
    {
        pep_height = 1;
    }
    
    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (pep_width < pep_height)
    {
        glOrtho(0.0f, 15.5f, 0.0f, (15.5f * ((GLfloat)pep_height / (GLfloat)pep_width)), -10.0f, 10.0f);
    }
    else
    {
        glOrtho(0.0f, (15.5f * ((GLfloat)pep_width / (GLfloat)pep_height)), 0.0f, 15.5f, -10.0f, 10.0f);
    }
	
    return;
}

void Draw(void)
{
    // Function Declarations
    void Draw24Sphere(void);
    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (1 == pep_gKeyPress)
    {
        glRotatef(pep_gAngleXRotation, 1.0f, 0.0f, 0.0f);
        pep_gLightPosition[0] = 0.0f;
        pep_gLightPosition[1] = pep_gAngleXRotation;
        pep_gLightPosition[2] = 0.0f;
    }
    else if (2 == pep_gKeyPress)
    {
        glRotatef(pep_gAngleYRotation, 0.0f, 1.0f, 0.0f);
        pep_gLightPosition[0] = 0.0f;
        pep_gLightPosition[1] = 0.0f;
        pep_gLightPosition[2] = pep_gAngleYRotation;
    }
    else if (3 == pep_gKeyPress)
    {
        glRotatef(pep_gAngleZRotation, 0.0f, 0.0f, 1.0f);
        pep_gLightPosition[0] = pep_gAngleZRotation;
        pep_gLightPosition[1] = 0.0f;
        pep_gLightPosition[2] = 0.0f;
    }

    glLightfv(GL_LIGHT0, GL_POSITION, pep_gLightPosition);

    Draw24Sphere();

    glXSwapBuffers(pep_gpDisplay, pep_gWindow);

    return;
}

void Update(void)
{  
    if(false == pep_gbAnimation)
    {
        return;
    }
    
    // Code
    if (1 == pep_gKeyPress)
    {
        pep_gAngleXRotation += 1.51f;

        if (pep_gAngleXRotation >= 360.0f)
        {
	        pep_gAngleXRotation = 0.0f;
        }
    }
    else if (2 == pep_gKeyPress)
    {
        pep_gAngleYRotation += 1.51f;

        if (pep_gAngleYRotation >= 360.0f)
        {
	        pep_gAngleYRotation = 0.0f;
        }
    }
    else if (3 == pep_gKeyPress)
    {
        pep_gAngleZRotation += 1.51f;
        if (pep_gAngleZRotation >= 360.0f)
        {
	        pep_gAngleZRotation = 0.0f;
        }
    }
}

void Draw24Sphere(void)
{
    // Variable Declarations
    GLfloat pep_materialAmbient[4];
    GLfloat pep_materialDiffuse[4];
    GLfloat pep_materialSpecular[4];
    GLfloat pep_materialShininess;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /*---------------------------1st Column : Gems----------------------------*/
    /* 1st Column, 1st Sphere, Emerald  */
    pep_materialAmbient[0] = 0.0215f;
    pep_materialAmbient[1] = 0.1745f;
    pep_materialAmbient[2] = 0.0215f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.07568f;
    pep_materialDiffuse[1] = 0.61424f;
    pep_materialDiffuse[2] = 0.07568f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.633f;
    pep_materialSpecular[1] = 0.727811f;
    pep_materialSpecular[2] = 0.633f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.6f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[0], 1.0f, 30, 30);

    /* 1st Column, 2nd Sphere, Jade */
    pep_materialAmbient[0] = 0.135f;
    pep_materialAmbient[1] = 0.2225f;
    pep_materialAmbient[2] = 0.1575f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.54f;
    pep_materialDiffuse[1] = 0.89f;
    pep_materialDiffuse[2] = 0.63f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.316228f;
    pep_materialSpecular[1] = 0.316228f;
    pep_materialSpecular[2] = 0.316228f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.1f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[1], 1.0f, 30, 30);

    /* 1st Column, 3rd Sphere, Obsidian */
    pep_materialAmbient[0] = 0.05375f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.06625f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.18275f;
    pep_materialDiffuse[1] = 0.17f;
    pep_materialDiffuse[2] = 0.22525f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.332741f;
    pep_materialSpecular[1] = 0.328634f;
    pep_materialSpecular[2] = 0.346435f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.3f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[2], 1.0f, 30, 30);

    /* 1st Column, 4th Sphere, Pearl */
    pep_materialAmbient[0] = 0.25f;
    pep_materialAmbient[1] = 0.20725f;
    pep_materialAmbient[2] = 0.20725f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 1.0f;
    pep_materialDiffuse[1] = 0.829f;
    pep_materialDiffuse[2] = 0.829f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.296648f;
    pep_materialSpecular[1] = 0.296648f;
    pep_materialSpecular[2] = 0.296648f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.088f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[3], 1.0f, 30, 30);

    /* 1st Column, 5th Sphere, Ruby */
    pep_materialAmbient[0] = 0.1745f;
    pep_materialAmbient[1] = 0.01175f;
    pep_materialAmbient[2] = 0.01175f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.61424f;
    pep_materialDiffuse[1] = 0.04136f;
    pep_materialDiffuse[2] = 0.04136f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.727811f;
    pep_materialSpecular[1] = 0.626959f;
    pep_materialSpecular[2] = 0.626959f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.6f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[4], 1.0f, 30, 30);

    /* 1st Column, 6th Sphere, Turquoise */
    pep_materialAmbient[0] = 0.1f;
    pep_materialAmbient[1] = 0.18725f;
    pep_materialAmbient[2] = 0.1745f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.396f;
    pep_materialDiffuse[1] = 0.74151f;
    pep_materialDiffuse[2] = 0.69102f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.297254f;
    pep_materialSpecular[1] = 0.30829f;
    pep_materialSpecular[2] = 0.306678f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.1f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[5], 1.0f, 30, 30);

    /*---------------------------2nd Column : Metal----------------------------*/
    /* 2nd Column, 1st Sphere, Brass */
    pep_materialAmbient[0] = 0.329412f;
    pep_materialAmbient[1] = 0.223529f;
    pep_materialAmbient[2] = 0.027451f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.780392f;
    pep_materialDiffuse[1] = 0.568627f;
    pep_materialDiffuse[2] = 0.113725f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.992157f;
    pep_materialSpecular[1] = 0.941176f;
    pep_materialSpecular[2] = 0.807843f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.21794872f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[6], 1.0f, 30, 30);

    /* 2nd Column, 2nd Sphere, Bronze */
    pep_materialAmbient[0] = 0.2125f;
    pep_materialAmbient[1] = 0.1275f;
    pep_materialAmbient[2] = 0.054f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.714f;
    pep_materialDiffuse[1] = 0.4284f;
    pep_materialDiffuse[2] = 0.18144f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.393548f;
    pep_materialSpecular[1] = 0.271906f;
    pep_materialSpecular[2] = 0.166721f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.2f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[7], 1.0f, 30, 30);

    /* 2nd Column, 3rd Sphere, Chrome */
    pep_materialAmbient[0] = 0.25f;
    pep_materialAmbient[1] = 0.25f;
    pep_materialAmbient[2] = 0.25f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.4f;
    pep_materialDiffuse[1] = 0.4f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.774597f;
    pep_materialSpecular[1] = 0.774597f;
    pep_materialSpecular[2] = 0.774597f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.6f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[8], 1.0f, 30, 30);

    /* 2nd Column, 4th Sphere, Copper */
    pep_materialAmbient[0] = 0.19125f;
    pep_materialAmbient[1] = 0.0735f;
    pep_materialAmbient[2] = 0.0225f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.7038f;
    pep_materialDiffuse[1] = 0.27048f;
    pep_materialDiffuse[2] = 0.0828f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.256777f;
    pep_materialSpecular[1] = 0.137622f;
    pep_materialSpecular[2] = 0.086014f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.1f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[9], 1.0f, 30, 30);

    /* 2nd Column, 5th Sphere, Gold */
    pep_materialAmbient[0] = 0.24725f;
    pep_materialAmbient[1] = 0.1995f;
    pep_materialAmbient[2] = 0.0745f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.75164f;
    pep_materialDiffuse[1] = 0.60648f;
    pep_materialDiffuse[2] = 0.22648f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.628281f;
    pep_materialSpecular[1] = 0.555802f;
    pep_materialSpecular[2] = 0.366065f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.4f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[10], 1.0f, 30, 30);

    /* 2nd Column, 6th Sphere, Silver */
    pep_materialAmbient[0] = 0.19225f;
    pep_materialAmbient[1] = 0.19225f;
    pep_materialAmbient[2] = 0.19225f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.50754f;
    pep_materialDiffuse[1] = 0.50754f;
    pep_materialDiffuse[2] = 0.50754f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.508273f;
    pep_materialSpecular[1] = 0.508273f;
    pep_materialSpecular[2] = 0.508273f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.4f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[11], 1.0f, 30, 30);

    /*---------------------------3rd Column : Plastic--------------------------*/
    /* 3rd Column, 1st Sphere, Black */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.01f;
    pep_materialDiffuse[1] = 0.01f;
    pep_materialDiffuse[2] = 0.01f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.50f;
    pep_materialSpecular[1] = 0.50f;
    pep_materialSpecular[2] = 0.50f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[12], 1.0f, 30, 30);

    /* 3rd Column, 2nd Sphere, Cyan */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.1f;
    pep_materialAmbient[2] = 0.06f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.0f;
    pep_materialDiffuse[1] = 0.50980392f;
    pep_materialDiffuse[2] = 0.50980392f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.50196078f;
    pep_materialSpecular[1] = 0.50196078f;
    pep_materialSpecular[2] = 0.50196078f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[13], 1.0f, 30, 30);

    /* 3rd Column, 3rd Sphere, Green */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.1f;
    pep_materialDiffuse[1] = 0.35f;
    pep_materialDiffuse[2] = 0.1f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.45f;
    pep_materialSpecular[1] = 0.55f;
    pep_materialSpecular[2] = 0.45f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[14], 1.0f, 30, 30);

    /* 3rd Column, 4th Sphere, Red */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.0f;
    pep_materialDiffuse[2] = 0.0f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.7f;
    pep_materialSpecular[1] = 0.6f;
    pep_materialSpecular[2] = 0.6f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[15], 1.0f, 30, 30);

    /*3rd Column, 5th Sphere, White */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.55f;
    pep_materialDiffuse[1] = 0.55f;
    pep_materialDiffuse[2] = 0.55f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.70f;
    pep_materialSpecular[1] = 0.70f;
    pep_materialSpecular[2] = 0.70f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[16], 1.0f, 30, 30);

    /* 3rd Column, 6th Sphere, Yellow  */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.0f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.60f;
    pep_materialSpecular[1] = 0.60f;
    pep_materialSpecular[2] = 0.50f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[17], 1.0f, 30, 30);

    /*---------------------------4th Column : Rubber---------------------------*/
    /* 4th Column, 1st Sphere, Black */
    pep_materialAmbient[0] = 0.02f;
    pep_materialAmbient[1] = 0.02f;
    pep_materialAmbient[2] = 0.02f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.01f;
    pep_materialDiffuse[1] = 0.01f;
    pep_materialDiffuse[2] = 0.01f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.40f;
    pep_materialSpecular[1] = 0.40f;
    pep_materialSpecular[2] = 0.40f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[18], 1.0f, 30, 30);

    /* 4th Column, 2nd Sphere, Cyan */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.05f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.4f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.5f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.04f;
    pep_materialSpecular[1] = 0.7f;
    pep_materialSpecular[2] = 0.7f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[19], 1.0f, 30, 30);

    /* 4th Column, 3rd Sphere, Green */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.4f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.04f;
    pep_materialSpecular[1] = 0.7f;
    pep_materialSpecular[2] = 0.04f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[20], 1.0f, 30, 30);

    /* 4th Column, 4th Sphere, Red */
    pep_materialAmbient[0] = 0.05f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.4f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.7f;
    pep_materialSpecular[1] = 0.04f;
    pep_materialSpecular[2] = 0.04f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[21], 1.0f, 30, 30);

    /*4th Column, 5th Sphere, White */
    pep_materialAmbient[0] = 0.05f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.05f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.5f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.7f;
    pep_materialSpecular[1] = 0.7f;
    pep_materialSpecular[2] = 0.7f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[22], 1.0f, 30, 30);

    /* 4th Column, 6th Sphere, Yellow */
    pep_materialAmbient[0] = 0.05f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.70f;
    pep_materialSpecular[1] = 0.70f;
    pep_materialSpecular[2] = 0.04f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[23], 1.0f, 30, 30);
    return;
}
