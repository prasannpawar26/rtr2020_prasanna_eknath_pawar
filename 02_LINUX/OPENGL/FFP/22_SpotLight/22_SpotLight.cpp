#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h> // #1st change for OpenGL
#include <GL/glu.h>
#include <GL/glx.h> // #2nd change for OpenGL -> bridging api

#include <math.h>

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;

//3rd change
GLXContext gGlxContext;

Colormap pep_gColormap;
Window pep_gWindow;
int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;

GLUquadric *quadric = NULL;

float cut_off_angle = 12.0f;
float spot_light_exponent = 3.0f;

GLfloat gLightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat gLightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat gLightSpecular[] = {1.0f, 1.0f, 0.0f, 1.0f};
GLfloat gLightPosition[] = {1.25f, -0.7f, 0.0f, 1.0f};
GLfloat spot_direction[] = {-0.10f, 0.80f, -10.0f};

GLfloat gLightModelAmbient[] = {0.2f, 0.2f, 0.2f, 1.0f};
GLfloat gLightModelLocalViewer[] = {0.0f};

// Ambient Light Spreads All Over Equally, Hence Zero.

GLfloat materialAmbient[4];
GLfloat materialDiffuse[4];
GLfloat materialSpecular[4];
GLfloat materialShininess;

bool pep_gbAnimation = true;
// Light Configuration
bool pep_gbLight = false;

float gMiddlePlaneTransformation_X = -10.74f;

float gFlagColor[3][4] = {{1.0f, 0.60f, 0.2f, 0.0f},
                          {1.0f, 1.0f, 1.0f, 0.0f},
                          {0.070f, 0.533f, 0.027f, 0.0f}};

int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    //4th change
    void Initialize(void);
    void Update(void);
    void Draw(void);
    void Resize(int, int);
    
    // Variable Declarations
    int pep_winWidth = pep_giWindowWidth;
    int pep_winHeight = pep_giWindowHeight;
    
    //5th change
    bool bDone = false;
    
    // Code
    
    CreateWindow();
    
    //6th change
    Initialize();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(bDone == false)
    {
        while(XPending(pep_gpDisplay))
        {
            XNextEvent(pep_gpDisplay, &pep_event);
        
            switch(pep_event.type)
            {
                case MapNotify:
                break;
            
                case KeyPress:
                {
                    pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                    
                    switch(pep_keysym)
                    {
                        case XK_Escape:
                        {
                            bDone = true;
                        }
                        break;
                            
                        case XK_F:
                        case XK_f:
                        {
                            if(false == pep_bFullScreen)
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = true;
                            }
                            else
                            {
                                ToggleFullscreen();
                                pep_bFullScreen = false;
                            }
                        }
                        break;
                        
                        case XK_l:
                        case XK_L:
                        {
                            if(false == pep_gbLight)
                            {
                                pep_gbLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                pep_gbLight = false;
                                glDisable(GL_LIGHTING);
                            }
                        }
                        break;

                        case XK_a:
                        case XK_A:
                        {
                            if(false == pep_gbAnimation)
                            {
                                pep_gbAnimation = true;
                            }
                            else
                            {
                                pep_gbAnimation = false;
                            }
                        }
                        break;
                                               
                        
                        default:
                            break;
                    } // End Of Switch-Case
                } // End Of Case KeyPress
                break;
            
                case ButtonPress:
                {
                    switch(pep_event.xbutton.button)
                    {
                        case 1:
                            break;
                            
                        case 2:
                            break;
                            
                        case 3:
                            break;
                            
                        default:
                            break;
                    } // End Of Switch
                }
                break;
                
                case MotionNotify:
                    break;
                
                case ConfigureNotify:
                {
                    pep_winWidth = pep_event.xconfigure.width;
                    pep_winHeight = pep_event.xconfigure.height;
                    
                    Resize(pep_winWidth, pep_winHeight);
                }
                break;
                
                case Expose:
                    break;
                    
                case DestroyNotify:
                    break;
                    
                case 33:
                {
                    bDone = true;
                }
                break;
                
                default:
                    break;
                            
            } // End Of Switch
        } // End Of While
        
        Update();
        
        Draw();
        
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    //int pep_defaultDepth;
    int pep_styleMask;
    
    // convensionally static not must
    // properties of visual we want
    static int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_DEPTH_SIZE, 24,
        GLX_RGBA, // PIXEL TYPE
        GLX_RED_SIZE, 8, // pixel madhe red chi size
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None        
    };
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    // mala asa visual/pfd de, je ki opengl sarkh readering karel visual pahije
    pep_gpXVisualInfo = glXChooseVisual(pep_gpDisplay, pep_defaultScreen, frameBufferAttributes);
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_giWindowWidth,
        pep_giWindowHeight,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "Spot Light");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
        
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    	
    GLXContext currentGlxContext = glXGetCurrentContext();
    if((NULL != currentGlxContext) && (currentGlxContext == gGlxContext))
    {
        glXMakeCurrent(pep_gpDisplay, 0, 0);
    }
    
    if(gGlxContext)
    {
        glXDestroyContext(pep_gpDisplay, gGlxContext);
    }
    
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}

void Initialize(void)
{
    void Resize(int, int);
    
    
    // opengl rendering context creation
    gGlxContext = glXCreateContext(
        pep_gpDisplay, //
        pep_gpXVisualInfo, //
        NULL, // do not want sharable context hence NULL (in mutiple monitor setup we can share context of other monitor)
        GL_TRUE // (actual graphics card)hardware rendering context. GL_FALSE -> software rendering (novavu/mesa)
    );
    
    glXMakeCurrent(pep_gpDisplay, pep_gWindow, gGlxContext);
    
    // ADS - light initialization
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // ADS - light initialization
    // LIGHT
    glLightfv(GL_LIGHT1, GL_AMBIENT, gLightAmbient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, gLightDiffuse);
    //glLightfv(GL_LIGHT1, GL_SPECULAR, gLightSpecular);
    glLightfv(GL_LIGHT1, GL_POSITION, gLightPosition);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, cut_off_angle);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, spot_light_exponent);

    glEnable(GL_LIGHT1);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    glShadeModel(GL_SMOOTH);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    Resize(pep_giWindowWidth, pep_giWindowHeight);
    
    return;
}

void Resize(int pep_width, int pep_height)
{
    if(0 == pep_height)
    {
        pep_height = 1;
    }
    
    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.1f, 100.0f);
	
    return;
}

void Draw(void)
{
    // Code
    void DrawMiddlePlane(void);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // 3-D Specific Change
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(gMiddlePlaneTransformation_X, 0.0f, -8.0f);

    DrawMiddlePlane();

    glXSwapBuffers(pep_gpDisplay, pep_gWindow);

    return;
}

void Update(void)
{  
    if(false == pep_gbAnimation)
    {
        return;
    }
    
    gMiddlePlaneTransformation_X += 0.005f;
}

void DrawMiddlePlane(void)
{
  // Function Declarations
  void DrawPlaneCordinates(void);

  // Plane
  DrawPlaneCordinates();
  return;
}

const int gPoints = 1000;
void DrawPlaneCordinates(void)
{
  // Variable Declaration
  float angle = 0.0f;
  int color_index;

  // Code
  materialAmbient[0] = 0.329412f;
  materialAmbient[1] = 0.223529f;
  materialAmbient[2] = 0.027451f;
  materialAmbient[3] = 1.0f;
  glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

  materialDiffuse[0] = 0.780392f;
  materialDiffuse[1] = 0.568627f;
  materialDiffuse[2] = 0.113725f;
  materialDiffuse[3] = 1.0f;
  glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

  materialSpecular[0] = 0.992157f;
  materialSpecular[1] = 0.941176f;
  materialSpecular[2] = 0.807843f;
  materialSpecular[3] = 1.0f;
  glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

  materialShininess = 0.21794872f * 128;
  glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

  // Middle-Part
  glBegin(GL_POLYGON);
  if (!pep_gbLight)
  {
    glColor3f(0.3647f, 0.5412f, 0.6589f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(0.35f, 0.25f, 0.0f);
  glVertex3f(0.35f, -0.25f, 0.0f);
  glVertex3f(-0.70f, -0.25f, 0.0f);
  glVertex3f(-0.70f, 0.25f, 0.0f);
  glEnd();

  // I
  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-0.45f, 0.15f, 0.0f);
  glVertex3f(-0.30f, 0.15f, 0.0f);
  glEnd();

  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-0.45f, -0.15f, 0.0f);
  glVertex3f(-0.30f, -0.15f, 0.0f);
  glEnd();

  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-0.375f, 0.15f, 0.0f);
  glVertex3f(-0.375f, -0.15f, 0.0f);
  glEnd();

  // A
  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-0.15f, 0.15f, 0.0f);
  glVertex3f(-0.25f, -0.15f, 0.0f);
  glEnd();

  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-0.15f, 0.15f, 0.0f);
  glVertex3f(-0.05f, -0.15f, 0.0f);
  glEnd();

  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-0.20f, 0.0f, 0.0f);
  glVertex3f(-0.10f, 0.0f, 0.0f);
  glEnd();

  // F
  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(0.02f, 0.15f, 0.0f);
  glVertex3f(0.02f, -0.15f, 0.0f);
  glEnd();

  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(0.02f, 0.15f, 0.0f);
  glVertex3f(0.15f, 0.15f, 0.0f);
  glEnd();

  glBegin(GL_LINES);
  if (!pep_gbLight) {
    glColor3f(0.0f, 0.0f, 0.0f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(0.02f, 0.05f, 0.0f);
  glVertex3f(0.15f, 0.05f, 0.0f);
  glEnd();

  // Head

  glBegin(GL_POLYGON);
  if (!pep_gbLight) {
    glColor3f(0.3647f, 0.5412f, 0.6589f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }

  glVertex3f(0.35f, 0.18f, 0.0f);
  glVertex3f(0.35f, -0.18f, 0.0f);
  glVertex3f(1.05f, -0.18f, 0.0f);
  glVertex3f(1.45f, 0.0f, 0.0f);
  glVertex3f(1.05f, 0.18f, 0.0f);
  glEnd();

  color_index = 0;
  for (float radius = 0.12f; radius > 0.0f; radius = radius - 0.04f) {
    glBegin(GL_POLYGON);
    if (!pep_gbLight) {
      glColor3f(gFlagColor[color_index][0], gFlagColor[color_index][1],
              gFlagColor[color_index][2]);
    } else {
      glNormal3f(0.0f, 0.0f, 1.0f);
    }
    for (int i = 0; i < gPoints; i++) {
      angle = (float)(2.0f * M_PI * i) / gPoints;
      glVertex3f((GLfloat)(0.60f + cos(angle) * radius),
                 (GLfloat)(sin(angle) * radius), 0.0f);
    }
    glEnd();

    ++color_index;
  }

  // Wings
  glBegin(GL_POLYGON);
  if (!pep_gbLight) {
    glColor3f(0.3647f, 0.5412f, 0.6589f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
 
  glVertex3f(0.20f, 0.25f, 0.0f);
  glVertex3f(-0.60f, 1.3f, 0.0f);
  glVertex3f(-0.80f, 1.1f, 0.0f);
  glVertex3f(-0.45f, 0.25f, 0.0f);
  glEnd();

  glBegin(GL_POLYGON);
  if (!pep_gbLight) {
    glColor3f(0.3647f, 0.5412f, 0.6589f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }

  glVertex3f(0.20f, -0.25f, 0.0f);
  glVertex3f(-0.60f, -1.3f, 0.0f);
  glVertex3f(-0.80f, -1.1f, 0.0f);
  glVertex3f(-0.45f, -0.25f, 0.0f);
  glEnd();

  // Tail
  glBegin(GL_POLYGON);
  if (!pep_gbLight) {
    glColor3f(0.3647f, 0.5412f, 0.6589f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }

  glVertex3f(-0.70f, 0.25f, 0.0f);
  glVertex3f(-1.0f, 0.70f, 0.0f);
  glVertex3f(-1.20f, 0.70f, 0.0f);

  if (!pep_gbLight) {
    glColor3f(0.3647f, 0.5412f, 0.6589f);
  } else {
    glNormal3f(0.0f, 0.0f, 1.0f);
  }
  glVertex3f(-1.20f, -0.70f, 0.0f);
  glVertex3f(-1.0f, -0.70f, 0.0f);
  glVertex3f(-0.70f, -0.25f, 0.0f);
  glEnd();

  return;
}

