#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

using namespace std;

bool pep_bFullScreen = false;
Display *pep_gpDisplay = NULL;
XVisualInfo *pep_gpXVisualInfo = NULL;
Colormap pep_gColormap;
Window pep_gWindow;
int pep_giWindowWidth = 800;
int pep_giWindowHeight = 600;


int main(void)
{
    // Function Prototype
    void CreateWindow(void);
    void ToggleFullscreen(void);
    void Uninitialize(void);
    
    // Variable Declarations
    int pep_winWidth = pep_giWindowWidth;
    int pep_winHeight = pep_giWindowHeight;
    
    // Code
    
    CreateWindow();
    
    // Message Loop
    XEvent pep_event;
    KeySym pep_keysym;
    
    while(1)
    {
        XNextEvent(pep_gpDisplay, &pep_event);
        
        switch(pep_event.type)
        {
            case MapNotify:
                break;
            
            case KeyPress:
            {
                pep_keysym = XkbKeycodeToKeysym(pep_gpDisplay, pep_event.xkey.keycode, 0, 0);
                
                switch(pep_keysym)
                {
                    case XK_Escape:
                        Uninitialize();
                        exit(0);
                        break;
                        
                    case XK_F:
                    case XK_f:
                    { if(false == pep_bFullScreen)
                        {
                            ToggleFullscreen();
                            pep_bFullScreen = true;
                        }
                        else
                        {
                            ToggleFullscreen();
                            pep_bFullScreen = false;
                        }
                    }
                    break;
                    
                    default:
                        break;
                } // End Of Switch-Case
            } // End Of Case KeyPress
            break;
            
            case ButtonPress:
            {
                switch(pep_event.xbutton.button)
                {
                    case 1:
                        break;
                        
                    case 2:
                        break;
                        
                    case 3:
                        break;
                        
                    default:
                        break;
                } // End Of Switch
            }
            break;
            
            case MotionNotify:
                break;
                
            case ConfigureNotify:
            {
                pep_winWidth = pep_event.xconfigure.width;
                pep_winHeight = pep_event.xconfigure.height;
                
            }
            break;
            
            case Expose:
                break;
                
            case DestroyNotify:
                break;
                
            case 33:
            {
                Uninitialize(); exit(0);
            }
            break;
            
            default:
                break;
                        
        } // End Of Switch
    } // End Of While
    
    Uninitialize();
    
    return 0;
    
}

void CreateWindow(void)
{
    // Function Prototype
    void Uninitialize(void);
    
    // Variable Declarations
    XSetWindowAttributes pep_winAttribs;
    int pep_defaultScreen;
    int pep_defaultDepth;
    int pep_styleMask;
    
    // Code
    pep_gpDisplay = XOpenDisplay(NULL);
    if(NULL == pep_gpDisplay)
    {
        printf("Error : Unable To Open X Display.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_defaultScreen = XDefaultScreen(pep_gpDisplay);
    
    pep_defaultDepth = DefaultDepth(pep_gpDisplay, pep_defaultScreen);
    
    pep_gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    if(NULL == pep_gpXVisualInfo)
    {
        printf("Error : Unable To Allocate Memory For Visual Info.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XMatchVisualInfo(
        pep_gpDisplay,
        pep_defaultScreen,
        pep_defaultDepth,
        TrueColor,
        pep_gpXVisualInfo
    );
    if(NULL == pep_gpXVisualInfo)
    {
        printf("Error : Unable To Get A Visual Info.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    pep_winAttribs.border_pixel = 0;
    pep_winAttribs.background_pixmap = 0;
    pep_winAttribs.colormap = XCreateColormap(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        pep_gpXVisualInfo->visual,
        AllocNone
    );
    pep_gColormap = pep_winAttribs.colormap;
    
    pep_winAttribs.background_pixel = BlackPixel(pep_gpDisplay, pep_defaultScreen);
    pep_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
    
    pep_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
    
    pep_gWindow = XCreateWindow(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        0,
        0,
        pep_giWindowWidth,
        pep_giWindowHeight,
        0,
        pep_gpXVisualInfo->depth,
        InputOutput,
        pep_gpXVisualInfo->visual,
        pep_styleMask,
        &pep_winAttribs
    );
    if(!pep_gWindow)
    {
        printf("Error : Failed To Create Main Window.\nExitting Now...\n");
        Uninitialize();
        exit(1);
    }
    
    XStoreName(pep_gpDisplay, pep_gWindow, "First XWindow : Prasanna Eknath Pawar");
    
    Atom pep_windowManagerDelete = XInternAtom(pep_gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(pep_gpDisplay, pep_gWindow, &pep_windowManagerDelete, 1);
    
    XMapWindow(pep_gpDisplay, pep_gWindow);
    
    return;
}

void ToggleFullscreen(void)
{
    // Variable Declarations
    Atom pep_wm_state;
    Atom pep_fullscreen;
    XEvent pep_xev = {0};
    
    // Code
    pep_wm_state = XInternAtom(pep_gpDisplay, "_NET_WM_STATE", False);
    memset(&pep_xev, 0, sizeof(pep_xev));
    
    pep_xev.type = ClientMessage;
    pep_xev.xclient.window = pep_gWindow;
    pep_xev.xclient.message_type = pep_wm_state;
    pep_xev.xclient.format = 32;
    pep_xev.xclient.data.l[0] = pep_bFullScreen? 0 : 1;
    
    pep_fullscreen = XInternAtom(pep_gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    
    pep_xev.xclient.data.l[1] = pep_fullscreen;
    
    XSendEvent(
        pep_gpDisplay,
        RootWindow(pep_gpDisplay, pep_gpXVisualInfo->screen),
        False,
        StructureNotifyMask,
        &pep_xev
    );
    
    return;
}

void Uninitialize(void)
{
    // Code
    if(pep_gWindow)
    {
        XDestroyWindow(pep_gpDisplay, pep_gWindow);
    }
    
    if(pep_gColormap)
    {
        XFreeColormap(pep_gpDisplay, pep_gColormap);
    }
    
    if(pep_gpXVisualInfo)
    {
        free(pep_gpXVisualInfo);
        pep_gpXVisualInfo = NULL;
    }
    
    if(pep_gpDisplay)
    {
       XCloseDisplay(pep_gpDisplay);
       pep_gpDisplay = NULL;
    }
    
    return;
}



