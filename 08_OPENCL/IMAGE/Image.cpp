// Headers

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <CL/opencl.h>
#include "helper_timer.h"

// Global OpenCl Variables
cl_int ret_ocl;
cl_platform_id oclPlatformID;
cl_device_id oclComputeDeviceID;
cl_context oclContext;
cl_command_queue oclCommandQueue;
cl_program oclProgram;
cl_kernel oclKernel;

char *oclSourceCode = NULL;
size_t sizeKernelCodeLength;

int skull_uImageCount;
int skull_uImageWidth;
int skull_uImageHeight;
WCHAR skull_data_file[256] = L"head256x256x109";

// Odd Number 11444777 Is Deliberate Illustration (NVidia OpenCL Sample)

size_t localWorkSize = 256;
size_t globalWorkSize;

char *hostBuffer = NULL;
char *hostRGBABuffer = NULL;

cl_mem deviceBuffer = NULL;
cl_mem deviceRGBABuffer = NULL;

float image_load_timeOnCPU;
float image_load_timeOnGPU;

size_t luminance_buffer_size;
size_t rgba_buffer_size;

int main(void) {
  // Function Declarations
  void fillFloatArrayWithRandomNumbers(float *, int);
  size_t roundGlobalSizeToNearestMultipleOfLocalSize(int, unsigned int);
  void vecAddHost(const float *, const float *, float *, int);
  char *loadOclProgramSource(const char *, const char *, size_t *);
  void Cleanup();
  // Function Declaration
  bool ReadRawFileCPU(WCHAR * lpDataFile_i, int nWidth_i, int nHeight_i,
	  int nSlices_i);

  void ConvertDataToRGBA(void);

  // File has only image data. The dimension of the data should be known.
  skull_uImageWidth = 256;
  skull_uImageHeight = 256;
  skull_uImageCount = 109;

  luminance_buffer_size = sizeof(char) * skull_uImageWidth * skull_uImageHeight * skull_uImageCount;
  rgba_buffer_size = luminance_buffer_size * 4;

  // Allocate Host-Memory
  // Holds the luminance buffer
  hostBuffer = (char *)malloc(luminance_buffer_size);
  if (NULL == hostBuffer)
  {
	    printf(
	        "CPU Memory Fatal Error = Can Not Allocate Enough Memory For Host "
	        "luminance buffer.\n Exitting ...\n");
	    Cleanup();
	    exit(EXIT_FAILURE);
  }
  memset(hostBuffer, 0, luminance_buffer_size);

  // Holds the RGBA buffer
  hostRGBABuffer = (char *)malloc(rgba_buffer_size);

  if (NULL == hostRGBABuffer)
  {
	  printf("CPU Memory Fatal Error = Can Not Allocate Enough Memory For Host "
		  "RGBA buffer.\n Exitting ...\n");
		  Cleanup();
		  exit(EXIT_FAILURE);
  }
  memset(hostRGBABuffer, 0, rgba_buffer_size);

  ReadRawFileCPU(skull_data_file, 256, 256, 109);

  // Get OpenCL Supporting Platform's ID
  ret_ocl = clGetPlatformIDs(1, &oclPlatformID, NULL);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clGetPlatformIDs Failed : %d. Exitting Now... \n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Get OpenCL Supporting GPU Device's ID
  ret_ocl = clGetDeviceIDs(oclPlatformID, CL_DEVICE_TYPE_GPU, 1,
                           &oclComputeDeviceID, NULL);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clGetDeviceIDs() Failed : %d. Exitting Now...\n", ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  char gpu_name[255];
  clGetDeviceInfo(oclComputeDeviceID, CL_DEVICE_NAME, sizeof(gpu_name),
                  &gpu_name, NULL);
  printf("gpu_name: %s\n", gpu_name);

  // Create OpenCL Compute Context
  oclContext =
      clCreateContext(NULL, 1, &oclComputeDeviceID, NULL, NULL, &ret_ocl);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clCreateContext() Failed : %d. Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Allocate Device-Memory
  deviceBuffer =
	  clCreateBuffer(oclContext, CL_MEM_READ_ONLY, luminance_buffer_size, NULL, &ret_ocl);
  if (CL_SUCCESS != ret_ocl) {
	  printf("OpenCL Error - clCreateBuffer() Failed  For device buffer: %d. "
		  "Exitting Now...\n",
		  ret_ocl);
	  Cleanup();
	  exit(EXIT_FAILURE);
  }

  deviceRGBABuffer =
	  clCreateBuffer(oclContext, CL_MEM_READ_ONLY, rgba_buffer_size, NULL, &ret_ocl);
  if (CL_SUCCESS != ret_ocl) {
	  printf("OpenCL Error - clCreateBuffer() Failed  For device RGBA buffer: %d. "
		  "Exitting Now...\n",
		  ret_ocl);
	  Cleanup();
	  exit(EXIT_FAILURE);
  }

  // Create Command Queue
  oclCommandQueue =
      clCreateCommandQueue(oclContext, oclComputeDeviceID, 0, &ret_ocl);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clCreateCommandQueue() Failed : %d. Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Create OpenCL Program From .cl
  oclSourceCode = loadOclProgramSource("image.cl", "", &sizeKernelCodeLength);

  oclProgram =
      clCreateProgramWithSource(oclContext, 1, (const char **)&oclSourceCode,
                                &sizeKernelCodeLength, &ret_ocl);
  if (CL_SUCCESS != ret_ocl) {
    printf(
        "OpenCL Error - clCreateProgramWithSource() Failed : %d. Exitting Now...\n",
        ret_ocl);
    Cleanup();
    exit(0);
  }

  // Build OpenCL Program
  ret_ocl = clBuildProgram(oclProgram, 0, NULL, NULL, NULL, NULL);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCl Error - clBuildProgram() Failed : %d. Exitting Now...\n",
           ret_ocl);

    size_t len;
    char buffer[2048];
    clGetProgramBuildInfo(oclProgram, oclComputeDeviceID, CL_PROGRAM_BUILD_LOG,
                          sizeof(buffer), buffer, &len);
    printf("OpenCL Program Build Log : %s\n", buffer);

    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Create OpenCL Kernel By Passing Kernel Function Name That We Used In .cl
  // File
  oclKernel = clCreateKernel(oclProgram, "ImageLoad", &ret_ocl);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clCreateKernel() Failed : %d. Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Set OpenCL kernel Argument. Our OpenCL Kernel Has 4 Arguments 0,1,2,3
  // Set 0 Based 0th Argument
  ret_ocl = clSetKernelArg(oclKernel, 0, sizeof(cl_mem), (void *)&deviceBuffer);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clSetKernelArg() Failed  For 1st Argument: %d. "
           "Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Set 0 Based 2nd Argument i.e len
  // iNumberOfArrayElements maps To len Param Of kernel Function in .cl File
  ret_ocl = clSetKernelArg(oclKernel, 1, sizeof(cl_int),
	  (void *)&luminance_buffer_size);
  if (CL_SUCCESS != ret_ocl) {
	  printf("OpenCL Error - clSetKernelArg() Failed  For 3rd Argument: %d. "
		  "Exitting Now...\n",
		  ret_ocl);
	  Cleanup();
	  exit(EXIT_FAILURE);
  }

  // Set 0 Based 3rd Argument
  ret_ocl = clSetKernelArg(oclKernel, 2, sizeof(cl_mem), (void *)&deviceRGBABuffer);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clSetKernelArg() Failed  For 2nd Argument: %d. "
           "Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Set 0 Based 4th Argument
  ret_ocl = clSetKernelArg(oclKernel, 3, sizeof(cl_int),
	  (void *)&rgba_buffer_size);
  if (CL_SUCCESS != ret_ocl) {
	  printf("OpenCL Error - clSetKernelArg() Failed  For 3rd Argument: %d. "
		  "Exitting Now...\n",
		  ret_ocl);
	  Cleanup();
	  exit(EXIT_FAILURE);
  }

  // Write Above 'input' cpu Buffer To Device Memory
  ret_ocl = clEnqueueWriteBuffer(oclCommandQueue, deviceBuffer, CL_FALSE, 0,
                                 luminance_buffer_size, hostBuffer, 0, NULL, NULL);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clEnqueueWriteBuffer() Failed  For 1st Input "
           "Buffer: %d. "
           "Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Run The kernel
  globalWorkSize = roundGlobalSizeToNearestMultipleOfLocalSize(
      localWorkSize, luminance_buffer_size);

  // Start Timer
  StopWatchInterface *timer = NULL;
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);

  ret_ocl =
      clEnqueueNDRangeKernel(oclCommandQueue, oclKernel, 1, NULL,
                             &globalWorkSize, &localWorkSize, 0, NULL, NULL);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clEnqueueNDRangeKernel() Failed  For 2nd Input "
           "Buffer: %d. "
           "Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // Finish OpenCL Command Queue
  clFinish(oclCommandQueue);

  // Stop Timer
  sdkStopTimer(&timer);
  image_load_timeOnGPU = sdkGetTimerValue(&timer);
  sdkDeleteTimer(&timer);

  // Read Back Result From The Device (i.e. From deviceRGBABuffer) Into CPU
  // Variable(i.e hostRGBABuffer)
  ret_ocl = clEnqueueReadBuffer(oclCommandQueue, deviceRGBABuffer, CL_TRUE, 0, rgba_buffer_size,
                                hostRGBABuffer, 0, NULL, NULL);
  if (CL_SUCCESS != ret_ocl) {
    printf("OpenCL Error - clEnqueueReadBuffer() Failed  For 2nd Input "
           "Buffer: %d. "
           "Exitting Now...\n",
           ret_ocl);
    Cleanup();
    exit(EXIT_FAILURE);
  }

  // CPU execution
  memset(hostRGBABuffer, 0, rgba_buffer_size);

  ConvertDataToRGBA();  

  printf("image_load_timeOnGPU = %.6f (ms)\n",
	  image_load_timeOnGPU);
  printf("image_load_timeOnCPU = %.6f (ms)\n",
	  image_load_timeOnCPU);

  Cleanup();

  return (0);
 }

void Cleanup(void) {
  // Code

  // OpenCL Cleanup

  if (NULL != oclSourceCode) {
    free((void *)oclSourceCode);
    oclSourceCode = NULL;
  }

  if (NULL != oclKernel) {
    clReleaseKernel(oclKernel);
    oclKernel = NULL;
  }

  if (NULL != oclProgram) {
    clReleaseProgram(oclProgram);
    oclProgram = NULL;
  }

  if (NULL != oclCommandQueue) {
    clReleaseCommandQueue(oclCommandQueue);
    oclCommandQueue = NULL;
  }

  if (NULL != oclContext) {
    clReleaseContext(oclContext);
    oclContext = NULL;
  }

  if (NULL != deviceBuffer) {
    clReleaseMemObject(deviceBuffer);
	deviceBuffer = NULL;
  }

  if (NULL != deviceRGBABuffer) {
    clReleaseMemObject(deviceRGBABuffer);
	deviceRGBABuffer = NULL;
  }

  // Free Allocated Host-Memory
  if (NULL != hostBuffer) {
    free(hostBuffer);
	hostBuffer = NULL;
  }

  if (NULL != hostRGBABuffer) {
    free(hostRGBABuffer);
	hostRGBABuffer = NULL;
  }

}

size_t roundGlobalSizeToNearestMultipleOfLocalSize(int local_size,
  unsigned int global_size) {

  // Code
  unsigned int r = global_size % local_size;

  if (0 == r) {
    return (global_size);
  } else {
    return (global_size + local_size - r);
  }
}

char *loadOclProgramSource(const char *filename, const char *preamble,
  size_t *sizeFinalLength) {

  // Variable Declarations
  FILE *pFile = NULL;
  size_t sizeSourceLength;

  pFile = fopen(filename, "rb"); // binary read
  if (NULL == pFile) {
    return NULL;
  }

  size_t sizePreambleLength = (size_t)strlen(preamble);

  // Get The Length Of The Soruce Code
  fseek(pFile, 0, SEEK_END);
  sizeSourceLength = ftell(pFile);
  fseek(pFile, 0, SEEK_SET); // Rest To Beginning

  // Allocate A BUffer For The Source Code String An Read It In
  char *sourceString =
      (char *)malloc(sizeSourceLength + sizePreambleLength + 1);
  memcpy(sourceString, preamble, sizePreambleLength);

  if (fread((sourceString) + sizePreambleLength, sizeSourceLength, 1, pFile) !=
      1) {
    fclose(pFile);
    free(sourceString);
    return (0);
  }

  // Close The File And Return The Total Length Of The Combined (Preamble +
  // Source) String
  fclose(pFile);
  if (0 != sizeFinalLength) {
    *sizeFinalLength = sizeSourceLength + sizePreambleLength;
  }

  sourceString[sizeSourceLength + sizePreambleLength] = '\0';

  return (sourceString);
}

bool ReadRawFileCPU(WCHAR *lpDataFile_i, int nWidth_i, int nHeight_i,
	int nSlices_i) {
	// Code
	HANDLE hRawFile = CreateFileW(lpDataFile_i /*L"F:\\head256x256x109"*/,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hRawFile) {
		printf("CreateFile  Failed Error : %d\n", GetLastError());
		return false;
	}
	printf("CreateFile  Successed\n");

	DWORD dwBytesRead = 0;
	if (!ReadFile(hRawFile, hostBuffer,
		luminance_buffer_size,
		&dwBytesRead,
		NULL)) {
		printf("ReadFile Failed Error : %d\n", GetLastError());
		return false;
	}
	CloseHandle(hRawFile);

	return true;
}


void ConvertDataToRGBA(void)
{
	// Start Timer
	StopWatchInterface *image_load_timer = NULL;
	sdkCreateTimer(&image_load_timer);
	sdkStartTimer(&image_load_timer);

	// Convert the data to RGBA data.
	// Here we are simply putting the same value to R, G, B and A channels.
	// Usually for raw data, the alpha value will be constructed by a threshold
	// value given by the user
	for (int nIndx = 0;
		nIndx < luminance_buffer_size;
		++nIndx) {
		hostRGBABuffer[nIndx * 4] = hostBuffer[nIndx];
		hostRGBABuffer[nIndx * 4 + 1] = hostBuffer[nIndx];
		hostRGBABuffer[nIndx * 4 + 2] = hostBuffer[nIndx];
		hostRGBABuffer[nIndx * 4 + 3] = hostBuffer[nIndx];
	}

	// Stop Timer
	sdkStopTimer(&image_load_timer);
	image_load_timeOnCPU = sdkGetTimerValue(&image_load_timer);
	sdkDeleteTimer(&image_load_timer);

	return;
}
