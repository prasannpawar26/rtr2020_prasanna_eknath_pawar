#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <CL/opencl.h>

cl_int				ret_ocl;
cl_platform_id		oclPlatformID;
cl_device_id		oclComputeDeviceID;
cl_context			oclContext;
cl_command_queue	oclCommandQueue;
cl_program			oclProgram;
cl_kernel			oclKernel;

float *hostInput1 = NULL;
float *hostInput2 = NULL;
float *hostOutput = NULL;

cl_mem deviceInput1 = NULL;
cl_mem deviceInput2 = NULL;
cl_mem deviceOutput = NULL;

int main(void)
{
	// function declarations
	void cleanup(void);
	
	// variable declarations
	int inputLength;
	// code

	inputLength = 5;

	hostInput1 = (float *)malloc(inputLength * sizeof(float));
	if (NULL == hostInput1)
	{
		printf("CPU Memory Fatal Error = Can Not Allocate Enough Memory For hostInput1\n");
		exit(EXIT_FAILURE);
	}

	hostInput2 = (float *)malloc(inputLength * sizeof(float));
	if (NULL == hostInput2)
	{
		printf("CPU Memory Fatal Error = Can Not Allocate Enough Memory For hostInput2\n");
		free(hostInput1);

		exit(EXIT_FAILURE);
	}

	hostOutput = (float *)malloc(inputLength * sizeof(float));
	if (NULL == hostOutput)
	{
		printf("CPU Memory Fatal Error = Can Not Allocate Enough Memory For hostOutput\n");
		free(hostInput1);
		free(hostInput2);

		exit(EXIT_FAILURE);
	}

	//fill above input host vectors
	hostInput1[0] = 101.0;
	hostInput1[1] = 201.0;
	hostInput1[2] = 301.0;
	hostInput1[3] = 401.0;
	hostInput1[4] = 501.0;

	hostInput2[0] = 201.0;
	hostInput2[1] = 201.0;
	hostInput2[2] = 201.0;
	hostInput2[3] = 201.0;
	hostInput2[4] = 201.0;


	// get OpenCL supporting platforms ID
	ret_ocl = clGetPlatformIDs(1, &oclPlatformID, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clGetPlatformIDs Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	//get OpenCL supporting GPU device's ID
	ret_ocl = clGetDeviceIDs(oclPlatformID, CL_DEVICE_TYPE_GPU, 1, &oclComputeDeviceID, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clGetDeviceIDs Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// create OpenCL compute context
	oclContext = clCreateContext(NULL, 1, &oclComputeDeviceID, NULL, NULL, &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateContext Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// create command queue
	oclCommandQueue = clCreateCommandQueue(oclContext, oclComputeDeviceID, 0, &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateCommandQueue Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	const char* oclKernelSource =
		"__kernel void VecAdd(__global float *in1, __global float *in2, __global float *out, int len)" \
		"{" \
			"int i = get_global_id(0);" \

			"if(i < len)" \
			"{" \
				"out[i] = in1[i] + in2[i];" \
			"}" \
		"}";

	oclProgram = clCreateProgramWithSource(oclContext, 1, &oclKernelSource, NULL, &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateProgramWithSource Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// build OpenCL program
	ret_ocl = clBuildProgram(oclProgram, 0, NULL, NULL, NULL, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clBuildProgram Failed : %d Exitting Now...\n", ret_ocl);

		size_t len;
		char buffer[2048];
		
		clGetProgramBuildInfo(oclProgram, oclComputeDeviceID, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("OpenCL Program Build Log : %s\n", buffer);

		cleanup();
		exit(EXIT_FAILURE);
	}

	//create OpenCL kernel by passing kernel function name that we used
	oclKernel = clCreateKernel(oclProgram, "VecAdd", &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateKernel Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	int size = inputLength * sizeof(cl_float);
	deviceInput1 = clCreateBuffer(oclContext, CL_MEM_READ_ONLY, size, NULL, &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateBuffer Failed For deviceInput1 : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	deviceInput2 = clCreateBuffer(oclContext, CL_MEM_READ_ONLY, size, NULL, &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateBuffer Failed For deviceInput2 : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	deviceOutput = clCreateBuffer(oclContext, CL_MEM_WRITE_ONLY, size, NULL, &ret_ocl);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clCreateBuffer Failed For deviceOutput : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// set OpenCL kernel arguments.
	ret_ocl = clSetKernelArg(oclKernel, 0, sizeof(cl_mem), (void *)&deviceInput1);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clSetKernelArg Failed For 0th Index Argument : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	ret_ocl = clSetKernelArg(oclKernel, 1, sizeof(cl_mem), (void *)&deviceInput2);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clSetKernelArg Failed For 1st Index Argument : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	ret_ocl = clSetKernelArg(oclKernel, 2, sizeof(cl_mem), (void *)&deviceOutput);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clSetKernelArg Failed For 2nd Index Argument : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	ret_ocl = clSetKernelArg(oclKernel, 3, sizeof(cl_int), (void *)&inputLength);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clSetKernelArg Failed For 3rd Index Argument : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// write above input buffers to device memory
	ret_ocl = clEnqueueWriteBuffer(oclCommandQueue, deviceInput1, CL_FALSE, 0, size, hostInput1, 0, NULL, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clEnqueueWriteBuffer Failed For deviceInput 1 : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	ret_ocl = clEnqueueWriteBuffer(oclCommandQueue, deviceInput2, CL_FALSE, 0, size, hostInput2, 0, NULL, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clEnqueueWriteBuffer Failed For deviceInput 2 : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// run the kernel
	size_t global_size = 5;

	// 1 in following argument list is for N-D i.e N= 1 means 1-Dimension
	ret_ocl = clEnqueueNDRangeKernel(oclCommandQueue, oclKernel, 1, NULL, &global_size, NULL, 0, NULL, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clEnqueueNDRangeKernel Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	// finish OpenCL command queue
	clFinish(oclCommandQueue);

	// read back the result
	ret_ocl = clEnqueueReadBuffer(oclCommandQueue, deviceOutput, CL_TRUE/*Sync-Blocking*/, 0, size, hostOutput, 0, NULL, NULL);
	if (CL_SUCCESS != ret_ocl)
	{
		printf("OpenCL Error - clEnqueueReadBuffer Failed : %d Exitting Now...\n", ret_ocl);
		cleanup();

		exit(EXIT_FAILURE);
	}

	//result
	int i;
	for (i = 0; i < 5; i++)
	{
		printf("%f + %f = %f\n", hostInput1[i], hostInput2[i], hostOutput[i]);
	}

	cleanup();

	return 0;
}


void cleanup(void)
{
	// code
	if (oclKernel)
	{
		clReleaseKernel(oclKernel);
		oclKernel = NULL;
	}

	if (oclProgram)
	{
		clReleaseProgram(oclProgram);
		oclProgram = NULL;
	}

	if (oclCommandQueue)
	{
		clReleaseCommandQueue(oclCommandQueue);
		oclCommandQueue = NULL;
	}

	if (oclContext)
	{
		clReleaseContext(oclContext);
		oclContext = NULL;
	}

	if (deviceOutput)
	{
		clReleaseMemObject(deviceOutput);
		deviceOutput = NULL;
	}

	if (deviceInput2)
	{
		clReleaseMemObject(deviceInput2);
		deviceInput2 = NULL;
	}

	if (deviceInput1)
	{
		clReleaseMemObject(deviceInput1);
		deviceInput1 = NULL;
	}

	if (hostOutput)
	{
		free(hostOutput);
		hostOutput = NULL;
	}

	if (hostInput2)
	{
		free(hostInput2);
		hostInput2 = NULL;
	}

	if (hostInput1)
	{
		free(hostInput1);
		hostInput1 = NULL;
	}
}
