#include <Windows.h>
#include <tlhelp32.h> 

#include <psapi.h>
#include <stdio.h>

#define MAX_FILE_PATH 1024

int main(int argc, char *argv[])
{

  // Function Declarations
  int Show_ProcessMemoryCounters(HANDLE);
  int Show_MemoryBasicInformation(HANDLE);
  int Show_IOCounters(HANDLE);
  int Show_ProcessAffinityMask(HANDLE);
  int Show_ProcessWorkingSetSize(HANDLE);
  //int Show_ProcessBasicInformation(HANDLE);
  int Show_ListProcessModules(HANDLE);
  // Variable Declarations
  int process_id = 12036;

  HANDLE hProcessHandle = INVALID_HANDLE_VALUE;
  DWORD dwError = 0;
  DWORD dwRet = 0;
  TCHAR szExePath[MAX_FILE_PATH];

  //Code
  printf("%S : %S : %ld Start\n", __FILEW__, __FUNCTIONW__, __LINE__);

  hProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, process_id);
  if (INVALID_HANDLE_VALUE == hProcessHandle)
  {
    dwError = GetLastError();
    printf("%S : %S : %ld OpenProcess Failed. Error => %d\n", __FILEW__,
           __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);
  }

  memset(szExePath, 0, MAX_FILE_PATH * sizeof(TCHAR));
  dwRet = GetProcessImageFileName(hProcessHandle, szExePath, MAX_FILE_PATH);
  if (0 == dwRet)
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetModuleFileNameW Failed. Error => %d\n", __FILEW__,
           __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

  DWORD dwHandleCount;
  if (!GetProcessHandleCount(hProcessHandle, &dwHandleCount))
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetProcessHandleCount Failed. Error => %d\n", __FILEW__,
           __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

  DWORD dwProcessVersion = 0;
  dwProcessVersion = GetProcessVersion(process_id);
  if (0 == dwProcessVersion)
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetProcessVersion Failed. Error => %d\n",
           __FILEW__, __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

  printf("\n\n");
  printf("Process Image Path :\t\t\t %S\n", szExePath);
  printf("\n\n");
  printf("Process Version :\t\t\t %d\n", dwProcessVersion);
  printf("\n\n");
  printf("Process Handle Count :\t\t\t %d\n\n", dwHandleCount);

  printf("\n\n");
  Show_IOCounters(hProcessHandle);
  printf("\n\n");
  Show_ProcessMemoryCounters(hProcessHandle);
  printf("\n\n");
  Show_ProcessAffinityMask(hProcessHandle);
  printf("\n\n");
  Show_MemoryBasicInformation(hProcessHandle);
  printf("\n\n");
  Show_ListProcessModules(hProcessHandle);
  /*SYSTEM_INFO SystemInfo;
  ZeroMemory(&SystemInfo, sizeof(SystemInfo));
  GetSystemInfo(&SystemInfo);*/

  CloseHandle(hProcessHandle);

  printf("%S : %S : %ld End\n", __FILEW__, __FUNCTIONW__, __LINE__);

  return 0;
}

int Show_ProcessWorkingSetSize(HANDLE hProcessHandle)
{
  // Variable Declarations
  SIZE_T minimumWorkingSetSize;
  SIZE_T maximumWorkingSetSize;
  DWORD dwError;

  // Code
  if (!GetProcessWorkingSetSize(hProcessHandle, &minimumWorkingSetSize,
                                &maximumWorkingSetSize))
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetProcessWorkingSetSize Failed. Error => %d\n",
           __FILEW__, __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

   printf(
      "ProcessWorkingSetSize : The  working set of a process is the set of "
      "memory pages currently visible to the "
      "process in physical RAM memory \n");
  printf("\tMinimumWorkingSetSize : %lld\n", minimumWorkingSetSize);
  printf("\tMaximumWorkingSetSize : %lld\n\n", maximumWorkingSetSize);

  return 0;
}

int Show_ProcessAffinityMask(HANDLE hProcessHandle)
{
  // Variable Declarations
  DWORD_PTR lProcessAffinityMask;
  DWORD_PTR lSystemAffinityMask;
  DWORD dwError;

  // Code
  if (!GetProcessAffinityMask(hProcessHandle, &lProcessAffinityMask,
                              &lSystemAffinityMask))
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetProcessAffinityMask Failed. Error => %d\n",
           __FILEW__, __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

  printf("Process Affinity Mask : \n");
  printf("\t ProcessAffinityMask : %lld\n", lProcessAffinityMask);
  printf("\t SystemAffinityMask : %lld\n", lSystemAffinityMask);

  return 0;
}

int Show_IOCounters(HANDLE hProcessHandle)
{
  // Variable Declarations
  IO_COUNTERS ioCounter;
  DWORD dwError = 0;

  // Code
  if (!GetProcessIoCounters(hProcessHandle, &ioCounter))
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetProcessIoCounters Failed. Error => %d\n",
           __FILEW__, __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

  printf("I/O Operations Performed By The Process :\n");
  printf("\t ReadOperationCount: %lld\n", ioCounter.ReadOperationCount);
  printf("\t WriteOperationCount : %lld\n", ioCounter.WriteOperationCount);
  printf("\t OtherOperationCount : %lld\n", ioCounter.OtherOperationCount);
  printf("\t ReadTransferCount : %lld\n", ioCounter.ReadTransferCount);
  printf("\t WriteTransferCount : %lld\n", ioCounter.WriteTransferCount);
  printf("\t OtherTransferCount : %lld\n\n", ioCounter.OtherTransferCount);

  return 0;
}

int Show_ProcessMemoryCounters(HANDLE hProcessHandle) {
  // Variable Declarations
  DWORD dwError;
  PROCESS_MEMORY_COUNTERS processMemoryCounters;
  if (!GetProcessMemoryInfo(hProcessHandle, &processMemoryCounters,
                            sizeof(processMemoryCounters)))
  {
    dwError = GetLastError();
    printf("%S : %S : %ld GetProcessMemoryInfo Failed. Error => %d\n",
           __FILEW__, __FUNCTIONW__, __LINE__, dwError);
    CloseHandle(hProcessHandle);

    return -1;
  }

  printf("\n\n");
  printf("Process Memory Usage : \n");
  printf("\t PageFaultCount : %d\n", processMemoryCounters.PageFaultCount);
  printf("\t PagefileUsage : %lld\n", processMemoryCounters.PagefileUsage);
  printf("\t PeakPagefileUsage : %lld\n",
         processMemoryCounters.PeakPagefileUsage);
  printf("\t PeakWorkingSetSize : %lld\n",
         processMemoryCounters.PeakWorkingSetSize);
  printf("\t QuotaNonPagedPoolUsage : %lld\n",
         processMemoryCounters.QuotaNonPagedPoolUsage);
  printf("\t QuotaPagedPoolUsage : %lld\n",
         processMemoryCounters.QuotaPagedPoolUsage);
  printf("\t QuotaPeakNonPagedPoolUsage : %lld\n",
         processMemoryCounters.QuotaPeakNonPagedPoolUsage);
  printf("\t QuotaPeakPagedPoolUsage : %lld\n",
         processMemoryCounters.QuotaPeakPagedPoolUsage);
  printf("\t WorkingSetSize : %lld\n", processMemoryCounters.WorkingSetSize);

  return 0;
}

int Show_MemoryBasicInformation(HANDLE hProcessHandle)
{
  // Variable Declarations;
  unsigned char *pAddress;
  DWORD dwError = 0;
  MEMORY_BASIC_INFORMATION MemoryBasicInformation;
  ZeroMemory(&MemoryBasicInformation, sizeof(MemoryBasicInformation));

  // Code
  printf(
      "Information about a range of pages within the virtual address space  of "
      "process :\n");

  pAddress = NULL;
  while (1)
  {
    printf("\t ---------------------------------------------------------------------------\n\n");

    //
    // 1. The VirtualQueryEx function determines the attributes of the first page in the region and then scans subsequent pages until it scans the entire range of pages, or until it encounters a page with a nonmatching set of attributes.
    // 2. If a shared copy-on-write page is modified, it becomes private to the process that modified the page. However, the VirtualQueryEx function will continue to report such pages as MEM_MAPPED (for data views) or MEM_IMAGE (for executable image views) rather than MEM_PRIVATE
    // 3. 
    if (0 == VirtualQueryEx(hProcessHandle, pAddress, &MemoryBasicInformation,
                            sizeof(MemoryBasicInformation))) {
      dwError = GetLastError();
      printf("%S : %S : %ld VirtualQueryEx Failed. Error => %d\n", __FILEW__,
             __FUNCTIONW__, __LINE__, dwError);
      CloseHandle(hProcessHandle);

      break;
    }

    printf("\t Base Address Of Region : %p\n", pAddress);
    printf("\t Region Size : %llu\n", MemoryBasicInformation.RegionSize);

    /*printf("\t Base Address Of Region %zu:\n",
           MemoryBasicInformation.BaseAddress);*/
   
    switch (MemoryBasicInformation.State)
    {
      // Code
      case MEM_COMMIT: {
        //
        // Indicates committed pages for which physical storage has been allocated, either in memory or in the paging file on disk.
        //
        printf("\t State of the pages in the region. : MEM_COMMIT\n");
      } break;

      case MEM_FREE: {
        //
        // Indicates free pages not accessible to the calling process and available to be allocated
        //
        printf("\t State of the pages in the region. : MEM_FREE\n");
      } break;

      case MEM_RESERVE: {
        printf("\t State of the pages in the region. : MEM_RESERVE\n");
      } break;

      default: {
        printf("\t State of the pages in the region. : %d\n",
               MemoryBasicInformation.State);
      } break;
    }

    switch (MemoryBasicInformation.AllocationProtect) {
      // Code
      case PAGE_EXECUTE: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_EXECUTE\n");
      } break;

      case PAGE_EXECUTE_READ: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_EXECUTE_READ\n");
      } break;

      case PAGE_EXECUTE_READWRITE: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_EXECUTE_READWRITE\n");
      } break;

      case PAGE_EXECUTE_WRITECOPY: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_EXECUTE_WRITECOPY\n");
      } break;

      case PAGE_NOACCESS: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_NOACCESS\n");
      } break;

      case PAGE_READONLY: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_READONLY\n");
      } break;
      case PAGE_READWRITE: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_READWRITE\n");
      } break;

      case PAGE_WRITECOPY: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_WRITECOPY\n");
      } break;

      case PAGE_TARGETS_INVALID: {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : PAGE_TARGETS_INVALID\n");
      } break;

      default:
      {
        printf(
            "\t The memory protection option when the region was initially "
            "allocated : %d\n",
            MemoryBasicInformation.AllocationProtect);
      } break;
    }

    switch (MemoryBasicInformation.Protect) {
      // Code
      case PAGE_EXECUTE: {
        printf(
            "\t The access protection of the pages in the region.: PAGE_EXECUTE\n");
      } break;

      case PAGE_EXECUTE_READ: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_EXECUTE_READ\n");
      } break;

      case PAGE_EXECUTE_READWRITE: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_EXECUTE_READWRITE\n");
      } break;

      case PAGE_EXECUTE_WRITECOPY: {
        printf(
            "\t The access protection of the pages in the region.: PAGE_EXECUTE_WRITECOPY\n");
      } break;

      case PAGE_NOACCESS: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_NOACCESS\n");
      } break;

      case PAGE_READONLY: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_READONLY\n");
      } break;
      case PAGE_READWRITE: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_READWRITE\n");
      } break;

      case PAGE_WRITECOPY: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_WRITECOPY\n");
      } break;

      case PAGE_TARGETS_INVALID: {
        printf(
            "\t The access protection of the pages in the region. : PAGE_TARGETS_INVALID\n");
      } break;

      default: {
        printf(
            "\t The access protection of the pages in the region. : %d\n",
               MemoryBasicInformation.Protect);
      } break;
    }

    switch (MemoryBasicInformation.Type) {
      // Code
      case MEM_IMAGE: {
        //
        // Indicates that the memory pages within the region are mapped into the view of an image section.
        //
        printf("\t Type of the pages in the region. : MEM_IMAGE\n");
      } break;

      case MEM_MAPPED: {
        //
        // Indicates that the memory pages within the region are mapped into the view of a section.
        //
        printf("\t Type of the pages in the region. : MEM_MAPPED\n");
      } break;

      case MEM_PRIVATE: {
        //
        // Indicates that the memory pages within the region are private (that is, not shared by other processes).
        //
        printf("\t Type of the pages in the region. : MEM_PRIVATE\n");
      } break;
      
      default:
      {
        printf("\t Type of the pages in the region. : %d\n",
               MemoryBasicInformation.Type);
      } break;
    }

    pAddress += MemoryBasicInformation.RegionSize;
  }

  return 0;
}

//int Show_ProcessBasicInformation(HANDLE hProcessHandle)
//{
//  // Variable Declarations
//  ULONG ulReturnLength = 0;
//  NTSTATUS ntStatus;
//  typedef  NTSTATUS (WINAPI *pfnZwQueryInformationProcess)(
//      _In_ HANDLE ProcessHandle, _In_ PROCESSINFOCLASS ProcessInformationClass,
//      _Out_ PVOID ProcessInformation, _In_ ULONG ProcessInformationLength,
//      _Out_opt_ PULONG ReturnLength);
//
//  pfnZwQueryInformationProcess ZwQueryInfoProcess = NULL;
//  
//  HMODULE hModuleNTDLL = LoadLibrary(TEXT("ntdll.dll"));
//  if (NULL == hModuleNTDLL)
//  {
//    printf("LoadLibrary ... FAILED Error : %d\n\n", GetLastError());
//    return 1;
//  }
//
//  ZwQueryInfoProcess = (pfnZwQueryInformationProcess)GetProcAddress(
//      hModuleNTDLL, "ZwQueryInformationProcess");
//  if (ZwQueryInfoProcess == NULL)
//  {
//    printf("GetProcAddress ... FAILED Error : %d\n\n", GetLastError());
//    CloseHandle(hModuleNTDLL);
//    return 1;
//  }
//
//  PROCESS_BASIC_INFORMATION process_basic_information;
//  ZeroMemory(&process_basic_information, sizeof(process_basic_information));
//
//  ntStatus = ZwQueryInfoProcess(
//      hProcessHandle, ProcessBasicInformation, &process_basic_information,
//      sizeof(process_basic_information), &ulReturnLength);
//
//  if (!NT_SUCCESS(ntStatus))
//  {
//    CloseHandle(hModuleNTDLL);
//    return 1;
//  }
//
//  CloseHandle(hModuleNTDLL);
//  return 0;
//}

int Show_ListProcessModules(HANDLE hProcessHandle)
{
  // Variable Declarations
  HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
  MODULEENTRY32 moduleEntry;

  // Code

  //
  // Take a snapshot of all modules in the specified process.
  //
  hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetProcessId(hProcessHandle));
  if (INVALID_HANDLE_VALUE == hModuleSnap)
  {
    printf("CreateToolhelp32Snapshot...Failed Error : %d\n\n", GetLastError());
    return -1;
  }

  ZeroMemory(&moduleEntry, sizeof(moduleEntry));
  moduleEntry.dwSize = sizeof(moduleEntry);

  //
  //  Retrieve information about the first module, and exit if unsuccessful 
  //
  if (!Module32First(hModuleSnap, &moduleEntry))
  {
    printf("Module32First...Failed Error : %d\n\n", GetLastError());
    CloseHandle(hModuleSnap);
    return -1;
  }

  //
  //  Now walk the module list of the process, and display information about each module.
  //

  printf("List Of Process Modules\n\n");
  do
  {
    printf("---------------------------------------------------------------------\n");
    printf("\tModuleName : %ws\n", moduleEntry.szModule);
    printf("\tModulePath : %ws\n", moduleEntry.szExePath);
    printf("\tBaseAddress : %p\n", moduleEntry.modBaseAddr);
    printf("\tModuleSize : %lu Bytes\n", moduleEntry.modBaseSize);
    printf("\n\n");

    ZeroMemory(&moduleEntry, sizeof(moduleEntry));
    moduleEntry.dwSize = sizeof(moduleEntry);
  } while (Module32Next(hModuleSnap, &moduleEntry));

  CloseHandle( hModuleSnap );

  return 0;
}
