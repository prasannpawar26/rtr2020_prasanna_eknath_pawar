#define UNICODE

#include <Windows.h>
#include <stdio.h>

#include "28_DynamicDeathlyHallow.h"

#include <gl/GL.h>
#include <gl/glu.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#define PI 3.1415

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

float gTriangleVertices[3][3] = { {0.0f, 1.0f, 0.0f}, {-1.0f, -1.0f, 0.0f}, {1.0f, -1.0f, 0.0f}};

float gDistanceBetween_TriangleVertices[3] = { 0.0f, 0.0f, 0.0f};

float gSemiPerimeter = 0.0f;
float gRadius = 0.0f;

float gInCenter_Coordinates[3] = {0.0f, 0.0f, 0.0f};

float gMidPoint_BC[3] = {0.0f, 0.0f, 0.0f};

const int gPoints = 1000;

float gAngle = 0.0f;

float gTriangleTranslation[2] = { -5.0f, -5.0f};
float gCircleTranslation[2] = { 5.0f, -5.0f};
float gLineTranslation = 5.0f;

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
	// Function Declarations
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);
	void Update(void);

	// Variable Declarations
	bool pep_bDone = false;
	WNDCLASSEX pep_wndClass;
	TCHAR pep_szAppName[] = TEXT("28_DynamicDeathlyHallow");
	MSG pep_msg;
	HWND pep_hwnd;

	// Code
	if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("28_DynamicDeathlyHallow"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.hInstance = pep_hInstance;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szAppName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szAppName,
		TEXT("28_DynamicDeathlyHallow"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
		PEP_WIDTH,
		PEP_HEIGHT,
		NULL,
		NULL,
		pep_hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	Initialize();

	ShowWindow(pep_gHwnd, pep_iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
	// Function Declarations
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void ReSize(int, int);

	switch (pep_iMsg)
	{
		case WM_DESTROY:
		{
			UnInitialize();
			PostQuitMessage(0);
		}
		break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
		break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
		break;

		case WM_SIZE:
		{
			ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
		}
		break;

		case WM_ERASEBKGND:
		{
			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(pep_hwnd);
		}
		break;

		case WM_KEYDOWN:
		{
			switch (pep_wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(pep_hwnd);
				}
				break;

				case 'F':
				case 'f':
				{
					ToggledFullScreen();
				}
				break;
			}
		}
		break;
	}

	return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
	// Variable Declarations
	MONITORINFO pep_mi = { sizeof(MONITORINFO) };

	if (false == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
		{
			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

		pep_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		pep_gbFullScreen = false;
	}
}

void Initialize(void)
{
	// Function Declarations
	void ReSize(int, int);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pep_pfd;
	int pep_iPixelFormatIndex;

	// Code
	pep_gHdc = GetDC(pep_gHwnd);

	ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_pfd.nVersion = 1;
	pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pep_pfd.iPixelType = PFD_TYPE_RGBA;
	pep_pfd.cColorBits = 32;
	pep_pfd.cRedBits = 8;
	pep_pfd.cGreenBits = 8;
	pep_pfd.cBlueBits = 8;
	pep_pfd.cAlphaBits = 8;

	pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
	if (0 == pep_iPixelFormatIndex)
	{
		fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
	{
		fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "wglCreateContext Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	// Distance Of BC
	gDistanceBetween_TriangleVertices[0] = (float)sqrt(
		(gTriangleVertices[2][0] - gTriangleVertices[1][0]) * (gTriangleVertices[2][0] - gTriangleVertices[1][0]) +
		(gTriangleVertices[2][1] - gTriangleVertices[1][1]) * (gTriangleVertices[2][1] - gTriangleVertices[1][1]));

	// Distance Of AC
	gDistanceBetween_TriangleVertices[1] = (float)sqrt(
		(gTriangleVertices[2][0] - gTriangleVertices[0][0]) * (gTriangleVertices[2][0] - gTriangleVertices[0][0]) +
		(gTriangleVertices[2][1] - gTriangleVertices[0][1]) * (gTriangleVertices[2][1] - gTriangleVertices[0][1]));

	// Distance Of AB
	gDistanceBetween_TriangleVertices[2] = (float)sqrt(
		(gTriangleVertices[1][0] - gTriangleVertices[0][0]) * (gTriangleVertices[1][0] - gTriangleVertices[0][0]) +
		(gTriangleVertices[1][1] - gTriangleVertices[0][1]) * (gTriangleVertices[1][1] - gTriangleVertices[0][1]));

	gSemiPerimeter =
		(float)(gDistanceBetween_TriangleVertices[0] + gDistanceBetween_TriangleVertices[1] + gDistanceBetween_TriangleVertices[2]) /
		2;

	gRadius =
		(float)sqrt(gSemiPerimeter * (gSemiPerimeter - gDistanceBetween_TriangleVertices[0]) *
		(gSemiPerimeter - gDistanceBetween_TriangleVertices[1]) *
			(gSemiPerimeter - gDistanceBetween_TriangleVertices[2])) /
		gSemiPerimeter;

	gInCenter_Coordinates[0] =
		(float)((gDistanceBetween_TriangleVertices[0] * gTriangleVertices[0][0]) +
		(gDistanceBetween_TriangleVertices[1] * gTriangleVertices[1][0]) +
			(gDistanceBetween_TriangleVertices[2] * gTriangleVertices[2][0])) /
			(gDistanceBetween_TriangleVertices[0] + gDistanceBetween_TriangleVertices[1] + gDistanceBetween_TriangleVertices[2]);

	gInCenter_Coordinates[1] =
		(float)((gDistanceBetween_TriangleVertices[0] * gTriangleVertices[0][1]) +
		(gDistanceBetween_TriangleVertices[1] * gTriangleVertices[1][1]) +
			(gDistanceBetween_TriangleVertices[2] * gTriangleVertices[2][1])) /
			(gDistanceBetween_TriangleVertices[0] + gDistanceBetween_TriangleVertices[1] + gDistanceBetween_TriangleVertices[2]);

	gMidPoint_BC[0] = (float)( gTriangleVertices[1][0] + gTriangleVertices[2][0]) / 2.0f;
	gMidPoint_BC[1] = (float)( gTriangleVertices[1][1] + gTriangleVertices[2][1]) / 2.0f;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(PEP_WIDTH, PEP_HEIGHT);

	return;
}

void ReSize(int pep_width, int pep_height)
{
	if (0 == pep_height)
	{
		pep_height = 1;
	}

	glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);

	return;
}

void Display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glLineWidth(2.0f);

	glMatrixMode(GL_MODELVIEW);

	//
	// Triangle
	//
	glLoadIdentity();
	glTranslatef(gTriangleTranslation[0], gTriangleTranslation[1], -5.0f);
	glRotatef(gAngle, 0.0f, 1.0f, 0.0f);

	glBegin(GL_LINES);
	for (int i = 0; i < 3; i++)
	{
		glVertex3f(gTriangleVertices[i][0], gTriangleVertices[i][1], gTriangleVertices[i][2]);
		glVertex3f(gTriangleVertices[(i + 1) % 3][0], gTriangleVertices[(i + 1) % 3][1], gTriangleVertices[(i + 1) % 3][2]);
	}
	glEnd();

	float angle = 0.0f;

	//
	// Circle
	//
	glLoadIdentity();
	glTranslatef(gCircleTranslation[0], gCircleTranslation[1], -5.0f);
	glRotatef(gAngle, 0.0f, 1.0f, 0.0f);

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < gPoints; i++) {
		angle = (float)(2.0f * M_PI * i) / gPoints;
		glVertex3f((GLfloat)(gInCenter_Coordinates[0] + cos(angle) * gRadius),
			(GLfloat)(gInCenter_Coordinates[1] + sin(angle) * gRadius),
			0.0f);
	}
	glEnd();

	glLoadIdentity();
	glTranslatef(0.0f, gLineTranslation, -5.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(gTriangleVertices[0][0], gTriangleVertices[0][1], 0.0f);
	glVertex3f(gMidPoint_BC[0], gMidPoint_BC[1], 0.0f);
	glEnd();

	SwapBuffers(pep_gHdc);
}

void UnInitialize(void)
{
	if (true == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (pep_gHglrc == wglGetCurrentContext())
	{
		wglMakeCurrent(0, 0);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
		pep_gHglrc = NULL;
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
		pep_gHdc = NULL;
	}

	if (NULL != pep_gpFile)
	{
		fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
		fclose(pep_gpFile);
		pep_gpFile = NULL;
	}

	return;
}

void Update(void)
{
	if (gAngle > 360.0f)
	{
		gAngle = 0.0f;
	}
	gAngle += 1.0f;

	if (gLineTranslation > 0.0f)
	{
		gLineTranslation -= 0.005f;
	}
	else
	{
		gLineTranslation = 0.0f;
	}

	if (gTriangleTranslation[0] < 0.0f)
	{
		gTriangleTranslation[0] += 0.005f;
		gTriangleTranslation[1] += 0.005f;
	}
	else
	{
		gTriangleTranslation[0] = 0.0f;
		gTriangleTranslation[1] = 0.0f;
	}

	if (gCircleTranslation[0] > 0.0f)
	{
		gCircleTranslation[0] -= 0.005f;
		gCircleTranslation[1] += 0.005f;
	}
	else
	{
		gTriangleTranslation[0] = 0.0f;
		gTriangleTranslation[1] = 0.0f;
	}

}
