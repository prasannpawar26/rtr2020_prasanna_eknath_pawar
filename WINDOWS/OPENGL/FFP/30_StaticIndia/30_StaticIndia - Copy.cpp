#define UNICODE

#include <Windows.h>
#include <stdio.h>

#include "30_StaticIndia.h"

#include <gl/GL.h>
#include <gl/glu.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#define PI 3.1415

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

float pep_gFlagColor[3][4] =
{	{1.0f, 0.60f, 0.2f, 0.0f},
	{1.0f, 1.0f, 1.0f, 0.0f},
	{0.070f, 0.533f, 0.027f, 0.0f}
}; 

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
	// Function Declarations
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);
	void Update(void);

	// Variable Declarations
	bool pep_bDone = false;
	WNDCLASSEX pep_wndClass;
	TCHAR pep_szAppName[] = TEXT("30_StaticIndia");
	MSG pep_msg;
	HWND pep_hwnd;

	// Code
	if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("30_StaticIndia"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.hInstance = pep_hInstance;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szAppName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szAppName,
		TEXT("30_StaticIndia"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
		PEP_WIDTH,
		PEP_HEIGHT,
		NULL,
		NULL,
		pep_hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	Initialize();

	ShowWindow(pep_gHwnd, pep_iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
	// Function Declarations
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void ReSize(int, int);

	switch (pep_iMsg)
	{
		case WM_DESTROY:
		{
			UnInitialize();
			PostQuitMessage(0);
		}
		break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
		break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
		break;

		case WM_SIZE:
		{
			ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
		}
		break;

		case WM_ERASEBKGND:
		{
			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(pep_hwnd);
		}
		break;

		case WM_KEYDOWN:
		{
			switch (pep_wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(pep_hwnd);
				}
				break;

				case 'F':
				case 'f':
				{
					ToggledFullScreen();
				}
				break;
			}
		}
		break;
	}

	return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
	// Variable Declarations
	MONITORINFO pep_mi = { sizeof(MONITORINFO) };

	if (false == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
		{
			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

		pep_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		pep_gbFullScreen = false;
	}
}

void Initialize(void)
{
	// Function Declarations
	void ReSize(int, int);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pep_pfd;
	int pep_iPixelFormatIndex;

	// Code
	pep_gHdc = GetDC(pep_gHwnd);

	ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_pfd.nVersion = 1;
	pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pep_pfd.iPixelType = PFD_TYPE_RGBA;
	pep_pfd.cColorBits = 32;
	pep_pfd.cRedBits = 8;
	pep_pfd.cGreenBits = 8;
	pep_pfd.cBlueBits = 8;
	pep_pfd.cAlphaBits = 8;

	pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
	if (0 == pep_iPixelFormatIndex)
	{
		fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
	{
		fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "wglCreateContext Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(PEP_WIDTH, PEP_HEIGHT);

	return;
}

void ReSize(int pep_width, int pep_height)
{
	if (0 == pep_height)
	{
		pep_height = 1;
	}

	glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);

	return;
}

void Display(void)
{
	//
	//Function Declarations
	void Draw_India(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	// Code
	Draw_India();

	SwapBuffers(pep_gHdc);
}

void UnInitialize(void)
{
	if (true == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (pep_gHglrc == wglGetCurrentContext())
	{
		wglMakeCurrent(0, 0);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
		pep_gHglrc = NULL;
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
		pep_gHdc = NULL;
	}

	if (NULL != pep_gpFile)
	{
		fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
		fclose(pep_gpFile);
		pep_gpFile = NULL;
	}

	return;
}

void Update(void)
{
	return;
}

void Draw_India(void)
{
	//Function Declarations
	void Draw_I(void);
	void Draw_N(void);
	void Draw_D(void);
	void Draw_I2(void);
	void Draw_A(void);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -13.0f);
	Draw_I();

	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -13.0f);
	Draw_N();

	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -13.0f);
	Draw_D();

	glLoadIdentity();
	glTranslatef(-1.7f, 0.0f, -13.0f);
	Draw_I2();

	glLoadIdentity();
	glTranslatef(-1.8f, 0.0f, -13.0f);
	Draw_A();

	return;
}

void Draw_I(void)
{
	glBegin(GL_POLYGON);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2], pep_gFlagColor[0][3]);
	glVertex3f(-4.5f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2], pep_gFlagColor[0][3]);
	glVertex3f(-2.5f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2], pep_gFlagColor[0][3]);
	glVertex3f(-2.5f, 1.0f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2], pep_gFlagColor[0][3]);
	glVertex3f(-4.5f, 1.0f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2], pep_gFlagColor[0][3]);
	glVertex3f(-3.80f, 1.0f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2], pep_gFlagColor[0][3]);
	glVertex3f(-3.2f, 1.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2], pep_gFlagColor[1][3]);
	glVertex3f(-3.2f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2], pep_gFlagColor[1][3]);
	glVertex3f(-3.8f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2], pep_gFlagColor[1][3]);
	glVertex3f(-3.80f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2], pep_gFlagColor[1][3]);
	glVertex3f(-3.2f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2], pep_gFlagColor[2][3]);
	glVertex3f(-3.2f, -1.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2], pep_gFlagColor[2][3]);
	glVertex3f(-3.8f, -1.0f, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2], pep_gFlagColor[2][3]);
	glVertex3f(-4.5f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2], pep_gFlagColor[2][3]);
	glVertex3f(-2.5f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2], pep_gFlagColor[2][3]);
	glVertex3f(-2.5f, -1.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2], pep_gFlagColor[2][3]);
	glVertex3f(-4.5f, -1.0f, 0.0f);
	glEnd();

	return;
}

void Draw_I2(void)
{
	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(2.70f, 1.5f, 0.0f);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(4.70f, 1.5f, 0.0f);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(4.70f, 1.0f, 0.0f);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(2.70f, 1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(3.40f, 1.0f, 0.0f);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(4.0f, 1.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(4.0f, 0.0f, 0.0f);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[2][3]);
	glVertex3f(3.40f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(3.40f, 0.0f, 0.0f);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(4.0f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(4.0f, -1.0f, 0.0f);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(3.40f, -1.0f, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(2.70f, -1.5f, 0.0f);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(4.70f, -1.5f, 0.0f);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(4.70f, -1.0f, 0.0f);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(2.70f, -1.0f, 0.0f);
	glEnd();

	return;
}

void Draw_N(void) {

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-2.1f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-1.5f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-1.5f, 0.0f, 0.0f);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-2.1f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-2.1f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-1.5f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(-1.5f, -1.5f, 0.0f);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(-2.1f, -1.5f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-1.5f, 1.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-1.5f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-1.1f, -0.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-1.1f, 0.50f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-1.1f, 0.50f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-1.1f, -0.50f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(-0.7f, -1.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-0.7f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-0.7f, 1.5f, 0.0f);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(-0.1f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-0.7f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-0.7f, 0.0f, 0.0f);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(-0.1f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(-0.1f, -1.5f, 0.0f);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(-0.7f, -1.5f, 0.0f);
	glEnd();

	return;
}

void Draw_D(void) {

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(0.30f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(0.90f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(0.90f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(0.30f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(0.30f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(0.90f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(0.90f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(0.30f, -1.5f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(1.70f, 0.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(2.30f, 0.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(2.30f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(1.70f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(1.70f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(2.30f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(2.30f, -0.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(1.70f, -0.5f, 0.0f);
	glEnd();


	// Orange Color
	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(0.90f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(1.50f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(1.50f, 0.9f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(0.90f, 0.9f, 0.0f);

	glEnd();


	// Green Color
	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(0.90f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(1.50f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(1.50f, -0.9f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(0.90f, -0.9f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(1.50f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(1.50f, 0.9f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(1.70f, 0.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(2.30f, 0.5f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(1.50f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(1.50f, -0.9f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(1.70f, -0.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(2.30f, -0.5f, 0.0f);

	glEnd();

	return;
}

void Draw_A(void) {

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glBegin(GL_POLYGON);
	glVertex3f(5.70f, 0.0f, 0.0f);
	glVertex3f(7.10f, 0.0f, 0.0f);
	glVertex3f(7.10f, -0.20f, 0.0f);
	glVertex3f(5.70f, -0.20f, 0.0f);
	glEnd();

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glBegin(GL_POLYGON);
	glVertex3f(5.70f, -0.20f, 0.0f);
	glVertex3f(7.10f, -0.20f, 0.0f);
	glVertex3f(7.10f, -0.40f, 0.0f);
	glVertex3f(5.70f, -0.40f, 0.0f);
	glEnd();

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glBegin(GL_POLYGON);
	glVertex3f(5.70f, -0.40f, 0.0f);
	glVertex3f(7.10f, -0.40f, 0.0f);
	glVertex3f(7.10f, -0.60f, 0.0f);
	glVertex3f(5.70f, -0.60f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(6.10f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(6.70f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(6.20f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(5.60f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(5.60f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(6.20f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(5.70f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(5.10f, -1.5f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(6.10f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[0][0], pep_gFlagColor[0][1], pep_gFlagColor[0][2],
		pep_gFlagColor[0][3]);
	glVertex3f(6.70f, 1.5f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(7.20f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(6.60f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(6.60f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[1][0], pep_gFlagColor[1][1], pep_gFlagColor[1][2],
		pep_gFlagColor[1][3]);
	glVertex3f(7.20f, 0.0f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(7.70f, -1.5f, 0.0f);

	glColor4f(pep_gFlagColor[2][0], pep_gFlagColor[2][1], pep_gFlagColor[2][2],
		pep_gFlagColor[2][3]);
	glVertex3f(7.10f, -1.5f, 0.0f);
	glEnd();
	return;
}
