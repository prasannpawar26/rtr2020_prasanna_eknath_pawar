#define UNICODE

#include <Windows.h>
#include <stdio.h>

#include "29_GraphPeparAndAllGeometries.h"

#include <gl/GL.h>
#include <gl/glu.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#define PI 3.1415

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

float pep_gOuterCircleOrigin[2];
float pep_gOuterCircleRadius;
float pep_gOuterCircleAngle;
int pep_gOuterCirclePoints;

float pep_gInnerRectangleCoordinate_A[2];
float pep_gInnerRectangleCoordinate_B[2];
float pep_gInnerRectangleCoordinate_C[2];
float pep_gInnerRectangleCoordinate_D[2];

float pep_gInnerTriangleCoordinate_A[2] = {0.0f, 0.0f};
float pep_gInnerTriangleCoordinate_B[2] = {0.0f, 0.0f};
float pep_gInnerTriangleCoordinate_C[2] = {0.0f, 0.0f};

float pep_gDistanceBetweenInnerTriangleSide_BC = 0.0f;
float pep_gDistanceBetweenInnerTriangleSide_AC = 0.0f;
float pep_gDistanceBetweenInnerTriangleSide_AB = 0.0f;

float pep_gInnerTriangleSemiPerimeter = 0.0f;
float pep_gInnerCircleRadius = 0.0f;
float pep_gInnerCirclOriginCoordinates[2] = {0.0f, 0.0f};
const int pep_gInnerCirclePoints = 1000;
float pep_gInnerCircleAngle = 0.0f;

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
	// Function Declarations
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);
	void Update(void);

	// Variable Declarations
	bool pep_bDone = false;
	WNDCLASSEX pep_wndClass;
	TCHAR pep_szAppName[] = TEXT("29_GraphPeparAndAllGeometries");
	MSG pep_msg;
	HWND pep_hwnd;

	// Code
	if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("29_GraphPeparAndAllGeometries"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.hInstance = pep_hInstance;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szAppName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szAppName,
		TEXT("29_GraphPeparAndAllGeometries"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
		PEP_WIDTH,
		PEP_HEIGHT,
		NULL,
		NULL,
		pep_hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	Initialize();

	ShowWindow(pep_gHwnd, pep_iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
	// Function Declarations
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void ReSize(int, int);

	switch (pep_iMsg)
	{
		case WM_DESTROY:
		{
			UnInitialize();
			PostQuitMessage(0);
		}
		break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
		break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
		break;

		case WM_SIZE:
		{
			ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
		}
		break;

		case WM_ERASEBKGND:
		{
			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(pep_hwnd);
		}
		break;

		case WM_KEYDOWN:
		{
			switch (pep_wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(pep_hwnd);
				}
				break;

				case 'F':
				case 'f':
				{
					ToggledFullScreen();
				}
				break;
			}
		}
		break;
	}

	return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
	// Variable Declarations
	MONITORINFO pep_mi = { sizeof(MONITORINFO) };

	if (false == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
		{
			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

		pep_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		pep_gbFullScreen = false;
	}
}

void Initialize(void)
{
	// Function Declarations
	void ReSize(int, int);
	void Initialize_OuterCircle(void);
	void Initialize_InnerRectangle(void);
	void Initialize_InnerTriangle(void);
	void Initialize_InnerCircle(void);
	
	// Variable Declarations
	PIXELFORMATDESCRIPTOR pep_pfd;
	int pep_iPixelFormatIndex;

	// Code
	pep_gHdc = GetDC(pep_gHwnd);

	ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_pfd.nVersion = 1;
	pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pep_pfd.iPixelType = PFD_TYPE_RGBA;
	pep_pfd.cColorBits = 32;
	pep_pfd.cRedBits = 8;
	pep_pfd.cGreenBits = 8;
	pep_pfd.cBlueBits = 8;
	pep_pfd.cAlphaBits = 8;

	pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
	if (0 == pep_iPixelFormatIndex)
	{
		fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
	{
		fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "wglCreateContext Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	//
	//
	//
	Initialize_OuterCircle();
	Initialize_InnerRectangle();
	Initialize_InnerTriangle();
	Initialize_InnerCircle();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(PEP_WIDTH, PEP_HEIGHT);

	return;
}

void ReSize(int pep_width, int pep_height)
{
	if (0 == pep_height)
	{
		pep_height = 1;
	}

	glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);

	return;
}

void Display(void)
{
	//
	//Function Declarations
	void Display_GraphPaper(void);
	void Display_OuterCircle(void);
	void Display_InnerRectangle(void);
	void Display_InnerTriangle(void);
	void Display_InnerCircle(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);

	Display_GraphPaper();

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);

	// Circle
	Display_OuterCircle();
	// Rectangle In-Side Circle
	Display_InnerRectangle();
	// Triangle In-Side Rectangle
	Display_InnerTriangle();
	// Inner Circle
	Display_InnerCircle();

	SwapBuffers(pep_gHdc);
}

void UnInitialize(void)
{
	if (true == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (pep_gHglrc == wglGetCurrentContext())
	{
		wglMakeCurrent(0, 0);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
		pep_gHglrc = NULL;
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
		pep_gHdc = NULL;
	}

	if (NULL != pep_gpFile)
	{
		fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
		fclose(pep_gpFile);
		pep_gpFile = NULL;
	}

	return;
}

void Update(void)
{
	return;
}

void Display_GraphPaper(void)
{
	// Code

	// Horizontal Lines
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (float y_axis = -1.0f; y_axis <= 1.0f; y_axis = y_axis + 0.02f) {
		glVertex3f(-1.0f, y_axis, 0);
		glVertex3f(1.0f, y_axis, 0);
	}
	glEnd();

	// Vertical Lines
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (float x_axis = -1.0f; x_axis <= 1.0f; x_axis = x_axis + 0.02f) {
		glVertex3f(x_axis, -1.0f, 0.0f);
		glVertex3f(x_axis, 1.0f, 0.0f);
	}
	glEnd();

	// Horizontal Line in Center
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0, 0);
	glVertex3f(1.0f, 0, 0);
	glEnd();

	// Vertical Line in Center
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0, -1.0f, 0);
	glVertex3f(0, 1.0f, 0);
	glEnd();

	return;
}

void Display_OuterCircle(void)
{
	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 0.0f);

	for (int i = 0; i < pep_gOuterCirclePoints; i++) {
		pep_gOuterCircleAngle = (float)(2.0f * M_PI * i) / pep_gOuterCirclePoints;
		glVertex3f((GLfloat)(pep_gOuterCircleOrigin[0] +
			cos(pep_gOuterCircleAngle) * pep_gOuterCircleRadius),
			(GLfloat)(pep_gOuterCircleOrigin[1] +
				sin(pep_gOuterCircleAngle) * pep_gOuterCircleRadius),
			0.0f);
	}

	glEnd();

	return;
}

void Display_InnerRectangle(void)
{

	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(pep_gInnerRectangleCoordinate_A[0], pep_gInnerRectangleCoordinate_A[1],
		0.0f);
	glVertex3f(pep_gInnerRectangleCoordinate_B[0], pep_gInnerRectangleCoordinate_B[1],
		0.0f);
	glVertex3f(pep_gInnerRectangleCoordinate_C[0], pep_gInnerRectangleCoordinate_C[1],
		0.0f);
	glVertex3f(pep_gInnerRectangleCoordinate_D[0], pep_gInnerRectangleCoordinate_D[1],
		0.0f);
	glEnd();

	return;
}

void Display_InnerTriangle(void) {

	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);

	// Co-Ordinate A
	glVertex3f(pep_gInnerTriangleCoordinate_A[0], pep_gInnerTriangleCoordinate_A[1],
		0.0f);
	// Co-Ordinate B
	glVertex3f(pep_gInnerTriangleCoordinate_B[0], pep_gInnerTriangleCoordinate_B[1],
		0.0f);
	// Co-Ordinate C
	glVertex3f(pep_gInnerTriangleCoordinate_C[0], pep_gInnerTriangleCoordinate_C[1],
		0.0f);

	glEnd();

	return;
}

void Display_InnerCircle(void) {

	// Code
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < pep_gInnerCirclePoints; i++) {
		pep_gInnerCircleAngle = (float)(2.0f * M_PI * i) / pep_gInnerCirclePoints;
		glVertex3f((GLfloat)(pep_gInnerCirclOriginCoordinates[0] +
			cos(pep_gInnerCircleAngle) * pep_gInnerCircleRadius),
			(GLfloat)(pep_gInnerCirclOriginCoordinates[1] +
				sin(pep_gInnerCircleAngle) * pep_gInnerCircleRadius),
			0.0f);
	}
	glEnd();

	return;
}

void Initialize_OuterCircle(void) {

	// Origin Of Outer Circle = (0.0f, 0.0f)
	pep_gOuterCircleOrigin[0] = 0.0f;
	pep_gOuterCircleOrigin[1] = 0.0f;

	// Radius Of Outer Circle = 1.0f
	pep_gOuterCircleRadius = 0.90f;

	// Starting Angle Of Outer Circle
	pep_gOuterCircleAngle = 0.0f;

	// Number Of Points To Draw To Draw Outer Circle
	pep_gOuterCirclePoints = 1000;

	return;
}

void Initialize_InnerRectangle(void) {

	// Calculating Inner Rectangle Starting Co-Ordinates. Taking HarCoded Angle
	// Value 15.0f.
	// Note: All The Co-Ordinates Of Inner Rectangle Should Touches The Outer
	// Circle Hence For Calulating Starting Co-ordinate Of Inner(In-Side Outer
	// Circle) Rectangle Using Radius Of Outer Circle

	pep_gInnerRectangleCoordinate_A[0] =
		(float)(pep_gOuterCircleOrigin[0] + cos(15.0f) * pep_gOuterCircleRadius);
	pep_gInnerRectangleCoordinate_A[1] =
		(float)(pep_gOuterCircleOrigin[1] + sin(15.0f) * pep_gOuterCircleRadius);

	// Picking 2nd Co-Ordinate Of Rectangle In (-X, Y) / 2nd Quadrant.
	// 2nd Co-Ordinate will be 2X = width distance from 1st Co-Ordinate. Hence
	// Assigning negative Of X of 1st To 2nd Co-Ordinate
	pep_gInnerRectangleCoordinate_B[0] = -pep_gInnerRectangleCoordinate_A[0];
	pep_gInnerRectangleCoordinate_B[1] = pep_gInnerRectangleCoordinate_A[1];

	// Picking 2nd Co-Ordinate Of Rectangle In (-X, -Y) / 3rd Quadrant.
	// 3rd Co-Ordinate will be 2Y = height distance from 2nd Co-Ordinate.
	pep_gInnerRectangleCoordinate_C[0] = -pep_gInnerRectangleCoordinate_A[0];
	pep_gInnerRectangleCoordinate_C[1] = -pep_gInnerRectangleCoordinate_A[1];

	// Picking 2nd Co-Ordinate Of Rectangle In (X, -Y) / 4th Quadrant.
	// 3rd Co-Ordinate will be 2X = width distance from 4th Co-Ordinate. (I.E 4th
	// Co-Ordinate Will be 2Y Distance From 1st Co-Ordinate). Hence Assigning
	// negative Of -Y of 1st To 4th Co-Ordinate
	pep_gInnerRectangleCoordinate_D[0] = pep_gInnerRectangleCoordinate_A[0];
	pep_gInnerRectangleCoordinate_D[1] = -pep_gInnerRectangleCoordinate_A[1];

	return;
}

void Initialize_InnerTriangle(void) {

	// Calculating Top "A" Co-Ordinate Of Triangle
	// Rectangles Co-Ordinate A And B Lies In 1st And 2nd Quadrant Respsectively
	pep_gInnerTriangleCoordinate_A[0] = (float)((pep_gInnerRectangleCoordinate_B[0]) +
		pep_gInnerRectangleCoordinate_A[0]) /
		2.0f;
	pep_gInnerTriangleCoordinate_A[1] =
		(float)(pep_gInnerRectangleCoordinate_B[1] + pep_gInnerRectangleCoordinate_A[1]) /
		2.0f;

	// Base Co-Ordinates Of Inner Triangle  Will Be Similar To Rectangles 3rd And
	// 4th Co-Ordinates Hence Directly Initializing With Them
	pep_gInnerTriangleCoordinate_B[0] = pep_gInnerRectangleCoordinate_C[0];
	pep_gInnerTriangleCoordinate_B[1] = pep_gInnerRectangleCoordinate_C[1];

	pep_gInnerTriangleCoordinate_C[0] = pep_gInnerRectangleCoordinate_D[0];
	pep_gInnerTriangleCoordinate_C[1] = pep_gInnerRectangleCoordinate_D[1];

	return;
}

void Initialize_InnerCircle(void) {

	// 1. Calculate Sides Of Triangle
	// 2. Calculate semi- perimeter
	// 3. Calculate radius
	// 4. Calculate Origin Coordinates Of Inner Circle

	// Distance BetWeen Triangles B And C Co-Ordinates = Called As Distance 'A'
	pep_gDistanceBetweenInnerTriangleSide_BC = (float)sqrt(
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_B[0]) *
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_B[0]) +
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_B[1]) *
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_B[1]));

	// Distance BetWeen Triangles A And C Co-Ordinates = Called As Distance 'B'
	pep_gDistanceBetweenInnerTriangleSide_AC = (float)sqrt(
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_A[0]) *
		(pep_gInnerTriangleCoordinate_C[0] - pep_gInnerTriangleCoordinate_A[0]) +
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_A[1]) *
		(pep_gInnerTriangleCoordinate_C[1] - pep_gInnerTriangleCoordinate_A[1]));

	// Distance BetWeen Triangles A And B Co-Ordinates = Called As Distance 'C'
	pep_gDistanceBetweenInnerTriangleSide_AB = (float)sqrt(
		(pep_gInnerTriangleCoordinate_B[0] - pep_gInnerTriangleCoordinate_A[0]) *
		(pep_gInnerTriangleCoordinate_B[0] - pep_gInnerTriangleCoordinate_A[0]) +
		(pep_gInnerTriangleCoordinate_B[1] - pep_gInnerTriangleCoordinate_A[1]) *
		(pep_gInnerTriangleCoordinate_B[1] - pep_gInnerTriangleCoordinate_A[1]));

	pep_gInnerTriangleSemiPerimeter = (float)(pep_gDistanceBetweenInnerTriangleSide_BC +
		pep_gDistanceBetweenInnerTriangleSide_AC +
		pep_gDistanceBetweenInnerTriangleSide_AB) /
		2;

	pep_gInnerCircleRadius =
		(float)sqrt(
			pep_gInnerTriangleSemiPerimeter *
			(pep_gInnerTriangleSemiPerimeter - pep_gDistanceBetweenInnerTriangleSide_BC) *
			(pep_gInnerTriangleSemiPerimeter - pep_gDistanceBetweenInnerTriangleSide_AC) *
			(pep_gInnerTriangleSemiPerimeter -
				pep_gDistanceBetweenInnerTriangleSide_AB)) /
		pep_gInnerTriangleSemiPerimeter;

	pep_gInnerCirclOriginCoordinates[0] =
		(float)((pep_gDistanceBetweenInnerTriangleSide_BC *
			pep_gInnerTriangleCoordinate_A[0]) +
			(pep_gDistanceBetweenInnerTriangleSide_AC *
				pep_gInnerTriangleCoordinate_B[0]) +
				(pep_gDistanceBetweenInnerTriangleSide_AB *
					pep_gInnerTriangleCoordinate_C[0])) /
					(pep_gDistanceBetweenInnerTriangleSide_BC +
						pep_gDistanceBetweenInnerTriangleSide_AC +
						pep_gDistanceBetweenInnerTriangleSide_AB);

	pep_gInnerCirclOriginCoordinates[1] =
		(float)((pep_gDistanceBetweenInnerTriangleSide_BC *
			pep_gInnerTriangleCoordinate_A[1]) +
			(pep_gDistanceBetweenInnerTriangleSide_AC *
				pep_gInnerTriangleCoordinate_B[1]) +
				(pep_gDistanceBetweenInnerTriangleSide_AB *
					pep_gInnerTriangleCoordinate_C[1])) /
					(pep_gDistanceBetweenInnerTriangleSide_BC +
						pep_gDistanceBetweenInnerTriangleSide_AC +
						pep_gDistanceBetweenInnerTriangleSide_AB);

	return;
}
