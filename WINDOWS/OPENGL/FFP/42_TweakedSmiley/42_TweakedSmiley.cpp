#define _UNICODE

#include <Windows.h>
#include <stdio.h>

#include "42_TweakedSmiley.h"

#include <gl/GL.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

GLuint pep_texture_smiley;
int pep_gPressedKey = 0;

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
	// Function Declarations
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);
	void Update(void);

	// Variable Declarations
	bool pep_bDone = false;
	WNDCLASSEX pep_wndClass;
	TCHAR pep_szAppName[] = TEXT("42_TweakedSmiley");
	MSG pep_msg;
	HWND pep_hwnd;

	// Code
	if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("42_TweakedSmiley"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.hInstance = pep_hInstance;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szAppName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szAppName,
		TEXT("42_TweakedSmiley"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
		PEP_WIDTH,
		PEP_HEIGHT,
		NULL,
		NULL,
		pep_hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	Initialize();

	ShowWindow(pep_gHwnd, pep_iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			Update();

			Display();
		}
	}

	return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
	// Function Declarations
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void ReSize(int, int);

	switch (pep_iMsg)
	{
		case WM_DESTROY:
		{
			UnInitialize();
			PostQuitMessage(0);
		}
		break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
		break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
		break;

		case WM_SIZE:
		{
			ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
		}
		break;

		case WM_ERASEBKGND:
		{
			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(pep_hwnd);
		}
		break;

		case WM_KEYDOWN:
		{
			switch (pep_wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(pep_hwnd);
				}
				break;

				case 'F':
				case 'f':
				{
					ToggledFullScreen();
				}
					break;

				case 49:
				case VK_NUMPAD1:
					pep_gPressedKey = 1;
					glEnable(GL_TEXTURE_2D); // enable the texture memory
					break;

				case 50:
				case VK_NUMPAD2:
					pep_gPressedKey = 2;
					glEnable(GL_TEXTURE_2D); // enable the texture memory
					break;

				case 51:
				case VK_NUMPAD3:
					pep_gPressedKey = 3;
					glEnable(GL_TEXTURE_2D); // enable the texture memory
					break;

				case 52:
				case VK_NUMPAD4:
					pep_gPressedKey = 4;
					glEnable(GL_TEXTURE_2D); // enable the texture memory
					break;
				
				case 53:
				case VK_NUMPAD5:
					pep_gPressedKey = 0;
					glDisable(GL_TEXTURE_2D); // enable the texture memory
			}
		}
		break;
	}

	return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
	// Variable Declarations
	MONITORINFO pep_mi = { sizeof(MONITORINFO) };

	if (false == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
		{
			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

		pep_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		pep_gbFullScreen = false;
	}
}

void Initialize(void)
{
	// Function Declarations
	void ReSize(int, int);
	bool LoadGlTexture(GLuint *, TCHAR[]);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pep_pfd;
	int pep_iPixelFormatIndex;

	// Code
	pep_gHdc = GetDC(pep_gHwnd);

	ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_pfd.nVersion = 1;
	pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pep_pfd.iPixelType = PFD_TYPE_RGBA;
	pep_pfd.cColorBits = 32;
	pep_pfd.cRedBits = 8;
	pep_pfd.cGreenBits = 8;
	pep_pfd.cBlueBits = 8;
	pep_pfd.cAlphaBits = 8;
	pep_pfd.cDepthBits = 32;

	pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
	if (0 == pep_iPixelFormatIndex)
	{
		fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
	{
		fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "wglCreateContext Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	LoadGlTexture(&pep_texture_smiley, MAKEINTRESOURCEA(IDBITMAP_SMILEY));

	//glEnable(GL_TEXTURE_2D); // enable the texture memory

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(PEP_WIDTH, PEP_HEIGHT);

	return;
}

bool LoadGlTexture(GLuint *pep_texture, TCHAR pep_resourceID[])
{
	//
	// Variable Declarations
	//
	bool pep_bResult = false;
	HBITMAP pep_hBitmap = NULL; // OS image loading related code
	BITMAP pep_bmp; // OS image loading related code

	//
	// Code
	//
	pep_hBitmap = (HBITMAP)LoadImage( // OS image loading related code
		GetModuleHandle(NULL), // NULL -> Return's hInstance of the current process/application.
		pep_resourceID,
		IMAGE_BITMAP, // which type of image to be load
		0, // width of image which is going to be load. 0(Zero) -> Means This Is Not ICON or CURSOR it is BITMAP
		0, // height of image which is going to be load. 0(Zero) -> Means This Is Not ICON or CURSOR it is BITMAP
		LR_CREATEDIBSECTION // In which image format to be loaded. LR_CREATEDIBSECTION -> load in windows native image format. LR -> load resource. DIB -> Device Independent Bitmap
	);
	// why not file , why resource
	// resource get embbed in exe.

	if (NULL == pep_hBitmap)
	{
		return pep_bResult;
	}

	pep_bResult = true;

	GetObject( // OS image loading related code
		pep_hBitmap,
		sizeof(pep_bmp),
		&pep_bmp
	);

	// alignment of image data
	glPixelStorei(
		GL_UNPACK_ALIGNMENT, // unpacked
		4 // rgba - per pixel -> alignment is depend of how much data we are using for 1 pixel
		);

	// texture buffer gets created at GPU side
	// and it is get mapped with cpu variable (called as target point) i.e. texture
	// generate texture can be multiple in one call
	glGenTextures(
		1, // no. of buffers
		pep_texture // name of target point which will be point to gpu texture memory
	);

	// step 4: binding texture
	//bind is one at a time
	//
	glBindTexture(
		GL_TEXTURE_2D,
		*pep_texture
	);

	//step 5: setting of texture parameters
	//maginafication -> near to audience
	//minification -> far from audience
	//mipmap -> image size decrease in  power of 2
	glTexParameteri(
		GL_TEXTURE_2D, // target 
		GL_TEXTURE_MAG_FILTER, // (which parameter to set) -> maginfication (near to far from audience)
		GL_LINEAR // quality (weighted avrage)
	);

	glTexParameteri(
		GL_TEXTURE_2D,
		GL_TEXTURE_MIN_FILTER, // minification
		GL_LINEAR_MIPMAP_LINEAR // decrease image size + quality by weighted avarage
	);

	// information of image required
	// image format
	// image width
	// image height
	// image data

	// push image data into memory with help of graphics driver
	gluBuild2DMipmaps(
		GL_TEXTURE_2D,
		3, // no. of colors (rgb) -> internal format
		pep_bmp.bmWidth,
		pep_bmp.bmHeight,
		GL_BGR_EXT, // windows os native bitmap format given to opengl
		GL_UNSIGNED_BYTE,
		pep_bmp.bmBits // image data
	);

	// gluBuild2DMipmaps = glTexImage2D + glGenerateMipmap

	DeleteObject(pep_hBitmap); // os related code

	pep_hBitmap = NULL;

	return pep_bResult;
}

void ReSize(int pep_width, int pep_height)
{
	if (0 == pep_height)
	{
		pep_height = 1;
	}

	glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);

	return;
}

void Display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);

	glColor3f(1.0f, 1.0f, 1.0f);

	if (0 == pep_gPressedKey)
	{
		glBindTexture(GL_TEXTURE_2D, pep_texture_smiley);

		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else if (1 == pep_gPressedKey)
	{
		glBindTexture(GL_TEXTURE_2D, pep_texture_smiley);

		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else if (2 == pep_gPressedKey)
	{
		glBindTexture(GL_TEXTURE_2D, pep_texture_smiley);

		glBegin(GL_QUADS);

		glTexCoord2f(0.50f, 0.50f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 0.50f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glTexCoord2f(0.50f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else if (3 == pep_gPressedKey)
	{
		glBindTexture(GL_TEXTURE_2D, pep_texture_smiley);

		glBegin(GL_QUADS);

		glTexCoord2f(2.0f, 2.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 2.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glTexCoord2f(2.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else if (4 == pep_gPressedKey)
	{
		glBindTexture(GL_TEXTURE_2D, pep_texture_smiley);

		glBegin(GL_QUADS);

		glTexCoord2f(0.50f, 0.50f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.50f, 0.50f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glTexCoord2f(0.50f, 0.50f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glTexCoord2f(0.50f, 0.50f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	SwapBuffers(pep_gHdc);
}

void Update(void)
{
	return;
}

void UnInitialize(void)
{
	//
	// Code
	//
	glDeleteTextures(1, &pep_texture_smiley);

	if (true == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (pep_gHglrc == wglGetCurrentContext())
	{
		wglMakeCurrent(0, 0);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
		pep_gHglrc = NULL;
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
		pep_gHdc = NULL;
	}

	if (NULL != pep_gpFile)
	{
		fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
		fclose(pep_gpFile);
		pep_gpFile = NULL;
	}

	return;
}
