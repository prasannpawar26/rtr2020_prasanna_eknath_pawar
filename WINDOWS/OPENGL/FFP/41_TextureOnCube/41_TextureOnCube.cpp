#define _UNICODE

#include <Windows.h>
#include <stdio.h>

#include "41_TextureOnCube.h"

#include <gl/GL.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

GLfloat pep_gCubeVertices[6][4][3];
GLfloat pep_gCubeTexCoords[6][4][2];
GLfloat pep_gCubeRotationAngle = 0.0f;

GLuint pep_texture_kundali;

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
	// Function Declarations
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);
	void Update(void);

	// Variable Declarations
	bool pep_bDone = false;
	WNDCLASSEX pep_wndClass;
	TCHAR pep_szAppName[] = TEXT("41_TextureOnCube");
	MSG pep_msg;
	HWND pep_hwnd;

	// Code
	if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("41_TextureOnCube"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.hInstance = pep_hInstance;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szAppName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szAppName,
		TEXT("41_TextureOnCube"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
		PEP_WIDTH,
		PEP_HEIGHT,
		NULL,
		NULL,
		pep_hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	Initialize();

	ShowWindow(pep_gHwnd, pep_iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			Update();

			Display();
		}
	}

	return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
	// Function Declarations
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void ReSize(int, int);

	switch (pep_iMsg)
	{
		case WM_DESTROY:
		{
			UnInitialize();
			PostQuitMessage(0);
		}
		break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
		break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
		break;

		case WM_SIZE:
		{
			ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
		}
		break;

		case WM_ERASEBKGND:
		{
			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(pep_hwnd);
		}
		break;

		case WM_KEYDOWN:
		{
			switch (pep_wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(pep_hwnd);
				}
				break;

				case 'F':
				case 'f':
				{
					ToggledFullScreen();
				}
				break;
			}
		}
		break;
	}

	return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
	// Variable Declarations
	MONITORINFO pep_mi = { sizeof(MONITORINFO) };

	if (false == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
		{
			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

		pep_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		pep_gbFullScreen = false;
	}
}

void Initialize(void)
{
	// Function Declarations
	void ReSize(int, int);
	bool LoadGlTexture(GLuint *, TCHAR[]);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pep_pfd;
	int pep_iPixelFormatIndex;

	// Code
	pep_gHdc = GetDC(pep_gHwnd);

	ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_pfd.nVersion = 1;
	pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pep_pfd.iPixelType = PFD_TYPE_RGBA;
	pep_pfd.cColorBits = 32;
	pep_pfd.cRedBits = 8;
	pep_pfd.cGreenBits = 8;
	pep_pfd.cBlueBits = 8;
	pep_pfd.cAlphaBits = 8;
	pep_pfd.cDepthBits = 32;

	pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
	if (0 == pep_iPixelFormatIndex)
	{
		fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
	{
		fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "wglCreateContext Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	//
	// Cube Vertices
	//
	
	// FrontSide-Vertices
	pep_gCubeVertices[0][0][0] = 1.0f;
	pep_gCubeVertices[0][0][1] = 1.0f;
	pep_gCubeVertices[0][0][2] = 1.0f;

	pep_gCubeVertices[0][1][0] = -1.0f;
	pep_gCubeVertices[0][1][1] = 1.0f;
	pep_gCubeVertices[0][1][2] = 1.0f;

	pep_gCubeVertices[0][2][0] = -1.0f;
	pep_gCubeVertices[0][2][1] = -1.0f;
	pep_gCubeVertices[0][2][2] = 1.0f;

	pep_gCubeVertices[0][3][0] = 1.0f;
	pep_gCubeVertices[0][3][1] = -1.0f;
	pep_gCubeVertices[0][3][2] = 1.0f;

	// RightSide-Vertices
	pep_gCubeVertices[1][0][0] = 1.0f;
	pep_gCubeVertices[1][0][1] = 1.0f;
	pep_gCubeVertices[1][0][2] = -1.0f;

	pep_gCubeVertices[1][1][0] = 1.0f;
	pep_gCubeVertices[1][1][1] = 1.0f;
	pep_gCubeVertices[1][1][2] = 1.0f;

	pep_gCubeVertices[1][2][0] = 1.0f;
	pep_gCubeVertices[1][2][1] = -1.0f;
	pep_gCubeVertices[1][2][2] = 1.0f;

	pep_gCubeVertices[1][3][0] = 1.0f;
	pep_gCubeVertices[1][3][1] = -1.0f;
	pep_gCubeVertices[1][3][2] = -1.0f;

	// BackSide-Vertices
	pep_gCubeVertices[2][0][0] = -1.0f;
	pep_gCubeVertices[2][0][1] = 1.0f;
	pep_gCubeVertices[2][0][2] = -1.0f;

	pep_gCubeVertices[2][1][0] = 1.0f;
	pep_gCubeVertices[2][1][1] = 1.0f;
	pep_gCubeVertices[2][1][2] = -1.0f;

	pep_gCubeVertices[2][2][0] = 1.0f;
	pep_gCubeVertices[2][2][1] = -1.0f;
	pep_gCubeVertices[2][2][2] = -1.0f;

	pep_gCubeVertices[2][3][0] = -1.0f;
	pep_gCubeVertices[2][3][1] = -1.0f;
	pep_gCubeVertices[2][3][2] = -1.0f;
	
	// LeftSide-Vertices
	pep_gCubeVertices[3][0][0] = -1.0f;
	pep_gCubeVertices[3][0][1] = 1.0f;
	pep_gCubeVertices[3][0][2] = 1.0f;

	pep_gCubeVertices[3][1][0] = -1.0f;
	pep_gCubeVertices[3][1][1] = 1.0f;
	pep_gCubeVertices[3][1][2] = -1.0f;

	pep_gCubeVertices[3][2][0] = -1.0f;
	pep_gCubeVertices[3][2][1] = -1.0f;
	pep_gCubeVertices[3][2][2] = -1.0f;

	pep_gCubeVertices[3][3][0] = -1.0f;
	pep_gCubeVertices[3][3][1] = -1.0f;
	pep_gCubeVertices[3][3][2] = 1.0f;

	// TopSide-Vertices
	pep_gCubeVertices[4][0][0] = 1.0f;
	pep_gCubeVertices[4][0][1] = 1.0f;
	pep_gCubeVertices[4][0][2] = -1.0f;

	pep_gCubeVertices[4][1][0] = -1.0f;
	pep_gCubeVertices[4][1][1] = 1.0f;
	pep_gCubeVertices[4][1][2] = -1.0f;

	pep_gCubeVertices[4][2][0] = -1.0f;
	pep_gCubeVertices[4][2][1] = 1.0f;
	pep_gCubeVertices[4][2][2] = 1.0f;

	pep_gCubeVertices[4][3][0] = 1.0f;
	pep_gCubeVertices[4][3][1] = 1.0f;
	pep_gCubeVertices[4][3][2] = 1.0f;

	// BottomSide-Vertices
	pep_gCubeVertices[5][0][0] = 1.0f;
	pep_gCubeVertices[5][0][1] = -1.0f;
	pep_gCubeVertices[5][0][2] = -1.0f;

	pep_gCubeVertices[5][1][0] = -1.0f;
	pep_gCubeVertices[5][1][1] = -1.0f;
	pep_gCubeVertices[5][1][2] = -1.0f;

	pep_gCubeVertices[5][2][0] = -1.0f;
	pep_gCubeVertices[5][2][1] = -1.0f;
	pep_gCubeVertices[5][2][2] = 1.0f;

	pep_gCubeVertices[5][3][0] = 1.0f;
	pep_gCubeVertices[5][3][1] = -1.0f;
	pep_gCubeVertices[5][3][2] = 1.0f;

	//
	// TexCoord
	//

	// FrontSide-TexCoord
	pep_gCubeTexCoords[0][0][0] = 0.0f;
	pep_gCubeTexCoords[0][0][1] = 0.0f;

	pep_gCubeTexCoords[0][1][0] = 1.0f;
	pep_gCubeTexCoords[0][1][1] = 0.0f;

	pep_gCubeTexCoords[0][2][0] = 1.0f;
	pep_gCubeTexCoords[0][2][1] = 1.0f;

	pep_gCubeTexCoords[0][3][0] = 0.0f;
	pep_gCubeTexCoords[0][3][1] = 1.0f;

	// RightSide-TexCoord
	pep_gCubeTexCoords[1][0][0] = 0.0f;
	pep_gCubeTexCoords[1][0][1] = 0.0f;

	pep_gCubeTexCoords[1][1][0] = 1.0f;
	pep_gCubeTexCoords[1][1][1] = 0.0f;

	pep_gCubeTexCoords[1][2][0] = 1.0f;
	pep_gCubeTexCoords[1][2][1] = 1.0f;

	pep_gCubeTexCoords[1][3][0] = 0.0f;
	pep_gCubeTexCoords[1][3][1] = 1.0f;

	// BackSide-TexCoord
	pep_gCubeTexCoords[2][0][0] = 0.0f;
	pep_gCubeTexCoords[2][0][1] = 0.0f;

	pep_gCubeTexCoords[2][1][0] = 1.0f;
	pep_gCubeTexCoords[2][1][1] = 0.0f;

	pep_gCubeTexCoords[2][2][0] = 1.0f;
	pep_gCubeTexCoords[2][2][1] = 1.0f;

	pep_gCubeTexCoords[2][3][0] = 0.0f;
	pep_gCubeTexCoords[2][3][1] = 1.0f;

	// LeftSide-TexCoord
	pep_gCubeTexCoords[3][0][0] = 0.0f;
	pep_gCubeTexCoords[3][0][1] = 0.0f;

	pep_gCubeTexCoords[3][1][0] = 1.0f;
	pep_gCubeTexCoords[3][1][1] = 0.0f;

	pep_gCubeTexCoords[3][2][0] = 1.0f;
	pep_gCubeTexCoords[3][2][1] = 1.0f;

	pep_gCubeTexCoords[3][3][0] = 0.0f;
	pep_gCubeTexCoords[3][3][1] = 1.0f;

	// TopSide-TexCoord
	pep_gCubeTexCoords[4][0][0] = 0.0f;
	pep_gCubeTexCoords[4][0][1] = 0.0f;

	pep_gCubeTexCoords[4][1][0] = 1.0f;
	pep_gCubeTexCoords[4][1][1] = 0.0f;

	pep_gCubeTexCoords[4][2][0] = 1.0f;
	pep_gCubeTexCoords[4][2][1] = 1.0f;

	pep_gCubeTexCoords[4][3][0] = 0.0f;
	pep_gCubeTexCoords[4][3][1] = 1.0f;

	// BottomSide-TexCoord
	pep_gCubeTexCoords[5][0][0] = 0.0f;
	pep_gCubeTexCoords[5][0][1] = 0.0f;

	pep_gCubeTexCoords[5][1][0] = 1.0f;
	pep_gCubeTexCoords[5][1][1] = 0.0f;

	pep_gCubeTexCoords[5][2][0] = 1.0f;
	pep_gCubeTexCoords[5][2][1] = 1.0f;

	pep_gCubeTexCoords[5][3][0] = 0.0f;
	pep_gCubeTexCoords[5][3][1] = 1.0f;

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	LoadGlTexture(&pep_texture_kundali, MAKEINTRESOURCEA(IDBITMAP_KUNDALI));

	glEnable(GL_TEXTURE_2D); // enable the texture memory

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(PEP_WIDTH, PEP_HEIGHT);

	return;
}

bool LoadGlTexture(GLuint *pep_texture, TCHAR pep_resourceID[])
{
	//
	// Variable Declarations
	//
	bool pep_bResult = false;
	HBITMAP pep_hBitmap = NULL; // OS image loading related code
	BITMAP pep_bmp; // OS image loading related code

	//
	// Code
	//
	pep_hBitmap = (HBITMAP)LoadImage( // OS image loading related code
		GetModuleHandle(NULL), // NULL -> Return's hInstance of the current process/application.
		pep_resourceID,
		IMAGE_BITMAP, // which type of image to be load
		0, // width of image which is going to be load. 0(Zero) -> Means This Is Not ICON or CURSOR it is BITMAP
		0, // height of image which is going to be load. 0(Zero) -> Means This Is Not ICON or CURSOR it is BITMAP
		LR_CREATEDIBSECTION // In which image format to be loaded. LR_CREATEDIBSECTION -> load in windows native image format. LR -> load resource. DIB -> Device Independent Bitmap
	);
	// why not file , why resource
	// resource get embbed in exe.

	if (NULL == pep_hBitmap)
	{
		return pep_bResult;
	}

	pep_bResult = true;

	GetObject( // OS image loading related code
		pep_hBitmap,
		sizeof(pep_bmp),
		&pep_bmp
	);

	// alignment of image data
	glPixelStorei(
		GL_UNPACK_ALIGNMENT, // unpacked
		4 // rgba - per pixel -> alignment is depend of how much data we are using for 1 pixel
		);

	// texture buffer gets created at GPU side
	// and it is get mapped with cpu variable (called as target point) i.e. texture
	// generate texture can be multiple in one call
	glGenTextures(
		1, // no. of buffers
		pep_texture // name of target point which will be point to gpu texture memory
	);

	// step 4: binding texture
	//bind is one at a time
	//
	glBindTexture(
		GL_TEXTURE_2D,
		*pep_texture
	);

	//step 5: setting of texture parameters
	//maginafication -> near to audience
	//minification -> far from audience
	//mipmap -> image size decrease in  power of 2
	glTexParameteri(
		GL_TEXTURE_2D, // target 
		GL_TEXTURE_MAG_FILTER, // (which parameter to set) -> maginfication (near to far from audience)
		GL_LINEAR // quality (weighted avrage)
	);

	glTexParameteri(
		GL_TEXTURE_2D,
		GL_TEXTURE_MIN_FILTER, // minification
		GL_LINEAR_MIPMAP_LINEAR // decrease image size + quality by weighted avarage
	);

	// information of image required
	// image format
	// image width
	// image height
	// image data

	// push image data into memory with help of graphics driver
	gluBuild2DMipmaps(
		GL_TEXTURE_2D,
		3, // no. of colors (rgb) -> internal format
		pep_bmp.bmWidth,
		pep_bmp.bmHeight,
		GL_BGR_EXT, // windows os native bitmap format given to opengl
		GL_UNSIGNED_BYTE,
		pep_bmp.bmBits // image data
	);

	// gluBuild2DMipmaps = glTexImage2D + glGenerateMipmap

	DeleteObject(pep_hBitmap); // os related code

	pep_hBitmap = NULL;

	return pep_bResult;
}

void ReSize(int pep_width, int pep_height)
{
	if (0 == pep_height)
	{
		pep_height = 1;
	}

	glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);

	return;
}

void Display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(pep_gCubeRotationAngle, 1.0f, 1.0f, 1.0f);

	glBindTexture(
		GL_TEXTURE_2D,
		pep_texture_kundali
	);

	for (int i = 0; i < 6; i++)
	{
		glBegin(GL_QUADS);

		glTexCoord2f(pep_gCubeTexCoords[i][0][0], pep_gCubeTexCoords[i][0][1]);
		glVertex3f(pep_gCubeVertices[i][0][0], pep_gCubeVertices[i][0][1], pep_gCubeVertices[i][0][2]);

		glTexCoord2f(pep_gCubeTexCoords[i][1][0], pep_gCubeTexCoords[i][1][1]);
		glVertex3f(pep_gCubeVertices[i][1][0], pep_gCubeVertices[i][1][1], pep_gCubeVertices[i][1][2]);

		glTexCoord2f(pep_gCubeTexCoords[i][2][0], pep_gCubeTexCoords[i][2][1]);
		glVertex3f(pep_gCubeVertices[i][2][0], pep_gCubeVertices[i][2][1], pep_gCubeVertices[i][2][2]);

		glTexCoord2f(pep_gCubeTexCoords[i][3][0], pep_gCubeTexCoords[i][3][1]);
		glVertex3f(pep_gCubeVertices[i][3][0], pep_gCubeVertices[i][3][1], pep_gCubeVertices[i][3][2]);
		glEnd();
	}

	glBindTexture(
		GL_TEXTURE_2D,
		0
	);

	SwapBuffers(pep_gHdc);
}

void Update(void)
{
	if (pep_gCubeRotationAngle > 360.0f)
	{
		pep_gCubeRotationAngle = 1.0f;
	}
	pep_gCubeRotationAngle += 1.0f;
}

void UnInitialize(void)
{
	//
	// Code
	//
	glDeleteTextures(1, &pep_texture_kundali);

	if (true == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (pep_gHglrc == wglGetCurrentContext())
	{
		wglMakeCurrent(0, 0);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
		pep_gHglrc = NULL;
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
		pep_gHdc = NULL;
	}

	if (NULL != pep_gpFile)
	{
		fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
		fclose(pep_gpFile);
		pep_gpFile = NULL;
	}

	return;
}
