#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "DasaraDemo.h"
#include <time.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <windowsx.h>

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")
#pragma comment(lib, "Winmm.lib")

#define WIN_WIDTH_PSM 800
#define WIN_HEIGHT_PSM 600

// Introduction
#define ASTROMEDICOMP_ANIMATION_STEPS 800
#define GREEN_COLOR 0.51f

enum AnimationObject
{
	NONE,
	FIRST_3_WORDS,
	FORM_ASTROMEDICOMP,
	KNOWLEDGE_IS_INTERRELATED,
	FADEOUT_ASTROMEDICOMP
};
AnimationObject currentAnimationObject = FIRST_3_WORDS;

// Leafs
#define NUMBEROFLEAVES 250
#define SMALL_LEAVES_SPEED 0.005f
#define LARGE_LEAVES_SPEED 0.0055f
#define INTERVAL_BETWEEN_INCREASING_COUNT 0.006f;
#define NUMBER_OF_INTERVAL 8
#define NUMBEROFPOINTSINARC 800
#define LEFT_BORDER_OF_LEAVES_STACK 0
#define RIGHT_BORDER_OF_LEAVES_STACK 250

GLfloat ScaleArrayOfLeaves[NUMBEROFLEAVES];
GLfloat xArrayOfLeaves[NUMBEROFLEAVES];
GLfloat yArrayOfLeaves[NUMBEROFLEAVES];
GLfloat TranslateOfLeaves_OnXAxis[NUMBEROFLEAVES] = {0.0f};
GLfloat TranslateOfLeaves_OnYAxis[NUMBEROFLEAVES] = {0.0f};
GLfloat ArcOfLeafFalling[NUMBEROFPOINTSINARC][2];
GLfloat AngleOfLeaf_atEnd[NUMBEROFLEAVES] = {0.0f};
GLfloat LeafAngle = 0.0f;
GLfloat FallingLeafTiming = 0.0f;
GLfloat gFadeColorValue = 1.0f;
GLint TranslateX_temp[NUMBEROFLEAVES] = {1};
GLint IncreaseCountOfLeave = NUMBER_OF_INTERVAL;
GLint Iterator_ArcOfLeafArray[NUMBEROFPOINTSINARC] = {0};
GLint FlowOfIterator_ArcOfLeafArray[NUMBEROFPOINTSINARC] = {0};

bool Flag_ShowLeaveFallingAnimation = true;
bool StopFallingLeaves[NUMBEROFLEAVES] = {false};
bool Flag_ShowPolygonToCoverRemainingArea = false;

// Cutout and diva
GLfloat gfTilakY_PSM = 0.35f;
GLfloat gfCutOutZ_PSM = 1.0f;
GLfloat fDivaX_PSM = 180.0f;
GLfloat fDivaY_PSM = 180.0f;
GLfloat fDivaZ_PSM = -3.0f;

GLfloat fDivaScaleX_PSM = 0.0f;
GLfloat fDivaScaleY_PSM = 0.0f;
GLfloat fDivaScaleZ_PSM = 0.0f;

bool gbisDivaInPlace_PSM = false;
bool gbIntroComplete_PSM = false;
bool gbIntro2Complete_PSM = false;
bool gbCutoutComplete_PSM = false;
bool gbThankYouComplete_PSM = false;

// outro
GLfloat fontHeightChange = 1.0f / 2.0f;
GLfloat fontWidthChange = 1.6f / 2.0f;
GLfloat translateChange_X = 1.8;
GLint giDivaAnimationCount_PSM = 0;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void circleDraw(GLfloat startAngle, GLfloat OuterCircleRadius, GLfloat innerCircleRadius, GLfloat endAngle);
void LetterT(void);
void LetterH(void);
void LetterA(void);
void LetterN(void);
void LetterK(void);

FILE *gpFile_PSM = NULL;

HWND ghwnd_PSM = NULL;
DWORD dwStyle_PSM = NULL;
WINDOWPLACEMENT wpPrev_PSM = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_PSM = false;

HDC ghdc_PSM = NULL;
HGLRC ghrc_PSM = NULL;
HINSTANCE ghInstance = NULL;
bool gbActiveWindow_PSM = false;
bool bisPlaying_PSM = false;
bool gbOutroComplete_PSM = false;

GLfloat colorGreenForInfo = 0.0f;
GLfloat colorGreenForInfoHeader = 3.1f;

GLint WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, GLint iCmdShow)
{
	void Initialize(void);
	void Display(void);
	void Update(void);
	GLint fillXandYArrayWithRandomNumbers(GLfloat * pFloatArray, GLint iSize, char);
	void fillScaleArrayWithRandomNumbers(GLfloat * pFloatArray, GLint iSize);
	void FillArcCoordinatesForLeaf(GLfloat arr[][2], GLint ArrSize);
	void fillShortInt_Array_WithRandomNumbers(GLint *pShortArray, GLint iSize, char Axis);

	WNDCLASSEX wndclass_PSM;
	HWND hwnd_PSM;
	MSG msg_PSM;
	TCHAR szAppName_PSM[] = TEXT("MyApp");
	bool bDone_PSM = false;

	if (fopen_s(&gpFile_PSM, "logs_PSM.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d File successfully create and program started\n", __DATE__, __TIME__, __FILE__, __LINE__);
	}

	GLint iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	GLint iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	GLint iScreenCenterX = iScreenWidth / 2;
	GLint iScreenCenterY = iScreenHeight / 2;

	GLint iWindowCenterX = WIN_WIDTH_PSM / 2;
	GLint iWindowsCenterY = WIN_HEIGHT_PSM / 2;

	GLint iWindowX = iScreenCenterX - iWindowCenterX;
	GLint iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_PSM.cbSize = sizeof(WNDCLASSEX);
	wndclass_PSM.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_PSM.cbClsExtra = 0;
	wndclass_PSM.cbWndExtra = 0;
	wndclass_PSM.hInstance = hInstance;
	wndclass_PSM.lpfnWndProc = WndProc;
	wndclass_PSM.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON_PSM));
	wndclass_PSM.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_PSM.lpszClassName = szAppName_PSM;
	wndclass_PSM.lpszMenuName = NULL;
	wndclass_PSM.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_PSM.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON_PSM));

	RegisterClassEx(&wndclass_PSM);

	hwnd_PSM = CreateWindowEx(WS_EX_APPWINDOW,
							  szAppName_PSM,
							  TEXT("Dasara demo"),
							  WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							  iWindowX,
							  iWindowY,
							  WIN_WIDTH_PSM,
							  WIN_HEIGHT_PSM,
							  NULL,
							  NULL,
							  hInstance,
							  NULL);
	ghwnd_PSM = hwnd_PSM;
	ghInstance = hInstance;
	Initialize();
	ShowWindow(hwnd_PSM, iCmdShow);
	SetForegroundWindow(hwnd_PSM);
	SetFocus(hwnd_PSM);

	fillScaleArrayWithRandomNumbers(ScaleArrayOfLeaves, NUMBEROFLEAVES);
	fillXandYArrayWithRandomNumbers(xArrayOfLeaves, NUMBEROFLEAVES, 'X');
	fillXandYArrayWithRandomNumbers(yArrayOfLeaves, NUMBEROFLEAVES, 'Y');
	fillShortInt_Array_WithRandomNumbers(Iterator_ArcOfLeafArray, NUMBEROFPOINTSINARC, 'TX');

	FillArcCoordinatesForLeaf(ArcOfLeafFalling, NUMBEROFPOINTSINARC);

	for (GLint i = 0; i < NUMBEROFPOINTSINARC; i++)
	{
		FlowOfIterator_ArcOfLeafArray[i] = 1;
	}

	while (bDone_PSM == false)
	{
		if (PeekMessage(&msg_PSM, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_PSM.message == WM_QUIT)
			{
				bDone_PSM = true;
			}
			else
			{
				TranslateMessage(&msg_PSM);
				DispatchMessage(&msg_PSM);
			}
		}
		else
		{
			if (gbActiveWindow_PSM == true)
			{
				Display();
				Update();
			}
		}
	}

	return ((GLint)msg_PSM.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Resize(GLint, GLint);
	void Uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
	{
		if (bisPlaying_PSM != TRUE)
		{
			bisPlaying_PSM = PlaySound(MAKEINTRESOURCE(BGM_PSM), ghInstance, SND_RESOURCE | SND_ASYNC);
		}
		gbActiveWindow_PSM = true;
		break;
	}
	case WM_KILLFOCUS:
	{
		// gbActiveWindow_PSM = false;
		break;
	}
	case WM_ERASEBKGND:
	{
		return (0);
	}
	case WM_SIZE:
	{
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			DestroyWindow(hwnd);
			break;
		}
		case 0x46:
		case 0x66:
		{
			ToggleFullScreen();
			break;
		}
		default:
		{
			break;
		}
		}
		break;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY:
	{
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	default:
	{
		break;
	}
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi_PSM = {sizeof(MONITORINFO)};

	if (gbFullScreen_PSM == false)
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d Program is in full screen mode\n", __DATE__, __TIME__, __FILE__, __LINE__);

		dwStyle_PSM = GetWindowLong(ghwnd_PSM, GWL_STYLE);
		if ((dwStyle_PSM & WS_OVERLAPPEDWINDOW))
		{
			if ((GetWindowPlacement(ghwnd_PSM, &wpPrev_PSM) && (GetMonitorInfo(MonitorFromWindow(ghwnd_PSM, MONITORINFOF_PRIMARY), &mi_PSM))))
			{
				SetWindowLong(ghwnd_PSM, GWL_STYLE, (dwStyle_PSM & (~WS_OVERLAPPEDWINDOW)));
				SetWindowPos(ghwnd_PSM,
							 HWND_TOP,
							 mi_PSM.rcMonitor.left,
							 mi_PSM.rcMonitor.top,
							 (mi_PSM.rcMonitor.right - mi_PSM.rcMonitor.left),
							 (mi_PSM.rcMonitor.bottom - mi_PSM.rcMonitor.top),
							 SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen_PSM = true;
	}
	else
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d Program exiting full screen mode\n", __DATE__, __TIME__, __FILE__, __LINE__);
		SetWindowLong(ghwnd_PSM, GWL_STYLE, (dwStyle_PSM | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_PSM, &wpPrev_PSM);
		SetWindowPos(ghwnd_PSM,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
		ShowCursor(TRUE);
		gbFullScreen_PSM = false;
	}
}

void Initialize(void)
{
	void Resize(GLint, GLint);
	void ToggleFullScreen(void);
	PIXELFORMATDESCRIPTOR pFD_PSM;
	GLint iPixelFormatIndex_PSM;

	ghdc_PSM = GetDC(ghwnd_PSM);

	ZeroMemory(&pFD_PSM, sizeof(PIXELFORMATDESCRIPTOR));
	pFD_PSM.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pFD_PSM.nVersion = 1;
	pFD_PSM.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pFD_PSM.iPixelType = PFD_TYPE_RGBA;
	pFD_PSM.cColorBits = 32;
	pFD_PSM.cRedBits = 8;
	pFD_PSM.cGreenBits = 8;
	pFD_PSM.cBlueBits = 8;
	pFD_PSM.cAlphaBits = 8;
	pFD_PSM.cDepthBits = 32;

	iPixelFormatIndex_PSM = ChoosePixelFormat(ghdc_PSM, &pFD_PSM);
	if (iPixelFormatIndex_PSM == 0)
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d ChoosePixelFormat failed\n", __DATE__, __TIME__, __FILE__, __LINE__);
		DestroyWindow(ghwnd_PSM);
	}

	if (SetPixelFormat(ghdc_PSM, iPixelFormatIndex_PSM, &pFD_PSM) == FALSE)
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d SetPixelFormat failed\n", __DATE__, __TIME__, __FILE__, __LINE__);
		DestroyWindow(ghwnd_PSM);
	}

	ghrc_PSM = wglCreateContext(ghdc_PSM);
	if (ghrc_PSM == NULL)
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d wglCreateContext failed\n", __DATE__, __TIME__, __FILE__, __LINE__);
		DestroyWindow(ghwnd_PSM);
	}

	if (wglMakeCurrent(ghdc_PSM, ghrc_PSM) == FALSE)
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d wglMakeCurrent failed\n", __DATE__, __TIME__, __FILE__, __LINE__);
		DestroyWindow(ghwnd_PSM);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// Resize(WIN_WIDTH_PSM, WIN_HEIGHT_PSM);
	ToggleFullScreen();
}

void Resize(GLint width, GLint height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void Display(void)
{
	void DrawDivaAndPlate_PSM(void);
	void DrawTilak_PSM(void);
	void StartLeaveFallingAnimation(void);
	void AnimateAstromedicomp(void);
	void DrawOutro_JSS(void);
	void ThankYou(void);
	void MatrixGroup(void);
	void DisplayInfo_JSS(void);
	void TsSynopsisFirstHalf(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	if (!gbIntroComplete_PSM)
	{
		MatrixGroup();
	}
	if (gbIntroComplete_PSM && !gbIntro2Complete_PSM)
	{
		AnimateAstromedicomp();
	}
	if (gbIntro2Complete_PSM && Flag_ShowLeaveFallingAnimation && giDivaAnimationCount_PSM < 3000)
	{
		StartLeaveFallingAnimation();
	}

	if (gbCutoutComplete_PSM && giDivaAnimationCount_PSM < 3000)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		// translate along circular path
		if (gbisDivaInPlace_PSM)
		{
			glTranslatef((cos(fDivaX_PSM) * 0.5f), (sin(fDivaY_PSM) * 0.5f), fDivaZ_PSM);
		}
		else
		{
			glTranslatef((cos(fDivaX_PSM) * 0.4f), (sin(fDivaY_PSM) * 0.4f), fDivaZ_PSM);
		}
		glScalef(fDivaScaleX_PSM, fDivaScaleY_PSM, fDivaScaleZ_PSM);
		DrawDivaAndPlate_PSM();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -2.0f);
		DrawTilak_PSM();
		giDivaAnimationCount_PSM += 1;
		if (giDivaAnimationCount_PSM >= 2500)
		{
			gFadeColorValue -= 0.01f;
		}
	}
	if (!gbOutroComplete_PSM && giDivaAnimationCount_PSM >= 3000)
	{
		DrawOutro_JSS();
	}

	if (gbOutroComplete_PSM && !(colorGreenForInfo < 0.0f))
	{

		TsSynopsisFirstHalf();
		DisplayInfo_JSS();
	}

	if (colorGreenForInfo < 0.0f)
	{
		ThankYou();
	}

	SwapBuffers(ghdc_PSM);
}

void Uninitialize(void)
{
	if (gbFullScreen_PSM == true)
	{
		dwStyle_PSM = GetWindowLong(ghwnd_PSM, GWL_STYLE);

		SetWindowLong(ghwnd_PSM, GWL_STYLE, (dwStyle_PSM | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_PSM, &wpPrev_PSM);
		SetWindowPos(ghwnd_PSM,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc_PSM)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_PSM)
	{
		wglDeleteContext(ghrc_PSM);
		ghrc_PSM = NULL;
	}

	if (ghdc_PSM)
	{
		ReleaseDC(ghwnd_PSM, ghdc_PSM);
		ghdc_PSM = NULL;
	}

	if (gpFile_PSM)
	{
		fprintf(gpFile_PSM, "%s\t%s\t%s\t%d Program terminated successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);
		fclose(gpFile_PSM);
		gpFile_PSM = NULL;
	}
}

void StartLeaveFallingAnimation()
{
	void ChangeRange(GLfloat * old_value);
	void TsCreateSketch(void);
	bool LeaveAnimationComplete = true;

	bool isElementStillDisplay = false;
	GLfloat CurrentTranslate = 0.0f;

	if (Flag_ShowPolygonToCoverRemainingArea)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -3.0f);

		glBegin(GL_QUADS);
		glColor3f(0.827451f * gFadeColorValue, 0.815686f * gFadeColorValue, 0.356863f * gFadeColorValue);

		glVertex3f(0.1f, 3.0f, 0.0f);
		glVertex3f(3.5f, 3.0f, 0.0f);
		glVertex3f(3.5f, -3.0f, 0.0f);
		glVertex3f(0.1f, -3.0f, 0.0f);
		glEnd();
	}

	for (GLint i = 0; i < NUMBEROFLEAVES; i++)
	{
		if (i % IncreaseCountOfLeave == 0)
		{
			if (yArrayOfLeaves[i] + TranslateOfLeaves_OnYAxis[i] < -1.5f && StopFallingLeaves[i] == false)
			{
				TranslateOfLeaves_OnYAxis[i] = 3.0f;
				if (IncreaseCountOfLeave <= 2)
				{
					if (FallingLeafTiming > 5.0f)
					{
						StopFallingLeaves[i] = true;
						if (ScaleArrayOfLeaves[i] < 0.3f)
						{
							ScaleArrayOfLeaves[i] += 0.45f;
						}
						else
						{
							ScaleArrayOfLeaves[i] += 0.25f;
						}
						AngleOfLeaf_atEnd[i] = LeafAngle;
						ChangeRange(&yArrayOfLeaves[i]);
					}
				}
			}
			if (ScaleArrayOfLeaves[i] < 0.3f)
			{
				//Translate[i] -= 0.0002f;
				if (StopFallingLeaves[i] == true)
				{
					if (TranslateOfLeaves_OnYAxis[i] > -2.5f)
					{
						TranslateOfLeaves_OnYAxis[i] -= SMALL_LEAVES_SPEED;
					}
				}
				else
				{
					TranslateOfLeaves_OnYAxis[i] -= SMALL_LEAVES_SPEED;
				}
			}
			else
			{
				if (StopFallingLeaves[i] == true)
				{
					if (TranslateOfLeaves_OnYAxis[i] > -2.5f)
					{
						TranslateOfLeaves_OnYAxis[i] -= LARGE_LEAVES_SPEED;
					}
				}
				else
				{
					TranslateOfLeaves_OnYAxis[i] -= LARGE_LEAVES_SPEED;
				}
			}

			if (Iterator_ArcOfLeafArray[i] > NUMBEROFPOINTSINARC)
			{
				FlowOfIterator_ArcOfLeafArray[i] = -1;
			}
			else if (Iterator_ArcOfLeafArray[i] <= 0)
			{
				FlowOfIterator_ArcOfLeafArray[i] = 1;
			}
			Iterator_ArcOfLeafArray[i] += FlowOfIterator_ArcOfLeafArray[i];
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			if (StopFallingLeaves[i] == false || TranslateOfLeaves_OnYAxis[i] > -2.5f)
			{
				LeaveAnimationComplete = false;
				//glTranslatef(xArrayOfLeaves[i] + TranslateOfLeaves_OnXAxis[i] + ArcOfLeafFalling[Iterator_ArcOfLeafArray][0], yArrayOfLeaves[i] + TranslateOfLeaves_OnYAxis[i] + ArcOfLeafFalling[Iterator_ArcOfLeafArray][1], -3.0f);
				glTranslatef(xArrayOfLeaves[i] + ArcOfLeafFalling[Iterator_ArcOfLeafArray[i]][0], yArrayOfLeaves[i] + TranslateOfLeaves_OnYAxis[i] + ArcOfLeafFalling[Iterator_ArcOfLeafArray[i]][1], -3.0f);

				//glTranslatef(xArrayOfLeaves[i]+ UAngleX, yArrayOfLeaves[i]+ UAngleY, -3.0f);
				glScalef(ScaleArrayOfLeaves[i], ScaleArrayOfLeaves[i], ScaleArrayOfLeaves[i]);
				if ((i % 3) == 0)
				{
					glRotatef((ScaleArrayOfLeaves[i] * 2000) + LeafAngle, 0.0f, 1.0f, 1.0f);
				}
				else if ((i % 3) == 1)
				{
					glRotatef((ScaleArrayOfLeaves[i] * 2000) + LeafAngle, 1.0f, 1.0f, 0.0f);
				}
				else
				{
					glRotatef((ScaleArrayOfLeaves[i] * 2000) + LeafAngle, 0.0f, 1.0f, 0.0f);
				}
			}
			else
			{
				glTranslatef(xArrayOfLeaves[i], yArrayOfLeaves[i] + TranslateOfLeaves_OnYAxis[i], -3.0f);
				glScalef(ScaleArrayOfLeaves[i], ScaleArrayOfLeaves[i], ScaleArrayOfLeaves[i]);
				glRotatef((ScaleArrayOfLeaves[i] * 2000) + AngleOfLeaf_atEnd[i], 0.0f, 0.0f, 1.0f);
			}
			glColor3f(1.0f * gFadeColorValue, 1.0f * gFadeColorValue, 0.0f);
			glBegin(GL_QUADS);
			glVertex3f(0.145f, 0.48f, 0.0f);
			glVertex3f(0.155f, 0.47f, 0.0f);
			glVertex3f(0.025f, 0.27f, 0.0f);
			glVertex3f(0.02f, 0.275f, 0.0f);
			glEnd();

			glBegin(GL_POLYGON);
			//Left Side of leaf
			glColor3f(0.690196f * gFadeColorValue, 0.552941f * gFadeColorValue, 0.047059f * gFadeColorValue);
			glVertex3f(0.03f, 0.24f, 0.0f);
			glVertex3f(-0.03f, 0.37f, 0.0f);
			glVertex3f(-0.11f, 0.41f, 0.0f);
			glVertex3f(-0.24f, 0.41f, 0.0f);
			glColor3f(0.756863f * gFadeColorValue, 0.647059f * gFadeColorValue, 0.105882f * gFadeColorValue);
			glVertex3f(-0.34f, 0.39f, 0.0f);
			glColor3f(0.729412f * gFadeColorValue, 0.666667f * gFadeColorValue, 0.133333f * gFadeColorValue);
			glVertex3f(-0.46f, 0.21f, 0.0f);

			glVertex3f(-0.5f, 0.05f, 0.0f);
			glColor3f(0.827451f * gFadeColorValue, 0.815686f * gFadeColorValue, 0.356863f * gFadeColorValue);
			glVertex3f(-0.5f, -0.19f, 0.0f);
			glVertex3f(-0.45f, -0.29f, 0.0f);

			glColor3f(0.756863f * gFadeColorValue, 0.796078f * gFadeColorValue, 0.243137f * gFadeColorValue);
			glVertex3f(-0.33f, -0.46f, 0.0f);
			glVertex3f(-0.28f, -0.49f, 0.0f);

			glColor3f(0.764706f * gFadeColorValue, 0.800000f * gFadeColorValue, 0.274510f * gFadeColorValue);
			glVertex3f(-0.225f, -0.495f, 0.0f);

			glColor3f(0.796078f * gFadeColorValue, 0.800000f * gFadeColorValue, 0.380392f * gFadeColorValue);

			glVertex3f(-0.16f, -0.46f, 0.0f);
			glVertex3f(-0.1f, -0.39f, 0.0f);
			glVertex3f(-0.05f, -0.3f, 0.0f);
			glEnd();

			//Lines on the left part of leaf
			glColor3f(0.658824f * gFadeColorValue, 0.525490f * gFadeColorValue, 0.015686f * gFadeColorValue);
			glLineWidth(2.0f);
			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(-0.1, 0.36, 0.0f);
			glVertex3f(-0.25, 0.37, 0.0f);
			glVertex3f(-0.37, 0.34, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(-0.13, 0.27, 0.0f);
			glVertex3f(-0.32, 0.24, 0.0f);
			glVertex3f(-0.47, 0.16, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(-0.16, 0.18, 0.0f);
			glVertex3f(-0.38, 0.05, 0.0f);
			glVertex3f(-0.5, -0.12, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(-0.27, -0.14, 0.0f);
			glVertex3f(-0.36, -0.38, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(-0.28, -0.49, 0.0f);
			glEnd();

			glLineWidth(1.0f);

			glBegin(GL_POLYGON);
			//Right side
			glColor3f(0.725490f * gFadeColorValue, 0.666667f * gFadeColorValue, 0.160784f * gFadeColorValue);
			glVertex3f(-0.05f, -0.3f, 0.0f);
			glVertex3f(-0.03f, -0.45f, 0.0f);

			glColor3f(0.705882f * gFadeColorValue, 0.721569f * gFadeColorValue, 0.180392f * gFadeColorValue);
			glVertex3f(-0.01f, -0.48f, 0.0f);

			glColor3f(0.745098f * gFadeColorValue, 0.780392f * gFadeColorValue, 0.227451f * gFadeColorValue);
			glVertex3f(0.075f, -0.535f, 0.0f);
			glVertex3f(0.15f, -0.54f, 0.0f);
			glVertex3f(0.2f, -0.535f, 0.0f);

			glColor3f(0.717647f * gFadeColorValue, 0.721569f * gFadeColorValue, 0.243137f * gFadeColorValue);
			glVertex3f(0.36f, -0.35f, 0.0f);
			glVertex3f(0.48f, -0.07f, 0.0f);

			glColor3f(0.741176f * gFadeColorValue, 0.619608f * gFadeColorValue, 0.109804f * gFadeColorValue);
			glVertex3f(0.47f, 0.075f, 0.0f);
			glVertex3f(0.45f, 0.18f, 0.0f);

			glColor3f(0.756863f * gFadeColorValue, 0.627451f * gFadeColorValue, 0.117647f * gFadeColorValue);
			glVertex3f(0.385f, 0.32f, 0.0f);

			glColor3f(0.678431f * gFadeColorValue, 0.541176f * gFadeColorValue, 0.031373f * gFadeColorValue);

			glVertex3f(0.3f, 0.39f, 0.0f);
			glVertex3f(0.25f, 0.395f, 0.0f);
			glVertex3f(0.18f, 0.39f, 0.0f);

			glColor3f(0.678431f * gFadeColorValue, 0.541176f * gFadeColorValue, 0.031373f * gFadeColorValue);
			glVertex3f(0.11f, 0.35f, 0.0f);
			glVertex3f(0.03f, 0.24f, 0.0f);
			glEnd();

			//Lines on the right part of the leaf
			glColor3f(0.658824f * gFadeColorValue, 0.525490f * gFadeColorValue, 0.015686f * gFadeColorValue);
			glLineWidth(2.0f);
			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(0.2, 0.3, 0.0f);
			glVertex3f(0.18, 0.32, 0.0f);
			glVertex3f(0.41, 0.25, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(0.33, 0.17, 0.0f);
			glVertex3f(0.4, 0.07, 0.0f);
			glVertex3f(0.45, -0.15, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(0.2, 0.09, 0.0f);
			glVertex3f(0.34, -0.15, 0.0f);
			glVertex3f(0.36, -0.3, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(0.14, 0.05, 0.0f);
			glVertex3f(0.2, -0.2, 0.0f);
			glVertex3f(0.25, -0.46, 0.0f);
			glEnd();

			glBegin(GL_LINE_STRIP);
			glVertex3f(0.03, 0.24, 0.0f);
			glVertex3f(0.12, -0.54, 0.0f);
			glEnd();

			glLineWidth(1.0f);
		}
	}
	if (Flag_ShowPolygonToCoverRemainingArea)
	{

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.0f, 0.0f, gfCutOutZ_PSM);
		glScalef(1.25f, 1.25f, 1.0f);
		TsCreateSketch();
	}

	Flag_ShowPolygonToCoverRemainingArea = LeaveAnimationComplete;
}

void fillShortInt_Array_WithRandomNumbers(GLint *pShortArray, GLint iSize, char Axis)
{
	GLint i, upper = iSize, lower = 0;

	for (i = 0; i < iSize; i++)
	{
		pShortArray[i] = (rand() % (upper - lower + 1)) + lower;
	}
}

GLint fillXandYArrayWithRandomNumbers(GLfloat *pFloatArray, GLint iSize, char Axis)
{
	GLint i, upper, lower;
	GLfloat temp = 0.0f;

	if (Axis == 'X' || Axis == 'x')
	{
		upper = RIGHT_BORDER_OF_LEAVES_STACK;
		lower = LEFT_BORDER_OF_LEAVES_STACK;
	}
	else if (Axis == 'Y' || Axis == 'y')
	{
		upper = 1000;
		lower = 200;
	}
	else if (Axis == 'TX' || Axis == 'tx') //Translate X
	{
		upper = 220;
		lower = 320;
	}
	else
	{
		return -1;
	}

	for (i = 0; i < iSize; i++)
	{
		temp = (rand() % (upper - lower + 1)) + lower;
		pFloatArray[i] = temp / 100;
	}

	return 0;
}

void fillScaleArrayWithRandomNumbers(GLfloat *pFloatArray, GLint iSize)
{
	GLint i = 0;
	const GLfloat fScale = 1.0f / (GLfloat)RAND_MAX;
	srand(time(0));
	while (i < iSize)
	{
		pFloatArray[i] = fScale * rand();
		if (pFloatArray[i] >= 0.15f && pFloatArray[i] <= 0.5f)
		{
			i++;
		}
	}
}

void Update(void)
{
	if (Flag_ShowLeaveFallingAnimation)
	{
		static GLint ChangeXAxisFlow = 1;

		LeafAngle += 0.1f;

		if (FallingLeafTiming < 5.0f)
		{
			FallingLeafTiming += INTERVAL_BETWEEN_INCREASING_COUNT;
		}
		else if (IncreaseCountOfLeave > 2)
		{
			IncreaseCountOfLeave -= 2;
			FallingLeafTiming = 0.0f;
		}
	}

	if (Flag_ShowPolygonToCoverRemainingArea)
	{
		if (gfCutOutZ_PSM > -3.0f)
		{
			gfCutOutZ_PSM -= 0.01f;
		}
		else
		{
			gfCutOutZ_PSM = -3.0f;
		}
	}
	if (gbCutoutComplete_PSM)
	{
		if (fDivaZ_PSM >= -2.0f)
		{
			gbisDivaInPlace_PSM = true;
		}
		else
		{
			fDivaZ_PSM += 0.007f;
		}
		if (gbisDivaInPlace_PSM)
		{
			if (gfTilakY_PSM <= 0.45f)
			{
				gfTilakY_PSM += 0.0009f;
			}

			fDivaX_PSM += 0.01f;
			fDivaY_PSM += 0.01f;
		}
		if (fDivaScaleX_PSM <= 0.3)
		{
			fDivaScaleX_PSM += 0.01f;
			fDivaScaleY_PSM += 0.01f;
			fDivaScaleZ_PSM += 0.01f;
		}
	}
}

void FillArcCoordinatesForLeaf(GLfloat arr[][2], GLint ArrSize)
{
	GLfloat StartAngle = 220 * M_PI / 180.0f;
	GLfloat EndAngle = 320 * M_PI / 180.0f;

	GLfloat angle = StartAngle;

	GLfloat different = (EndAngle - StartAngle) / ArrSize;
	for (GLint i = 0; i < ArrSize; i++)
	{
		glVertex3f(cos(angle), sin(angle), 0.0f);
		arr[i][0] = cos(angle);
		arr[i][1] = sin(angle);
		angle += different;
	}
}

void ChangeRange(GLfloat *old_value)
{
	GLfloat old_min = 200.0f;
	GLfloat old_max = 1000.0f;
	GLfloat new_max = 390.0f;
	GLfloat new_min = 130.0f;

	*old_value = *old_value * 100;
	GLint new_value = ((*old_value - old_min) / (old_max - old_min)) * (new_max - new_min) + new_min;
	*old_value = new_value / 100.0f;
}

void DrawTilak_PSM(void)
{
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f * gFadeColorValue, 0.0f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(0.38f, gfTilakY_PSM, 0.0f);
	glColor3f(1.0f * gFadeColorValue, 0.0f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(0.37f, 0.35f, 0.0f);
	glColor3f(1.0f * gFadeColorValue, 0.0f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(0.39f, 0.35f, 0.0f);
	glEnd();
}

void TsCreateSketch(void)
{
	// Code
	//glBegin(GL_LINES);
	static GLfloat fTsR = 0.690196f;
	static GLfloat fTsG = 0.552941f;
	static GLfloat fTsB = 0.047059f;
	static bool bTsAnimate = true;

	if (fTsR < 0 || fTsG < 0 || fTsB < 0)
	{
		bTsAnimate = false;
	}
	glColor3f(fTsR, fTsG, fTsB); // Temporary white final must be black

	if (bTsAnimate)
	{
		fTsR -= 0.00115;
		fTsG -= 0.00115;
		fTsB -= 0.000115;
	}
	else
	{
		fTsR = fTsG = fTsB = 0.0f;
		gbCutoutComplete_PSM = true;
	}

	// 4th Qudrant
	glBegin(GL_POLYGON);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -0.1f, 0.0f);
	glVertex3f(0.9f, -0.03f, 0.0f);

	// Y-axis 0
	glVertex3f(0.85f, 0.0f, 0.0f);
	glVertex3f(0.85f, 1.0f, 0.0f);
	glEnd();

	// 1st Quadrant
	glBegin(GL_POLYGON);
	glVertex3f(0.85f, 1.0f, 0.0f);
	glVertex3f(0.85f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.03f, 0.0f);
	glVertex3f(0.75f, 0.07f, 0.0f);
	glVertex3f(0.72f, 0.1f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.65f, 0.12f, 0.0f);
	glVertex3f(0.62f, 0.125f, 0.0f);
	glVertex3f(0.6f, 0.14f, 0.0f);
	glVertex3f(0.57f, 0.155f, 0.0f);
	glVertex3f(0.55f, 0.16f, 0.0f);
	glVertex3f(0.515f, 0.175f, 0.0f);
	glVertex3f(0.475f, 0.15f, 0.0f);
	glVertex3f(0.415f, 0.225f, 0.0f);
	glVertex3f(0.415f, 0.225f, 0.0f);
	glVertex3f(0.427f, 0.25f, 0.0f);
	glVertex3f(0.437f, 0.27f, 0.0f);
	glVertex3f(0.44f, 0.29f, 0.0f);
	glVertex3f(0.435f, 0.31f, 0.0f);
	glVertex3f(0.43f, 0.33f, 0.0f);
	glVertex3f(0.425f, 0.35f, 0.0f);
	glVertex3f(0.4f, 0.4f, 0.0f);
	glVertex3f(0.39f, 0.45f, 0.0f);
	glVertex3f(0.385f, 0.49f, 0.0f);
	glVertex3f(0.39f, 0.5f, 0.0f);
	glVertex3f(0.39f, 0.53f, 0.0f);
	glVertex3f(0.387f, 0.55f, 0.0f);
	glVertex3f(0.38f, 0.61f, 0.0f);
	glVertex3f(0.377f, 0.64f, 0.0f);
	glVertex3f(0.36f, 0.68f, 0.0f);
	glVertex3f(0.37f, 0.7f, 0.0f);
	glVertex3f(0.36f, 0.735f, 0.0f);
	glVertex3f(0.35f, 0.75f, 0.0f);
	glVertex3f(0.35f, 0.77f, 0.0f);
	glVertex3f(0.34f, 0.79f, 0.0f);
	glVertex3f(0.32f, 0.8f, 0.0f);
	glVertex3f(0.31f, 0.81f, 0.0f);
	glVertex3f(0.3f, 0.83f, 0.0f);
	glVertex3f(0.27f, 0.84f, 0.0f);
	glVertex3f(0.25f, 0.855f, 0.0f);
	glVertex3f(0.21f, 0.9f, 0.0f);
	glVertex3f(0.195f, 0.91f, 0.0f);
	glVertex3f(0.155f, 0.91f, 0.0f);
	glVertex3f(0.145f, 0.9f, 0.0f);
	glVertex3f(0.13f, 0.918f, 0.0f);
	glVertex3f(0.095f, 0.928f, 0.0f);
	glVertex3f(0.07f, 0.92f, 0.0f);
	glVertex3f(0.06f, 0.925f, 0.0f);
	glVertex3f(0.0f, 0.93f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glEnd();

	// 2nd Qudrant
	glBegin(GL_POLYGON);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.93f, 0.0f);
	glVertex3f(-0.04f, 0.928f, 0.0f);
	glVertex3f(-0.075f, 0.918f, 0.0f);
	glVertex3f(-0.11f, 0.904f, 0.0f);
	glVertex3f(-0.127f, 0.91f, 0.0f);
	glVertex3f(-0.127f, 1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-0.127f, 1.0f, 0.0f);
	glVertex3f(-0.127f, 0.91f, 0.0f);
	glVertex3f(-0.15f, 0.91f, 0.0f);
	glVertex3f(-0.2f, 0.885f, 0.0f);
	glVertex3f(-0.24f, 0.86f, 0.0f);
	glVertex3f(-0.275f, 0.85f, 0.0f);
	glVertex3f(-0.275f, 1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-0.275f, 1.0f, 0.0f);
	glVertex3f(-0.275f, 0.85f, 0.0f);
	glVertex3f(-0.3f, 0.84f, 0.0f);
	glVertex3f(-0.33f, 0.82f, 0.0f);
	glVertex3f(-0.35f, 0.8f, 0.0f);
	glVertex3f(-0.35f, 1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-0.35f, 1.0f, 0.0f);
	glVertex3f(-0.35f, 0.8f, 0.0f);
	glVertex3f(-0.387f, 0.78f, 0.0f);
	glVertex3f(-0.398f, 0.77f, 0.0f);
	glVertex3f(-0.398f, 0.77f, 0.0f);
	glVertex3f(-0.42f, 0.73f, 0.0f);
	glVertex3f(-0.425f, 0.695f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, 1.0f, 0.0f);
	glVertex3f(-0.425f, 0.695f, 0.0f);
	glVertex3f(-0.455f, 1.0f, 0.0f);
	glVertex3f(-0.425f, 0.695f, 0.0f);
	glVertex3f(-0.455f, 0.685f, 0.0f);
	glVertex3f(-0.455f, 0.6f, 0.0f);
	glVertex3f(-1.25f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, 0.6f, 0.0f);
	glVertex3f(-0.455f, 0.6f, 0.0f);
	glVertex3f(-0.45f, 0.577f, 0.0f);
	glVertex3f(-0.432f, 0.55f, 0.0f);
	glVertex3f(-0.429f, 0.53f, 0.0f);
	glVertex3f(-1.25f, 0.53f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, 0.53f, 0.0f);
	glVertex3f(-0.429f, 0.53f, 0.0f);
	glVertex3f(-0.428f, 0.44f, 0.0f);
	glVertex3f(-0.43f, 0.41f, 0.0f);
	glVertex3f(-0.44f, 0.382f, 0.0f);
	glVertex3f(-1.25f, 0.382f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, 0.382f, 0.0f);
	glVertex3f(-0.44f, 0.382f, 0.0f);
	glVertex3f(-0.432f, 0.366f, 0.0f);
	glVertex3f(-0.42f, 0.35f, 0.0f);
	glVertex3f(-0.4f, 0.313f, 0.0f);
	glVertex3f(-0.4f, 0.19f, 0.0f);
	glVertex3f(-1.25f, 0.19f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, 0.19f, 0.0f);
	glVertex3f(-0.4f, 0.19f, 0.0f);
	glVertex3f(-0.383f, 0.15f, 0.0f);
	glVertex3f(-0.37f, 0.12f, 0.0f);
	glVertex3f(-0.36f, 0.1f, 0.0f);
	glVertex3f(-0.35f, 0.08f, 0.0f);
	glVertex3f(-0.33f, 0.06f, 0.0f);
	glVertex3f(-0.323f, 0.047f, 0.0f);
	glVertex3f(-0.3f, 0.03f, 0.0f);
	glVertex3f(-0.28f, 0.0f, 0.0f);
	glVertex3f(-1.25f, 0.0f, 0.0f);
	glEnd();

	// 4th Quadrant
	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, 0.0f, 0.0f);
	glVertex3f(-0.28f, 0.0f, 0.0f);
	glVertex3f(-0.28f, -0.0212f, 0.0f);
	glVertex3f(-0.28f, -0.021f, 0.0f);
	glVertex3f(-0.26f, -0.08f, 0.0f);
	glVertex3f(-0.247f, -0.1f, 0.0f);
	glVertex3f(-0.22f, -0.11f, 0.0f);
	glVertex3f(-0.2f, -0.11f, 0.0f);
	glVertex3f(-0.1f, -0.165f, 0.0f);
	glVertex3f(-0.03f, -0.22f, 0.0f);
	glVertex3f(-1.25f, -0.22f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(-1.25f, -0.22f, 0.0f);
	glVertex3f(-0.03f, -0.22f, 0.0f);
	glVertex3f(-0.08f, -0.3f, 0.0f);
	glVertex3f(-0.1f, -0.3f, 0.0f);
	glVertex3f(-0.094f, -0.315f, 0.0f);
	glVertex3f(-0.07f, -0.34f, 0.0f);
	glVertex3f(-0.1f, -0.38f, 0.0f);
	glVertex3f(-0.111f, -0.4f, 0.0f);
	glVertex3f(-0.135f, -0.5f, 0.0f);
	glVertex3f(-0.15f, -0.55f, 0.0f);
	glVertex3f(-0.16f, -0.6f, 0.0f);
	glVertex3f(-0.17f, -0.65f, 0.0f);
	glVertex3f(-0.17f, -0.7f, 0.0f);
	glVertex3f(-0.2f, -0.75f, 0.0f);
	glVertex3f(-0.22f, -0.78f, 0.0f);
	glVertex3f(-0.222f, -0.83f, 0.0f);
	glVertex3f(-0.23f, -0.85f, 0.0f);
	glVertex3f(-0.228f, -0.88f, 0.0f);
	glVertex3f(-0.22f, -0.9f, 0.0f);
	glVertex3f(-0.21f, -0.925f, 0.0f);
	glVertex3f(-0.215f, -0.95f, 0.0f);
	glVertex3f(-0.235f, -1.0f, 0.0f);
	glVertex3f(-1.25f, -1.0f, 0.0f);
	glEnd();
}

void DrawDivaAndPlate_PSM(void)
{
	void RightKaranda();
	void LeftKaranda();
	GLfloat fcircleAngle_PSM = 0.0f;
	// draw flame
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f * gFadeColorValue, 0.271f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(0.0f, 0.9f, 0.0f);
	glColor3f(0.855 * gFadeColorValue, 0.647 * gFadeColorValue, 0.125 * gFadeColorValue);
	glVertex3f(-0.2f, 0.6f, 0.0f);
	glColor3f(0.933f * gFadeColorValue, 0.910f * gFadeColorValue, 0.667f * gFadeColorValue);
	glVertex3f(0.2f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.855f * gFadeColorValue, 0.647f * gFadeColorValue, 0.125f * gFadeColorValue);
	glVertex3f(0.2f, 0.6f, 0.0f);
	glColor3f(0.855f * gFadeColorValue, 0.647f * gFadeColorValue, 0.125f * gFadeColorValue);
	glVertex3f(-0.2f, 0.6f, 0.0f);
	glColor3f(0.933f * gFadeColorValue, 0.910f * gFadeColorValue, 0.667f * gFadeColorValue);
	glVertex3f(-0.1f, 0.4f, 0.0f);
	glVertex3f(0.1f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f * gFadeColorValue, 0.271f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(0.0f, 0.7f, 0.0f);
	glColor3f(1.0f * gFadeColorValue, 0.271f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(-0.1f, 0.4f, 0.0f);
	glColor3f(1.0f * gFadeColorValue, 0.271f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(0.1f, 0.4f, 0.0f);
	glEnd();

	// draw diva
	glBegin(GL_TRIANGLE_FAN);
	for (fcircleAngle_PSM = (3.14 - 0.1); fcircleAngle_PSM <= (3.14 * 2.0) + 0.1; fcircleAngle_PSM += 0.01f)
	{
		glColor3f(0.855f * gFadeColorValue, 0.647f * gFadeColorValue, 0.125f * gFadeColorValue);
		glVertex3f((cos(fcircleAngle_PSM) * 0.45f), (sin(fcircleAngle_PSM) * 0.45f) + 0.5f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.855f * gFadeColorValue, 0.647f * gFadeColorValue, 0.125f * gFadeColorValue);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(0.855f * gFadeColorValue, 0.647f * gFadeColorValue, 0.125f * gFadeColorValue);
	glVertex3f(-0.1f, 0.1f, 0.0f);
	glColor3f(0.933f * gFadeColorValue, 0.910f * gFadeColorValue, 0.667f * gFadeColorValue);
	glVertex3f(-0.3f, -0.32f, 0.0f);
	glVertex3f(0.3f, -0.32f, 0.0f);
	glEnd();

	RightKaranda();
	LeftKaranda();

	glColor3f(0.502f * gFadeColorValue, 0.0f * gFadeColorValue, 0.0f * gFadeColorValue);
	glBegin(GL_TRIANGLE_FAN);
	for (GLfloat fSupariAngle = 1.0f; fSupariAngle <= 360.0f; fSupariAngle += 0.1f)
	{
		glVertex3f((sin(fSupariAngle) * 0.09f) + 1.0f, (cos(fSupariAngle) * 0.09f) - 0.32f, 0.0f);
	}
	glEnd();

	glColor3f(0.961f * gFadeColorValue, 1.0f * gFadeColorValue, 0.980f * gFadeColorValue);
	glBegin(GL_TRIANGLE_FAN);
	for (GLfloat fPedhaAAngle = 1.0f; fPedhaAAngle <= 360.0f; fPedhaAAngle += 0.1f)
	{
		glVertex3f((sin(fPedhaAAngle) * 0.12f) + 0.7f, (cos(fPedhaAAngle) * 0.12f) - 0.32f, 0.00f);
	}
	glEnd();

	glColor3f(0.961f * gFadeColorValue, 1.0f * gFadeColorValue, 0.980f * gFadeColorValue);
	glBegin(GL_TRIANGLE_FAN);
	for (GLfloat fPedhaBAngle = 1.0f; fPedhaBAngle <= 360.0f; fPedhaBAngle += 0.1f)
	{
		glVertex3f((sin(fPedhaBAngle) * 0.10f) + 0.5f, (cos(fPedhaBAngle) * 0.10f) - 0.32f, 0.00f);
	}
	glEnd();
	// draw plate
	glBegin(GL_QUADS);
	glColor3f(0.663f * gFadeColorValue, 0.663f * gFadeColorValue, 0.663f * gFadeColorValue);
	glVertex3f(1.4f, -0.32f, 0.0f);
	glVertex3f(-1.4f, -0.32f, 0.0f);
	glVertex3f(-1.2f, -0.42f, 0.0f);
	glVertex3f(1.2f, -0.42f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.753f * gFadeColorValue, 0.753f * gFadeColorValue, 0.753f * gFadeColorValue);
	glVertex3f(1.2f, -0.42f, 0.0f);
	glVertex3f(-1.2f, -0.42f, 0.0f);
	glVertex3f(-1.2f, -0.52f, 0.0f);
	glVertex3f(1.2f, -0.52f, 0.0f);
	glEnd();
}

void RightKaranda()
{
	glBegin(GL_TRIANGLES);

	glColor3f(1.0f * gFadeColorValue, 0.0f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(-0.5f, 0.23f, 0.0f);
	glVertex3f(-0.38f, 0.13f, 0.0f);
	glVertex3f(-0.62f, 0.13f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	//Top Part of Quad
	glColor3f(1.0f * gFadeColorValue, 1.0f * gFadeColorValue, 1.0f * gFadeColorValue);
	glVertex3f(-0.33f, 0.1f, 0.0f);
	glVertex3f(-0.67f, 0.1f, 0.0f);
	glVertex3f(-0.62f, 0.13f, 0.0f);
	glVertex3f(-0.38f, 0.13f, 0.0f);

	//Middle Part Quad
	glColor3f(0.9f * gFadeColorValue, 0.9f * gFadeColorValue, 0.9f * gFadeColorValue);
	glVertex3f(-0.33f, 0.1f, 0.0f);
	glVertex3f(-0.67f, 0.1f, 0.0f);
	glVertex3f(-0.67f, -0.1f, 0.0f);
	glVertex3f(-0.33f, -0.1f, 0.0f);

	//Bottom Part Quads
	glColor3f(1.0f * gFadeColorValue, 1.0f * gFadeColorValue, 1.0f * gFadeColorValue);
	glVertex3f(-0.33f, -0.1f, 0.0f);
	glVertex3f(-0.67f, -0.1f, 0.0f);
	glVertex3f(-0.62f, -0.13f, 0.0f);
	glVertex3f(-0.38f, -0.13f, 0.0f);

	glColor3f(0.9f * gFadeColorValue, 0.9f * gFadeColorValue, 0.9f * gFadeColorValue);
	glVertex3f(-0.45f, -0.13f, 0.0f);
	glVertex3f(-0.55f, -0.13f, 0.0f);
	glVertex3f(-0.55f, -0.22f, 0.0f);
	glVertex3f(-0.45f, -0.22f, 0.0f);

	glColor3f(0.9f * gFadeColorValue, 0.9f * gFadeColorValue, 0.9f * gFadeColorValue);
	glVertex3f(-0.44f, -0.22f, 0.0f);
	glVertex3f(-0.56f, -0.22f, 0.0f);
	glVertex3f(-0.70f, -0.46f, 0.0f);
	glVertex3f(-0.30f, -0.46f, 0.0f);
	glEnd();
}

void LeftKaranda()
{

	glBegin(GL_TRIANGLES);

	glColor3f(1.0f * gFadeColorValue, 1.0f * gFadeColorValue, 0.0f * gFadeColorValue);
	glVertex3f(-1.0f, 0.23f, 0.0f);
	glVertex3f(-0.88f, 0.13f, 0.0f);
	glVertex3f(-1.12f, 0.13f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	//Top Part of Quad
	glColor3f(1.0f * gFadeColorValue, 1.0f * gFadeColorValue, 1.0f * gFadeColorValue);
	glVertex3f(-0.83f, 0.1f, 0.0f);
	glVertex3f(-1.17f, 0.1f, 0.0f);
	glVertex3f(-1.12f, 0.13f, 0.0f);
	glVertex3f(-0.88f, 0.13f, 0.0f);

	//Middle Part Quad
	glColor3f(0.9f * gFadeColorValue, 0.9f * gFadeColorValue, 0.9f * gFadeColorValue);
	glVertex3f(-0.83f, 0.1f, 0.0f);
	glVertex3f(-1.17f, 0.1f, 0.0f);
	glVertex3f(-1.17f, -0.1f, 0.0f);
	glVertex3f(-0.83f, -0.1f, 0.0f);

	//Bottom Part Quads
	glColor3f(1.0f * gFadeColorValue, 1.0f * gFadeColorValue, 1.0f * gFadeColorValue);
	glVertex3f(-0.83f, -0.1f, 0.0f);
	glVertex3f(-1.17f, -0.1f, 0.0f);
	glVertex3f(-1.12f, -0.13f, 0.0f);
	glVertex3f(-0.88f, -0.13f, 0.0f);

	glColor3f(0.9f * gFadeColorValue, 0.9f * gFadeColorValue, 0.9f * gFadeColorValue);
	glVertex3f(-0.95f, -0.13f, 0.0f);
	glVertex3f(-1.05f, -0.13f, 0.0f);
	glVertex3f(-1.05f, -0.22f, 0.0f);
	glVertex3f(-0.95f, -0.22f, 0.0f);

	glColor3f(0.9f * gFadeColorValue, 0.9f * gFadeColorValue, 0.9f * gFadeColorValue);
	glVertex3f(-0.94f, -0.22f, 0.0f);
	glVertex3f(-1.06f, -0.22f, 0.0f);
	glVertex3f(-1.2f, -0.46f, 0.0f);
	glVertex3f(-0.80f, -0.46f, 0.0f);
	glEnd();
}

void DrawL2()
{
	void DrawLeftLine1(GLfloat, GLfloat);
	void DrawBottomLine(GLfloat, GLfloat);

	DrawLeftLine1(-0.8f, 0.8f);
	DrawBottomLine(0.8f, -0.8f);
}

void DrawI()
{

	void DrawRightLine1(GLfloat, GLfloat);
	DrawRightLine1(0.8f, 0.8f);
}

void DrawO()
{
	void DrawLeftLine(GLfloat, GLfloat);
	void DrawBottomLine(GLfloat, GLfloat);
	void DrawTopLine(GLfloat, GLfloat);
	void DrawRightLine(GLfloat, GLfloat);

	DrawLeftLine(-0.8f, 0.8f);
	DrawBottomLine(0.8f, -0.8f);
	DrawTopLine(0.8f, 0.8f);
	DrawRightLine(0.8f, 0.8f);
}

void DrawP()
{
	void DrawVerticle(GLfloat, GLfloat);
	DrawVerticle(-0.8f, 0.8f);
	//top
	glBegin(GL_QUADS);
	glVertex3f((0.8f - 0.3f), 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.4f, (0.8f - 0.5f), 0.0f);
	glVertex3f((0.8f - 0.5f), (0.8f - 0.5f), 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f((0.8f - 0.3f), 0.8f, 0.0f); //1
	glVertex3f(0.3f, (0.8f - 0.5f), 0.0f); //up quad 4
	glVertex3f(0.8f, (0.8f - 0.3f), 0.0f);
	glEnd();

	//right
	glBegin(GL_QUADS);
	glVertex3f(0.8f, (0.8f - 0.3f), 0.0f);
	glVertex3f(0.3f, (0.8f - 0.5f), 0.0f); //4
	glVertex3f(0.3f, (0.8f - 0.7f), 0.0f); //4
	glVertex3f((0.8f), -0.1f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f((0.8f), -0.1f, 0.0f);	   //1
	glVertex3f(0.3f, (0.8f - 0.7f), 0.0f); //up quad 4
	glVertex3f(0.5f, -0.4f, 0.0f);
	glEnd();

	//Bottom
	glBegin(GL_QUADS);
	glVertex3f(0.3f, (0.8f - 0.7f), 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(-0.4f, -0.4f, 0.0f);
	glVertex3f(0.5f, -0.4f, 0.0f);
	glEnd();
}

void DrawY()
{
	//left
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.3f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.25f, 0.0f);
	glVertex3f(0.0f, -0.25f, 0.0f);
	glEnd();

	//right
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.3f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(0.8f, 0.25f, 0.0f);
	glVertex3f(0.0f, -0.25f, 0.0f);
	glEnd();

	//leftBottom
	glBegin(GL_QUADS);
	glVertex3f(0.0f, -0.25f, 0.0f);
	glVertex3f(-0.25f, -0.0f, 0.0f);
	glVertex3f(-0.25f, -0.8f, 0.0f);
	glVertex3f(0.0f, -0.65, 0.0f);
	glEnd();

	//rightBottom
	glBegin(GL_QUADS);
	glVertex3f(0.25f, -0.05f, 0.0f);
	glVertex3f(0.0f, -0.25f, 0.0f);
	glVertex3f(0.0f, -0.65f, 0.0f);
	glVertex3f(0.25f, -0.5f, 0.0f);

	glEnd();
}

void DrawG()
{
	void DrawLeftLine(GLfloat, GLfloat);
	void DrawBottomLine(GLfloat, GLfloat);
	void DrawTopLine(GLfloat, GLfloat);
	void DrawGRightLine(GLfloat, GLfloat);
	DrawLeftLine(-0.8f, 0.8f);
	DrawBottomLine(0.8f, -0.8f);
	DrawTopLine(0.8f, 0.8f);
	DrawGRightLine(0.8f, -0.8f);

	//Inner line
	glBegin(GL_QUADS);

	glVertex3f(0.4f, 0.15f, 0.0f);
	glVertex3f(0.05f, 0.15f, 0.0f);
	glVertex3f(-0.25f, -0.15f, 0.0f);
	glVertex3f(0.4f, -0.15f, 0.0f);
	glEnd();
}

void DrawC()
{
	void DrawLeftLine(GLfloat, GLfloat);
	void DrawBottomLine(GLfloat, GLfloat);
	void DrawTopLine(GLfloat, GLfloat);
	DrawLeftLine(-0.8f, 0.8f);
	DrawBottomLine(0.8f, -0.8f);
	DrawTopLine(0.8f, 0.8f);
}

void DrawN()
{
	void DrawLeftLine1(GLfloat, GLfloat);
	void DrawRightLine1(GLfloat, GLfloat);
	void DrawNCross(GLfloat, GLfloat);
	DrawLeftLine1(-0.8f, 0.8f);
	DrawRightLine1(0.8f, 0.8f);
	DrawNCross(-0.8f, 0.8f);
}

void DrawU()
{
	void DrawLeftLine1(GLfloat, GLfloat);
	void DrawBottomLine(GLfloat, GLfloat);
	void DrawRightLine1(GLfloat, GLfloat);
	DrawLeftLine1(-0.8f, 0.8f);
	DrawBottomLine(0.8f, -0.8f);
	DrawRightLine1(0.8f, 0.8f);
}

void DrawLeftLine(GLfloat leftX, GLfloat topY)
{
	glBegin(GL_QUADS);

	glVertex3f((leftX + 0.4f), (topY - 0.5), 0.0f);
	glVertex3f(leftX, topY, 0.0f);
	glVertex3f(leftX, -topY, 0.0f);
	glVertex3f((leftX + 0.4f), (-topY + 0.5), 0.0f);
	glEnd();
}

void DrawRightLine(GLfloat rightX, GLfloat topY)
{
	glBegin(GL_QUADS);

	glVertex3f(rightX, topY, 0.0f);
	glVertex3f((rightX - 0.4f), (topY - 0.5), 0.0f);
	glVertex3f((rightX - 0.4), (-topY + 0.5), 0.0f);
	glVertex3f(rightX, -topY, 0.0f);
	glEnd();
}

void DrawGRightLine(GLfloat rightX, GLfloat bottomY)
{
	glBegin(GL_QUADS);
	glVertex3f(rightX, (-bottomY - 0.65f), 0.0f);
	glVertex3f((rightX - 0.4f), (-bottomY - 0.65f), 0.0f);
	glVertex3f((rightX - 0.4f), (bottomY + 0.5f), 0.0f);
	glVertex3f(rightX, bottomY, 0.0f);
	glEnd();
}

void DrawLeftLine1(GLfloat leftX, GLfloat topY)
{
	glBegin(GL_QUADS);

	glVertex3f((leftX + 0.4f), (topY - 0.4), 0.0f);
	glVertex3f(leftX, topY, 0.0f);
	glVertex3f(leftX, -topY, 0.0f);
	glVertex3f((leftX + 0.4f), (-topY + 0.4), 0.0f);
	glEnd();
}

void DrawRightLine1(GLfloat rightX, GLfloat topY)
{
	glBegin(GL_QUADS);

	glVertex3f(rightX, topY, 0.0f);
	glVertex3f((rightX - 0.4f), (topY - 0.4), 0.0f);
	glVertex3f((rightX - 0.4), (-topY + 0.4), 0.0f);
	glVertex3f(rightX, -topY, 0.0f);
	glEnd();
}

void DrawBottomLine(GLfloat rightX, GLfloat bottomY)
{
	glBegin(GL_QUADS);

	glVertex3f((rightX - 0.4f), (bottomY + 0.5), 0.0f);
	glVertex3f((-rightX + 0.4f), (bottomY + 0.5), 0.0f);
	glVertex3f(-rightX, bottomY, 0.0f);
	glVertex3f(rightX, bottomY, 0.0f);
	glEnd();
}

void DrawTopLine(GLfloat rightX, GLfloat TopY)
{
	glBegin(GL_QUADS);

	glVertex3f(rightX, TopY, 0.0f);
	glVertex3f(-rightX, TopY, 0.0f);
	glVertex3f((-rightX + 0.4), (TopY - 0.5), 0.0f);
	glVertex3f((rightX - 0.4), (TopY - 0.5), 0.0f);
	glEnd();
}

void DrawBottomLine1(GLfloat rightX, GLfloat bottomY)
{
	glBegin(GL_QUADS);

	glVertex3f((rightX - 0.4f), (bottomY + 0.4), 0.0f);
	glVertex3f((-rightX + 0.4f), (bottomY + 0.4), 0.0f);
	glVertex3f(-rightX, bottomY, 0.0f);
	glVertex3f(rightX, bottomY, 0.0f);
	glEnd();
}

void DrawTopLine1(GLfloat rightX, GLfloat TopY)
{
	glBegin(GL_QUADS);

	glVertex3f(rightX, TopY, 0.0f);
	glVertex3f(-rightX, TopY, 0.0f);
	glVertex3f((-rightX + 0.4), (TopY - 0.4), 0.0f);
	glVertex3f((rightX - 0.4), (TopY - 0.4), 0.0f);
	glEnd();
}

void DrawVerticle(GLfloat rightX, GLfloat TopY)
{
	glBegin(GL_QUADS);

	glVertex3f((rightX + 0.4f), TopY, 0.0f);
	glVertex3f(rightX, TopY, 0.0f);
	glVertex3f(rightX, -TopY, 0.0f);
	glVertex3f((rightX + 0.4f), -TopY, 0.0f);
	glEnd();
}

void DrawNCross(GLfloat leftX, GLfloat leftTopY)
{
	glBegin(GL_QUADS);

	glVertex3f(leftX, leftTopY, 0.0f);
	glVertex3f(leftX, (leftTopY - 0.5), 0.0f);
	glVertex3f(-leftX, (-leftTopY), 0.0f);
	glVertex3f(-leftX, (-leftTopY + 0.5), 0.0f);

	glEnd();
}

void DrawE()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.33f, 0.0f);
	glVertex3f(0.6f, 0.33f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.5f, 0.15f, 0.0f);
	glVertex3f(-0.4f, 0.15f, 0.0f);
	glVertex3f(-0.4f, -0.15f, 0.0f);
	glVertex3f(0.3f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.6f, -0.33f, 0.0f);
	glVertex3f(-0.4f, -0.33f, 0.0f);
	glVertex3f(-0.4f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(-0.4f, -0.8f, 0.0f);
	glEnd();
}

void DrawT()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glVertex3f(0.6f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.2f, 0.3f, 0.0f);
	glVertex3f(-0.2f, 0.3f, 0.0f);
	glVertex3f(-0.2f, -0.6f, 0.0f);
	glVertex3f(0.2f, -0.8f, 0.0f);
	glEnd();
}

void DrawR()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	/// culling starts ///
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);
	glVertex3f(-0.4f, -0.3f, 0.0f);
	glVertex3f(-0.4f, -0.8f, 0.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.0f, 0.0f);
	glVertex3f(0.5f, -0.3f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(0.5f, 0.8f, 0.0f);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glEnd();
	/// culling Ends ///
}

void DrawS()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	/// culling starts ///
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f); //a
	glVertex3f(0.6f, 0.3f, 0.0f); //b
	glVertex3f(0.8f, 0.3f, 0.0f); //c
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.3f, 0.0f);  //C
	glVertex3f(-0.4f, 0.3f, 0.0f); //H
	glVertex3f(-0.4f, 0.1f, 0.0f); //I
	glVertex3f(0.8f, 0.1f, 0.0f);  //E
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f, -0.4f, 0.0f); //a
	glVertex3f(-0.8f, -0.4f, 0.0f); //b
	glVertex3f(-0.8f, -0.8f, 0.0f); //c
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, -0.2f, 0.0f);	//h
	glVertex3f(-0.8f, -0.2f, 0.0f); //i
	glVertex3f(-0.8f, -0.4f, 0.0f); //j
	glVertex3f(0.4f, -0.4f, 0.0f);	//k
	glEnd();
	/// culling Ends ///
}

void DrawD()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	//culling
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, -0.3f, 0.0f);
	glVertex3f(0.4f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(0.5f, 0.8f, 0.0f);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();
}

void DrawM()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.3f, 0.0f);
	glVertex3f(0.8f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.4f, 0.3f, 0.0f);
	glVertex3f(-0.8f, 0.3f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.2f, 0.3f, 0.0f);
	glVertex3f(-0.2f, -0.8f, 0.0f);
	glVertex3f(0.2f, -0.6f, 0.0f);
	glVertex3f(0.2f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.4f, 0.3f, 0.0f);
	glVertex3f(0.4f, -0.6f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, 0.3f, 0.0f);
	glEnd();

	//culling
	glBegin(GL_TRIANGLES);
	glVertex3f(0.2f, 0.8f, 0.0f);
	glVertex3f(-0.2f, 0.8f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glEnd();
}

void DrawA()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	/// culling starts ///
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, -0.3f, 0.0f);	//i
	glVertex3f(-0.4f, -0.3f, 0.0f); //a
	glVertex3f(-0.4f, -0.6f, 0.0f); //d
	glVertex3f(0.4f, -0.6f, 0.0f);	//g
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, -0.6f, 0.0f);	//g
	glVertex3f(-0.4f, -0.6f, 0.0f); //d
	glVertex3f(-0.8f, -0.8f, 0.0f); //c
	glVertex3f(0.8f, -0.8f, 0.0f);	//h
	glEnd();
	/// culling ends ///
}

void DrawF(void)
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.3f, 0.0f);
	glVertex3f(0.5f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(-0.4f, -0.3f, 0.0f);
	glVertex3f(0.2f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();
}

void DrawAstrology(GLfloat xTranslation, GLfloat yTranslation, GLfloat greenColor, GLfloat fadeoutAMCGreen)
{
	void DrawO();
	void DrawL2();
	void DrawG();
	void DrawY();
	void DrawT();
	void DrawR();
	void DrawS();
	void DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xTranslation, yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 2.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 2.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawL2();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 4.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 4.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 6.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawS();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 6.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawG();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 8.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 8.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawY();
}

void DrawMedicine(GLfloat zTranslation, GLfloat greenColor, GLfloat fadeoutAMCGreen)
{
	static GLfloat fadeoutAMCGreenD_PSM = 0.0f;
	static GLfloat fadeoutAMCRedD_PSM = 0.0f;
	static GLfloat fadeoutAMCBlueD_PSM = 0.0f;
	if ((currentAnimationObject == FIRST_3_WORDS) || (currentAnimationObject == FORM_ASTROMEDICOMP))
	{
		fadeoutAMCGreenD_PSM = fadeoutAMCGreen;
	}
	else if (currentAnimationObject == KNOWLEDGE_IS_INTERRELATED)
	{
		if (fadeoutAMCGreenD_PSM <= 0.8f)
		{
			fadeoutAMCGreenD_PSM += 0.0007f;
		}
		
		if (fadeoutAMCRedD_PSM <= 0.8f)
		{
			fadeoutAMCRedD_PSM += 0.003f;
		}
		
		if (fadeoutAMCBlueD_PSM <= 0.8f)
		{
			fadeoutAMCBlueD_PSM += 0.003f;
		}
	} else if (currentAnimationObject == FADEOUT_ASTROMEDICOMP) {
		fadeoutAMCGreenD_PSM -= 0.0011f;
		fadeoutAMCRedD_PSM -= 0.0011f;
		fadeoutAMCBlueD_PSM -= 0.0011f;
	}
	void DrawM();
	void DrawE();
	void DrawD();
	void DrawI();
	void DrawC();
	void DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.0f, 0.0f, (zTranslation - 25.0f));
	glColor3f(fadeoutAMCRedD_PSM, fadeoutAMCGreenD_PSM, fadeoutAMCBlueD_PSM);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 0.0f, (zTranslation - 25.0f));
	glColor3f(0.0f, greenColor, 0.0f);
	DrawC();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.75f, 0.0f, (zTranslation - 25.0f));
	glColor3f(fadeoutAMCRedD_PSM, fadeoutAMCGreenD_PSM, fadeoutAMCBlueD_PSM);
	DrawD();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.75f, 0.0f, (zTranslation - 25.0f));
	glColor3f(0.0f, greenColor, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-3.75f, 0.0f, (zTranslation - 25.0f));
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.75f, 0.0f, (zTranslation - 25.0f));
	glColor3f(0.0f, greenColor, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-5.75f, 0.0f, (zTranslation - 25.0f));
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.75f, 0.0f, (zTranslation - 25.0f));
	glColor3f(0.0f, greenColor, 0.0f);
	DrawE();
}

void DrawComputer(GLfloat xTranslation, GLfloat yTranslation, GLfloat greenColor, GLfloat fadeoutAMCGreen)
{
	void DrawC();
	void DrawO();
	void DrawM();
	void DrawP();
	void DrawU();
	void DrawT();
	void DrawE();
	void DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 1.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawP();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 1.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawU();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 3.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 3.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 5.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 5.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation - 7.0f), yTranslation, -25.0f);
	glColor3f(0.0f, fadeoutAMCGreen, 0.0f);
	DrawC();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef((xTranslation + 7.0f), yTranslation, -25.0f);
	glColor3f(0.0f, greenColor, 0.0f);
	DrawR();
}

void TranslateAstrology()
{
	//Function declaration
	void DrawAstrology(GLfloat, GLfloat, GLfloat, GLfloat);
	void KnowledgeIsInterRelated(GLfloat, GLfloat);
	void DrawAstroMediCompLogo(GLfloat, GLfloat);
	//Variables
	GLfloat xTranslationEnd_ppg = -7.75f, xTranslationStart_ppg = -40.0f;
	static GLfloat translateX_ppg = xTranslationStart_ppg;
	//Color
	GLfloat greenColorStart = GREEN_COLOR, greenColorEnd = 0.0f, KnowledgeGreenEnd = 0.40f , logoZStart = -40, logoZEnd = -4;
	static GLfloat shadeGreen = greenColorStart, fadeoutGreen = greenColorStart, knowldgeGreen = greenColorEnd, logoTranslate = logoZStart;

	//Y translation with Fadout
	GLfloat yTranslationEnd_ppg = 0.0f, yTranslationStart_ppg = 2.0f;
	static GLfloat translateY_ppg = yTranslationStart_ppg;

	//Code
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawAstrology(translateX_ppg, translateY_ppg, shadeGreen, fadeoutGreen);

	if (currentAnimationObject == FIRST_3_WORDS)
	{
		if (translateX_ppg <= xTranslationEnd_ppg)
		{
			translateX_ppg -= (xTranslationStart_ppg - xTranslationEnd_ppg) / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else
		{
			currentAnimationObject = FORM_ASTROMEDICOMP;
		}
	}
	else if (currentAnimationObject == FORM_ASTROMEDICOMP)
	{
		if (shadeGreen >= greenColorEnd)
		{
			shadeGreen -= greenColorStart / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (translateY_ppg >= yTranslationEnd_ppg)
		{
			translateY_ppg -= yTranslationStart_ppg / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else
		{
			currentAnimationObject = KNOWLEDGE_IS_INTERRELATED;
		}
	}
	else if (currentAnimationObject == KNOWLEDGE_IS_INTERRELATED) {
		KnowledgeIsInterRelated(-4.0f, knowldgeGreen);
		DrawAstroMediCompLogo(1.8f, logoTranslate);
		if (knowldgeGreen <= KnowledgeGreenEnd) {
			knowldgeGreen += KnowledgeGreenEnd / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (logoTranslate <= logoZEnd) {
			logoTranslate -= (logoZStart-logoZEnd) / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		else {
			currentAnimationObject = FADEOUT_ASTROMEDICOMP;
		}
	}
	else if (currentAnimationObject == FADEOUT_ASTROMEDICOMP) {
		KnowledgeIsInterRelated(-4.0f, knowldgeGreen);
		DrawAstroMediCompLogo(1.8f, logoTranslate);
		if (knowldgeGreen >= greenColorEnd) {
			knowldgeGreen = knowldgeGreen - (KnowledgeGreenEnd / ASTROMEDICOMP_ANIMATION_STEPS);
		}
		if (fadeoutGreen >= greenColorEnd)
		{
			fadeoutGreen = fadeoutGreen - (GREEN_COLOR / ASTROMEDICOMP_ANIMATION_STEPS);
		}
		else {
			gbIntro2Complete_PSM = true;
		}
	}
}

void TranslateComputer()
{
	//Function declaration
	void DrawComputer(GLfloat, GLfloat, GLfloat, GLfloat);
	//Variables
	GLfloat xTranslationEnd_ppg = 8.0f, xTranslationStart_ppg = 40.0f;
	static GLfloat translateX_ppg = xTranslationStart_ppg;

	//Color Fade out
	GLfloat greenColorStart = GREEN_COLOR, greenColorEnd = 0.0f;
	static GLfloat shadeGreen = greenColorStart, fadeoutGreen = greenColorStart;

	//Y-axis translation with fadeout
	//Y translation with Fadout
	GLfloat yTranslationEnd_ppg = 0.0f, yTranslationStart_ppg = -2.0f;
	static GLfloat translateY_ppg = yTranslationStart_ppg;

	//Code
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawComputer(translateX_ppg, translateY_ppg, shadeGreen, fadeoutGreen);

	if (currentAnimationObject == FIRST_3_WORDS)
	{
		if (translateX_ppg >= xTranslationEnd_ppg)
		{
			translateX_ppg -= (xTranslationStart_ppg - xTranslationEnd_ppg) / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else
		{
			currentAnimationObject = FORM_ASTROMEDICOMP;
		}
	}
	else if (currentAnimationObject == FORM_ASTROMEDICOMP)
	{

		if (shadeGreen >= greenColorEnd)
		{
			shadeGreen -= greenColorStart / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (translateY_ppg <= yTranslationEnd_ppg)
		{
			translateY_ppg += -(yTranslationStart_ppg / ASTROMEDICOMP_ANIMATION_STEPS);
		}
		else {
			currentAnimationObject = KNOWLEDGE_IS_INTERRELATED;
		}
	}
	else if (currentAnimationObject == FADEOUT_ASTROMEDICOMP) {

		if (fadeoutGreen >= greenColorEnd)
		{

			fadeoutGreen -= GREEN_COLOR / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else {
			gbIntro2Complete_PSM = true;
		}
	}
}

void TranslateMedicine()
{
	//Function declaration
	void DrawMedicine(GLfloat, GLfloat, GLfloat);
	//Variables
	GLfloat zTranslationEnd_ppg = 0.0f, zTranslationStart_ppg = -65.0f;
	static GLfloat translateZ_ppg = zTranslationStart_ppg;

	//color
	GLfloat greenColorStart = GREEN_COLOR, greenColorEnd = 0.0f;
	static GLfloat shadeGreen = greenColorStart, fadeoutGreen = greenColorStart;
	//Code
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawMedicine(translateZ_ppg, shadeGreen, fadeoutGreen);

	if (currentAnimationObject == FIRST_3_WORDS)
	{
		if (translateZ_ppg <= zTranslationEnd_ppg)
		{
			translateZ_ppg -= (zTranslationStart_ppg - zTranslationEnd_ppg) / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else
		{
			currentAnimationObject = FORM_ASTROMEDICOMP;
		}
	}
	else if (currentAnimationObject == FORM_ASTROMEDICOMP)
	{

		if (shadeGreen >= greenColorEnd)
		{
			shadeGreen -= greenColorStart / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else {
			currentAnimationObject = KNOWLEDGE_IS_INTERRELATED;
		}
	}
	else if (currentAnimationObject == FADEOUT_ASTROMEDICOMP) {
		if (fadeoutGreen >= greenColorEnd)
		{
			fadeoutGreen -= GREEN_COLOR / ASTROMEDICOMP_ANIMATION_STEPS;
		}
		else {
			gbIntro2Complete_PSM = true;
		}
	}
}

void AnimateAstromedicomp()
{
	TranslateAstrology();
	TranslateMedicine();
	TranslateComputer();
}

void DrawOutro_JSS()
{
	//Local Color function
	static GLfloat colorTranformation_JSS[4] = {0, 0, 0, 0};
	static GLfloat colorTransformation_Chirayu = 0.0f;
	static bool NameTranfomationComplete_Sir = false;
	static bool NameTranfomationComplete_Chirau = false;
	//Local function declaration
	void DrWord(void);
	void VijayWord();
	void GokhaleWord();
	void circleDraw();
	void Sir();
	void Chirayu();
	void Bhav();

	//code

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-3.0f - translateChange_X, 0.0f, -9.0f);
	glColor3f(colorTranformation_JSS[0], 0.0f, 0.0f);
	DrWord();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f - translateChange_X, 0.0f, -9.0f);
	glColor3f(colorTranformation_JSS[1], 0.0f, 0.0f);
	VijayWord();
	glColor3f(colorTranformation_JSS[2], 0.0f, 0.0f);
	GokhaleWord();
	glColor3f(colorTranformation_JSS[3], 0.0f, 0.0f);
	Sir();
	//circleDraw();1
	if (colorTranformation_JSS[0] <= 1.0f && !NameTranfomationComplete_Sir)
	{
		colorTranformation_JSS[0] = colorTranformation_JSS[0] + 0.002;
	}
	else if (colorTranformation_JSS[1] <= 1.0f && !NameTranfomationComplete_Sir)
	{
		colorTranformation_JSS[1] = colorTranformation_JSS[1] + 0.002;
	}
	else if (colorTranformation_JSS[2] <= 1.0f && !NameTranfomationComplete_Sir)
	{
		colorTranformation_JSS[2] = colorTranformation_JSS[2] + 0.002;
	}
	else if (colorTranformation_JSS[3] <= 1.0f && !NameTranfomationComplete_Sir)
	{
		colorTranformation_JSS[3] = colorTranformation_JSS[3] + 0.002;
	}
	else if (colorTranformation_JSS[4] <= 1.0f && !NameTranfomationComplete_Sir)
	{
		colorTranformation_JSS[4] = colorTranformation_JSS[4] + 0.002;
	}
	else
	{

		NameTranfomationComplete_Sir = true;
		if (colorTranformation_JSS[0] > 0.0f)
		{
			colorTranformation_JSS[0] = colorTranformation_JSS[0] - 0.01;
			colorTranformation_JSS[1] = colorTranformation_JSS[1] - 0.01;
			colorTranformation_JSS[2] = colorTranformation_JSS[2] - 0.01;
			colorTranformation_JSS[3] = colorTranformation_JSS[3] - 0.01;
			colorTranformation_JSS[4] = colorTranformation_JSS[4] - 0.01;
		}
		if (colorTranformation_JSS[0] <= 0.0f)
		{
			translateChange_X = 0.0f;
			glColor3f(colorTransformation_Chirayu, 0.0f, 0.0f);
			Chirayu();
			Bhav();
			if (colorTransformation_Chirayu <= 2.0f && !NameTranfomationComplete_Chirau)
			{
				colorTransformation_Chirayu = colorTransformation_Chirayu + 0.01;
			}
			else
			{
				NameTranfomationComplete_Chirau = true;
				colorTransformation_Chirayu = colorTransformation_Chirayu - 0.02;
				if (colorTransformation_Chirayu < 0.0f)
				{
					colorTransformation_Chirayu = 0.0f;
					gbOutroComplete_PSM = true;
				}
			}
		}
	}
}
void DrWord()
{
	glBegin(GL_QUADS);
	/*glColor3f(1.0f , 0.0f , 0.0f);
        glVertex3f(0.6f , 1.4f ,0.0f);
        glVertex3f(0.4f , 1.3f , 0.0f);
        glVertex3f( 0.4f , 1.2f , 0.0f);
        glVertex3f( 0.65f , 1.3f , 0.0f);

        glVertex3f(0.4f , 1.3f , 0.0f);
        glVertex3f(-0.4f , 1.3f , 0.0f);
        glVertex3f(-0.4f , 1.2f , 0.0f);
        glVertex3f( 0.4f , 1.2f , 0.0f);
        
        glVertex3f(-0.4f , 1.3f , 0.0f);
        glVertex3f(-0.6f , 1.4f , 0.0f);
        glVertex3f(-0.65f , 1.3f , 0.0f);
        glVertex3f(-0.4f , 1.2f , 0.0f);*/

	glVertex3f(0.6f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-0.4f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-0.3f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.6f, 1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.5f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.05, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.00, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.00, 0.8f * fontHeightChange, 0.0f);
	glVertex3f(0.05, 0.8f * fontHeightChange, 0.0f);

	/*glVertex3f(0.05f, 0.8f * fontHeightChange, 0.0f);
        glVertex3f(-0.5f, 0.8f * fontHeightChange, 0.0f);
        glVertex3f(-0.5f, 0.75f * fontHeightChange, 0.0f);
        glVertex3f(0.05f, 0.75f * fontHeightChange, 0.0f);

        glVertex3f(-0.45f, 0.75f * fontHeightChange, 0.0f);
        glVertex3f(-0.5f, 0.75f * fontHeightChange, 0.0f);
        glVertex3f(-0.5f, -0.1f * fontHeightChange, 0.0f);
        glVertex3f(-0.45f, -0.1f * fontHeightChange, 0.0f);

        glVertex3f( 0.05f, -0.05f * fontHeightChange, 0.0f);
        glVertex3f(-0.45f, -0.05f * fontHeightChange, 0.0f);
        glVertex3f(-0.45f, -0.1f * fontHeightChange, 0.0f);
        glVertex3f(0.05f, -0.1f * fontHeightChange, 0.0f);

        glVertex3f( 0.05f, -0.1f * fontHeightChange, 0.0f);
        glVertex3f(0.00f, -0.1f * fontHeightChange, 0.0f);
        glVertex3f(0.00f, -1.0f * fontHeightChange, 0.0f);
        glVertex3f( 0.05f, -1.0f * fontHeightChange, 0.0f);

        glVertex3f(0.0f, -0.95f * fontHeightChange, 0.0f);
        glVertex3f(-0.5f, -0.95f * fontHeightChange, 0.0f);
        glVertex3f(-0.5f, -1.0f * fontHeightChange, 0.0f);
        glVertex3f(0.0f, -1.0f * fontHeightChange, 0.0f);*/
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.96f - translateChange_X, 0.2f, -9.0f);
	circleDraw(1.0f / 2.0f, 0.25, 0.20, 3.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.97f - translateChange_X, -0.25f, -9.0f);
	circleDraw(0.0f, 0.25, 0.20, 1.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.97f - translateChange_X, -0.25f, -9.0f);
	circleDraw(7.0 / 6.0f, 0.25, 0.20, 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.8f - translateChange_X, 1.2f, -9.0f);
	circleDraw(7.0f / 6.0f, 0.5, 0.45, 11.0f / 6.0f);
}

void VijayWord()
{

	glBegin(GL_QUADS);
	// Vi

	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(-0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.35f - translateChange_X, 0.0f, -9.0f);
	circleDraw(1.0f / 6.0f, 0.3, 0.25, 11.0f / 6.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f - translateChange_X, 0.5f, -9.0f);
	circleDraw(0, 0.4, 0.35, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.3f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);

	//J

	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.5f * fontWidthChange, 0.05f * fontHeightChange, 0.0f);
	glVertex3f(-0.05f * fontWidthChange, 0.05f * fontHeightChange, 0.0f);
	glVertex3f(-0.05f * fontWidthChange, -0.05f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -0.05f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.65f - translateChange_X, 0.02f, -9.0f);
	circleDraw(5.0f / 6.0f, 0.35, 0.30, 2);

	//y
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.7f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);

	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.35f - translateChange_X, 0.23f, -9.0f);
	circleDraw(5.0f / 3.2f, 0.32, 0.27, 2.0f);
	circleDraw(0, 0.32, 0.27, 1.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.8f - translateChange_X, -0.05, -9.0f);
	circleDraw(1.0f, 0.38, 0.33, 11.0f / 6.2f);
	//circleDraw(0 , 0.35 , 0.30 , 1.0f/2.0f);

	//line
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.3f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-2.2f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-1.9f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(1.3f, 1.0f * fontHeightChange, 0.0f);

	glEnd();
}

void GokhaleWord()
{
	//Go
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.35f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.30f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.30f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.35f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.5f * fontWidthChange, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(0.0f * fontWidthChange, 2.0f * fontHeightChange, 0.0f);
	glVertex3f(0.05f * fontWidthChange, 2.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.9f - translateChange_X, 0.05f, -9.0f);
	circleDraw(3.0f / 2.0f, 0.40, 0.35, 2.0f);
	circleDraw(0, 0.40, 0.35, 1.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.95f - translateChange_X, -0.15f, -9.0f);
	circleDraw(0.0f, 0.20, 0.15, 2.0f);

	//Kha

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.1f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);

	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.82f - translateChange_X, 0.22f, -9.0f);
	circleDraw(3.0f / 2.0f, 0.32, 0.27, 2.0f);
	circleDraw(0, 0.32, 0.27, 1.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.32f - translateChange_X, -0.084f, -9.0f);
	circleDraw(1.0f / 6.0f, 0.20, 0.15, 11.0f / 6.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.2f - translateChange_X, -0.095, -9.0f);
	circleDraw(1.0f, 0.38, 0.33, 11.0f / 6.2f);

	//Le
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.34f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.5f * fontWidthChange, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(0.0f * fontWidthChange, 2.0f * fontHeightChange, 0.0f);
	glVertex3f(0.05f * fontWidthChange, 2.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.95f - translateChange_X, -0.2f, -9.0f);
	circleDraw(0.0f, 0.30, 0.25, 4.0 / 3.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.45f - translateChange_X, -0.2f, -9.0f);
	circleDraw(0.0f, 0.30, 0.25, 1.0f);

	//Line
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.7f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.3f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-2.2f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-1.9f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(1.3f, 1.0f * fontHeightChange, 0.0f);

	glEnd();
}

void Sir()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.9f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.2f * fontWidthChange, -0.9f * fontHeightChange, 0.0f);
	glVertex3f(-0.4f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(0.2f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.45f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(-0.4f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(-0.4f * fontWidthChange, -0.3f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -0.3f * fontHeightChange, 0.0f);
	glEnd();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.5f - translateChange_X, 0.14f, -9.0f);
	circleDraw(3.0f / 2.0f, 0.35, 0.30, 2.0f);
	circleDraw(0, 0.35, 0.30, 1.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(6.9f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.05f * fontWidthChange, -0.9f * fontHeightChange, 0.0f);
	glVertex3f(-0.4f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(0.05f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glEnd();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(6.5f - translateChange_X, 0.14f, -9.0f);
	circleDraw(3.0f / 2.0f, 0.35, 0.30, 2.0f);
	circleDraw(0, 0.35, 0.30, 1.0f / 2.0f);

	//Line
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(6.5f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-1.35f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-1.05f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f, 1.0f * fontHeightChange, 0.0f);

	glEnd();
}

void Chirayu()
{
	//Chi
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.0f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(-0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(-0.05f * fontWidthChange, 0.05f * fontHeightChange, 0.0f);
	glVertex3f(-0.35f * fontWidthChange, 0.05f * fontHeightChange, 0.0f);
	glVertex3f(-0.35f * fontWidthChange, 0.0f * fontHeightChange, 0.0f);
	glVertex3f(-0.05f * fontWidthChange, 0.0f * fontHeightChange, 0.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.0f - translateChange_X, 0.5f, -9.0f);
	circleDraw(0, 0.4, 0.35, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.85f - translateChange_X, 0.02f, -9.0f);
	circleDraw(1, 0.25, 0.20, 2);

	//Ra

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.9f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(0.05f * fontWidthChange, -0.9f * fontHeightChange, 0.0f);
	glVertex3f(-0.4f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(-0.5f * fontWidthChange, -0.4f * fontHeightChange, 0.0f);
	glVertex3f(0.05f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glEnd();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.3f - translateChange_X, 0.14f, -9.0f);
	circleDraw(3.0f / 2.0f, 0.35, 0.30, 2.0f);
	circleDraw(0, 0.35, 0.30, 1.0f / 2.0f);

	//Yu

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.1f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);

	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.16f - translateChange_X, 0.23f, -9.0f);
	circleDraw(3.0f / 2.0f, 0.32, 0.27, 2.0f);
	circleDraw(0, 0.32, 0.27, 1.0f / 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.22f - translateChange_X, -0.05, -9.0f);
	circleDraw(1.0f, 0.38, 0.33, 11.0f / 6.2f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.45f - translateChange_X, -0.57f, -9.0f);
	circleDraw(0, 0.10, 0.05, 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.45f - translateChange_X, -0.8f, -9.0f);
	circleDraw(4.0f / 3.0f, 0.35, 0.30, 2.0f);
	circleDraw(0.0f, 0.35, 0.30, 1.0f / 2.0f);
	//circleDraw(0 , 0.35 , 0.30 , 1.0f/2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.5f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.2f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-2.35f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-2.05f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(1.2f, 1.0f * fontHeightChange, 0.0f);

	glEnd();
}

void Bhav()
{
	//Bha
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glVertex3f(-0.18f * fontWidthChange, 0.7f * fontHeightChange, 0.0f);
	glVertex3f(-0.23f * fontWidthChange, 0.7f * fontHeightChange, 0.0f);
	glVertex3f(-0.23f * fontWidthChange, -0.7f * fontHeightChange, 0.0f);
	glVertex3f(-0.18f * fontWidthChange, -0.7f * fontHeightChange, 0.0f);

	glVertex3f(0.5f * fontWidthChange, -0.35f * fontHeightChange, 0.0f);
	glVertex3f(-0.25f * fontWidthChange, -0.35f * fontHeightChange, 0.0f);
	glVertex3f(-0.25f * fontWidthChange, -0.45f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -0.45f * fontHeightChange, 0.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.3f - translateChange_X, 0.3f, -9.0f);
	circleDraw(0, 0.15, 0.10, 2.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.3f - translateChange_X, -0.3f, -9.0f);
	circleDraw(0, 0.15, 0.10, 2.0f);

	//Va
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f - translateChange_X, 0.0f, -9.0f);

	glBegin(GL_QUADS);

	glVertex3f(0.5f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.45f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.5f * fontWidthChange, -1.0f * fontHeightChange, 0.0f);

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.39f - translateChange_X, 0.0f, -9.0f);
	circleDraw(1.0f / 6.0f, 0.25, 0.20, 11.0f / 6.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f - translateChange_X, 0.0f, -9.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-1.05f, 1.1f * fontHeightChange, 0.0f);
	glVertex3f(-0.95f, 1.0f * fontHeightChange, 0.0f);
	glVertex3f(0.8f, 1.0f * fontHeightChange, 0.0f);

	glEnd();
}

void circleDraw(GLfloat startAngle, GLfloat OuterCircleRadius, GLfloat innerCircleRadius, GLfloat endAngle)
{
	double pi = 2 * acos(0.0);
	startAngle = startAngle * pi;
	glBegin(GL_QUAD_STRIP);

	while (startAngle <= (pi)*endAngle)
	{
		glVertex3f(OuterCircleRadius * cos(startAngle), OuterCircleRadius * sin(startAngle), 0.0f);
		startAngle = startAngle + 0.01f;
		glVertex3f(innerCircleRadius * cos(startAngle), innerCircleRadius * sin(startAngle), 0.0f);
		startAngle = startAngle + 0.01f;
		glVertex3f(OuterCircleRadius * cos(startAngle), OuterCircleRadius * sin(startAngle), 0.0f);
		startAngle = startAngle + 0.01f;
		glVertex3f(innerCircleRadius * cos(startAngle), innerCircleRadius * sin(startAngle), 0.0f);
		startAngle = startAngle + 0.01f;
	}
	glEnd();
}

void LetterK()
{
	glBegin(GL_QUADS);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	//culling
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.24f, 0.0f);
	glVertex3f(-0.2f, 0.24f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.24f, 0.0f);
	glVertex3f(-0.4f, -0.24f, 0.0f);
	glVertex3f(-0.4f, -0.8f, 0.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();
}

void LetterH()
{
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	//culling part
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(0.3f, 0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, -0.2f, 0.0f);
	glVertex3f(-0.3f, -0.2f, 0.0f);
	glVertex3f(-0.3f, -0.6f, 0.0f);
	glVertex3f(0.3f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, -0.6f, 0.0f);
	glVertex3f(-0.3f, -0.6f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();
}

void LetterT()
{
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.6f, 0.3f, 0.0f);
	glVertex3f(0.6f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.2f, 0.3f, 0.0f);
	glVertex3f(-0.2f, 0.3f, 0.0f);
	glVertex3f(-0.2f, -0.6f, 0.0f);
	glVertex3f(0.2f, -0.8f, 0.0f);
	glEnd();
} // T

void LetterA()
{
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	/// culling starts ///
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.3f, 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, -0.3f, 0.0f);	//i
	glVertex3f(-0.4f, -0.3f, 0.0f); //a
	glVertex3f(-0.4f, -0.6f, 0.0f); //d
	glVertex3f(0.4f, -0.6f, 0.0f);	//g
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.4f, -0.6f, 0.0f);	//g
	glVertex3f(-0.4f, -0.6f, 0.0f); //d
	glVertex3f(-0.8f, -0.8f, 0.0f); //c
	glVertex3f(0.8f, -0.8f, 0.0f);	//h
	glEnd();
	/// culling ends ///
} // A

void ThankYou(void)
{
	static GLfloat glMovef = 0.0f, glMvPx = 0.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMovef, 0.0f, -15.0f);
	LetterT(); // -8.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMovef + 2.0, 0.0f, -15.0f);
	LetterH(); // -6.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMovef + 4.0, 0.0f, -15.0f);
	LetterA(); // -4.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMovef + 6.0, 0.0f, -15.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	DrawN(); // -2.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMovef + 8.0, 0.0f, -15.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	LetterK(); // 0.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMvPx, 0.0f, -15.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	DrawY(); // 3.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMvPx + 2.0, 0.0f, -15.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	DrawO(); // 5.0

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(glMvPx + 4.0, 0.0f, -15.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	DrawU(); // 7.0

	if (glMovef >= -8.0f)
	{
		glMovef = glMovef - 0.04f;
	}

	if (glMvPx <= 2.5f)
	{
		glMvPx = glMvPx + 0.02f;
	}

	if (glMovef <= -8.0f && glMvPx >= 2.5f)
	{
		gbThankYouComplete_PSM = true;
	}
}

void MatrixGroup(void)
{
	//Local Variable
	static GLfloat angle;
	static GLfloat Dr_R = 0.0f;
	static GLfloat Dr_G = 0.0f;
	static GLfloat Dr_B = 0.0f;
	static GLfloat Dr_Z = -6.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.50f, Dr_Z);
	glBegin(GL_QUADS);

	// M
	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(-1.80f, 0.30f, 0.0f);  //1
	glVertex3f(-1.95f, 0.30f, 0.0f);  //2
	glVertex3f(-1.95f, -0.20f, 0.0f); //3
	glVertex3f(-1.80f, -0.20f, 0.0f); //4

	glVertex3f(-1.80f, 0.30f, 0.0f);  //1
	glVertex3f(-1.80f, 0.10f, 0.0f);  //5
	glVertex3f(-1.65f, -0.05f, 0.0f); //6
	glVertex3f(-1.65f, 0.15f, 0.0f);  //7

	glVertex3f(-1.65f, -0.05f, 0.0f); //6
	glVertex3f(-1.65f, 0.15f, 0.0f);  //7
	glVertex3f(-1.50f, 0.30f, 0.0f);  //8
	glVertex3f(-1.50f, 0.10f, 0.0f);  //9

	glVertex3f(-1.50f, 0.30f, 0.0f);  //8
	glVertex3f(-1.50f, -0.20f, 0.0f); //10
	glVertex3f(-1.35f, -0.20f, 0.0f); //11
	glVertex3f(-1.35f, 0.30f, 0.0f);  //12

	// A

	glVertex3f(-1.05f, 0.30f, 0.0f); //1
	glVertex3f(-1.08f, 0.20f, 0.0f); //2
	glVertex3f(-0.72f, 0.20f, 0.0f); //3
	glVertex3f(-0.75f, 0.30f, 0.0f); //4

	glVertex3f(-1.08f, 0.20f, 0.0f);  //2
	glVertex3f(-1.20f, -0.20f, 0.0f); //5
	glVertex3f(-1.05f, -0.20f, 0.0f); //6
	glVertex3f(-0.90f, 0.20f, 0.0f);  //9

	glVertex3f(-0.72f, 0.20f, 0.0f);  //3
	glVertex3f(-0.90f, 0.20f, 0.0f);  //9
	glVertex3f(-0.75f, -0.20f, 0.0f); //12
	glVertex3f(-0.60f, -0.20f, 0.0f); //13

	glVertex3f(-0.975f, 0.00f, 0.0f); //7
	glVertex3f(-0.94f, 0.10f, 0.0f);  //8
	glVertex3f(-0.84f, 0.10f, 0.0f);  //10
	glVertex3f(-0.82f, 0.0f, 0.0f);	  //11

	//T

	glVertex3f(0.0f, 0.30f, 0.0f);	 //1
	glVertex3f(-0.45f, 0.30f, 0.0f); //2
	glVertex3f(-0.45f, 0.20f, 0.0f); //3
	glVertex3f(0.0f, 0.20f, 0.0f);	 //4

	glVertex3f(-0.15f, 0.20f, 0.0f);  //5
	glVertex3f(-0.30f, 0.20f, 0.0f);  //6
	glVertex3f(-0.30f, -0.20f, 0.0f); //7
	glVertex3f(-0.15f, -0.20f, 0.0f); //8

	//R

	glVertex3f(0.30f, 0.30f, 0.0f);	 //1
	glVertex3f(0.15f, 0.30f, 0.0f);	 //2
	glVertex3f(0.15f, -0.20f, 0.0f); //3
	glVertex3f(0.30f, -0.20f, 0.0f); //4

	glVertex3f(0.30f, 0.30f, 0.0f); //1
	glVertex3f(0.30f, 0.20f, 0.0f); //7
	glVertex3f(0.60f, 0.20f, 0.0f); //9
	glVertex3f(0.45f, 0.30f, 0.0f); //10

	glVertex3f(0.30f, 0.10f, 0.0f); //6
	glVertex3f(0.30f, 0.0f, 0.0f);	//5
	glVertex3f(0.45f, 0.0f, 0.0f);	//12
	glVertex3f(0.60f, 0.10f, 0.0f); //11

	glVertex3f(0.45f, 0.20f, 0.0f); //8
	glVertex3f(0.60f, 0.20f, 0.0f); //9
	glVertex3f(0.60f, 0.10f, 0.0f); //11
	glVertex3f(0.45f, 0.10f, 0.0f); //13

	glVertex3f(0.30f, 0.0f, 0.0f);	 //5
	glVertex3f(0.45f, 0.0f, 0.0f);	 //12
	glVertex3f(0.60f, -0.20f, 0.0f); //14
	glVertex3f(0.45f, -0.20f, 0.0f); //15

	// I

	glVertex3f(1.20f, 0.30f, 0.0f); //1
	glVertex3f(0.75f, 0.30f, 0.0f); //2
	glVertex3f(0.75f, 0.20f, 0.0f); //3
	glVertex3f(1.20f, 0.20f, 0.0f); //4

	glVertex3f(1.05f, 0.20f, 0.0f);	 //5
	glVertex3f(0.90f, 0.20f, 0.0f);	 //6
	glVertex3f(0.90f, -0.10f, 0.0f); //7
	glVertex3f(1.05f, -0.10f, 0.0f); //8

	glVertex3f(1.20f, -0.10f, 0.0f); //9
	glVertex3f(1.20f, -0.20f, 0.0f); //10
	glVertex3f(0.75f, -0.20f, 0.0f); //11
	glVertex3f(0.75f, -0.10f, 0.0f); //12

	glEnd();

	//X
	glBegin(GL_TRIANGLES);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(1.455f, 0.30f, 0.0f); //1
	glVertex3f(1.35f, 0.30f, 0.0f);	 //2
	glVertex3f(1.35f, 0.23f, 0.0f);	 //3

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(1.455f, 0.30f, 0.0f); //1
	glVertex3f(1.35f, 0.23f, 0.0f);	 //3
	glVertex3f(1.84f, -0.20f, 0.0f); //5
	glVertex3f(1.95f, -0.13f, 0.0f); //7

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(1.84f, -0.20f, 0.0f); //5
	glVertex3f(1.95f, -0.20f, 0.0f); //6
	glVertex3f(1.95f, -0.13f, 0.0f); //7

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(1.95f, 0.30f, 0.0f);	 //8
	glVertex3f(1.845f, 0.30f, 0.0f); //9
	glVertex3f(1.95f, 0.23f, 0.0f);	 //13

	glEnd();

	glBegin(GL_QUADS);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(1.845f, 0.30f, 0.0f);  //9
	glVertex3f(1.35f, -0.13f, 0.0f);  //10
	glVertex3f(1.455f, -0.20f, 0.0f); //12
	glVertex3f(1.95f, 0.23f, 0.0f);	  //13

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(1.35f, -0.13f, 0.0f);  //10
	glVertex3f(1.35f, -0.20f, 0.0f);  //11
	glVertex3f(1.455f, -0.20f, 0.0f); //12

	glEnd();

	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, Dr_Z);
	glScalef(0.50f, 0.50f, 0.50f);

	//G
	glBegin(GL_QUADS);

	glColor3f(Dr_R, Dr_G, Dr_B);

	glVertex3f(-0.90f, -0.40f, 0.0f); //1
	glVertex3f(-0.90f, -0.30f, 0.0f); //2
	glVertex3f(-1.35f, -0.30f, 0.0f); //3
	glVertex3f(-1.35f, -0.40f, 0.0f); //4

	glVertex3f(-1.35f, -0.30f, 0.0f); //3
	glVertex3f(-1.50f, -0.40f, 0.0f); //5
	glVertex3f(-1.50f, -0.70f, 0.0f); //6
	glVertex3f(-1.35f, -0.80f, 0.0f); //7

	glVertex3f(-1.05f, -0.50f, 0.0f); //12
	glVertex3f(-1.05f, -0.60f, 0.0f); //14
	glVertex3f(-1.20f, -0.60f, 0.0f); //15
	glVertex3f(-1.20f, -0.50f, 0.0f); //16

	glVertex3f(-1.35f, -0.80f, 0.0f); //7
	glVertex3f(-1.35f, -0.70f, 0.0f); //8
	glVertex3f(-0.90f, -0.70f, 0.0f); //9
	glVertex3f(-0.90f, -0.80f, 0.0f); //10

	glVertex3f(-0.90f, -0.70f, 0.0f); //9
	glVertex3f(-1.05f, -0.70f, 0.0f); //11
	glVertex3f(-1.05f, -0.50f, 0.0f); //12
	glVertex3f(-0.90f, -0.50f, 0.0f); //13

	//R
	glVertex3f(-0.60f, -0.30f, 0.0f); //1
	glVertex3f(-0.75f, -0.30f, 0.0f); //2
	glVertex3f(-0.75f, -0.80f, 0.0f); //3
	glVertex3f(-0.60f, -0.80f, 0.0f); //4

	glVertex3f(-0.60f, -0.60f, 0.0f); //5
	glVertex3f(-0.45f, -0.60f, 0.0f); //6
	glVertex3f(-0.30f, -0.50f, 0.0f); //7
	glVertex3f(-0.60f, -0.50f, 0.0f); //8

	glVertex3f(-0.60f, -0.40f, 0.0f); //9
	glVertex3f(-0.30f, -0.40f, 0.0f); //10
	glVertex3f(-0.45f, -0.30f, 0.0f); //11
	glVertex3f(-0.60f, -0.30f, 0.0f); //1

	glVertex3f(-0.30f, -0.40f, 0.0f); //10
	glVertex3f(-0.45f, -0.40f, 0.0f); //12
	glVertex3f(-0.45f, -0.50f, 0.0f); //13
	glVertex3f(-0.30f, -0.50f, 0.0f); //7

	glVertex3f(-0.60f, -0.60f, 0.0f); //5
	glVertex3f(-0.45f, -0.60f, 0.0f); //6
	glVertex3f(-0.30f, -0.80f, 0.0f); //14
	glVertex3f(-0.45f, -0.80f, 0.0f); //15

	glEnd();

	//o
	//Outer Circle
	glBegin(GL_POLYGON);

	glColor3f(Dr_R, Dr_G, Dr_B);

	for (angle = 0.0f; angle <= 2 * 3.148; angle = angle + 0.01f)
	{
		glVertex3f(0.075f + 0.24f * cos(angle), -0.55f + 0.25f * sin(angle), 0.0f); // outer circle
	}
	glEnd();

	//Inner Circle
	glBegin(GL_POLYGON);

	glColor3f(0.0f, 0.0f, 0.0f);

	for (angle = 0.0f; angle <= 2 * 3.148; angle = angle + 0.01f)
	{
		glVertex3f(0.075f + 0.14f * cos(angle), -0.55f + 0.15f * sin(angle), 0.0f); // Inner circle
	}
	glEnd();

	glBegin(GL_QUADS);

	glColor3f(Dr_R, Dr_G, Dr_B);

	//U
	glVertex3f(0.45f, -0.30f, 0.0f); //1
	glVertex3f(0.60f, -0.30f, 0.0f); //2
	glVertex3f(0.60f, -0.70f, 0.0f); //3
	glVertex3f(0.45f, -0.70f, 0.0f); //4

	glVertex3f(0.45f, -0.70f, 0.0f); //4
	glVertex3f(0.60f, -0.80f, 0.0f); //5
	glVertex3f(0.90f, -0.80f, 0.0f); //6
	glVertex3f(1.05f, -0.70f, 0.0f); //7

	glVertex3f(1.05f, -0.70f, 0.0f); //7
	glVertex3f(0.90f, -0.70f, 0.0f); //8
	glVertex3f(0.90f, -0.30f, 0.0f); //9
	glVertex3f(1.05f, -0.30f, 0.0f); //10

	//P
	glVertex3f(1.35f, -0.30f, 0.0f); //1
	glVertex3f(1.20f, -0.30f, 0.0f); //2
	glVertex3f(1.20f, -0.80f, 0.0f); //3
	glVertex3f(1.35f, -0.80f, 0.0f); //4

	glVertex3f(1.35f, -0.40f, 0.0f); //5
	glVertex3f(1.65f, -0.40f, 0.0f); //6
	glVertex3f(1.50f, -0.30f, 0.0f); //7
	glVertex3f(1.35f, -0.30f, 0.0f); //1

	glVertex3f(1.35f, -0.60f, 0.0f); //8
	glVertex3f(1.50f, -0.60f, 0.0f); //9
	glVertex3f(1.65f, -0.50f, 0.0f); //10
	glVertex3f(1.35f, -0.50f, 0.0f); //11

	glVertex3f(1.65f, -0.40f, 0.0f); //6
	glVertex3f(1.50f, -0.40f, 0.0f); //13
	glVertex3f(1.50f, -0.50f, 0.0f); //12
	glVertex3f(1.65f, -0.50f, 0.0f); //10

	glEnd();

	glLoadIdentity();
	glTranslatef(-1.45f, -0.95f, Dr_Z);
	glScalef(0.16f, 0.16f, 0.16f);
	DrawP();
	glTranslatef(2.5f, 0.0f, 0.0f);
	DrawR();
	glColor3f(Dr_R, Dr_G, Dr_B);
	glTranslatef(2.5f, 0.0f, 0.0f);
	DrawE();
	glTranslatef(2.5f, 0.0f, 0.0f);
	DrawS();
	glTranslatef(2.5f, 0.0f, 0.0f);
	glColor3f(Dr_R, Dr_G, Dr_B);
	DrawE();
	glTranslatef(2.5f, 0.0f, 0.0f);
	DrawN();
	glTranslatef(2.5f, 0.0f, 0.0f);
	DrawT();
	glTranslatef(2.5f, 0.0f, 0.0f);
	DrawS();

	Dr_R = Dr_R + 0.0002f;
	if (Dr_R >= 0.1f)
		Dr_R = 0.1f;

	Dr_G = Dr_G + 0.002f;
	if (Dr_G >= 1.0f)
		Dr_G = 1.0f;

	Dr_B = Dr_B + 0.0004f;
	if (Dr_B >= 0.2f)
		Dr_B = 0.2f;

	if (Dr_G == 1.0f)
		Dr_Z = Dr_Z + 0.01f;

	if (Dr_G == 1.0f)
		Dr_Z = Dr_Z + 0.01f;

	if (Dr_Z >= 1.0f)
	{
		gbIntroComplete_PSM = true;
	}
}


void LetterH_Info()
{
	glBegin(GL_QUADS);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	//culling part
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.6f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(0.3f, 0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, -0.2f, 0.0f);
	glVertex3f(-0.3f, -0.2f, 0.0f);
	glVertex3f(-0.3f, -0.6f, 0.0f);
	glVertex3f(0.3f, -0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, -0.6f, 0.0f);
	glVertex3f(-0.3f, -0.6f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();
}

void drawB()
{
	// left vertical
	glBegin(GL_QUADS);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(-0.4f, -0.8f, 0.0f);
	glEnd();

	// top
	glBegin(GL_POLYGON);
	glVertex3f(0.5f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.8f, 0.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);
	glVertex3f(0.8f, 0.4f, 0.0f);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glEnd();

	// bottom
	glBegin(GL_POLYGON);
	glVertex3f(0.8f, -0.4f, 0.0f);
	glVertex3f(-0.4f, -0.4f, 0.0f);
	glVertex3f(-0.4f, -0.8f, 0.0f);
	glVertex3f(0.5f, -0.8f, 0.0f);
	glVertex3f(0.8f, -0.5f, 0.0f);
	glEnd();

	// middle
	glBegin(GL_POLYGON);
	glVertex3f(0.65f, 0.0f, 0.0f);
	glVertex3f(0.4f, 0.15f, 0.0f);
	glVertex3f(-0.4f, 0.15f, 0.0f);
	glVertex3f(-0.4f, -0.15f, 0.0f);
	glVertex3f(0.4f, -0.15f, 0.0f);
	glEnd();

	// right upper
	glBegin(GL_POLYGON);
	glVertex3f(0.8f, 0.4f, 0.0f);
	glVertex3f(0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, 0.15f, 0.0f);
	glVertex3f(0.65f, 0.0f, 0.0f);
	glVertex3f(0.8f, 0.175f, 0.0f);
	glEnd();
	// right bottom
	glBegin(GL_POLYGON);
	glVertex3f(0.65f, 0.0f, 0.0f);
	glVertex3f(0.4f, -0.15f, 0.0f);
	glVertex3f(0.4f, -0.4f, 0.0f);
	glVertex3f(0.8f, -0.4f, 0.0f);
	glVertex3f(0.8f, -0.175f, 0.0f);

	glEnd();
}

void drawV()
{
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0, -0.4, 0.0f);
	glVertex3f(-0.15, -0.8, 0.0f);
	glVertex3f(0.15, -0.8, 0.0f);
	glEnd();

	// right bottom
	glBegin(GL_QUADS);
	glVertex3f(0.4, -0.1, 0.0f);
	glVertex3f(0.0, -0.4, 0.0f);
	glVertex3f(0.15, -0.8, 0.0f);
	glVertex3f(0.8, -0.2, 0.0f);
	glEnd();

	// left bottom
	glBegin(GL_QUADS);
	glVertex3f(-0.4, -0.1, 0.0f);
	glVertex3f(-0.0, -0.4, 0.0f);
	glVertex3f(-0.15, -0.8, 0.0f);
	glVertex3f(-0.8, -0.2, 0.0f);
	glEnd();

	// right top
	glBegin(GL_QUADS);
	glVertex3f(0.8, 0.8, 0.0f);
	glVertex3f(0.4, 0.5, 0.0f);
	glVertex3f(0.4, -0.1, 0.0f);
	glVertex3f(0.8, -0.2, 0.0f);
	glEnd();

	//left top
	glBegin(GL_QUADS);
	glVertex3f(-0.4, 0.5, 0.0f);
	glVertex3f(-0.8, 0.8, 0.0f);
	glVertex3f(-0.8, -0.2, 0.0f);
	glVertex3f(-0.4, -0.1, 0.0f);
	glEnd();
}

void DisplayInfo_JSS()
{

	//Local Function Declaration
	void SoundTrackUsedFunc();
	void BombayTheme();
	void ManWhoKnewInfinity();
	void ManWhoKnewInfinity_2Music();

	//Local Variable
	static bool infoColorFadeIn = true;

	//Code

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0);

	SoundTrackUsedFunc();
	BombayTheme();
	ManWhoKnewInfinity();
	ManWhoKnewInfinity_2Music();

	if (colorGreenForInfo <= 3.0f && infoColorFadeIn)
	{
		colorGreenForInfo = colorGreenForInfo + 0.005;
	}
	else
	{
		infoColorFadeIn = false;
		colorGreenForInfo = colorGreenForInfo - 0.01;
		colorGreenForInfoHeader = colorGreenForInfoHeader - 0.01;
	}
}

void SoundTrackUsedFunc()
{
	glTranslatef(0.2f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	DrawS();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.65f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.1f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawU();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.55f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawD();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.45f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.9f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.4f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.85f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawC();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.3f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	LetterK();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.0f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawU();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.45f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawS();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.9f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(6.35f, 3.0f, -9.0f);
	glScalef(0.23, 0.23, 0.23);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawD();
}

void BombayTheme()
{

	//BombayTheme
	GLfloat heightForBombay = 2.3, heightForAR = 1.8, heightForMovie = 1.3;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); //Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.4f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); //Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.8f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.8f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.2f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.6f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.4f, heightForBombay, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	//By - A. R. Rahman

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); //Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.2f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForAR, -9.0f);
	//glScalef(2.2,2.2,2.2);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.05f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.05f, 0.0f);
	glVertex3f(-0.05f, -0.05f, 0.0f);
	glVertex3f(0.05f, -0.05f, 0.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.4f, heightForAR, -9.0f);
	//glScalef(2.2,2.2,2.2);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.05f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.05f, 0.0f);
	glVertex3f(-0.05f, -0.05f, 0.0f);
	glVertex3f(0.05f, -0.05f, 0.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.8f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.2f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.6f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.4f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.8f, heightForAR, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	//Movie Name

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawV(); // Required V

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); // Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.4f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); //Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.8f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();
}

void ManWhoKnewInfinity()
{
	GLfloat heightForName = 0.5, heightForArtist = 0.0, heightForMovie = -0.5;
	//Name
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.4f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawS();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.4f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	//Artist

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); //Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.2f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawC();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); // Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.4f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); // B Required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.4f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.8f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.2f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM(); // W required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.6f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	//Movie Name

	//Movie Name

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawV(); // Required V

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.4f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM(); // W Required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.4f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.8f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterK();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.4f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM(); // W required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.7f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.1f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.5f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawF();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.2f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.6f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();
}

void ManWhoKnewInfinity_2Music()
{
	GLfloat heightForName = -1.7, heightForArtist = -2.2, heightForMovie = -2.7;
	//Name
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawD();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawP();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.4f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.8f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawU();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.4f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.4f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.8f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.0f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.4f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawV(); // V required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.8f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(6.2f, heightForName, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawL2();

	//Artist

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); //Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.2f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawC();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); // Required B

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.4f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); // B Required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.4f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.8f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.2f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM(); // W required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.6f, heightForArtist, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	//Movie Name

	//Movie Name

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawV(); // Required V

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.2f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.6f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.4f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.0f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM(); // W Required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.4f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterH_Info();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(5.8f, heightForMovie, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.2f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterK();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.6f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.4f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM(); // W required

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.7f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.1f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.5f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawF();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.6f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.2f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.6f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(4.0f, heightForMovie - 0.5, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();
}

// Function for Half statement
void TsSynopsisFirstHalf(void)
{
	// Local function declarations
	void DrawT(void);
	void DrawE(void);
	void DrawC(void);
	void LetterH_Info(void);
	void DrawN(void);
	void DrawO(void);
	void DrawL2(void);
	void DrawG(void);
	void DrawY(void);
	void DrawU(void);
	void DrawS(void);
	void DrawD(void);

	void DrawR(void);
	void DrawI(void);
	void DrawF(void);
	void DrawP(void);

	void DrawA(void);
	void DrawM(void); // M with 180.0f X-Axis rotation to get W
	void drawB(void); // Right now using LetterH() please replace it with DrawB()
	void LetterK(void);
	void DrawL(void); // L with 180.0f Y-Axis rotation to get J

	// Local variable declarations
	GLfloat fTsX = -6.4f;
	GLfloat fTsY = 3.0f;
	GLfloat fTsXStep = 0.40f;
	GLfloat fTsXStep_Header = 0.45f;

	// Code
	glColor3f(0.0f, GREEN_COLOR, 0.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*
		TECHNOLOGY USED

		RENDERING: OPENGL- FPP
		LANGUAGE - C
		OS - WINDOWS

		POEM BY - KAJAL TINGARE
	*/

	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawT();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawE();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawC();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	LetterH_Info();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawN();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawO();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawL2();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawO();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawG();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawY();
	fTsX += fTsXStep_Header;
	fTsX += 0.15;

	// Used
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawU();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawS();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawE();
	fTsX += fTsXStep_Header;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.23f, 0.23f, 0.23f);
	glColor3f(0.0f, colorGreenForInfoHeader, 0.0f);
	DrawD();
	fTsX += fTsXStep_Header;

	// Rendering OpenGL - FPP
	fTsY = 2.3;
	fTsX = -6.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	glScalef(0.17, 0.17, 0.17);
	DrawD();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();
	fTsX += fTsXStep / 2;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawG();
	fTsX += fTsXStep;

	//OPENGL - FPP
	fTsY = 1.8f;
	fTsX = -5.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawP();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawG();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawL2();
	fTsX += fTsXStep * 2;

	// FPP
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawF();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawP();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawP();

	// Language C
	fTsY = 1.3f;
	fTsX = -6.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawL2();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	glScalef(0.17, 0.17, 0.17);
	DrawG();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawU();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawG();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();
	fTsX += fTsXStep * 2;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawC();
	fTsX += fTsXStep;

	// OS - WINDOWS
	fTsY = 0.8f;
	fTsX = -6.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawS();
	fTsX += fTsXStep * 2;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();
	fTsX += fTsXStep / 2;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	glScalef(0.13, 0.13, 0.13);
	DrawI();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawD();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawM();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawS();
	fTsX += fTsXStep;

	// POEM By KAJAL TINGARE
	fTsY = 0.0f;
	fTsX = -6.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawP();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawO();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	glScalef(0.17, 0.17, 0.17);
	DrawM();
	fTsX += fTsXStep * 2;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	drawB(); // Place Holder for B
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.17, 0.17, 0.17);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawY();

	// Reset
	fTsY = -0.5f;
	fTsX = -5.0f;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	LetterK();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawL2();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawL2();
	fTsX += fTsXStep * 1.5;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawT();
	fTsX += fTsXStep / 2;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawI();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawN();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawG();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawA();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawR();
	fTsX += fTsXStep;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(fTsX, fTsY, -9.0f);
	glScalef(0.13, 0.13, 0.13);
	glColor3f(0.0f, colorGreenForInfo, 0.0f);
	DrawE();
	fTsX += fTsXStep;
}

void KnowledgeIsInterRelated(GLfloat leftX, GLfloat knowldgeGreen)
{
	void DrawM();
	void LetterK();
	void DrawE();
	void DrawI();
	void DrawL2();
	void DrawN();
	void DrawG();
	void DrawS(void);
	void DrawO(void);
	void DrawD(void);
	void DrawE();
	void DrawI();
	void DrawL2();
	void DrawN();
	void DrawA(void);
	void DrawT(void);
	void DrawR(void);
	void DrawD(void);
	void Dash(void);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -23.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	LetterK(); //K

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -21.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawN(); //N

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -19.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawO(); //O

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -17.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	glRotatef(180, 1.0f, 0.0f, 0.0f);
	DrawM(); //W

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -15.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawL2(); //L

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -13.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawE(); //E

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -11.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawD(); //D

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -9.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawG(); //G

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX - 7.0f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawE(); //E

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -4.00f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawI(); //I

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX -1.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawS(); //S

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 0.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawI(); //I

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 2.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawN(); //

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 4.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawT(); //

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 06.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawE(); //E

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 8.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawR(); //R

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 10.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	Dash(); //-

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 12.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawR(); //R

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 14.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawE(); //E

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 16.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawL2(); //L

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 18.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawA(); //A

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 20.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawT(); //T

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 22.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawE(); //E

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(leftX + 24.75f, -4.0f, -55.0f);
	glColor3f(0.0f, knowldgeGreen, 0.0f);
	DrawD(); //D


}

void Dash()
{
	glBegin(GL_QUADS);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(-0.4f, -0.1f, 0.0f);
	glVertex3f(0.4f, -0.1f, 0.0f);
	glEnd();

}

void DrawAstroMediCompLogo(GLfloat xTranslation, GLfloat zTranslation ) {
	GLUquadric *quadric = NULL;

	GLfloat diskRed = 0.42, diskGreen = 0.34, diskBlue = 0.14, blackColor = 0.0f;
	GLfloat sphrRed = 0.68f, sphrGreen = 0.54f, sphrBlue = 0.20f;
	static GLfloat diskRedShade = diskRed, diskGreenShade = diskGreen, diskBlueShade = diskBlue;
	static GLfloat sphrRedShade = sphrRed, sphrGreenShade = sphrGreen, sphrBlueShade = sphrBlue;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xTranslation, 0.0f, zTranslation);
	glRotatef(66.0f, -2.0f, 1.0f, 0.0f);
	glColor3f(diskRedShade, diskGreenShade, diskBlueShade);
	quadric = gluNewQuadric();
	gluDisk(quadric, 0.3f, 0.35, 120, 120);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xTranslation, 0.0f, zTranslation);
	glColor3f(sphrRedShade, sphrGreenShade, sphrBlueShade);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.2f, 60, 60);

	if (currentAnimationObject == FADEOUT_ASTROMEDICOMP) {
		if (diskRedShade >= blackColor) {
			diskRedShade -= diskRed / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (diskGreenShade >= blackColor) {
			diskGreenShade -= diskGreen / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (diskBlueShade >= blackColor) {
			diskBlueShade -= diskBlue / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		//Sphere

		if (sphrRedShade >= blackColor) {
			sphrRedShade -= sphrRed / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (sphrGreenShade >= blackColor) {
			sphrGreenShade -= sphrGreen / ASTROMEDICOMP_ANIMATION_STEPS;
		}

		if (sphrBlueShade >= blackColor) {
			sphrBlueShade -= sphrBlue / ASTROMEDICOMP_ANIMATION_STEPS;
		}
	}
}
