#define UNICODE

#include <Windows.h>
#include <stdio.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "35_TranformationWithoutMatrixFunctions.h"

#include <gl/GL.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

GLfloat pep_gCubeVertices[6][4][3];
GLfloat pep_gCubeColors[6][4][3];
GLfloat pep_gRotationAngle = 0.0f;

GLfloat pep_identity_matrix[16];
GLfloat pep_translation_matrix[16];
GLfloat pep_scale_matrix[16];
GLfloat pep_rotation_matrix_X[16];
GLfloat pep_rotation_matrix_Y[16];
GLfloat pep_rotation_matrix_Z[16];

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
	// Function Declarations
	void Initialize(void);
	void UnInitialize(void);
	void Display(void);
	void Update(void);

	// Variable Declarations
	bool pep_bDone = false;
	WNDCLASSEX pep_wndClass;
	TCHAR pep_szAppName[] = TEXT("35_TranformationWithoutMatrixFunctions");
	MSG pep_msg;
	HWND pep_hwnd;

	// Code
	if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("35_TranformationWithoutMatrixFunctions"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.hInstance = pep_hInstance;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szAppName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szAppName,
		TEXT("35_TranformationWithoutMatrixFunctions"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
		PEP_WIDTH,
		PEP_HEIGHT,
		NULL,
		NULL,
		pep_hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	Initialize();

	ShowWindow(pep_gHwnd, pep_iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			Update();

			Display();
		}
	}

	return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
	// Function Declarations
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void ReSize(int, int);

	switch (pep_iMsg)
	{
		case WM_DESTROY:
		{
			UnInitialize();
			PostQuitMessage(0);
		}
		break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
		break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
		break;

		case WM_SIZE:
		{
			ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
		}
		break;

		case WM_ERASEBKGND:
		{
			return 0;
		}

		case WM_CLOSE:
		{
			DestroyWindow(pep_hwnd);
		}
		break;

		case WM_KEYDOWN:
		{
			switch (pep_wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(pep_hwnd);
				}
				break;

				case 'F':
				case 'f':
				{
					ToggledFullScreen();
				}
				break;
			}
		}
		break;
	}

	return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
	// Variable Declarations
	MONITORINFO pep_mi = { sizeof(MONITORINFO) };

	if (false == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
		{
			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

		pep_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		pep_gbFullScreen = false;
	}
}

void Initialize(void)
{
	// Function Declarations
	void ReSize(int, int);

	// Variable Declarations
	PIXELFORMATDESCRIPTOR pep_pfd;
	int pep_iPixelFormatIndex;

	// Code
	pep_gHdc = GetDC(pep_gHwnd);

	ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_pfd.nVersion = 1;
	pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pep_pfd.iPixelType = PFD_TYPE_RGBA;
	pep_pfd.cColorBits = 32;
	pep_pfd.cRedBits = 8;
	pep_pfd.cGreenBits = 8;
	pep_pfd.cBlueBits = 8;
	pep_pfd.cAlphaBits = 8;
	pep_pfd.cDepthBits = 32;

	pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
	if (0 == pep_iPixelFormatIndex)
	{
		fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
	{
		fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "wglCreateContext Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
		DestroyWindow(pep_gHwnd);
	}

	// Column-Major Matrix
	pep_identity_matrix[0] = 1.0f;
	pep_identity_matrix[1] = 0.0f;
	pep_identity_matrix[2] = 0.0f;
	pep_identity_matrix[3] = 0.0f;
	pep_identity_matrix[4] = 0.0f;
	pep_identity_matrix[5] = 1.0f;
	pep_identity_matrix[6] = 0.0f;
	pep_identity_matrix[7] = 0.0f;
	pep_identity_matrix[8] = 0.0f;
	pep_identity_matrix[9] = 0.0f;
	pep_identity_matrix[10] = 1.0f;
	pep_identity_matrix[11] = 0.0f;
	pep_identity_matrix[12] = 0.0f;
	pep_identity_matrix[13] = 0.0f;
	pep_identity_matrix[14] = 0.0f;
	pep_identity_matrix[15] = 1.0f;

	pep_translation_matrix[0] = 1.0f;
	pep_translation_matrix[1] = 0.0f;
	pep_translation_matrix[2] = 0.0f;
	pep_translation_matrix[3] = 0.0f;
	pep_translation_matrix[4] = 0.0f;
	pep_translation_matrix[5] = 1.0f;
	pep_translation_matrix[6] = 0.0f;
	pep_translation_matrix[7] = 0.0f;
	pep_translation_matrix[8] = 0.0f;
	pep_translation_matrix[9] = 0.0f;
	pep_translation_matrix[10] = 1.0f;
	pep_translation_matrix[11] = 0.0f;
	pep_translation_matrix[12] = 0.0f;
	pep_translation_matrix[13] = 0.0f;
	pep_translation_matrix[14] = -6.0f;
	pep_translation_matrix[15] = 1.0f;

	pep_scale_matrix[0] = 0.750f;
	pep_scale_matrix[1] = 0.0f;
	pep_scale_matrix[2] = 0.0f;
	pep_scale_matrix[3] = 0.0f;
	pep_scale_matrix[4] = 0.0f;
	pep_scale_matrix[5] = 0.750f;
	pep_scale_matrix[6] = 0.0f;
	pep_scale_matrix[7] = 0.0f;
	pep_scale_matrix[8] = 0.0f;
	pep_scale_matrix[9] = 0.0f;
	pep_scale_matrix[10] = 0.750f;
	pep_scale_matrix[11] = 0.0f;
	pep_scale_matrix[12] = 0.0f;
	pep_scale_matrix[13] = 0.0f;
	pep_scale_matrix[14] = 0.0f;
	pep_scale_matrix[15] = 1.0f;


	//
	// Pyramid Vertices
	//
	
	// FrontSide-Vertices
	pep_gCubeVertices[0][0][0] = 1.0f;
	pep_gCubeVertices[0][0][1] = 1.0f;
	pep_gCubeVertices[0][0][2] = 1.0f;

	pep_gCubeVertices[0][1][0] = -1.0f;
	pep_gCubeVertices[0][1][1] = 1.0f;
	pep_gCubeVertices[0][1][2] = 1.0f;

	pep_gCubeVertices[0][2][0] = -1.0f;
	pep_gCubeVertices[0][2][1] = -1.0f;
	pep_gCubeVertices[0][2][2] = 1.0f;

	pep_gCubeVertices[0][3][0] = 1.0f;
	pep_gCubeVertices[0][3][1] = -1.0f;
	pep_gCubeVertices[0][3][2] = 1.0f;

	// RightSide-Vertices
	pep_gCubeVertices[1][0][0] = 1.0f;
	pep_gCubeVertices[1][0][1] = 1.0f;
	pep_gCubeVertices[1][0][2] = -1.0f;

	pep_gCubeVertices[1][1][0] = 1.0f;
	pep_gCubeVertices[1][1][1] = 1.0f;
	pep_gCubeVertices[1][1][2] = 1.0f;

	pep_gCubeVertices[1][2][0] = 1.0f;
	pep_gCubeVertices[1][2][1] = -1.0f;
	pep_gCubeVertices[1][2][2] = 1.0f;

	pep_gCubeVertices[1][3][0] = 1.0f;
	pep_gCubeVertices[1][3][1] = -1.0f;
	pep_gCubeVertices[1][3][2] = -1.0f;

	// BackSide-Vertices
	pep_gCubeVertices[2][0][0] = -1.0f;
	pep_gCubeVertices[2][0][1] = 1.0f;
	pep_gCubeVertices[2][0][2] = -1.0f;

	pep_gCubeVertices[2][1][0] = 1.0f;
	pep_gCubeVertices[2][1][1] = 1.0f;
	pep_gCubeVertices[2][1][2] = -1.0f;

	pep_gCubeVertices[2][2][0] = 1.0f;
	pep_gCubeVertices[2][2][1] = -1.0f;
	pep_gCubeVertices[2][2][2] = -1.0f;

	pep_gCubeVertices[2][3][0] = -1.0f;
	pep_gCubeVertices[2][3][1] = -1.0f;
	pep_gCubeVertices[2][3][2] = -1.0f;
	
	// LeftSide-Vertices
	pep_gCubeVertices[3][0][0] = -1.0f;
	pep_gCubeVertices[3][0][1] = 1.0f;
	pep_gCubeVertices[3][0][2] = 1.0f;

	pep_gCubeVertices[3][1][0] = -1.0f;
	pep_gCubeVertices[3][1][1] = 1.0f;
	pep_gCubeVertices[3][1][2] = -1.0f;

	pep_gCubeVertices[3][2][0] = -1.0f;
	pep_gCubeVertices[3][2][1] = -1.0f;
	pep_gCubeVertices[3][2][2] = -1.0f;

	pep_gCubeVertices[3][3][0] = -1.0f;
	pep_gCubeVertices[3][3][1] = -1.0f;
	pep_gCubeVertices[3][3][2] = 1.0f;

	// TopSide-Vertices
	pep_gCubeVertices[4][0][0] = 1.0f;
	pep_gCubeVertices[4][0][1] = 1.0f;
	pep_gCubeVertices[4][0][2] = -1.0f;

	pep_gCubeVertices[4][1][0] = -1.0f;
	pep_gCubeVertices[4][1][1] = 1.0f;
	pep_gCubeVertices[4][1][2] = -1.0f;

	pep_gCubeVertices[4][2][0] = -1.0f;
	pep_gCubeVertices[4][2][1] = 1.0f;
	pep_gCubeVertices[4][2][2] = 1.0f;

	pep_gCubeVertices[4][3][0] = 1.0f;
	pep_gCubeVertices[4][3][1] = 1.0f;
	pep_gCubeVertices[4][3][2] = 1.0f;

	// BottomSide-Vertices
	pep_gCubeVertices[5][0][0] = 1.0f;
	pep_gCubeVertices[5][0][1] = -1.0f;
	pep_gCubeVertices[5][0][2] = -1.0f;

	pep_gCubeVertices[5][1][0] = -1.0f;
	pep_gCubeVertices[5][1][1] = -1.0f;
	pep_gCubeVertices[5][1][2] = -1.0f;

	pep_gCubeVertices[5][2][0] = -1.0f;
	pep_gCubeVertices[5][2][1] = -1.0f;
	pep_gCubeVertices[5][2][2] = 1.0f;

	pep_gCubeVertices[5][3][0] = 1.0f;
	pep_gCubeVertices[5][3][1] = -1.0f;
	pep_gCubeVertices[5][3][2] = 1.0f;

	//
	// Color
	//

	// FrontSide-Color
	pep_gCubeColors[0][0][0] = 0.0f;
	pep_gCubeColors[0][0][1] = 0.0f;
	pep_gCubeColors[0][0][2] = 1.0f;

	pep_gCubeColors[0][1][0] = 0.0f;
	pep_gCubeColors[0][1][1] = 0.0f;
	pep_gCubeColors[0][1][2] = 1.0f;

	pep_gCubeColors[0][2][0] = 0.0f;
	pep_gCubeColors[0][2][1] = 0.0f;
	pep_gCubeColors[0][2][2] = 1.0f;

	pep_gCubeColors[0][3][0] = 0.0f;
	pep_gCubeColors[0][3][1] = 0.0f;
	pep_gCubeColors[0][3][2] = 1.0f;

	// RightSide-Color
	pep_gCubeColors[1][0][0] = 1.0f;
	pep_gCubeColors[1][0][1] = 0.0f;
	pep_gCubeColors[1][0][2] = 1.0f;

	pep_gCubeColors[1][1][0] = 1.0f;
	pep_gCubeColors[1][1][1] = 0.0f;
	pep_gCubeColors[1][1][2] = 1.0f;

	pep_gCubeColors[1][2][0] = 1.0f;
	pep_gCubeColors[1][2][1] = 0.0f;
	pep_gCubeColors[1][2][2] = 1.0f;

	pep_gCubeColors[1][3][0] = 1.0f;
	pep_gCubeColors[1][3][1] = 0.0f;
	pep_gCubeColors[1][3][2] = 1.0f;

	// BackSide-Color
	pep_gCubeColors[2][0][0] = 0.0f;
	pep_gCubeColors[2][0][1] = 1.0f;
	pep_gCubeColors[2][0][2] = 1.0f;

	pep_gCubeColors[2][1][0] = 0.0f;
	pep_gCubeColors[2][1][1] = 1.0f;
	pep_gCubeColors[2][1][2] = 1.0f;

	pep_gCubeColors[2][2][0] = 0.0f;
	pep_gCubeColors[2][2][1] = 1.0f;
	pep_gCubeColors[2][2][2] = 1.0f;

	pep_gCubeColors[2][3][0] = 0.0f;
	pep_gCubeColors[2][3][1] = 1.0f;
	pep_gCubeColors[2][3][2] = 1.0f;

	// LeftSide-Color
	pep_gCubeColors[3][0][0] = 1.0f;
	pep_gCubeColors[3][0][1] = 1.0f;
	pep_gCubeColors[3][0][2] = 0.0f;

	pep_gCubeColors[3][1][0] = 1.0f;
	pep_gCubeColors[3][1][1] = 1.0f;
	pep_gCubeColors[3][1][2] = 0.0f;

	pep_gCubeColors[3][2][0] = 1.0f;
	pep_gCubeColors[3][2][1] = 1.0f;
	pep_gCubeColors[3][2][2] = 0.0f;

	pep_gCubeColors[3][3][0] = 1.0f;
	pep_gCubeColors[3][3][1] = 1.0f;
	pep_gCubeColors[3][3][2] = 0.0f;

	// TopSide-Color
	pep_gCubeColors[4][0][0] = 1.0f;
	pep_gCubeColors[4][0][1] = 0.0f;
	pep_gCubeColors[4][0][2] = 0.0f;

	pep_gCubeColors[4][1][0] = 1.0f;
	pep_gCubeColors[4][1][1] = 0.0f;
	pep_gCubeColors[4][1][2] = 0.0f;

	pep_gCubeColors[4][2][0] = 1.0f;
	pep_gCubeColors[4][2][1] = 0.0f;
	pep_gCubeColors[4][2][2] = 0.0f;

	pep_gCubeColors[4][3][0] = 1.0f;
	pep_gCubeColors[4][3][1] = 0.0f;
	pep_gCubeColors[4][3][2] = 0.0f;

	// BottomSide-Color
	pep_gCubeColors[5][0][0] = 0.0f;
	pep_gCubeColors[5][0][1] = 1.0f;
	pep_gCubeColors[5][0][2] = 0.0f;

	pep_gCubeColors[5][1][0] = 0.0f;
	pep_gCubeColors[5][1][1] = 1.0f;
	pep_gCubeColors[5][1][2] = 0.0f;

	pep_gCubeColors[5][2][0] = 0.0f;
	pep_gCubeColors[5][2][1] = 1.0f;
	pep_gCubeColors[5][2][2] = 0.0f;

	pep_gCubeColors[5][3][0] = 0.0f;
	pep_gCubeColors[5][3][1] = 1.0f;
	pep_gCubeColors[5][3][2] = 0.0f;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);

	ReSize(PEP_WIDTH, PEP_HEIGHT);

	return;
}

void ReSize(int pep_width, int pep_height)
{
	if (0 == pep_height)
	{
		pep_height = 1;
	}

	glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)pep_width / (GLfloat)pep_height, 0.01f, 100.0f);

	return;
}

void Display(void)
{
	// Variable Declarations
	GLfloat angle_cube_rad = 0.0f;

	angle_cube_rad = pep_gRotationAngle * M_PI / 180.0f;

	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	pep_rotation_matrix_X[0] = 1.0f;
	pep_rotation_matrix_X[1] = 0.0f;
	pep_rotation_matrix_X[2] = 0.0f;
	pep_rotation_matrix_X[3] = 0.0f;
	pep_rotation_matrix_X[4] = 0.0f;
	pep_rotation_matrix_X[5] = (GLfloat)cos(angle_cube_rad);
	pep_rotation_matrix_X[6] = (GLfloat)sin(angle_cube_rad);
	pep_rotation_matrix_X[7] = 0.0f;
	pep_rotation_matrix_X[8] = 0.0f;
	pep_rotation_matrix_X[9] = (GLfloat)-sin(angle_cube_rad);
	pep_rotation_matrix_X[10] = (GLfloat)cos(angle_cube_rad);
	pep_rotation_matrix_X[11] = 0.0f;
	pep_rotation_matrix_X[12] = 0.0f;
	pep_rotation_matrix_X[13] = 0.0f;
	pep_rotation_matrix_X[14] = 0.0f;
	pep_rotation_matrix_X[15] = 1.0f;
	
	pep_rotation_matrix_Y[0] = (GLfloat)cos(angle_cube_rad);
	pep_rotation_matrix_Y[1] = 0.0f;
	pep_rotation_matrix_Y[2] = (GLfloat)-sin(angle_cube_rad);
	pep_rotation_matrix_Y[3] = 0.0f;
	pep_rotation_matrix_Y[4] = 0.0f;
	pep_rotation_matrix_Y[5] = 1.0f;
	pep_rotation_matrix_Y[6] = 0.0f;
	pep_rotation_matrix_Y[7] = 0.0f;
	pep_rotation_matrix_Y[8] = (GLfloat)sin(angle_cube_rad);
	pep_rotation_matrix_Y[9] = 0.0f;
	pep_rotation_matrix_Y[10] = (GLfloat)cos(angle_cube_rad);
	pep_rotation_matrix_Y[11] = 0.0f;
	pep_rotation_matrix_Y[12] = 0.0f;
	pep_rotation_matrix_Y[13] = 0.0f;
	pep_rotation_matrix_Y[14] = 0.0f;
	pep_rotation_matrix_Y[15] = 1.0f;

	pep_rotation_matrix_Z[0] = (GLfloat)cos(angle_cube_rad);
	pep_rotation_matrix_Z[1] = (GLfloat)sin(angle_cube_rad);
	pep_rotation_matrix_Z[2] = 0.0f;
	pep_rotation_matrix_Z[3] = 0.0f;
	pep_rotation_matrix_Z[4] = (GLfloat)-sin(angle_cube_rad);
	pep_rotation_matrix_Z[5] = (GLfloat)cos(angle_cube_rad);
	pep_rotation_matrix_Z[6] = 0.0f;
	pep_rotation_matrix_Z[7] = 0.0f;
	pep_rotation_matrix_Z[8] = 0.0f;
	pep_rotation_matrix_Z[9] = 0.0f;
	pep_rotation_matrix_Z[10] = 1.0f;
	pep_rotation_matrix_Z[11] = 0.0f;
	pep_rotation_matrix_Z[12] = 0.0f;
	pep_rotation_matrix_Z[13] = 0.0f;
	pep_rotation_matrix_Z[14] = 0.0f;
	pep_rotation_matrix_Z[15] = 1.0f;

	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	glLoadMatrixf(pep_identity_matrix);
	//glTranslatef(0.0f, 0.0f, -6.0f);
	glMultMatrixf(pep_translation_matrix);
	//glScalef(0.75f, 0.75f, 0.75f);
	glMultMatrixf(pep_scale_matrix);
	//glRotatef(pep_gRotationAngle, 1.0f, 0.0f, 0.0f);
	glMultMatrixf(pep_rotation_matrix_X);
	//glRotatef(pep_gRotationAngle, 0.0f, 1.0f, 0.0f);
	glMultMatrixf(pep_rotation_matrix_Y);
	//glRotatef(pep_gRotationAngle, 0.0f, 0.0f, 1.0f);
	glMultMatrixf(pep_rotation_matrix_Z);

	for (int i = 0; i < 6; i++)
	{
		glBegin(GL_QUADS);
		glColor3f(pep_gCubeColors[i][0][0], pep_gCubeColors[i][0][1], pep_gCubeColors[i][0][2]);
		glVertex3f(pep_gCubeVertices[i][0][0], pep_gCubeVertices[i][0][1], pep_gCubeVertices[i][0][2]);

		glColor3f(pep_gCubeColors[i][1][0], pep_gCubeColors[i][1][1], pep_gCubeColors[i][1][2]);
		glVertex3f(pep_gCubeVertices[i][1][0], pep_gCubeVertices[i][1][1], pep_gCubeVertices[i][1][2]);

		glColor3f(pep_gCubeColors[i][2][0], pep_gCubeColors[i][2][1], pep_gCubeColors[i][2][2]);
		glVertex3f(pep_gCubeVertices[i][2][0], pep_gCubeVertices[i][2][1], pep_gCubeVertices[i][2][2]);

		glColor3f(pep_gCubeColors[i][3][0], pep_gCubeColors[i][3][1], pep_gCubeColors[i][3][2]);
		glVertex3f(pep_gCubeVertices[i][3][0], pep_gCubeVertices[i][3][1], pep_gCubeVertices[i][3][2]);
		glEnd();
	}

	SwapBuffers(pep_gHdc);
}

void Update(void)
{
	if (pep_gRotationAngle > 360.0f)
	{
		pep_gRotationAngle = 1.0f;
	}
	pep_gRotationAngle += 1.0f;
}

void UnInitialize(void)
{
	if (true == pep_gbFullScreen)
	{
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (pep_gHglrc == wglGetCurrentContext())
	{
		wglMakeCurrent(0, 0);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
		pep_gHglrc = NULL;
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
		pep_gHdc = NULL;
	}

	if (NULL != pep_gpFile)
	{
		fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
		fclose(pep_gpFile);
		pep_gpFile = NULL;
	}

	return;
}
