#define UNICODE

#include <Windows.h>
#include <stdio.h>

#include "48_24SphereWithLight.h"

#include <gl/GL.h>
#include <gl/glu.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE* pep_gpFile = NULL;
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;

// Light Configuration
bool pep_gbLight = false;

GLfloat pep_gLightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat pep_gLightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
// W Component Of Light Source Is 1.0f, It Menas It Is Positional Light
GLfloat pep_gLightPosition[] = {0.0f, 0.0f, 0.0f, 1.0f};

GLfloat pep_gLightModelAmbient[] = {0.2f, 0.2f, 0.2f, 1.0f};
GLfloat pep_gLightModelLocalViewer[] = {0.0f};

GLfloat pep_gAngleXRotation = 0.0f;
GLfloat pep_gAngleYRotation = 0.0f;
GLfloat pep_gAngleZRotation = 0.0f;

GLint pep_gKeyPress = 0;

GLUquadric *pep_gpQuadric[24];

GLfloat pep_gPositionOnXAxis = 0.0f;
GLfloat pep_gDisplacementOnXAxis = 0.0f;

int WINAPI WinMain(HINSTANCE pep_hInstance, HINSTANCE pep_hPrevInstance, LPSTR pep_lpszCmdLine, int pep_iCmdShow)
{
    // Function Declarations
    void Initialize(void);
    void UnInitialize(void);
    void Display(void);
    void Update(void);

    // Variable Declarations
    bool pep_bDone = false;
    WNDCLASSEX pep_wndClass;
    TCHAR pep_szAppName[] = TEXT("49_ShapePatterns");
    MSG pep_msg;
    HWND pep_hwnd;

    // Code
    if (0 != fopen_s(&pep_gpFile, "pep_log.txt", "w"))
    {
        MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("49_ShapePatterns"), MB_OK);
        exit(0);
    }
    else
    {
        fprintf(pep_gpFile, "Log File Opened Successfully And Application Started Successfully\n");
    }

    pep_wndClass.cbSize = sizeof(WNDCLASSEX);
    pep_wndClass.cbWndExtra = 0;
    pep_wndClass.cbClsExtra = 0;
    pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    pep_wndClass.hInstance = pep_hInstance;
    pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    pep_wndClass.hIcon = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
    pep_wndClass.hIconSm = LoadIcon(pep_hInstance, MAKEINTRESOURCE(MYICON));
    pep_wndClass.lpszMenuName = NULL;
    pep_wndClass.lpszClassName = pep_szAppName;
    pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    pep_wndClass.lpfnWndProc = WndProc;

    RegisterClassEx(&pep_wndClass);

    pep_hwnd = CreateWindowEx(
        WS_EX_APPWINDOW,
        pep_szAppName,
        TEXT("49_ShapePatterns"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        (GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2),
        (GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
        PEP_WIDTH,
        PEP_HEIGHT,
        NULL,
        NULL,
        pep_hInstance,
        NULL
    );

    pep_gHwnd = pep_hwnd;

    Initialize();

    ShowWindow(pep_gHwnd, pep_iCmdShow);
    SetForegroundWindow(pep_hwnd);
    SetFocus(pep_hwnd);

    while (false == pep_bDone)
    {
        if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
        {
            if (WM_QUIT == pep_msg.message)
            {
	            pep_bDone = true;
            }
            else
            {
	            TranslateMessage(&pep_msg);
	            DispatchMessage(&pep_msg);
            }
        }
        else
        {
            Update();
            Display();
        }
    }

    return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT pep_iMsg, WPARAM pep_wParam, LPARAM pep_lParam)
{
    // Function Declarations
    void ToggledFullScreen(void);
    void UnInitialize(void);
    void ReSize(int, int);

    switch (pep_iMsg)
    {
        case WM_DESTROY:
        {
        UnInitialize();
        PostQuitMessage(0);
        }
        break;

        case WM_SETFOCUS:
        pep_gbActiveWindow = true;
        break;

        case WM_KILLFOCUS:
        pep_gbActiveWindow = false;
        break;

        case WM_SIZE:
        {
        ReSize(LOWORD(pep_lParam), HIWORD(pep_lParam));
        }
        break;

        case WM_ERASEBKGND:
        {
        return 0;
        }

        case WM_CLOSE:
        {
        DestroyWindow(pep_hwnd);
        }
        break;

        case WM_KEYDOWN:
        {
            switch (pep_wParam)
            {
                case VK_ESCAPE:
                {
                DestroyWindow(pep_hwnd);
                }
                break;

                case 'F':
                case 'f':
                {
                    ToggledFullScreen();
                }
                break;

                case 'L':
                case 'l':
                {
                    if (false == pep_gbLight)
                    {
                        pep_gbLight = true;
                        glEnable(GL_LIGHTING);
                    }
                    else
                    {
                        pep_gbLight = false;
                        glDisable(GL_LIGHTING);
                    }
                } break;

                case 'X':
                case 'x':
                {
                    pep_gKeyPress = 1;
                    pep_gAngleXRotation = 0.0f;
                }
                break;

                case 'Y':
                case 'y':
                {
                    pep_gKeyPress = 2;
                    pep_gAngleYRotation = 0.0f;
                }
                break;

                case 'Z':
                case 'z':
                {
                    pep_gKeyPress = 3;
                    pep_gAngleZRotation = 0.0f;
                }
                break;
            }
        }
        break;
    }

    return DefWindowProc(pep_hwnd, pep_iMsg, pep_wParam, pep_lParam);
}

void  ToggledFullScreen(void)
{
    // Variable Declarations
    MONITORINFO pep_mi = { sizeof(MONITORINFO) };

    // Code
    if (false == pep_gbFullScreen)
    {
        pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
        {
            if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
            {
                SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

                SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left, pep_mi.rcMonitor.top, pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);

        pep_gbFullScreen = true;

        pep_gDisplacementOnXAxis = 7.5f;
    }
    else
    {
        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

        SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

        SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

        ShowCursor(TRUE);

        pep_gbFullScreen = false;

        pep_gDisplacementOnXAxis = 5.0f;
    }
}

void Initialize(void)
{
    // Function Declarations
    void ReSize(int, int);

    // Variable Declarations
    PIXELFORMATDESCRIPTOR pep_pfd;
    int pep_iPixelFormatIndex;

    // Code
    pep_gHdc = GetDC(pep_gHwnd);

    ZeroMemory(&pep_pfd, sizeof(PIXELFORMATDESCRIPTOR));

    pep_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pep_pfd.nVersion = 1;
    pep_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pep_pfd.iPixelType = PFD_TYPE_RGBA;
    pep_pfd.cColorBits = 32;
    pep_pfd.cRedBits = 8;
    pep_pfd.cGreenBits = 8;
    pep_pfd.cBlueBits = 8;
    pep_pfd.cAlphaBits = 8;

    pep_iPixelFormatIndex = ChoosePixelFormat(pep_gHdc, &pep_pfd);
    if (0 == pep_iPixelFormatIndex)
    {
        fprintf(pep_gpFile, "ChoosePixelFormat Failed...\n");
        DestroyWindow(pep_gHwnd);
    }

    if (FALSE == SetPixelFormat(pep_gHdc, pep_iPixelFormatIndex, &pep_pfd))
    {
        fprintf(pep_gpFile, "SetPixelFormat Failed...\n");
        DestroyWindow(pep_gHwnd);
    }

    pep_gHglrc = wglCreateContext(pep_gHdc);
    if (NULL == pep_gHglrc)
    {
        fprintf(pep_gpFile, "wglCreateContext Failed...\n");
        DestroyWindow(pep_gHwnd);
    }

    if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
    {
        fprintf(pep_gpFile, "wglMakeCurrent Failed...\n");
        DestroyWindow(pep_gHwnd);
    }

    pep_gPositionOnXAxis = 2.5f;
    pep_gDisplacementOnXAxis = 5.0f;

    for (int i = 0; i < 24; i++)
    {
        pep_gpQuadric[i] = gluNewQuadric();
    }

    glClearDepth(1.0f);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);

    glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

    // ADS - light initialization
    // LIGHT
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);

    glLightfv(GL_LIGHT0, GL_AMBIENT , pep_gLightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, pep_gLightDiffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, pep_gLightPosition);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, pep_gLightModelAmbient);
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, pep_gLightModelLocalViewer);

    glEnable(GL_LIGHT0);

    ReSize(PEP_WIDTH, PEP_HEIGHT);

    return;
}

void ReSize(int pep_width, int pep_height)
{
    if (0 == pep_height)
    {
        pep_height = 1;
    }

    glViewport(0, 0, (GLsizei)pep_width, (GLsizei)pep_height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (pep_width < pep_height)
    {
        glOrtho(0.0f, 15.5f, 0.0f, (15.5f * ((GLfloat)pep_height / (GLfloat)pep_width)), -10.0f, 10.0f);
    }
    else
    {
        glOrtho(0.0f, (15.5f * ((GLfloat)pep_width / (GLfloat)pep_height)), 0.0f, 15.5f, -10.0f, 10.0f);
    }

    return;
}

void Display(void)
{
    // Function Declarations
    void Draw24Sphere(void);

    // Code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (1 == pep_gKeyPress)
    {
        glRotatef(pep_gAngleXRotation, 1.0f, 0.0f, 0.0f);
        pep_gLightPosition[0] = 0.0f;
        pep_gLightPosition[1] = pep_gAngleXRotation;
        pep_gLightPosition[2] = 0.0f;
    }
    else if (2 == pep_gKeyPress)
    {
        glRotatef(pep_gAngleYRotation, 0.0f, 1.0f, 0.0f);
        pep_gLightPosition[0] = 0.0f;
        pep_gLightPosition[1] = 0.0f;
        pep_gLightPosition[2] = pep_gAngleYRotation;
    }
    else if (3 == pep_gKeyPress)
    {
        glRotatef(pep_gAngleZRotation, 0.0f, 0.0f, 1.0f);
        pep_gLightPosition[0] = pep_gAngleZRotation;
        pep_gLightPosition[1] = 0.0f;
        pep_gLightPosition[2] = 0.0f;
    }

    glLightfv(GL_LIGHT0, GL_POSITION, pep_gLightPosition);

    Draw24Sphere();

    SwapBuffers(pep_gHdc);
}

void UnInitialize(void)
{
    //
    // Code
    //
    for (int i = 0; i < 24; i++)
    {
        gluDeleteQuadric(pep_gpQuadric[i]);
    }

    if (true == pep_gbFullScreen)
    {
        pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

        SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

        SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

        ShowCursor(TRUE);
    }

    if (pep_gHglrc == wglGetCurrentContext())
    {
        wglMakeCurrent(0, 0);
    }

    if (NULL != pep_gHglrc)
    {
        wglDeleteContext(pep_gHglrc);
        pep_gHglrc = NULL;
    }

    if (NULL != pep_gHdc)
    {
        ReleaseDC(pep_gHwnd, pep_gHdc);
        pep_gHdc = NULL;
    }

    if (NULL != pep_gpFile)
    {
        fprintf(pep_gpFile, "Log File Closed Successfully And Application Closed Successfully\n");
        fclose(pep_gpFile);
        pep_gpFile = NULL;
    }

    return;
}

void Update(void)
{
    // Code
    if (1 == pep_gKeyPress)
    {
        pep_gAngleXRotation += 1.51f;

        if (pep_gAngleXRotation == 360.0f)
        {
	        pep_gAngleXRotation = 0.0f;
        }
    }
    else if (2 == pep_gKeyPress)
    {
        pep_gAngleYRotation += 1.51f;

        if (pep_gAngleYRotation == 360.0f)
        {
	        pep_gAngleYRotation = 0.0f;
        }
    }
    else if (3 == pep_gKeyPress)
    {
        pep_gAngleZRotation += 1.51f;
        if (pep_gAngleZRotation == 360.0f)
        {
	        pep_gAngleZRotation = 0.0f;
        }
    }

    return;
}

void Draw24Sphere(void)
{
    // Variable Declarations
    GLfloat pep_materialAmbient[4];
    GLfloat pep_materialDiffuse[4];
    GLfloat pep_materialSpecular[4];
    GLfloat pep_materialShininess;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /*---------------------------1st Column : Gems----------------------------*/
    /* 1st Column, 1st Sphere, Emerald  */
    pep_materialAmbient[0] = 0.0215f;
    pep_materialAmbient[1] = 0.1745f;
    pep_materialAmbient[2] = 0.0215f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.07568f;
    pep_materialDiffuse[1] = 0.61424f;
    pep_materialDiffuse[2] = 0.07568f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.633f;
    pep_materialSpecular[1] = 0.727811f;
    pep_materialSpecular[2] = 0.633f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.6f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[0], 1.0f, 30, 30);

    /* 1st Column, 2nd Sphere, Jade */
    pep_materialAmbient[0] = 0.135f;
    pep_materialAmbient[1] = 0.2225f;
    pep_materialAmbient[2] = 0.1575f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.54f;
    pep_materialDiffuse[1] = 0.89f;
    pep_materialDiffuse[2] = 0.63f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.316228f;
    pep_materialSpecular[1] = 0.316228f;
    pep_materialSpecular[2] = 0.316228f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.1f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[1], 1.0f, 30, 30);

    /* 1st Column, 3rd Sphere, Obsidian */
    pep_materialAmbient[0] = 0.05375f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.06625f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.18275f;
    pep_materialDiffuse[1] = 0.17f;
    pep_materialDiffuse[2] = 0.22525f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.332741f;
    pep_materialSpecular[1] = 0.328634f;
    pep_materialSpecular[2] = 0.346435f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.3f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[2], 1.0f, 30, 30);

    /* 1st Column, 4th Sphere, Pearl */
    pep_materialAmbient[0] = 0.25f;
    pep_materialAmbient[1] = 0.20725f;
    pep_materialAmbient[2] = 0.20725f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 1.0f;
    pep_materialDiffuse[1] = 0.829f;
    pep_materialDiffuse[2] = 0.829f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.296648f;
    pep_materialSpecular[1] = 0.296648f;
    pep_materialSpecular[2] = 0.296648f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.088f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[3], 1.0f, 30, 30);

    /* 1st Column, 5th Sphere, Ruby */
    pep_materialAmbient[0] = 0.1745f;
    pep_materialAmbient[1] = 0.01175f;
    pep_materialAmbient[2] = 0.01175f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.61424f;
    pep_materialDiffuse[1] = 0.04136f;
    pep_materialDiffuse[2] = 0.04136f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.727811f;
    pep_materialSpecular[1] = 0.626959f;
    pep_materialSpecular[2] = 0.626959f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.6f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[4], 1.0f, 30, 30);

    /* 1st Column, 6th Sphere, Turquoise */
    pep_materialAmbient[0] = 0.1f;
    pep_materialAmbient[1] = 0.18725f;
    pep_materialAmbient[2] = 0.1745f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.396f;
    pep_materialDiffuse[1] = 0.74151f;
    pep_materialDiffuse[2] = 0.69102f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.297254f;
    pep_materialSpecular[1] = 0.30829f;
    pep_materialSpecular[2] = 0.306678f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.1f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (0.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[5], 1.0f, 30, 30);

    /*---------------------------2nd Column : Metal----------------------------*/
    /* 2nd Column, 1st Sphere, Brass */
    pep_materialAmbient[0] = 0.329412f;
    pep_materialAmbient[1] = 0.223529f;
    pep_materialAmbient[2] = 0.027451f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.780392f;
    pep_materialDiffuse[1] = 0.568627f;
    pep_materialDiffuse[2] = 0.113725f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.992157f;
    pep_materialSpecular[1] = 0.941176f;
    pep_materialSpecular[2] = 0.807843f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.21794872f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[6], 1.0f, 30, 30);

    /* 2nd Column, 2nd Sphere, Bronze */
    pep_materialAmbient[0] = 0.2125f;
    pep_materialAmbient[1] = 0.1275f;
    pep_materialAmbient[2] = 0.054f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.714f;
    pep_materialDiffuse[1] = 0.4284f;
    pep_materialDiffuse[2] = 0.18144f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.393548f;
    pep_materialSpecular[1] = 0.271906f;
    pep_materialSpecular[2] = 0.166721f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.2f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[7], 1.0f, 30, 30);

    /* 2nd Column, 3rd Sphere, Chrome */
    pep_materialAmbient[0] = 0.25f;
    pep_materialAmbient[1] = 0.25f;
    pep_materialAmbient[2] = 0.25f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.4f;
    pep_materialDiffuse[1] = 0.4f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.774597f;
    pep_materialSpecular[1] = 0.774597f;
    pep_materialSpecular[2] = 0.774597f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.6f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[8], 1.0f, 30, 30);

    /* 2nd Column, 4th Sphere, Copper */
    pep_materialAmbient[0] = 0.19125f;
    pep_materialAmbient[1] = 0.0735f;
    pep_materialAmbient[2] = 0.0225f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.7038f;
    pep_materialDiffuse[1] = 0.27048f;
    pep_materialDiffuse[2] = 0.0828f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.256777f;
    pep_materialSpecular[1] = 0.137622f;
    pep_materialSpecular[2] = 0.086014f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.1f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[9], 1.0f, 30, 30);

    /* 2nd Column, 5th Sphere, Gold */
    pep_materialAmbient[0] = 0.24725f;
    pep_materialAmbient[1] = 0.1995f;
    pep_materialAmbient[2] = 0.0745f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.75164f;
    pep_materialDiffuse[1] = 0.60648f;
    pep_materialDiffuse[2] = 0.22648f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.628281f;
    pep_materialSpecular[1] = 0.555802f;
    pep_materialSpecular[2] = 0.366065f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.4f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[10], 1.0f, 30, 30);

    /* 2nd Column, 6th Sphere, Silver */
    pep_materialAmbient[0] = 0.19225f;
    pep_materialAmbient[1] = 0.19225f;
    pep_materialAmbient[2] = 0.19225f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.50754f;
    pep_materialDiffuse[1] = 0.50754f;
    pep_materialDiffuse[2] = 0.50754f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.508273f;
    pep_materialSpecular[1] = 0.508273f;
    pep_materialSpecular[2] = 0.508273f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.4f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (1.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[11], 1.0f, 30, 30);

    /*---------------------------3rd Column : Plastic--------------------------*/
    /* 3rd Column, 1st Sphere, Black */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.01f;
    pep_materialDiffuse[1] = 0.01f;
    pep_materialDiffuse[2] = 0.01f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.50f;
    pep_materialSpecular[1] = 0.50f;
    pep_materialSpecular[2] = 0.50f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[12], 1.0f, 30, 30);

    /* 3rd Column, 2nd Sphere, Cyan */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.1f;
    pep_materialAmbient[2] = 0.06f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.0f;
    pep_materialDiffuse[1] = 0.50980392f;
    pep_materialDiffuse[2] = 0.50980392f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.50196078f;
    pep_materialSpecular[1] = 0.50196078f;
    pep_materialSpecular[2] = 0.50196078f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[13], 1.0f, 30, 30);

    /* 3rd Column, 3rd Sphere, Green */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.1f;
    pep_materialDiffuse[1] = 0.35f;
    pep_materialDiffuse[2] = 0.1f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.45f;
    pep_materialSpecular[1] = 0.55f;
    pep_materialSpecular[2] = 0.45f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[14], 1.0f, 30, 30);

    /* 3rd Column, 4th Sphere, Red */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.0f;
    pep_materialDiffuse[2] = 0.0f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.7f;
    pep_materialSpecular[1] = 0.6f;
    pep_materialSpecular[2] = 0.6f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[15], 1.0f, 30, 30);

    /*3rd Column, 5th Sphere, White */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.55f;
    pep_materialDiffuse[1] = 0.55f;
    pep_materialDiffuse[2] = 0.55f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.70f;
    pep_materialSpecular[1] = 0.70f;
    pep_materialSpecular[2] = 0.70f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[16], 1.0f, 30, 30);

    /* 3rd Column, 6th Sphere, Yellow  */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.0f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.60f;
    pep_materialSpecular[1] = 0.60f;
    pep_materialSpecular[2] = 0.50f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.25f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (2.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[17], 1.0f, 30, 30);

    /*---------------------------4th Column : Rubber---------------------------*/
    /* 4th Column, 1st Sphere, Black */
    pep_materialAmbient[0] = 0.02f;
    pep_materialAmbient[1] = 0.02f;
    pep_materialAmbient[2] = 0.02f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.01f;
    pep_materialDiffuse[1] = 0.01f;
    pep_materialDiffuse[2] = 0.01f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.40f;
    pep_materialSpecular[1] = 0.40f;
    pep_materialSpecular[2] = 0.40f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 14.0f, 0.0f);

    gluSphere(pep_gpQuadric[18], 1.0f, 30, 30);

    /* 4th Column, 2nd Sphere, Cyan */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.05f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.4f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.5f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.04f;
    pep_materialSpecular[1] = 0.7f;
    pep_materialSpecular[2] = 0.7f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 11.5f, 0.0f);

    gluSphere(pep_gpQuadric[19], 1.0f, 30, 30);

    /* 4th Column, 3rd Sphere, Green */
    pep_materialAmbient[0] = 0.0f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.4f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.04f;
    pep_materialSpecular[1] = 0.7f;
    pep_materialSpecular[2] = 0.04f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 9.0f, 0.0f);

    gluSphere(pep_gpQuadric[20], 1.0f, 30, 30);

    /* 4th Column, 4th Sphere, Red */
    pep_materialAmbient[0] = 0.05f;
    pep_materialAmbient[1] = 0.0f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.4f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.7f;
    pep_materialSpecular[1] = 0.04f;
    pep_materialSpecular[2] = 0.04f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 6.5f, 0.0f);

    gluSphere(pep_gpQuadric[21], 1.0f, 30, 30);

    /*4th Column, 5th Sphere, White */
    pep_materialAmbient[0] = 0.05f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.05f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.5f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.7f;
    pep_materialSpecular[1] = 0.7f;
    pep_materialSpecular[2] = 0.7f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 4.0f, 0.0f);

    gluSphere(pep_gpQuadric[22], 1.0f, 30, 30);

    /* 4th Column, 6th Sphere, Yellow */
    pep_materialAmbient[0] = 0.05f;
    pep_materialAmbient[1] = 0.05f;
    pep_materialAmbient[2] = 0.0f;
    pep_materialAmbient[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_AMBIENT, pep_materialAmbient);

    pep_materialDiffuse[0] = 0.5f;
    pep_materialDiffuse[1] = 0.5f;
    pep_materialDiffuse[2] = 0.4f;
    pep_materialDiffuse[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, pep_materialDiffuse);

    pep_materialSpecular[0] = 0.70f;
    pep_materialSpecular[1] = 0.70f;
    pep_materialSpecular[2] = 0.04f;
    pep_materialSpecular[3] = 1.0f;
    glMaterialfv(GL_FRONT, GL_SPECULAR, pep_materialSpecular);

    pep_materialShininess = 0.078125f * 128;
    glMaterialf(GL_FRONT, GL_SHININESS, pep_materialShininess);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(pep_gPositionOnXAxis + (3.0f * pep_gDisplacementOnXAxis), 1.5f, 0.0f);

    gluSphere(pep_gpQuadric[23], 1.0f, 30, 30);
    return;
}
