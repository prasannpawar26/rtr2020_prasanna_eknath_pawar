#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
//#include "resource.h"

#pragma comment(lib, "opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DWORD ssa_dwStyle;
WINDOWPLACEMENT ssa_wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ssa_ghWnd;
HDC ssa_ghdc = NULL;
HGLRC ssa_ghrc = NULL;

FILE* ssa_gFilePointer = NULL;

bool ssa_gbActiveWindow = false;
bool ssa_gbFullScreen = false;

void Initialize(void);

int WINAPI WinMain(HINSTANCE ssa_hInstance, HINSTANCE ssa_hPreInstance, LPSTR ssa_lpszCmdLine, int ssa_iCmdShow)
{
	int FindCenterCoordinate(int, int);
	void Display(void);

	WNDCLASSEX ssa_WndClassEx;
	HWND ssa_hWnd;
	MSG ssa_Msg;
	TCHAR ssa_szAppName[] = TEXT("My Application");

	bool ssa_bDone = false;


	if (fopen_s(&ssa_gFilePointer, "ssa.log", "w") != 0)
	{
		MessageBox(NULL, TEXT("ERROR: Not able to create file"), TEXT("My Message"), MB_ICONINFORMATION | MB_OK);
		exit(0);
	}
	else
	{
		fprintf(ssa_gFilePointer, "File Created Successfully ...\n");
		fprintf(ssa_gFilePointer, "Program Start...\n");
	}

	//Initialize Member
	ssa_WndClassEx.cbSize = sizeof(WNDCLASSEX);
	ssa_WndClassEx.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	ssa_WndClassEx.cbClsExtra = 0;
	ssa_WndClassEx.cbWndExtra = 0;
	ssa_WndClassEx.lpfnWndProc = WndProc;
	ssa_WndClassEx.hInstance = ssa_hInstance;
	ssa_WndClassEx.hIcon = LoadIcon(ssa_hInstance, NULL/*MAKEINTRESOURCE(MYICON)*/);
	ssa_WndClassEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	ssa_WndClassEx.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	ssa_WndClassEx.lpszClassName = ssa_szAppName;
	ssa_WndClassEx.lpszMenuName = NULL;
	ssa_WndClassEx.hIconSm = LoadIcon(ssa_hInstance, NULL/*MAKEINTRESOURCE(MYICON)*/);

	RegisterClassEx(&ssa_WndClassEx);


	int ssa_iScreenLength = GetSystemMetrics(SM_CXSCREEN);
	int ssa_iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int ssa_iX = FindCenterCoordinate(ssa_iScreenLength, 800);
	int ssa_iY = FindCenterCoordinate(ssa_iScreenHeight, 600);

	ssa_hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		ssa_szAppName,
		TEXT("My WIndow Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		ssa_iX,
		ssa_iY,
		800,
		600,
		NULL,
		NULL,
		ssa_hInstance,
		NULL
	);

	ssa_ghWnd = ssa_hWnd;

	Initialize();
	ShowWindow(ssa_hWnd, ssa_iCmdShow);
	SetForegroundWindow(ssa_hWnd);
	SetFocus(ssa_hWnd);

	//Game Loop
	while (ssa_bDone == false)
	{
		if (PeekMessage(&ssa_Msg, NULL, 0, 0, PM_REMOVE))
		{
			if (ssa_Msg.message == WM_QUIT)
			{
				ssa_bDone = true;
			}
			else
			{
				TranslateMessage(&ssa_Msg);
				DispatchMessage(&ssa_Msg);
			}
		}
		else
		{
			if (ssa_gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return (int)ssa_Msg.wParam;
}

LRESULT CALLBACK WndProc(HWND ssa_hWnd, UINT ssa_iMsg, WPARAM ssa_wParam, LPARAM ssa_lParam)
{
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);
	void Display(void);

	switch (ssa_iMsg)
	{
	case WM_CREATE:
		fprintf(ssa_gFilePointer, "\t !!! INDIA IS MY COUNTRY !!!\n");
		break;
		/*case WM_PAINT:
		Display();
		break;*/

	case WM_SETFOCUS:
		ssa_gbActiveWindow = true;
		break;
	case WM_ERASEBKGND:
		return (0);
	case WM_KEYDOWN:
		switch (ssa_wParam)
		{
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(ssa_hWnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		fprintf(ssa_gFilePointer, "\t !!! JAI HIND !!!\n");

		Uninitialize();
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return (DefWindowProc(ssa_hWnd, ssa_iMsg, ssa_wParam, ssa_lParam));
}

int FindCenterCoordinate(int ssa_iWindowCoordinate, int ssa_iScreenCoordinate)
{
	int ssa_iCoordinate = (ssa_iWindowCoordinate / 2) - (ssa_iScreenCoordinate / 2);

	return (ssa_iCoordinate);
}

void ToggleFullScreen(void)
{
	MONITORINFO ssa_MI = { sizeof(MONITORINFO) };

	if (ssa_gbFullScreen == false)
	{
		ssa_dwStyle = GetWindowLong(ssa_ghWnd, GWL_STYLE);

		if (ssa_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ssa_ghWnd, &ssa_wpPrev) &&
				(GetMonitorInfo(MonitorFromWindow(ssa_ghWnd, MONITORINFOF_PRIMARY), &ssa_MI))
				)
			{
				SetWindowLong(ssa_ghWnd, GWL_STYLE, (ssa_dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ssa_ghWnd,
					HWND_TOP,
					ssa_MI.rcMonitor.left,
					ssa_MI.rcMonitor.top,
					(ssa_MI.rcMonitor.right - ssa_MI.rcMonitor.left),
					(ssa_MI.rcMonitor.bottom - ssa_MI.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
		ssa_gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ssa_ghWnd, GWL_STYLE, (ssa_dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ssa_ghWnd, &ssa_wpPrev);

		SetWindowPos(ssa_ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED
		);

		ShowCursor(TRUE);
		ssa_gbFullScreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);

	PIXELFORMATDESCRIPTOR ssa_pfd;

	int iPixelFormatIndex;


	ssa_ghdc = GetDC(ssa_ghWnd);

	ZeroMemory(&ssa_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize pixel member
	ssa_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	ssa_pfd.nVersion = 1;
	ssa_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	ssa_pfd.iPixelType = PFD_TYPE_RGBA;
	ssa_pfd.cColorBits = 32;
	ssa_pfd.cRedBits = 8;
	ssa_pfd.cGreenBits = 8;
	ssa_pfd.cBlueBits = 8;
	ssa_pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ssa_ghdc, &ssa_pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(ssa_gFilePointer, "ChoosePixelFormat() failed \n");
		DestroyWindow(ssa_ghWnd);
	}

	if (SetPixelFormat(ssa_ghdc, iPixelFormatIndex, &ssa_pfd) == FALSE)
	{
		fprintf(ssa_gFilePointer, "SetPixelFormat() failed \n");
		DestroyWindow(ssa_ghWnd);
	}

	ssa_ghrc = wglCreateContext(ssa_ghdc);

	if (ssa_ghrc == NULL)
	{
		fprintf(ssa_gFilePointer, "wglCreateContext() failed \n");
		DestroyWindow(ssa_ghWnd);
	}

	if (wglMakeCurrent(ssa_ghdc, ssa_ghrc) == FALSE)
	{
		fprintf(ssa_gFilePointer, "wglMakeCurrent() failed \n");
		DestroyWindow(ssa_ghWnd);
	}

	//Set Clear Color
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int ssa_iWidth, int ssa_iHeight)
{
	if (ssa_iHeight == 0)
		ssa_iHeight = 1;

	glViewport(0, 0, (GLsizei)ssa_iWidth, (GLsizei)ssa_iHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	SwapBuffers(ssa_ghdc);
}

void Uninitialize(void)
{
	if (ssa_gbFullScreen == true)
	{
		ssa_dwStyle = GetWindowLong(ssa_ghWnd, GWL_STYLE);

		SetWindowLong(ssa_ghWnd, GWL_STYLE, (ssa_dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ssa_ghWnd, &ssa_wpPrev);

		SetWindowPos(ssa_ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
	}
	if (wglGetCurrentContext() == ssa_ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ssa_ghrc)
	{
		wglDeleteContext(ssa_ghrc);
		ssa_ghrc = NULL;
	}

	if (ssa_ghdc)
	{
		ReleaseDC(ssa_ghWnd, ssa_ghdc);
		ssa_ghdc = NULL;
	}

	if (ssa_gFilePointer)
	{
		fprintf(ssa_gFilePointer, "Cloaing .log file\n");
		fprintf(ssa_gFilePointer, "Code successfully ended...\n");

		fclose(ssa_gFilePointer);
		ssa_gFilePointer = NULL;
	}
}