#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"
#include "04_TextureUnits.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow;
bool gbIsFullScreen;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_TEXCOORD0,
  AMC_ATTRIBUTES_NORMAL
};

GLuint vao_rectangle;
GLuint vbo_rectangle;
GLuint vbo_texture;

GLuint texture_container;
GLuint texture_smiley;

GLuint samplerContainerUniform;
GLuint samplerSmileyUniform;

GLuint mvpUniform;
GLuint mixUniform;

float gMixTexture;

mat4 perspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("04_TextureUnits");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("04_TextureUnits"), MB_OK);
    exit(0);
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);
  fflush(gpFile);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("04_TextureUnits"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, 800, 600, NULL, NULL, hInstance, NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
            "Failed.\nExitting Now... "
            "Successfully\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
            "\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
            "Successful.\n",
            __FILE__, __LINE__, __FUNCTION__);
  }

  while (bDone == false) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {
    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case VK_UP:
            gMixTexture += 0.01f;
            break;

        case VK_DOWN:
            gMixTexture -= 0.01f;
            break;
      }
    } break;
  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen) {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}


BOOL LoadTexture(GLuint *texture, TCHAR imageResourceID[])
{
  // variable declarations
  HBITMAP hBitmap = NULL;
  BITMAP bmp;
  BOOL bStatus = FALSE;

  // code
  hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceID,
                               IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
  if (NULL == hBitmap)
  {
    return bStatus;
  }

  bStatus = TRUE;

  GetObject(hBitmap, sizeof(bmp), &bmp);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

  glGenTextures(1, texture);

  glBindTexture(GL_TEXTURE_2D, *texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  // glTexImages2D : Parameters from 2 to 9 Get Attach To Target GL_TEXTURE_2D.
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR,
                GL_UNSIGNED_BYTE, bmp.bmBits);

  glGenerateMipmap(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, 0);

  DeleteObject(hBitmap);

  hBitmap = NULL;

  return bStatus;

}

  int Initialize(void)
{
  // variable delcarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 8;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
          __FILE__, __LINE__, __FUNCTION__);
  fflush(gpFile);

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index)
  {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd))
  {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc)
  {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc))
  {
    return -4;
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

    GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject)
  {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

    return -6;
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
          __FILE__, __LINE__, __FUNCTION__);
  fflush(gpFile);

   const GLchar *vertexShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "in vec4 vPosition;" \
      "in vec2 vTexCoord;" \
      "uniform mat4 u_mvp_matrix;" \
      "out vec2 out_texcoord;" \
      "void main(void)" \
      "{" \
      "gl_Position = u_mvp_matrix * vPosition;" \
      "out_texcoord = vTexCoord;" \
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);

  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS,
  &iShaderCompileStatus); if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);
        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
        return -7;
      }
    }
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject)
  {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "in vec2 out_texcoord;" \

      "uniform float u_mix_matrix;" \

      "uniform sampler2D u_sampler_container;" \
      "uniform sampler2D u_sampler_smiley;" \
      "out vec4 FragColor;" \
      "void main(void)" \
      "{" \
      "FragColor = mix ( texture(u_sampler_container, out_texcoord), texture(u_sampler_smiley, out_texcoord), u_mix_matrix);" \
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);

  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);

  if (FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH,
  &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
        return -9;
      }
    }
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);
  fflush(gpFile);
  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                      "vPosition");
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0,
                       "vTexCoord");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus) {
    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                           szInfoLog);
        fprintf(gpFile,
                "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                "Failed:\n\t%s\n",
                __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
        return -10;
      }
    }
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");
  mixUniform = glGetUniformLocation(giShaderProgramObject, "u_mix_matrix");

  samplerContainerUniform = glGetUniformLocation(giShaderProgramObject, "u_sampler_container");
  samplerSmileyUniform = glGetUniformLocation(giShaderProgramObject, "u_sampler_smiley");
  // rectangle
  glGenVertexArrays(1, &vao_rectangle);

  glBindVertexArray(vao_rectangle);

  // rectangle-position
  const GLfloat rectangleVertices[] = {
      1.0f,  1.0f,  0.0f,
      -1.0f, 1.0f,  0.0f,
      -1.0f, -1.0f, 0.0f,
	  1.0f, -1.0f, 0.0f
  };

  glGenBuffers(1, &vbo_rectangle);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle);

  glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // rectangle-texture
  const GLfloat textureCoords[] = {
      1.0f, 1.0f,
      0.0f, 1.0f,
      0.0f, 0.0f,
	  1.0f, 0.0f
  };

  glGenBuffers(1, &vbo_texture);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);

  glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords), textureCoords,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  //

  glBindVertexArray(0);
  //
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  glClearDepth(1.0f);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  perspectiveProjectionMatrix = mat4::identity();

  LoadTexture(&texture_container, MAKEINTRESOURCEA(IDBITMAP_CONTAINER));
  LoadTexture(&texture_smiley, MAKEINTRESOURCEA(IDBITMAP_SMILEY));

  ReSize(800, 600);

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
          __FILE__, __LINE__, __FUNCTION__);
  return 0;
}


void ReSize(int width, int height)
{
  // code
  if (0 == height)
  {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  perspectiveProjectionMatrix =
      perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Display(void)
{
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(giShaderProgramObject);

  mat4 modelViewMatrix;
  mat4 modelViewProjectionMatrix;

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture_container);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, texture_smiley);

  glUniform1i(samplerContainerUniform, 0);
  glUniform1i(samplerSmileyUniform, 1);

  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();

  modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
  modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

  glUniform1f(mixUniform, gMixTexture);
  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

  glBindVertexArray(vao_rectangle);

  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  glBindVertexArray(0);

  glBindTexture(GL_TEXTURE_2D, 0);

  glUseProgram(0);

  SwapBuffers(gHdc);

  return;
}

void Uninitialize(void)
{
  // code

  glDeleteTextures(1, &texture_smiley);
  glDeleteTextures(1, &texture_container);

  if (vbo_texture)
  {
    glDeleteBuffers(1, &vbo_texture);
    vbo_texture = 0;
  }

  if (vbo_rectangle)
  {
    glDeleteBuffers(1, &vbo_rectangle);
    vbo_rectangle = 0;
  }

  if (vao_rectangle)
  {
    glDeleteVertexArrays(1, &vao_rectangle);
    vao_rectangle = 0;
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);
      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully ",
          __FILE__, __LINE__, __FUNCTION__);

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}
