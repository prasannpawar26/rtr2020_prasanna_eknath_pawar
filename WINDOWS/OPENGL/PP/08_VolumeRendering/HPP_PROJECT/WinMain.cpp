
#include <GL/glew.h>
#include <gl/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include <CL/opencl.h>
#include "helper_timer.h"

#include "vmath.h"

#include <ft2build.h>
#include FT_FREETYPE_H 

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "freetyped.lib")
#pragma comment(lib, "Winmm.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//
// Raw Image Data Processor Related Variables
//
int gSkullImageCount = 256;
int gSkullImageWidth = 256;
int gSkullImageHeight = 109;
int gSkullTexId;
int gSkullCurrentImageCount = 109;

//
// Buffer Related Variables
//
char *gHostLuminanceBuffer = NULL;
char *gHostRGBABuffer = NULL;

float gImageBufferConvertionTimeOnCPU;
float gImageBufferConvertionTimeOnGPU;

size_t gLuminanceBufferSize;
size_t gRGBABufferSize;

GLfloat gSkullOrthoSize = 1.0f;

/////////////////////////////////////////////////////////////////////////////////////////////////////

//
// OpenGL Context Related Variables
//
HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////////

//
// FullScreen Related Variables
//
DWORD gdwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbIsFullScreen = false;

/////////////////////////////////////////////////////////////////////////////////////////////////////

enum {
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXCOORD0
};

/////////////////////////////////////////////////////////////////////////////////////////////////////

#define NUM_CHARS 128

//
// Font Shader Related Variables
//
GLuint gFontVertexShaderObject;
GLuint gFontFragmentShaderObject;
GLuint gFontShaderProgramObject;

//
// UNIFORMS
//
GLuint gFontModelMatrixUniform;
GLuint gFontViewMatrixUniform;
GLuint gFontProjectionMatrixUniform;
GLuint gFontSamplerUniform;
GLuint gFontTextColorUniform;
GLuint gFontAlphaUniform;

//
// Matrices To Be Send To ShadersRenterText
//
mat4 gFontModelMatrix;
mat4 gFontViewMatrix;
mat4 gFontPerspectiveProjectionMatrix;

//
typedef struct _CHARACTER_ {
    GLuint TextureID;  // ID handle of the glyph texture
    vmath::uvec2 Size;         // Size of glyph
    vmath::uvec2 Bearing;      // Offset from baseline to left/top of glyph
    GLuint Advance;    // Horizontal offset to advance to next glyph
}CHARACTER;

CHARACTER gFontCharacter[NUM_CHARS];

GLuint gFontVao;
GLuint gFontVbo;

char RenterText[3][255];

float gFontColor[3] = {0.5f, 0.8f, 0.2f};
/////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// Skull-Shader Related Variables
//
GLuint gSkullVertexShaderObject;
GLuint gSkullFragmentShaderObject;
GLuint gSkullShaderProgramObject;

GLuint gSkullVao;
GLuint gSkullVboVertex;
GLuint gSkullVboTexture;
GLuint gSkullSamplerUniform;
GLuint gSkullMVPUniform;
GLuint gSkullTextureMatrixUniform;
GLuint gSkullAlphaUniform;

mat4 gSkullOrthoProjectionMatrix;

GLfloat skull_vertices[2400];
GLfloat skull_texcoords[2400];

/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// OpenCL Related Variables
//
size_t gLocalWorkSize = 256;
size_t gGlobalWorkSize;


cl_int gRetOcl;
cl_platform_id gOclPlatformID;
cl_device_id gOclComputeDeviceID;
cl_context gOclContext;
cl_command_queue gOclCommandQueue;
cl_program gOclProgram;
cl_kernel gOclKernel;

char *gOclSourceCode = NULL;
size_t gKernelCodeLength;

cl_mem gDeviceLuminanceBuffer = NULL;
cl_mem gDeviceRGBABuffer = NULL;

bool gbCurrentImageCountChange = true;

bool gbCPress = false;

int gScene = 0;
float gFontAlpha = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpszCmdLine, int iCmdShow)
{
    //
    // Function Declarations
    //
    int Initialize(void);
    void Display(void);
    void Update(void);

    //
    // Variable Declarations
    //
    bool bDone = false;
    MSG msg = {0};
    int iRet = 0;
    HWND hwnd;
    WNDCLASSEX wndClass;
    TCHAR szAppName[] = TEXT("HPP");

    //
    // Code
    //
    if (0 != fopen_s(&gpFile, "Log.txt", "w")) {
        MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
        exit(0);
    }

    fprintf(gpFile, "Log File Created Successfully\n");

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.cbWndExtra = 0;
    wndClass.cbClsExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szAppName;
    wndClass.lpfnWndProc = WndProc;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

    RegisterClassEx(&wndClass);

    hwnd = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName, TEXT("HPP"),
        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
        100, 800, 600, NULL, NULL, hInstance, NULL);

    gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    iRet = Initialize();
    if (0 != iRet) {
        fprintf(gpFile, "Initialization Failed\n");
        DestroyWindow(hwnd);
    } else {
        fprintf(gpFile, "Initialization Successful\n");
    }

    if (!PlaySound(L"Padoson.wav", NULL,
        SND_NODEFAULT | SND_ASYNC | SND_FILENAME | SND_LOOP)) {
        fprintf(gpFile, "PlaySound FAILED\n");
    }

    // GAME LOOP
    while (false == bDone) {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            if (WM_QUIT == msg.message) {
                bDone = true;
            } else {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        } else {
            if (gbActiveWindow) {
            }
            Update();
            Display();
        }
    }  // END OF WHILE

    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //
    // Function Declarations
    //
    void ToggledFullScreen(void);
    void UnInitialize(void);
    void ReSize(int, int);
    void ConvertDataToRGBA(void);
    void RunOpenCLKernel(void);
    void CopyImageDataToTextureMemory(int);

    //
    // Code
    //
    switch (iMsg) {
    case WM_DESTROY:
        UnInitialize();
        PostQuitMessage(0);
        break;

    case WM_ERASEBKGND:
        return 0;
        break;

    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;

    case WM_SIZE:
        ReSize(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_KILLFOCUS:
        gbActiveWindow = false;
        break;

    case WM_SETFOCUS:
        gbActiveWindow = true;
        break;

    case WM_KEYDOWN:
        switch (wParam) {
            case VK_ESCAPE:
                DestroyWindow(hwnd);
                break;

            case 'F':
            case 'f':
                //ToggledFullScreen();
                break;

            case VK_UP:
            {
                gSkullCurrentImageCount++;
                if (gSkullCurrentImageCount >= 109) {
                    gSkullCurrentImageCount = 109;
                }

                gLuminanceBufferSize = sizeof(char) * gSkullImageWidth * gSkullImageHeight * gSkullCurrentImageCount;
                gRGBABufferSize = gLuminanceBufferSize * 4;

                gbCurrentImageCountChange = true;
            }
            break;

            case VK_DOWN:
            {
                gSkullCurrentImageCount--;
                if (gSkullCurrentImageCount <= 0) {
                    gSkullCurrentImageCount = 0;
                }

                gLuminanceBufferSize = sizeof(char) * gSkullImageWidth * gSkullImageHeight * gSkullCurrentImageCount;
                gRGBABufferSize = gLuminanceBufferSize * 4;

                gbCurrentImageCountChange = true;
            }
            break;

            case 'S':
            case 's':
            {
                gScene += 1;

                if (5 == gScene)
                {
                    gbCPress = true;

                    for (int i = 0; i < 3; i++)
                    {
                        memset(RenterText[i], 0, 255);
                    }
                }
                else if (7 == gScene)
                {
                    gbCurrentImageCountChange = true;
                }
                else if (9 == gScene)
                {
                    gbCurrentImageCountChange = true;
                    gFontAlpha = 1.0f;
                }

                if ((9 != gScene) && gScene % 2 == 1)
                {
                    gFontAlpha = 0.0f;
                }

                gFontColor[0] = 0.5f;
                gFontColor[1] = 0.8f;
                gFontColor[2] = 0.2f;
            }
            break;
        }
        break;
    }  // End Of Switch Case

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
    //
    // Variable Declarations
    //
    MONITORINFO mi;

    //
    // Code
    //
    if (false == gbIsFullScreen) {
        gdwStyle = GetWindowLong(gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & gdwStyle) {
            mi = {sizeof(MONITORINFO)};

            if (GetWindowPlacement(gHwnd, &gWpPrev) &&
                GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
                SetWindowLong(gHwnd, GWL_STYLE, gdwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);
        gbIsFullScreen = true;
    } else {
        SetWindowLong(gHwnd, GWL_STYLE, gdwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(gHwnd, &gWpPrev);
        SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
            SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
            SWP_NOMOVE | SWP_NOSIZE);

        gbIsFullScreen = false;
        ShowCursor(TRUE);
    }

    return;
}

void ReadRawFile(void)
{
    //
    // Variable Declarations
    //
    FILE *pRawFile;
    errno_t err;

    //
    // Code
    //
    err = fopen_s(&pRawFile, "head256x256x109", "rb"); // binary read
    if (0  != err)
    {
        fprintf(gpFile, "fopen_s Failed Error : %d\n", err);
        return;
    }

    fread(gHostLuminanceBuffer, sizeof(char), gLuminanceBufferSize, pRawFile);

    fclose(pRawFile);

    return;
}

void ConvertDataToRGBA(void)
{
    //
    // Variable Declarations
    //

    StopWatchInterface *image_load_timer = NULL;

    //
    // Code
    //

    // Start Timer
    sdkCreateTimer(&image_load_timer);
    sdkStartTimer(&image_load_timer);

    // Convert the data to RGBA data.
    // Here we are simply putting the same value to R, G, B and A channels.
    // Usually for raw data, the alpha value will be constructed by a threshold
    // value given by the user
    for (int nIndx = 0;
        nIndx < gLuminanceBufferSize;
        ++nIndx) {
        gHostRGBABuffer[nIndx * 4] = gHostLuminanceBuffer[nIndx];
        gHostRGBABuffer[nIndx * 4 + 1] = gHostLuminanceBuffer[nIndx];
        gHostRGBABuffer[nIndx * 4 + 2] = gHostLuminanceBuffer[nIndx];
        gHostRGBABuffer[nIndx * 4 + 3] = gHostLuminanceBuffer[nIndx];
    }

    // Stop Timer
    sdkStopTimer(&image_load_timer);
    gImageBufferConvertionTimeOnCPU = sdkGetTimerValue(&image_load_timer);
    sdkDeleteTimer(&image_load_timer);

    fprintf(gpFile,"ImageBuffer Convertion Time On CPU: %f ms\n",gImageBufferConvertionTimeOnCPU);

    return;
}

void CopyImageDataToTextureMemory(int iImageCount)
{
    //
    // Code
    //
    glBindTexture(GL_TEXTURE_3D, gSkullTexId);

    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, gSkullImageWidth, gSkullImageHeight,
        iImageCount, 0, GL_RGBA, GL_UNSIGNED_BYTE, gHostRGBABuffer);

    glBindTexture(GL_TEXTURE_3D, 0);

    return;
}

int Initialize(void)
{
    //
    // Function Declarations
    //
    int InitializeSkull(void);
    int InitializeFont(void);
    int AllocateSkullImageBuffer(void);
    void ToggledFullScreen(void);
    void ReSize(int, int);
    void Load3DTexture(void);
    void ReadRawFile(void);
    void SetupOpenCLKernel(void);

    //
    // Variable Declarations
    //
    int index;
    PIXELFORMATDESCRIPTOR pfd;

    //
    // Code
    //

    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 8;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

    fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
        __FILE__, __LINE__, __FUNCTION__);

    gHdc = GetDC(gHwnd);

    index = ChoosePixelFormat(gHdc, &pfd);
    if (0 == index) {
        fprintf(gpFile, "Choose Pixel Format Failed\n");
        return -1;
    }

    if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
        fprintf(gpFile, "Set Pixel Format Failed\n");
        return -2;
    }

    gHglrc = wglCreateContext(gHdc);
    if (NULL == gHglrc) {
        fprintf(gpFile, "wglCreateContext Failed\n");
        return -3;
    }

    if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
        fprintf(gpFile, "wglMakeCurrent Failed\n");
        return -4;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
        __FILE__, __LINE__, __FUNCTION__);

    GLenum result;

    result = glewInit();
    if (GLEW_OK != result) {
        fprintf(gpFile, "glewInit Failed\n");
        return -5;
    }
    if (GL_TRUE != glewGetExtension("GL_EXT_texture3D")) {
        fprintf(gpFile, "glewGetExtension Failed\n");
        return -6;
    }

    InitializeSkull();

    // Load Texture
    Load3DTexture();

    InitializeFont();

    // Allocate Buffers
    AllocateSkullImageBuffer();

    ReadRawFile();

    SetupOpenCLKernel();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glClearDepth(1.0f);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_TEXTURE_3D);

    gSkullOrthoProjectionMatrix = mat4::identity();

    //ReSize(800, 600);
    ToggledFullScreen();
    return 0;
}

void UnInitialize(void)
{
    // 
    // Function Declarations
    //
    void Cleanup(void);
    void UninitializeFont(void);
    void UninitializeSkull(void);

    //
    // Code
    //
    UninitializeFont();

    UninitializeSkull();
    
    Cleanup();

    // If not initialized, then this will be zero. So no checking is needed.
    if (0 != gSkullTexId) {
        glDeleteTextures(1, (GLuint *)&gSkullTexId);
    }

    if (wglGetCurrentContext() == gHglrc) {
        wglMakeCurrent(NULL, NULL);
    }

    if (NULL != gHglrc) {
        wglDeleteContext(gHglrc);
    }

    if (NULL != gHdc) {
        ReleaseDC(gHwnd, gHdc);
    }

    fprintf(gpFile, "Log File Closed Successfully\n");

    if (NULL != gpFile) {
        fclose(gpFile);
    }

    return;
}

void ReSize(int width, int height)
{
    //
    // Code
    //
    if (0 == height) {
        height = 1;
    }
    GLdouble AspectRatio = (GLdouble)(width) / (GLdouble)(height);

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    // Set the orthographic projection.
    if (width <= height) {
        gSkullOrthoProjectionMatrix = ortho(
            -gSkullOrthoSize, gSkullOrthoSize, -(gSkullOrthoSize / AspectRatio), gSkullOrthoSize / AspectRatio,
            2.0f * -gSkullOrthoSize, 2.0f * gSkullOrthoSize);
    } else {
        gSkullOrthoProjectionMatrix =
            ortho(-gSkullOrthoSize * AspectRatio, gSkullOrthoSize * AspectRatio,
                -gSkullOrthoSize, gSkullOrthoSize, 2.0f * -gSkullOrthoSize,
                2.0f * gSkullOrthoSize);
    }

    gFontPerspectiveProjectionMatrix =
        perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);

    return;
}

void SkullDisplay(float alpha)
{
    //
    // Code
    //
    glUseProgram(gSkullShaderProgramObject);

    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.05f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    mat4 skull_modelViewMatrix;
    mat4 skull_modelViewProjectionMatrix;
    mat4 skull_rotationMatrix;
    mat4 skull_textureMatrix;

    skull_modelViewMatrix = mat4::identity();
    skull_modelViewProjectionMatrix = mat4::identity();
    skull_rotationMatrix = mat4::identity();
    skull_textureMatrix = mat4::identity();

    skull_rotationMatrix = rotate(180.0f, 0.0f, 0.0f, 1.0f);

    skull_modelViewMatrix = skull_rotationMatrix;
    skull_modelViewProjectionMatrix =
        gSkullOrthoProjectionMatrix * skull_modelViewMatrix;

    glUniformMatrix4fv(gSkullMVPUniform, 1, GL_FALSE,
        skull_modelViewProjectionMatrix);
    glUniformMatrix4fv(gSkullTextureMatrixUniform, 1, GL_FALSE,
        skull_textureMatrix);
    glUniform1f(gSkullAlphaUniform, alpha);

    glEnable(GL_TEXTURE_3D);
    glBindTexture(GL_TEXTURE_3D, gSkullTexId);
    glBindVertexArray(gSkullVao);
    glBindBuffer(GL_ARRAY_BUFFER, gSkullVboVertex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skull_vertices), skull_vertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, gSkullVboTexture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skull_texcoords), skull_texcoords,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    for (int skull_i = 0; skull_i < 200; skull_i++) {
        glDrawArrays(GL_TRIANGLE_FAN, skull_i * 4, 4);
    }

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_3D, 0);

    glUseProgram(0);

    return;
}

void FontDisplay(const char *text, float positionX, float positionY, float positionZ, float alpha) 
{
    //
    //Code
    //
    GLfloat x = 0.0f;
    GLfloat y = 0.0f;
    GLfloat scale = 0.015f;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glActiveTexture(GL_TEXTURE0);
    glUseProgram(gFontShaderProgramObject);

    gFontModelMatrix = mat4::identity();
    gFontViewMatrix = mat4::identity();

    gFontModelMatrix = gFontModelMatrix * translate(positionX, positionY, positionZ);

    glUniformMatrix4fv(gFontModelMatrixUniform, 1, GL_FALSE, gFontModelMatrix);
    glUniformMatrix4fv(gFontViewMatrixUniform, 1, GL_FALSE, gFontViewMatrix);
    glUniformMatrix4fv(gFontProjectionMatrixUniform, 1, GL_FALSE,
        gFontPerspectiveProjectionMatrix);

    glUniform1i(gFontSamplerUniform, 0);
    glUniform3f(gFontTextColorUniform, gFontColor[0], gFontColor[1], gFontColor[2]);
    glUniform1f(gFontAlphaUniform, alpha);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(gFontVao);

    for (unsigned int i = 0; i < strlen(text); i++)
    {
        char c = text[i];
        CHARACTER fc = gFontCharacter[c];

        GLfloat xpos = x + fc.Bearing[0] * scale;
        GLfloat ypos = y - (fc.Size[1] - fc.Bearing[1]) * scale;

        GLfloat w = fc.Size[0] * scale;
        GLfloat h = fc.Size[1] * scale;

        GLfloat vertices[6][4] =
        {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };

        glBindTexture(GL_TEXTURE_2D, fc.TextureID);

        glBindBuffer(GL_ARRAY_BUFFER, gFontVbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices),vertices); 
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        glBindTexture(GL_TEXTURE_2D, 0);

        x += (fc.Advance >> 6) * scale;
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
    glDisable(GL_BLEND);

    glUseProgram(0);

}

void Display(void)
{
    //
    // Function Declarations
    //
    void SkullDisplay(float);
    void FontDisplay(const char *, float, float, float, float);
    void RunOpenCLKernel(void);
    void CopyImageDataToTextureMemory(int);

    //
    // Code
    //
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (0 == gScene)
    {

    }
    else if (1 == gScene || 2 == gScene)
    {
        float yT = 1.0f;
        float xT = -3.250f;
        float zT = -8.0f;
        FontDisplay("ASTROMEDiCOMP", xT, yT, zT, gFontAlpha);

        yT = 0.0f;
        xT = -3.15f;
        zT = -9.0f;
        FontDisplay("(MATRIX GROUP)", xT, yT, zT, gFontAlpha);

        yT = -1.0f;
        xT = -2.0f;
        zT = -9.0f;
        FontDisplay("PRESENTS", xT, yT, zT, gFontAlpha);

        if (1 == gScene)
        {
            gFontAlpha += 0.005f;
            if (gFontAlpha > 1.0f)
            {
                gFontAlpha = 1.0f;
            }
        }
        else if (2 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
        
    }
    else if (3 == gScene || 4 == gScene)
    {
        float yT = 0.0f;
        float xT = -5.50f;
        float zT = -10.0f;
        FontDisplay("HPP FOR MEDICAL IMAGING", xT, yT, zT, gFontAlpha);

        if (3 == gScene)
        {
            gFontAlpha += 0.005f;
            if (gFontAlpha > 1.0f)
            {
                gFontAlpha = 1.0f;
            }
        }
        else if (4 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
    }
    else if (5 == gScene)
    {
        if (true == gbCurrentImageCountChange)
        {
            ConvertDataToRGBA();
        }
        gbCurrentImageCountChange = false;

        gFontColor[0] = 1.0f;
        gFontColor[1] = 0.0f;
        gFontColor[2] = 0.0f;

        sprintf(RenterText[0], "Calculation:              256 X 256 X %d", gSkullCurrentImageCount);
        sprintf(RenterText[1], "Conversion Time:    %f ms", gImageBufferConvertionTimeOnCPU);

        CopyImageDataToTextureMemory(gSkullCurrentImageCount);

        float yT = 3.750f;
        float xT = -1.75f;
        float zT = -11.0f;

        FontDisplay("ON CPU", xT, yT, zT, gFontAlpha);

        gFontAlpha += 0.005f;
        if (gFontAlpha > 1.0f)
        {
            gFontAlpha = 1.0f;
        }

        SkullDisplay(gFontAlpha);

        if (gbCPress)
        {
            float yT = 11.0f;
            float xT = 10.50f;
            float zT = -31.0f;

            for (int index = 0; index < 3; index++)
            {
                FontDisplay(RenterText[index], xT, yT, zT, gFontAlpha);
                yT -= 1.0f;
            }
        }
    }

    else if (6 == gScene)
    {
        gFontColor[0] = 1.0f;
        gFontColor[1] = 0.0f;
        gFontColor[2] = 0.0f;

        sprintf(RenterText[0], "Calculation:              256 X 256 X %d", gSkullCurrentImageCount);
        sprintf(RenterText[1], "Conversion Time:    %f ms", gImageBufferConvertionTimeOnCPU);

        CopyImageDataToTextureMemory(gSkullCurrentImageCount);

        float yT = 8.0f;
        float xT = -9.0f;
        float zT = -24.0f;

        FontDisplay("PRESS KEY TO SWITCH FROM CPU TO GPU ", xT, yT, zT, 1.0f);

        SkullDisplay(1.0f);

        if (gbCPress)
        {
            float yT = 11.0f;
            float xT = 10.50f;
            float zT = -31.0f;

            for (int index = 0; index < 3; index++)
            {
                FontDisplay(RenterText[index], xT, yT, zT, 1.0f);
                yT -= 1.0f;
            }
        }
    }
    else if (7 == gScene)
    {
        if (true == gbCurrentImageCountChange)
        {
            RunOpenCLKernel();
        }
        gbCurrentImageCountChange = false;

        gFontColor[0] = 0.5f;
        gFontColor[1] = 0.8f;
        gFontColor[2] = 0.2f;

        sprintf(RenterText[0], "Calculation:              256 X 256 X %d", gSkullCurrentImageCount);
        sprintf(RenterText[1], "Conversion Time:    %f ms", gImageBufferConvertionTimeOnGPU);

        CopyImageDataToTextureMemory(gSkullCurrentImageCount);

        float yT = 3.750f;
        float xT = -1.75f;
        float zT = -11.0f;

        FontDisplay("ON GPU", xT, yT, zT, 1.0f);

        SkullDisplay(1.0f);

        if (gbCPress)
        {
            float yT = 11.0f;
            float xT = 10.50f;
            float zT = -31.0f;

            for (int index = 0; index < 3; index++)
            {
                FontDisplay(RenterText[index], xT, yT, zT, 1.0f);
                yT -= 1.0f;
            }
        }
    }
    else if (8 == gScene)
    {
        if (true == gbCurrentImageCountChange)
        {
            RunOpenCLKernel();
        }
        gbCurrentImageCountChange = false;

        gFontColor[0] = 0.5f;
        gFontColor[1] = 0.8f;
        gFontColor[2] = 0.2f;

        sprintf(RenterText[0], "Calculation:              256 X 256 X %d", gSkullCurrentImageCount);
        sprintf(RenterText[1], "Conversion Time:    %f ms", gImageBufferConvertionTimeOnGPU);

        CopyImageDataToTextureMemory(gSkullCurrentImageCount);

        float yT = 8.0f;
        float xT = -9.0f;
        float zT = -24.0f;

        FontDisplay("PRESS KEY TO SWITCH FROM GPU TO CPU ", xT, yT, zT, 1.0f);

        SkullDisplay(1.0f);

        if (gbCPress)
        {
            float yT = 11.0f;
            float xT = 10.50f;
            float zT = -31.0f;

            for (int index = 0; index < 3; index++)
            {
                FontDisplay(RenterText[index], xT, yT, zT, 1.0f);
                yT -= 1.0f;
            }
        }
    }
    else if (9 == gScene || 10 == gScene)
    {
        if (true == gbCurrentImageCountChange)
        {
            ConvertDataToRGBA();
        }
        gbCurrentImageCountChange = false;

        gFontColor[0] = 1.0f;
        gFontColor[1] = 0.0f;
        gFontColor[2] = 0.0f;

        sprintf(RenterText[0], "Calculation:              256 X 256 X %d", gSkullCurrentImageCount);
        sprintf(RenterText[1], "Conversion Time:    %f ms", gImageBufferConvertionTimeOnCPU);

        CopyImageDataToTextureMemory(gSkullCurrentImageCount);

        float yT = 3.750f;
        float xT = -1.75f;
        float zT = -11.0f;

        FontDisplay("ON CPU", xT, yT, zT, gFontAlpha);

        SkullDisplay(gFontAlpha);

        if (gbCPress)
        {
            float yT = 11.0f;
            float xT = 10.50f;
            float zT = -31.0f;

            for (int index = 0; index < 3; index++)
            {
                FontDisplay(RenterText[index], xT, yT, zT, gFontAlpha);
                yT -= 1.0f;
            }
        }

        if (10 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
    }
    else if (11 == gScene || 12 == gScene)
    {

        float yT = 0.750f;
        float xT = -2.5f;
        float zT = -11.0f;
        FontDisplay("POWERED BY", xT, yT, zT, gFontAlpha);

        yT = -0.750f;
        xT = -3.350f;
        zT = -11.0f;
        FontDisplay("ASTROMEDiCOMP", xT, yT, zT, gFontAlpha);

        if (11 == gScene)
        {
            gFontAlpha += 0.005f;
            if (gFontAlpha > 1.0f)
            {
                gFontAlpha = 1.0f;
            }
        }
        else if (12 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
    }

    else if (13 == gScene || 14 == gScene)
    {

        float yT = 4.250f;
        float xT = -3.5f;
        float zT = -13.0f;
        FontDisplay("TECHNOLOGY USED", xT, yT, zT, gFontAlpha);

        yT = 4.0f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("HPP:                      OPENCL", xT, yT, zT, gFontAlpha);

        yT = 3.0f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("RENDERING:      OPENGL", xT, yT, zT, gFontAlpha);

        yT = 2.0f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("LANGUAGE:        C", xT, yT, zT, gFontAlpha);

        yT = 1.0f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("OS:                         WINDOWS 10", xT, yT, zT, gFontAlpha);

        yT = -0.750f;
        xT = -3.5f;
        zT = -13.0f;
        FontDisplay("BACKGROUND MUSIC", xT, yT, zT, gFontAlpha);

        yT = -2.250f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("MOVIE:                PADOSAN", xT, yT, zT, gFontAlpha);

        yT = -3.250f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("MUSIC BY:          R. D. BURMAN", xT, yT, zT, gFontAlpha);

        yT = -4.250f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("                                            &", xT, yT, zT, gFontAlpha);

        yT = -5.250f;
        xT = -3.750f;
        zT = -17.0f;
        FontDisplay("                              KISHORE KUMAR", xT, yT, zT, gFontAlpha);

        if (13 == gScene)
        {
            gFontAlpha += 0.005f;
            if (gFontAlpha > 1.0f)
            {
                gFontAlpha = 1.0f;
            }
        }
        else if (14 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
    }

    else if (15 == gScene || 16 == gScene)
    {

        float yT = 0.750f;
        float xT = -3.25f;
        float zT = -13.0f;
        FontDisplay("IGNITED BY GURU", xT, yT, zT, gFontAlpha);

        yT = -0.750f;
        xT = -4.250f;
        zT = -11.0f;
        FontDisplay("DR. VIJAY D. GOKHALE", xT, yT, zT, gFontAlpha);

        if (15 == gScene)
        {
            gFontAlpha += 0.005f;
            if (gFontAlpha > 1.0f)
            {
                gFontAlpha = 1.0f;
            }
        }
        else if (16 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
    }
    else if (17 == gScene || 18 == gScene)
    {
        float yT = 0.0f;
        float xT = -2.0f;
        float zT = -11.0f;
        FontDisplay("THANK YOU", xT, yT, zT, gFontAlpha);

        if (17 == gScene)
        {
            gFontAlpha += 0.005f;
            if (gFontAlpha > 1.0f)
            {
                gFontAlpha = 1.0f;
            }
        }
        else if (18 == gScene)
        {
            gFontAlpha -= 0.005f;
            if (gFontAlpha < 0.0f)
            {
                gFontAlpha = 0.0f;
            }
        }
    }

    SwapBuffers(gHdc);

    return;
}

void Update(void)
{
    //
    // Code
    //

}

void Load3DTexture(void)
{
    //
    // Code
    //
    glGenTextures(1, (GLuint *)&gSkullTexId);

    glBindTexture(GL_TEXTURE_3D, gSkullTexId);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    /*
    m_uImageCount => Specifies the depth/slices of the texture image or the
    number of layers in a texture array. All implementations support 3D texture
    images that are at least 256 texels deep and texture arrays that are at
    least 256 layers deep.
    */
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, 256, 256, gSkullCurrentImageCount, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, NULL);

    glBindTexture(GL_TEXTURE_3D, 0);

    return;
}

void Cleanup(void)
{
    //
    // Code
    //

    if (NULL != gOclSourceCode) {
        free((void *)gOclSourceCode);
        gOclSourceCode = NULL;
    }

    if (NULL != gOclKernel) {
        clReleaseKernel(gOclKernel);
        gOclKernel = NULL;
    }

    if (NULL != gOclProgram) {
        clReleaseProgram(gOclProgram);
        gOclProgram = NULL;
    }

    if (NULL != gOclCommandQueue) {
        clReleaseCommandQueue(gOclCommandQueue);
        gOclCommandQueue = NULL;
    }

    if (NULL != gOclContext) {
        clReleaseContext(gOclContext);
        gOclContext = NULL;
    }

    if (NULL != gDeviceLuminanceBuffer) {
        clReleaseMemObject(gDeviceLuminanceBuffer);
        gDeviceLuminanceBuffer = NULL;
    }

    if (NULL != gDeviceRGBABuffer) {
        clReleaseMemObject(gDeviceRGBABuffer);
        gDeviceRGBABuffer = NULL;
    }

    // Free Allocated Host-Memory
    if (NULL != gHostLuminanceBuffer) {
        free(gHostLuminanceBuffer);
        gHostLuminanceBuffer = NULL;
    }

    if (NULL != gHostRGBABuffer) {
        free(gHostRGBABuffer);
        gHostRGBABuffer = NULL;
    }
}

size_t RoundGlobalSizeToNearestMultipleOfLocalSize(size_t local_size,
    size_t global_size)
{
    //
    // Code
    //
    unsigned int r = global_size % local_size;

    if (0 == r) {
        return (global_size);
    } else {
        return (global_size + local_size - r);
    }
}

char* LoadOclProgramSource(const char *filename, const char *preamble,
    size_t *sizeFinalLength)
{
    // Variable Declarations
    FILE *pFile = NULL;
    size_t sizeSourceLength;

    //
    // Code
    //
    pFile = fopen(filename, "rb"); // binary read
    if (NULL == pFile) {
        return NULL;
    }

    size_t sizePreambleLength = (size_t)strlen(preamble);

    // Get The Length Of The Soruce Code
    fseek(pFile, 0, SEEK_END);
    sizeSourceLength = ftell(pFile);
    fseek(pFile, 0, SEEK_SET); // Rest To Beginning

                               // Allocate A BUffer For The Source Code String An Read It In
    char *sourceString =
        (char *)malloc(sizeSourceLength + sizePreambleLength + 1);
    memcpy(sourceString, preamble, sizePreambleLength);

    if (fread((sourceString) + sizePreambleLength, sizeSourceLength, 1, pFile) !=
        1) {
        fclose(pFile);
        free(sourceString);
        return (0);
    }

    // Close The File And Return The Total Length Of The Combined (Preamble +
    // Source) String
    fclose(pFile);
    if (0 != sizeFinalLength) {
        *sizeFinalLength = sizeSourceLength + sizePreambleLength;
    }

    sourceString[sizeSourceLength + sizePreambleLength] = '\0';

    return (sourceString);
}

void SetupOpenCLKernel(void)
{
    //
    // Code
    //

    // Get OpenCL Supporting Platform's ID
    gRetOcl = clGetPlatformIDs(1, &gOclPlatformID, NULL);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile, "OpenCL Error - clGetPlatformIDs Failed : %d. Exitting Now... \n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Get OpenCL Supporting GPU Device's ID
    gRetOcl = clGetDeviceIDs(gOclPlatformID, CL_DEVICE_TYPE_GPU, 1,
        &gOclComputeDeviceID, NULL);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clGetDeviceIDs() Failed : %d. Exitting Now...\n", gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    char gpu_name[255];
    clGetDeviceInfo(gOclComputeDeviceID, CL_DEVICE_NAME, sizeof(gpu_name),
        &gpu_name, NULL);
    fprintf(gpFile,"gpu_name: %s\n", gpu_name);

    // Create OpenCL Compute Context
    gOclContext =
        clCreateContext(NULL, 1, &gOclComputeDeviceID, NULL, NULL, &gRetOcl);
    if (CL_SUCCESS != gRetOcl) {
        printf("OpenCL Error - clCreateContext() Failed : %d. Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Allocate Device-Memory
    gDeviceLuminanceBuffer =
        clCreateBuffer(gOclContext, CL_MEM_READ_ONLY, gLuminanceBufferSize, NULL, &gRetOcl);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clCreateBuffer() Failed  For device buffer: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    gDeviceRGBABuffer =
        clCreateBuffer(gOclContext, CL_MEM_READ_ONLY, gRGBABufferSize, NULL, &gRetOcl);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clCreateBuffer() Failed  For device RGBA buffer: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Create Command Queue
    gOclCommandQueue =
        clCreateCommandQueue(gOclContext, gOclComputeDeviceID, 0, &gRetOcl);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clCreateCommandQueue() Failed : %d. Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Create OpenCL Program From .cl
    gOclSourceCode = LoadOclProgramSource("image.cl", "", &gKernelCodeLength);

    gOclProgram =
        clCreateProgramWithSource(gOclContext, 1, (const char **)&gOclSourceCode,
            &gKernelCodeLength, &gRetOcl);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,
            "OpenCL Error - clCreateProgramWithSource() Failed : %d. Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(0);
    }

    // Build OpenCL Program
    gRetOcl = clBuildProgram(gOclProgram, 0, NULL, NULL, NULL, NULL);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCl Error - clBuildProgram() Failed : %d. Exitting Now...\n",
            gRetOcl);

        size_t len;
        char buffer[2048];
        clGetProgramBuildInfo(gOclProgram, gOclComputeDeviceID, CL_PROGRAM_BUILD_LOG,
            sizeof(buffer), buffer, &len);
        fprintf(gpFile,"OpenCL Program Build Log : %s\n", buffer);

        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Create OpenCL Kernel By Passing Kernel Function Name That We Used In .cl
    // File
    gOclKernel = clCreateKernel(gOclProgram, "ImageLoad", &gRetOcl);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clCreateKernel() Failed : %d. Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Set OpenCL kernel Argument. Our OpenCL Kernel Has 4 Arguments 0,1,2,3
    // Set 0 Based 0th Argument
    gRetOcl = clSetKernelArg(gOclKernel, 0, sizeof(cl_mem), (void *)&gDeviceLuminanceBuffer);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 1st Argument: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Set 0 Based 2nd Argument i.e len
    // iNumberOfArrayElements maps To len Param Of kernel Function in .cl File
    gRetOcl = clSetKernelArg(gOclKernel, 1, sizeof(cl_int),
        (void *)&gLuminanceBufferSize);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 3rd Argument: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Set 0 Based 3rd Argument
    gRetOcl = clSetKernelArg(gOclKernel, 2, sizeof(cl_mem), (void *)&gDeviceRGBABuffer);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 2nd Argument: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Set 0 Based 4th Argument
    gRetOcl = clSetKernelArg(gOclKernel, 3, sizeof(cl_int),
        (void *)&gRGBABufferSize);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 3rd Argument: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Write Above 'input' cpu Buffer To Device Memory
    gRetOcl = clEnqueueWriteBuffer(gOclCommandQueue, gDeviceLuminanceBuffer, CL_FALSE, 0,
        gLuminanceBufferSize, gHostLuminanceBuffer, 0, NULL, NULL);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clEnqueueWriteBuffer() Failed  For 1st Input "
            "Buffer: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    return;
}

void RunOpenCLKernel(void)
{
    //
    // Code
    //

    // Run The kernel
    gGlobalWorkSize = RoundGlobalSizeToNearestMultipleOfLocalSize(
        gLocalWorkSize, gLuminanceBufferSize);

    // Start Timer
    StopWatchInterface *timer = NULL;
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);

    gRetOcl =
        clEnqueueNDRangeKernel(gOclCommandQueue, gOclKernel, 1, NULL,
            &gGlobalWorkSize, &gLocalWorkSize, 0, NULL, NULL);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clEnqueueNDRangeKernel() Failed: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    // Finish OpenCL Command Queue
    clFinish(gOclCommandQueue);

    // Stop Timer
    sdkStopTimer(&timer);
    gImageBufferConvertionTimeOnGPU = sdkGetTimerValue(&timer);
    sdkDeleteTimer(&timer);

    fprintf(gpFile,"ImageBuffer Convertion Time On GPU : %f ms.\n",gImageBufferConvertionTimeOnGPU);

    // Read Back Result From The Device (i.e. From gDeviceRGBABuffer) Into CPU
    // Variable(i.e gHostRGBABuffer)
    gRetOcl = clEnqueueReadBuffer(gOclCommandQueue, gDeviceRGBABuffer, CL_TRUE, 0, gRGBABufferSize,
        gHostRGBABuffer, 0, NULL, NULL);
    if (CL_SUCCESS != gRetOcl) {
        fprintf(gpFile,"OpenCL Error - clEnqueueReadBuffer() Failed  For 2nd Input "
            "Buffer: %d. "
            "Exitting Now...\n",
            gRetOcl);
        Cleanup();
        exit(EXIT_FAILURE);
    }

    return;
}

int LoadFreeType(void)
{
    //
    // Code
    //
    FT_Library fontLibrary;
    if (FT_Init_FreeType(&fontLibrary))
    {
        fprintf(gpFile, "ERROR::FREETYPE: Could not init FreeType Library\n");
        return -1;
    }

    FT_Face face;
    if (FT_New_Face(fontLibrary, "./Fonts_Files/timesbd.ttf", 0, &face))
    /*if (FT_New_Face(fontLibrary, "./Fonts_Files/Cinzel-Regular.ttf", 0, &face))*/
    {
        fprintf(gpFile, "ERROR::FREETYPE: Failed to load font\n");
        FT_Done_FreeType(fontLibrary);
        return -1;
    }

    FT_Set_Pixel_Sizes(face, 0, 50);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); 

    // load first 128 characters of ASCII set
    for (unsigned int c = 0; c < NUM_CHARS; c++)
    {
        // Load character glyph
        if (FT_Load_Char(face, c, FT_LOAD_RENDER))
        {
            fprintf(gpFile, "ERROR::FREETYTPE: Failed to load Glyph\n");
            continue;
        }

        // Generate Texture
        GLuint char_texture;
        glGenTextures(1, &char_texture);

        glBindTexture(GL_TEXTURE_2D, char_texture);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width,
            face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        gFontCharacter[c].TextureID = char_texture;
        gFontCharacter[c].Size = vmath::uvec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);
        gFontCharacter[c].Bearing =	vmath::uvec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
        gFontCharacter[c].Advance = face->glyph->advance.x;

    } // End Of For Loop
    glBindTexture(GL_TEXTURE_2D, 0);
    FT_Done_Face(face);
    FT_Done_FreeType(fontLibrary);

    return 0;
}

int InitializeFont(void)
{
    //
    // Function Declarations
    //
    int LoadFreeType(void);

    //
    // Code
    //
    LoadFreeType();

    const GLchar *vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "out vec2 out_textureCoord;" \

        "uniform mat4 model_matrix;" \
        "uniform mat4 view_matrix;" \
        "uniform mat4 projection_matrix;" \

        "void main(void)" \
        "{" \
        "gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vPosition.xy, 0.0, 1.0);" \
        "out_textureCoord = vPosition.zw;" \
        "}";

    const GLchar *fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_textureCoord;" \
        "out vec4 FragColor;" \

        "uniform sampler2D texture0_sampler_u;" \
        "uniform vec3 textColor_u;" \
        "uniform float u_alpha;"

        "void main(void)" \
        "{" \
        "vec4 sampled = vec4(1.0,1.0,1.0,texture(texture0_sampler_u, out_textureCoord).r);" \
        "FragColor = vec4(textColor_u,1.0)* sampled * vec4(u_alpha);" \
        "}";

    gFontVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == gFontVertexShaderObject) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

        return -1;
    }

    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader glCreateShader "
        "Successful\n",
        __FILE__, __LINE__, __FUNCTION__);

    glShaderSource(gFontVertexShaderObject, 1,
        (const GLchar **)&vertexShaderSourceCode, NULL);
    glCompileShader(gFontVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gFontVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

        glGetShaderiv(gFontVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;

                glGetShaderInfoLog(gFontVertexShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(
                    gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -2;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    gFontFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == gFontFragmentShaderObject) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        return -3;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader glCreateShader "
        "Successful\n",
        __FILE__, __LINE__, __FUNCTION__);

    glShaderSource(gFontFragmentShaderObject, 1,
        (const GLchar **)&fragmentShaderSourceCode, NULL);
    glCompileShader(gFontFragmentShaderObject);

    glGetShaderiv(gFontFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (FALSE == iShaderCompileStatus) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
        glGetShaderiv(gFontFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;

                glGetShaderInfoLog(gFontFragmentShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(
                    gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -4;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    gFontShaderProgramObject = glCreateProgram();

    glAttachShader(gFontShaderProgramObject, gFontVertexShaderObject);
    glAttachShader(gFontShaderProgramObject, gFontFragmentShaderObject);

    // Attributes Binding Before Linking
    glBindAttribLocation(gFontShaderProgramObject, AMC_ATTRIBUTES_POSITION,
        "vPosition");

    glLinkProgram(gFontShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gFontShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus) {
        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

        glGetProgramiv(gFontShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;

                glGetProgramInfoLog(gFontShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -5;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    // Uniform Binding After Linking
    gFontModelMatrixUniform =
        glGetUniformLocation(gFontShaderProgramObject, "model_matrix");
    gFontViewMatrixUniform =
        glGetUniformLocation(gFontShaderProgramObject, "view_matrix");
    gFontProjectionMatrixUniform =
        glGetUniformLocation(gFontShaderProgramObject, "projection_matrix");

    gFontSamplerUniform =
        glGetUniformLocation(gFontShaderProgramObject, "texture0_sampler_u");
    gFontTextColorUniform =
        glGetUniformLocation(gFontShaderProgramObject, "textColor_u");

    gFontAlphaUniform =
        glGetUniformLocation(gFontShaderProgramObject, "u_alpha");

    glGenVertexArrays(1, &gFontVao);
    glBindVertexArray(gFontVao);
    glGenBuffers(1, &gFontVbo);
    glBindBuffer(GL_ARRAY_BUFFER, gFontVbo);

    // The 2D quad requires 6 vertices of 4 floats each so we reserve 6 * 4 floats
    // of memory. 
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE,
        4 * sizeof(GLfloat), 0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    for (int i = 0; i < 3; i++)
    {
        memset(RenterText[i], 0, 255);
    }

    return 0;
}

int InitializeSkull(void)
{
    //
    // Code
    //
    gSkullVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == gSkullVertexShaderObject) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

        return -6;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
        __FILE__, __LINE__, __FUNCTION__);

    const GLchar *skull_vertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vTexCoord;"
        "uniform mat4 u_mvp_matrix;"
        "uniform mat4 u_texture_matrix;"
        "out vec3 out_texcoord;"
        "void main(void)"
        "{"
        "gl_Position = u_mvp_matrix * vPosition;"
        "out_texcoord = mat3(u_texture_matrix) * vTexCoord;"
        "}";

    glShaderSource(gSkullVertexShaderObject, 1,
        (const GLchar **)&skull_vertexShaderSourceCode, NULL);
    glCompileShader(gSkullVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(gSkullVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

        glGetShaderiv(gSkullVertexShaderObject, GL_INFO_LOG_LENGTH,
            &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;

                glGetShaderInfoLog(gSkullVertexShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(
                    gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -7;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    gSkullFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == gSkullFragmentShaderObject) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        return -8;
    }

    const GLchar *skull_fragmentShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec3 out_texcoord;"
        "uniform sampler3D u_sampler;"
        "uniform float u_alpha;"

        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "FragColor = texture(u_sampler, out_texcoord) * vec4(u_alpha);"
        "}";

    glShaderSource(gSkullFragmentShaderObject, 1,
        (const GLchar **)&skull_fragmentShaderSourceCode, NULL);
    glCompileShader(gSkullFragmentShaderObject);

    glGetShaderiv(gSkullFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (FALSE == iShaderCompileStatus) {
        fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
        glGetShaderiv(gSkullFragmentShaderObject, GL_INFO_LOG_LENGTH,
            &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;

                glGetShaderInfoLog(gSkullFragmentShaderObject, iInfoLogLength,
                    &written,
                    szInfoLog);

                fprintf(
                    gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -9;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    gSkullShaderProgramObject = glCreateProgram();

    glAttachShader(gSkullShaderProgramObject, gSkullVertexShaderObject);
    glAttachShader(gSkullShaderProgramObject, gSkullFragmentShaderObject);

    glBindAttribLocation(gSkullShaderProgramObject, AMC_ATTRIBUTES_POSITION,
        "vPosition");

    glBindAttribLocation(gSkullShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0,
        "vTexCoord");

    glLinkProgram(gSkullShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(gSkullShaderProgramObject, GL_LINK_STATUS,
        &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus) {
        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

        glGetProgramiv(gSkullShaderProgramObject, GL_INFO_LOG_LENGTH,
            &iInfoLogLength);
        if (0 < iInfoLogLength) {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog) {
                GLsizei written;

                glGetProgramInfoLog(gSkullShaderProgramObject, iInfoLogLength,
                    &written,
                    szInfoLog);

                fprintf(gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -10;
    }
    fprintf(gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    gSkullMVPUniform =
        glGetUniformLocation(gSkullShaderProgramObject, "u_mvp_matrix");
    gSkullTextureMatrixUniform = gSkullTextureMatrixUniform =
        glGetUniformLocation(gSkullShaderProgramObject, "u_texture_matrix");
    gSkullSamplerUniform =
        glGetUniformLocation(gSkullShaderProgramObject, "u_sampler");
    gSkullAlphaUniform =
        glGetUniformLocation(gSkullShaderProgramObject, "u_alpha");

    glGenVertexArrays(1, &gSkullVao);
    glBindVertexArray(gSkullVao);

    glGenBuffers(1, &gSkullVboVertex);
    glBindBuffer(GL_ARRAY_BUFFER, gSkullVboVertex);

    glBufferData(GL_ARRAY_BUFFER, 2400, NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &gSkullVboTexture);
    glBindBuffer(GL_ARRAY_BUFFER, gSkullVboTexture);
    glBufferData(GL_ARRAY_BUFFER, 2400, NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 3, GL_FLOAT, GL_FALSE, 0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    int skull_index = 0;
    for (float skull_fIndx = -1.0f; skull_fIndx <= 1.0f; skull_fIndx += 0.01f) {
        skull_vertices[skull_index + 0] = -gSkullOrthoSize;
        skull_vertices[skull_index + 1] = -gSkullOrthoSize;
        skull_vertices[skull_index + 2] = skull_fIndx;
        skull_vertices[skull_index + 3] = gSkullOrthoSize;
        skull_vertices[skull_index + 4] = -gSkullOrthoSize;
        skull_vertices[skull_index + 5] = skull_fIndx;
        skull_vertices[skull_index + 6] = gSkullOrthoSize;
        skull_vertices[skull_index + 7] = gSkullOrthoSize;
        skull_vertices[skull_index + 8] = skull_fIndx;
        skull_vertices[skull_index + 9] = -gSkullOrthoSize;
        skull_vertices[skull_index + 10] = gSkullOrthoSize;
        skull_vertices[skull_index + 11] = skull_fIndx;

        skull_texcoords[skull_index + 0] = 0.0f;
        skull_texcoords[skull_index + 1] = 0.0f;
        skull_texcoords[skull_index + 2] = ((float)skull_fIndx + 1.0f) / 2.0f;
        skull_texcoords[skull_index + 3] = 1.0f;
        skull_texcoords[skull_index + 4] = 0.0f;
        skull_texcoords[skull_index + 5] = ((float)skull_fIndx + 1.0f) / 2.0f;
        skull_texcoords[skull_index + 6] = 1.0f;
        skull_texcoords[skull_index + 7] = 1.0f;
        skull_texcoords[skull_index + 8] = ((float)skull_fIndx + 1.0f) / 2.0f;
        skull_texcoords[skull_index + 9] = 0.0f;
        skull_texcoords[skull_index + 10] = 1.0f;
        skull_texcoords[skull_index + 11] = ((float)skull_fIndx + 1.0f) / 2.0f;

        skull_index += 12;
        if (skull_index >= 2400) {
            break;
        }
    }

    return 0;
}

int AllocateSkullImageBuffer(void)
{
    //
    // Code
    //

    // File has only image data. The dimension of the data should be known.
    gSkullImageWidth = 256;
    gSkullImageHeight = 256;
    gSkullImageCount = gSkullCurrentImageCount/*109*/;

    gLuminanceBufferSize = sizeof(char) * gSkullImageWidth * gSkullImageHeight * gSkullImageCount;
    gRGBABufferSize = gLuminanceBufferSize * 4;

    // Allocate Host-Memory
    // Holds the luminance buffer
    gHostLuminanceBuffer = (char *)malloc(gLuminanceBufferSize);
    if (NULL == gHostLuminanceBuffer)
    {
        fprintf(gpFile,
            "CPU Memory Fatal Error = Can Not Allocate Enough Memory For Host "
            "luminance buffer.\n Exitting ...\n");
        return -1;
    }
    memset(gHostLuminanceBuffer, 0, gLuminanceBufferSize);

    // Holds the RGBA buffer
    gHostRGBABuffer = (char *)malloc(gRGBABufferSize);

    if (NULL == gHostRGBABuffer)
    {
        fprintf(gpFile,
            "CPU Memory Fatal Error = Can Not Allocate Enough Memory For Host "
            "RGBA buffer.\n Exitting ...\n");
        return -2;
    }
    memset(gHostRGBABuffer, 0, gRGBABufferSize);

    return 0;
}

int DeallocateSkullImageBuffer(void)
{
    //
    // Code
    //

    if (gHostRGBABuffer)
    {
        free(gHostRGBABuffer);
        gHostRGBABuffer = NULL;
    }

    if (gHostLuminanceBuffer)
    {
        free(gHostLuminanceBuffer);
        gHostLuminanceBuffer = NULL;
    }

    return 0;
}

void UninitializeSkull(void)
{
    //
    // Code
    //

    if (gSkullShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gSkullShaderProgramObject);

        glGetProgramiv(gSkullShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gSkullShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gSkullShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        glDeleteProgram(gSkullShaderProgramObject);
        gSkullShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

void UninitializeFont(void)
{
    //
    // Code
    //
    for (unsigned char c = 0; c < NUM_CHARS; c++)
    {
        glDeleteTextures(1, &gFontCharacter[(int)c].TextureID);
    }

    if (gFontShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(gFontShaderProgramObject);

        glGetProgramiv(gFontShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(gFontShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(gFontShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        glDeleteProgram(gFontShaderProgramObject);
        gFontShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
