WIN32:
cl /c /EHsc 2DTextureBased.cpp -I C:\glew-2.1.0\include
link 2DTextureBased.obj /LIBPATH C:\glew-2.1.0\lib\Release\x64\glew32.lib

X64:
cl 2DTextureBased.cpp -I C:\glew-2.1.0\include\  /link C:\glew-2.1.0\lib\Release\x64\glew32.lib

cl 2DTextureBased.cpp /I C:\glew-2.1.0\include\ /I "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\include"  /link "C:\glew-2.1.0\lib\Release\x64\glew32.lib" "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\lib\x64\OpenCL.lib"