
#include <GL/glew.h>
#include <gl/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include <CL/opencl.h>
#include "helper_timer.h"

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//
// Raw Image Data Processor Related Variables
//
int gSkullImageCount = 256;
int gSkullImageWidth = 256;
int gSkullImageHeight = 109;
int gSkullTexId;
int gSkullCurrentImageCount = 109;

//
// Buffer Related Variables
//
char *gHostLuminanceBuffer = NULL;
char *gHostRGBABuffer = NULL;

float gImageBufferConvertionTimeOnCPU;
float gImageBufferConvertionTimeOnGPU;

size_t gLuminanceBufferSize;
size_t gRGBABufferSize;

GLfloat gSkullOrthoSize = 1.0f;

//
// OpenGL Context Related Variables
//
HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

//
// FullScreen Related Variables
//
DWORD gdwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbIsFullScreen = false;

// 
// Skull-Shader Related Variables
//
GLuint gSkullVertexShaderObject;
GLuint gSkullFragmentShaderObject;
GLuint gSkullShaderProgramObject;

GLuint gSkullVao;
GLuint gSkullVboVertex;
GLuint gSkullVboTexture;
GLuint gSkullSamplerUniform;
GLuint gSkullMVPUniform;
GLuint gSkullTextureMatrixUniform;

enum {
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

mat4 gSkullOrthoProjectionMatrix;

//
// OpenCL Related Variables
//
size_t gLocalWorkSize = 256;
size_t gGlobalWorkSize;


cl_int gRetOcl;
cl_platform_id gOclPlatformID;
cl_device_id gOclComputeDeviceID;
cl_context gOclContext;
cl_command_queue gOclCommandQueue;
cl_program gOclProgram;
cl_kernel gOclKernel;

char *gOclSourceCode = NULL;
size_t gKernelCodeLength;

cl_mem gDeviceLuminanceBuffer = NULL;
cl_mem gDeviceRGBABuffer = NULL;

bool gbToggleDevice = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {
  // Function Declarations
  int Initialize(void);
  void Display(void);
  void Update(void);

  // Variable Declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("PP - Skull");

  // Code
  if (0 != fopen_s(&gpFile, "Log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.lpfnWndProc = WndProc;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("PP - Skull"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, 800, 600, NULL, NULL, hInstance, NULL);

  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (0 != iRet) {
	  fprintf(gpFile, "Initialization Failed\n");
	  DestroyWindow(hwnd);
  } else {
	  fprintf(gpFile, "Initialization Successful\n");
  }

  // GAME LOOP
  while (false == bDone) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }
      Update();
      Display();
    }
  }  // END OF WHILE

  return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // Function Declarations
  void ToggledFullScreen(void);
  void UnInitialize(void);
  void ReSize(int, int);

  void ConvertDataToRGBA(void);

  void RunOpenCLKernel(void);

  void CopyImageDataToTextureMemory(int);

  // Variable Declarations

  // Code
  switch (iMsg) {
    case WM_DESTROY:
      UnInitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_KEYDOWN:
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case VK_UP:
        case 'W':
        case 'w':
		{
			gSkullCurrentImageCount++;
			if (gSkullCurrentImageCount >= 109) {
				gSkullCurrentImageCount = 109;
			}
			CopyImageDataToTextureMemory(gSkullCurrentImageCount);
		}
          break;

        case VK_DOWN:
        case 'S':
        case 's':
		{
			gSkullCurrentImageCount--;
			if (gSkullCurrentImageCount <= 0) {
				gSkullCurrentImageCount = 0;
			}
			CopyImageDataToTextureMemory(gSkullCurrentImageCount);
		}
          break;

		case 'c':
		case 'C':
		{
			if (gbToggleDevice)
			{
				RunOpenCLKernel();
				CopyImageDataToTextureMemory(gSkullCurrentImageCount);
				gbToggleDevice = false;
			}
			else
			{
				ConvertDataToRGBA();
				CopyImageDataToTextureMemory(gSkullCurrentImageCount);
				gbToggleDevice = true;
			}
		}
		break;
      }
      break;
  }  // End Of Switch Case

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // Variable Declarations
  MONITORINFO mi;

  // Code
  if (false == gbIsFullScreen) {
    gdwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & gdwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, gdwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.left,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, gdwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }

  return;
}

void ReadRawFile(void)
{
	FILE *pRawFile;
	errno_t err;
	err = fopen_s(&pRawFile, "head256x256x109", "rb"); // binary read
	if (0  != err)
	{
		fprintf(gpFile, "fopen_s Failed Error : %d\n", err);
		return;
	}

	fread(gHostLuminanceBuffer, sizeof(char), gLuminanceBufferSize, pRawFile);

	fclose(pRawFile);

	return;
}

void ConvertDataToRGBA(void)
{
	// Start Timer
	StopWatchInterface *image_load_timer = NULL;
	sdkCreateTimer(&image_load_timer);
	sdkStartTimer(&image_load_timer);

	// Convert the data to RGBA data.
	// Here we are simply putting the same value to R, G, B and A channels.
	// Usually for raw data, the alpha value will be constructed by a threshold
	// value given by the user
	for (int nIndx = 0;
		nIndx < gLuminanceBufferSize;
		++nIndx) {
		gHostRGBABuffer[nIndx * 4] = gHostLuminanceBuffer[nIndx];
		gHostRGBABuffer[nIndx * 4 + 1] = gHostLuminanceBuffer[nIndx];
		gHostRGBABuffer[nIndx * 4 + 2] = gHostLuminanceBuffer[nIndx];
		gHostRGBABuffer[nIndx * 4 + 3] = gHostLuminanceBuffer[nIndx];
	}

	// Stop Timer
	sdkStopTimer(&image_load_timer);
	gImageBufferConvertionTimeOnCPU = sdkGetTimerValue(&image_load_timer);
	sdkDeleteTimer(&image_load_timer);

	fprintf(gpFile,"ImageBuffer Convertion Time On CPU: %f\n",gImageBufferConvertionTimeOnCPU);

	return;
}

void CopyImageDataToTextureMemory(int iImageCount)
{
	// Code
	glBindTexture(GL_TEXTURE_3D, gSkullTexId);

	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, gSkullImageWidth, gSkullImageHeight,
		iImageCount, 0, GL_RGBA, GL_UNSIGNED_BYTE, gHostRGBABuffer);

	glBindTexture(GL_TEXTURE_3D, 0);

  return;
}

int Initialize(void) {
  // Variable Declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // Function Declarations
  void ReSize(int, int);
  void Load3DTexture(void);

  void ReadRawFile(void);
  void ConvertDataToRGBA(void);
  void CopyImageDataToTextureMemory(int);

  void SetupOpenCLKernel(void);
  void RunOpenCLKernel(void);

  // Code

  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
          __FILE__, __LINE__, __FUNCTION__);

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
	fprintf(gpFile, "Choose Pixel Format Failed\n");
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
	fprintf(gpFile, "Set Pixel Format Failed\n");
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
	fprintf(gpFile, "wglCreateContext Failed\n");
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
	fprintf(gpFile, "wglMakeCurrent Failed\n");
    return -4;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

  GLenum result;

  result = glewInit();
  if (GLEW_OK != result) {
	  fprintf(gpFile, "glewInit Failed\n");
    return -5;
  }
  if (GL_TRUE != glewGetExtension("GL_EXT_texture3D")) {
	  fprintf(gpFile, "glewGetExtension Failed\n");
    return -6;
  }

  gSkullVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == gSkullVertexShaderObject) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

    return -6;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

  const GLchar *skull_vertexShaderSourceCode =
      "#version 450 core"
      "\n"
      "in vec4 vPosition;"
      "in vec3 vTexCoord;"
      "uniform mat4 u_mvp_matrix;"
      "uniform mat4 u_texture_matrix;"
      "out vec3 out_texcoord;"
      "void main(void)"
      "{"
      "gl_Position = u_mvp_matrix * vPosition;"
      "out_texcoord = mat3(u_texture_matrix) * vTexCoord;"
      "}";

  glShaderSource(gSkullVertexShaderObject, 1,
                 (const GLchar **)&skull_vertexShaderSourceCode, NULL);
  glCompileShader(gSkullVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(gSkullVertexShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

    glGetShaderiv(gSkullVertexShaderObject, GL_INFO_LOG_LENGTH,
                  &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(gSkullVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -7;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  gSkullFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == gSkullFragmentShaderObject) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

    return -8;
  }

  const GLchar *skull_fragmentShaderSourceCode =
      "#version 450 core"
      "\n"
      "in vec3 out_texcoord;"
      "uniform sampler3D u_sampler;"
      "out vec4 FragColor;"
      "void main(void)"
      "{"
      "FragColor = texture(u_sampler, out_texcoord);"
      "}";

  glShaderSource(gSkullFragmentShaderObject, 1,
                 (const GLchar **)&skull_fragmentShaderSourceCode, NULL);
  glCompileShader(gSkullFragmentShaderObject);

  glGetShaderiv(gSkullFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (FALSE == iShaderCompileStatus) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    glGetShaderiv(gSkullFragmentShaderObject, GL_INFO_LOG_LENGTH,
                  &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(gSkullFragmentShaderObject, iInfoLogLength,
                           &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -9;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  gSkullShaderProgramObject = glCreateProgram();

  glAttachShader(gSkullShaderProgramObject, gSkullVertexShaderObject);
  glAttachShader(gSkullShaderProgramObject, gSkullFragmentShaderObject);

  glBindAttribLocation(gSkullShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                       "vPosition");

  glBindAttribLocation(gSkullShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0,
                       "vTexCoord");

  glLinkProgram(gSkullShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(gSkullShaderProgramObject, GL_LINK_STATUS,
                 &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus) {
    fprintf(
        gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
        __FILE__, __LINE__, __FUNCTION__);

    glGetProgramiv(gSkullShaderProgramObject, GL_INFO_LOG_LENGTH,
                   &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetProgramInfoLog(gSkullShaderProgramObject, iInfoLogLength,
                            &written,
                            szInfoLog);

        fprintf(gpFile,
                "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                "Failed:\n\t%s\n",
                __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -10;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  gSkullMVPUniform =
      glGetUniformLocation(gSkullShaderProgramObject, "u_mvp_matrix");
  gSkullTextureMatrixUniform = gSkullTextureMatrixUniform =
      glGetUniformLocation(gSkullShaderProgramObject, "u_texture_matrix");
  gSkullSamplerUniform =
      glGetUniformLocation(gSkullShaderProgramObject, "u_sampler");

  glGenVertexArrays(1, &gSkullVao);
  glBindVertexArray(gSkullVao);

  glGenBuffers(1, &gSkullVboVertex);
  glBindBuffer(GL_ARRAY_BUFFER, gSkullVboVertex);

  glBufferData(GL_ARRAY_BUFFER, 2400, NULL, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenBuffers(1, &gSkullVboTexture);
  glBindBuffer(GL_ARRAY_BUFFER, gSkullVboTexture);
  glBufferData(GL_ARRAY_BUFFER, 2400, NULL, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  // Load Texture
  Load3DTexture();

  // File has only image data. The dimension of the data should be known.
  gSkullImageWidth = 256;
  gSkullImageHeight = 256;
  gSkullImageCount = 109;

  gLuminanceBufferSize = sizeof(char) * gSkullImageWidth * gSkullImageHeight * gSkullImageCount;
  gRGBABufferSize = gLuminanceBufferSize * 4;

  // Allocate Host-Memory
  // Holds the luminance buffer
  gHostLuminanceBuffer = (char *)malloc(gLuminanceBufferSize);
  if (NULL == gHostLuminanceBuffer)
  {
	  fprintf(gpFile,
		  "CPU Memory Fatal Error = Can Not Allocate Enough Memory For Host "
		  "luminance buffer.\n Exitting ...\n");
	  exit(EXIT_FAILURE);
  }
  memset(gHostLuminanceBuffer, 0, gLuminanceBufferSize);

  // Holds the RGBA buffer
  gHostRGBABuffer = (char *)malloc(gRGBABufferSize);

  if (NULL == gHostRGBABuffer)
  {
	  fprintf(gpFile,
		  "CPU Memory Fatal Error = Can Not Allocate Enough Memory For Host "
		  "RGBA buffer.\n Exitting ...\n");
	  exit(EXIT_FAILURE);
  }
  memset(gHostRGBABuffer, 0, gRGBABufferSize);

  ReadRawFile();

  SetupOpenCLKernel();

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  glEnable(GL_TEXTURE_3D);

  gSkullOrthoProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  return 0;
}

void UnInitialize(void) {
	void Cleanup(void);
  // If not initialized, then this will be zero. So no checking is needed.
  if (0 != gSkullTexId) {
    glDeleteTextures(1, (GLuint *)&gSkullTexId);
  }

  if (wglGetCurrentContext() == gHglrc) {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc) {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc) {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile) {
    fclose(gpFile);
  }

  Cleanup();

  return;
}

void ReSize(int width, int height) {
  if (0 == height) {
    height = 1;
  }
  GLdouble AspectRatio = (GLdouble)(width) / (GLdouble)(height);

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  // Set the orthographic projection.
  if (width <= height) {
    gSkullOrthoProjectionMatrix = ortho(
        -gSkullOrthoSize, gSkullOrthoSize, -(gSkullOrthoSize / AspectRatio), gSkullOrthoSize / AspectRatio,
              2.0f * -gSkullOrthoSize, 2.0f * gSkullOrthoSize);
  } else {
    gSkullOrthoProjectionMatrix =
        ortho(-gSkullOrthoSize * AspectRatio, gSkullOrthoSize * AspectRatio,
              -gSkullOrthoSize, gSkullOrthoSize, 2.0f * -gSkullOrthoSize,
              2.0f * gSkullOrthoSize);
  }

  return;
}

void Display(void) {
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(gSkullShaderProgramObject);

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0.05f);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  mat4 skull_modelViewMatrix;
  mat4 skull_modelViewProjectionMatrix;
  mat4 skull_rotationMatrix;
  mat4 skull_textureMatrix;

  skull_modelViewMatrix = mat4::identity();
  skull_modelViewProjectionMatrix = mat4::identity();
  skull_rotationMatrix = mat4::identity();
  skull_textureMatrix = mat4::identity();

  skull_rotationMatrix = rotate(180.0f, 0.0f, 0.0f, 1.0f);

  skull_modelViewMatrix = skull_rotationMatrix;
  skull_modelViewProjectionMatrix =
      gSkullOrthoProjectionMatrix * skull_modelViewMatrix;

  glUniformMatrix4fv(gSkullMVPUniform, 1, GL_FALSE,
                     skull_modelViewProjectionMatrix);
  glUniformMatrix4fv(gSkullTextureMatrixUniform, 1, GL_FALSE,
                     skull_textureMatrix);

  GLfloat skull_vertices[2400];
  GLfloat skull_texcoords[2400];

  int skull_index = 0;
  for (float skull_fIndx = -1.0f; skull_fIndx <= 1.0f; skull_fIndx += 0.01f) {
    skull_vertices[skull_index + 0] = -gSkullOrthoSize;
    skull_vertices[skull_index + 1] = -gSkullOrthoSize;
    skull_vertices[skull_index + 2] = skull_fIndx;
    skull_vertices[skull_index + 3] = gSkullOrthoSize;
    skull_vertices[skull_index + 4] = -gSkullOrthoSize;
    skull_vertices[skull_index + 5] = skull_fIndx;
    skull_vertices[skull_index + 6] = gSkullOrthoSize;
    skull_vertices[skull_index + 7] = gSkullOrthoSize;
    skull_vertices[skull_index + 8] = skull_fIndx;
    skull_vertices[skull_index + 9] = -gSkullOrthoSize;
    skull_vertices[skull_index + 10] = gSkullOrthoSize;
    skull_vertices[skull_index + 11] = skull_fIndx;

    skull_texcoords[skull_index + 0] = 0.0f;
    skull_texcoords[skull_index + 1] = 0.0f;
    skull_texcoords[skull_index + 2] = ((float)skull_fIndx + 1.0f) / 2.0f;
    skull_texcoords[skull_index + 3] = 1.0f;
    skull_texcoords[skull_index + 4] = 0.0f;
    skull_texcoords[skull_index + 5] = ((float)skull_fIndx + 1.0f) / 2.0f;
    skull_texcoords[skull_index + 6] = 1.0f;
    skull_texcoords[skull_index + 7] = 1.0f;
    skull_texcoords[skull_index + 8] = ((float)skull_fIndx + 1.0f) / 2.0f;
    skull_texcoords[skull_index + 9] = 0.0f;
    skull_texcoords[skull_index + 10] = 1.0f;
    skull_texcoords[skull_index + 11] = ((float)skull_fIndx + 1.0f) / 2.0f;

    skull_index += 12;
    if (skull_index >= 2400) {
      break;
    }
  }

  glEnable(GL_TEXTURE_3D);
  glBindTexture(GL_TEXTURE_3D, gSkullTexId);
  glBindVertexArray(gSkullVao);
  glBindBuffer(GL_ARRAY_BUFFER, gSkullVboVertex);
  glBufferData(GL_ARRAY_BUFFER, sizeof(skull_vertices), skull_vertices,
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, gSkullVboTexture);
  glBufferData(GL_ARRAY_BUFFER, sizeof(skull_texcoords), skull_texcoords,
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  for (int skull_i = 0; skull_i < 200; skull_i++) {
    glDrawArrays(GL_TRIANGLE_FAN, skull_i * 4, 4);
  }

  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_3D, 0);

  glUseProgram(0);

  SwapBuffers(gHdc);

  return;
}

void Update(void) {
  // Function Declaration
  
}

void Load3DTexture(void) {
  glGenTextures(1, (GLuint *)&gSkullTexId);

  glBindTexture(GL_TEXTURE_3D, gSkullTexId);

  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  /*
    m_uImageCount => Specifies the depth/slices of the texture image or the
    number of layers in a texture array. All implementations support 3D texture
    images that are at least 256 texels deep and texture arrays that are at
    least 256 layers deep.
  */
  glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, 256, 256, gSkullCurrentImageCount, 0, GL_RGBA,
               GL_UNSIGNED_BYTE, NULL);

  glBindTexture(GL_TEXTURE_3D, 0);

  return;
}

void Cleanup(void) {
	// Code

	// OpenCL Cleanup

	if (NULL != gOclSourceCode) {
		free((void *)gOclSourceCode);
		gOclSourceCode = NULL;
	}

	if (NULL != gOclKernel) {
		clReleaseKernel(gOclKernel);
		gOclKernel = NULL;
	}

	if (NULL != gOclProgram) {
		clReleaseProgram(gOclProgram);
		gOclProgram = NULL;
	}

	if (NULL != gOclCommandQueue) {
		clReleaseCommandQueue(gOclCommandQueue);
		gOclCommandQueue = NULL;
	}

	if (NULL != gOclContext) {
		clReleaseContext(gOclContext);
		gOclContext = NULL;
	}

	if (NULL != gDeviceLuminanceBuffer) {
		clReleaseMemObject(gDeviceLuminanceBuffer);
		gDeviceLuminanceBuffer = NULL;
	}

	if (NULL != gDeviceRGBABuffer) {
		clReleaseMemObject(gDeviceRGBABuffer);
		gDeviceRGBABuffer = NULL;
	}

	// Free Allocated Host-Memory
	if (NULL != gHostLuminanceBuffer) {
		free(gHostLuminanceBuffer);
		gHostLuminanceBuffer = NULL;
	}

	if (NULL != gHostRGBABuffer) {
		free(gHostRGBABuffer);
		gHostRGBABuffer = NULL;
	}
}

size_t RoundGlobalSizeToNearestMultipleOfLocalSize(int local_size,
	unsigned int global_size) {

	// Code
	unsigned int r = global_size % local_size;

	if (0 == r) {
		return (global_size);
	} else {
		return (global_size + local_size - r);
	}
}

char* LoadOclProgramSource(const char *filename, const char *preamble,
	size_t *sizeFinalLength)
{
	// Variable Declarations
	FILE *pFile = NULL;
	size_t sizeSourceLength;

	pFile = fopen(filename, "rb"); // binary read
	if (NULL == pFile) {
		return NULL;
	}

	size_t sizePreambleLength = (size_t)strlen(preamble);

	// Get The Length Of The Soruce Code
	fseek(pFile, 0, SEEK_END);
	sizeSourceLength = ftell(pFile);
	fseek(pFile, 0, SEEK_SET); // Rest To Beginning

	// Allocate A BUffer For The Source Code String An Read It In
	char *sourceString =
		(char *)malloc(sizeSourceLength + sizePreambleLength + 1);
	memcpy(sourceString, preamble, sizePreambleLength);

	if (fread((sourceString) + sizePreambleLength, sizeSourceLength, 1, pFile) !=
		1) {
		fclose(pFile);
		free(sourceString);
		return (0);
	}

	// Close The File And Return The Total Length Of The Combined (Preamble +
	// Source) String
	fclose(pFile);
	if (0 != sizeFinalLength) {
		*sizeFinalLength = sizeSourceLength + sizePreambleLength;
	}

	sourceString[sizeSourceLength + sizePreambleLength] = '\0';

	return (sourceString);
}

void SetupOpenCLKernel(void)
{
	// Get OpenCL Supporting Platform's ID
	gRetOcl = clGetPlatformIDs(1, &gOclPlatformID, NULL);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile, "OpenCL Error - clGetPlatformIDs Failed : %d. Exitting Now... \n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Get OpenCL Supporting GPU Device's ID
	gRetOcl = clGetDeviceIDs(gOclPlatformID, CL_DEVICE_TYPE_GPU, 1,
		&gOclComputeDeviceID, NULL);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clGetDeviceIDs() Failed : %d. Exitting Now...\n", gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	char gpu_name[255];
	clGetDeviceInfo(gOclComputeDeviceID, CL_DEVICE_NAME, sizeof(gpu_name),
		&gpu_name, NULL);
	fprintf(gpFile,"gpu_name: %s\n", gpu_name);

	// Create OpenCL Compute Context
	gOclContext =
		clCreateContext(NULL, 1, &gOclComputeDeviceID, NULL, NULL, &gRetOcl);
	if (CL_SUCCESS != gRetOcl) {
		printf("OpenCL Error - clCreateContext() Failed : %d. Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Allocate Device-Memory
	gDeviceLuminanceBuffer =
		clCreateBuffer(gOclContext, CL_MEM_READ_ONLY, gLuminanceBufferSize, NULL, &gRetOcl);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clCreateBuffer() Failed  For device buffer: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	gDeviceRGBABuffer =
		clCreateBuffer(gOclContext, CL_MEM_READ_ONLY, gRGBABufferSize, NULL, &gRetOcl);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clCreateBuffer() Failed  For device RGBA buffer: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Create Command Queue
	gOclCommandQueue =
		clCreateCommandQueue(gOclContext, gOclComputeDeviceID, 0, &gRetOcl);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clCreateCommandQueue() Failed : %d. Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Create OpenCL Program From .cl
	gOclSourceCode = LoadOclProgramSource("image.cl", "", &gKernelCodeLength);

	gOclProgram =
		clCreateProgramWithSource(gOclContext, 1, (const char **)&gOclSourceCode,
			&gKernelCodeLength, &gRetOcl);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,
			"OpenCL Error - clCreateProgramWithSource() Failed : %d. Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(0);
	}

	// Build OpenCL Program
	gRetOcl = clBuildProgram(gOclProgram, 0, NULL, NULL, NULL, NULL);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCl Error - clBuildProgram() Failed : %d. Exitting Now...\n",
			gRetOcl);

		size_t len;
		char buffer[2048];
		clGetProgramBuildInfo(gOclProgram, gOclComputeDeviceID, CL_PROGRAM_BUILD_LOG,
			sizeof(buffer), buffer, &len);
		fprintf(gpFile,"OpenCL Program Build Log : %s\n", buffer);

		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Create OpenCL Kernel By Passing Kernel Function Name That We Used In .cl
	// File
	gOclKernel = clCreateKernel(gOclProgram, "ImageLoad", &gRetOcl);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clCreateKernel() Failed : %d. Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Set OpenCL kernel Argument. Our OpenCL Kernel Has 4 Arguments 0,1,2,3
	// Set 0 Based 0th Argument
	gRetOcl = clSetKernelArg(gOclKernel, 0, sizeof(cl_mem), (void *)&gDeviceLuminanceBuffer);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 1st Argument: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Set 0 Based 2nd Argument i.e len
	// iNumberOfArrayElements maps To len Param Of kernel Function in .cl File
	gRetOcl = clSetKernelArg(gOclKernel, 1, sizeof(cl_int),
		(void *)&gLuminanceBufferSize);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 3rd Argument: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Set 0 Based 3rd Argument
	gRetOcl = clSetKernelArg(gOclKernel, 2, sizeof(cl_mem), (void *)&gDeviceRGBABuffer);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 2nd Argument: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Set 0 Based 4th Argument
	gRetOcl = clSetKernelArg(gOclKernel, 3, sizeof(cl_int),
		(void *)&gRGBABufferSize);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clSetKernelArg() Failed  For 3rd Argument: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Write Above 'input' cpu Buffer To Device Memory
	gRetOcl = clEnqueueWriteBuffer(gOclCommandQueue, gDeviceLuminanceBuffer, CL_FALSE, 0,
		gLuminanceBufferSize, gHostLuminanceBuffer, 0, NULL, NULL);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clEnqueueWriteBuffer() Failed  For 1st Input "
			"Buffer: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	return;
}

void RunOpenCLKernel(void)
{
	// Run The kernel
	gGlobalWorkSize = RoundGlobalSizeToNearestMultipleOfLocalSize(
		gLocalWorkSize, gLuminanceBufferSize);

	// Start Timer
	StopWatchInterface *timer = NULL;
	sdkCreateTimer(&timer);
	sdkStartTimer(&timer);

	gRetOcl =
		clEnqueueNDRangeKernel(gOclCommandQueue, gOclKernel, 1, NULL,
			&gGlobalWorkSize, &gLocalWorkSize, 0, NULL, NULL);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clEnqueueNDRangeKernel() Failed: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	// Finish OpenCL Command Queue
	clFinish(gOclCommandQueue);

	// Stop Timer
	sdkStopTimer(&timer);
	gImageBufferConvertionTimeOnGPU = sdkGetTimerValue(&timer);
	sdkDeleteTimer(&timer);

	fprintf(gpFile,"ImageBuffer Convertion Time On GPU : %f\n",gImageBufferConvertionTimeOnGPU);

	// Read Back Result From The Device (i.e. From gDeviceRGBABuffer) Into CPU
	// Variable(i.e gHostRGBABuffer)
	gRetOcl = clEnqueueReadBuffer(gOclCommandQueue, gDeviceRGBABuffer, CL_TRUE, 0, gRGBABufferSize,
		gHostRGBABuffer, 0, NULL, NULL);
	if (CL_SUCCESS != gRetOcl) {
		fprintf(gpFile,"OpenCL Error - clEnqueueReadBuffer() Failed  For 2nd Input "
			"Buffer: %d. "
			"Exitting Now...\n",
			gRetOcl);
		Cleanup();
		exit(EXIT_FAILURE);
	}

	return;
}
