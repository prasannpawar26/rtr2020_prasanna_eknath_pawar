// OpenCL Kernel
__kernel void ImageLoad(__global char *luminance_buffer, unsigned int luminance_buffer_len, __global char *rgba_buffer, unsigned int rgba_buffer_len) {

  // Variable Declarations
  int nIndx = get_global_id(0);

  // Code
  if(nIndx < luminance_buffer_len)
  {
	rgba_buffer[nIndx * 4 + 0] = luminance_buffer[nIndx];
	rgba_buffer[nIndx * 4 + 1] = luminance_buffer[nIndx];
	rgba_buffer[nIndx * 4 + 2] = luminance_buffer[nIndx];
	rgba_buffer[nIndx * 4 + 3] = luminance_buffer[nIndx];
  }
}