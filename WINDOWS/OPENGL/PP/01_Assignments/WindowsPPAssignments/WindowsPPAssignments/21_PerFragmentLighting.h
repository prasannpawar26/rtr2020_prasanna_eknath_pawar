#pragma once

int PerFragmentLighting_Initialize(void);
void PerFragmentLighting_Display(void);
void PerFragmentLighting_Update(void);
void PerFragmentLighting_ReSize(int width, int height);
void PerFragmentLighting_Uninitialize(void);
void PerFragmentLighting_WmKeydown(int keyPressed);

