#include "Common.h"
#include "14_ThreeDShapesAnimation.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_ThreeDShapesAnimation_gVertexShaderObject;
GLuint pep_ThreeDShapesAnimation_gFragmentShaderObject;
GLuint pep_ThreeDShapesAnimation_gShaderProgramObject;

GLuint pep_ThreeDShapesAnimation_Pyramid_vao;
GLuint pep_ThreeDShapesAnimation_Pyramid_vbo_position;
GLuint pep_ThreeDShapesAnimation_Pyramid_vbo_color;
GLfloat pep_ThreeDShapesAnimation_PyramidRotation;

GLuint pep_ThreeDShapesAnimation_Cube_vao;
GLuint pep_ThreeDShapesAnimation_Cube_vbo_position;
GLuint pep_ThreeDShapesAnimation_Cube_vbo_color;
GLfloat pep_ThreeDShapesAnimation_CubeRotation;

GLuint pep_ThreeDShapesAnimation_mvpUniform;

int ThreeDShapesAnimation_Initialize(void)
{
    fprintf(pep_gpFile, "\nThreeDShapesAnimation\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_ThreeDShapesAnimation_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec4 vColor;" \

        "out vec4 out_color;" \

        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_color = vColor;" \
        "}";

    glShaderSource(pep_ThreeDShapesAnimation_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_ThreeDShapesAnimation_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_ThreeDShapesAnimation_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ThreeDShapesAnimation_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ThreeDShapesAnimation_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_ThreeDShapesAnimation_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 out_color;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = out_color;" \
        "}";

    glShaderSource(pep_ThreeDShapesAnimation_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_ThreeDShapesAnimation_gFragmentShaderObject);

    glGetShaderiv(pep_ThreeDShapesAnimation_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ThreeDShapesAnimation_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ThreeDShapesAnimation_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ThreeDShapesAnimation_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_ThreeDShapesAnimation_gShaderProgramObject, pep_ThreeDShapesAnimation_gVertexShaderObject);
    glAttachShader(pep_ThreeDShapesAnimation_gShaderProgramObject, pep_ThreeDShapesAnimation_gFragmentShaderObject);

    glBindAttribLocation(pep_ThreeDShapesAnimation_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_ThreeDShapesAnimation_gShaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_ThreeDShapesAnimation_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_ThreeDShapesAnimation_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_ThreeDShapesAnimation_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_ThreeDShapesAnimation_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ThreeDShapesAnimation_mvpUniform = glGetUniformLocation(pep_ThreeDShapesAnimation_gShaderProgramObject, "u_mvp_matrix");

    //
    // Pyramid
    //
    const GLfloat triangleVertices[] = {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };
    const GLfloat triangleColor[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f
    };

    glGenVertexArrays(1, &pep_ThreeDShapesAnimation_Pyramid_vao);
    glBindVertexArray(pep_ThreeDShapesAnimation_Pyramid_vao);

    glGenBuffers(1, &pep_ThreeDShapesAnimation_Pyramid_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Pyramid_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_ThreeDShapesAnimation_Pyramid_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Pyramid_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
   /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //
    // Cube
    //
    const GLfloat squareVertices[] = {
        // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,  

        // bottom
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,

        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // back
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, -1.0f, -1.0f, 
        -1.0f, -1.0f, 1.0f, 
    };
    const GLfloat squareColor[] = {0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        1.0f, 0.5f, 0.0f,
        1.0f, 0.5f, 0.0f,
        1.0f, 0.5f, 0.0f,
        1.0f, 0.5f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f
    };

    glGenVertexArrays(1, &pep_ThreeDShapesAnimation_Cube_vao);
    glBindVertexArray(pep_ThreeDShapesAnimation_Cube_vao);

    glGenBuffers(1, &pep_ThreeDShapesAnimation_Cube_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Cube_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_ThreeDShapesAnimation_Cube_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Cube_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
    /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void ThreeDShapesAnimation_Display(void)
{
    // Render
    glUseProgram(pep_ThreeDShapesAnimation_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Pyramid
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(-2.0f, 0.0f, -6.0f) * rotate(pep_ThreeDShapesAnimation_PyramidRotation, 0.0f, 1.0f, 0.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_ThreeDShapesAnimation_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(pep_ThreeDShapesAnimation_Pyramid_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Pyramid_vbo_color);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Pyramid_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    //
    // Cube
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(2.0f, 0.0f, -6.0f) * rotate(pep_ThreeDShapesAnimation_PyramidRotation, 1.0f, 0.0f, 0.0f) * rotate(pep_ThreeDShapesAnimation_PyramidRotation, 0.0f, 1.0f, 0.0f) * rotate(pep_ThreeDShapesAnimation_PyramidRotation, 0.0f, 0.0f, 1.0f) * scale(0.75f, 0.75f, 0.75f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_ThreeDShapesAnimation_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(pep_ThreeDShapesAnimation_Cube_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Cube_vbo_color);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesAnimation_Cube_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void ThreeDShapesAnimation_Update(void)
{
    pep_ThreeDShapesAnimation_PyramidRotation += 0.5f;
    if (pep_ThreeDShapesAnimation_PyramidRotation > 360.0f)
    {
        pep_ThreeDShapesAnimation_PyramidRotation = 0.0f;
    }

    pep_ThreeDShapesAnimation_CubeRotation += 0.5f;
    if (pep_ThreeDShapesAnimation_CubeRotation > 360.0f)
    {
        pep_ThreeDShapesAnimation_CubeRotation = 0.0f;
    }

    return;
}

void ThreeDShapesAnimation_ReSize(int width, int height)
{

}

void ThreeDShapesAnimation_Uninitialize(void)
{
    if (pep_ThreeDShapesAnimation_Cube_vbo_color)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesAnimation_Cube_vbo_color);
        pep_ThreeDShapesAnimation_Cube_vbo_color = 0;
    }

    if (pep_ThreeDShapesAnimation_Cube_vbo_position)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesAnimation_Cube_vbo_position);
        pep_ThreeDShapesAnimation_Cube_vbo_position = 0;
    }

    if (pep_ThreeDShapesAnimation_Cube_vao)
    {
        glDeleteVertexArrays(1, &pep_ThreeDShapesAnimation_Cube_vao);
        pep_ThreeDShapesAnimation_Cube_vao = 0;
    }

    if (pep_ThreeDShapesAnimation_Pyramid_vbo_color)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesAnimation_Pyramid_vbo_color);
        pep_ThreeDShapesAnimation_Pyramid_vbo_color = 0;
    }

    if (pep_ThreeDShapesAnimation_Pyramid_vbo_position)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesAnimation_Pyramid_vbo_position);
        pep_ThreeDShapesAnimation_Pyramid_vbo_position = 0;
    }

    if (pep_ThreeDShapesAnimation_Pyramid_vao)
    {
        glDeleteVertexArrays(1, &pep_ThreeDShapesAnimation_Pyramid_vao);
        pep_ThreeDShapesAnimation_Pyramid_vao = 0;
    }

    if (pep_ThreeDShapesAnimation_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_ThreeDShapesAnimation_gShaderProgramObject);

        glGetProgramiv(pep_ThreeDShapesAnimation_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_ThreeDShapesAnimation_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_ThreeDShapesAnimation_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_ThreeDShapesAnimation_gShaderProgramObject);
        pep_ThreeDShapesAnimation_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
