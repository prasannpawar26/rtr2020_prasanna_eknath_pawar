#pragma once

int ThreeDCube_Initialize(void);
void ThreeDCube_Display(void);
void ThreeDCube_Update(void);
void ThreeDCube_ReSize(int width, int height);
void ThreeDCube_Uninitialize(void);

