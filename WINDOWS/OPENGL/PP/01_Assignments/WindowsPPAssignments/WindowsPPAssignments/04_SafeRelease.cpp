#include "Common.h"
#include "04_SafeRelease.h"

extern FILE *pep_gpFile;

GLuint pep_SafeRelease_gVertexShaderObject;
GLuint pep_SafeRelease_gFragmentShaderObject;
GLuint pep_SafeRelease_gShaderProgramObject;

int SafeRelease_Initialize(void)
{
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

    fprintf(pep_gpFile, "\nSafe Release\n");

    /*fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_SafeRelease_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 460" \
        "\n" \
        "void main(void)" \
        "{" \
        "}";

    glShaderSource(pep_SafeRelease_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_SafeRelease_gVertexShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_SafeRelease_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 460" \
        "\n" \
        "void main(void)" \
        "{" \
        "}"; \

        glShaderSource(pep_SafeRelease_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_SafeRelease_gFragmentShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SafeRelease_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_SafeRelease_gShaderProgramObject, pep_SafeRelease_gVertexShaderObject);
    glAttachShader(pep_SafeRelease_gShaderProgramObject, pep_SafeRelease_gFragmentShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_SafeRelease_gShaderProgramObject);

    return 0;
}

void SafeRelease_Display(void)
{

    // Render

}

void SafeRelease_Update(void)
{

}

void SafeRelease_ReSize(int width, int height)
{

}

void SafeRelease_Uninitialize(void)
{
    if (pep_SafeRelease_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_SafeRelease_gShaderProgramObject);

        glGetProgramiv(pep_SafeRelease_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_SafeRelease_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_SafeRelease_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_SafeRelease_gShaderProgramObject);
        pep_SafeRelease_gShaderProgramObject = 0;

        glUseProgram(0);
    }
}
