#pragma once

int EmptyShader_Initialize(void);
void EmptyShader_Display(void);
void EmptyShader_Update(void);
void EmptyShader_ReSize(int width, int height);
void EmptyShader_Uninitialize(void);

