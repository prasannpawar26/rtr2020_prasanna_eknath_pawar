#pragma once

int MultiColorTriangle_Initialize(void);
void MultiColorTriangle_Display(void);
void MultiColorTriangle_Update(void);
void MultiColorTriangle_ReSize(int width, int height);
void MultiColorTriangle_Uninitialize(void);

