#pragma once

int BlueScreenWithExtensionInfo_Initialize(void);
void BlueScreenWithExtensionInfo_Display(void);
void BlueScreenWithExtensionInfo_Update(void);
void BlueScreenWithExtensionInfo_ReSize(int width, int height);
void BlueScreenWithExtensionInfo_Uninitialize(void);
