#include "Common.h"
#include "Sphere.h"
#include "19_BlackAndWhiteSphere.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_BlackAndWhiteSphere_gVertexShaderObject;
GLuint pep_BlackAndWhiteSphere_gFragmentShaderObject;
GLuint pep_BlackAndWhiteSphere_gShaderProgramObject;

GLuint pep_BlackAndWhiteSphere_vao;
GLuint pep_BlackAndWhiteSphere_vbo_position;
GLuint pep_BlackAndWhiteSphere_vbo_element;

GLuint pep_BlackAndWhiteSphere_mvpUniform;

float pep_BlackAndWhiteSphere_sphere_vertices[1146];
float pep_BlackAndWhiteSphere_sphere_normals[1146];
float pep_BlackAndWhiteSphere_sphere_textures[764];
unsigned short pep_BlackAndWhiteSphere_sphere_elements[2280];

unsigned int pep_BlackAndWhiteSphere_gNumVertices;
unsigned int pep_BlackAndWhiteSphere_gNumElements;

int BlackAndWhiteSphere_Initialize(void)
{
    fprintf(pep_gpFile, "\nBlackAndWhiteSphere\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    getSphereVertexData(pep_BlackAndWhiteSphere_sphere_vertices, pep_BlackAndWhiteSphere_sphere_normals, pep_BlackAndWhiteSphere_sphere_textures,
        pep_BlackAndWhiteSphere_sphere_elements);
    pep_BlackAndWhiteSphere_gNumVertices = getNumberOfSphereVertices();
    pep_BlackAndWhiteSphere_gNumElements = getNumberOfSphereElements();

    pep_BlackAndWhiteSphere_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "uniform mat4 u_mvp_matrix;"
        "void main(void)"
        "{"
             "gl_Position = u_mvp_matrix * vPosition;"
        "}";

    glShaderSource(pep_BlackAndWhiteSphere_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_BlackAndWhiteSphere_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_BlackAndWhiteSphere_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_BlackAndWhiteSphere_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_BlackAndWhiteSphere_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_BlackAndWhiteSphere_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
        "}";

    glShaderSource(pep_BlackAndWhiteSphere_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_BlackAndWhiteSphere_gFragmentShaderObject);

    glGetShaderiv(pep_BlackAndWhiteSphere_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_BlackAndWhiteSphere_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_BlackAndWhiteSphere_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_BlackAndWhiteSphere_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_BlackAndWhiteSphere_gShaderProgramObject, pep_BlackAndWhiteSphere_gVertexShaderObject);
    glAttachShader(pep_BlackAndWhiteSphere_gShaderProgramObject, pep_BlackAndWhiteSphere_gFragmentShaderObject);

    glBindAttribLocation(pep_BlackAndWhiteSphere_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_BlackAndWhiteSphere_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_BlackAndWhiteSphere_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_BlackAndWhiteSphere_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_BlackAndWhiteSphere_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_BlackAndWhiteSphere_mvpUniform = glGetUniformLocation(pep_BlackAndWhiteSphere_gShaderProgramObject, "u_mvp_matrix");

    glGenVertexArrays(1, &pep_BlackAndWhiteSphere_vao);
    glBindVertexArray(pep_BlackAndWhiteSphere_vao);

    glGenBuffers(1, &pep_BlackAndWhiteSphere_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_BlackAndWhiteSphere_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pep_BlackAndWhiteSphere_sphere_vertices), pep_BlackAndWhiteSphere_sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &pep_BlackAndWhiteSphere_vbo_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_BlackAndWhiteSphere_vbo_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(pep_BlackAndWhiteSphere_sphere_elements),
        pep_BlackAndWhiteSphere_sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void BlackAndWhiteSphere_Display(void)
{
    // Render
    glUseProgram(pep_BlackAndWhiteSphere_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Cube
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(pep_BlackAndWhiteSphere_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
 
    glBindVertexArray(pep_BlackAndWhiteSphere_vao);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_BlackAndWhiteSphere_vbo_element);
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawElements(GL_TRIANGLES, pep_BlackAndWhiteSphere_gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void BlackAndWhiteSphere_Update(void)
{
    return;
}

void BlackAndWhiteSphere_ReSize(int width, int height)
{

}

void BlackAndWhiteSphere_Uninitialize(void)
{
    if (pep_BlackAndWhiteSphere_vbo_element) {
        glDeleteBuffers(1, &pep_BlackAndWhiteSphere_vbo_element);
        pep_BlackAndWhiteSphere_vbo_element = 0;
    }

    if (pep_BlackAndWhiteSphere_vbo_position)
    {
        glDeleteBuffers(1, &pep_BlackAndWhiteSphere_vbo_position);
        pep_BlackAndWhiteSphere_vbo_position = 0;
    }

    if (pep_BlackAndWhiteSphere_vao)
    {
        glDeleteVertexArrays(1, &pep_BlackAndWhiteSphere_vao);
        pep_BlackAndWhiteSphere_vao = 0;
    }

    if (pep_BlackAndWhiteSphere_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_BlackAndWhiteSphere_gShaderProgramObject);

        glGetProgramiv(pep_BlackAndWhiteSphere_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_BlackAndWhiteSphere_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_BlackAndWhiteSphere_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_BlackAndWhiteSphere_gShaderProgramObject);
        pep_BlackAndWhiteSphere_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

