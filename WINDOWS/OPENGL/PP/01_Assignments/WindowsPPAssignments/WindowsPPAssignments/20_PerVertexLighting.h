#pragma once

int PerVertexLighting_Initialize(void);
void PerVertexLighting_Display(void);
void PerVertexLighting_Update(void);
void PerVertexLighting_ReSize(int width, int height);
void PerVertexLighting_Uninitialize(void);
void PerVertexLighting_WmKeydown(int keyPressed);

