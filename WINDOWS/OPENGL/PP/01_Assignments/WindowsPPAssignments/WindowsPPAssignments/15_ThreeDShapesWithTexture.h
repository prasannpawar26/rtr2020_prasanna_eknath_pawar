#pragma once

#define THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_STONE 101
#define THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_VIJAY_KUNDALI 102

int ThreeDShapesWithTexture_Initialize(void);
void ThreeDShapesWithTexture_Display(void);
void ThreeDShapesWithTexture_Update(void);
void ThreeDShapesWithTexture_ReSize(int width, int height);
void ThreeDShapesWithTexture_Uninitialize(void);
BOOL ThreeDShapesWithTexture_LoadTexture(GLuint* texture, CHAR imageResourceID[]);
