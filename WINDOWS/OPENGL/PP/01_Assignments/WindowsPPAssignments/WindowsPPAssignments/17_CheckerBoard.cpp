#include "Common.h"
#include "17_CheckerBoard.h"

extern FILE *pep_gpFile;

GLuint pep_CheckerBoard_gVertexShaderObject;
GLuint pep_CheckerBoard_gFragmentShaderObject;
GLuint pep_CheckerBoard_gShaderProgramObject;

GLuint pep_CheckerBoard_Square1_vao;
GLuint pep_CheckerBoard_Square1_vbo_position;
GLuint pep_CheckerBoard_Square1_vbo_texture;

GLuint pep_CheckerBoard_Square2_vao;
GLuint pep_CheckerBoard_Square2_vbo_position;
GLuint pep_CheckerBoard_Square2_vbo_texture;

GLuint pep_CheckerBoard_mvpUniform;
GLuint pep_CheckerBoard_SamplerUniform;

#define CHECKERBOARD_IMAGE_WIDTH 64
#define CHECKERBOARD_IMAGE_HEIGHT 64
GLubyte pep_CheckerBoard_Image[CHECKERBOARD_IMAGE_HEIGHT][CHECKERBOARD_IMAGE_WIDTH][4];
GLuint pep_CheckerBoard_MathematicalTexture;

extern mat4 pep_Perspective_ProjectionMatrix;

// Function Declarations
void CheckerBoard_MakeCheckerImage(void);
void CheckerBoard_LoadTexture(void);

int CheckerBoard_Initialize(void)
{
    fprintf(pep_gpFile, "\nCheckerBoard\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_CheckerBoard_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \

        "out vec2 out_texcoord;" \

        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_texcoord = vTexCoord;" \
        "}";

    glShaderSource(pep_CheckerBoard_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_CheckerBoard_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_CheckerBoard_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_CheckerBoard_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_CheckerBoard_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_CheckerBoard_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \

        "uniform sampler2D u_sampler;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = texture(u_sampler, out_texcoord);" \
        "}";

    glShaderSource(pep_CheckerBoard_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_CheckerBoard_gFragmentShaderObject);

    glGetShaderiv(pep_CheckerBoard_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_CheckerBoard_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_CheckerBoard_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_CheckerBoard_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_CheckerBoard_gShaderProgramObject, pep_CheckerBoard_gVertexShaderObject);
    glAttachShader(pep_CheckerBoard_gShaderProgramObject, pep_CheckerBoard_gFragmentShaderObject);

    glBindAttribLocation(pep_CheckerBoard_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_CheckerBoard_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_CheckerBoard_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_CheckerBoard_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_CheckerBoard_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_CheckerBoard_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_CheckerBoard_mvpUniform = glGetUniformLocation(pep_CheckerBoard_gShaderProgramObject, "u_mvp_matrix");
    pep_CheckerBoard_SamplerUniform = glGetUniformLocation(pep_CheckerBoard_gShaderProgramObject, "u_sampler");
    //
    // Square 1
    //
    const GLfloat square1Texture[] = { 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};

    glGenVertexArrays(1, &pep_CheckerBoard_Square1_vao);
    glBindVertexArray(pep_CheckerBoard_Square1_vao);

    glGenBuffers(1, &pep_CheckerBoard_Square1_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_CheckerBoard_Square1_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 12, NULL, GL_DYNAMIC_DRAW);
    //glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_CheckerBoard_Square1_vbo_texture);
    glBindBuffer(GL_ARRAY_BUFFER, pep_CheckerBoard_Square1_vbo_texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square1Texture), square1Texture, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //
    // Square 2
    //
    const GLfloat square2Texture[] = { 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};

    glGenVertexArrays(1, &pep_CheckerBoard_Square2_vao);
    glBindVertexArray(pep_CheckerBoard_Square2_vao);

    glGenBuffers(1, &pep_CheckerBoard_Square2_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_CheckerBoard_Square2_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 12, NULL, GL_DYNAMIC_DRAW);
    //glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_CheckerBoard_Square2_vbo_texture);
    glBindBuffer(GL_ARRAY_BUFFER, pep_CheckerBoard_Square2_vbo_texture);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square2Texture), square2Texture, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    CheckerBoard_LoadTexture();

    return 0;
}

void CheckerBoard_Display(void)
{
    glEnable(GL_TEXTURE_2D);

    glUseProgram(pep_CheckerBoard_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Square
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -6.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_CheckerBoard_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1i(pep_CheckerBoard_SamplerUniform, 0);

    const GLfloat suqare1Position[] = { 0.0f, 1.0f, 0.0f, -2.0f, 1.0f, 0.0f, -2.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f};

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pep_CheckerBoard_MathematicalTexture);
    glBindVertexArray(pep_CheckerBoard_Square1_vao);
    glBindBuffer(GL_ARRAY_BUFFER, pep_CheckerBoard_Square1_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(suqare1Position), suqare1Position, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);
    const GLfloat suqare2Position[] = { 2.41421f, 1.0f, -1.41421f, 1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 2.41421f, -1.0f, -1.41421f};

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pep_CheckerBoard_MathematicalTexture);
    glBindVertexArray(pep_CheckerBoard_Square2_vao);
    glBindBuffer(GL_ARRAY_BUFFER, pep_CheckerBoard_Square2_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(suqare2Position), suqare2Position, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_TEXTURE_2D);

    return;
}

void CheckerBoard_Update(void)
{

}

void CheckerBoard_ReSize(int width, int height)
{

}

void CheckerBoard_Uninitialize(void)
{
    if (pep_CheckerBoard_Square2_vbo_position)
    {
        glDeleteBuffers(1, &pep_CheckerBoard_Square2_vbo_position);
        pep_CheckerBoard_Square2_vbo_position = 0;
    }

    if (pep_CheckerBoard_Square2_vao)
    {
        glDeleteVertexArrays(1, &pep_CheckerBoard_Square2_vao);
        pep_CheckerBoard_Square2_vao = 0;
    }

    if (pep_CheckerBoard_Square2_vbo_position)
    {
        glDeleteBuffers(1, &pep_CheckerBoard_Square2_vbo_position);
        pep_CheckerBoard_Square2_vbo_position = 0;
    }

    if (pep_CheckerBoard_Square1_vao)
    {
        glDeleteVertexArrays(1, &pep_CheckerBoard_Square1_vao);
        pep_CheckerBoard_Square1_vao = 0;
    }

    if (pep_CheckerBoard_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_CheckerBoard_gShaderProgramObject);

        glGetProgramiv(pep_CheckerBoard_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_CheckerBoard_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_CheckerBoard_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_CheckerBoard_gShaderProgramObject);
        pep_CheckerBoard_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

void CheckerBoard_LoadTexture(void)
{
    CheckerBoard_MakeCheckerImage();

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &pep_CheckerBoard_MathematicalTexture);
    glBindTexture(GL_TEXTURE_2D, pep_CheckerBoard_MathematicalTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECKERBOARD_IMAGE_WIDTH, CHECKERBOARD_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, pep_CheckerBoard_Image);
    glBindTexture(GL_TEXTURE_2D, 0);

    return;
}

void CheckerBoard_MakeCheckerImage(void)
{
    // Variable Delcarations
    int i, j, c;

    for (i = 0; i < CHECKERBOARD_IMAGE_HEIGHT; i++) {
        for (j = 0; j < CHECKERBOARD_IMAGE_WIDTH; j++) {
            c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            pep_CheckerBoard_Image[i][j][0] = (GLubyte)c; // Red
            pep_CheckerBoard_Image[i][j][1] = (GLubyte)c; // Green
            pep_CheckerBoard_Image[i][j][2] = (GLubyte)c; // Blue
            pep_CheckerBoard_Image[i][j][3] = 255; // Alpha
        }
    }

    return;
}
