#pragma once

int BlueScreen_Initialize(void);
void BlueScreen_Display(void);
void BlueScreen_Update(void);
void BlueScreen_ReSize(int width, int height);
void BlueScreen_Uninitialize(void);

