#include "Common.h"
#include "06_Perspective.h"

extern FILE *pep_gpFile;
extern HWND pep_gHwnd;

GLuint pep_Perspective_gVertexShaderObject;
GLuint giTessellationControlShaderObject;
GLuint giTessellationEvaluationShaderObject;
GLuint pep_Perspective_gFragmentShaderObject;
GLuint pep_Perspective_gShaderProgramObject;

GLuint pep_Perspective_vao;
GLuint pep_Perspective_vbo;
GLuint pep_Perspective_mvpUniform;
GLuint gNumberOfSegmentsUniform;
GLuint gNumberOfStripsUniform;
GLuint gLineColorUniform;

extern unsigned int gNumberOfLineSegments;

extern mat4 pep_Perspective_ProjectionMatrix;

int Perspective_Initialize(void)
{
    fprintf(pep_gpFile, "\nPerspective\n");

    /*fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_Perspective_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 vPosition;" \

        "void main(void)" \
        "{" \
            "gl_Position = vec4(vPosition, 0.0, 1.0);" \
        "}";

    glShaderSource(pep_Perspective_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_Perspective_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Perspective_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Perspective_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Perspective_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    /**/
    giTessellationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
    const GLchar *tessellationControlShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "layout(vertices=4)out;" \
        "uniform int numberOfSegments;" \
        "uniform int numberOfStrips;" \
        "void main(void)" \
        "{" \
        "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \
        "gl_TessLevelOuter[0] = float(numberOfStrips);" \
        "gl_TessLevelOuter[1] = float(numberOfSegments);" \
        "}";
    glShaderSource(giTessellationControlShaderObject, 1,
        (const GLchar **)&tessellationControlShaderSourceCode, NULL);

    glCompileShader(giTessellationControlShaderObject);
    //
    //TODO: Compilation Code Here
    //
    

    giTessellationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

    const GLchar *tessellationEvaluationShaderSourceCode =
        "#version 450 core"
        "\n"
        "layout(isolines)in;"
        "uniform mat4 u_mvp_matrix;"
        "void main(void)"
        "{"
        "float u = gl_TessCoord.x;"
        "vec3 p0 = gl_in[0].gl_Position.xyz;"
        "vec3 p1 = gl_in[1].gl_Position.xyz;"
        "vec3 p2 = gl_in[2].gl_Position.xyz;"
        "vec3 p3 = gl_in[3].gl_Position.xyz;"

        "float u1 = (1.0 - u);"
        "float u2 = u * u;"

        "float b3 = u2 * u;"
        "float b2 = 3.0 * u2 * u1;"
        "float b1 = 3.0 * u * u1 * u1;"
        "float b0 = u1 * u1 * u1;"
        "vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;"
        "gl_Position = u_mvp_matrix * vec4(p, 1.0);"
        "}";
    glShaderSource(giTessellationEvaluationShaderObject, 1,
        (const GLchar **)&tessellationEvaluationShaderSourceCode,
        NULL);

    glCompileShader(giTessellationEvaluationShaderObject);
    //
    //TODO: Compilation Code Here
    //
    
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_Perspective_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "uniform vec4 lineColor;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = lineColor;" \
        "}";

    glShaderSource(pep_Perspective_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_Perspective_gFragmentShaderObject);

    glGetShaderiv(pep_Perspective_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Perspective_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Perspective_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Perspective_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_Perspective_gShaderProgramObject, pep_Perspective_gVertexShaderObject);
    glAttachShader(pep_Perspective_gShaderProgramObject, giTessellationControlShaderObject);
    glAttachShader(pep_Perspective_gShaderProgramObject, giTessellationEvaluationShaderObject);
    glAttachShader(pep_Perspective_gShaderProgramObject, pep_Perspective_gFragmentShaderObject);

    glBindAttribLocation(pep_Perspective_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_Perspective_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Perspective_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_Perspective_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Perspective_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Perspective_mvpUniform = glGetUniformLocation(pep_Perspective_gShaderProgramObject, "u_mvp_matrix");
    gNumberOfSegmentsUniform = glGetUniformLocation(pep_Perspective_gShaderProgramObject, "numberOfSegments");
    gNumberOfStripsUniform = glGetUniformLocation(pep_Perspective_gShaderProgramObject, "numberOfStrips");
    gLineColorUniform = glGetUniformLocation(pep_Perspective_gShaderProgramObject, "lineColor");

    const GLfloat vertices[] = {-1.0f, -1.0f, -0.50f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f};

    glGenVertexArrays(1, &pep_Perspective_vao);
    glBindVertexArray(pep_Perspective_vao);
    glGenBuffers(1, &pep_Perspective_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Perspective_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    gNumberOfLineSegments = 1;

    return 0;
}

void Perspective_Display(void)
{
    glEnable(GL_CULL_FACE);

    // Render
    glUseProgram(pep_Perspective_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.5f, 0.5f, -2.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_Perspective_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1i(gNumberOfSegmentsUniform, gNumberOfLineSegments);
    glUniform1i(gNumberOfStripsUniform, 1);
    glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));

    TCHAR str[255];
    wsprintf(str, TEXT("Segments : %d"), gNumberOfLineSegments);
    SetWindowText(pep_gHwnd, str);

    glBindVertexArray(pep_Perspective_vao);
    glDrawArrays(GL_PATCHES, 0, 4); // 4 vertices cha array gl_patchvertices_in. 4 here is equal to "layout(vertices=4)out;"
    glBindVertexArray(0);

    glUseProgram(0);

    glDisable(GL_CULL_FACE);

    return;
}

void Perspective_Update(void)
{

}

void Perspective_ReSize(int width, int height)
{
    /*pep_Perspective_ProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);*/
}

void Perspective_Uninitialize(void)
{
    if (pep_Perspective_vbo)
    {
        glDeleteBuffers(1, &pep_Perspective_vbo);
        pep_Perspective_vbo = 0;
    }

    if (pep_Perspective_vao)
    {
        glDeleteVertexArrays(1, &pep_Perspective_vao);
        pep_Perspective_vao = 0;
    }

    if (pep_Perspective_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Perspective_gShaderProgramObject);

        glGetProgramiv(pep_Perspective_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Perspective_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Perspective_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Perspective_gShaderProgramObject);
        pep_Perspective_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
