#include "Common.h"
#include "09_TwoDShapes.h"

extern FILE *pep_gpFile;

GLuint pep_TwoDShapes_gVertexShaderObject;
GLuint pep_TwoDShapes_gFragmentShaderObject;
GLuint pep_TwoDShapes_gShaderProgramObject;

GLuint pep_TwoDShapes_Triangle_vao;
GLuint pep_TwoDShapes_Triangle_vbo_position;
GLuint pep_TwoDShapes_Triangle_vbo_color;

GLuint pep_TwoDShapes_Square_vao;
GLuint pep_TwoDShapes_Square_vbo_position;
GLuint pep_TwoDShapes_Square_vbo_color;

GLuint pep_TwoDShapes_mvpUniform;

extern mat4 pep_Perspective_ProjectionMatrix;

int TwoDShapes_Initialize(void)
{
    fprintf(pep_gpFile, "\nTwoDShapes\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_TwoDShapes_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec4 vColor;" \

        "out vec4 out_color;" \

        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_color = vColor;" \
        "}";

    glShaderSource(pep_TwoDShapes_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_TwoDShapes_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_TwoDShapes_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TwoDShapes_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TwoDShapes_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_TwoDShapes_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 out_color;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = out_color;" \
        "}";

    glShaderSource(pep_TwoDShapes_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_TwoDShapes_gFragmentShaderObject);

    glGetShaderiv(pep_TwoDShapes_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TwoDShapes_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TwoDShapes_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_TwoDShapes_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_TwoDShapes_gShaderProgramObject, pep_TwoDShapes_gVertexShaderObject);
    glAttachShader(pep_TwoDShapes_gShaderProgramObject, pep_TwoDShapes_gFragmentShaderObject);

    glBindAttribLocation(pep_TwoDShapes_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_TwoDShapes_gShaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_TwoDShapes_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_TwoDShapes_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_TwoDShapes_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_TwoDShapes_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_TwoDShapes_mvpUniform = glGetUniformLocation(pep_TwoDShapes_gShaderProgramObject, "u_mvp_matrix");

    //
    // Triangle
    //
    const GLfloat triangleVertices[] = {0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
    const GLfloat triangleColor[] = {1.0f, 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &pep_TwoDShapes_Triangle_vao);
    glBindVertexArray(pep_TwoDShapes_Triangle_vao);

    glGenBuffers(1, &pep_TwoDShapes_Triangle_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Triangle_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_TwoDShapes_Triangle_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Triangle_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
   /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //
    // Square
    //
    const GLfloat squareVertices[] = {1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
    const GLfloat squareColor[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &pep_TwoDShapes_Square_vao);
    glBindVertexArray(pep_TwoDShapes_Square_vao);

    glGenBuffers(1, &pep_TwoDShapes_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_TwoDShapes_Square_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Square_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
    /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void TwoDShapes_Display(void)
{
    glEnable(GL_CULL_FACE);

    // Render
    glUseProgram(pep_TwoDShapes_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Triangle
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(-2.0f, 0.0f, -6.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_TwoDShapes_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(pep_TwoDShapes_Triangle_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Triangle_vbo_color);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Triangle_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    //
    // Square
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(2.0f, 0.0f, -6.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_TwoDShapes_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(pep_TwoDShapes_Square_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Square_vbo_color);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoDShapes_Square_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindVertexArray(0);

    glUseProgram(0);

    glDisable(GL_CULL_FACE);

    return;
}

void TwoDShapes_Update(void)
{

}

void TwoDShapes_ReSize(int width, int height)
{

}

void TwoDShapes_Uninitialize(void)
{
    if (pep_TwoDShapes_Square_vbo_color)
    {
        glDeleteBuffers(1, &pep_TwoDShapes_Square_vbo_color);
        pep_TwoDShapes_Square_vbo_color = 0;
    }

    if (pep_TwoDShapes_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_TwoDShapes_Square_vbo_position);
        pep_TwoDShapes_Square_vbo_position = 0;
    }

    if (pep_TwoDShapes_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_TwoDShapes_Square_vao);
        pep_TwoDShapes_Square_vao = 0;
    }

    if (pep_TwoDShapes_Triangle_vbo_color)
    {
        glDeleteBuffers(1, &pep_TwoDShapes_Triangle_vbo_color);
        pep_TwoDShapes_Triangle_vbo_color = 0;
    }

    if (pep_TwoDShapes_Triangle_vbo_position)
    {
        glDeleteBuffers(1, &pep_TwoDShapes_Triangle_vbo_position);
        pep_TwoDShapes_Triangle_vbo_position = 0;
    }

    if (pep_TwoDShapes_Triangle_vao)
    {
        glDeleteVertexArrays(1, &pep_TwoDShapes_Triangle_vao);
        pep_TwoDShapes_Triangle_vao = 0;
    }

    if (pep_TwoDShapes_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_TwoDShapes_gShaderProgramObject);

        glGetProgramiv(pep_TwoDShapes_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_TwoDShapes_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_TwoDShapes_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_TwoDShapes_gShaderProgramObject);
        pep_TwoDShapes_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
