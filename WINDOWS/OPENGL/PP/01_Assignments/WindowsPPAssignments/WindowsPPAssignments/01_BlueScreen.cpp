
#include "Common.h"
#include "01_BlueScreen.h"

int BlueScreen_Initialize(void)
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	return 0;
}

void BlueScreen_Display(void)
{

}

void BlueScreen_Update(void)
{

}

void BlueScreen_ReSize(int width, int height)
{

}

void BlueScreen_Uninitialize(void)
{

}
