#pragma once

int Perspective_Initialize(void);
void Perspective_Display(void);
void Perspective_Update(void);
void Perspective_ReSize(int width, int height);
void Perspective_Uninitialize(void);
