#include "Common.h"
#include "05_Ortho.h"

extern FILE *pep_gpFile;

GLuint pep_Ortho_gVertexShaderObject;
GLuint pep_Ortho_gFragmentShaderObject;
GLuint pep_Ortho_gShaderProgramObject;

GLuint pep_Ortho_vao;
GLuint pep_Ortho_vbo;
GLuint pep_Ortho_mvpUniform;

mat4 pep_OrthoProjectionMatrix;

int Ortho_Initialize(void)
{
    /*glClearColor(0.0f, 0.0f, 1.0f, 1.0f);*/

    fprintf(pep_gpFile, "\nOrtho\n");

    /*fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_Ortho_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \

        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
        "}";

    glShaderSource(pep_Ortho_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_Ortho_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Ortho_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Ortho_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Ortho_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_Ortho_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);" \
        "}";

    glShaderSource(pep_Ortho_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_Ortho_gFragmentShaderObject);

    glGetShaderiv(pep_Ortho_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Ortho_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Ortho_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Ortho_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_Ortho_gShaderProgramObject, pep_Ortho_gVertexShaderObject);
    glAttachShader(pep_Ortho_gShaderProgramObject, pep_Ortho_gFragmentShaderObject);

    glBindAttribLocation(pep_Ortho_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_Ortho_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Ortho_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_Ortho_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Ortho_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Ortho_mvpUniform = glGetUniformLocation(pep_Ortho_gShaderProgramObject, "u_mvp_matrix");

    const GLfloat triangleVertices[] = {0.0f, 50.0f, 0.0f, -50.0f, -50.0f, 0.0f, 50.0f, -50.0f, 0.0f};

    glGenVertexArrays(1, &pep_Ortho_vao);
    glBindVertexArray(pep_Ortho_vao);
    glGenBuffers(1, &pep_Ortho_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Ortho_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    //glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    pep_OrthoProjectionMatrix = mat4::identity();

	return 0;
}

void Ortho_Display(void)
{
    glEnable(GL_CULL_FACE);

    // Render
    glUseProgram(pep_Ortho_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewProjectionMatrix = pep_OrthoProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_Ortho_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(pep_Ortho_vao);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Ortho_vbo);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    glUseProgram(0);

    glDisable(GL_CULL_FACE);

    return;
}

void Ortho_Update(void)
{
    return;
}

void Ortho_ReSize(int width, int height)
{
    if (width <= height) {
        pep_OrthoProjectionMatrix =
            ortho(-100.0f, 100.0f, (-100.0f * ((GLfloat)height / (GLfloat)width)),
                (100.0f * ((GLfloat)height / (GLfloat)width)), -100.0f, 100.0f);
    } else {
        pep_OrthoProjectionMatrix =
            ortho((-100.0f * ((GLfloat)width / (GLfloat)height)),
                (100.0f * ((GLfloat)width / (GLfloat)height)), -100.0f, 100.0f,
                -100.0f, 100.0f);
    }

    return;
}

void Ortho_Uninitialize(void)
{
    if (pep_Ortho_vbo)
    {
        glDeleteBuffers(1, &pep_Ortho_vbo);
        pep_Ortho_vbo = 0;
    }

    if (pep_Ortho_vao)
    {
        glDeleteVertexArrays(1, &pep_Ortho_vao);
        pep_Ortho_vao = 0;
    }

    if (pep_Ortho_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Ortho_gShaderProgramObject);

        glGetProgramiv(pep_Ortho_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Ortho_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Ortho_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Ortho_gShaderProgramObject);
        pep_Ortho_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
