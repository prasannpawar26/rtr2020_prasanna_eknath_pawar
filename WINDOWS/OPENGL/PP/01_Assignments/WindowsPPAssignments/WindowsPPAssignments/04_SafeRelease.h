#pragma once

int SafeRelease_Initialize(void);
void SafeRelease_Display(void);
void SafeRelease_Update(void);
void SafeRelease_ReSize(int width, int height);
void SafeRelease_Uninitialize(void);
