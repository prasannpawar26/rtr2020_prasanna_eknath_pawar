
#include "Common.h"
#include "03_EmptyShader.h"

extern FILE *pep_gpFile;

GLuint pep_EmptyShader_gVertexShaderObject;
GLuint pep_EmptyShader_gFragmentShaderObject;
GLuint pep_EmptyShader_gShaderProgramObject;

int EmptyShader_Initialize(void)
{
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

    /*fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_EmptyShader_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 460" \
        "\n" \
        "void main(void)" \
        "{" \
        "}";

    glShaderSource(pep_EmptyShader_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_EmptyShader_gVertexShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_EmptyShader_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 460" \
        "\n" \
        "void main(void)" \
        "{" \
        "}"; \

    glShaderSource(pep_EmptyShader_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_EmptyShader_gFragmentShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_EmptyShader_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_EmptyShader_gShaderProgramObject, pep_EmptyShader_gVertexShaderObject);
    glAttachShader(pep_EmptyShader_gShaderProgramObject, pep_EmptyShader_gFragmentShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_EmptyShader_gShaderProgramObject);

    return 0;
}

void EmptyShader_Display(void)
{

    // Render

}

void EmptyShader_Update(void)
{

}

void EmptyShader_ReSize(int width, int height)
{

}

void EmptyShader_Uninitialize(void)
{
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glDetachShader(pep_EmptyShader_gShaderProgramObject, pep_EmptyShader_gVertexShaderObject);
    glDetachShader(pep_EmptyShader_gShaderProgramObject, pep_EmptyShader_gFragmentShaderObject);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Delete vertex shader object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glDeleteShader(pep_EmptyShader_gVertexShaderObject);
    pep_EmptyShader_gVertexShaderObject = 0;

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Delete fragment shader object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glDeleteShader(pep_EmptyShader_gFragmentShaderObject);
    pep_EmptyShader_gFragmentShaderObject = 0;

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glDeleteProgram(pep_EmptyShader_gShaderProgramObject);
    pep_EmptyShader_gShaderProgramObject = 0;

    return;
}

