#pragma once

int DiffuseLightOnCube_Initialize(void);
void DiffuseLightOnCube_Display(void);
void DiffuseLightOnCube_Update(void);
void DiffuseLightOnCube_ReSize(int width, int height);
void DiffuseLightOnCube_Uninitialize(void);
void DiffuseLightOnCube_WmKeydown(int);
