#pragma once

int TwoSteadyLights_Initialize(void);
void TwoSteadyLights_Display(void);
void TwoSteadyLights_Update(void);
void TwoSteadyLights_ReSize(int width, int height);
void TwoSteadyLights_Uninitialize(void);
void TwoSteadyLights_WmKeydown(int keyPressed);

