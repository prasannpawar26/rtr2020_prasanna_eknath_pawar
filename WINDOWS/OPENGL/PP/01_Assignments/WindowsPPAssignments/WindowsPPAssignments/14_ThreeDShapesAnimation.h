#pragma once

int ThreeDShapesAnimation_Initialize(void);
void ThreeDShapesAnimation_Display(void);
void ThreeDShapesAnimation_Update(void);
void ThreeDShapesAnimation_ReSize(int width, int height);
void ThreeDShapesAnimation_Uninitialize(void);

