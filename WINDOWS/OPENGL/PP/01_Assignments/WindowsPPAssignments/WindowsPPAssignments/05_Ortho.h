#pragma once

int Ortho_Initialize(void);
void Ortho_Display(void);
void Ortho_Update(void);
void Ortho_ReSize(int width, int height);
void Ortho_Uninitialize(void);
