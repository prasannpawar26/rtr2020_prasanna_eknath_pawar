#include "Common.h"
#include "08_ColorSquare.h"

extern FILE *pep_gpFile;

GLuint pep_ColorSquare_gVertexShaderObject;
GLuint pep_ColorSquare_gFragmentShaderObject;
GLuint pep_ColorSquare_gShaderProgramObject;

GLuint pep_ColorSquare_Square_vao;
GLuint pep_ColorSquare_Square_vbo_position;
GLuint pep_ColorSquare_Square_vbo_color;

GLuint pep_ColorSquare_mvpUniform;
GLuint pep_ColorSquare_dimentionUniform;
GLuint pep_ColorSquare_radiusUniform;

int pep_ColorSquare_Height;
int pep_ColorSquare_Width;

extern mat4 pep_Perspective_ProjectionMatrix;

int ColorSquare_Initialize(void)
{
    fprintf(pep_gpFile, "\nColorSquare\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_ColorSquare_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec4 vColor;" \

        "out vec4 out_color;" \

        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_color = vColor;" \
        "}";

    glShaderSource(pep_ColorSquare_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_ColorSquare_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_ColorSquare_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ColorSquare_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ColorSquare_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_ColorSquare_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 out_color;" \

        "out vec4 FragColor;" \

        "uniform vec2 u_dimention;" \
        "uniform float u_radius;"

        "void main(void)" \
        "{" \
            "vec2 center = vec2(u_dimention.x / 2.0, u_dimention.y / 2.0);" \
        
            //"if((center.x - 150.0 < gl_FragCoord.x) && (center.x + 150.0 > gl_FragCoord.x) && (center.y - 150.0 < gl_FragCoord.y) && (center.y + 150.0 > gl_FragCoord.y))" \

            "float dist = (gl_FragCoord.x - center.x) * (gl_FragCoord.x - center.x) + (gl_FragCoord.y - center.y) * (gl_FragCoord.y - center.y);"

            "if( dist <= u_radius * u_radius)"
            "{"
                "FragColor = vec4(1.0, 0.0, 0.0, 0.0);" \
            "}"
            "else"
            "{"
                "FragColor = out_color;" \
            "}"

        "}";

    glShaderSource(pep_ColorSquare_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_ColorSquare_gFragmentShaderObject);

    glGetShaderiv(pep_ColorSquare_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ColorSquare_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ColorSquare_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ColorSquare_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_ColorSquare_gShaderProgramObject, pep_ColorSquare_gVertexShaderObject);
    glAttachShader(pep_ColorSquare_gShaderProgramObject, pep_ColorSquare_gFragmentShaderObject);

    glBindAttribLocation(pep_ColorSquare_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_ColorSquare_gShaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_ColorSquare_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_ColorSquare_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_ColorSquare_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_ColorSquare_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ColorSquare_mvpUniform = glGetUniformLocation(pep_ColorSquare_gShaderProgramObject, "u_mvp_matrix");
    pep_ColorSquare_dimentionUniform = glGetUniformLocation(pep_ColorSquare_gShaderProgramObject, "u_dimention");
    pep_ColorSquare_radiusUniform = glGetUniformLocation(pep_ColorSquare_gShaderProgramObject, "u_radius");
    //u_radius
    

    //
    // Square
    //
    const GLfloat squareVertices[] = {1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
    const GLfloat squareColor[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &pep_ColorSquare_Square_vao);
    glBindVertexArray(pep_ColorSquare_Square_vao);

    glGenBuffers(1, &pep_ColorSquare_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ColorSquare_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_ColorSquare_Square_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ColorSquare_Square_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
     glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void ColorSquare_Display(void)
{
    glEnable(GL_CULL_FACE);

    // Render
    glUseProgram(pep_ColorSquare_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Square
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -4.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_ColorSquare_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform2f(pep_ColorSquare_dimentionUniform, (float)pep_ColorSquare_Width, (float)pep_ColorSquare_Height);

    static float radius = 0.10f;
    glUniform1f(pep_ColorSquare_radiusUniform, radius);
    radius += 0.1f;
    glBindVertexArray(pep_ColorSquare_Square_vao);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindVertexArray(0);

    glUseProgram(0);

    glDisable(GL_CULL_FACE);

    return;
}

void ColorSquare_Update(void)
{

}

void ColorSquare_ReSize(int width, int height)
{
    pep_ColorSquare_Width = width;
    pep_ColorSquare_Height = height;
}

void ColorSquare_Uninitialize(void)
{
    if (pep_ColorSquare_Square_vbo_color)
    {
        glDeleteBuffers(1, &pep_ColorSquare_Square_vbo_color);
        pep_ColorSquare_Square_vbo_color = 0;
    }

    if (pep_ColorSquare_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_ColorSquare_Square_vbo_position);
        pep_ColorSquare_Square_vbo_position = 0;
    }

    if (pep_ColorSquare_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_ColorSquare_Square_vao);
        pep_ColorSquare_Square_vao = 0;
    }

    if (pep_ColorSquare_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_ColorSquare_gShaderProgramObject);

        glGetProgramiv(pep_ColorSquare_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_ColorSquare_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_ColorSquare_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_ColorSquare_gShaderProgramObject);
        pep_ColorSquare_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
