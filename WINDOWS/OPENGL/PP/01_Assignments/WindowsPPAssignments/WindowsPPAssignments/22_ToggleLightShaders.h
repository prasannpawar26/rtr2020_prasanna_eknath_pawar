#pragma once

void ToggleLightShaders_ReSize(int width, int height);
void ToggleLightShaders_Update(void);
void ToggleLightShaders_WmKeydown(int keyPressed);
void ToggleLightShaders_Display(bool bInnerFadeOut, bool *outer_fade_out);
int ToggleLightShaders_Initialize(void);
void ToggleLightShaders_Uninitialize(void);
