#include "Common.h"
#include "Sphere.h"
#include "20_PerVertexLighting.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_PerVertexLighting_gVertexShaderObject;
GLuint pep_PerVertexLighting_gFragmentShaderObject;
GLuint pep_PerVertexLighting_gShaderProgramObject;

GLuint pep_PerVertexLighting_vao;
GLuint pep_PerVertexLighting_vbo_position;
GLuint pep_PerVertexLighting_vbo_normal;
GLuint pep_PerVertexLighting_vbo_element;

GLuint pep_PerVertexLighting_mvpUniform;

float pep_PerVertexLighting_sphere_vertices[1146];
float pep_PerVertexLighting_sphere_normals[1146];
float pep_PerVertexLighting_sphere_textures[764];
unsigned short pep_PerVertexLighting_sphere_elements[2280];

unsigned int pep_PerVertexLighting_gNumVertices;
unsigned int pep_PerVertexLighting_gNumElements;

// UNIFORMS
GLuint pep_PerVertexLighting_modelMatrixUniform;
GLuint pep_PerVertexLighting_viewMatrixUniform;
GLuint pep_PerVertexLighting_projectionMatrixUniform;
GLuint pep_PerVertexLighting_lightAmbientUniform;
GLuint pep_PerVertexLighting_lightDiffuseUniform;
GLuint pep_PerVertexLighting_lightSpecularUniform;
GLuint pep_PerVertexLighting_materialAmbientUniform;
GLuint pep_PerVertexLighting_materialDiffuseUniform;
GLuint pep_PerVertexLighting_materialSpecularUniform;
GLuint pep_PerVertexLighting_materialShinessUniform;
GLuint pep_PerVertexLighting_lightPositionUniform;
GLuint pep_PerVertexLighting_keyLPressedUniform;

//
// Application Variables Associated With Uniforms
float pep_PerVertexLighting_lightAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_PerVertexLighting_lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_lightPosition[4] = {100.0f, 100.0f, 100.0f, 1.0f};

float pep_PerVertexLighting_materialAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_PerVertexLighting_materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_PerVertexLighting_materialShiness = 50.0f;  // Also Try 128.0f

bool pep_PerVertexLighting_bIsLightEnable = true;

int PerVertexLighting_Initialize(void)
{
    fprintf(pep_gpFile, "\nPerVertexLighting\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    getSphereVertexData(pep_PerVertexLighting_sphere_vertices, pep_PerVertexLighting_sphere_normals, pep_PerVertexLighting_sphere_textures,
        pep_PerVertexLighting_sphere_elements);
    pep_PerVertexLighting_gNumVertices = getNumberOfSphereVertices();
    pep_PerVertexLighting_gNumElements = getNumberOfSphereElements();

    pep_PerVertexLighting_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec3 u_light_ambient;"
        "uniform vec3 u_light_diffuse;"
        "uniform vec3 u_light_specular;"
        "uniform vec4 u_light_position;"
        "uniform vec3 u_material_ambient;"
        "uniform vec3 u_material_diffuse;"
        "uniform vec3 u_material_specular;"
        "uniform float u_material_shiness;"
        "uniform int u_key_L_pressed;"

        "out vec3 phong_ads_light;"

        "void main(void)"
        "{"
            "if(1 == u_key_L_pressed)"
            "{"
                "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
                "vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
                "vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));"
                "float t_normal_dot_light_direction = max(dot(light_direction, t_normal), 0.0f);"
                "vec3 reflection_vector = reflect(-light_direction, t_normal);"
                "vec3 viewer_vector = normalize(vec3(-eye_coordinates));"
                "vec3 ambient = u_light_ambient * u_material_ambient;"
                "vec3 diffuse = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction;"
                "vec3 specular = u_light_specular * u_material_specular * pow(max(dot(reflection_vector, viewer_vector), 0.0f), u_material_shiness);"

                "phong_ads_light = ambient + diffuse + specular;"
            "}"
            "else"
            "{"
               "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
            "}"

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

    glShaderSource(pep_PerVertexLighting_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_PerVertexLighting_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_PerVertexLighting_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_PerVertexLighting_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_PerVertexLighting_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_PerVertexLighting_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec3 phong_ads_light;"
        "uniform int u_key_L_pressed;"
        "out vec4 FragColor;"

        "void main(void)"
        "{"
            "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

    glShaderSource(pep_PerVertexLighting_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_PerVertexLighting_gFragmentShaderObject);

    glGetShaderiv(pep_PerVertexLighting_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_PerVertexLighting_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_PerVertexLighting_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_PerVertexLighting_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_PerVertexLighting_gShaderProgramObject, pep_PerVertexLighting_gVertexShaderObject);
    glAttachShader(pep_PerVertexLighting_gShaderProgramObject, pep_PerVertexLighting_gFragmentShaderObject);

    glBindAttribLocation(pep_PerVertexLighting_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_PerVertexLighting_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_PerVertexLighting_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_PerVertexLighting_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_PerVertexLighting_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_PerVertexLighting_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    //pep_PerVertexLighting_mvpUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_mvp_matrix");
    pep_PerVertexLighting_modelMatrixUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_model_matrix");
    pep_PerVertexLighting_viewMatrixUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_view_matrix");
    pep_PerVertexLighting_projectionMatrixUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_projection_matrix");

    pep_PerVertexLighting_lightAmbientUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_ambient");
    pep_PerVertexLighting_lightDiffuseUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_diffuse");
    pep_PerVertexLighting_lightSpecularUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_specular");
    pep_PerVertexLighting_lightPositionUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_light_position");

    pep_PerVertexLighting_materialAmbientUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_ambient");
    pep_PerVertexLighting_materialDiffuseUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_diffuse");
    pep_PerVertexLighting_materialSpecularUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_specular");
    pep_PerVertexLighting_materialShinessUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_material_shiness");

    pep_PerVertexLighting_keyLPressedUniform = glGetUniformLocation(pep_PerVertexLighting_gShaderProgramObject, "u_key_L_pressed");

    glGenVertexArrays(1, &pep_PerVertexLighting_vao);
    glBindVertexArray(pep_PerVertexLighting_vao);

    glGenBuffers(1, &pep_PerVertexLighting_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_PerVertexLighting_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pep_PerVertexLighting_sphere_vertices), pep_PerVertexLighting_sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_PerVertexLighting_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_PerVertexLighting_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pep_PerVertexLighting_sphere_normals), pep_PerVertexLighting_sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &pep_PerVertexLighting_vbo_element);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_PerVertexLighting_vbo_element);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(pep_PerVertexLighting_sphere_elements),
        pep_PerVertexLighting_sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void PerVertexLighting_Display(void)
{
    // Render
    glUseProgram(pep_PerVertexLighting_gShaderProgramObject);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;

    //
    // Cube
    //
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    viewMatrix = translate(0.0f, 0.0f, -3.0f);
    projectionMatrix = pep_Perspective_ProjectionMatrix;

    glUniformMatrix4fv(pep_PerVertexLighting_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_PerVertexLighting_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_PerVertexLighting_projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);
 
    if (pep_PerVertexLighting_bIsLightEnable)
    {
        glUniform1i(pep_PerVertexLighting_keyLPressedUniform, 1);
        glUniform3fv(pep_PerVertexLighting_lightAmbientUniform, 1, pep_PerVertexLighting_lightAmbient);
        glUniform3fv(pep_PerVertexLighting_lightDiffuseUniform, 1, pep_PerVertexLighting_lightDiffuse);
        glUniform3fv(pep_PerVertexLighting_lightSpecularUniform, 1, pep_PerVertexLighting_lightSpecular);
        glUniform4fv(pep_PerVertexLighting_lightPositionUniform, 1, pep_PerVertexLighting_lightPosition);
        glUniform3fv(pep_PerVertexLighting_materialAmbientUniform, 1, pep_PerVertexLighting_materialAmbient);
        glUniform3fv(pep_PerVertexLighting_materialDiffuseUniform, 1, pep_PerVertexLighting_materialDiffuse);
        glUniform3fv(pep_PerVertexLighting_materialSpecularUniform, 1, pep_PerVertexLighting_materialSpecular);
        glUniform1f(pep_PerVertexLighting_materialShinessUniform, pep_PerVertexLighting_materialShiness);
    }
    else
    {
        glUniform1i(pep_PerVertexLighting_keyLPressedUniform, 0);
    }

    glBindVertexArray(pep_PerVertexLighting_vao);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_PerVertexLighting_vbo_element);
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawElements(GL_TRIANGLES, pep_PerVertexLighting_gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void PerVertexLighting_Update(void)
{
    return;
}

void PerVertexLighting_ReSize(int width, int height)
{

}

void PerVertexLighting_Uninitialize(void)
{

    if (pep_PerVertexLighting_vbo_element)
    {
        glDeleteBuffers(1, &pep_PerVertexLighting_vbo_element);
        pep_PerVertexLighting_vbo_element = 0;
    }

    if (pep_PerVertexLighting_vbo_normal)
    {
        glDeleteBuffers(1, &pep_PerVertexLighting_vbo_normal);
        pep_PerVertexLighting_vbo_normal = 0;
    }

    if (pep_PerVertexLighting_vbo_position)
    {
        glDeleteBuffers(1, &pep_PerVertexLighting_vbo_position);
        pep_PerVertexLighting_vbo_position = 0;
    }

    if (pep_PerVertexLighting_vao)
    {
        glDeleteVertexArrays(1, &pep_PerVertexLighting_vao);
        pep_PerVertexLighting_vao = 0;
    }

    if (pep_PerVertexLighting_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_PerVertexLighting_gShaderProgramObject);

        glGetProgramiv(pep_PerVertexLighting_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_PerVertexLighting_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_PerVertexLighting_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_PerVertexLighting_gShaderProgramObject);
        pep_PerVertexLighting_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

void PerVertexLighting_WmKeydown(int keyPressed)
{
    switch (keyPressed)
    {
    case 'L':
    case 'l':
        if (false == pep_PerVertexLighting_bIsLightEnable)
        {
            pep_PerVertexLighting_bIsLightEnable = true;
        }
        else
        {
            pep_PerVertexLighting_bIsLightEnable = false;
        }
        break;
    }

    return;
}
