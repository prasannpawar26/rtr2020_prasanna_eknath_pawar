#pragma once

int CheckerBoard_Initialize(void);
void CheckerBoard_Display(void);
void CheckerBoard_Update(void);
void CheckerBoard_ReSize(int width, int height);
void CheckerBoard_Uninitialize(void);

