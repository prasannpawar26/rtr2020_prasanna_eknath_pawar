#include "Common.h"
#include "23_TwoSteadyLights.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_TwoSteadyLights_gVertexShaderObject;
GLuint pep_TwoSteadyLights_gFragmentShaderObject;
GLuint pep_TwoSteadyLights_gShaderProgramObject;

GLuint pep_TwoSteadyLights_vao;
GLuint pep_TwoSteadyLights_vbo_position;
GLuint pep_TwoSteadyLights_vbo_normal;

GLfloat pep_TwoSteadyLights_PyramidRotation;

unsigned int pep_TwoSteadyLights_gNumVertices;
unsigned int pep_TwoSteadyLights_gNumElements;

// UNIFORMS
GLuint pep_TwoSteadyLights_modelMatrixUniform;
GLuint pep_TwoSteadyLights_viewMatrixUniform;
GLuint pep_TwoSteadyLights_projectionMatrixUniform;

GLuint pep_TwoSteadyLights_lightAmbient0Uniform0;
GLuint pep_TwoSteadyLights_lightDiffuse0Uniform0;
GLuint pep_TwoSteadyLights_lightSpecular0Uniform0;
GLuint pep_TwoSteadyLights_lightPosition0Uniform0;

GLuint pep_TwoSteadyLights_lightAmbient0Uniform1;
GLuint pep_TwoSteadyLights_lightDiffuse0Uniform1;
GLuint pep_TwoSteadyLights_lightSpecular0Uniform1;
GLuint pep_TwoSteadyLights_lightPosition0Uniform1;


GLuint pep_TwoSteadyLights_materialAmbientUniform;
GLuint pep_TwoSteadyLights_materialDiffuseUniform;
GLuint pep_TwoSteadyLights_materialSpecularUniform;
GLuint pep_TwoSteadyLights_materialShinessUniform;

GLuint pep_TwoSteadyLights_keyLPressedUniform;

//
// Application Variables Associated With Uniforms
float pep_TwoSteadyLights_lightAmbient0[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_TwoSteadyLights_lightDiffuse0[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float pep_TwoSteadyLights_lightSpecular0[4] = {1.0f, 0.0f, 0.0f, 1.0f};
float pep_TwoSteadyLights_lightPosition0[4] = {-2.0f, 0.0f, 0.0f, 1.0f};

float pep_TwoSteadyLights_lightAmbient1[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_TwoSteadyLights_lightDiffuse1[4] = {0.0f, 0.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_lightSpecular1[4] = {0.0f, 0.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_lightPosition1[4] = {2.0f, 0.0f, 0.0f, 1.0f};

float pep_TwoSteadyLights_materialAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_TwoSteadyLights_materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_TwoSteadyLights_materialShiness = 50.0f;  // Also Try 128.0f

bool pep_TwoSteadyLights_bIsLightEnable = true;

int TwoSteadyLights_Initialize(void)
{
    fprintf(pep_gpFile, "\nTwoSteadyLights\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    //
    // Pyramid
    //
    const GLfloat pyramidVertices[] = {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };
    const GLfloat pyramidNormals[] = {
        0.0f, 0.447214f, 0.894427f,// front-top
        0.0f, 0.447214f, 0.894427f,// front-left
        0.0f, 0.447214f, 0.894427f,// front-right

        0.894427f, 0.447214f, 0.0f, // right-top
        0.894427f, 0.447214f, 0.0f, // right-left
        0.894427f, 0.447214f, 0.0f, // right-right

        0.0f, 0.447214f, -0.894427f, // back-top
        0.0f, 0.447214f, -0.894427f, // back-left
        0.0f, 0.447214f, -0.894427f, // back-right

        -0.894427f, 0.447214f, 0.0f, // left-top
        -0.894427f, 0.447214f, 0.0f, // left-left
        -0.894427f, 0.447214f, 0.0f // left-right
    };

    pep_TwoSteadyLights_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"

        "uniform vec3 u_light_ambient0;"
        "uniform vec3 u_light_diffuse0;"
        "uniform vec3 u_light_specular0;"
        "uniform vec4 u_light_position0;"

        "uniform vec3 u_light_ambient1;"
        "uniform vec3 u_light_diffuse1;"
        "uniform vec3 u_light_specular1;"
        "uniform vec4 u_light_position1;"

        "uniform vec3 u_material_ambient;"
        "uniform vec3 u_material_diffuse;"
        "uniform vec3 u_material_specular;"
        "uniform float u_material_shiness;"

        "uniform int u_key_L_pressed;"

        "out vec3 phong_ads_light;"

        "void main(void)"
        "{"
            "if(1 == u_key_L_pressed)"
            "{"
                "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
                "vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
                "vec3 viewer_vector = normalize(vec3(-eye_coordinates));"

                "vec3 light_direction0 = normalize(vec3(u_light_position0 - eye_coordinates));"
                "float t_normal_dot_light_direction0 = max(dot(light_direction0, t_normal), 0.0f);"
                "vec3 reflection_vector0 = reflect(-light_direction0, t_normal);"

                "vec3 light_direction1 = normalize(vec3(u_light_position1 - eye_coordinates));"
                "float t_normal_dot_light_direction1 = max(dot(light_direction1, t_normal), 0.0f);"
                "vec3 reflection_vector1 = reflect(-light_direction1, t_normal);"

                "vec3 ambient;"
                "vec3 diffuse;"
                "vec3 specular;"

                "ambient = u_light_ambient0 * u_material_ambient;"
                "diffuse = u_light_diffuse0 * u_material_diffuse * t_normal_dot_light_direction0;"
                "specular = u_light_specular0 * u_material_specular * pow(max(dot(reflection_vector0, viewer_vector), 0.0f), u_material_shiness);"

                "ambient += u_light_ambient1 * u_material_ambient;"
                "diffuse += u_light_diffuse1 * u_material_diffuse * t_normal_dot_light_direction1;"
                "specular += u_light_specular1 * u_material_specular * pow(max(dot(reflection_vector1, viewer_vector), 0.0f), u_material_shiness);"

                "phong_ads_light = ambient + diffuse + specular;"
            "}"
            "else"
            "{"
               "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
            "}"

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

    glShaderSource(pep_TwoSteadyLights_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_TwoSteadyLights_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_TwoSteadyLights_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TwoSteadyLights_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TwoSteadyLights_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_TwoSteadyLights_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec3 phong_ads_light;"
        "uniform int u_key_L_pressed;"
        "out vec4 FragColor;"

        "void main(void)"
        "{"
            "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

    glShaderSource(pep_TwoSteadyLights_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_TwoSteadyLights_gFragmentShaderObject);

    glGetShaderiv(pep_TwoSteadyLights_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TwoSteadyLights_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TwoSteadyLights_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_TwoSteadyLights_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_TwoSteadyLights_gShaderProgramObject, pep_TwoSteadyLights_gVertexShaderObject);
    glAttachShader(pep_TwoSteadyLights_gShaderProgramObject, pep_TwoSteadyLights_gFragmentShaderObject);

    glBindAttribLocation(pep_TwoSteadyLights_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_TwoSteadyLights_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_TwoSteadyLights_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_TwoSteadyLights_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_TwoSteadyLights_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_TwoSteadyLights_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    //pep_TwoSteadyLights_mvpUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_mvp_matrix");
    pep_TwoSteadyLights_modelMatrixUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_model_matrix");
    pep_TwoSteadyLights_viewMatrixUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_view_matrix");
    pep_TwoSteadyLights_projectionMatrixUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_projection_matrix");

    pep_TwoSteadyLights_lightAmbient0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_ambient0");
    pep_TwoSteadyLights_lightDiffuse0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_diffuse0");
    pep_TwoSteadyLights_lightSpecular0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_specular0");
    pep_TwoSteadyLights_lightPosition0Uniform0 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_position0");

    pep_TwoSteadyLights_lightAmbient0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_ambient1");
    pep_TwoSteadyLights_lightDiffuse0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_diffuse1");
    pep_TwoSteadyLights_lightSpecular0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_specular1");
    pep_TwoSteadyLights_lightPosition0Uniform1 = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_light_position1");

    pep_TwoSteadyLights_materialAmbientUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_ambient");
    pep_TwoSteadyLights_materialDiffuseUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_diffuse");
    pep_TwoSteadyLights_materialSpecularUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_specular");
    pep_TwoSteadyLights_materialShinessUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_material_shiness");

    pep_TwoSteadyLights_keyLPressedUniform = glGetUniformLocation(pep_TwoSteadyLights_gShaderProgramObject, "u_key_L_pressed");

    glGenVertexArrays(1, &pep_TwoSteadyLights_vao);
    glBindVertexArray(pep_TwoSteadyLights_vao);

    glGenBuffers(1, &pep_TwoSteadyLights_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoSteadyLights_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_TwoSteadyLights_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TwoSteadyLights_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void TwoSteadyLights_Display(void)
{
    // Render
    glUseProgram(pep_TwoSteadyLights_gShaderProgramObject);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;

    //
    // Cube
    //
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    viewMatrix = translate(0.0f, 0.0f, -6.0f) * rotate(pep_TwoSteadyLights_PyramidRotation, 0.0f, 1.0f, 0.0f);;
    projectionMatrix = pep_Perspective_ProjectionMatrix;

    glUniformMatrix4fv(pep_TwoSteadyLights_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_TwoSteadyLights_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_TwoSteadyLights_projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);
 
    if (pep_TwoSteadyLights_bIsLightEnable)
    {
        glUniform1i(pep_TwoSteadyLights_keyLPressedUniform, 1);
        glUniform3fv(pep_TwoSteadyLights_lightAmbient0Uniform0, 1, pep_TwoSteadyLights_lightAmbient0);
        glUniform3fv(pep_TwoSteadyLights_lightDiffuse0Uniform0, 1, pep_TwoSteadyLights_lightDiffuse0);
        glUniform3fv(pep_TwoSteadyLights_lightSpecular0Uniform0, 1, pep_TwoSteadyLights_lightSpecular0);
        glUniform4fv(pep_TwoSteadyLights_lightPosition0Uniform0, 1, pep_TwoSteadyLights_lightPosition0);

        glUniform3fv(pep_TwoSteadyLights_lightAmbient0Uniform1, 1, pep_TwoSteadyLights_lightAmbient1);
        glUniform3fv(pep_TwoSteadyLights_lightDiffuse0Uniform1, 1, pep_TwoSteadyLights_lightDiffuse1);
        glUniform3fv(pep_TwoSteadyLights_lightSpecular0Uniform1, 1, pep_TwoSteadyLights_lightSpecular1);
        glUniform4fv(pep_TwoSteadyLights_lightPosition0Uniform1, 1, pep_TwoSteadyLights_lightPosition1);

        glUniform3fv(pep_TwoSteadyLights_materialAmbientUniform, 1, pep_TwoSteadyLights_materialAmbient);
        glUniform3fv(pep_TwoSteadyLights_materialDiffuseUniform, 1, pep_TwoSteadyLights_materialDiffuse);
        glUniform3fv(pep_TwoSteadyLights_materialSpecularUniform, 1, pep_TwoSteadyLights_materialSpecular);
        glUniform1f(pep_TwoSteadyLights_materialShinessUniform, pep_TwoSteadyLights_materialShiness);
    }
    else
    {
        glUniform1i(pep_TwoSteadyLights_keyLPressedUniform, 0);
    }

    glBindVertexArray(pep_TwoSteadyLights_vao);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void TwoSteadyLights_Update(void)
{
    pep_TwoSteadyLights_PyramidRotation += 0.5f;
    if (pep_TwoSteadyLights_PyramidRotation > 360.0f)
    {
        pep_TwoSteadyLights_PyramidRotation = 0.0f;
    }
    return;
}

void TwoSteadyLights_ReSize(int width, int height)
{

}

void TwoSteadyLights_Uninitialize(void)
{
    if (pep_TwoSteadyLights_vbo_normal)
    {
        glDeleteBuffers(1, &pep_TwoSteadyLights_vbo_normal);
        pep_TwoSteadyLights_vbo_normal = 0;
    }

    if (pep_TwoSteadyLights_vbo_position)
    {
        glDeleteBuffers(1, &pep_TwoSteadyLights_vbo_position);
        pep_TwoSteadyLights_vbo_position = 0;
    }

    if (pep_TwoSteadyLights_vao)
    {
        glDeleteVertexArrays(1, &pep_TwoSteadyLights_vao);
        pep_TwoSteadyLights_vao = 0;
    }

    if (pep_TwoSteadyLights_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_TwoSteadyLights_gShaderProgramObject);

        glGetProgramiv(pep_TwoSteadyLights_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_TwoSteadyLights_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_TwoSteadyLights_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_TwoSteadyLights_gShaderProgramObject);
        pep_TwoSteadyLights_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

void TwoSteadyLights_WmKeydown(int keyPressed)
{
    switch (keyPressed)
    {
    case 'L':
    case 'l':
        if (false == pep_TwoSteadyLights_bIsLightEnable)
        {
            pep_TwoSteadyLights_bIsLightEnable = true;
        }
        else
        {
            pep_TwoSteadyLights_bIsLightEnable = false;
        }
        break;
    }

    return;
}
