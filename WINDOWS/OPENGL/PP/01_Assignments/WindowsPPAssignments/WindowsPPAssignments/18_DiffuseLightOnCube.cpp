#include "Common.h"
#include "18_DiffuseLightOnCube.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_DiffuseLightOnCube_gVertexShaderObject;
GLuint pep_DiffuseLightOnCube_gFragmentShaderObject;
GLuint pep_DiffuseLightOnCube_gShaderProgramObject;

GLuint pep_DiffuseLightOnCube_Cube_vao;
GLuint pep_DiffuseLightOnCube_Cube_vbo_position;
GLuint pep_DiffuseLightOnCube_Cube_vbo_normal;
GLfloat pep_DiffuseLightOnCube_CubeRotation;

GLuint pep_DiffuseLightOnCube_modelViewMatrixUniform;
GLuint pep_DiffuseLightOnCube_projectionMatrixUniform;
GLuint pep_DiffuseLightOnCube_lightDiffuseUniform;
GLuint pep_DiffuseLightOnCube_materialDiffuseUniform;
GLuint pep_DiffuseLightOnCube_lightPositionUniform;
GLuint pep_DiffuseLightOnCube_keyLPressedUniform;

bool pep_DiffuseLightOnCube_bIsAnimation = true;
bool pep_DiffuseLightOnCube_bIsLightEnable = true;

int DiffuseLightOnCube_Initialize(void)
{
    fprintf(pep_gpFile, "\nDiffuseLightOnCube\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_DiffuseLightOnCube_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec3 vNormal;" \

        "uniform mat4 u_modelview_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_key_L_pressed;" \
        "uniform vec3 u_light_diffuse;" \
        "uniform vec3 u_material_diffuse;" \
        "uniform vec4 u_light_position;" \

        "out vec3 diffuse_light;" \

        "void main(void)" \
        "{" \
            "if(1 == u_key_L_pressed)" \
            "{" \
                "vec4 eye_coordinates = u_modelview_matrix * vPosition;" \

                "mat3 normalMatrix = mat3(transpose(inverse(u_modelview_matrix)));" \

                "vec3 t_normal = normalize(normalMatrix * vNormal);" \

                "vec3 lightSource = normalize(vec3(u_light_position - eye_coordinates));" \

                "diffuse_light = u_light_diffuse * u_material_diffuse * max(dot(lightSource, t_normal), 0.0);" \
            "}" \
            "else" \
            "{" \
                "diffuse_light = vec3(1.0, 1.0, 1.0);" \
            "}" \
    

            "gl_Position = u_projection_matrix * u_modelview_matrix * vPosition;" \
        "}";

    glShaderSource(pep_DiffuseLightOnCube_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_DiffuseLightOnCube_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_DiffuseLightOnCube_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DiffuseLightOnCube_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DiffuseLightOnCube_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_DiffuseLightOnCube_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec3 diffuse_light;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = vec4(diffuse_light, 1.0);" \
        "}";

    glShaderSource(pep_DiffuseLightOnCube_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_DiffuseLightOnCube_gFragmentShaderObject);

    glGetShaderiv(pep_DiffuseLightOnCube_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DiffuseLightOnCube_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DiffuseLightOnCube_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DiffuseLightOnCube_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_DiffuseLightOnCube_gShaderProgramObject, pep_DiffuseLightOnCube_gVertexShaderObject);
    glAttachShader(pep_DiffuseLightOnCube_gShaderProgramObject, pep_DiffuseLightOnCube_gFragmentShaderObject);

    glBindAttribLocation(pep_DiffuseLightOnCube_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_DiffuseLightOnCube_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_DiffuseLightOnCube_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_DiffuseLightOnCube_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_DiffuseLightOnCube_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_DiffuseLightOnCube_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DiffuseLightOnCube_modelViewMatrixUniform = glGetUniformLocation(pep_DiffuseLightOnCube_gShaderProgramObject, "u_modelview_matrix");
    pep_DiffuseLightOnCube_projectionMatrixUniform = glGetUniformLocation(pep_DiffuseLightOnCube_gShaderProgramObject, "u_projection_matrix");
    pep_DiffuseLightOnCube_lightDiffuseUniform = glGetUniformLocation(pep_DiffuseLightOnCube_gShaderProgramObject, "u_light_diffuse");
    pep_DiffuseLightOnCube_materialDiffuseUniform = glGetUniformLocation(pep_DiffuseLightOnCube_gShaderProgramObject, "u_material_diffuse");
    pep_DiffuseLightOnCube_lightPositionUniform = glGetUniformLocation(pep_DiffuseLightOnCube_gShaderProgramObject, "u_light_position");
    pep_DiffuseLightOnCube_keyLPressedUniform = glGetUniformLocation(pep_DiffuseLightOnCube_gShaderProgramObject, "u_key_L_pressed");

    //
    // Cube
    //
    const GLfloat cubeVertices[] = {
        // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,  

        // bottom
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,

        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // back
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, -1.0f, -1.0f, 
        -1.0f, -1.0f, 1.0f, 
    };
    const GLfloat cubeNormals[] = {
        0.0f,  1.0f,  0.0f,
        0.0f,  1.0f,  0.0f,
        0.0f,  1.0f,  0.0f,
        0.0f,  1.0f,  0.0f,
        0.0f,  -1.0f, 0.0f,
        0.0f,  -1.0f, 0.0f,
        0.0f,  -1.0f, 0.0f,
        0.0f,  -1.0f, 0.0f,
        0.0f,  0.0f,  1.0f,
        0.0f,  0.0f,  1.0f,
        0.0f,  0.0f,  1.0f,
        0.0f,  0.0f,  1.0f,
        0.0f,  0.0f,  -1.0f,
        0.0f,  0.0f,  -1.0f,
        0.0f,  0.0f,  -1.0f,
        0.0f,  0.0f,  -1.0f,
        1.0f,  0.0f,  0.0f,
        1.0f,  0.0f,  0.0f,
        1.0f,  0.0f,  0.0f,
        1.0f,  0.0f,  0.0f,
        -1.0f, 0.0f,  0.0f,
        -1.0f, 0.0f,  0.0f,
        -1.0f, 0.0f,  0.0f,
        -1.0f, 0.0f,  0.0f
    };

    glGenVertexArrays(1, &pep_DiffuseLightOnCube_Cube_vao);
    glBindVertexArray(pep_DiffuseLightOnCube_Cube_vao);

    glGenBuffers(1, &pep_DiffuseLightOnCube_Cube_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_DiffuseLightOnCube_Cube_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_DiffuseLightOnCube_Cube_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_DiffuseLightOnCube_Cube_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void DiffuseLightOnCube_Display(void)
{
    // Render
    glUseProgram(pep_DiffuseLightOnCube_gShaderProgramObject);

    if (pep_DiffuseLightOnCube_bIsLightEnable)
    {
        glUniform1i(pep_DiffuseLightOnCube_keyLPressedUniform, 1);
        glUniform3f(pep_DiffuseLightOnCube_lightDiffuseUniform, 1.0f, 1.0f, 1.0f);
        glUniform3f(pep_DiffuseLightOnCube_materialDiffuseUniform, 0.50f, 0.50f, 0.50f);
        glUniform4f(pep_DiffuseLightOnCube_lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    }
    else
    {
        glUniform1i(pep_DiffuseLightOnCube_keyLPressedUniform, 0);
    }

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Cube
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -6.0f) * rotate(pep_DiffuseLightOnCube_CubeRotation, 1.0f, 0.0f, 0.0f) * rotate(pep_DiffuseLightOnCube_CubeRotation, 0.0f, 1.0f, 0.0f) * rotate(pep_DiffuseLightOnCube_CubeRotation, 0.0f, 0.0f, 1.0f);

    glUniformMatrix4fv(pep_DiffuseLightOnCube_modelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(pep_DiffuseLightOnCube_projectionMatrixUniform, 1, GL_FALSE, pep_Perspective_ProjectionMatrix);

    glBindVertexArray(pep_DiffuseLightOnCube_Cube_vao);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void DiffuseLightOnCube_Update(void)
{
    if (false == pep_DiffuseLightOnCube_bIsAnimation)
    {
        return;
    }

    pep_DiffuseLightOnCube_CubeRotation += 0.5f;
    if (pep_DiffuseLightOnCube_CubeRotation > 360.0f)
    {
        pep_DiffuseLightOnCube_CubeRotation = 0.0f;
    }

    return;
}

void DiffuseLightOnCube_ReSize(int width, int height)
{

}

void DiffuseLightOnCube_Uninitialize(void)
{
    if (pep_DiffuseLightOnCube_Cube_vbo_normal)
    {
        glDeleteBuffers(1, &pep_DiffuseLightOnCube_Cube_vbo_normal);
        pep_DiffuseLightOnCube_Cube_vbo_normal = 0;
    }

    if (pep_DiffuseLightOnCube_Cube_vbo_position)
    {
        glDeleteBuffers(1, &pep_DiffuseLightOnCube_Cube_vbo_position);
        pep_DiffuseLightOnCube_Cube_vbo_position = 0;
    }

    if (pep_DiffuseLightOnCube_Cube_vao)
    {
        glDeleteVertexArrays(1, &pep_DiffuseLightOnCube_Cube_vao);
        pep_DiffuseLightOnCube_Cube_vao = 0;
    }

    if (pep_DiffuseLightOnCube_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_DiffuseLightOnCube_gShaderProgramObject);

        glGetProgramiv(pep_DiffuseLightOnCube_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_DiffuseLightOnCube_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_DiffuseLightOnCube_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_DiffuseLightOnCube_gShaderProgramObject);
        pep_DiffuseLightOnCube_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

void DiffuseLightOnCube_WmKeydown(int keyPressed)
{
    switch (keyPressed)
    {
        case 'L':
        case 'l':
            if (false == pep_DiffuseLightOnCube_bIsLightEnable)
            {
                pep_DiffuseLightOnCube_bIsLightEnable = true;
            }
            else
            {
                pep_DiffuseLightOnCube_bIsLightEnable = false;
            }
        break;

        case 'A':
        case 'a':
            if (false == pep_DiffuseLightOnCube_bIsAnimation)
            {
                pep_DiffuseLightOnCube_bIsAnimation = true;
            }
            else
            {
                pep_DiffuseLightOnCube_bIsAnimation = false;
            }
        break;
    }

    return;
}
