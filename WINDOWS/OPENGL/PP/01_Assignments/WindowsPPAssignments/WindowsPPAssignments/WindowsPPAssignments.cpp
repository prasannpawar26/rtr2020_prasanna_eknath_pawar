#include "WindowsPPAssignments.h"

#include "01_BlueScreen.h"
#include "02_BlueScreenWithExtensionInfo.h"
#include "03_EmptyShader.h"
#include "04_SafeRelease.h"
#include "05_Ortho.h"
#include "06_Perspective.h"
#include "07_MultiColorTriangle.h"
#include "08_ColorSquare.h"
#include "09_TwoDShapes.h"
#include "10_TwoDShapes_Version2.h"
#include "11_TwoDShapesAnimation.h"
#include "12_ThreeDPyramid.h"
#include "13_ThreeDCube.h"
#include "14_ThreeDShapesAnimation.h"
#include "15_ThreeDShapesWithTexture.h"
#include "16_TweakedSmiley.h"
#include "17_CheckerBoard.h"
#include "18_DiffuseLightOnCube.h"
#include "19_BlackAndWhiteSphere.h"
#include "20_PerVertexLighting.h"
#include "21_PerFragmentLighting.h"
#include "22_ToggleLightShaders.h"
#include "23_TwoSteadyLights.h"
#include "SceneTransition.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;
DWORD pep_dwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool pep_gbActiveWindow = false;
bool pep_gbIsFullScreen = false;

int pep_AssignmentDisplay = 6;

mat4 pep_Perspective_ProjectionMatrix;

bool pep_bFadeOut = false;
unsigned int gNumberOfLineSegments;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    // Function Declarations
    int Initialize(void);
    void Display(void);
    void Update(void);

    // Variable Declarations
    bool bDone = false;
    MSG msg = {0};
    int iRet = 0;
    HWND hwnd;
    WNDCLASSEX wndClass;
    TCHAR szAppName[] = TEXT("Windows Programmable PipeLine Assignments");

    // Code
    if (0 != fopen_s(&pep_gpFile, "Log.txt", "w"))
    {
        MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
        exit(0);
    }

    fprintf(pep_gpFile, "Log File Created Successfully\n");

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.cbWndExtra = 0;
    wndClass.cbClsExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szAppName;
    wndClass.lpfnWndProc = WndProc;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

    RegisterClassEx(&wndClass);

    hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Programmable PipeLine Skeleton"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100, 100, 800, 600, NULL, NULL, hInstance, NULL);
    pep_gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    iRet = Initialize();
    if (-1 == iRet)
    {
        fprintf(pep_gpFile, "Choose Pixel Format Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-2 == iRet)
    {
        fprintf(pep_gpFile, "Set Pixel Format Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-3 == iRet)
    {
        fprintf(pep_gpFile, "wglCreateContext Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-4 == iRet)
    {
        fprintf(pep_gpFile, "wglMakeCurrent Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-5 == iRet)
    {
        fprintf(pep_gpFile, "glewInit Failed\n");
        DestroyWindow(hwnd);
    } else {
        fprintf(pep_gpFile, "Initialization Successful\n");
    }

    // GAME LOOP
    while (false == bDone)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (WM_QUIT == msg.message)
            {
                bDone = true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        } else
        {
            if (pep_gbActiveWindow)
            {
            }
            Update();

            Display();
        }
    }  // END OF WHILE

    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // Function Declarations
    void ToggledFullScreen(void);
    void UnInitialize(void);
    void ReSize(int, int);

    // Variable Declarations

    // Code
    switch (iMsg)
    {
    case WM_DESTROY:
        UnInitialize();
        PostQuitMessage(0);
        break;

    case WM_ERASEBKGND:
        return 0;
        break;

    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;

    case WM_SIZE:
        ReSize(LOWORD(lParam), HIWORD(lParam));
        break;


    case WM_KILLFOCUS:
        pep_gbActiveWindow = false;
        break;

    case WM_SETFOCUS:
        pep_gbActiveWindow = true;
        break;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_UP:
            gNumberOfLineSegments++;
            if (gNumberOfLineSegments >= 50) {
                gNumberOfLineSegments = 50;
            }
            break;

        case VK_DOWN:
            gNumberOfLineSegments--;
            if (gNumberOfLineSegments <= 0) {
                gNumberOfLineSegments = 1;
            }
            break;
        case VK_ESCAPE:
            DestroyWindow(hwnd);
            break;

        case 'E':
        case 'e':
            break;

        case 'F':
        case 'f':
            ToggledFullScreen();
            // As we are handling this on flag hence commented here
            /*if (22 == pep_AssignmentDisplay)
            {
                ToggleLightShaders_WmKeydown(wParam);
            }*/
            break;

        case 'V':
        case 'v':
            // As we are handling this on flag hence commented here
            /*if (22 == pep_AssignmentDisplay)
            {
                ToggleLightShaders_WmKeydown(wParam);
            }*/
            break;

        case 'N':
        case 'n':
            //pep_AssignmentDisplay++;
            break;

        case 'L':
        case 'l':
            if (17 == pep_AssignmentDisplay)
            {
                DiffuseLightOnCube_WmKeydown(wParam);
            }
            else if (20 == pep_AssignmentDisplay)
            {
                PerVertexLighting_WmKeydown(wParam);
            }
            else if (22 == pep_AssignmentDisplay)
            {
                ToggleLightShaders_WmKeydown(wParam);
            }
            break;

        case 'A':
        case 'a':
            if (17 == pep_AssignmentDisplay)
            {
                DiffuseLightOnCube_WmKeydown(wParam);
            }
            break;
        }
        break;
    }  // End Of Switch Case

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
    // Variable Declarations
    MONITORINFO mi;

    // Code
    if (false == pep_gbIsFullScreen)
    {
        pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
        {
            mi = {sizeof(MONITORINFO)};

            if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
                GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);
        pep_gbIsFullScreen = true;
    }
    else
    {
        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
        SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
            SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
            SWP_NOMOVE | SWP_NOSIZE);

        pep_gbIsFullScreen = false;
        ShowCursor(TRUE);
    }

    return;
}

int Initialize(void)
{
    // Variable Declarations
    int index;
    PIXELFORMATDESCRIPTOR pfd;

    // Function Declarations
    void ReSize(int, int);

    // Code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cBlueBits = 8;
    pfd.cGreenBits = 8;
    pfd.cAlphaBits = 8;

    pfd.cDepthBits = 32;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

    pep_gHdc = GetDC(pep_gHwnd);

    index = ChoosePixelFormat(pep_gHdc, &pfd);
    if (0 == index)
    {
        return -1;
    }

    if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd))
    {
        return -2;
    }

    pep_gHglrc = wglCreateContext(pep_gHdc);
    if (NULL == pep_gHglrc)
    {
        return -3;
    }

    if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
    {
        return -4;
    }

    GLenum result;

    result = glewInit();
    if (GLEW_OK != result)
    {
        return -5;
    }

    BlueScreen_Initialize();
    BlueScreenWithExtensionInfo_Initialize();
    EmptyShader_Initialize();
    SafeRelease_Initialize();
    Ortho_Initialize();
    Perspective_Initialize();
    MultiColorTriangle_Initialize();
    ColorSquare_Initialize();
    TwoDShapes_Initialize();
    TwoDShapes_InitializeV2();
    TwoDShapesAnimation_Initialize();
    ThreeDPyramid_Initialize();
    ThreeDCube_Initialize();
    ThreeDShapesAnimation_Initialize();
    ThreeDShapesWithTexture_Initialize();
    TweakedSmiley_Initialize();
    CheckerBoard_Initialize();
    DiffuseLightOnCube_Initialize();
    BlackAndWhiteSphere_Initialize();
    PerVertexLighting_Initialize();
    PerFragmentLighting_Initialize();
    ToggleLightShaders_Initialize();
    TwoSteadyLights_Initialize();
    SceneTransition_Initialize();

    glClearDepth(1.0f);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    pep_Perspective_ProjectionMatrix = mat4::identity();

    ReSize(800, 600);

    return 0;
}

void UnInitialize(void)
{
    //
    // Code
    //
    TwoSteadyLights_Uninitialize();
    ToggleLightShaders_Uninitialize();
    PerFragmentLighting_Uninitialize();
    PerVertexLighting_Uninitialize();
    BlackAndWhiteSphere_Uninitialize();
    DiffuseLightOnCube_Uninitialize();
    CheckerBoard_Uninitialize();
    TweakedSmiley_Uninitialize();
    SceneTransition_Uninitialize();
    ThreeDShapesWithTexture_Uninitialize();
    ThreeDShapesAnimation_Uninitialize();
    ThreeDCube_Uninitialize();
    ThreeDPyramid_Uninitialize();
    TwoDShapesAnimation_Uninitialize();
    TwoDShapes_UninitializeV2();
    TwoDShapes_Uninitialize();
    ColorSquare_Uninitialize();
    MultiColorTriangle_Uninitialize();
    Perspective_Uninitialize();
    Ortho_Uninitialize();
   /* SafeRelease_Uninitialize();
    EmptyShader_Uninitialize();
    BlueScreenWithExtensionInfo_Uninitialize();
    BlueScreen_Uninitialize();*/

    if (wglGetCurrentContext() == pep_gHglrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    if (NULL != pep_gHglrc)
    {
        wglDeleteContext(pep_gHglrc);
    }

    if (NULL != pep_gHdc)
    {
        ReleaseDC(pep_gHwnd, pep_gHdc);
    }

    fprintf(pep_gpFile, "Log File Closed Successfully\n");

    if (NULL != pep_gpFile)
    {
        fclose(pep_gpFile);
    }

    return;
}

void ReSize(int width, int height)
{
    if (0 == height)
    {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    pep_Perspective_ProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    if (0 == pep_AssignmentDisplay)
    {
       /* BlueScreen_ReSize(width, height);
        BlueScreenWithExtensionInfo_ReSize(width, height);
        EmptyShader_ReSize(width, height);
        SafeRelease_ReSize(width, height);*/
        Ortho_ReSize(width, height);
        Perspective_ReSize(width, height);
        MultiColorTriangle_ReSize(width, height);
        ColorSquare_ReSize(width, height);
        TwoDShapes_ReSize(width, height);
        TwoDShapes_ReSizeV2(width, height);
        TwoDShapesAnimation_ReSize(width, height);
        ThreeDPyramid_ReSize(width, height);
        ThreeDCube_ReSize(width, height);
        ThreeDShapesAnimation_ReSize(width, height);
        ThreeDShapesWithTexture_ReSize(width, height);
        TweakedSmiley_ReSize(width, height);
        CheckerBoard_ReSize(width, height);
        DiffuseLightOnCube_ReSize(width, height);
        BlackAndWhiteSphere_ReSize(width, height);
        PerVertexLighting_ReSize(width, height);
        PerFragmentLighting_ReSize(width, height);
        ToggleLightShaders_ReSize(width, height);
        TwoSteadyLights_ReSize(width, height);
        SceneTransition_ReSize(width, height);
    }
    else if (1 == pep_AssignmentDisplay)
    {
        BlueScreen_ReSize(width, height);
    }
    else if (2 == pep_AssignmentDisplay)
    {
        BlueScreenWithExtensionInfo_ReSize(width, height);
    }
    else if (3 == pep_AssignmentDisplay)
    {
        EmptyShader_ReSize(width, height);
    }
    else if (4 == pep_AssignmentDisplay)
    {
        SafeRelease_ReSize(width, height);
    }
    else if (5 == pep_AssignmentDisplay)
    {
        Ortho_ReSize(width, height);
    }
    else if (6 == pep_AssignmentDisplay)
    {
        Perspective_ReSize(width, height);
    }
    else if (7 == pep_AssignmentDisplay)
    {
        MultiColorTriangle_ReSize(width, height);
    }
    else if (8 == pep_AssignmentDisplay)
    {
        ColorSquare_ReSize(width, height);
    }
    else if (9 == pep_AssignmentDisplay)
    {
        TwoDShapes_ReSize(width, height);
    }
    else if (10 == pep_AssignmentDisplay)
    {
        TwoDShapes_ReSizeV2(width, height);
    }
    else if (11 == pep_AssignmentDisplay)
    {
        TwoDShapesAnimation_ReSize(width, height);
    }
    else if (12 == pep_AssignmentDisplay)
    {
        ThreeDPyramid_ReSize(width, height);
    }
    else if (13 == pep_AssignmentDisplay)
    {
        ThreeDCube_ReSize(width, height);
    }
    else if (14 == pep_AssignmentDisplay)
    {
        ThreeDShapesAnimation_ReSize(width, height);
    }
    else if (15 == pep_AssignmentDisplay)
    {
        ThreeDShapesWithTexture_ReSize(width, height);
    }
    else if (16 == pep_AssignmentDisplay)
    {
        TweakedSmiley_ReSize(width, height);
    }
    else if (17 == pep_AssignmentDisplay)
    {
        CheckerBoard_ReSize(width, height);
    }
    else if (18 == pep_AssignmentDisplay)
    {
        DiffuseLightOnCube_ReSize(width, height);
    }
    else if (19 == pep_AssignmentDisplay)
    {
        BlackAndWhiteSphere_ReSize(width, height);
    }
    else if (20 == pep_AssignmentDisplay)
    {
        PerVertexLighting_ReSize(width, height);
    }
    else if (21 == pep_AssignmentDisplay)
    {
        PerFragmentLighting_ReSize(width, height);
    }
    else if (22 == pep_AssignmentDisplay)
    {
        ToggleLightShaders_ReSize(width, height);
    }
    else if (23 == pep_AssignmentDisplay)
    {
        TwoSteadyLights_ReSize(width, height);
    }
    else if (24 == pep_AssignmentDisplay)
    {
        SceneTransition_ReSize(width, height);
    }

    return;
}

void Display(void)
{
    // Code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (1 == pep_AssignmentDisplay)
    {
        BlueScreen_Display();
    }
    else if (2 == pep_AssignmentDisplay)
    {
        BlueScreenWithExtensionInfo_Display();
    }
    else if (3 == pep_AssignmentDisplay)
    {
        EmptyShader_Display();
    }
    else if (4 == pep_AssignmentDisplay)
    {
        SafeRelease_Display();
    }
    else if (5 == pep_AssignmentDisplay)
    {
        Ortho_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f); // 0 to 1
        }
        else
        {
            SceneTransition_Display(false, -0.005f); // 1 to 0
        }
        
    }
    else if (6 == pep_AssignmentDisplay)
    {
        Perspective_Display();
        if (pep_bFadeOut)
        {
            //SceneTransition_Display(true, 0.005f);
        }
        else
        {
            //SceneTransition_Display(false, -0.005f);
        }
    }
    else if (7 == pep_AssignmentDisplay)
    {
        MultiColorTriangle_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (8 == pep_AssignmentDisplay)
    {
        ColorSquare_Display();
       /* if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }*/
    }
    else if (9 == pep_AssignmentDisplay)
    {
        TwoDShapes_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (10 == pep_AssignmentDisplay)
    {
        TwoDShapes_DisplayV2();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (11 == pep_AssignmentDisplay)
    {
        TwoDShapesAnimation_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (12 == pep_AssignmentDisplay)
    {
        ThreeDPyramid_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (13 == pep_AssignmentDisplay)
    {
        ThreeDCube_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (14 == pep_AssignmentDisplay)
    {
        ThreeDShapesAnimation_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (15 == pep_AssignmentDisplay)
    {
        ThreeDShapesWithTexture_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (16 == pep_AssignmentDisplay)
    {
        static bool outer_fade_out = false;
        //TweakedSmiley_Display(false);
        if (pep_bFadeOut)
        {
            TweakedSmiley_Display(true, &outer_fade_out);
            if (outer_fade_out)
            {
                SceneTransition_Display(true, 0.005f);
            }
        }
        else
        {
            TweakedSmiley_Display(false, &outer_fade_out);
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (17 == pep_AssignmentDisplay)
    {
        CheckerBoard_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (18 == pep_AssignmentDisplay)
    {
        DiffuseLightOnCube_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (19 == pep_AssignmentDisplay)
    {
        BlackAndWhiteSphere_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (20 == pep_AssignmentDisplay)
    {
        PerVertexLighting_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (21 == pep_AssignmentDisplay)
    {
        PerFragmentLighting_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (22 == pep_AssignmentDisplay)
    {
        static bool tls_outer_fade_out = false;
        //ToggleLightShaders_Display(false);
        if (pep_bFadeOut)
        {
            ToggleLightShaders_Display(true, &tls_outer_fade_out);
            if (tls_outer_fade_out)
            {
                SceneTransition_Display(true, 0.005f);
            }
        }
        else
        {
            ToggleLightShaders_Display(false, &tls_outer_fade_out);
            SceneTransition_Display(false, -0.005f);
        }
    }
    else if (23 == pep_AssignmentDisplay)
    {
        TwoSteadyLights_Display();
        if (pep_bFadeOut)
        {
            SceneTransition_Display(true, 0.005f);
        }
        else
        {
            SceneTransition_Display(false, -0.005f);
        }
    }

    SwapBuffers(pep_gHdc);

    return;
}

void Update(void)
{
    if (11 == pep_AssignmentDisplay)
    {
        TwoDShapesAnimation_Update();
    }
    else if (12 == pep_AssignmentDisplay)
    {
        ThreeDPyramid_Update();
    }
    else if (13 == pep_AssignmentDisplay)
    {
        ThreeDCube_Update();
    }
    else if (14 == pep_AssignmentDisplay)
    {
        ThreeDShapesAnimation_Update();
    }
    else if (15 == pep_AssignmentDisplay)
    {
        ThreeDShapesWithTexture_Update();
    }
    else if (18 == pep_AssignmentDisplay)
    {
        DiffuseLightOnCube_Update();
    }
    else if (23 == pep_AssignmentDisplay)
    {
        TwoSteadyLights_Update();
    }
   /* else if (13 == pep_AssignmentDisplay)
    {
        SceneTransition_Update();
    }*/
}
