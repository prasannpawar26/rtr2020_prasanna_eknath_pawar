#pragma once

int ThreeDPyramid_Initialize(void);
void ThreeDPyramid_Display(void);
void ThreeDPyramid_Update(void);
void ThreeDPyramid_ReSize(int width, int height);
void ThreeDPyramid_Uninitialize(void);

