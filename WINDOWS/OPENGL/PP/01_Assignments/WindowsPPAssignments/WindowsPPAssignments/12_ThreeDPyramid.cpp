#include "Common.h"
#include "12_ThreeDPyramid.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_ThreeDPyramid_gVertexShaderObject;
GLuint pep_ThreeDPyramid_gFragmentShaderObject;
GLuint pep_ThreeDPyramid_gShaderProgramObject;

GLuint pep_ThreeDPyramid_Pyramid_vao;
GLuint pep_ThreeDPyramid_Pyramid_vbo_position;
GLfloat pep_ThreeDPyramid_PyramidRotation;

GLuint pep_ThreeDPyramid_mvpUniform;

int ThreeDPyramid_Initialize(void)
{
    fprintf(pep_gpFile, "\nThreeDPyramid\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_ThreeDPyramid_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \

        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
        "}";

    glShaderSource(pep_ThreeDPyramid_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_ThreeDPyramid_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_ThreeDPyramid_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ThreeDPyramid_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ThreeDPyramid_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_ThreeDPyramid_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
        "}";

    glShaderSource(pep_ThreeDPyramid_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_ThreeDPyramid_gFragmentShaderObject);

    glGetShaderiv(pep_ThreeDPyramid_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ThreeDPyramid_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ThreeDPyramid_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ThreeDPyramid_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_ThreeDPyramid_gShaderProgramObject, pep_ThreeDPyramid_gVertexShaderObject);
    glAttachShader(pep_ThreeDPyramid_gShaderProgramObject, pep_ThreeDPyramid_gFragmentShaderObject);

    glBindAttribLocation(pep_ThreeDPyramid_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_ThreeDPyramid_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_ThreeDPyramid_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_ThreeDPyramid_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_ThreeDPyramid_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ThreeDPyramid_mvpUniform = glGetUniformLocation(pep_ThreeDPyramid_gShaderProgramObject, "u_mvp_matrix");

    //
    // Pyramid
    //
    const GLfloat triangleVertices[] = {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };
    const GLfloat triangleColor[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f
    };

    glGenVertexArrays(1, &pep_ThreeDPyramid_Pyramid_vao);
    glBindVertexArray(pep_ThreeDPyramid_Pyramid_vao);

    glGenBuffers(1, &pep_ThreeDPyramid_Pyramid_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDPyramid_Pyramid_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void ThreeDPyramid_Display(void)
{
    // Render
    glUseProgram(pep_ThreeDPyramid_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Pyramid
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -4.0f) * rotate(pep_ThreeDPyramid_PyramidRotation, 0.0f, 1.0f, 0.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_ThreeDPyramid_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    glBindVertexArray(pep_ThreeDPyramid_Pyramid_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDPyramid_Pyramid_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void ThreeDPyramid_Update(void)
{
    pep_ThreeDPyramid_PyramidRotation += 0.5f;
    if (pep_ThreeDPyramid_PyramidRotation > 360.0f)
    {
        pep_ThreeDPyramid_PyramidRotation = 0.0f;
    }

    return;
}

void ThreeDPyramid_ReSize(int width, int height)
{

}

void ThreeDPyramid_Uninitialize(void)
{
    if (pep_ThreeDPyramid_Pyramid_vbo_position)
    {
        glDeleteBuffers(1, &pep_ThreeDPyramid_Pyramid_vbo_position);
        pep_ThreeDPyramid_Pyramid_vbo_position = 0;
    }

    if (pep_ThreeDPyramid_Pyramid_vao)
    {
        glDeleteVertexArrays(1, &pep_ThreeDPyramid_Pyramid_vao);
        pep_ThreeDPyramid_Pyramid_vao = 0;
    }

    if (pep_ThreeDPyramid_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_ThreeDPyramid_gShaderProgramObject);

        glGetProgramiv(pep_ThreeDPyramid_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_ThreeDPyramid_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_ThreeDPyramid_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_ThreeDPyramid_gShaderProgramObject);
        pep_ThreeDPyramid_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
