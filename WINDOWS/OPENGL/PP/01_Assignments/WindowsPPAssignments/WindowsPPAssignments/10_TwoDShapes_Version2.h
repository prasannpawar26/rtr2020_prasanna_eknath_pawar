#pragma once

int TwoDShapes_InitializeV2(void);
void TwoDShapes_DisplayV2(void);
void TwoDShapes_UpdateV2(void);
void TwoDShapes_ReSizeV2(int width, int height);
void TwoDShapes_UninitializeV2(void);

