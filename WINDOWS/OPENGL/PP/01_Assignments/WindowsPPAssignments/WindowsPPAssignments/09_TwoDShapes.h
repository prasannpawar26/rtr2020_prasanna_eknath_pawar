#pragma once

int TwoDShapes_Initialize(void);
void TwoDShapes_Display(void);
void TwoDShapes_Update(void);
void TwoDShapes_ReSize(int width, int height);
void TwoDShapes_Uninitialize(void);

