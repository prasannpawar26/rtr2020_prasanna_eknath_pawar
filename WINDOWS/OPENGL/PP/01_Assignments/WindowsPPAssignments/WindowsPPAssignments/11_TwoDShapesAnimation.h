#pragma once

int TwoDShapesAnimation_Initialize(void);
void TwoDShapesAnimation_Display(void);
void TwoDShapesAnimation_Update(void);
void TwoDShapesAnimation_ReSize(int width, int height);
void TwoDShapesAnimation_Uninitialize(void);

