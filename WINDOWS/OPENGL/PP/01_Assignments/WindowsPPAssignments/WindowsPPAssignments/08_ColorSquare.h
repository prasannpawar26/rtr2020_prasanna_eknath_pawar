#pragma once

int ColorSquare_Initialize(void);
void ColorSquare_Display(void);
void ColorSquare_Update(void);
void ColorSquare_ReSize(int width, int height);
void ColorSquare_Uninitialize(void);

