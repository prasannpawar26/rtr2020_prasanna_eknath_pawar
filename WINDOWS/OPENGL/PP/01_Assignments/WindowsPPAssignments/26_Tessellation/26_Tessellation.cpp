#include <stdio.h>
#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giTessellationControlShaderObject;
GLuint giTessellationEvaluationShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao;
GLuint vbo;
GLuint mvpUniform;
GLuint gNumberOfSegmentsUniform;
GLuint gNumberOfStripsUniform;
GLuint gLineColorUniform;

unsigned int gNumberOfLineSegments;

mat4 gPerspectiveProjectionMatrix; // ->declared in vmath.h

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow) {
	// function prototype
	int Initialize(void);
	void Display(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("TessellationShader");

	// code
	if (0 != fopen_s(&gpFile, "log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}

	fprintf(gpFile, "Log File Created Successfully\n");

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("TessellationShader"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet) {
		fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
		DestroyWindow(hwnd);
	} else if (-2 == iRet) {
		fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
		DestroyWindow(hwnd);
	} else if (-3 == iRet) {
		fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
		DestroyWindow(hwnd);
	} else if (-4 == iRet) {
		fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
		DestroyWindow(hwnd);
	} else if (-5 == iRet) {
		fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
		DestroyWindow(hwnd);
	} else if (-6 == iRet) {
		DestroyWindow(hwnd);
	} else if (-7 == iRet) {
		DestroyWindow(hwnd);
	} else if (-8 == iRet) {
		DestroyWindow(hwnd);
	} else if (-9 == iRet) {
		DestroyWindow(hwnd);
	} else if (-10 == iRet) {
		DestroyWindow(hwnd);
	} else {
		fprintf(gpFile, "Initialize Successful.\n");
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message) {
				bDone = true;
			} else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} else {
			if (gbActiveWindow) {
			}

			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// code
	switch (iMsg) {

	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) { // if 0, the window is active
			gbActiveWindow = true;
		} else {
			gbActiveWindow = false;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;

	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN: {
		switch (wParam) {
		case VK_UP:
			gNumberOfLineSegments++;
			if (gNumberOfLineSegments >= 50) {
				gNumberOfLineSegments = 50;
			}
			break;

		case VK_DOWN:
			gNumberOfLineSegments--;
			if (gNumberOfLineSegments <= 0) {
				gNumberOfLineSegments = 1;
			}
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'F':
		case 'f':
			ToggledFullScreen();
			break;
		}
	} break;

	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

/*
Algorithm (Only Regarding Shaders):
1. Create Shader
2. Shader Source Code
3. Compile Shader
4. Create Shader program
5. Attach Shaders

6. Pre-Linking Bind **Attribute Location** Of Vertex Data CPU(application
program variable) to GPU(shader program variable)

7. Link Shader Program
8. Obtain the location of a uniform variable (of shader) i.e. GPU -> CPU
9.
10. Create Storage for vertex data (VBO) and Bind It
11. Trannsfer Vertex Data(CPU) To GPU Buffer
12. Attach/ Map attribute pointer To VBO's Buffer

13. Enable Vertex Attribute Array Tell GPU Drvier To access Vertex Data Which
Is Store In VBO and used for rendering when calls are made to vertex array
commands such as glDrawArrays.
14. UnBind VBO
15. UnBInd VAO
*/
int Initialize(void)
{
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	// function declarations
	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc) {
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result) {
		return -5;
	}

	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject) {
		fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 vPosition;" \
		"void main(void)" \
		"{" \
		"gl_Position = vec4(vPosition, 0.0, 1.0);" \
		"}";

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

	giTessellationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
	const GLchar *tessellationControlShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(vertices=4)out;" \
		"uniform int numberOfSegments;" \
		"uniform int numberOfStrips;" \
		"void main(void)" \
		"{" \
		"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \
		"gl_TessLevelOuter[0] = float(numberOfStrips);" \
		"gl_TessLevelOuter[1] = float(numberOfSegments);" \
		"}";
	glShaderSource(giTessellationControlShaderObject, 1,
		(const GLchar **)&tessellationControlShaderSourceCode, NULL);

	glCompileShader(giTessellationControlShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giTessellationControlShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giTessellationControlShaderObject, GL_INFO_LOG_LENGTH,
			&iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giTessellationControlShaderObject, iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "Error: Tessellation Control Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -8;
			}
		}
	}
	fprintf(gpFile, "Tessellation Control Shader Compiled Successfully\n");

	giTessellationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

	const GLchar *tessellationEvaluationShaderSourceCode =
		"#version 450 core"
		"\n"
		"layout(isolines)in;"
		"uniform mat4 u_mvp_matrix;"
		"void main(void)"
		"{"
		"float u = gl_TessCoord.x;"
		"vec3 p0 = gl_in[0].gl_Position.xyz;"
		"vec3 p1 = gl_in[1].gl_Position.xyz;"
		"vec3 p2 = gl_in[2].gl_Position.xyz;"
		"vec3 p3 = gl_in[3].gl_Position.xyz;"

		"float u1 = (1.0 - u);"
		"float u2 = u * u;"

		"float b3 = u2 * u;"
		"float b2 = 3.0 * u2 * u1;"
		"float b1 = 3.0 * u * u1 * u1;"
		"float b0 = u1 * u1 * u1;"
		"vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;"
		"gl_Position = u_mvp_matrix * vec4(p, 1.0);"
		"}";
	glShaderSource(giTessellationEvaluationShaderObject, 1,
		(const GLchar **)&tessellationEvaluationShaderSourceCode,
		NULL);

	glCompileShader(giTessellationEvaluationShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giTessellationEvaluationShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giTessellationEvaluationShaderObject, GL_INFO_LOG_LENGTH,
			&iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giTessellationEvaluationShaderObject, iInfoLogLength,
					&written, szInfoLog);
				fprintf(gpFile, "Error: Tessellation Evaluation Shader:\n\t%s\n",
					szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile, "Tessellation Evaluation Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFragmentShaderObject) 
	{
		fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"uniform vec4 lineColor;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = lineColor;" \
		"}";

	glShaderSource(giFragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(giFragmentShaderObject);

	glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile, "Fragment Shader Compiled Successfully\n");

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);
	glAttachShader(giShaderProgramObject, giTessellationControlShaderObject);
	glAttachShader(giShaderProgramObject, giTessellationEvaluationShaderObject);
	glAttachShader(giShaderProgramObject, giFragmentShaderObject);

	// glBindAttribLocation
	//
	// Description:
	//  Used to associate a user-defined attribute variable(in Vertex Shader i.e
	//  GPU) in the program object specified by program with a generic vertex
	//  attribute index(i.e CPU)
	//
	// When program is made part of **current state**, values provided via the
	// generic vertex attribute index will modify the value of the user-defined
	// attribute variable specified by name.
	//
	// glBindAttribLocation can be called before any vertex shader objects are
	// bound to the specified program object.
	//
	// If name was bound previously, that information is lost. Thus you cannot
	// bind one user-defined attribute variable to multiple indices, but you can
	// bind multiple user-defined attribute variables to the same index.

	// Parameters:
	// 1st Parameter: Specifies the handle of the program object in which the
	// association is to be made.
	//
	// 2nd Parameter: Specifies the index of the generic vertex attribute to be
	// bound. => CPU Variable
	//
	// 3rd Parameter: containing the name of the vertex shader attribute variable
	// to which index is to be bound => GPU Variable
	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(gpFile, "Shader Program Linked Successfully\n");

	mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");
	gNumberOfSegmentsUniform =
		glGetUniformLocation(giShaderProgramObject, "numberOfSegments");
	gNumberOfStripsUniform =
		glGetUniformLocation(giShaderProgramObject, "numberOfStrips");
	gLineColorUniform = glGetUniformLocation(giShaderProgramObject, "lineColor");

	const GLfloat vertices[] = {-1.0f, -1.0f, -0.50f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f};

	/*
	If each time you called **glDrawArrays**, **glDrawElements**, or
	some other OpenGL function that required vertex data, the information was taken
	from the application�s memory, on a high performance system with a local GPU,
	this would mean that the data would be transferred from the application�s memory
	(attached to the CPU) across the bus connecting the CPU to the GPU (usually
	PCI-Express) to the GPU�s local memory so that it can work it. This would take
	so much time that it would slow down the application significantly.
	*/

	/*
	When the GPU accesses memory that is local to it (physically attached to the
	video card, for example), it is often several times, perhaps even orders of
	magnitude faster than accessing the same data stored in system memory.
	*/

	/*
	it is advantageous to copy the data to the GPU�s local memory once and then
	reuse that copy over and over again.
	*/

	/****************************************************************************
	Hence ensure that vertex data and other information required by the GPU is
	available and is stored in its memory.
	==> Below Code Is Written In Such Manner That It Will Be Store In GPU Memory.
	****************************************************************************/

	// Application may require several VBOs and many vertex attributes, a special
	// container object called a vertex array object(VAO) is available to OpenGL
	// to manage all of this state.
	//
	// A VAO is a container that packages together all of the state that can be set
	// by glVertexAttribPointer and a few other functions.
	//
	// Need to create and bind one before you can use any of the code in this section.
	//
	// Below Code creates and binds a single VAO.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// A VBO is a buffer object that represents storage for vertex data.
	// Data can be placed in these buffers with hints that tell OpenGL how you
	// plan to use it OpenGL can then use those hints to decide what it will do
	// with that data.
	//
	// To store vertex data into or retrieve vertex data from a buffer, it must be
	// bound to the GL_ARRAY_BUFFER binding
	//
	// To create one or more buffer objects, Hence Call Below API's.
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), vertices, GL_STATIC_DRAW);

	// When glVertexAttribPointer is called, the value of the attribute pointer is
	// not interpreted as a real, physical pointer to data in memory. The pointer
	// is actually interpreted as   an offset into the buffer object that is bound
	// to the GL_ARRAY_BUFFER binding       at the time of the call
	//  Also, a record of the currently bound buffer is made in the current VAO
	//  and used for that attribute.
	//
	// *****tells OpenGL which buffer contains the data*****

	/*
	It is therefore possible to use multiple buffers�one for each
	attribute�simultaneously by calling glBindBuffer followed by
	glVertexAttribPointer for each attribute. It is also possible to store several
	different attributes in a single buffer by interleaving them. To do this, call
	glVertexArrayPointer with the stride parameter set to the distance (in bytes)
	between attributes of the same type
	*/
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	/*
	to unbind a buffer without specifying a new buffer to use in its place, simply bind
	the name zero to the GL_ARRAY_BUFFER binding.
	*/
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glLineWidth(3.0f);

	gPerspectiveProjectionMatrix = mat4::identity();

	gNumberOfLineSegments = 1;

	ReSize(800, 600);

	return 0;
}

void ReSize(int width, int height)
{
	// code
	if (0 == height) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	return;
}

/*
Algorithm:

1. Use Shader Program - Installs a program object
2. Bind VAO
3. Update And Send Uniform
4. Draw
5. UnBind VAO
6. Invalidate program object
*/
void Display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(giShaderProgramObject);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(gNumberOfSegmentsUniform, gNumberOfLineSegments);
	glUniform1i(gNumberOfStripsUniform, 1);
	glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));

	TCHAR str[255];
	wsprintf(str, TEXT("Segments : %d"), gNumberOfLineSegments);
	SetWindowText(gHwnd, str);

	glBindVertexArray(vao);

	glPatchParameteri(GL_PATCH_VERTICES, 4);

	glDrawArrays(GL_PATCHES, 0, 4);

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(gHdc);

	return;
}

void Uninitialize(void)
{
	// code
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
	}

	if (giShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(giShaderProgramObject);

		glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(giShaderProgramObject);
		giShaderProgramObject = 0;

		glUseProgram(0);
	}

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(gpFile, "Log File Closed Successfully\n");

	if (NULL != gpFile)
	{
		fclose(gpFile);
	}

	return;
}
