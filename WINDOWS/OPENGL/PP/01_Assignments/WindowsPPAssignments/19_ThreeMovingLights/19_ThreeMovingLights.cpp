#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "Sphere.h"
#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;

DWORD pep_dwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool pep_gbActiveWindow;
bool pep_gbIsFullScreen;

bool pep_gbIsLightEnable;

bool pep_gbIsVertexShader = true;

GLuint pep_giVertexShaderObject;
GLuint pep_giFragmentShaderObject;
GLuint pep_giShaderProgramObject;

enum {
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0
};

GLuint pep_vao_sphere;
GLuint pep_vbo_sphere_vertex;
GLuint pep_vbo_sphere_normal;
GLuint pep_vbo_sphere_element;

float pep_sphere_vertices[1146];
float pep_sphere_normals[1146];
float pep_sphere_textures[764];
unsigned short pep_sphere_elements[2280];

unsigned int pep_gNumVertices;
unsigned int pep_gNumElements;

// UNIFORMS
GLuint pep_modelMatrixUniform;
GLuint pep_viewMatrixUniform;
GLuint pep_projectionMatrixUniform;
GLuint pep_materialAmbientUniform;
GLuint pep_materialDiffuseUniform;
GLuint pep_materialSpecularUniform;
GLuint pep_materialShinessUniform;
GLuint pep_keyLPressedUniform;

struct light_configuration_uniform {
	GLuint pep_lightAmbient;
	GLuint pep_lightDiffuse;
	GLuint pep_lightSpecular;
	GLuint pep_lightPosition;
};
struct light_configuration_uniform pep_light_configuration_uniform0;
struct light_configuration_uniform pep_light_configuration_uniform1;
struct light_configuration_uniform pep_light_configuration_uniform2;

struct light_configuration {
	float pep_lightAmbient[4];
	float pep_lightDiffuse[4];
	float pep_lightSpecular[4];
	float pep_lightPosition[4];
};
struct light_configuration pep_light0;
struct light_configuration pep_light1;
struct light_configuration pep_light2;

GLfloat pep_gLightAngle_Zero = 0.0f;
GLfloat pep_gLightAngle_One = 0.0f;
GLfloat pep_gLightAngle_Two = 0.0f;


float pep_materialAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float pep_materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_materialShiness = 50.0f;  // Also Try 128.0f

int pep_lKeyPressed = 0;

mat4 pep_modelMatrix;
mat4 pep_viewMatrix;
mat4 pep_perspectiveProjectionMatrix;
mat4 pep_translateMatrix;

const GLchar *gc_PerVertex_vertexShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec4 vPosition;" \
"in vec3 vNormal;" \

"struct matrices {" \
"mat4 model_matrix;" \
"mat4 view_matrix;" \
"mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \

"struct light_configuration {" \
"vec3 light_ambient;" \
"vec3 light_diffuse;" \
"vec3 light_specular;" \
"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration0;" \
"uniform light_configuration u_light_configuration1;" \
"uniform light_configuration u_light_configuration2;" \

"struct material_configuration {" \
"vec3 material_ambient;" \
"vec3 material_diffuse;" \
"vec3 material_specular;" \
"float material_shiness;" \
"};" \
"uniform material_configuration u_material_configuration;" \

"uniform int u_key_L_pressed;" \
"out vec3 phong_ads_light;" \

"void main(void)" \
"{" \
"if(1 == u_key_L_pressed)" \
"{" \
"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
"vec3 t_normal = normalize(mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal);" \
"vec3 viewer_vector = normalize(vec3(-eye_coordinates));"\

"vec3 light_direction0 = normalize(vec3(u_light_configuration0.light_position - eye_coordinates));" \
"float t_normal_dot_light_direction0 = max(dot(light_direction0, t_normal), 0.0f);" \
"vec3 reflection_vector0 = reflect(-light_direction0, t_normal);" \
"vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" \
"vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector), 0.0f), u_material_configuration.material_shiness);" \

"vec3 light_direction1 = normalize(vec3(u_light_configuration1.light_position - eye_coordinates));" \
"float t_normal_dot_light_direction1 = max(dot(light_direction1, t_normal), 0.0f);" \
"vec3 reflection_vector1 = reflect(-light_direction1, t_normal);" \
"vec3 ambient1 = u_light_configuration1.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse1 = u_light_configuration1.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction1;" \
"vec3 specular1 = u_light_configuration1.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector1, viewer_vector), 0.0f), u_material_configuration.material_shiness);" \

"vec3 light_direction2 = normalize(vec3(u_light_configuration2.light_position - eye_coordinates));" \
"float t_normal_dot_light_direction2 = max(dot(light_direction2, t_normal), 0.0f);" \
"vec3 reflection_vector2 = reflect(-light_direction2, t_normal);" \
"vec3 ambient2 = u_light_configuration2.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse2 = u_light_configuration2.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction2;" \
"vec3 specular2 = u_light_configuration2.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector2, viewer_vector), 0.0f), u_material_configuration.material_shiness);" \

"phong_ads_light = ambient0 + ambient1 + ambient2 + diffuse0 + diffuse1 + diffuse2 + specular0 + specular1 + specular2;" \

"}" \
"else" \
"{" \
"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
"}" \

"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
"}";

const GLchar *gc_PerVertex_fragmentShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec3 phong_ads_light;" \
"uniform int u_key_L_pressed;" \
"out vec4 FragColor;" \
"void main(void)" \
"{" \
"FragColor = vec4(phong_ads_light, 1.0);" \
"}"; \

const GLchar *gc_PerFragment_vertexShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec4 vPosition;" \
"in vec3 vNormal;" \
"struct matrices {" \
"mat4 model_matrix;" \
"mat4 view_matrix;" \
"mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \
"uniform int u_key_L_pressed;" \

"struct light_configuration {" \
"vec3 light_ambient;" \
"vec3 light_diffuse;" \
"vec3 light_specular;" \
"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration0;" \
"uniform light_configuration u_light_configuration1;" \
"uniform light_configuration u_light_configuration2;" \

"out vec3 light_direction0;" \
"out vec3 light_direction1;" \
"out vec3 light_direction2;" \

"out vec3 tranformation_matrix;" \
"out vec3 viewer_vector;" \
"void main(void)" \
"{" \
"if(1 == u_key_L_pressed)" \
"{" \
"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \

"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" \
"viewer_vector = vec3(-eye_coordinates);" \

"light_direction0 = vec3(u_light_configuration0.light_position - eye_coordinates);" \
"light_direction1 = vec3(u_light_configuration1.light_position - eye_coordinates);" \
"light_direction2 = vec3(u_light_configuration2.light_position - eye_coordinates);" \

"}" \

"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * " \
"vPosition;" \
"}";

const GLchar *gc_PerFragment_fragmentShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec3 light_direction0;" \
"in vec3 light_direction1;" \
"in vec3 light_direction2;" \
"in vec3 tranformation_matrix;" \
"in vec3 viewer_vector;" \

"uniform int u_key_L_pressed;" \
"struct light_configuration {" \
"vec3 light_ambient;" \
"vec3 light_diffuse;" \
"vec3 light_specular;" \
"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration0;" \
"uniform light_configuration u_light_configuration1;" \
"uniform light_configuration u_light_configuration2;" \

"struct material_configuration {"
"vec3 material_ambient;" \
"vec3 material_diffuse;" \
"vec3 material_specular;" \
"float material_shiness;" \
"};" \
"uniform material_configuration u_material_configuration;" \

"out vec4 FragColor;" \
"vec3 phong_ads_light;" \
"void main(void)" \
"{" \
"if(1 == u_key_L_pressed)" \
"{" \
"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
"vec3 viewer_vector_normal = normalize(viewer_vector);" \

"vec3 light_direction_normalize0 = normalize(light_direction0);" \
"vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" \
"float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" \
"vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" \
"vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" \

"vec3 light_direction_normalize1 = normalize(light_direction1);" \
"vec3 reflection_vector1 = reflect(-light_direction_normalize1, tranformation_matrix_normalize);" \
"float t_normal_dot_light_direction1 = max(dot(light_direction_normalize1, tranformation_matrix_normalize), 0.0f);" \
"vec3 ambient1 = u_light_configuration1.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse1 = u_light_configuration1.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction1;" \
"vec3 specular1 = u_light_configuration1.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector1, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" \

"vec3 light_direction_normalize2 = normalize(light_direction2);" \
"vec3 reflection_vector2 = reflect(-light_direction_normalize2, tranformation_matrix_normalize);" \
"float t_normal_dot_light_direction2 = max(dot(light_direction_normalize2, tranformation_matrix_normalize), 0.0f);" \
"vec3 ambient2 = u_light_configuration2.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse2 = u_light_configuration2.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction2;" \
"vec3 specular2 = u_light_configuration2.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector2, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" \

"phong_ads_light = ambient0 + ambient1 + ambient2 + diffuse0 + diffuse1 + diffuse2 + specular0 + specular1 + specular2;" \
"}" \
"else" \
"{" \
"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
"}" \

"FragColor = vec4(phong_ads_light, 1.0);" \
"}";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow) {
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("PP-ToggleShaders");

	// code
	if (0 != fopen_s(&pep_gpFile, "log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("PP-ToggleShaders"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, 800, 600, NULL, NULL, hInstance, NULL);
	pep_gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-2 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-3 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
			"Failed.\nExitting Now... "
			"Successfully\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-4 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-5 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
			"\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-6 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-7 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-8 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-9 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-10 == iRet) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (WM_QUIT == msg.message) {
				bDone = true;
			} else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} else {
			if (pep_gbActiveWindow) {
			}
			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);
	int Initialize_Shaders(const GLchar *, const GLchar *);
	void Uninitialize_Shaders(void);

	// code
	switch (iMsg) {
	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	case WM_ERASEBKGND:
		return 0;
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KILLFOCUS:
		pep_gbActiveWindow = false;
		break;
	case WM_SETFOCUS:
		pep_gbActiveWindow = true;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN: {
		switch (wParam) {
		case VK_ESCAPE:
			ToggledFullScreen();
			break;

		case 'e':
		case 'E':
			DestroyWindow(hwnd);
			break;

		case 'V':
		case 'v':
			Uninitialize_Shaders();
			Initialize_Shaders(gc_PerVertex_vertexShaderSourceCode,
				gc_PerVertex_fragmentShaderSourceCode);
			break;

		case 'F':
		case 'f':
			Uninitialize_Shaders();
			Initialize_Shaders(gc_PerFragment_vertexShaderSourceCode,
				gc_PerFragment_fragmentShaderSourceCode);
			break;

		case 'L':
		case 'l':
			if (false == pep_gbIsLightEnable) {
				pep_gbIsLightEnable = true;
				pep_lKeyPressed = 1;
			} else {
				pep_gbIsLightEnable = false;
				pep_lKeyPressed = 0;
			}
			break;
		}
	} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == pep_gbIsFullScreen) {
		pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_dwStyle) {
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		pep_gbIsFullScreen = true;
	} else {
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		pep_gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void ReSize(int width, int height) {
	// code
	if (0 == height) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	pep_perspectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	return;
}

void Display(void) {
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(pep_giShaderProgramObject);

	mat4 pep_translateMatrix;
	mat4 rotationMatrix;

	pep_modelMatrix = mat4::identity();
	pep_viewMatrix = mat4::identity();
	pep_translateMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	pep_translateMatrix = translate(0.0f, 0.0f, -3.0f);

	pep_viewMatrix = pep_translateMatrix;

	glUniformMatrix4fv(pep_modelMatrixUniform, 1, GL_FALSE, pep_modelMatrix);
	glUniformMatrix4fv(pep_viewMatrixUniform, 1, GL_FALSE, pep_viewMatrix);
	glUniformMatrix4fv(pep_projectionMatrixUniform, 1, GL_FALSE,
		pep_perspectiveProjectionMatrix);

	if (pep_gbIsLightEnable) {
		glUniform1i(pep_keyLPressedUniform, pep_lKeyPressed);

		glUniform3fv(pep_light_configuration_uniform0.pep_lightAmbient, 1,
			pep_light0.pep_lightAmbient);
		glUniform3fv(pep_light_configuration_uniform0.pep_lightDiffuse, 1,
			pep_light0.pep_lightDiffuse);
		glUniform3fv(pep_light_configuration_uniform0.pep_lightSpecular, 1,
			pep_light0.pep_lightSpecular);

		pep_light0.pep_lightPosition[0] = cos(pep_gLightAngle_Zero) - sin(pep_gLightAngle_Zero);
		pep_light0.pep_lightPosition[1] = cos(pep_gLightAngle_Zero) + sin(pep_gLightAngle_Zero);
		pep_light0.pep_lightPosition[2] = 0.0f;
		pep_light0.pep_lightPosition[3] = 1.0f;

		glUniform4fv(pep_light_configuration_uniform0.pep_lightPosition, 1,
			pep_light0.pep_lightPosition);

		glUniform3fv(pep_light_configuration_uniform1.pep_lightAmbient, 1,
			pep_light1.pep_lightAmbient);
		glUniform3fv(pep_light_configuration_uniform1.pep_lightDiffuse, 1,
			pep_light1.pep_lightDiffuse);
		glUniform3fv(pep_light_configuration_uniform1.pep_lightSpecular, 1,
			pep_light1.pep_lightSpecular);

		pep_light1.pep_lightPosition[0] = cos(pep_gLightAngle_One) - sin(pep_gLightAngle_One);
		pep_light1.pep_lightPosition[1] = 0.0f;
		pep_light1.pep_lightPosition[2] = cos(pep_gLightAngle_One) + sin(pep_gLightAngle_One);
		pep_light1.pep_lightPosition[3] = 1.0f;
		glUniform4fv(pep_light_configuration_uniform1.pep_lightPosition, 1,
			pep_light1.pep_lightPosition);

		glUniform3fv(pep_light_configuration_uniform2.pep_lightAmbient, 1,
			pep_light2.pep_lightAmbient);
		glUniform3fv(pep_light_configuration_uniform2.pep_lightDiffuse, 1,
			pep_light2.pep_lightDiffuse);
		glUniform3fv(pep_light_configuration_uniform2.pep_lightSpecular, 1,
			pep_light2.pep_lightSpecular);

		pep_light2.pep_lightPosition[0] = 0.0f;
		pep_light2.pep_lightPosition[1] = cos(pep_gLightAngle_Two) + sin(pep_gLightAngle_Two);
		pep_light2.pep_lightPosition[2] = cos(pep_gLightAngle_Two) - sin(pep_gLightAngle_Two);
		pep_light2.pep_lightPosition[3] = 1.0f;
		glUniform4fv(pep_light_configuration_uniform2.pep_lightPosition, 1,
			pep_light2.pep_lightPosition);

		glUniform3fv(pep_materialAmbientUniform, 1, pep_materialAmbient);
		glUniform3fv(pep_materialDiffuseUniform, 1, pep_materialDiffuse);
		glUniform3fv(pep_materialSpecularUniform, 1, pep_materialSpecular);
		glUniform1f(pep_materialShinessUniform, pep_materialShiness);
	} else {
		glUniform1i(pep_keyLPressedUniform, pep_lKeyPressed);
	}

	glBindVertexArray(pep_vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_vbo_sphere_element);
	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glDrawElements(GL_TRIANGLES, pep_gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(pep_gHdc);
}

void Uninitialize_Shaders(void)
{
	if (pep_giShaderProgramObject) {
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_giShaderProgramObject);

		glGetProgramiv(pep_giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders) {
			glGetAttachedShaders(pep_giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(pep_giShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_giShaderProgramObject);
		pep_giShaderProgramObject = 0;

		glUseProgram(0);
	}

	return;

}

void Uninitialize(void) {
	// function declarations
	void Uninitialize_Shaders(void);

	// code

	if (pep_vbo_sphere_element) {
		glDeleteBuffers(1, &pep_vbo_sphere_element);
		pep_vbo_sphere_element = 0;
	}
	if (pep_vbo_sphere_normal) {
		glDeleteBuffers(1, &pep_vbo_sphere_normal);
		pep_vbo_sphere_normal = 0;
	}

	if (pep_vbo_sphere_vertex) {
		glDeleteBuffers(1, &pep_vbo_sphere_vertex);
		pep_vbo_sphere_vertex = 0;
	}

	if (pep_vao_sphere) {
		glDeleteVertexArrays(1, &pep_vao_sphere);
		pep_vao_sphere = 0;
	}

	Uninitialize_Shaders();

	if (wglGetCurrentContext() == pep_gHglrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != pep_gHglrc) {
		wglDeleteContext(pep_gHglrc);
	}

	if (NULL != pep_gHdc) {
		ReleaseDC(pep_gHwnd, pep_gHdc);
	}

	fprintf(
		pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile) {
		fclose(pep_gpFile);
	}

	return;
}

int Initialize_Shaders(const GLchar *vertexShaderSourceCode,
	const GLchar *fragmentShaderSourceCode) {

	// code
	pep_giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_giVertexShaderObject) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(pep_giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(pep_giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -7;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_giFragmentShaderObject) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(pep_giFragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_giFragmentShaderObject);

	glGetShaderiv(pep_giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(pep_giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -9;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_giShaderProgramObject = glCreateProgram();

	glAttachShader(pep_giShaderProgramObject, pep_giVertexShaderObject);
	glAttachShader(pep_giShaderProgramObject, pep_giFragmentShaderObject);

	// Attributes Binding Before Linking
	glBindAttribLocation(pep_giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glBindAttribLocation(pep_giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(pep_giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(pep_giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(pep_giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -10;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	pep_modelMatrixUniform =
		glGetUniformLocation(pep_giShaderProgramObject, "u_matrices.model_matrix");
	pep_viewMatrixUniform =
		glGetUniformLocation(pep_giShaderProgramObject, "u_matrices.view_matrix");
	pep_projectionMatrixUniform =
		glGetUniformLocation(pep_giShaderProgramObject, "u_matrices.projection_matrix");

	pep_light_configuration_uniform0.pep_lightAmbient = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration0.light_ambient");
	pep_light_configuration_uniform0.pep_lightDiffuse = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration0.light_diffuse");
	pep_light_configuration_uniform0.pep_lightSpecular = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration0.light_specular");
	pep_light_configuration_uniform0.pep_lightPosition = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration0.light_position");

	pep_light_configuration_uniform1.pep_lightAmbient = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration1.light_ambient");
	pep_light_configuration_uniform1.pep_lightDiffuse = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration1.light_diffuse");
	pep_light_configuration_uniform1.pep_lightSpecular = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration1.light_specular");
	pep_light_configuration_uniform1.pep_lightPosition = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration1.light_position");

	pep_light_configuration_uniform2.pep_lightAmbient = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration2.light_ambient");
	pep_light_configuration_uniform2.pep_lightDiffuse = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration2.light_diffuse");
	pep_light_configuration_uniform2.pep_lightSpecular = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration2.light_specular");
	pep_light_configuration_uniform2.pep_lightPosition = glGetUniformLocation(
		pep_giShaderProgramObject, "u_light_configuration2.light_position");

	pep_materialAmbientUniform = glGetUniformLocation(
		pep_giShaderProgramObject, "u_material_configuration.material_ambient");
	pep_materialDiffuseUniform = glGetUniformLocation(
		pep_giShaderProgramObject, "u_material_configuration.material_diffuse");
	pep_materialSpecularUniform = glGetUniformLocation(
		pep_giShaderProgramObject, "u_material_configuration.material_specular");
	pep_materialShinessUniform = glGetUniformLocation(
		pep_giShaderProgramObject, "u_material_configuration.material_shiness");

	pep_keyLPressedUniform =
		glGetUniformLocation(pep_giShaderProgramObject, "u_key_L_pressed");

	return 0;
}

int Initialize(void) {
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	void ReSize(int, int);
	void Uninitialize(void);
	int Initialize_Shaders(const GLchar *, const GLchar *);

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_gHdc = GetDC(pep_gHwnd);

	index = ChoosePixelFormat(pep_gHdc, &pfd);
	if (0 == index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd)) {
		return -2;
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc) {
		return -3;
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc)) {
		return -4;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result) {
		return -5;
	}

	pep_light0.pep_lightAmbient[0] = 0.0f;
	pep_light0.pep_lightAmbient[1] = 0.0f;
	pep_light0.pep_lightAmbient[2] = 0.0f;
	pep_light0.pep_lightAmbient[3] = 0.0f;
	pep_light0.pep_lightDiffuse[0] = 1.0f;
	pep_light0.pep_lightDiffuse[1] = 0.0f;
	pep_light0.pep_lightDiffuse[2] = 0.0f;
	pep_light0.pep_lightDiffuse[3] = 1.0f;
	pep_light0.pep_lightSpecular[0] = 1.0f;
	pep_light0.pep_lightSpecular[1] = 0.0f;
	pep_light0.pep_lightSpecular[2] = 0.0f;
	pep_light0.pep_lightSpecular[3] = 1.0f;
	pep_light0.pep_lightPosition[0] = 0.0f;
	pep_light0.pep_lightPosition[1] = 0.0f;
	pep_light0.pep_lightPosition[2] = 0.0f;
	pep_light0.pep_lightPosition[3] = 1.0f;

	pep_light1.pep_lightAmbient[0] = 0.0f;
	pep_light1.pep_lightAmbient[1] = 0.0f;
	pep_light1.pep_lightAmbient[2] = 0.0f;
	pep_light1.pep_lightAmbient[3] = 0.0f;
	pep_light1.pep_lightDiffuse[0] = 0.0f;
	pep_light1.pep_lightDiffuse[1] = 1.0f;
	pep_light1.pep_lightDiffuse[2] = 0.0f;
	pep_light1.pep_lightDiffuse[3] = 1.0f;
	pep_light1.pep_lightSpecular[0] = 0.0f;
	pep_light1.pep_lightSpecular[1] = 1.0f;
	pep_light1.pep_lightSpecular[2] = 0.0f;
	pep_light1.pep_lightSpecular[3] = 1.0f;
	pep_light1.pep_lightPosition[0] = 0.0f;
	pep_light1.pep_lightPosition[1] = 0.0f;
	pep_light1.pep_lightPosition[2] = 0.0f;
	pep_light1.pep_lightPosition[3] = 1.0f;

	pep_light2.pep_lightAmbient[0] = 0.0f;
	pep_light2.pep_lightAmbient[1] = 0.0f;
	pep_light2.pep_lightAmbient[2] = 0.0f;
	pep_light2.pep_lightAmbient[3] = 0.0f;
	pep_light2.pep_lightDiffuse[0] = 0.0f;
	pep_light2.pep_lightDiffuse[1] = 0.0f;
	pep_light2.pep_lightDiffuse[2] = 1.0f;
	pep_light2.pep_lightDiffuse[3] = 1.0f;
	pep_light2.pep_lightSpecular[0] = 0.0f;
	pep_light2.pep_lightSpecular[1] = 0.0f;
	pep_light2.pep_lightSpecular[2] = 1.0f;
	pep_light2.pep_lightSpecular[3] = 1.0f;
	pep_light2.pep_lightPosition[0] = 0.0f;
	pep_light2.pep_lightPosition[1] = 0.0f;
	pep_light2.pep_lightPosition[2] = 0.0f;
	pep_light2.pep_lightPosition[3] = 1.0f;

	getSphereVertexData(pep_sphere_vertices, pep_sphere_normals, pep_sphere_textures,
		pep_sphere_elements);
	pep_gNumVertices = getNumberOfSphereVertices();
	pep_gNumElements = getNumberOfSphereElements();

	Initialize_Shaders(gc_PerVertex_vertexShaderSourceCode,
		gc_PerVertex_fragmentShaderSourceCode);

	// cube
	glGenVertexArrays(1, &pep_vao_sphere);
	glBindVertexArray(pep_vao_sphere);

	glGenBuffers(1, &pep_vbo_sphere_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, pep_vbo_sphere_vertex);

	glBufferData(GL_ARRAY_BUFFER, sizeof(pep_sphere_vertices), pep_sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pep_vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, pep_vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pep_sphere_normals), pep_sphere_normals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &pep_vbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_vbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(pep_sphere_elements),
		pep_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	pep_modelMatrix = mat4::identity();
	pep_viewMatrix = mat4::identity();
	pep_perspectiveProjectionMatrix = mat4::identity();

	ReSize(800, 600);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}

void Update(void) {
	// Code
	if (pep_gLightAngle_Zero >= 360.0f) {
		pep_gLightAngle_Zero = 0.0f;
	}
	pep_gLightAngle_Zero += 0.02f;

	if (pep_gLightAngle_One >= 360.0f) {
		pep_gLightAngle_One = 0.0f;
	}
	pep_gLightAngle_One += 0.02f;

	if (pep_gLightAngle_Two >= 360.0f) {
		pep_gLightAngle_Two = 0.0f;
	}
	pep_gLightAngle_Two += 0.02f;

	return;
}
