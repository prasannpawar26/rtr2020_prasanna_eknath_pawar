#include <stdio.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow;
bool gbIsFullScreen;

GLuint giVertexShaderObject;
GLuint giGeometryShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao;
GLuint vbo;
GLuint mvpUniform;

mat4 perspectiveProjectionMatrix;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow) {
	// function prototype
	int Initialize(void);
	void Display(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("PP-GeometryShader");

	// code
	if (0 != fopen_s(&gpFile, "log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("PP-GeometryShader"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-2 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-3 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
			"Failed.\nExitting Now... "
			"Successfully\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-4 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-5 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
			"\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-6 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-7 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-8 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-9 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else if (-10 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	} else {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message) {
				bDone = true;
			} else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} else {
			if (gbActiveWindow) {
			}

			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// code
	switch (iMsg)
	{ case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	case WM_ERASEBKGND:
		return 0;
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{ case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'F':
		case 'f':
			ToggledFullScreen();
			break;
		}
	}
	break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

int Initialize(void)
{
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	// function declarations
	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index)
	{
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, index, &pfd))
	{
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc))
	{
		return -4;
	}

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result)
	{
		return -5;
	}

	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject)
	{
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// GEOMETRY SHADER
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giGeometryShaderObject = glCreateShader(GL_GEOMETRY_SHADER);

	const GLchar *geometryShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(triangles)in;" \
		"layout(triangle_strip, max_vertices = 9)out;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"for(int vertex=0; vertex < 3; vertex++)" \
		"{" \
		"gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(0.0, 1.0, 0.0, 0.0));" \
		"EmitVertex();" \
		"gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(-1.0, -1.0, 0.0, 0.0));" \
		"EmitVertex();" \
		"gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(1.0, -1.0, 0.0, 0.0));" \
		"EmitVertex();" \

		"EndPrimitive();" \
		"}" \
		"}";

	glShaderSource(giGeometryShaderObject, 1,
		(const GLchar **)&geometryShaderSourceCode, NULL);
	glCompileShader(giGeometryShaderObject);

	glGetShaderiv(giGeometryShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giGeometryShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giGeometryShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Geometry Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -8;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Geometry Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// FRAGMENT SHADER
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;
	giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}"; 

	glShaderSource(giFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(giFragmentShaderObject);

	glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);
	glAttachShader(giShaderProgramObject, giGeometryShaderObject);
	glAttachShader(giShaderProgramObject, giFragmentShaderObject);

	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		glGetShaderiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -10;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLint iAttachedShaderCount = 0;
	glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS,
		&iAttachedShaderCount);
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program: Attached Shader Count - %d \n",
		__FILE__, __LINE__, __FUNCTION__, iAttachedShaderCount);

	mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");

	const GLfloat triangleVertices[] = {0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f};

	glGenVertexArrays(1, &vao);

	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	ReSize(800, 600);

	return 0;
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	return;
}

void Display(void) {
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(giShaderProgramObject);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -4.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(gHdc);

	return;
}

void Uninitialize(void)
{
	// code
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
	}

	if (giShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(giShaderProgramObject);

		glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(giShaderProgramObject);
		giShaderProgramObject = 0;

		glUseProgram(0);
	}

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully ",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != gpFile)
	{
		fclose(gpFile);
	}

	return;
}
