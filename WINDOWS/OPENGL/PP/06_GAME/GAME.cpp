#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow;
bool gbIsAnimationStart = false;
bool gbIsFullScreen;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum {
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXTCOORD0
};

// box1
GLuint box1_vao;
GLuint box1_vbo_position;
GLuint box1_vbo_color;
vec3 box1_initial_translation = {1.9f, 1.9f, -6.0f};
vec3 box1_current_translation = {0.0f, 0.0f, 0.0f};
vec3 box1_factor_for_translation = {0.0f, 0.0f, 0.0f};
bool box1_reverse = false;

GLfloat box1_alpha;
GLfloat box1_position[12];
GLfloat box1_color[12];

// slider
GLuint slider_vao;
GLuint slider_vbo_position;
GLuint slider_vbo_color;
vec3 slider_initial_translation = {0.0f, -1.9f, -6.0f};
vec3 slider_current_translation = {0.0f, 0.0f, 0.0f};
bool slider_reverse = false;

GLfloat slider_alpha;
GLfloat slider_position[12];
GLfloat slider_color[12];


GLuint vao_border;
GLuint vbo_border_position;
GLuint vbo_border_color;
GLfloat rotation_border_angle;

// UNIFORM
GLuint mvpUniform;  
GLuint alphaUniform;

mat4 perpsectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("GAME");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("GAME"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, 800, 600, NULL, NULL, hInstance, NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
            "Failed.\nExitting Now... "
            "Successfully\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
            "\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
            "Successful.\n",
            __FILE__, __LINE__, __FUNCTION__);
  }

  while (bDone == false) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow && gbIsAnimationStart) {
        Update();
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {
    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_LEFT:
        {
          if (-1.60f <
              (slider_current_translation[0] + slider_initial_translation[0])) {
            slider_current_translation[0] -= 0.05f;
          }
        }
          break;

        case VK_RIGHT:
        {
          if (1.60f > (slider_current_translation[0] + slider_initial_translation[0]))
          {
            slider_current_translation[0] += 0.05f;
          }
        }
          break;

        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case 'A':
        case 'a':
          if (gbIsAnimationStart)
          {
            gbIsAnimationStart = false;
          }
          else
          {
            gbIsAnimationStart = true;
          }
          break;
      }
    } break;
  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen) {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

int Initialize(void) {
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 8;
  pfd.cRedBits = 8;
  pfd.cBlueBits = 8;
  pfd.cGreenBits = 8;
  pfd.cAlphaBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

    return -6;
  }

  const GLchar *vertexShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "in vec4 vPosition;" \
      "in vec4 vColor;" \
      "out vec4 out_color;" \
      "uniform mat4 u_mvp_matrix;" \
      "void main(void)" \
      "{" \
      "gl_Position = u_mvp_matrix * vPosition;" \
      "out_color = vColor;" \
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);

  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus) {
    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
        return -7;
      }
    }
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "in vec4 out_color;" \
      "out vec4 FragColor;" \
      "void main(void)" \
      "{" \
      "FragColor = out_color;" \
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);

  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (FALSE == iShaderCompileStatus) {
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
        return -9;
      }
    }
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                       "vPosition");
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetShaderiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus) {
    glGetShaderiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(gpFile,
                "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                "Failed:\n\t%s\n",
                __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
        return -10;
      }
    }
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");

  // RECTANGLE
  const GLfloat borderVertices[] = {
    2.0f,  2.0f,  0.0f,
    -2.0f, 2.0f,  0.0f,
    -2.0f, -2.0f, 0.0f,
    2.0f, -2.0f, 0.0f
  };
  
  glGenVertexArrays(1, &vao_border);

  glBindVertexArray(vao_border);

  // Position
  glGenBuffers(1, &vbo_border_position);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_border_position);

  glBufferData(GL_ARRAY_BUFFER, sizeof(borderVertices), borderVertices,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  //

  // Color

  // If Shape Have Only One Color, The Use ***glVertexAttrib3f*** To Pass Color
  // Values
  //glVertexAttrib3f(AMC_ATTRIBUTES_COLOR, 0.0f, 0.0f, 1.0f);

  // If Shape Have More Than One Color, Then Use VBO Style

  const GLfloat borderColor[] = {0.0f,  0.0f,  1.0f, 0.0f, 0.0f, 1.0f,
                                       0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};

  glGenBuffers(1, &vbo_border_color);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_border_color);

  glBufferData(GL_ARRAY_BUFFER, sizeof(borderColor), borderColor,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  // 

  glBindVertexArray(0);
  //

  box1_factor_for_translation[0] = (float)rand() / 2.0f;

  // box 1
  box1_position[0] = 0.10f;
  box1_position[1] = 0.10f;
  box1_position[2] = 0.0f;

  box1_position[3] = -0.10f;
  box1_position[4] = 0.10f;
  box1_position[5] = 0.0f;

  box1_position[6] = -0.10f;
  box1_position[7] = -0.10f;
  box1_position[8] = 0.0f;

  box1_position[9] = 0.10f;
  box1_position[10] = -0.10f;
  box1_position[11] = 0.0f;

  glGenVertexArrays(1, &box1_vao);

  glBindVertexArray(box1_vao);

  // Position
  glGenBuffers(1, &box1_vbo_position);

  glBindBuffer(GL_ARRAY_BUFFER, box1_vbo_position);

  glBufferData(GL_ARRAY_BUFFER, sizeof(box1_position), box1_position,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  box1_color[0] = 1.0f;
  box1_color[1] = 0.0f;
  box1_color[2] = 0.0f;

  box1_color[3] = 1.0f;
  box1_color[4] = 0.0f;
  box1_color[5] = 0.0f;

  box1_color[6] = 1.0f;
  box1_color[7] = 0.0f;
  box1_color[8] = 0.0f;

  box1_color[9] = 1.0f;
  box1_color[10] = 0.0f;
  box1_color[11] = 0.0f;

  glGenBuffers(1, &box1_vbo_color);

  glBindBuffer(GL_ARRAY_BUFFER, box1_vbo_color);

  glBufferData(GL_ARRAY_BUFFER, sizeof(box1_color), box1_color,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);


  // Slider
  // box 1
  slider_position[0] = 0.35f;
  slider_position[1] = 0.10f;
  slider_position[2] = 0.0f;

  slider_position[3] = -0.35f;
  slider_position[4] = 0.10f;
  slider_position[5] = 0.0f;

  slider_position[6] = -0.35f;
  slider_position[7] = -0.10f;
  slider_position[8] = 0.0f;

  slider_position[9] = 0.35f;
  slider_position[10] = -0.10f;
  slider_position[11] = 0.0f;

  glGenVertexArrays(1, &slider_vao);

  glBindVertexArray(slider_vao);

  // Position
  glGenBuffers(1, &slider_vbo_position);

  glBindBuffer(GL_ARRAY_BUFFER, slider_vbo_position);

  glBufferData(GL_ARRAY_BUFFER, sizeof(slider_position), slider_position,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  slider_color[0] = 1.0f;
  slider_color[1] = 1.0f;
  slider_color[2] = 0.0f;

  slider_color[3] = 1.0f;
  slider_color[4] = 1.0f;
  slider_color[5] = 0.0f;

  slider_color[6] = 1.0f;
  slider_color[7] = 1.0f;
  slider_color[8] = 0.0f;

  slider_color[9] = 1.0f;
  slider_color[10] = 1.0f;
  slider_color[11] = 0.0f;

  glGenBuffers(1, &slider_vbo_color);

  glBindBuffer(GL_ARRAY_BUFFER, slider_vbo_color);

  glBufferData(GL_ARRAY_BUFFER, sizeof(slider_color), slider_color,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  perpsectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height) {
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  perpsectiveProjectionMatrix =
      perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Display(void) {
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(giShaderProgramObject);

  mat4 modelViewMatrix;
  mat4 modelViewProjectionMatrix;
  mat4 translationMatrix;
  mat4 rotationMatrix;

  // border
  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  translationMatrix = mat4::identity();
  rotationMatrix = mat4::identity();

  translationMatrix = translate(0.0f, 0.0f, -6.0f);
  rotationMatrix = rotate(rotation_border_angle, 1.0f, 0.0f, 0.0f);
  modelViewMatrix = translationMatrix * rotationMatrix;
  modelViewProjectionMatrix = perpsectiveProjectionMatrix * modelViewMatrix;

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

  glBindVertexArray(vao_border);

  glDrawArrays(GL_LINE_LOOP /*GL_TRIANGLE_FAN*/, 0, 4);

  glBindVertexArray(0);

  // box
  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  translationMatrix = mat4::identity();
  rotationMatrix = mat4::identity();

  translationMatrix =
      translate(box1_current_translation[0] + box1_initial_translation[0],
                box1_current_translation[1] + box1_initial_translation[1],
                box1_current_translation[2] + box1_initial_translation[2]);
  rotationMatrix = rotate(rotation_border_angle, 1.0f, 0.0f, 0.0f);
  modelViewMatrix = translationMatrix * rotationMatrix;
  modelViewProjectionMatrix = perpsectiveProjectionMatrix * modelViewMatrix;

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

  glBindVertexArray(box1_vao);

  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  glBindVertexArray(0);

  // slider
  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  translationMatrix = mat4::identity();
  rotationMatrix = mat4::identity();

  translationMatrix =
      translate(slider_current_translation[0] + slider_initial_translation[0],
                slider_current_translation[1] + slider_initial_translation[1],
                slider_current_translation[2] + slider_initial_translation[2]);
  //rotationMatrix = rotate(rotation_slider_angle, 1.0f, 0.0f, 0.0f);
  modelViewMatrix = translationMatrix * rotationMatrix;
  modelViewProjectionMatrix = perpsectiveProjectionMatrix * modelViewMatrix;

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

  glBindVertexArray(slider_vao);

  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  glBindVertexArray(0);

  glUseProgram(0);

  SwapBuffers(gHdc);

  return;
}

void Uninitialize(void)
{
  //code
  if (vbo_border_color)
  {
    glDeleteBuffers(1, &vbo_border_color);
    vbo_border_color = 0;
  }

  if (vbo_border_position) {
    glDeleteBuffers(1, &vbo_border_position);
    vbo_border_position = 0;
  }

  if (vao_border)
  {
    glDeleteVertexArrays(1, &vao_border);
    vao_border = 0;
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);
      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully ",
          __FILE__, __LINE__, __FUNCTION__);

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}

void Update(void)
{
  if (box1_reverse)
  {
    // Left Border
    if (-1.9f < (box1_current_translation[0] + box1_initial_translation[0]))
    {
      box1_current_translation[0] -= 0.01f;
      box1_current_translation[1] -= 0.01f;
    }
    else
    {
      box1_reverse = false;
    }
  }
  else
  {
    // Right Border
    if (1.9f > (box1_current_translation[0] + box1_initial_translation[0]))
    {
      box1_current_translation[0] += 0.01f;
      box1_current_translation[1] += 0.01f;
    }
    else
    {
      box1_reverse = true;
    }
  }
  
  
  // code
  //if (360.0f < rotation_triangle_angle)
  //{
  //  rotation_triangle_angle = 0.0f;
  //}
  //rotation_triangle_angle += 1.0f;

  if (360.0f < rotation_border_angle) {
    rotation_border_angle = 0.0f;
  }
  rotation_border_angle += 0.0f;

  return;
}
