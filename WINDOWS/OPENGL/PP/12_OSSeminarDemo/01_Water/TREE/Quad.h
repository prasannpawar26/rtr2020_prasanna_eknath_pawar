#pragma once
#include "vmath.h"

void Quad_Initialize(void);
int Quad_Initialize_Shader(void);
void Quad_Uninitialize_Shaders(void);
void Quad_Display(void);
void Quad_Uniforms(vmath::mat4, vmath::mat4, vmath::mat4);
void Quad_Uninitialize(void);

int Refraction_Initialize(void);
int Refraction_Resize_Fbo(int width, int height);
void Refraction_Display(void);
void Refraction_Uniforms(vmath::mat4, vmath::mat4, vmath::mat4);
int Refraction_Uninitialize(void);
int Refraction_Initialize_Shaders(void);
void Refraction_Uninitialize_Shaders(void);
