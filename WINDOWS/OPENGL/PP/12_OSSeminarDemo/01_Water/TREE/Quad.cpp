#include "Quad.h"

#include "common.h"

using namespace vmath;

extern FILE *gpFile;

extern int lKeyPressed;
extern bool gbIsLightEnable;
extern int gWindowWidth;
extern int gWindowHeight;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

GLuint quad_giVertexShaderObject;
GLuint quad_giFragmentShaderObject;
GLuint quad_giShaderProgramObject;

GLuint quad_modelMatrixUniform;
GLuint quad_viewMatrixUniform;
GLuint quad_projectionMatrixUniform;

GLuint quad_sampler_uniform;

GLuint quad_vao;
GLuint quad_vbo_vertex;
GLuint quad_vbo_texcoord;

float quad_material_ambient[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float quad_material_diffuse[4] = {0.0f, 1.0f, 1.01f, 1.0f};
float quad_material_specular[4] = {0.0f, 1.0f, 1.0f, 1.0f};
float quad_material_shiness = 0.4f * 128;  // Also Try 128.0f

float quad_lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float quad_lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float quad_lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float quad_lightPosition[4] = { 0.0f, 0.0f, 2.0f, 1.0f };

mat4 quad_modelMatrix;
mat4 quad_viewMatrix;
mat4 quad_perspectiveProjectionMatrix;

float quad_vertices[] =
{
	1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f
};

float quad_texcoord[] =
{
	0.0f, 1.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,
	0.0f, 0.0f
};

const GLchar *quad_VertexShaderSourceCode =
"#version 450 core"
"\n"

"in vec4 vPosition;"
"in vec2 vTexCoord;"

"out vec2 out_texcoord;"

"struct matrices {" \
	"mat4 model_matrix;" \
	"mat4 view_matrix;" \
	"mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \

"void main(void)"
"{"
	"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
	"out_texcoord = vTexCoord;"
"}";

const GLchar *quad_FragmentShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec2 out_texcoord;" \
"out vec4 FragColor;" \

"uniform sampler2D u_sampler;" \

"void main(void)" \
"{" \
	"FragColor = texture(u_sampler, out_texcoord);" \
"}";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

GLuint refraction_giVertexShaderObject;
GLuint refraction_giFragmentShaderObject;
GLuint refraction_giShaderProgramObject;

GLuint refraction_modelMatrixUniform;
GLuint refraction_viewMatrixUniform;
GLuint refraction_projectionMatrixUniform;
GLuint refraction_lightAmbientUniform;
GLuint refraction_lightDiffuseUniform;
GLuint refraction_lightSpecularUniform;
GLuint refraction_materialAmbientUniform;
GLuint refraction_materialDiffuseUniform;
GLuint refraction_materialSpecularUniform;
GLuint refraction_materialShinessUniform;
GLuint refraction_lightPositionUniform;
GLuint refraction_keyLPressedUniform;
GLuint refraction_alphaUniform;
GLuint refraction_sampler_uniform;

GLuint refraction_fbo;
GLuint refraction_fbo_texture;
GLuint refraction_fbo_depth;

float refraction_material_ambient[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float refraction_material_diffuse[4] = {1.0f, 0.0f, 1.0f, 1.0f};
float refraction_material_specular[4] = {1.0f, 0.0f, 1.0f, 1.0f};
float refraction_material_shiness = 0.4f * 128;  // Also Try 128.0f

float refraction_lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float refraction_lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float refraction_lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float refraction_lightPosition[4] = { 0.0f, 0.0f, 2.0f, 1.0f };

int refraction_width = 1920;
int refraction_height = 1920;

mat4 refraction_modelMatrix;
mat4 refraction_viewMatrix;
mat4 refraction_perspectiveProjectionMatrix;

GLuint refraction_g_triangle_vao;
GLuint refraction_g_triangle_vbo_vertex;
GLuint refraction_g_triangle_vbo_normal;

float refraction_g_triangle_vertices[] =
{
	0.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f
};

float refraction_g_triangle_normal[] =
{
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f,
};

const GLchar *refraction_vertexShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec4 vPosition;" \
"in vec3 vNormal;" \

"out vec3 light_direction;" \
"out vec3 tranformation_matrix;" \
"out vec3 viewer_vector;" \

"struct matrices {" \
	"mat4 model_matrix;" \
	"mat4 view_matrix;" \
	"mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \
"uniform int u_key_L_pressed;" \

"struct light_configuration {" \
	"vec3 light_ambient;" \
	"vec3 light_diffuse;" \
	"vec3 light_specular;" \
	"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration;" \

"void main(void)" \
"{" \
	"if(1 == u_key_L_pressed)" \
	"{" \
		"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
		"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_configuration.light_position - eye_coordinates);" \
		"viewer_vector = vec3(-eye_coordinates);" \
	"}" \

	"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * " \
	"vPosition;" \
"}";

const GLchar *refraction_fragmentShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec3 light_direction;" \
"in vec3 tranformation_matrix;" \
"in vec3 viewer_vector;" \

"out vec4 FragColor;" \

"uniform int u_key_L_pressed;" \
"uniform float u_alpha;" \

"struct light_configuration {" \
	"vec3 light_ambient;" \
	"vec3 light_diffuse;" \
	"vec3 light_specular;" \
	"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration;" \

"struct material_configuration {"
	"vec3 material_ambient;" \
	"vec3 material_diffuse;" \
	"vec3 material_specular;" \
	"float material_shiness;" \
"};" \
"uniform material_configuration u_material_configuration;" \

"vec3 phong_ads_light;" \

"void main(void)" \
"{" \
	"if(1 == u_key_L_pressed)" \
	"{" \
		"vec3 light_direction_normalize = normalize(light_direction);" \
		"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
		"vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \
		"vec3 viewer_vector_normal = normalize(viewer_vector);" \
		"float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \
		"vec3 ambient = u_light_configuration.light_ambient * u_material_configuration.material_ambient;" \
		"vec3 diffuse = u_light_configuration.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction;" \
		"vec3 specular = u_light_configuration.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" \
		"phong_ads_light= ambient + diffuse + specular;" \
	"}" \
	"else" \
	"{" \
		"phong_ads_light = vec3(1.0f * u_alpha, 1.0f * u_alpha, 1.0f * u_alpha);" \
	"}" \

	"FragColor = vec4(phong_ads_light[0] *u_alpha, phong_ads_light[1] *u_alpha, phong_ads_light[2] * u_alpha , 1.0 * u_alpha);" \
"}";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Quad_Initialize(void)
{
	//
	// Code
	//
	glGenVertexArrays(1, &quad_vao);
	glBindVertexArray(quad_vao);

	glGenBuffers(1, &quad_vbo_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vbo_vertex);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), NULL,GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &quad_vbo_texcoord);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vbo_texcoord);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return;
}

int Quad_Initialize_Shader(void)
{
	//
	// Code
	//
	quad_giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == quad_giVertexShaderObject)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(quad_giVertexShaderObject, 1, (const GLchar **)&quad_VertexShaderSourceCode, NULL);
	glCompileShader(quad_giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(quad_giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(quad_giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(quad_giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	quad_giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == quad_giFragmentShaderObject)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -2;
	}

	glShaderSource(quad_giFragmentShaderObject, 1, (const GLchar **)&quad_FragmentShaderSourceCode, NULL);
	glCompileShader(quad_giFragmentShaderObject);

	glGetShaderiv(quad_giFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(quad_giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(quad_giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -3;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	quad_giShaderProgramObject = glCreateProgram();

	glAttachShader(quad_giShaderProgramObject, quad_giVertexShaderObject);
	glAttachShader(quad_giShaderProgramObject, quad_giFragmentShaderObject);

	glBindAttribLocation(quad_giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glBindAttribLocation(quad_giShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

	glLinkProgram(quad_giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(quad_giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(quad_giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(quad_giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -4;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	quad_modelMatrixUniform = glGetUniformLocation(quad_giShaderProgramObject, "u_matrices.model_matrix");
	quad_viewMatrixUniform = glGetUniformLocation(quad_giShaderProgramObject, "u_matrices.view_matrix");
	quad_projectionMatrixUniform = glGetUniformLocation(quad_giShaderProgramObject, "u_matrices.projection_matrix");
	quad_sampler_uniform = glGetUniformLocation(quad_giShaderProgramObject, "u_sampler");

	return 0;
}

void Quad_Uninitialize_Shaders(void)
{
	//
	// Code
	//
	if (quad_giShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(quad_giShaderProgramObject);

		glGetProgramiv(quad_giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders) {
			glGetAttachedShaders(quad_giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(quad_giShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(quad_giShaderProgramObject);
		quad_giShaderProgramObject = 0;

		glUseProgram(0);
	}

	return;

}

void Quad_Display(void)
{
	//
	// Code
	//
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(quad_giShaderProgramObject);

	glBindTexture(GL_TEXTURE_2D, refraction_fbo_texture);

	quad_modelMatrix = mat4::identity();
	quad_modelMatrix = translate(0.0f, 0.0f, -1.0f);
	quad_viewMatrix = mat4::identity();
	quad_viewMatrix = lookat(vec3(0.0f, 0.0f, 2.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	glViewport(0, 0, gWindowWidth, gWindowHeight);
	quad_perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)gWindowWidth / (GLfloat)gWindowHeight, 0.1f, 100.0f);

	Quad_Uniforms(quad_modelMatrix, quad_viewMatrix, quad_perspectiveProjectionMatrix);

	glBindVertexArray(quad_vao);

	glBindBuffer(GL_ARRAY_BUFFER, quad_vbo_vertex);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), quad_vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, quad_vbo_texcoord);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), quad_texcoord, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glBindVertexArray(0);

	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);
	
	return;
}

void Quad_Uniforms(vmath::mat4 modelMatrix, vmath::mat4 viewMatrix, vmath::mat4 perspectiveMatrix)
{
	//
	// Code
	//
	glUniformMatrix4fv(quad_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(quad_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(quad_projectionMatrixUniform, 1, GL_FALSE, perspectiveMatrix);
	glUniform1i(quad_sampler_uniform, 0);

	return;
}

void Quad_Uninitialize(void)
{
	//
	// Code
	//
	if (quad_vbo_texcoord)
	{
		glDeleteBuffers(1, &quad_vbo_texcoord);
		quad_vbo_texcoord = 0;
	}

	if (quad_vbo_vertex)
	{
		glDeleteBuffers(1, &quad_vbo_vertex);
		quad_vbo_vertex = 0;
	}

	if (quad_vao)
	{
		glDeleteVertexArrays(1, &quad_vao);
		quad_vao = 0;
	}

	return;
}

int Refraction_Initialize(void)
{
	//
	// Code
	//

	// Generating Buffers
	glGenVertexArrays(1, &refraction_g_triangle_vao);
	glBindVertexArray(refraction_g_triangle_vao);

	glGenBuffers(1, &refraction_g_triangle_vbo_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, refraction_g_triangle_vbo_vertex);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &refraction_g_triangle_vbo_normal);
	glBindBuffer(GL_ARRAY_BUFFER, refraction_g_triangle_vbo_normal);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Generating FBOs
	glGenFramebuffers(1, &refraction_fbo);
	glBindFramebuffer( GL_FRAMEBUFFER, refraction_fbo);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glGenTextures(1, &refraction_fbo_texture);
	glBindTexture( GL_TEXTURE_2D, refraction_fbo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, refraction_width, refraction_height, 0, GL_BGR, GL_UNSIGNED_BYTE, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, refraction_fbo_texture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//
	// depth render buffer
	//
	glGenRenderbuffers(1, &refraction_fbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, refraction_fbo_depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, refraction_width, refraction_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, refraction_fbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		return 1;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return 0;
}

void Refraction_Display(void)
{
	//
	// Code
	//
	glBindFramebuffer(GL_FRAMEBUFFER, refraction_fbo);

	refraction_modelMatrix = mat4::identity();
	refraction_modelMatrix = translate(0.0f, 0.0f, -1.0f);
	refraction_viewMatrix = mat4::identity();
	refraction_viewMatrix = lookat(vec3(0.0f, 0.0f, 2.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	glViewport(0, 0, refraction_width, refraction_height);
	refraction_perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)refraction_width / (GLfloat)refraction_height, 0.1f, 100.0f);

	glUseProgram(refraction_giShaderProgramObject);

	Refraction_Uniforms(refraction_modelMatrix, refraction_viewMatrix, refraction_perspectiveProjectionMatrix);

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glBindVertexArray(refraction_g_triangle_vao);

	glBindBuffer(GL_ARRAY_BUFFER, refraction_g_triangle_vbo_vertex);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), refraction_g_triangle_vertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, refraction_g_triangle_vbo_normal);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), refraction_g_triangle_normal,GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);

	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return;
}

int Refraction_Uninitialize(void)
{
	//
	// Code
	//
	if (refraction_fbo_texture)
	{
		glDeleteRenderbuffers(1, &refraction_fbo_texture);
		refraction_fbo_texture = 0;
	}

	if (refraction_fbo_depth)
	{
		glDeleteRenderbuffers(1, &refraction_fbo_depth);
		refraction_fbo_depth = 0;
	}

	if (refraction_fbo)
	{
		glDeleteFramebuffers(1, &refraction_fbo);
		refraction_fbo = 0;
	}

	if (refraction_g_triangle_vbo_normal)
	{
		glDeleteBuffers(1, &refraction_g_triangle_vbo_normal);
		refraction_g_triangle_vbo_normal = 0;
	}

	if (refraction_g_triangle_vbo_vertex)
	{
		glDeleteBuffers(1, &refraction_g_triangle_vbo_vertex);
		refraction_g_triangle_vbo_vertex = 0;
	}

	if (refraction_g_triangle_vao)
	{
		glDeleteVertexArrays(1, &refraction_g_triangle_vao);
		refraction_g_triangle_vao = 0;
	}

	return 0;
}

int Refraction_Resize_Fbo(int width, int height)
{
	//
	// Code
	//
	glBindFramebuffer(GL_FRAMEBUFFER, refraction_fbo);
	glBindTexture(GL_TEXTURE_2D, refraction_fbo_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, refraction_fbo_texture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, refraction_fbo_depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, refraction_fbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return 0;
}

int Refraction_Initialize_Shaders(void)
{
	//
	// Code
	//
	refraction_giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == refraction_giVertexShaderObject)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(refraction_giVertexShaderObject, 1, (const GLchar **)&refraction_vertexShaderSourceCode, NULL);
	glCompileShader(refraction_giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(refraction_giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(refraction_giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(refraction_giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -7;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	refraction_giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == refraction_giFragmentShaderObject)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(refraction_giFragmentShaderObject, 1,
		(const GLchar **)&refraction_fragmentShaderSourceCode, NULL);
	glCompileShader(refraction_giFragmentShaderObject);

	glGetShaderiv(refraction_giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(refraction_giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(refraction_giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -9;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	refraction_giShaderProgramObject = glCreateProgram();

	glAttachShader(refraction_giShaderProgramObject, refraction_giVertexShaderObject);
	glAttachShader(refraction_giShaderProgramObject, refraction_giFragmentShaderObject);

	// Attributes Binding Before Linking
	glBindAttribLocation(refraction_giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");

	glBindAttribLocation(refraction_giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(refraction_giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(refraction_giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(refraction_giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(refraction_giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -10;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	refraction_modelMatrixUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_matrices.model_matrix");
	refraction_viewMatrixUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_matrices.view_matrix");
	refraction_projectionMatrixUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_matrices.projection_matrix");
	refraction_lightAmbientUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_light_configuration.light_ambient");
	refraction_lightDiffuseUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_light_configuration.light_diffuse");
	refraction_lightSpecularUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_light_configuration.light_specular");
	refraction_lightPositionUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_light_configuration.light_position");
	refraction_materialAmbientUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_material_configuration.material_ambient");
	refraction_materialDiffuseUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_material_configuration.material_diffuse");
	refraction_materialSpecularUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_material_configuration.material_specular");
	refraction_materialShinessUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_material_configuration.material_shiness");
	refraction_keyLPressedUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_key_L_pressed");
	refraction_alphaUniform = glGetUniformLocation(refraction_giShaderProgramObject, "u_alpha");

	return 0;
}

void Refraction_Uniforms(vmath::mat4 modelMatrix, vmath::mat4 viewMatrix, vmath::mat4 perspectiveMatrix)
{
	//
	// Code
	//
	glUniform1f(refraction_alphaUniform, 1.0f);

	glUniformMatrix4fv(refraction_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(refraction_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(refraction_projectionMatrixUniform, 1, GL_FALSE, perspectiveMatrix);

	glUniform3fv(refraction_materialAmbientUniform, 1, refraction_material_ambient);
	glUniform3fv(refraction_materialDiffuseUniform, 1, refraction_material_diffuse);
	glUniform3fv(refraction_materialSpecularUniform, 1, refraction_material_specular);
	glUniform1f(refraction_materialShinessUniform, refraction_material_shiness);

	if (gbIsLightEnable)
	{
		glUniform1i(refraction_keyLPressedUniform, lKeyPressed);	
		glUniform3fv(refraction_lightAmbientUniform, 1, refraction_lightAmbient);
		glUniform3fv(refraction_lightDiffuseUniform, 1, refraction_lightDiffuse);
		glUniform3fv(refraction_lightSpecularUniform, 1, refraction_lightSpecular);
		glUniform4fv(refraction_lightPositionUniform, 1, refraction_lightPosition);
	}
	else {
		glUniform1i(refraction_keyLPressedUniform, lKeyPressed);

	}

	return;
}

void Refraction_Uninitialize_Shaders(void)
{
	//
	// Code
	//
	if (refraction_giShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(refraction_giShaderProgramObject);

		glGetProgramiv(refraction_giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(refraction_giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(refraction_giShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(refraction_giShaderProgramObject);
		refraction_giShaderProgramObject = 0;

		glUseProgram(0);
	}

	return;

}
