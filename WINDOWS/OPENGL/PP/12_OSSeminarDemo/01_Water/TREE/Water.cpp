#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "common.h"

#include "vmath.h"

#include "Quad.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow;
bool gbIsFullScreen;

bool gbIsLightEnable = true;
int lKeyPressed = 1;

//
// Shader Related Variables
//
extern GLuint refraction_fbo_texture;
extern GLuint refraction_giShaderProgramObject;
extern GLuint quad_giShaderProgramObject;

int gWindowWidth;
int gWindowHeight;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpSzCmdLine, int iCmdShow)
{
	//
	// Function prototype
	//
	int Initialize(void);
	void Display(void);
	void Update(void);
	void ToggledFullScreen(void);
	void ReSize(int, int);

	//
	// Variable declarations
	//
	bool bDone = false;
	MSG msg = { 0 };
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("Water");

	//
	// Code
	//
	if (0 != fopen_s(&gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Water"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100, 100, 800, 600, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-2 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-3 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
			"Failed.\nExitting Now... "
			"Successfully\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-4 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-5 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
			"\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-6 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-7 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-8 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-9 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-10 == iRet)
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	/*ToggledFullScreen();*/

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow)
			{
			}

			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function declarations
	//
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);
	int Initialize_Shaders(const GLchar *, const GLchar *);
	void Uninitialize_Shaders(void);

	//
	// Code
	//
	switch (iMsg)
	{
		case WM_DESTROY: 
		{
			Uninitialize();
			PostQuitMessage(0);
		} break;


		case WM_ERASEBKGND:
		{
			
		} break;

		case WM_CLOSE:
		{
			DestroyWindow(hwnd);
		} break;

		case WM_KILLFOCUS:
		{
			gbActiveWindow = false;
		} break;

		case WM_SETFOCUS:
		{
			gbActiveWindow = true;
		} break;

		case WM_SIZE:
		{
			ReSize(LOWORD(lParam), HIWORD(lParam));
		} break;

		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_ESCAPE:
				{
					DestroyWindow(hwnd);
				} break;

				case 'F':
				case 'f':
				if (false == gbIsFullScreen)
				{
					ToggledFullScreen();
				}
				else
				{
					ToggledFullScreen();
				}
			
				break;

				case 'L':
				case 'l':
				{
					if (false == gbIsLightEnable)
					{
						gbIsLightEnable = true;
						lKeyPressed = 1;
					} else {
						gbIsLightEnable = false;
						lKeyPressed = 0;
					}
				} break;

			} // Switch Case End

		} break; // WM_KEYDOWN
	} // Switch Case End

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	//
	// Variable declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.top, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}

	gWindowWidth = width;
	gWindowHeight = height;

	//
	// ToDo : Resize FBO If Required
	//
	//
	// Code Of Viewport setting and prespectivematrix initialization moved into respective Display Function
	//

	return;
}

void Display(void) 
{
	//
	// Code
	//

	Refraction_Display();
	
	Quad_Display();

	SwapBuffers(gHdc);
}

void Uninitialize(void)
{
	//
	// Code
	//
	Refraction_Uninitialize_Shaders();
	Refraction_Uninitialize();

	Quad_Uninitialize_Shaders();
	Quad_Uninitialize();

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != gpFile)
	{
		fclose(gpFile);
	}

	return;
}

int Initialize(void)
{
	//
	// Function Declarations
	//
	void ReSize(int, int);
	void Uninitialize(void);

	//
	// Variable declarations
	//
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	//
	// Code
	//
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index)
	{
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, index, &pfd))
	{
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc))
	{
		return -4;
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result)
	{
		return -5;
	}

	gWindowWidth = 800;
	gWindowHeight = 600;

	Quad_Initialize_Shader();
	Quad_Initialize();

	Refraction_Initialize_Shaders();
	Refraction_Initialize();

	//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glDepthFunc(GL_LEQUAL);

	ReSize(gWindowWidth, gWindowHeight);

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);

	return 0;
}

void Update(void)
{
	return;
}
