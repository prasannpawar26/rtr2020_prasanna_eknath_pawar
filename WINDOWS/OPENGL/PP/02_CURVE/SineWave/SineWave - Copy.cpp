#include <stdio.h>
#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao;
GLuint vbo;
GLuint mvpUniform;
GLuint gLineColorUniform;

GLfloat gTimeLine = -3.0f;

mat4 gPerspectiveProjectionMatrix; // ->declared in vmath.h

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Curve");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                        szAppName,
                        TEXT("Curve"),
                        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                        100,
                        100,
                        800,
                        600,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile, "Initialize Successful.\n");
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {

    case WM_ACTIVATE:
      if (HIWORD(wParam) == 0) { // if 0, the window is active
        gbActiveWindow = true;
      } else {
        gbActiveWindow = false;
      }
      break;

    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;
      }
    } break;

  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject) {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -6;
  }

  const GLchar *vertexShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "in vec4 vPosition;" \
	  "uniform mat4 u_mvp_matrix;" \
      "void main(void)" \
      "{" \
         "gl_Position = u_mvp_matrix * vPosition;" \
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);

  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);
        fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -7;
      }
    }
  }
  fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject) 
  {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "uniform vec4 lineColor;" \
      "out vec4 FragColor;" \
      "void main(void)" \
      "{" \
      "FragColor = lineColor;" \
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);

  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;
        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
        fprintf(gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -9;
      }
    }
  }
  fprintf(gpFile, "Fragment Shader Compiled Successfully\n");

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                          "vPosition");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus)
  {
    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                            szInfoLog);

        fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

        free(szInfoLog);
        return -10;
      }
    }
  }

  fprintf(gpFile, "Shader Program Linked Successfully\n");

  mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");
  gLineColorUniform = glGetUniformLocation(giShaderProgramObject, "lineColor");


  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 360 * 3, NULL, GL_DYNAMIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION,
                        3,
                        GL_FLOAT,
                        GL_FALSE,
                        0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  glLineWidth(1.0f);

  gPerspectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height)
{
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Display(void)
{
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(giShaderProgramObject);

  mat4 modelViewMatrix;
  mat4 modelViewProjectionMatrix;

  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
  modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
  glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));

  glBindVertexArray(vao);

  //const GLfloat vertices[] = { -0.50f, 0.0f, 0.0f, 0.50f, 0.0f, 0.0f };

  //GLfloat vertices[1080];

  //GLfloat x = -0.50f;
  //int i = 0;
  //for (int angle = 0; angle < 360; angle++)
  //{
	 // vertices[i + 0] = x;
	 // vertices[i + 1] = sin(angle);
	 // vertices[i + 2] = 0.0f;

	 // i += 3;
	 // x += 1.0f / 360.0f;

  //}

  GLfloat vertices[1080];

  int i = 0;
  GLfloat amblitude = 0.10f; // Controls Y Factor - Height
  GLfloat angular_frequency = 1.0f; // Frequency Of Sine Wave In 1 Unit.

  // Sine Wave Equation
  // y(t) = A * sin(2 * Pi * frequency * time + radian of angle);
  ; // X -Axis Points
  int angle = 0;
  for (GLfloat time_x = -3.0f; time_x < gTimeLine; time_x += 1.0f / 360.0f)
  {
	  vertices[i + 0] = time_x;
	  vertices[i + 1] = amblitude * sin(2.0f * 3.14f * angular_frequency * time_x + vmath::radians(angle));
	  vertices[i + 2] = 0.0f;

	  angle++;
	  if (angle > 360)
	  {
		  angle = 0;
	  }

	  i += 3;
  }

  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glDrawArrays(GL_LINE_STRIP, 0, 360);

  glBindVertexArray(0);

  glUseProgram(0);

  gTimeLine += 1.0f / 360.0f;

  SwapBuffers(gHdc);

  return;
}

void Uninitialize(void)
{
  // code
  if (vbo)
  {
    glDeleteBuffers(1, &vbo);
    vbo = 0;
  }

  if (vao)
  {
    glDeleteVertexArrays(1, &vao);
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}
