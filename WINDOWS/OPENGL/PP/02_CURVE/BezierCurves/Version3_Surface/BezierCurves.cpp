#include <stdio.h>
#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao_curve;
GLuint vbo_curve;

#define K1(T) ((1 - T) * (1 - T) * (1 - T))
#define K2(T) (3 * (1 - T) * (1 - T) * T)
#define K3(T) (3 * (1 - T) * T * T)
#define K4(T) (T * T * T)

float ControlPoints[16][3] =
{
	{-1.0f, 1.0f, 0.0f},
	{-0.33f, 1.0f, 0.0f},
	{0.33f, 1.0f, 0.0f},
	{1.0f, 1.0f, 0.0f},

	{-1.0f, 0.33f, 0.0f},
	{-0.33f, 0.33f, 0.0f},
	{0.33f, 0.33f, 0.0f},
	{1.0f, 0.33f, 0.0f},

	{-1.0f, -0.33f, 0.0f},
	{-0.33f, -0.33f, 0.0f},
	{0.33f, -0.33f, 0.0f},
	{1.0f, -0.33f, 0.0f},

	{-1.0f, -1.0f, 0.0f},
	{-0.33f, -1.0f, 0.0f},
	{0.33f, -1.0f, 0.0f},
	{1.0f, -1.0f, 0.0f}
};

int gbSelectControlPoint = 0;

GLuint mvpUniform;
GLuint gLineColorUniform;

mat4 gPerspectiveProjectionMatrix; // ->declared in vmath.h

#define NO_OF_VERTICES 12000
#define VERTICES_COMPONENT 3
#define TOTAL_VERTICES_COMPONENT  NO_OF_VERTICES * VERTICES_COMPONENT
#define VERETICES 121
#define TOTAL_COMPONENT VERETICES * 3

GLfloat curve1_vertices[TOTAL_COMPONENT];

void EvaluateBezierCurve(float ControlPoint[4][3], float v, float *point)
{
	float b0 = (1 - v) * (1 - v) * (1 - v);
	float b1 = 3 * v * (1 - v) * (1 - v);
	float b2 = 3 * v * v * (1 - v);
	float b3 = v * v * v;

	(point)[0] = ControlPoint[0][0] * b0;
	(point)[1] = ControlPoint[0][1] * b0;
	(point)[2] = ControlPoint[0][2] * b0;

	(point)[0] += ControlPoint[1][0] * b1;
	(point)[1] += ControlPoint[1][1] * b1;
	(point)[2] += ControlPoint[1][2] * b1;

	(point)[0] += ControlPoint[2][0] * b2;
	(point)[1] += ControlPoint[2][1] * b2;
	(point)[2] += ControlPoint[2][2] * b2;

	(point)[0] += ControlPoint[3][0] * b3;
	(point)[1] += ControlPoint[3][1] * b3;
	(point)[2] += ControlPoint[3][2] * b3;

	return;
}

void EvaluateBezierSurface(float ControlPoint[16][3], float u, float v, float *point)
{
	float Pu[4][3];
	// compute 4 control points along u direction
	for (int i = 0; i < 4; ++i)
	{
		float curveP[4][3];

		curveP[0][0] = ControlPoint[i * 4 + 0][0];
		curveP[0][1] = ControlPoint[i * 4 + 0][1];
		curveP[0][2] = ControlPoint[i * 4 + 0][2];

		curveP[1][0] = ControlPoint[i * 4 + 1][0];
		curveP[1][1] = ControlPoint[i * 4 + 1][1];
		curveP[1][2] = ControlPoint[i * 4 + 1][2];

		curveP[2][0] = ControlPoint[i * 4 + 2][0];
		curveP[2][1] = ControlPoint[i * 4 + 2][1];
		curveP[2][2] = ControlPoint[i * 4 + 2][2];

		curveP[3][0] = ControlPoint[i * 4 + 3][0];
		curveP[3][1] = ControlPoint[i * 4 + 3][1];
		curveP[3][2] = ControlPoint[i * 4 + 3][2];

		float Temp[3];

		EvaluateBezierCurve(curveP, u, Temp);

		Pu[i][0] = Temp[0];
		Pu[i][1] = Temp[1];
		Pu[i][2] = Temp[2];
	}

	float Temp[3];
	EvaluateBezierCurve(Pu, v, Temp);

	point[0] = Temp[0];
	point[1] = Temp[1];
	point[2] = Temp[2];

	return;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Curve");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                        szAppName,
                        TEXT("Curve"),
                        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                        100,
                        100,
                        800,
                        600,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile, "Initialize Successful.\n");
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {

    case WM_ACTIVATE:
      if (HIWORD(wParam) == 0) { // if 0, the window is active
        gbActiveWindow = true;
      } else {
        gbActiveWindow = false;
      }
      break;

    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'T':
        case 't':
          ToggledFullScreen();
          break;

		case VK_NUMPAD1:
		{
			gbSelectControlPoint = 0;
		}
			break;
		case VK_NUMPAD2:
		{
			gbSelectControlPoint = 1;
		}
			break;
		case VK_NUMPAD3:
		{
			gbSelectControlPoint = 2;
		}
			break;
		case VK_NUMPAD4:
		{
			gbSelectControlPoint = 3;
		}
			break;
		case VK_NUMPAD5:
		{
			gbSelectControlPoint = 4;
		}
		break;
		case VK_NUMPAD6:
		{
			gbSelectControlPoint = 5;
		}
		break;
		case VK_NUMPAD7:
		{
			gbSelectControlPoint = 6;
		}
		break;
		case VK_NUMPAD8:
		{
			gbSelectControlPoint = 7;
		}
		break;
		case VK_UP:
		{
			ControlPoints[gbSelectControlPoint][1] += 0.1f;
		}
			break;

		case VK_DOWN:
		{
			ControlPoints[gbSelectControlPoint][1] -= 0.1f;
		}
			break;

		case VK_LEFT:
		{
			ControlPoints[gbSelectControlPoint][0] -= 0.1f;
		}
			break;

		case VK_RIGHT:
		{
			ControlPoints[gbSelectControlPoint][0] += 0.1f;
		}
			break;
      }
    } break;

  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject) {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -6;
  }

  const GLchar *vertexShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "in vec4 vPosition;" \
	  "uniform mat4 u_mvp_matrix;" \
      "void main(void)" \
      "{" \
         "gl_Position = u_mvp_matrix * vPosition;" \
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);

  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);
        fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -7;
      }
    }
  }
  fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject) 
  {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core" \
      "\n" \
      "uniform vec4 lineColor;" \
      "out vec4 FragColor;" \
      "void main(void)" \
      "{" \
      "FragColor = lineColor;" \
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);

  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;
        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
        fprintf(gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -9;
      }
    }
  }
  fprintf(gpFile, "Fragment Shader Compiled Successfully\n");

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                          "vPosition");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus)
  {
    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                            szInfoLog);

        fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

        free(szInfoLog);
        return -10;
      }
    }
  }

  fprintf(gpFile, "Shader Program Linked Successfully\n");

  mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");
  gLineColorUniform = glGetUniformLocation(giShaderProgramObject, "lineColor");

	// SINE 
  glGenVertexArrays(1, &vao_curve);
  glBindVertexArray(vao_curve);

  glGenBuffers(1, &vbo_curve);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_curve);

  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * TOTAL_VERTICES_COMPONENT, NULL, GL_DYNAMIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION,
                        3,
                        GL_FLOAT,
                        GL_FALSE,
                        0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  glBindVertexArray(0);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  glLineWidth(1.0f);

  gPerspectiveProjectionMatrix = mat4::identity();

  ZeroMemory(curve1_vertices, sizeof(float) * TOTAL_COMPONENT);

  float time_u = 0.0f;
  float time_v = 0.0f;
  float time_interval = 0.1f;

  int i = 0;
  int j = 0;

  while (time_u <= 1.100000f)
  {
	  time_v = 0.0f;

	  while (time_v <= 1.100000f)
	  {
		  float ResultantPoint[3] = { 0.0f, 0.0f, 0.0f };

		  EvaluateBezierSurface(ControlPoints, time_u, time_v, ResultantPoint);

		  curve1_vertices[(i * 11 + j) * 3 + 0] = ResultantPoint[0];
		  curve1_vertices[(i * 11 + j) * 3 + 1] = ResultantPoint[1];
		  curve1_vertices[(i * 11 + j) * 3 + 2] = ResultantPoint[2];

		  j++;
		  time_v += time_interval;
	  }

	  i++;
	  time_u += time_interval;
  }


  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height)
{
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Display(void)
{
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  

  mat4 modelViewMatrix;
  mat4 modelViewProjectionMatrix;

  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
  modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
  
 // 


  glUseProgram(giShaderProgramObject);

  // SINE
  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
  glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f));

  glBindVertexArray(vao_curve);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_curve);

  glBufferData(GL_ARRAY_BUFFER, sizeof(curve1_vertices), curve1_vertices, GL_DYNAMIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glDrawArrays(GL_LINE_STRIP, 0, VERETICES);

  glBindVertexArray(0);

  glUseProgram(0);

  SwapBuffers(gHdc);

  return;
}

void Uninitialize(void)
{
  // code
  if (vbo_curve)
  {
    glDeleteBuffers(1, &vbo_curve);
	vbo_curve = 0;
  }

  if (vao_curve)
  {
    glDeleteVertexArrays(1, &vao_curve);
	vao_curve = 0;
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}
