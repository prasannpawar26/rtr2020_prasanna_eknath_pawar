There are two main differences between point lights and directional lights.

First, with a point light source, the direction of maximum highlights must be computed at each vertex rather than with the precomputed value from LightSource[i].halfVector or reflection vector.

Second, light received at the surface is expected to decrease as the point light source gets farther and farther away.

This is called ATTENUATION. Each light source has constant, linear, and quadratic attenuation factors that are taken into account when the lighting contribution from a point light
is computed.
