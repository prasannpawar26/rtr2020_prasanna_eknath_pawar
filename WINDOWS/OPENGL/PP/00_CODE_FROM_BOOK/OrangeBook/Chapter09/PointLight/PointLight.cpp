#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "Sphere.h"
#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow;
bool gbIsFullScreen;

bool gbIsLightEnable;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum {
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao_sphere;
GLuint vbo_sphere_vertex;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_element;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;

// UNIFORMS
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint lightAmbientUniform;
GLuint lightDiffuseUniform;
GLuint lightSpecularUniform;
GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShinessUniform;
GLuint lightPositionUniform;
GLuint keyLPressedUniform;
//
// Application Variables Associated With Uniforms
float lightAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition[4] = {0.0f, 0.0f, 1.0f, 1.0f};

float materialAmbient[4] = {0.0f, 0.0f, 0.0f, 0.0f};
float materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialShiness = 50.0f;  // Also Try 128.0f

int lKeyPressed = 0;

mat4 modelMatrix;
mat4 viewMatrix;
mat4 perspectiveProjectionMatrix;
mat4 translateMatrix;
//

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("PP-ADS Light Per Vertex");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("PP-ADS Light Per Vertex"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, 800, 600, NULL, NULL, hInstance, NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
            "Failed.\nExitting Now... "
            "Successfully\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
            "Failed.\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
            "\nExitting Now... ",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
            "Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
            "Successful.\n",
            __FILE__, __LINE__, __FUNCTION__);
  }

  while (bDone == false) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {
    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case 'L':
        case 'l':
          if (false == gbIsLightEnable) {
            gbIsLightEnable = true;
            lKeyPressed = 1;
          } else {
            gbIsLightEnable = false;
            lKeyPressed = 0;
          }
          break;
      }
    } break;
  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen) {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

void ReSize(int width, int height) {
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  perspectiveProjectionMatrix =
      perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Display(void) {
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(giShaderProgramObject);

  modelMatrix = mat4::identity();
  viewMatrix = mat4::identity();
  translateMatrix = mat4::identity();

  viewMatrix = translate(0.0f, 0.0f, -3.0f);

  glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
  glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
  glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                     perspectiveProjectionMatrix);
  fprintf(gpFile, "lKeyPressed : %d\n", lKeyPressed);
  if (gbIsLightEnable) {
    glUniform1i(keyLPressedUniform, lKeyPressed);
    glUniform3fv(lightAmbientUniform, 1, lightAmbient);
    glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
    glUniform3fv(lightSpecularUniform, 1, lightSpecular);
    glUniform4fv(lightPositionUniform, 1, lightPosition);
    glUniform3fv(materialAmbientUniform, 1, materialAmbient);
    glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
    glUniform3fv(materialSpecularUniform, 1, materialSpecular);
    glUniform1f(materialShinessUniform, materialShiness);
  } else {
    glUniform1i(keyLPressedUniform, lKeyPressed);
  }

  glBindVertexArray(vao_sphere);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
  // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
  glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

  glBindVertexArray(0);

  modelMatrix = mat4::identity();
  viewMatrix = mat4::identity();
  translateMatrix = mat4::identity();

  viewMatrix = translate(2.0f, 0.0f, -7.0f);

  glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
  glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
  glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
	  perspectiveProjectionMatrix);

  glBindVertexArray(vao_sphere);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
  // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
  glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

  glBindVertexArray(0);

  glUseProgram(0);

  SwapBuffers(gHdc);
}

void Uninitialize(void) {
  // code

  if (vbo_sphere_element) {
    glDeleteBuffers(1, &vbo_sphere_element);
    vbo_sphere_element = 0;
  }
  if (vbo_sphere_normal) {
    glDeleteBuffers(1, &vbo_sphere_normal);
    vbo_sphere_normal = 0;
  }

  if (vbo_sphere_vertex) {
    glDeleteBuffers(1, &vbo_sphere_vertex);
    vbo_sphere_vertex = 0;
  }

  if (vao_sphere) {
    glDeleteVertexArrays(1, &vao_sphere);
    vao_sphere = 0;
  }

  if (giShaderProgramObject) {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders) {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc) {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc) {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc) {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(
      gpFile,
      "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
      __FILE__, __LINE__, __FUNCTION__);

  if (NULL != gpFile) {
    fclose(gpFile);
  }

  return;
}

int Initialize(void) {
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  void ReSize(int, int);
  void Uninitialize(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 8;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
          __FILE__, __LINE__, __FUNCTION__);

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
                      sphere_elements);
  gNumVertices = getNumberOfSphereVertices();
  gNumElements = getNumberOfSphereElements();

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

    return -6;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
          __FILE__, __LINE__, __FUNCTION__);

  const GLchar *vertexShaderSourceCode =
	  "#version 450 core"
	  "\n"
	  "in vec4 vPosition;"
	  "in vec3 vNormal;"
	  "uniform mat4 u_model_matrix;"
	  "uniform mat4 u_view_matrix;"
	  "uniform mat4 u_projection_matrix;"
	  "uniform vec3 u_light_ambient;"
	  "uniform vec3 u_light_diffuse;"
	  "uniform vec3 u_light_specular;"
	  "uniform vec4 u_light_position;"
	  "uniform vec3 u_material_ambient;"
	  "uniform vec3 u_material_diffuse;"
	  "uniform vec3 u_material_specular;"
	  "uniform float u_material_shiness;"
	  "uniform int u_key_L_pressed;"
	  "out vec3 phong_ads_light;"

	  "float distance;" // distance from surface to light source
	  "float pf;" // power factor
	  "float attenuation;"

	  "float constantAttenuation  = 1.0;"
	  "float linearAttenuation  = 0.10;"
	  "float quadraticAttenuation  = 0.20;"

	  "void main(void)"
	  "{"
	  "if(1 == u_key_L_pressed)"
	  "{"
	  "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"

	  "vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * "
	  "vNormal);"   //  incoming surface normals normalized

		// Compute vector from surface to light position
	  "vec3 light_direction = normalize(vec3(u_light_position - "
	  "eye_coordinates));"

	  "vec3 ecPosition3;"
	  "ecPosition3 = (vec3(eye_coordinates)) / eye_coordinates.w;"

	  "distance =  length(vec3(u_light_position) - ecPosition3);"

	  // Compute attenuation
	  "attenuation = 1.0 / (constantAttenuation + linearAttenuation * distance);"

	  "attenuation = clamp( 1.0 / (constantAttenuation + linearAttenuation * distance + quadraticAttenuation * distance * distance), 0.0, 1.0);"

		//nDotVP = max(0.0, dot(normal, VP));  Below Line Is Equivalent To This Line
		  "float t_normal_dot_light_direction = max(dot(light_direction, "
		  "t_normal), 0.0f);"

		  "vec3 reflection_vector = reflect(-light_direction, t_normal);"
      
		  "vec3 viewer_vector = normalize(vec3(-eye_coordinates));"
      
		  "vec3 ambient = u_light_ambient * u_material_ambient * attenuation;"
      
		  "vec3 diffuse = u_light_diffuse * u_material_diffuse * "
		  "t_normal_dot_light_direction * attenuation;"
      
		  "vec3 specular = u_light_specular * u_material_specular * "
		  "pow(max(dot(reflection_vector, viewer_vector), 0.0f), "
		  "u_material_shiness) * attenuation;"

		  "phong_ads_light = ambient + diffuse + specular;"

      "}"
      "else"
      "{"
		"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
      "}"

		  "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * "
		  "vPosition;"
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);
  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);

    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -7;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core"
      "\n"
      "in vec3 phong_ads_light;"
      "uniform int u_key_L_pressed;"
      "out vec4 FragColor;"
      "void main(void)"
      "{"
      "FragColor = vec4(phong_ads_light, 1.0);"
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);
  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (FALSE == iShaderCompileStatus) {
    fprintf(gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
            "Compilation Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
                           szInfoLog);

        fprintf(
            gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -9;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  // Attributes Binding Before Linking
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                       "vPosition");

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus) {
    fprintf(
        gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
        __FILE__, __LINE__, __FUNCTION__);

    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength) {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog) {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                            szInfoLog);

        fprintf(gpFile,
                "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                "Failed:\n\t%s\n",
                __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        free(szInfoLog);
      }
    }

    return -10;
  }
  fprintf(gpFile,
          "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
          "Successfully\n",
          __FILE__, __LINE__, __FUNCTION__);

  // Uniform Binding After Linking
  modelMatrixUniform =
      glGetUniformLocation(giShaderProgramObject, "u_model_matrix");
  viewMatrixUniform =
      glGetUniformLocation(giShaderProgramObject, "u_view_matrix");
  projectionMatrixUniform =
      glGetUniformLocation(giShaderProgramObject, "u_projection_matrix");

  lightAmbientUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_ambient");
  lightDiffuseUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_diffuse");
  lightSpecularUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_specular");
  lightPositionUniform =
      glGetUniformLocation(giShaderProgramObject, "u_light_position");

  materialAmbientUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_ambient");
  materialDiffuseUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_diffuse");
  materialSpecularUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_specular");
  materialShinessUniform =
      glGetUniformLocation(giShaderProgramObject, "u_material_shiness");

  keyLPressedUniform =
      glGetUniformLocation(giShaderProgramObject, "u_key_L_pressed");

  // Default Initialization
  glUniform1i(keyLPressedUniform, lKeyPressed);
  glUniform3fv(lightAmbientUniform, 1, lightAmbient);
  glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
  glUniform3fv(lightSpecularUniform, 1, lightSpecular);
  glUniform4fv(lightPositionUniform, 1, lightPosition);
  glUniform3fv(materialAmbientUniform, 1, materialAmbient);
  glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
  glUniform3fv(materialSpecularUniform, 1, materialSpecular);
  glUniform1f(materialShinessUniform, materialShiness);

  // cube
  glGenVertexArrays(1, &vao_sphere);
  glBindVertexArray(vao_sphere);

  glGenBuffers(1, &vbo_sphere_vertex);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_vertex);

  glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices,
               GL_STATIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenBuffers(1, &vbo_sphere_normal);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
  glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals,
               GL_STATIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // element vbo
  glGenBuffers(1, &vbo_sphere_element);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements),
               sphere_elements, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  modelMatrix = mat4::identity();
  viewMatrix = mat4::identity();
  perspectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
          __FILE__, __LINE__, __FUNCTION__);
  return 0;
}
