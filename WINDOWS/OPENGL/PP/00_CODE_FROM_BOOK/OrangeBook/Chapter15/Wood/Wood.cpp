#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao_turbulence;
GLuint vbo_turbulence;

GLuint mvpUniform;
GLuint scaleUniform;
GLuint sampler3DNoiseUniform;
GLuint lightWoodUniform;
GLuint darkWoodUniform;
GLuint ringFreqUniform;
GLuint lightGrainsUniform;
GLuint darkGrainsUniform;
GLuint grainThresholdUniform;
GLuint noiseScaleUniform;
GLuint noisinessUniform;
GLuint grainScaleUniform;


GLuint texture_cloud;
GLubyte * noise3DTexPtr = NULL;

float vertices[12] = {
	1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f
	};

mat4 gPerspectiveProjectionMatrix; // ->declared in vmath.h

#define N 0x1000
#define NP 12   /* 2^N */
#define NM 0xfff

#define s_curve(t) ( t * t * (3. - 2. * t) )

#define lerp(t, a, b) ( a + t * (b - a) )

#define setup(i,b0,b1,r0,r1)\
        t = vec[i] + N;\
        b0 = ((int)t) & BM;\
        b1 = (b0+1) & BM;\
        r0 = t - (int)t;\
        r1 = r0 - 1.;

#define at2(rx,ry) ( rx * q[0] + ry * q[1] )

#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )

enum { MAXB = 0x100 };

int p[MAXB + MAXB + 2];
double g3[MAXB + MAXB + 2][3];
double g2[MAXB + MAXB + 2][2];
double g1[MAXB + MAXB + 2];

int start;
int B;
int BM;

void Normalize2(double v[2])
{
	double s;

	s = sqrt(v[0] * v[0] + v[1] * v[1]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
}

void Normalize3(double v[3])
{
	double s;

	s = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
	v[2] = v[2] / s;
}

void InitNoise(void)
{
	int i, j, k;

	srand(30757);
	for (i = 0; i < B; i++)
	{
		p[i] = i;
		g1[i] = (double)((rand() % (B + B)) - B) / B;

		for (j = 0; j < 2; j++)
		{
			g2[i][j] = (double)((rand() % (B + B)) - B) / B;
		}

		Normalize2(g2[i]);

		for (j = 0; j < 3; j++)
		{
			g3[i][j] = (double)((rand() % (B + B)) - B) / B;
		}

		Normalize3(g3[i]);
	}

	while (--i)
	{
		k = p[i];
		p[i] = p[j = rand() % B];
		p[j] = k;
	}

	for (i = 0; i < B + 2; i++)
	{
		p[B + i] = p[i];
		g1[B + i] = g1[i];

		for (j = 0; j < 2; j++)
		{
			g2[B + i][j] = g2[i][j];
		}

		for (j = 0; j < 3; j++)
		{
			g3[B + i][j] = g3[i][j];
		}
	}
}
void SetNoiseFrequency(int frequency)
{
	start = 1;
	B = frequency;
	BM = B - 1;
}

double noise3(double vec[3])
{
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
	double rx0, rx1, ry0, ry1, rz0, rz1, *q, sy, sz, a, b, c, d, t, u, v;
	int i, j;

	if (start) {
		start = 0;
		InitNoise();
	}

	setup(0, bx0, bx1, rx0, rx1);
	setup(1, by0, by1, ry0, ry1);
	setup(2, bz0, bz1, rz0, rz1);

	i = p[bx0];
	j = p[bx1];

	b00 = p[i + by0];
	b10 = p[j + by0];
	b01 = p[i + by1];
	b11 = p[j + by1];

	t = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);

	q = g3[b00 + bz0]; u = at3(rx0, ry0, rz0);
	q = g3[b10 + bz0]; v = at3(rx1, ry0, rz0);
	a = lerp(t, u, v);

	q = g3[b01 + bz0]; u = at3(rx0, ry1, rz0);
	q = g3[b11 + bz0]; v = at3(rx1, ry1, rz0);
	b = lerp(t, u, v);

	c = lerp(sy, a, b);

	q = g3[b00 + bz1]; u = at3(rx0, ry0, rz1);
	q = g3[b10 + bz1]; v = at3(rx1, ry0, rz1);
	a = lerp(t, u, v);

	q = g3[b01 + bz1]; u = at3(rx0, ry1, rz1);
	q = g3[b11 + bz1]; v = at3(rx1, ry1, rz1);
	b = lerp(t, u, v);

	d = lerp(sy, a, b);

	//fprintf(stderr, "%f\n", lerp(sz, c, d));

	return lerp(sz, c, d);
}
void Make3DNoiseTexture(void)
{
	int f, i, j, k, inc;
	int startFrequency = 4;
	int numOctaves = 4;
	double ni[3];
	double inci, incj, inck;
	int frequency = startFrequency;
	GLubyte *ptr;
	double amp = 0.5;

	if ((noise3DTexPtr = (GLubyte *)malloc(CLOUD_IMAGE_WIDTH *
		CLOUD_IMAGE_HEIGHT *
		CLOUD_IMAGE_DEPTH * 4)) == NULL)
	{
		fprintf(gpFile, "ERROR: Could not allocate 3D noise texture\n");
		exit(1);
	}

	for (f = 0, inc = 0; f < numOctaves;
		++f, frequency *= 2, ++inc, amp *= 0.5)
	{
		SetNoiseFrequency(frequency);

		ptr = noise3DTexPtr;

		ni[0] = ni[1] = ni[2] = 0;

		inci = 1.0 / (CLOUD_IMAGE_WIDTH / frequency);

		for (i = 0; i < CLOUD_IMAGE_WIDTH; ++i, ni[0] += inci)
		{
			incj = 1.0 / (CLOUD_IMAGE_HEIGHT / frequency);

			for (j = 0; j < CLOUD_IMAGE_HEIGHT; ++j, ni[1] += incj)
			{
				inck = 1.0 / (CLOUD_IMAGE_DEPTH / frequency);

				for (k = 0; k < CLOUD_IMAGE_DEPTH; ++k, ni[2] += inck, ptr += 4)
				{
					*(ptr + inc) = (GLubyte)(((noise3(ni) + 1.0) * amp)*128.0);
				}
			}
		}
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Marble");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                        szAppName,
                        TEXT("Marble"),
                        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                        100,
                        100,
                        800,
                        600,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile, "Initialize Successful.\n");
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {

    case WM_ACTIVATE:
      if (HIWORD(wParam) == 0) { // if 0, the window is active
        gbActiveWindow = true;
      } else {
        gbActiveWindow = false;
      }
      break;

    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;
      }
    } break;

  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

void Init3DNoiseTexture(void)
{
	glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &texture_cloud);
	glBindTexture(GL_TEXTURE_3D, texture_cloud);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, CLOUD_IMAGE_WIDTH,
		CLOUD_IMAGE_HEIGHT, CLOUD_IMAGE_DEPTH, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, noise3DTexPtr);

	return;
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);
  void Init3DNoiseTexture(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject) {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -6;
  }

  const GLchar *vertexShaderSourceCode =
      "#version 450 core" \
      "\n" \

      "in vec4 vPosition;" \

	  "uniform mat4 u_mvp_matrix;" \
	  "uniform float u_scale;" \

	  "out vec3 mcPosition;" \

      "void main(void)" \
      "{" \
			"mcPosition = vec3(vPosition) * u_scale;" \

			"gl_Position = u_mvp_matrix * vPosition;" \
      "}";

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);

  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);
        fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -7;
      }
    }
  }
  fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject) 
  {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -8;
  }



  const GLchar *fragmentShaderSourceCode =
      "#version 450 core" \
      "\n" \

	  "in vec3 mcPosition;" \

	  "uniform float u_scale;" \
	  "uniform vec3 u_light_wood;" \
	  "uniform vec3 u_dark_wood;" \
	  "uniform float u_ring_freq;" \
	  "uniform float u_light_grains;" \
	  "uniform float u_dark_grains;" \
	  "uniform float u_grain_threshold;" \
	  "uniform vec3 u_noise_scale;" \
	  "uniform float u_noisiness;" \
	  "uniform float u_grain_scale;" \

	  "uniform sampler3D u_noise;" \

      "out vec4 FragColor;" \

      "void main(void)" \
      "{" \
			"vec3 noise_vector = vec3(texture(u_noise, mcPosition * u_noise_scale) * u_noisiness);" \

			"vec3 location = mcPosition + noise_vector;" \

			"float dist = sqrt(location.x * location.x + location.z * location.z);" \

			"dist *= u_ring_freq;" \

			"float r = fract(dist + noise_vector[0] + noise_vector[1] + noise_vector[2]) * 2.0;" \

			"if(r > 1.0)" \
			"{" \
				"r = 2.0 - r;" \
			"}" \

			"vec3 color = mix(u_light_wood, u_dark_wood, r);" \

			"r = fract((mcPosition.x + mcPosition.z) * u_grain_scale + 0.5);" \

			"noise_vector[2] *= r;" \

			"if(r < u_grain_threshold)" \
			"{"
				"color += u_light_wood * u_light_grains * noise_vector[2];" \
			"}" \
			"else" \
			"{" \
				"color -= u_light_wood * u_dark_grains * noise_vector[2];" \
			"}" \

			"FragColor = vec4(color, 1.0);" \
      "}";

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);

  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;
        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
        fprintf(gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -9;
      }
    }
  }
  fprintf(gpFile, "Fragment Shader Compiled Successfully\n");

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                          "vPosition");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus)
  {
    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                            szInfoLog);

        fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

        free(szInfoLog);
        return -10;
      }
    }
  }

  fprintf(gpFile, "Shader Program Linked Successfully\n");

  mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");
  scaleUniform = glGetUniformLocation(giShaderProgramObject, "u_scale");
  sampler3DNoiseUniform = glGetUniformLocation(giShaderProgramObject, "u_noise");
  lightWoodUniform = glGetUniformLocation(giShaderProgramObject, "u_light_wood");
  darkWoodUniform = glGetUniformLocation(giShaderProgramObject, "u_dark_wood");
  ringFreqUniform = glGetUniformLocation(giShaderProgramObject, "u_ring_freq");
  lightGrainsUniform = glGetUniformLocation(giShaderProgramObject, "u_light_grains");
  darkGrainsUniform = glGetUniformLocation(giShaderProgramObject, "u_dark_grains");
  grainThresholdUniform = glGetUniformLocation(giShaderProgramObject, "u_grain_threshold");
  noiseScaleUniform =  glGetUniformLocation(giShaderProgramObject, "u_noise_scale");
  noisinessUniform = glGetUniformLocation(giShaderProgramObject, "u_noisiness");
  grainScaleUniform = glGetUniformLocation(giShaderProgramObject, "u_grain_scale");

  glGenVertexArrays(1, &vao_turbulence);
  glBindVertexArray(vao_turbulence);

  glGenBuffers(1, &vbo_turbulence);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_turbulence);

  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 12, NULL, GL_DYNAMIC_DRAW);

  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION,
                        3,
                        GL_FLOAT,
                        GL_FALSE,
                        0,
                        NULL);

  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  glBindVertexArray(0);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  glEnable(GL_TEXTURE_3D);

  glLineWidth(1.0f);

  Make3DNoiseTexture();
  Init3DNoiseTexture();

  gPerspectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height)
{
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Display(void)
{
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  

  mat4 modelViewMatrix;
  mat4 modelViewProjectionMatrix;

  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
  modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
 
  glUseProgram(giShaderProgramObject);

  glBindTexture(GL_TEXTURE_3D, texture_cloud);

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
  glUniform1f(scaleUniform, 2.0);
  glUniform1i(sampler3DNoiseUniform, 0); // glActiveTexture(GL_TEXTURE0);
  glUniform3f(lightWoodUniform, 0.60, 0.3, 0.1);
  glUniform3f(darkWoodUniform, 0.40, 0.2, 0.07);
  glUniform1f(ringFreqUniform, 4.0);
  glUniform1f(lightGrainsUniform, 1.0);
  glUniform1f(darkGrainsUniform, 0.0);
  glUniform1f(grainThresholdUniform, 0.5);
  glUniform3f(noiseScaleUniform, 0.5, 0.1, 0.1);
  glUniform1f(noisinessUniform, 3.0);
  glUniform1f(grainScaleUniform, 27.0);

  glBindVertexArray(vao_turbulence);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_turbulence);

  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  glBindVertexArray(0);

  glBindTexture(GL_TEXTURE_3D, 0);
  glUseProgram(0);

  SwapBuffers(gHdc);

  return;
}

void Uninitialize(void)
{
  // code
  if (vbo_turbulence)
  {
    glDeleteBuffers(1, &vbo_turbulence);
	vbo_turbulence = 0;
  }

  if (vao_turbulence)
  {
    glDeleteVertexArrays(1, &vao_turbulence);
	vao_turbulence = 0;
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}
