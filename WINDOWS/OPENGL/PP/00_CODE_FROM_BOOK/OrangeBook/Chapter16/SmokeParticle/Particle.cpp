#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

float gParticleTime = 0.0f;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0,
  AMC_ATTRIBUTES_VELOCITY,
  AMC_ATTRIBUTES_START_TIME
};

GLuint vao;
GLuint vbo_vertices;
GLuint vbo_colors;
GLuint vbo_velocities;
GLuint vbo_starttimes;

GLuint mvpUniform;
GLuint timeUniform;
GLuint backgroundUniform;

static GLfloat *gVertices = NULL;
static GLfloat *gColors = NULL;
static GLfloat *gVelocities = NULL;
static GLfloat *gStartTimes = NULL;

GLint gWidth;
GLint gHeight;

void CreatePoints(GLint width, GLint height)
{
	// Variable Declarations
	GLfloat *ptrVertices, *ptrColors, *ptrVelocities, *ptrStartTimes;
	GLfloat i, j;

	if (gVertices != NULL)
	{
		free(gVertices);
	}

	gVertices = (GLfloat *)malloc(width * height * 3 * sizeof(GLfloat));
	gColors = (GLfloat *)malloc(width * height * 3 * sizeof(GLfloat));
	gVelocities = (GLfloat *)malloc(width * height * 3 * sizeof(GLfloat));
	gStartTimes = (GLfloat *)malloc(width * height * sizeof(GLfloat));

	ptrVertices = gVertices;
	ptrColors = gColors;
	ptrVelocities = gVelocities;
	ptrStartTimes = gStartTimes;

	for (i = 0.5 / width - 0.5; i < 0.5; i = i + 1.0 / width)
	{
		for (j = 0.5 / height - 0.5; j < 0.5; j = j + 1.0 / height)
		{
			*(ptrVertices + 0) = i;
			*(ptrVertices + 1) = 0.0;
			*(ptrVertices + 2) = j;
			ptrVertices += 3;

			*(ptrColors + 0) = ((float) rand() / RAND_MAX) * 0.5 + 0.5;
			*(ptrColors + 1) = ((float) rand() / RAND_MAX) * 0.5 + 0.5;
			*(ptrColors + 2) = ((float) rand() / RAND_MAX) * 0.5 + 0.5;
			ptrColors += 3;

			*(ptrVelocities + 0) = (((float) rand() / RAND_MAX)) + 3.0;
			*(ptrVelocities + 1) = ((float) rand() / RAND_MAX) * 10.0;
			*(ptrVelocities + 2) = (((float) rand() / RAND_MAX)) + 3.0;
			ptrVelocities += 3;

			*ptrStartTimes = ((float) rand() / RAND_MAX) * 10.0;
			ptrStartTimes++;
		}
	}

	gWidth = width;
	gHeight = height;
}

mat4 gPerspectiveProjectionMatrix; // ->declared in vmath.h

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Particle");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                        szAppName,
                        TEXT("Particle"),
                        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                        100,
                        100,
                        800,
                        600,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile, "Initialize Successful.\n");
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {

      }
	  Update();
      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {

    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;
      }
    } break;

  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);
  void Init3DNoiseTexture(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
  if (0 == giVertexShaderObject) {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -6;
  }

  const GLchar *vertexShaderSourceCode =

	  "#version 450 core" \
	  "\n" \

	  "layout (location = 0) in vec4 vPosition;" \
	  "layout (location = 1) in vec4 vColor;" \
	  "layout (location = 2) in vec3 vVelocity;" \
	  "layout (location = 3) in float vStartTime;" \
	  "in float vStartTime;" \

	  "out vec2 TexCoords;" \
	  "out vec4 ParticleColor;" \

	  "uniform mat4 u_mvp_matrix;" \
	  "uniform vec2 u_offset;" \
	  "uniform vec4 u_color;" \
	  "uniform float u_time;"\

	  "void main()" \
	  "{" \
		  "vec4 vertex;" \
		  "float t =  u_time - vStartTime;" \

		  "if(t >= 0.0)"
		  "{"
			  "vertex = vPosition + vec4(vVelocity * t, 0.0);" \
			  "vertex.y -= 2.9 * t * t;" \
			  "ParticleColor = vColor;" \
		  "}"

		  "float scale = 10.0f;" \
		  "TexCoords = vertex.zw;" \
		  "gl_Position = u_mvp_matrix * vec4((vertex.xy * scale) + u_offset, 0.0, 1.0);" \
	  "}";

     /* "#version 450 core" \
      "\n" \

      "in vec4 vPosition;" \
	  "in vec4 vColor;" \
	  "in vec3 vVelocity;" \
	  "in float vStartTime;" \

	  "uniform mat4 u_mvp_matrix;" \
	  "uniform float u_time;"\
	  "uniform vec4 u_background;" \

	  "out vec4 color;" \

      "void main(void)" \
      "{" \
			"vec4 vertex;" \
			"float t =  u_time - vStartTime;" \

			"if(t >= 0.0)"
			"{"
				"vertex = vPosition + vec4(vVelocity * t, 0.0);" \
				"vertex.y -= 4.9 * t * t;" \
				"color = vColor;" \
			"}"
			"else"
			"{"
				"vertex = vPosition;" \
				"color = u_background;" \
			"}"

			"gl_Position = u_mvp_matrix * vertex;" \
      "}";*/

  glShaderSource(giVertexShaderObject, 1,
                 (const GLchar **)&vertexShaderSourceCode, NULL);

  glCompileShader(giVertexShaderObject);

  GLint iShaderCompileStatus = 0;
  GLint iInfoLogLength = 0;
  GLchar *szInfoLog = NULL;

  glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                           szInfoLog);
        fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -7;
      }
    }
  }
  fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

  iShaderCompileStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
  if (0 == giFragmentShaderObject) 
  {
    fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
    return -8;
  }

  const GLchar *fragmentShaderSourceCode =
      "#version 450 core" \
      "\n" \

	  "in vec2 TexCoords;" \
	  "in vec4 ParticleColor;" \
	  
	  "out vec4 FragColor;" \

	  "uniform sampler2D u_sprite;" \

	  "void main(void)" \
	  "{" \
		"FragColor = (texture(u_sprite, TexCoords) * ParticleColor);" \
	  "}";

	  /*"in vec4 color;" \
      "out vec4 FragColor;" \

      "void main(void)" \
      "{" \
			"FragColor = color;" \
      "}";*/

  glShaderSource(giFragmentShaderObject, 1,
                 (const GLchar **)&fragmentShaderSourceCode, NULL);

  glCompileShader(giFragmentShaderObject);

  glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
                &iShaderCompileStatus);
  if (GL_FALSE == iShaderCompileStatus)
  {
    glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;
        glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
        fprintf(gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
        free(szInfoLog);
        return -9;
      }
    }
  }
  fprintf(gpFile, "Fragment Shader Compiled Successfully\n");

  giShaderProgramObject = glCreateProgram();

  glAttachShader(giShaderProgramObject, giVertexShaderObject);
  glAttachShader(giShaderProgramObject, giFragmentShaderObject);

  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
                          "vPosition");
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_COLOR,
	  "vColor");
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_VELOCITY,
	  "vVelocity");
  glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_START_TIME,
	  "vStartTime");

  glLinkProgram(giShaderProgramObject);

  GLint iProgramLinkStatus = 0;
  iInfoLogLength = 0;
  szInfoLog = NULL;

  glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
  if (GL_FALSE == iProgramLinkStatus)
  {
    glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (0 < iInfoLogLength)
    {
      szInfoLog = (GLchar *)malloc(iInfoLogLength);
      if (NULL != szInfoLog)
      {
        GLsizei written;

        glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                            szInfoLog);

        fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

        free(szInfoLog);
        return -10;
      }
    }
  }

  fprintf(gpFile, "Shader Program Linked Successfully\n");

  mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");
  timeUniform = glGetUniformLocation(giShaderProgramObject, "u_time");
  backgroundUniform = glGetUniformLocation(giShaderProgramObject, "u_background");

  CreatePoints(800, 600);

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Vertices
  glGenBuffers(1, &vbo_vertices);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);
  glBufferData(GL_ARRAY_BUFFER, gWidth * gHeight * 3 * sizeof(GLfloat), gVertices, GL_STATIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Colors
  glGenBuffers(1, &vbo_colors);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_colors);
  glBufferData(GL_ARRAY_BUFFER, gWidth * gHeight * 3 * sizeof(GLfloat), gColors, GL_STATIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Velocities
  glGenBuffers(1, &vbo_velocities);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_velocities);
  glBufferData(GL_ARRAY_BUFFER, gWidth * gHeight * 3 * sizeof(GLfloat), gVelocities, GL_STATIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_VELOCITY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Start Time
  glGenBuffers(1, &vbo_starttimes);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_starttimes);
  glBufferData(GL_ARRAY_BUFFER, gWidth * gHeight * 1 * sizeof(GLfloat), gStartTimes, GL_STATIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_START_TIME, 1, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_START_TIME);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  glLineWidth(1.0f);

  gPerspectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height)
{
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

void Update(void)
{
	gParticleTime += 0.01f;
}

void Display(void)
{
  // code
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  mat4 modelViewMatrix;
  mat4 modelViewProjectionMatrix;

  modelViewMatrix = mat4::identity();
  modelViewProjectionMatrix = mat4::identity();
  modelViewMatrix = vmath::translate(-2.0f, 0.0f, -10.0f);
  modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
 
  glUseProgram(giShaderProgramObject);

  glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
  glUniform1f(timeUniform, gParticleTime);
  glUniform4f(backgroundUniform, 0.0, 1.0, 1.0, 0.0);

  glBindVertexArray(vao);

  glDrawArrays(GL_POINTS, 0, gWidth * gHeight);

  glBindVertexArray(0);

  glUseProgram(0);

  SwapBuffers(gHdc);

  return;
}

void Uninitialize(void)
{
  // code
  if (vbo_starttimes)
  {
    glDeleteBuffers(1, &vbo_starttimes);
	vbo_starttimes = 0;
  }

  if (vbo_velocities)
  {
	  glDeleteBuffers(1, &vbo_velocities);
	  vbo_velocities = 0;
  }

  if (vbo_colors)
  {
	  glDeleteBuffers(1, &vbo_colors);
	  vbo_colors = 0;
  }

  if (vbo_vertices)
  {
	  glDeleteBuffers(1, &vbo_vertices);
	  vbo_vertices = 0;
  }

  if (vao)
  {
    glDeleteVertexArrays(1, &vao);
	vao = 0;
  }

  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}
