#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <cmath>

#include "vmath.h"

#include "StartFiled.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;


GLuint giFinalVertexShaderObject;
GLuint giFinalFragmentShaderObject;
GLuint giFinalShaderProgramObject;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_VELOCITY,
  AMC_ATTRIBUTES_CONNECTED,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint mvpUniform;
GLuint timeUniform;


mat4 gPerspectiveProjectionMatrix;

enum
{
	NUM_STARS           = 2000
};





// Random number generator
static unsigned int seed = 0x13371337;

static inline float random_float()
{
	float res;
	unsigned int tmp;

	seed *= 16807;

	tmp = seed ^ (seed >> 4) ^ (seed << 15);

	*((unsigned int *) &res) = (tmp >> 9) | 0x3F800000;

	return (res - 1.0f);
}

GLuint vao;
GLuint vbo_position;
GLuint vbo_color;

GLuint texture;
GLuint sampler_uniform;

void Setup(void)
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLfloat * star_position = (GLfloat *)malloc(sizeof(GLfloat) * 3 * NUM_STARS);
	memset(star_position, 0, sizeof(GLfloat) * 3 * NUM_STARS);

	int i;
	for (i = 0; i < 1000 * 3; i += 3)
	{
		star_position[i + 0] = (random_float() * 2.0f - 1.0f) * 100.0f;
		star_position[i + 1] = (random_float() * 2.0f - 1.0f) * 100.0f;
		star_position[i + 2] = random_float();
	}

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * NUM_STARS, star_position, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	GLfloat * star_color = (GLfloat *)malloc(sizeof(GLfloat) * 3 * NUM_STARS);
	memset(star_color, 0, sizeof(GLfloat) * 3 * NUM_STARS);

	for (i = 0; i < 1000 * 3; i += 3)
	{
		star_color[i + 0] = 0.8f + random_float() * 0.2f;
		star_color[i + 1] = 0.8f + random_float() * 0.2f;
		star_color[i + 2] = 0.8f + random_float() * 0.2f;
	}
	glGenBuffers(1, &vbo_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * NUM_STARS, star_color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Cloud");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                        szAppName,
                        TEXT("Cloud"),
                        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                        100,
                        100,
                        800,
                        600,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile, "Initialize Successful.\n");
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {

    case WM_ACTIVATE:
      if (HIWORD(wParam) == 0) { // if 0, the window is active
        gbActiveWindow = true;
      } else {
        gbActiveWindow = false;
      }
      break;

    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;
      }
    } break;

  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}

int FinalShader(void)
{
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		
		"layout (location = 0) in vec4 vPosition;" \
		"layout (location = 0) in vec4 vColor;" \

		"uniform float u_time;" \
		"uniform mat4 u_mvp_matrix;" \
		
		"flat out vec4 starColor" \

		"void main(void)" \
		"{" \
			"vec4 newVertex = vPosition;" \

			"newVertex.z += u_time;" \

			"newVertex.z = fract(newVertex.z);" \

			"float size = (20.0 * newVertex.z * newVertex.z);" \

			"starColor = smoothstep(1.0, 7.0, size) * color;" \

			"newVertex.z = (999.9 * newVertex.z) - 1000.0;" \

			"gl_Position = u_mvp_matrix * newVertex;" \

			"gl_PointSize = size;" \
		"}";

	giFinalVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giFinalVertexShaderObject)
	{
		return -8;
	}

	glShaderSource(giFinalVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giFinalVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giFinalVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(giFinalVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(giFinalVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFinalFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFinalFragmentShaderObject)
	{
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader (GL_FRAGMENT_SHADER) "
			"Failed:\n",
			__FILE__, __LINE__, __FUNCTION__);
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"layout (location = 0) out vec4 FragColor;" \
		"uniform sampler2D u_tex_star;" \
		"flat in vec4 starColor;" \

		"void main(void)" \
		"{" \
			"FragColor = starColor * texture(u_tex_star, gl_PointCoord);" \
		"}"; 

	glShaderSource(giFinalFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(giFinalFragmentShaderObject);

	glGetShaderiv(giFinalFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giFinalFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFinalFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giFinalShaderProgramObject = glCreateProgram();

	glAttachShader(giFinalShaderProgramObject, giFinalVertexShaderObject);
	glAttachShader(giFinalShaderProgramObject, giFinalFragmentShaderObject);

	glBindAttribLocation(giFinalShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");
	glBindAttribLocation(giFinalShaderProgramObject, AMC_ATTRIBUTES_COLOR,
		"vColor");

	glLinkProgram(giFinalShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giFinalShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		glGetShaderiv(giFinalShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFinalShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -10;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	mvpUniform = glGetUniformLocation(giFinalShaderProgramObject, "u_mvp_matrix");
	timeUniform = glGetUniformLocation(giFinalShaderProgramObject, "u_time");
	sampler_uniform = glGetUniformLocation(giShaderProgramObject, "u_tex_star");

	return 0;
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);
  void Init3DNoiseTexture(void);
  BOOL LoadTexture(GLuint *texture, TCHAR imageResourceID[]);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  FinalShader();

  Setup();

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  gPerspectiveProjectionMatrix = mat4::identity();

  LoadTexture(&texture, MAKEINTRESOURCEA(IDBITMAP_SMILEY));

  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height)
{
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

float time = 0.0f;
void Display(void)
{
	static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	static const GLfloat one[] = { 1.0f };

	glClearBufferfv(GL_COLOR, 0, black);
	glClearBufferfv(GL_DEPTH, 0, one);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(giFinalShaderProgramObject);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(sampler_uniform, 0);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);

	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	time *= 0.1f;
	time -= floor(time);

	glUniform1f(timeUniform, time);
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	glBindVertexArray(vao);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glDrawArrays(GL_POINTS, 0, NUM_STARS);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(gHdc);
}

void Uninitialize(void)
{
  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}

BOOL LoadTexture(GLuint *texture, TCHAR imageResourceID[])
{
	// variable declarations
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	BOOL bStatus = FALSE;

	// code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceID,
		IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (NULL == hBitmap)
	{
		return bStatus;
	}

	bStatus = TRUE;

	GetObject(hBitmap, sizeof(bmp), &bmp);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glGenTextures(1, texture);

	glBindTexture(GL_TEXTURE_2D, *texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);

	// glTexImages2D : Parameters from 2 to 9 Get Attach To Target GL_TEXTURE_2D.
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR,
		GL_UNSIGNED_BYTE, bmp.bmBits);

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	DeleteObject(hBitmap);

	hBitmap = NULL;

	return bStatus;

}