#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;


GLuint giFinalVertexShaderObject;
GLuint giFinalFragmentShaderObject;
GLuint giFinalShaderProgramObject;

enum
{
  AMC_ATTRIBUTES_POSITION = 0,
  AMC_ATTRIBUTES_VELOCITY,
  AMC_ATTRIBUTES_CONNECTED,
  AMC_ATTRIBUTES_COLOR,
  AMC_ATTRIBUTES_NORMAL,
  AMC_ATTRIBUTES_TEXCOORD0
};

GLuint mvpUniform;
GLuint timeUniform;

mat4 gPerspectiveProjectionMatrix;

#define POINT_X 10
#define POINT_Y 10

#define POINT_TOTAL POINT_X * POINT_Y

#define TOTAL_VERTICES POINT_TOTAL * 4
#define TOTAL_VELOCITY POINT_TOTAL * 3
#define TOTAL_CONNECTED_VERTICES POINT_TOTAL * 4

#define CONNECTION_TOTAL  (POINT_X - 1) * POINT_Y + (POINT_Y - 1) * POINT_X

GLfloat vertices[TOTAL_VERTICES];
GLfloat velocity[TOTAL_VELOCITY];
GLint connected_vertices[TOTAL_CONNECTED_VERTICES];

void initialize_data(void)
{
	int indexVertices = 0;
	int indexVelocity = 0;
	int indexConnected = 0;

	for (int j = 0; j < POINT_Y; j++)
	{
		float fJ = (float)j / (float)POINT_Y;
		
		for (int i = 0; i < POINT_X; i++)
		{
			float fI = (float)i / (float)POINT_X;
			
			vertices[indexVertices + 0] = (fI * 0.1f) * (float)POINT_X;
			vertices[indexVertices + 1] = (fJ * 0.1f) * (float)POINT_Y;
			vertices[indexVertices + 2] = 0.6f * sinf(fI) * cos(fJ);
			vertices[indexVertices + 3] = 1.0f;

			velocity[indexVelocity + 0] = 0.0f;
			velocity[indexVelocity + 1] = 0.0f;
			velocity[indexVelocity + 2] = 0.0f;

			connected_vertices[indexConnected + 0] = -1;
			connected_vertices[indexConnected + 1] = -1;
			connected_vertices[indexConnected + 2] = -1;
			connected_vertices[indexConnected + 3] = -1;

			if (j != (POINT_Y - 1))
			{
				if (i != 0)
					connected_vertices[indexConnected + 0] = (int)(indexVertices / 4) - 1;
				if (j != 0)
					connected_vertices[indexConnected + 1] = (int)(indexVertices / 4) - POINT_X;
				if (i != (POINT_X - 1))
					connected_vertices[indexConnected + 2] = (int)(indexVertices / 4) + 1;
				if (j != (POINT_Y - 1))
					connected_vertices[indexConnected + 3] = (int)(indexVertices / 4) + POINT_X;

			}

			fprintf(gpFile, "vertices : [%f, %f, %f, %f]\n", vertices[indexVertices + 0], vertices[indexVertices + 1], vertices[indexVertices + 2], vertices[indexVertices + 3]);
			//fprintf(gpFile, "velocity : [%f, %f, %f]\n", velocity[indexVelocity + 0], velocity[indexVelocity + 1], velocity[indexVelocity + 2]);
			//fprintf(gpFile, "connected_vertices : [%d, %d, %d, %d]\n", connected_vertices[indexConnected + 0], connected_vertices[indexConnected + 1], connected_vertices[indexConnected + 2], connected_vertices[indexConnected + 3]);

			indexVertices += 4;
			indexVelocity += 3;
			indexConnected += 4;
		}
		fprintf(gpFile, "------------------------------------------------\n");
	}
}

GLuint vao[2];
GLuint vbo[5];
GLuint pos_tbo[2];

GLuint m_index_buffer;

#define POSITION_A 0
#define POSITION_B 1
#define VELOCITY_A 2
#define VELOCITY_B 3
#define CONNECTION 4

void Setup(void)
{
	glGenVertexArrays(2, vao);
	glGenBuffers(5, vbo);

	glBindVertexArray(vao[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[POSITION_A]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * POINT_TOTAL * 4, vertices, GL_DYNAMIC_COPY);
		glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[VELOCITY_A]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * POINT_TOTAL * 3, velocity, GL_DYNAMIC_COPY);
		glVertexAttribPointer(AMC_ATTRIBUTES_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_VELOCITY);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[CONNECTION]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * POINT_TOTAL * 4, connected_vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_CONNECTED, 4, GL_INT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_CONNECTED);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glBindVertexArray(vao[1]);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[POSITION_B]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * POINT_TOTAL * 4, vertices, GL_DYNAMIC_COPY);
		glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[VELOCITY_B]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * POINT_TOTAL * 3, velocity, GL_DYNAMIC_COPY);
		glVertexAttribPointer(AMC_ATTRIBUTES_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_VELOCITY);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[CONNECTION]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * POINT_TOTAL * 4, connected_vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_CONNECTED, 4, GL_INT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_CONNECTED);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glGenTextures(2, pos_tbo);
	glBindTexture(GL_TEXTURE_BUFFER, pos_tbo[0]);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, vbo[POSITION_A]);
	glBindTexture(GL_TEXTURE_BUFFER, pos_tbo[1]);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, vbo[POSITION_B]);

	int lines = (POINT_X - 1) * POINT_Y + (POINT_Y - 1) * POINT_X;

	glGenBuffers(1, &m_index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, lines * 2 * sizeof(int), NULL, GL_STATIC_DRAW);

	int * e = (int *)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, lines * 2 * sizeof(int), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

	for (int j = 0; j < POINT_Y; j++)  
	{
		for (int i = 0; i < POINT_X - 1; i++)
		{
			*e++ = i + j * POINT_X;
			*e++ = 1 + i + j * POINT_X;
		}
	}

	for (int i = 0; i < POINT_X; i++)
	{
		for (int j = 0; j < POINT_Y - 1; j++)
		{
			*e++ = i + j * POINT_X;
			*e++ = POINT_X + i + j * POINT_X;
		}
	}

	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	return;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpSzCmdLine, int iCmdShow) {
  // function prototype
  int Initialize(void);
  void Display(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("Cloud");

  // code
  if (0 != fopen_s(&gpFile, "log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf(gpFile, "Log File Created Successfully\n");

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                        szAppName,
                        TEXT("Cloud"),
                        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
                        100,
                        100,
                        800,
                        600,
                        NULL,
                        NULL,
                        hInstance,
                        NULL);
  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  iRet = Initialize();
  if (-1 == iRet) {
    fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-2 == iRet) {
    fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-3 == iRet) {
    fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-4 == iRet) {
    fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-5 == iRet) {
    fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
    DestroyWindow(hwnd);
  } else if (-6 == iRet) {
    DestroyWindow(hwnd);
  } else if (-7 == iRet) {
    DestroyWindow(hwnd);
  } else if (-8 == iRet) {
    DestroyWindow(hwnd);
  } else if (-9 == iRet) {
    DestroyWindow(hwnd);
  } else if (-10 == iRet) {
    DestroyWindow(hwnd);
  } else {
    fprintf(gpFile, "Initialize Successful.\n");
  }

  while (bDone == false)
  {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Display();
    }
  }

  return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);

  // code
  switch (iMsg) {

    case WM_ACTIVATE:
      if (HIWORD(wParam) == 0) { // if 0, the window is active
        gbActiveWindow = true;
      } else {
        gbActiveWindow = false;
      }
      break;

    case WM_DESTROY:
      Uninitialize();
      PostQuitMessage(0);
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;

    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;
      }
    } break;

  }

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen)
  {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle)
    {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
      {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.top,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  }
  else
  {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }
}


int IntermediateShader(void)
{
	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject) {
		fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"layout (location = 0) in vec4 position_mass;" \

		"layout (location = 1) in vec3 velocity;" \

		"layout (location = 2) in ivec4 connection;" \

		"layout (binding = 0) uniform samplerBuffer tex_position;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec4 tf_position_mass;" \
		"out vec3 tf_velocity;" \

		"uniform float t;" \
		"uniform float k = 7.1;" \
		"uniform float c = 2.8;" \
		"uniform float rest_length = 0.88;" \

		"const vec3 gravity = vec3(0.0, -0.08, 0.0);" \

		"void main(void)" \
		"{" \
		"vec3 p = position_mass.xyz; " \
		"float m = position_mass.w;" \
		"vec3 u = velocity;" \
		"vec3 F = gravity * m - c * u;" \
		"bool fixed_node = true;" \

		"for (int i = 0; i < 4; i++)" \
		"{" \
		"if (connection[i] != -1)" \
		"{" \
		"vec3 q = texelFetch(tex_position, connection[i]).xyz;" \
		"vec3 d = q - p;" \
		"float x = length(d);" \
		"F += -k * (rest_length - x) * normalize(d);" \
		"fixed_node = false;" \
		"}" \
		"}" \

		"if (fixed_node)" \
		"{" \
		"F = vec3(0.0);" \
		"}" \

		"vec3 a = F / m;" \

		"vec3 s = u * t + 0.5 * a * t * t;" \

		"vec3 v = u + a * t;" \

		"s = clamp(s, vec3(-25.0), vec3(25.0));" \

		"tf_position_mass = vec4(p + s, m);" \

		"tf_velocity = v;" \
		"}";

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);

	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "position_mass");
	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "velocity");
	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "connection");

	static const char * varyings[] = 
	{
		"tf_position_mass",
		"tf_velocity"
	};

	glTransformFeedbackVaryings(giShaderProgramObject, 2, varyings, GL_SEPARATE_ATTRIBS);

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(gpFile, "Shader Program Linked Successfully\n");

	timeUniform = glGetUniformLocation(giFinalShaderProgramObject, "t");

	return 0;
}

int FinalShader(void)
{
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		
		//"in vec4 vPosition;" \
		
		"layout (location = 0) in vec4 position_mass;" \

		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vec4(position_mass.xyz, 1.0);" \
		"}";

	giFinalVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giFinalVertexShaderObject)
	{
		return -8;
	}

	glShaderSource(giFinalVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giFinalVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giFinalVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(giFinalVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(giFinalVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFinalFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFinalFragmentShaderObject)
	{
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader (GL_FRAGMENT_SHADER) "
			"Failed:\n",
			__FILE__, __LINE__, __FUNCTION__);
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}"; 

	glShaderSource(giFinalFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(giFinalFragmentShaderObject);

	glGetShaderiv(giFinalFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giFinalFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFinalFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giFinalShaderProgramObject = glCreateProgram();

	glAttachShader(giFinalShaderProgramObject, giFinalVertexShaderObject);
	glAttachShader(giFinalShaderProgramObject, giFinalFragmentShaderObject);

	glBindAttribLocation(giFinalShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(giFinalShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giFinalShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		glGetShaderiv(giFinalShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFinalShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);
				free(szInfoLog);
				return -10;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	mvpUniform = glGetUniformLocation(giFinalShaderProgramObject, "u_mvp_matrix");

	return 0;
}

int Initialize(void)
{
  // variable declarations
  int index;
  PIXELFORMATDESCRIPTOR pfd;

  // function declarations
  void ReSize(int, int);
  void Uninitialize(void);
  void Init3DNoiseTexture(void);

  // code
  ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.cColorBits = 32;
  pfd.cRedBits = 8;
  pfd.cGreenBits = 8;
  pfd.cBlueBits = 8;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 8;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

  gHdc = GetDC(gHwnd);

  index = ChoosePixelFormat(gHdc, &pfd);
  if (0 == index) {
    return -1;
  }

  if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
    return -2;
  }

  gHglrc = wglCreateContext(gHdc);
  if (NULL == gHglrc) {
    return -3;
  }

  if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
    return -4;
  }

  GLenum result;
  result = glewInit();
  if (GLEW_OK != result) {
    return -5;
  }

  initialize_data();

 // IntermediateShader();

  FinalShader();

  Setup();

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  /*static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
  glClearBufferfv(GL_COLOR, 0, black);*/

  glClearDepth(1.0f);

  glEnable(GL_DEPTH_TEST);

  glDepthFunc(GL_LEQUAL);

  //glEnable(GL_TEXTURE_3D);

//  glLineWidth(1.0f);

  

  gPerspectiveProjectionMatrix = mat4::identity();

  ReSize(800, 600);

  return 0;
}

void ReSize(int width, int height)
{
  // code
  if (0 == height) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);

  gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

  return;
}

float time = 0.0f;
void Display(void)
{
	int iterationIndex = 0;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(giShaderProgramObject);

	glUniform1f(timeUniform, time);

	glEnable(GL_RASTERIZER_DISCARD);

	for (int i = 5; i != 0; i--)
	{
		glBindVertexArray(vao[iterationIndex & 1]);

		glBindTexture(GL_TEXTURE_BUFFER, pos_tbo[iterationIndex & 1]);
		iterationIndex++;

		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[POSITION_A + (iterationIndex & 1)]);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, vbo[VELOCITY_A + (iterationIndex & 1)]);

		glBeginTransformFeedback(GL_POINTS);
		glDrawArrays(GL_POINTS, 0, POINT_TOTAL);
		glEndTransformFeedback();

		glBindVertexArray(0);
	}

	glDisable(GL_RASTERIZER_DISCARD);

	glUseProgram(0);

	glUseProgram(giFinalShaderProgramObject);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glPointSize(4.0f);

	glBindVertexArray(vao[0]);
	glDrawArrays(GL_POINTS, 0, POINT_TOTAL);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
	glDrawElements(GL_LINES, CONNECTION_TOTAL * 4, GL_UNSIGNED_INT, NULL);

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(gHdc);

	time += 0.01f;
}

void Uninitialize(void)
{
  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
      glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                           pShaders);

      for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

    glDeleteProgram(giShaderProgramObject);
    giShaderProgramObject = 0;

    glUseProgram(0);
  }

  if (wglGetCurrentContext() == gHglrc)
  {
    wglMakeCurrent(NULL, NULL);
  }

  if (NULL != gHglrc)
  {
    wglDeleteContext(gHglrc);
  }

  if (NULL != gHdc)
  {
    ReleaseDC(gHwnd, gHdc);
  }

  fprintf(gpFile, "Log File Closed Successfully\n");

  if (NULL != gpFile)
  {
    fclose(gpFile);
  }

  return;
}

