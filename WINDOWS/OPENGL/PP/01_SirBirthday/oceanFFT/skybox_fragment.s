#version 430 core

/* In Variables */
in vec3 fs_v_texture_0_coordinate;

/* Unifrom for texture sampler */
uniform samplerCube u_texture_0_sampler;
uniform float u_skybox_alpha;

/* Out variables */
out vec4 FragColor;

void main()
{
    FragColor = texture(u_texture_0_sampler, fs_v_texture_0_coordinate) * u_skybox_alpha ;
}
