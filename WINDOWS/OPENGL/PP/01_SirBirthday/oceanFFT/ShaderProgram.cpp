#include <stdio.h>
#include <stdlib.h>
#include "ShaderProgram.h"
extern FILE* pep_gpFile;

int LinkProgram(GLuint shader_program)
{
	int result = 0;

	do
	{
		glLinkProgram(shader_program);

		GLint iShaderProgramLinkStatus = 0;
		glGetProgramiv(shader_program, GL_LINK_STATUS, &iShaderProgramLinkStatus);

		if (iShaderProgramLinkStatus == GL_FALSE)
		{
			result = -1;

			GLint iInfoLogLength;
			glGetProgramiv(shader_program, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				char* szInfoLog = (char*)malloc(iInfoLogLength);

				if (szInfoLog)
				{
					GLsizei written;
					glGetProgramInfoLog(shader_program, iInfoLogLength, &written, szInfoLog);

					fprintf(pep_gpFile, "%s", "Error while linking program\n");
					fprintf(pep_gpFile, "%s", szInfoLog);
					free(szInfoLog);
				}
			}
		}
	} while (false);

	return result;

}
