#include <fstream>
#include <string>
#include "Shaders.h"

extern FILE* pep_gpFile;

GLuint CompileShader(const char* shader_file_name, GLuint shader_type)
{
	GLuint shader_object = -1;
	GLuint result = 0;

	do
	{
		if (!shader_file_name || shader_file_name[0] == '\0')
		{
			break;
		}

		std::ifstream file;
		file.open(shader_file_name);

		if (!file.good())
		{
			break;
		}

		std::string line;
		std::string fileContent = "";

		while (std::getline(file, line))
		{
			fileContent += line;
			fileContent += "\n";
		}

		const char* shaderSource = fileContent.c_str();
		shader_object = glCreateShader(shader_type);
		glShaderSource(shader_object, 1, (const GLchar**)&shaderSource, NULL);

		glCompileShader(shader_object);

		GLint infoLength = 0;
		GLint shaderCompiledStatus = 0;
		char* infoLog = NULL;

		glGetShaderiv(shader_object, GL_COMPILE_STATUS, &shaderCompiledStatus);

		if (shaderCompiledStatus == GL_FALSE)
		{
			result = -1;
			glGetShaderiv(shader_object, GL_INFO_LOG_LENGTH, &infoLength);

			if (infoLength > 0)
			{
				infoLog = (char*)malloc(infoLength);

				if (infoLog)
				{
					GLsizei written;
					glGetShaderInfoLog(shader_object, infoLength, &written, infoLog);

					fprintf(pep_gpFile, "ShaderName: %s\n", shader_file_name);
					fprintf(pep_gpFile, "infoLog: %s\n", infoLog);
				}
			}
		}

		if (infoLog)
		{
			free(infoLog);
		}

	} while (false);

	if (result == 0)
	{
		return shader_object;
	}
	else
	{
		return result;
	}
}