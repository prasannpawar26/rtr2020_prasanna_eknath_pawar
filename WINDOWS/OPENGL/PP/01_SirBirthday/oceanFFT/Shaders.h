#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
GLuint CompileShader(const char* shader_file_name, GLuint shader_type);

#endif