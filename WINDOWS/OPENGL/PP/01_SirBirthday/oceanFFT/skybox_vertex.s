#version 430 core

/* Vertex Attributes */
in vec3 v_position;

/* Uniform Variable For Matrices */
uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

/* Out variables */
out vec3 fs_v_texture_0_coordinate;

void main()
{
	fs_v_texture_0_coordinate = v_position;
	vec4 pos = u_projection_matrix * ((u_view_matrix)) * u_model_matrix * vec4(v_position, 1.0);
	gl_Position = pos.xyww;
}