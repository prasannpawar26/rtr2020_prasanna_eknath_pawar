#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

// includes
#include <helper_gl.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <cufft.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include <math_constants.h>
#include <rendercheck_gl.h>

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew64.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

enum {
	PEP_ATTRIBUTES_POSITION = 0,
	PEP_ATTRIBUTES_COLOR,
	PEP_ATTRIBUTES_NORMAL,
	PEP_ATTRIBUTES_TEXCOORD0,
	PEP_ATTRIBUTES_TEXCOORD1
};

#define PEP_MAX_EPSILON 0.10f
#define PEP_THRESHOLD   0.15f
#define PEP_REFRESH_DELAY     2 //ms
#define PEP_REFRESH_DELAY_ID     2001 //ms

const unsigned int pep_gMeshSize = 2048/*256*/;
const unsigned int pep_gSpectrumWidth = pep_gMeshSize + 4;
const unsigned int pep_gSpectrumHeight = pep_gMeshSize + 1;
const int pep_gFrameCompare = 4;
const char *pep_gsSDKsample = "CUDA FFT Ocean Simulation";

unsigned int pep_gWindowWidth = 1920, pep_gWindowHeight = 1080;

//
// OpenGL Context Related Variables
//
HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;

//
// FullScreen Related Variables
//
DWORD pep_gdwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool pep_gbActiveWindow = false;
bool pep_gbIsFullScreen = false;

vmath::mat4 pep_gProjectionMatrix;

// Shader Related Variables
GLuint pep_gOceanVertexShaderObject;
GLuint pep_gOceanFragmentShaderObject;
GLuint pep_gOceanShaderProgramObject;

// Uniforms
GLuint pep_gUniformModelMatrix;
GLuint pep_gUniformViewMatrix;
GLuint pep_gUniformProjectionMatrix;
GLuint pep_gUniformHeightScale;
GLuint pep_gUniformChopiness;
GLuint pep_gUniformSize;
GLuint pep_gUniformDeepColor;
GLuint pep_gUniformShallowColor;
GLuint pep_gUniformSkyColor;
GLuint pep_gUniformLightDir;

// OpenGL vertex buffers
GLuint pep_gVertexPositionBuffer;
GLuint pep_gVertexHeightBuffer;
GLuint pep_gVertexSlopeBuffer;

// Cuda Introp Related Variables - Handles OpenGL-CUDA Exchange
struct cudaGraphicsResource *pep_gCudaResourceVertexHeightBuffer;
struct cudaGraphicsResource *pep_gCudaResourceVertexSlopeBuffer;

GLuint pep_gIndexBuffer;

// Mouse controls
int pep_gMouseOldX;
int pep_gMouseOldY;
float pep_gRotateX = 20.0f;
float pep_gRotateY = 0.0f;
float pep_gTranslateX = 0.0f;
float pep_gTranslateY = 0.0f;
float pep_gTranslateZ = -2.0f;

bool pep_gbAnimate = true;
bool pep_gbDrawPoints = false;
bool pep_gbWireFrame = false;
bool pep_gbHasDouble = false;

// FFT data
cufftHandle pep_gFFTPlan;
float2 *pep_gHeightFieldDeltaAtTime0 = 0;   // heightfield at time 0
float2 *pep_gH0 = 0;
float2 *pep_gHeightFieldDeltaAtTimeT = 0;   // heightfield at time t
float2 *pep_gDeltaSlope = 0;

// pointers to device object
float *pep_gHptr = NULL;
float2 *pep_gSptr = NULL;

// simulation parameters
const float pep_gGravitationalConstant = 9.81f;
const float pep_gWaveScaleFactor = 1e-7f;
const float pep_gPatchSize = 100;
float pep_gWindSpeed = 100.0f;
float pep_gWindDirection = CUDART_PI_F/3.0f;
float pep_gDirectionDepend = 0.07f;

StopWatchInterface *pep_gTimer = NULL;
float pep_gAnimationTime = 0.0f;
float pep_gPrevTime = 0.0f;
float pep_gAnimationRate = -0.001f;

// Auto-Verification Code
const int pep_gFrameCheckNumber = 4;
int pep_gFpsCount = 0;        // FPS count for averaging
int pep_gFpsLimit = 1;        // FPS limit for sampling
unsigned int pep_gFrameCount = 0;
unsigned int pep_gTotalErrors = 0;

float Urand(void)
{
	//
	// Code
	//
	return rand() / (float)RAND_MAX;
}

//
// Generates Gaussian random number with mean 0 and standard deviation 1.
//
float Gauss(void)
{
	//
	// Function Declarations
	//
	float Urand(void);

	//
	// Variable Declarations
	//
	float u1 = Urand();
	float u2 = Urand();

	//
	// Code
	//
	if (u1 < 1e-6f)
	{
		u1 = 1e-6f;
	}

	return sqrtf(-2 * logf(u1)) * cosf(2*CUDART_PI_F * u2);
}

// Phillips spectrum
// (Kx, Ky) - normalized wave vector
// Vdir - wind angle in radians
// V - wind speed
// A - constant
float Phillips(float Kx, float Ky, float windDirection, float windSpeed, float waveScaleFactor, float directionDepend)
{
	//
	// Variable Declarations
	//
	float k_squared = Kx * Kx + Ky * Ky;

	//
	// Code
	//
	if (k_squared == 0.0f)
	{
		return 0.0f;
	}

	// largest possible wave from constant wind of velocity v
	float L = windSpeed * windSpeed / pep_gGravitationalConstant;

	float k_x = Kx / sqrtf(k_squared);
	float k_y = Ky / sqrtf(k_squared);
	float w_dot_k = k_x * cosf(windDirection) + k_y * sinf(windDirection);

	float phillips = waveScaleFactor * expf(-1.0f / (k_squared * L * L)) / (k_squared * k_squared) * w_dot_k * w_dot_k;

	// filter out waves moving opposite to wind
	if (w_dot_k < 0.0f)
	{
		phillips *= directionDepend;
	}

	// damp out waves with very small length w << l
	/*float w = L / 10000;
	phillips *= expf(-k_squared * w * w);*/

	return phillips;
}

//
// Generate base heightfield in frequency space
//
void Generate_h0(float2 *h0)
{
	//
	// Function Declarations
	//
	float Phillips(float, float, float, float, float, float);
	float Gauss(void);

	//
	// Code
	//
	for (unsigned int y = 0; y<=pep_gMeshSize; y++)
	{
		for (unsigned int x = 0; x<=pep_gMeshSize; x++)
		{
			float kx = (-(int)pep_gMeshSize / 2.0f + x) * (2.0f * CUDART_PI_F / pep_gPatchSize);
			float ky = (-(int)pep_gMeshSize / 2.0f + y) * (2.0f * CUDART_PI_F / pep_gPatchSize);

			float P = sqrtf(Phillips(kx, ky, pep_gWindDirection, pep_gWindSpeed, pep_gWaveScaleFactor, pep_gDirectionDepend));

			if (kx == 0.0f && ky == 0.0f)
			{
				P = 0.0f;
			}

			//float Er = urand()*2.0f-1.0f;
			//float Ei = urand()*2.0f-1.0f;
			float Er = Gauss();
			float Ei = Gauss();

			float h0_re = Er * P * CUDART_SQRT_HALF_F;
			float h0_im = Ei * P * CUDART_SQRT_HALF_F;

			int i = y* pep_gSpectrumWidth + x;
			h0[i].x = h0_re;
			h0[i].y = h0_im;
		}
	}

	return;
}

//Cuda kernels Wrappers Forward Declarations
extern "C" void CudaGenerateSpectrumKernel(float2 *, float2 *, unsigned int, unsigned int, unsigned int, float, float);
extern "C" void CudaUpdateHeightmapKernel(float *, float2 *, unsigned int, unsigned int,bool);
extern "C" void CudaCalculateSlopeKernel(float *, float2 *, unsigned int, unsigned int);

void RunCuda(void)
{
	//
	// Variable Declarations
	//
	size_t num_bytes;

	//
	// Code
	//

	// generate wave spectrum in frequency domain
	CudaGenerateSpectrumKernel(
		pep_gHeightFieldDeltaAtTime0,
		pep_gHeightFieldDeltaAtTimeT,
		pep_gSpectrumWidth,
		pep_gMeshSize,
		pep_gMeshSize,
		pep_gAnimationTime,
		pep_gPatchSize);

	// execute inverse FFT to convert to spatial domain
	cufftExecC2C(pep_gFFTPlan, pep_gHeightFieldDeltaAtTimeT, pep_gHeightFieldDeltaAtTimeT, CUFFT_INVERSE);

	// update heightmap values in vertex buffer
	cudaGraphicsMapResources(1, &pep_gCudaResourceVertexHeightBuffer, 0);
	cudaGraphicsResourceGetMappedPointer((void **)&pep_gHptr, &num_bytes, pep_gCudaResourceVertexHeightBuffer);

	// Execute Kernel
	CudaUpdateHeightmapKernel(pep_gHptr, pep_gHeightFieldDeltaAtTimeT, pep_gMeshSize, pep_gMeshSize, false);

	// calculate slope for shading
	cudaGraphicsMapResources(1, &pep_gCudaResourceVertexSlopeBuffer, 0);
	cudaGraphicsResourceGetMappedPointer((void **)&pep_gSptr, &num_bytes, pep_gCudaResourceVertexSlopeBuffer);

	// Execute Kernel
	CudaCalculateSlopeKernel(pep_gHptr, pep_gSptr, pep_gMeshSize, pep_gMeshSize);

	cudaGraphicsUnmapResources(1, &pep_gCudaResourceVertexHeightBuffer, 0);
	cudaGraphicsUnmapResources(1, &pep_gCudaResourceVertexSlopeBuffer, 0);

	return;
}

//void computeFPS()
//{
//    frameCount++;
//    fpsCount++;
//
//    if (fpsCount == fpsLimit) {
//        fpsCount = 0;
//    }
//}


float light_direction = 1.0f;
void Display()
{
	//
	// Function Declarations
	//
	void RunCuda(void);

	//
	// Code
	//

	// run CUDA kernel to generate vertex positions
	if (pep_gbAnimate)
	{
		RunCuda();
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	vmath::mat4 modelMatrix = mat4::identity();
	vmath::mat4 viewMatrix = mat4::identity();

	modelMatrix =
		modelMatrix *
		translate(pep_gTranslateX, pep_gTranslateY, pep_gTranslateZ) *
		rotate(pep_gRotateX, 1.0f, 0.0f, 0.0f) *
		rotate(pep_gRotateY, 0.0f, 1.0f, 0.0f);

	glUseProgram(pep_gOceanShaderProgramObject);

	glUniformMatrix4fv(pep_gUniformModelMatrix, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_gUniformViewMatrix, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_gUniformProjectionMatrix, 1, GL_FALSE, pep_gProjectionMatrix);
	glUniform1f(pep_gUniformHeightScale, 0.250f);
	glUniform1f(pep_gUniformChopiness, 1.0f);
	glUniform2f(pep_gUniformSize, (float)pep_gMeshSize, (float)pep_gMeshSize);
	glUniform4f(pep_gUniformDeepColor, 0.0f, 0.4f, 0.6f, 1.0f);
	glUniform4f(pep_gUniformShallowColor, 0.1f, 0.3f, 0.3f, 1.0f);
	glUniform4f(pep_gUniformSkyColor, 1.0f, 1.0f, 1.0f, 1.0f);
	glUniform3f(pep_gUniformLightDir, 0.0f, light_direction, 0.0f);

	if (pep_gbDrawPoints)
	{
		glDrawArrays(GL_POINTS, 0, pep_gMeshSize * pep_gMeshSize);
	}
	else
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_gIndexBuffer);

		glPolygonMode(GL_FRONT_AND_BACK, pep_gbWireFrame ? GL_LINE : GL_FILL);
		glDrawElements(GL_TRIANGLE_STRIP, ((pep_gMeshSize * 2) + 2) * (pep_gMeshSize - 1), GL_UNSIGNED_INT, 0);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	glUseProgram(0);

	SwapBuffers(pep_gHdc);

	if (light_direction > 0.0f)
	{
		light_direction -= 0.0005f;
	}

	//computeFPS();

	return;
}

void Cleanup(void)
{
	//
	// Code
	//

	sdkDeleteTimer(&pep_gTimer);
	checkCudaErrors(cudaGraphicsUnregisterResource(pep_gCudaResourceVertexHeightBuffer));
	checkCudaErrors(cudaGraphicsUnregisterResource(pep_gCudaResourceVertexSlopeBuffer));

	glDeleteBuffers(1, &pep_gVertexPositionBuffer);
	pep_gVertexPositionBuffer = 0;

	glDeleteBuffers(1, &pep_gVertexHeightBuffer);
	pep_gVertexHeightBuffer = 0;

	glDeleteBuffers(1, &pep_gVertexSlopeBuffer);
	pep_gVertexSlopeBuffer = 0;

	if (NULL != pep_gHeightFieldDeltaAtTime0)
	{
		cudaFree(pep_gHeightFieldDeltaAtTime0);
	}
	
	if (NULL != pep_gDeltaSlope)
	{
		cudaFree(pep_gDeltaSlope);
	}

	if (NULL != pep_gHeightFieldDeltaAtTimeT)
	{
		cudaFree(pep_gHeightFieldDeltaAtTimeT);
	}

	if (NULL != pep_gH0)
	{
		free(pep_gH0);
	}
	
	cufftDestroy(pep_gFFTPlan);

	return;
}

void Reshape(int width, int height)
{
	//
	// Code
	//
	glViewport(0, 0, width, height);

	pep_gProjectionMatrix = mat4::identity();

	pep_gProjectionMatrix =
		perspective(60.0f, (GLfloat)width / (GLfloat)height, 0.1f, 10.0f);

	pep_gWindowWidth = width;
	pep_gWindowHeight = height;

	return;
}

GLint InitializeShaderProgram(void)
{
	//
	// Code
	//
	pep_gOceanVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_gOceanVertexShaderObject)
	{
		return -1;
	}

	const GLchar *ocean_vertexShaderSourceCode =

		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in float vTexCoord0;" \
		"in vec2 vTexCoord1;" \

		"out vec3 eyeSpacePos;" \
		"out vec3 worldSpaceNormal;"\
		"out vec3 eyeSpaceNormal;" \

		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \

		"uniform float heightScale; "\
		"uniform float chopiness;" \
		"uniform vec2  size;"

		"void main()" \
		"{" \
			"float height     = vTexCoord0.x;" \
			"vec2  slope      = vTexCoord1.xy;" \

			"vec3 normal      = normalize(cross( vec3(0.0, slope.y*heightScale, 2.0 / size.x), vec3(2.0 / size.y, slope.x*heightScale, 0.0)));" \
			"worldSpaceNormal = normal;" \


			"vec4 pos         = vec4(vPosition.x, height * heightScale, vPosition.z, 1.0);" \
			"gl_Position      = u_projectionMatrix * u_viewMatrix * u_modelMatrix * pos;" \

			"eyeSpacePos      = (u_viewMatrix * u_modelMatrix * pos).xyz;" \
			"eyeSpaceNormal   = (transpose(inverse(mat3(u_viewMatrix * u_modelMatrix))) * normal).xyz;" \
		"}";

	glShaderSource(pep_gOceanVertexShaderObject, 1, (const GLchar **)&ocean_vertexShaderSourceCode, NULL);
	glCompileShader(pep_gOceanVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_gOceanVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {

		glGetShaderiv(pep_gOceanVertexShaderObject, GL_INFO_LOG_LENGTH,
			&iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_gOceanVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_gOceanFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_gOceanFragmentShaderObject) {
		return -1;
	}

	const GLchar *ocean_fragmentShaderSourceCode =

		"#version 450 core"
		"\n"

		"in vec3 eyeSpacePos;" \
		"in vec3 worldSpaceNormal;" \
		"in vec3 eyeSpaceNormal;" \

		"uniform vec4 deepColor;" \
		"uniform vec4 shallowColor;" \
		"uniform vec4 skyColor;" \
		"uniform vec3 lightDir;" \

		"out vec4 FragColor;" \

		"void main()" \
		"{" \
		"vec3 eyeVector              = normalize(eyeSpacePos);" \
		"vec3 eyeSpaceNormalVector   = normalize(eyeSpaceNormal);" \
		"vec3 worldSpaceNormalVector = normalize(worldSpaceNormal);" \
		"float facing    = max(0.0, dot(eyeSpaceNormalVector, -eyeVector));" \
		"float fresnel   = pow(1.0 - facing, 5.0);" \
		"float diffuse   = max(0.0, dot(worldSpaceNormalVector, lightDir));" \
		"vec4 waterColor = deepColor;" \
		"FragColor = waterColor*diffuse + skyColor*fresnel;" \
		"}";

	glShaderSource(
		pep_gOceanFragmentShaderObject,
		1,
		(const GLchar **)&ocean_fragmentShaderSourceCode,
		NULL);

	glCompileShader(pep_gOceanFragmentShaderObject);

	glGetShaderiv(
		pep_gOceanFragmentShaderObject,
		GL_COMPILE_STATUS,
		&iShaderCompileStatus);

	if (FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(
			pep_gOceanFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(
					pep_gOceanFragmentShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	pep_gOceanShaderProgramObject = glCreateProgram();

	glAttachShader(pep_gOceanShaderProgramObject, pep_gOceanVertexShaderObject);
	glAttachShader(pep_gOceanShaderProgramObject, pep_gOceanFragmentShaderObject);

	glBindAttribLocation(pep_gOceanShaderProgramObject, PEP_ATTRIBUTES_POSITION,
		"vPosition");
	glBindAttribLocation(pep_gOceanShaderProgramObject, PEP_ATTRIBUTES_TEXCOORD0,
		"vTexCoord0");
	glBindAttribLocation(pep_gOceanShaderProgramObject, PEP_ATTRIBUTES_TEXCOORD1,
		"vTexCoord1");

	glLinkProgram(pep_gOceanShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(
		pep_gOceanShaderProgramObject,
		GL_LINK_STATUS,
		&iProgramLinkStatus);

	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(
			pep_gOceanShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);

		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(
					pep_gOceanShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}

	pep_gUniformModelMatrix = glGetUniformLocation(pep_gOceanShaderProgramObject, "u_modelMatrix");
	pep_gUniformViewMatrix = glGetUniformLocation(pep_gOceanShaderProgramObject, "u_viewMatrix");
	pep_gUniformProjectionMatrix = glGetUniformLocation(pep_gOceanShaderProgramObject, "u_projectionMatrix");
	pep_gUniformHeightScale = glGetUniformLocation(pep_gOceanShaderProgramObject, "heightScale");
	pep_gUniformChopiness   = glGetUniformLocation(pep_gOceanShaderProgramObject, "chopiness");
	pep_gUniformSize        = glGetUniformLocation(pep_gOceanShaderProgramObject, "size");
	pep_gUniformDeepColor = glGetUniformLocation(pep_gOceanShaderProgramObject, "deepColor");
	pep_gUniformShallowColor = glGetUniformLocation(pep_gOceanShaderProgramObject, "shallowColor");
	pep_gUniformSkyColor = glGetUniformLocation(pep_gOceanShaderProgramObject, "skyColor");
	pep_gUniformLightDir = glGetUniformLocation(pep_gOceanShaderProgramObject, "lightDir");

	return 0;
}

int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpszCmdLine,
	int iCmdShow)
{
	//
	// Function Declarations
	//
	int Initialize(void);
	void Update(void);
	void Display(void);

	//
	// Variable Declarations
	//
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("CUDA FFT Ocean Simulation");

	//
	// Code
	//
	if (0 != fopen_s(&pep_gpFile, "Log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created/Opend Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName, 
		TEXT("CUDA FFT Ocean Simulation"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();

	if (0 != iRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Failed\n", __FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}

	SetTimer(pep_gHwnd, PEP_REFRESH_DELAY_ID, PEP_REFRESH_DELAY, NULL);

	while (false == bDone) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} 
		else
		{
			if (pep_gbActiveWindow)
			{
			}

			Display();
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	void ToggledFullScreen(void);
	void UnInitialize(void);
	void Reshape(int w, int h);

	//
	// Code
	//
	switch (iMsg)
	{
		case WM_MOUSEMOVE:
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);

			float dx, dy;
			dx = (float)(x - pep_gMouseOldX);
			dy = (float)(y - pep_gMouseOldY);

			switch (wParam)
			{
			case MK_LBUTTON:
				pep_gRotateX += dy * 0.2f;
				pep_gRotateY += dx * 0.2f;
				break;

			case MK_MBUTTON:
				pep_gTranslateX += dx * 0.01f;
				pep_gTranslateY -= dy * 0.01f;
				break;

			case MK_RBUTTON:
				pep_gTranslateZ += dy * 0.01f;
				break;
			}

			pep_gMouseOldX = x;
			pep_gMouseOldY = y;
		}
		break;

		case WM_LBUTTONDOWN:
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);

			int button = 0;
			switch (wParam)
			{
				case MK_LBUTTON:
					button = 0;
					break;

				case MK_MBUTTON:
					button = 1;
					break;

				case MK_RBUTTON:
					button = 2;
					break;
			}

			pep_gMouseOldX = x;
			pep_gMouseOldY = y;
		}
		break;

		case WM_LBUTTONUP:
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);

			pep_gMouseOldX = x;
			pep_gMouseOldY = y;
		}
		break;

		case WM_TIMER:
		{
			switch (wParam)
			{
				case PEP_REFRESH_DELAY_ID:
				{
					float time = sdkGetTimerValue(&pep_gTimer);

					if (pep_gbAnimate)
					{
						pep_gAnimationTime += (time - pep_gPrevTime) * pep_gAnimationRate;
					}

					pep_gPrevTime = time;

					SetTimer(pep_gHwnd, PEP_REFRESH_DELAY_ID, PEP_REFRESH_DELAY   , NULL);
				}
				break;
			}
		}
		break;

		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;

		case WM_ERASEBKGND:
			return 0;
			break;

		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;

		case WM_SIZE:
			Reshape(LOWORD(lParam), HIWORD(lParam));
			break;

		case WM_KILLFOCUS:
			pep_gbActiveWindow = false;
			break;

		case WM_SETFOCUS:
			pep_gbActiveWindow = true;
			break;
		case WM_KEYDOWN:
		{
			switch (wParam) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;

			case 'F':
			case 'f':
				ToggledFullScreen();
				break;

			case 'W':
			case 'w':
				if (pep_gbDrawPoints)
				{
					pep_gbDrawPoints = false;
				}
				else
				{
					pep_gbDrawPoints = true;
				}
				break;
			}
		}
			break;
	}  // End Of Switch Case

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize(void)
{
	//
	// Function Declarations
	//
	void RunCuda(void);
	void Reshape(int, int);
	void Generate_h0(float2 *);

	//
	// Variable Declarations
	//
	int index;
	PIXELFORMATDESCRIPTOR pfd;
	int iRet;
	cudaError_t cudaRet;
	cufftResult cufftRet;

	//
	// Code
	//
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	pep_gHdc = GetDC(pep_gHwnd);

	index = ChoosePixelFormat(pep_gHdc, &pfd);
	if (0 == index)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : ChoosePixelFormat Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd))
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : SetPixelFormat Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	pep_gHglrc = wglCreateContext(pep_gHdc);
	if (NULL == pep_gHglrc)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	GLenum result;

	result = glewInit();
	if (GLEW_OK != result)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : glewInit Successful\n", __FILE__, __LINE__, __FUNCTION__);

	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);

	iRet = InitializeShaderProgram();
	if (0 != iRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : InitializeShaderProgram Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}
	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program Initialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	// create FFT plan
	cufftRet = cufftPlan2d(&pep_gFFTPlan, pep_gMeshSize, pep_gMeshSize, CUFFT_C2C);
	if (CUFFT_SUCCESS != cufftRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cufftPlan2d Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	// allocate memory
	int spectrumSize = pep_gSpectrumWidth * pep_gSpectrumHeight * sizeof(float2);
	cudaRet = cudaMalloc((void **)&pep_gHeightFieldDeltaAtTime0, spectrumSize);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : InitializeShaderProgram Failed\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	pep_gH0 = (float2 *) malloc(spectrumSize);
	Generate_h0(pep_gH0);
	cudaRet = cudaMemcpy(pep_gHeightFieldDeltaAtTime0, pep_gH0, spectrumSize, cudaMemcpyHostToDevice);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMemcpy Failed For HeightFieldDeltaAtTime0\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	int outputSize =  pep_gMeshSize * pep_gMeshSize * sizeof(float2);
	cudaRet = cudaMalloc((void **)&pep_gHeightFieldDeltaAtTimeT, outputSize);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMalloc Failed For HeightFieldDeltaAtTimeT\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	cudaRet = cudaMalloc((void **)&pep_gDeltaSlope, outputSize);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaMalloc Failed For DeltaSlope\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	sdkCreateTimer(&pep_gTimer);
	sdkStartTimer(&pep_gTimer);
	pep_gPrevTime = sdkGetTimerValue(&pep_gTimer);

	//
	// create vertex buffers and register with CUDA
	//

	// Vertex-Height Buffer
	glGenBuffers(1, &pep_gVertexHeightBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pep_gVertexHeightBuffer);
	glBufferData(GL_ARRAY_BUFFER, pep_gMeshSize * pep_gMeshSize * sizeof(float), 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(PEP_ATTRIBUTES_TEXCOORD0, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	cudaRet = cudaGraphicsGLRegisterBuffer(
		&pep_gCudaResourceVertexHeightBuffer,
		pep_gVertexHeightBuffer,
		cudaGraphicsMapFlagsWriteDiscard);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsGLRegisterBuffer Failed For VertexHeightBuffer\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	// Vertex-Slope Buffer
	glGenBuffers(1, &pep_gVertexSlopeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pep_gVertexSlopeBuffer);
	glBufferData(GL_ARRAY_BUFFER, outputSize, 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(PEP_ATTRIBUTES_TEXCOORD1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_TEXCOORD1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	cudaRet = cudaGraphicsGLRegisterBuffer(
		&pep_gCudaResourceVertexSlopeBuffer,
		pep_gVertexSlopeBuffer,
		cudaGraphicsMapFlagsWriteDiscard);
	if (cudaSuccess != cudaRet)
	{
		fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : cudaGraphicsGLRegisterBuffer Failed For VertexSlopeBuffer\n", __FILE__, __LINE__, __FUNCTION__);
		return -1;
	}

	//
	// create fixed vertex buffer to store mesh vertices
	//
	glGenBuffers(1, &pep_gVertexPositionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pep_gVertexPositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, pep_gMeshSize * pep_gMeshSize * 4 * sizeof(float), 0, GL_STATIC_DRAW);

	float *vertexPositionBuffer = (float *) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	if (!vertexPositionBuffer)
	{
		return -1;
	}

	for (int y=0; y < pep_gMeshSize; y++)
	{
		for (int x=0; x < pep_gMeshSize; x++)
		{
			float u = x / (float)(pep_gMeshSize - 1);
			float v = y / (float)(pep_gMeshSize - 1);

			*vertexPositionBuffer++ = u * 2.0f - 1.0f;
			*vertexPositionBuffer++ = 0.0f;
			*vertexPositionBuffer++ = v * 2.0f - 1.0f;
			*vertexPositionBuffer++ = 1.0f;
		}
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glVertexAttribPointer(PEP_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(PEP_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//
	// create index buffer for rendering quad mesh
	//
	// create index buffer
	glGenBuffers(1, &pep_gIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_gIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ((pep_gMeshSize * 2) + 2) * (pep_gMeshSize - 1) * sizeof(GLuint), 0, GL_STATIC_DRAW);

	// fill with indices for rendering mesh as triangle strips
	GLuint *indicesBuffer = (GLuint *) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

	if (!indicesBuffer)
	{
		return -1;
	}

	for (int y=0; y < pep_gMeshSize - 1; y++)
	{
		for (int x=0; x < pep_gMeshSize; x++)
		{
			*indicesBuffer++ = y * pep_gMeshSize + x;
			*indicesBuffer++ = (y + 1) * pep_gMeshSize + x;
		}

		// start new strip with degenerate triangle
		*indicesBuffer++ = (y +1 )* pep_gMeshSize +(pep_gMeshSize - 1);
		*indicesBuffer++ = (y + 1)* pep_gMeshSize;
	}

	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	RunCuda();

	Reshape(pep_gWindowWidth, pep_gWindowHeight);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	return 0;
}

void UnInitialize(void)
{
	// 
	// Function Declarations
	//
	void Cleanup(void);

	//
	// Code
	//
	Cleanup();

	if (wglGetCurrentContext() == pep_gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != pep_gHglrc)
	{
		wglDeleteContext(pep_gHglrc);
	}

	if (NULL != pep_gHdc)
	{
		ReleaseDC(pep_gHwnd, pep_gHdc);
	}

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully\n", __FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}
	return;
}

void ToggledFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbIsFullScreen)
	{
		pep_gdwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & pep_gdwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gdwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		pep_gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gdwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		pep_gbIsFullScreen = false;
		ShowCursor(TRUE);
	}

	return;
}
