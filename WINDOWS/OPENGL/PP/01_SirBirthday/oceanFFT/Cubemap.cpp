#include <Windows.h>
#include <gl/glew.h>
#include <stdio.h>
#include "Shaders.h"
#include "ShaderProgram.h"
#include "SOIL.h"
#include "vmath.h"

extern FILE* pep_gpFile;
extern vmath::mat4 pep_gProjectionMatrix;

int LoadTextureCubeMap(GLuint* texture, char* image_resources[], int resource_count)
{
	int width = 0, height = 0, channels = 0;
	glGenTextures(1, texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

	for (int index = 0; index < resource_count; index++)
	{
		unsigned char* ht_map = SOIL_load_image(image_resources[index], &width, &height, &channels, SOIL_LOAD_AUTO);

		if (!ht_map)
		{
			return -1;
		}

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + index, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, ht_map);
		SOIL_free_image_data(ht_map);
	}

	return 0;
}

GLuint texture_image_object;
GLuint sky_box_program;
GLuint uniform_model_matrix;
GLuint uniform_view_matrix;
GLuint uniform_projection_matrix;
GLuint uniform_texture_0_sampler;
GLuint uniform_skybox_alpha;
GLuint vao;
GLuint vbo;


int InitializeAndCompileShaders()
{
	DWORD result = 0;

	enum {
		SHREEMIT_ATTRIBUTE_VERTEX = 0,
		SHREEMIT_ATTRIBUTE_COLOR,
		SHREEMIT_ATTRIBUTE_NORMAL,
		SHREEMIT_ATTRIBUTE_TEXTURE_0,
	};

	do
	{
		/* Shaders Compilation and Program Linking */
		GLuint vertex_shader = CompileShader("skybox_vertex.s", GL_VERTEX_SHADER);

		if (result != 0)
		{
			fprintf(pep_gpFile, "%s", "failed to compile skybox vertex shader");
			break;
		}

		GLuint fragment_shader = CompileShader("skybox_fragment.s", GL_FRAGMENT_SHADER);

		if (result != 0)
		{
			fprintf(pep_gpFile, "%s", "failed to compile skybox fragment shader");
			break;
		}

		/*Enable Some parameters */
		glEnable(GL_BLEND);

		/*Enable Texture */
		glEnable(GL_TEXTURE0);
		char image_right_face[] = { "..\\oceanFFT\\skybox\\right.jpg" };
		char image_left_face[] = { "..\\oceanFFT\\skybox\\left.jpg" };
		char image_top_face[] = { "..\\oceanFFT\\skybox\\top.jpg" };
		char image_bottom_face[] = { "..\\oceanFFT\\skybox\\bottom.jpg" };
		char image_front_face[] = { "..\\oceanFFT\\skybox\\front.jpg" };
		char image_back_face[] = { "..\\oceanFFT\\skybox\\back.jpg" };


		char* image_resources[] = { image_right_face,
									image_left_face,
									image_top_face,
									image_bottom_face,
									image_front_face,
									image_back_face,
		};

		int ret_val = LoadTextureCubeMap(&texture_image_object, image_resources, sizeof(image_resources) / sizeof(char*));

		if (ret_val != 0)
		{
			fprintf(pep_gpFile, "%s", "failed to load cube map texture");
			break;
		}


		/* Bind Vertex Attributes */
		sky_box_program = glCreateProgram();
		glAttachShader(sky_box_program, vertex_shader);
		glAttachShader(sky_box_program, fragment_shader);
		glBindAttribLocation(sky_box_program, SHREEMIT_ATTRIBUTE_VERTEX, "v_position");

		result = LinkProgram(sky_box_program);

		if (result != 0)
		{
			fprintf(pep_gpFile, "%s", "failed to link skybox program");
			break;
		}

		/* Get Address of all uniform variables */
		uniform_model_matrix = glGetUniformLocation(sky_box_program, "u_model_matrix");
		uniform_view_matrix = glGetUniformLocation(sky_box_program, "u_view_matrix");
		uniform_projection_matrix = glGetUniformLocation(sky_box_program, "u_projection_matrix");
		uniform_texture_0_sampler = glGetUniformLocation(sky_box_program, "u_texture_0_sampler");
		uniform_skybox_alpha = glGetUniformLocation(sky_box_program, "u_skybox_alpha");

		GLfloat size = 10;
		GLfloat size_y = 0.0f;
		float skyboxVertices[] = {
			-size,  size, -size,
			-size, -size, -size,
			 size, -size, -size,
			 size, -size, -size,
			 size,  size, -size,
			-size,  size, -size,

			-size, -size,  size,
			-size, -size, -size,
			-size,  size, -size,
			-size,  size, -size,
			-size,  size,  size,
			-size, -size,  size,

			 size, -size, -size,
			 size, -size,  size,
			 size,  size,  size,
			 size,  size,  size,
			 size,  size, -size,
			 size, -size, -size,

			-size, -size,  size,
			-size,  size,  size,
			 size,  size,  size,
			 size,  size,  size,
			 size, -size,  size,
			-size, -size,  size,

			-size,  size, -size,
			 size,  size, -size,
			 size,  size,  size,
			 size,  size,  size,
			-size,  size,  size,
			-size,  size, -size,

			-size, -size, -size,
			-size, -size,  size,
			 size, -size, -size,
			 size, -size, -size,
			-size, -size,  size,
			 size, -size,  size
		};


		/* Vertex Data */
		/* Normal Data */
		/* Text Coordinates Data */
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		/* Vertices VBO */
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(SHREEMIT_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(SHREEMIT_ATTRIBUTE_VERTEX);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	}while (false);

	return result;
}

GLfloat camera_z = 2.0f;
GLfloat camera_x = 0.0f;
GLfloat camera_rotation_angle = -15.0f;

void SkyBoxDisplay()
{
	static GLfloat skybox_alpha = 1.0f;
	glUseProgram(sky_box_program);

	vmath::mat4 modelMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	vmath::mat4 viewMatrix = vmath::mat4::identity();
	
	if (skybox_alpha < 0.0f)
	{
		if (camera_rotation_angle > 60.0f)
		{
			camera_z -= 0.011f;
		}
		
		if (camera_rotation_angle < 75.0f)
			camera_rotation_angle += 0.1f;

		viewMatrix = viewMatrix * vmath::translate(camera_x, 0.0f, camera_z) * vmath::rotate(camera_rotation_angle, 0.0f, 1.0f, 0.0f);
	}
	else
	{
		skybox_alpha -= 0.001f;
		camera_z += 0.011f;
		viewMatrix = viewMatrix * vmath::translate(camera_x, 0.0f, camera_z) * vmath::rotate(camera_rotation_angle, 0.0f, 1.0f, 0.0f);
	}
	/* Uniform matrices */
	glUniformMatrix4fv(uniform_projection_matrix, 1, GL_FALSE, pep_gProjectionMatrix);
	glUniformMatrix4fv(uniform_view_matrix, 1, GL_FALSE , viewMatrix);
	glUniformMatrix4fv(uniform_model_matrix, 1, GL_FALSE, modelMatrix);
	glUniform1f(uniform_skybox_alpha, skybox_alpha);

	/* Texture Binding */
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture_image_object);
	glUniform1i(uniform_texture_0_sampler, 0);

	/* Draw Your Geometries */
	glBindVertexArray(vao);




	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glUseProgram(0);

	
}
