#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "common.h"

#include "vmath.h"

#include <ft2build.h>
#include FT_FREETYPE_H 

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
//#pragma comment(lib,"freetype.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "freetyped.lib")

#define NUM_CHARS 128

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow;
bool gbIsFullScreen;

//
// Shader Related Variables
//
GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

//
// UNIFORMS
//
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint texture0_sampler_uUniform;
GLuint textColorUniform;

//
// Matrices To Be Send To Shaders
//
mat4 modelMatrix;
mat4 viewMatrix;
mat4 perspectiveProjectionMatrix;

//
typedef struct _CHARACTER_ {
	GLuint TextureID;  // ID handle of the glyph texture
	vmath::uvec2 Size;         // Size of glyph
	vmath::uvec2 Bearing;      // Offset from baseline to left/top of glyph
	GLuint Advance;    // Horizontal offset to advance to next glyph
}CHARACTER;

CHARACTER font_character[NUM_CHARS];

GLuint VAO, VBO;

const GLchar *gc_PerFragment_vertexShaderSourceCode =
"#version 450 core" \
"\n" \

"in vec4 vPosition;" \
"out vec2 out_textureCoord;" \

"uniform mat4 model_matrix;" \
"uniform mat4 view_matrix;" \
"uniform mat4 projection_matrix;" \

"void main(void)" \
"{" \
    "gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vPosition.xy, 0.0, 1.0);" \
    "out_textureCoord = vPosition.zw;" \
 "}";

const GLchar *gc_PerFragment_fragmentShaderSourceCode =
"#version 450 core" \
"\n" \

"in vec2 out_textureCoord;" \
"out vec4 FragColor;" \

"uniform sampler2D texture0_sampler_u;" \
"uniform vec3 textColor_u;" \

"void main(void)" \
"{" \
    "vec4 sampled = vec4(1.0,1.0,1.0,texture(texture0_sampler_u, out_textureCoord).r);" \
    "FragColor = vec4(textColor_u,1.0)* sampled;" \
"}";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow) {
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);
	void ToggledFullScreen(void);
	void ReSize(int, int);

	// variable declarations
	bool bDone = false;
	MSG msg = { 0 };
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("FONT");

	// code
	if (0 != fopen_s(&gpFile, "log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("font"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, 800, 600, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-2 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-3 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
			"Failed.\nExitting Now... "
			"Successfully\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-4 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-5 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
			"\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-6 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-7 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-8 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-9 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-10 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	ToggledFullScreen();

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (WM_QUIT == msg.message) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			Update();
			Display();
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);
  int Initialize_Shaders(const GLchar *, const GLchar *);
  void Uninitialize_Shaders(void);

  // code
  switch (iMsg)
  {

    case WM_DESTROY: 
    {
      Uninitialize();
      PostQuitMessage(0);
    } break;


    case WM_ERASEBKGND:
    {
      return 0;
    } break;

    case WM_CLOSE:
    {
      DestroyWindow(hwnd);
    } break;

    case WM_KILLFOCUS:
    {
      gbActiveWindow = false;
    } break;

    case WM_SETFOCUS:
    {
      gbActiveWindow = true;
    } break;

    case WM_SIZE:
    {
      ReSize(LOWORD(lParam), HIWORD(lParam));
    } break;

    case WM_KEYDOWN: {

      switch (wParam)
      {
        case VK_ESCAPE:
        {
          DestroyWindow(hwnd);
        } break;

      } // Switch Case End

    } break; // WM_KEYDOWN
  } // Switch Case End

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen) {
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle) {
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void ReSize(int width, int height) {
	// code
	if (0 == height) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);

	return;
}

void Display(void) 
{
  /// code
  
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	char text[] = "PRASANNA PAWAR";
	GLfloat x = -2.0f;
	GLfloat y = 1.0f;
	GLfloat scale = 0.025f;

	glUseProgram(giShaderProgramObject);

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

  modelMatrix = modelMatrix * translate(0.0f, 0.0f, -16.0f);

  glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
  glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
  glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                     perspectiveProjectionMatrix);
  glUniform1i(texture0_sampler_uUniform, 0);
  glUniform3f(textColorUniform, 0.5f, 0.8f, 0.2f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glActiveTexture(GL_TEXTURE0);
  glBindVertexArray(VAO);

  for (unsigned int i = 0; i < strlen(text); i++)
  {
    char c = text[i];
    CHARACTER fc = font_character[c];

    GLfloat xpos = x + fc.Bearing[0] * scale;
    GLfloat ypos = y - (fc.Size[1] - fc.Bearing[1]) * scale;

    GLfloat w = fc.Size[0] * scale;
    GLfloat h = fc.Size[1] * scale;

    GLfloat vertices[6][4] =
    {
		  { xpos,     ypos + h,   0.0, 0.0 },
		  { xpos,     ypos,       0.0, 1.0 },
		  { xpos + w, ypos,       1.0, 1.0 },

		  { xpos,     ypos + h,   0.0, 0.0 },
		  { xpos + w, ypos,       1.0, 1.0 },
		  { xpos + w, ypos + h,   1.0, 0.0 }
	  };

    glBindTexture(GL_TEXTURE_2D, fc.TextureID);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices),vertices); 
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindTexture(GL_TEXTURE_2D, 0);

    x += (fc.Advance >> 6) * scale;
  }

  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);
  glDisable(GL_BLEND);
	glUseProgram(0);

	SwapBuffers(gHdc);
}

void Uninitialize_Shaders(void)
{
  if (giShaderProgramObject)
  {
    GLsizei shaderCount;
    GLsizei shaderNumber;

    glUseProgram(giShaderProgramObject);

    glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

    GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
    if (pShaders)
    {
			glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
      {
        glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
        glDeleteShader(pShaders[shaderNumber]);
        pShaders[shaderNumber] = 0;
      }

      free(pShaders);
    }

		glDeleteProgram(giShaderProgramObject);
		giShaderProgramObject = 0;

		glUseProgram(0);
	}

	return;

}
void Uninitialize(void) {
	// function declarations
	void Uninitialize_Shaders(void);
  void Unload_FreeTypeResources(void);

  Unload_FreeTypeResources();
	Uninitialize_Shaders();

	if (wglGetCurrentContext() == gHglrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc) {
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc) {
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != gpFile) {
		fclose(gpFile);
	}

	return;
}

int Initialize_Shaders(const GLchar *vertexShaderSourceCode,
	const GLchar *fragmentShaderSourceCode) {

	// code
	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -7;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFragmentShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(giFragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(giFragmentShaderObject);

	glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -9;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);
	glAttachShader(giShaderProgramObject, giFragmentShaderObject);

	// Attributes Binding Before Linking
	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -10;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	modelMatrixUniform =
		glGetUniformLocation(giShaderProgramObject, "model_matrix");
	viewMatrixUniform =
		glGetUniformLocation(giShaderProgramObject, "view_matrix");
	projectionMatrixUniform =
		glGetUniformLocation(giShaderProgramObject, "projection_matrix");

	texture0_sampler_uUniform =
		glGetUniformLocation(giShaderProgramObject, "texture0_sampler_u");
  textColorUniform =
		glGetUniformLocation(giShaderProgramObject, "textColor_u");

	return 0;
}

int Initialize(void) {

  // Function Declarations
  int Load_FreeType(void);

	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	void ReSize(int, int);
	void Uninitialize(void);
	int Initialize_Shaders(const GLchar *, const GLchar *);

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc) {
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result) {
		return -5;
	}

  // Initialization
  Load_FreeType();

  Initialize_Shaders(gc_PerFragment_vertexShaderSourceCode,
                     gc_PerFragment_fragmentShaderSourceCode);

  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);
  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  #ifdef DEBUG
  glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat) , NULL, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
#else

  // The 2D quad requires 6 vertices of 4 floats each so we reserve 6 * 4 floats
  // of memory. 
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE,
                        4 * sizeof(GLfloat), 0);
#endif
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	perspectiveProjectionMatrix = mat4::identity();

	ReSize(800, 600);

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}

void Update(void)
{
	return;
}

int Load_FreeType(void)
{
  // Code
  FT_Library fontLibrary;
  if (FT_Init_FreeType(&fontLibrary))
  {
    fprintf(gpFile, "ERROR::FREETYPE: Could not init FreeType Library\n",
            __FILE__, __LINE__, __FUNCTION__);
    return -1;
  }

	FT_Face face;
	if (FT_New_Face(fontLibrary, "../Fonts_Files/arialbd.ttf", 0, &face))
	{
		fprintf(gpFile, "ERROR::FREETYPE: Failed to load font\n",
			__FILE__, __LINE__, __FUNCTION__);
		FT_Done_FreeType(fontLibrary);
		return -1;
	}

	FT_Set_Pixel_Sizes(face, 0, 25);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1); 

  // load first 128 characters of ASCII set
  for (unsigned int c = 0; c < NUM_CHARS; c++)
  {
    // Load character glyph
    if (FT_Load_Char(face, c, FT_LOAD_RENDER))
    {
      fprintf(gpFile, "ERROR::FREETYTPE: Failed to load Glyph\n", __FILE__,
              __LINE__, __FUNCTION__);
      continue;
    }

    // Generate Texture
    GLuint char_texture;
    glGenTextures(1, &char_texture);

    glBindTexture(GL_TEXTURE_2D, char_texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width,
                 face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
                 face->glyph->bitmap.buffer);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		font_character[c].TextureID = char_texture;
		font_character[c].Size = vmath::uvec2(face->glyph->bitmap.width, face->glyph->bitmap.rows);
		font_character[c].Bearing =	vmath::uvec2(face->glyph->bitmap_left, face->glyph->bitmap_top);
		font_character[c].Advance = face->glyph->advance.x;

  } // End Of For Loop
  glBindTexture(GL_TEXTURE_2D, 0);
  FT_Done_Face(face);
  FT_Done_FreeType(fontLibrary);

  return 0;
}

void Unload_FreeTypeResources(void)
{
  // Code
  for (unsigned char c = 0; c < NUM_CHARS; c++)
  {
    glDeleteTextures(1, &font_character[(int)c].TextureID);
  }
}
