#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>

enum {
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0
};
