#pragma once
#include "GLUtil.h"
class RawModel
{
private:
	GLuint vaoID;
	GLuint positionVBOID;
	GLuint colorVBOID;
	GLuint normalVBOID;
	GLuint textureVBOID;
	GLuint indicesVBOID;
	unsigned int vertexCount;

public:
	//RawModel(GLuint vaoID,unsigned int vertexCount, GLuint vboIndicesID);
	//RawModel(GLuint vaoID, GLuint vboID);
	RawModel(GLuint vaoID, unsigned int vertexCount, GLuint vboPositionID, GLuint vboColorID, GLuint vboNormalID, GLuint vboTextureID , GLuint vboIndicesID);
	~RawModel();
	GLuint GetVAOID();
	GLuint GetFontVBOID();

	GLuint GetIndicesVBOID();
	GLuint GetColorVBOID();
	GLuint GetNormalVBOID();
	GLuint GetTextureVBOID();
	GLuint GetPositionVBOID();
	
	unsigned int GetVertexCount();
};

