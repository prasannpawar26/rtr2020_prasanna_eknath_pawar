#include "RawModel.h"

RawModel::RawModel(GLuint vaoID, unsigned int vertexCount, GLuint vboPositionID, GLuint vboColorID, GLuint vboNormalID, GLuint vboTextureID, GLuint vboIndicesID)
{
	this->vaoID = vaoID;
	this->vertexCount = vertexCount;
	this->positionVBOID = vboPositionID;
	this->colorVBOID = vboColorID;
	this->normalVBOID = vboNormalID;
	this->textureVBOID = vboTextureID;
	this->indicesVBOID = vboIndicesID;
}

RawModel::~RawModel()
{
}

GLuint RawModel::GetVAOID() {
	return vaoID;
}

GLuint RawModel::GetFontVBOID() {
	return positionVBOID;
}

GLuint RawModel::GetPositionVBOID() {
	return positionVBOID;
}

GLuint RawModel::GetIndicesVBOID()
{
	return indicesVBOID;
}

GLuint RawModel::GetColorVBOID()
{
	return colorVBOID;
}
GLuint RawModel::GetNormalVBOID()
{
	return normalVBOID;
}

GLuint RawModel::GetTextureVBOID()
{
	return textureVBOID;
}
unsigned int RawModel::GetVertexCount() {
	return vertexCount;
}
