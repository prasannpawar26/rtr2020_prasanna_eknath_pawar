#include "DICOM_GL.h"


DICOM_GL::DICOM_GL()
{
}

DICOM_GL::~DICOM_GL()
{
}

std::vector<uint8_t> DICOM_GL::networkSerialize(const std::vector<uint16_t>& input) {
	std::vector<uint8_t> output;
	output.reserve(input.size() * sizeof(uint8_t)); // Pre-allocate for sake of
													 // performance
	for (auto snumber : input) {
		//output.push_back((snumber & 0xFF)); // Extract the LSB
		output.push_back((snumber & 0xFF00) >> 8); // Extract the MSB
	}
	return output;
}

void DICOM_GL::ParseDICOM_And_LoadGLTextures(GLuint* texture, std::string dicomFILEPath, std::vector<std::string> &dicomInfo)
{
	// variable declarations
	DicomImageViewer::DICOMParser* parser = new DicomImageViewer::DICOMParser();

	parser->setDicomFileName(dicomFILEPath);

	dicomInfo = *parser->dicomInfo;

	std::vector<std::uint8_t> PngBuffer = networkSerialize(*parser->pixels16);	

	char* rgbaBuffer = new char[parser->width * parser->height * 4];

	for (int i = 0; i < parser->width * parser->height; ++i) {

		rgbaBuffer[i * 4] = PngBuffer[i];
		rgbaBuffer[i * 4 + 1] = PngBuffer[i];
		rgbaBuffer[i * 4 + 2] = PngBuffer[i];
		rgbaBuffer[i * 4 + 3] = PngBuffer[i];
	}

	//stbi_write_png("Data/DICOM/output/dicomAsImage.png", parser->width, parser->height, 4, rgbaBuffer, 0);
	// code
	glGenTextures(1, texture);// 1 image

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);// set 1 rather than default 4, for better performance
	glBindTexture(GL_TEXTURE_2D, *texture);// bind texture
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_INTENSITY,
		parser->width,
		parser->height,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		//&PngBuffer[0]);
		(void*)rgbaBuffer);

	delete[] rgbaBuffer;
	// Create mipmaps for this texture for better image quality
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
}
