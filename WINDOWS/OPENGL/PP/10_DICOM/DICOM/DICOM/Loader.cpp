#include "Loader.h"

Loader::Loader()
{
	vaos = new std::vector<GLuint>();
	vbos = new std::vector<GLuint>();
	textures = new std::vector<GLuint>();
}

Loader::~Loader()
{
	delete vaos;
	delete vbos;
	delete textures;
}

RawModel * Loader::LoadToVAO(std::vector<glm::vec3> &position, std::vector<GLuint> &indices)
{
	GLint vaoID = CreateVAO();
	StoreDataIntoAttributeList(0, position);
	GLuint vboIndicesID= BindIndicesBuffer(indices);
	UnbindVAO();
	return new RawModel(vaoID, indices.size(),0,0,0,0,vboIndicesID);
}

RawModel * Loader::LoadToVAO(std::vector<glm::vec3> &positions,
	std::vector<glm::vec3> &colors, 
	std::vector<glm::vec3> &normals, 
	std::vector<glm::vec2> &textureCoords,
	std::vector<GLuint> &indices)
{
	GLint vaoID = CreateVAO();
	StoreDataIntoAttributeList(SAM_ATTRIBUTE_VERTEX, 3, positions);
	StoreDataIntoAttributeList(SAM_ATTRIBUTE_COLOR,3, colors);
	StoreDataIntoAttributeList(SAM_ATTRIBUTE_NORMAL, 3,normals);
	StoreDataIntoAttributeList(SAM_ATTRIBUTE_TEXTURE0, 2, textureCoords);
	GLuint vboIndicesID = BindIndicesBuffer(indices);
	UnbindVAO();
	return new RawModel(vaoID, indices.size(), 0, 0, 0, 0, vboIndicesID);
	
}

RawModel* Loader::LoadToVAO_VBO_DICOM()
{

	GLint vaoID = CreateVAO();
	GLint vboID = CreateVBO(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	//The 2D quad requires 6 vertices of 4 floats each so we reserve 6 * 4 floats of memory. 
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(SAM_ATTRIBUTE_VERTEX);
	glVertexAttribPointer(SAM_ATTRIBUTE_VERTEX, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	UnbindVAO();
	return new RawModel(vaoID, 0, vboID, 0, 0, 0, 0);
}

GLuint Loader::CreateVAO(void)
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos->push_back(vaoID);
	glBindVertexArray(vaoID);
	return vaoID;
}

GLuint Loader::CreateVBO(GLenum target)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos->push_back(vboID);
	glBindBuffer(target, vboID);
	return vboID;
}

void Loader::StoreDataIntoAttributeList(int attributeNumber, std::vector<glm::vec3> & data)
{
	CreateVBO(GL_ARRAY_BUFFER);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec3), &data[0], GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, 3 , GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(attributeNumber);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::StoreDataIntoAttributeList(int attributeNumber, GLint dimension, std::vector<glm::vec3> & data)
{
	CreateVBO(GL_ARRAY_BUFFER);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec3), &data[0], GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, dimension, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(attributeNumber);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::StoreDataIntoAttributeList(int attributeNumber, GLint dimension, std::vector<glm::vec2> & data)
{
	CreateVBO(GL_ARRAY_BUFFER);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec3), &data[0], GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, dimension, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(attributeNumber);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::UnbindVAO()
{
	glBindVertexArray(0);
}

GLuint Loader::BindIndicesBuffer(std::vector<GLuint>&indices)
{
	GLuint vboIndices = CreateVBO(GL_ELEMENT_ARRAY_BUFFER);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	return vboIndices;
}

void Loader::CleanUp()
{
	std::vector<GLuint>::iterator i;

	for (i = vaos->begin(); i != vaos->end(); i++)
	{
		glDeleteVertexArrays(1, (const GLuint*)&(*i));
	}
	vaos->clear();

	for (i = vbos->begin(); i != vbos->end(); i++)
	{
		glDeleteBuffers(1, (const GLuint*)&(*i));
	}
	vbos->clear();

	for (i = textures->begin(); i != textures->end(); i++)
	{
		glDeleteBuffers(1, (const GLuint*)&(*i));
	}
	textures->clear();
}
