#include <stdio.h>
#include <GL/glew.h> // for GLSL extensions IMPORTANT : THis LIne Should Be Before gl\gl.h
#include <GL/gl.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

//DICOM
#include "DICOMShader.h"
#include "DICOM_GL.h"
#include "DICOMParser.h"

#include "Loader.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXCOORD0
};

GLuint vao_brick;
GLuint vbo_brick;
GLuint vbo_brick_text;

GLuint mvpUniform;
GLuint brickColorUniform;
GLuint mortarColorUniform;
GLuint brickSizeUniform;
GLuint brickPctUniform;

float vertices[12] = {
    1.0f, 1.0f, 0.0f,
    -1.0f, 1.0f, 0.0f,
    -1.0f, -1.0f, 0.0f,
    1.0f, -1.0f, 0.0f
};

mat4 gPerspectiveProjectionMatrix; // ->declared in vmath.h


DICOMShader* pDICOMShader;
DICOM_GL* pDICOM_GL;
std::vector<std::string> dicomInfo;
RawModel* rawModelDICOM;

Loader* loader;

glm::mat4	gDicomPerspectiveProjectionMatrix;
glm::mat4	gDicomOrthoProjectionMatrix;

//DICOM image and text
unsigned int DICOM_TextureColorbuffer;
GLuint DICOM_DefaultFrameBuffer;
GLuint DICOM_ColorRenderBuffer;
GLuint DICOM_DepthRenderBuffer;

bool bChangeBrightness = false;
bool bChangeContrast = false;
bool bChangeVibrance = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpSzCmdLine, int iCmdShow) {
    // function prototype
    int Initialize(void);
    void Display(void);

    // variable declarations
    bool bDone = false;
    MSG msg = {0};
    int iRet = 0;
    HWND hwnd;
    WNDCLASSEX wndClass;
    TCHAR szAppName[] = TEXT("DICOM");

    // code
    if (0 != fopen_s(&gpFile, "log.txt", "w")) {
        MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
        exit(0);
    }

    fprintf(gpFile, "Log File Created Successfully\n");

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.cbWndExtra = 0;
    wndClass.cbClsExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szAppName;
    wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wndClass.lpfnWndProc = WndProc;

    RegisterClassEx(&wndClass);

    hwnd = CreateWindowEx(WS_EX_APPWINDOW,
        szAppName,
        TEXT("DICOM"),
        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
        100,
        100,
        800,
        600,
        NULL,
        NULL,
        hInstance,
        NULL);
    gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    iRet = Initialize();
    if (-1 == iRet) {
        fprintf(gpFile, "Choose Pixel Format Failed.\nExitting Now...\n");
        DestroyWindow(hwnd);
    } else if (-2 == iRet) {
        fprintf(gpFile, "Set Pixel Format Failed.\nExitting Now...\n");
        DestroyWindow(hwnd);
    } else if (-3 == iRet) {
        fprintf(gpFile, "wglCreateContext Failed.\nExitting Now...\n");
        DestroyWindow(hwnd);
    } else if (-4 == iRet) {
        fprintf(gpFile, "wglMakeCurrent Failed.\nExitting Now...\n");
        DestroyWindow(hwnd);
    } else if (-5 == iRet) {
        fprintf(gpFile, "glewInit Failed.\nExitting Now...\n");
        DestroyWindow(hwnd);
    } else if (-6 == iRet) {
        DestroyWindow(hwnd);
    } else if (-7 == iRet) {
        DestroyWindow(hwnd);
    } else if (-8 == iRet) {
        DestroyWindow(hwnd);
    } else if (-9 == iRet) {
        DestroyWindow(hwnd);
    } else if (-10 == iRet) {
        DestroyWindow(hwnd);
    } else {
        fprintf(gpFile, "Initialize Successful.\n");
    }

    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (WM_QUIT == msg.message) {
                bDone = true;
            } else {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        } else {
            if (gbActiveWindow) {
            }

            Display();
        }
    }

    return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // function declarations
    void ToggledFullScreen(void);
    void Uninitialize(void);
    void ReSize(int, int);

    // code
    switch (iMsg) {

    case WM_ACTIVATE:
        if (HIWORD(wParam) == 0) { // if 0, the window is active
            gbActiveWindow = true;
        } else {
            gbActiveWindow = false;
        }
        break;

    case WM_DESTROY:
        Uninitialize();
        PostQuitMessage(0);
        break;

    case WM_ERASEBKGND:
        return 0;
        break;

    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;

    case WM_KILLFOCUS:
        gbActiveWindow = false;
        break;

    case WM_SETFOCUS:
        gbActiveWindow = true;
        break;

    case WM_SIZE:
        ReSize(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_KEYDOWN: {
        switch (wParam) {
        case VK_ESCAPE:
            DestroyWindow(hwnd);
            break;

        case 'F':
        case 'f':
            ToggledFullScreen();
            break;

        case 'B':
        case 'b':
        {
            if (bChangeBrightness)
            {
                bChangeBrightness = false;
            }
            else
            {
                bChangeBrightness = true;
            }
        }
            break;

        case 'C':
        case 'c':
        {
            if (bChangeContrast)
            {
                bChangeContrast = false;
            }
            else
            {
                bChangeContrast = true;
            }
        }
        break;

        case 'V':
        case 'v':
        {
            if (bChangeVibrance)
            {
                bChangeVibrance = false;
            }
            else
            {
                bChangeVibrance = true;
            }
        }
        break;

        case VK_UP:
        {
            if (bChangeBrightness)
            {
                pDICOM_GL->fChangeBrightness += 0.1f;
            }
        }
        break;

        case VK_DOWN:
        {
            if (bChangeBrightness)
            {
                pDICOM_GL->fChangeBrightness -= 0.1f;
            }
        }
        break;

        case VK_RIGHT:
        {
            if (bChangeContrast)
            {
                pDICOM_GL->fChangeContrast += 0.1f;
            }
        }
        break;

        case VK_LEFT:
        {
            if (bChangeContrast)
            {
                pDICOM_GL->fChangeContrast -= 0.1f;
            }
        }
        break;

        case VK_NUMPAD8:
        {
            if (bChangeVibrance)
            {
                pDICOM_GL->fChangeVibrance += 0.1f;
            }
        }
        break;

        case VK_NUMPAD5:
        {
            if (bChangeVibrance)
            {
                pDICOM_GL->fChangeVibrance -= 0.1f;
            }
        }
        break;

        }
    } break;

    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
    // variable declarations
    MONITORINFO mi;

    // code
    if (false == gbIsFullScreen)
    {
        dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & dwStyle)
        {
            mi = {sizeof(MONITORINFO)};

            if (GetWindowPlacement(gHwnd, &gWpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.top,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);
        gbIsFullScreen = true;
    }
    else
    {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(gHwnd, &gWpPrev);
        SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
            SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
            SWP_NOMOVE | SWP_NOSIZE);

        gbIsFullScreen = false;
        ShowCursor(TRUE);
    }
}


void initRenderToTextureBuffers(GLuint* defaultFrameBuffer, GLuint* textureColorbuffer, GLuint* colorRenderBuffer)
{
    glGenFramebuffers(1, defaultFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, *defaultFrameBuffer);
    glGenTextures(1, textureColorbuffer);
    glBindTexture(GL_TEXTURE_2D, *textureColorbuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, *textureColorbuffer, 0);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        //logger("ERROR::FRAMEBUFFER:: Framebuffer is not complete!");
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);


    glGenRenderbuffers(1, colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, *colorRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600); // use a single renderbuffer object for both a depth AND stencil buffer.
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, *colorRenderBuffer); // now actually attach it
                                                                                                                 // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        //logger("ERROR::FRAMEBUFFER:: Framebuffer is not complete!");
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

int Initialize(void)
{
    // variable declarations
    int index;
    PIXELFORMATDESCRIPTOR pfd;

    // function declarations
    void ReSize(int, int);
    void Uninitialize(void);

    // code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.cDepthBits = 8;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

    gHdc = GetDC(gHwnd);

    index = ChoosePixelFormat(gHdc, &pfd);
    if (0 == index) {
        return -1;
    }

    if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
        return -2;
    }

    gHglrc = wglCreateContext(gHdc);
    if (NULL == gHglrc) {
        return -3;
    }

    if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
        return -4;
    }

    GLenum result;
    result = glewInit();
    if (GLEW_OK != result) {
        return -5;
    }

    loader = new Loader();

    pDICOMShader = new DICOMShader("vertexShaderDICOM.vert", "fragmentShaderDICOM.frag");
    pDICOM_GL = new DICOM_GL();

    rawModelDICOM = loader->LoadToVAO_VBO_DICOM();
    pDICOM_GL->ParseDICOM_And_LoadGLTextures(&pDICOM_GL->gTexture_DICOM, "I19", dicomInfo);

    giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == giVertexShaderObject) {
        fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
        return -6;
    }

    //glShadeModel(GL_SMOOTH);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
//glDepthFunc(GL_LEQUAL);

    //Enable Texture
    glEnable(GL_TEXTURE_2D);
    //glEnable(GL_TEXTURE_3D);

    gDicomPerspectiveProjectionMatrix = glm::mat4(1.0f);
    gDicomOrthoProjectionMatrix = glm::mat4(1.0f);

    //DICOM fbo
    //initRenderToTextureBuffers(&DICOM_DefaultFrameBuffer, &DICOM_TextureColorbuffer, &DICOM_ColorRenderBuffer);

    const GLchar *vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;"
        "uniform mat4 u_mvp_matrix;" \
        "out vec2 out_texcoord;"
        "out vec2 mcPosition;" \

        "void main(void)" \
        "{" \
        "mcPosition = vPosition.xy;" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texcoord = vTexCoord;"
        "}";

    glShaderSource(giVertexShaderObject, 1,
        (const GLchar **)&vertexShaderSourceCode, NULL);

    glCompileShader(giVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
                    szInfoLog);
                fprintf(gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
                free(szInfoLog);
                return -7;
            }
        }
    }
    fprintf(gpFile, "Vertex Shader Compiled Successfully\n");

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == giFragmentShaderObject) 
    {
        fprintf(gpFile, "glCreateShader Failed.\nExitting Now...\n");
        return -8;
    }

    const GLchar *fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 mcPosition;" \

        "uniform vec3 u_brick_color;" \
        "uniform vec3 u_mortar_color;" \
        "uniform vec2 u_brick_size;" \
        "uniform vec2 u_brick_pct;" \

        "in vec2 out_texcoord;" \
        "uniform sampler2D u_sampler;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
        "vec3 color;" \
        "vec2 position;" \
        "vec2 use_brick;" \

        "position = mcPosition / u_brick_size;" \

        "if(fract(position.y * 0.5) > 0.5)" \
        "{"
        "position.x += 0.5;" \
        "}" \

        "position = fract(position);" \
        "use_brick = step(position, u_brick_pct);" \

        "color = mix(u_mortar_color, u_brick_color, use_brick.x * use_brick.y);" \
        "FragColor = texture(u_sampler, out_texcoord);" \
        //"FragColor = vec4(color, 1.0);" \

        "}";

    glShaderSource(giFragmentShaderObject, 1,
        (const GLchar **)&fragmentShaderSourceCode, NULL);

    glCompileShader(giFragmentShaderObject);

    glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
                free(szInfoLog);
                return -9;
            }
        }
    }
    fprintf(gpFile, "Fragment Shader Compiled Successfully\n");

    giShaderProgramObject = glCreateProgram();

    glAttachShader(giShaderProgramObject, giVertexShaderObject);
    glAttachShader(giShaderProgramObject, giFragmentShaderObject);

    glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
        "vPosition");
    glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0,
        "vTexCoord");

    glLinkProgram(giShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

                free(szInfoLog);
                return -10;
            }
        }
    }

    fprintf(gpFile, "Shader Program Linked Successfully\n");

    mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");

    glGenVertexArrays(1, &vao_brick);
    glBindVertexArray(vao_brick);

    glGenBuffers(1, &vbo_brick);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_brick);

    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 12, NULL, GL_DYNAMIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    const GLfloat cubeTexCoords[] = {
        0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f
    };

    glGenBuffers(1, &vbo_brick_text);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_brick_text);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoords), cubeTexCoords,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

   /* glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);

    glLineWidth(1.0f);*/

    gPerspectiveProjectionMatrix = mat4::identity();

    ReSize(800, 600);

    return 0;
}

void ReSize(int width, int height)
{
    // code
    if (0 == height) {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    gDicomPerspectiveProjectionMatrix = glm::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 1000.0f);

    return;
}

void RenderDICOM(void )
{
    glm::mat4 modelMatrix = glm::mat4(1.0f);
    glm::mat4 viewMatrix = glm::mat4(1.0f);
    glm::mat4 translateMatrix = glm::mat4(1.0f);
    glm::mat4 mvpMatrix = glm::mat4(1.0f);

    pDICOMShader->start();
    //DICOM render Start
    modelMatrix = glm::mat4(1.0f);
    viewMatrix = glm::mat4(1.0f);
    //viewMatrix = camera->getViewMatrix();
    //viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    translateMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -9.0));

    modelMatrix = modelMatrix * translateMatrix;

    mvpMatrix = gDicomPerspectiveProjectionMatrix * viewMatrix * modelMatrix;

    pDICOMShader->loadMatrix(pDICOMShader->location_mvpMatrix, mvpMatrix);

    //bind texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pDICOM_GL->gTexture_DICOM);
    pDICOMShader->loadInt(pDICOMShader->location_tex0Sampler, 0); //0th sampler
    pDICOMShader->loadFloat(glGetUniformLocation(pDICOMShader->getShaderProgramID(), "fBrigtness"), pDICOM_GL->fChangeBrightness);
    pDICOMShader->loadFloat(glGetUniformLocation(pDICOMShader->getShaderProgramID(), "fContrast"), pDICOM_GL->fChangeContrast);
    pDICOMShader->loadFloat(glGetUniformLocation(pDICOMShader->getShaderProgramID(), "fVibrance"), pDICOM_GL->fChangeVibrance);

    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.03f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //DICOM image resolution 512x512
    GLfloat xpos = 5.12f;
    //GLfloat ypos = 3.41 - 0.2125;
    GLfloat ypos = 5.12f;

    // Update VBO for each character
    GLfloat vertices[6][4] = {
        { -xpos, -ypos ,  0.0f, 1.0f },
        { -xpos, ypos ,   0.0f, 0.0f },
        { xpos, ypos ,   1.0f, 0.0f  },

        { -xpos, -ypos ,   0.0f, 1.0f },
        { xpos, ypos ,   1.0f, 0.0f },
        { xpos, -ypos ,   1.0f, 1.0f }
    };

    glBindVertexArray(rawModelDICOM->GetVAOID());

    // Update content of VBO memory
    glBindBuffer(GL_ARRAY_BUFFER, rawModelDICOM->GetPositionVBOID());
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData
                                                                     // Render quad
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);

    glBindTexture(GL_TEXTURE_2D, 0);
    //DICOM render end	
    pDICOMShader->stop();

}


void Display(void)
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //glClipControl(GL_UPPER_LEFT, GL_ZERO_TO_ONE);

    RenderDICOM();

    //glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);

    SwapBuffers(gHdc);
}

//void Display(void)
//{
//    // code
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//    glClipControl(GL_UPPER_LEFT, GL_ZERO_TO_ONE);
//    glBindFramebuffer(GL_FRAMEBUFFER, DICOM_DefaultFrameBuffer);
//    glDisable(GL_DEPTH_TEST);
//
//    glClearColor(0.0, 0.0, 0.0, 1.0);
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//    RenderDICOM();
//    //RenderDICOMInfo(dt);
//
//    glBindFramebuffer(GL_FRAMEBUFFER, 0);
//    glEnable(GL_DEPTH_TEST);
//    glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);
//
//    mat4 modelViewMatrix;
//    mat4 modelViewProjectionMatrix;
//
//    modelViewMatrix = mat4::identity();
//    modelViewProjectionMatrix = mat4::identity();
//    modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
//    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
//
//    glBindTexture(GL_TEXTURE_2D, pDICOM_GL->gTexture_DICOM);
//
//    glUseProgram(giShaderProgramObject);
//
//    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
//
//    glBindVertexArray(vao_brick);
//
//    glBindBuffer(GL_ARRAY_BUFFER, vbo_brick);
//
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
//
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//   // glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
//
//    glBindVertexArray(0);
//
//    glUseProgram(0);
//
//    glBindTexture(GL_TEXTURE_2D, 0);
//
//    SwapBuffers(gHdc);
//
//    return;
//}

void Uninitialize(void)
{
    // code
    if (vbo_brick)
    {
        glDeleteBuffers(1, &vbo_brick);
        vbo_brick = 0;
    }

    if (vao_brick)
    {
        glDeleteVertexArrays(1, &vao_brick);
        vao_brick = 0;
    }

    if (giShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(giShaderProgramObject);

        glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        glDeleteProgram(giShaderProgramObject);
        giShaderProgramObject = 0;

        glUseProgram(0);
    }

    if (wglGetCurrentContext() == gHglrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    if (NULL != gHglrc)
    {
        wglDeleteContext(gHglrc);
    }

    if (NULL != gHdc)
    {
        ReleaseDC(gHwnd, gHdc);
    }

    fprintf(gpFile, "Log File Closed Successfully\n");

    if (NULL != gpFile)
    {
        fclose(gpFile);
    }

    return;
}
