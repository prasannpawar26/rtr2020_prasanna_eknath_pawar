#pragma once
#include "ShaderProgram.h"

class DICOMShader : public ShaderProgram
{
public:
	DICOMShader(std::string vertexShader, std::string fragmentShader);
	void BindAttributes();
	void GetAllUniformLocations();

	~DICOMShader();

public:
	GLuint location_mvpMatrix;
	GLuint location_tex0Sampler;
	
	
};

