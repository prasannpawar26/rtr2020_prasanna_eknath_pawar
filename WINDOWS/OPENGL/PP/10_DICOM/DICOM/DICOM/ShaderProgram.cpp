#include "ShaderProgram.h"

ShaderProgram::ShaderProgram(std::string vertexShaderFile, std::string fragmentShaderFile)
{
	vertexFile = vertexShaderFile;
	fragmentFile = fragmentShaderFile;

	vertexShaderID = LoadAndCompileShader(vertexShaderFile,GL_VERTEX_SHADER);
	fragmentShaderID= LoadAndCompileShader(fragmentShaderFile, GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);

}

GLuint ShaderProgram::LoadAndCompileShader(std::string fileName, GLenum shaderType)
{
	std::string src = ReadShaderSource(fileName);

	const char* shaderSource = src.c_str();

	GLuint shaderID = glCreateShader(shaderType);

	glShaderSource(shaderID, 1, (const GLchar **)&shaderSource, NULL);

	glCompileShader(shaderID);

	GLint iInfoLogLenth = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE) {
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &iInfoLogLenth);
		if (iInfoLogLenth > 0) {
			szInfoLog = (char *)malloc(iInfoLogLenth);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(shaderID, iInfoLogLenth, &written, szInfoLog);
				//logger("Shader Compilation Log : File - %s :\n %s", fileName.c_str(), szInfoLog);
				free(szInfoLog);
				exit(0);
			}
		}
	}

	return shaderID;
}

void ShaderProgram::BindAttribute(GLuint attributeIndex, std::string uniformName)
{
	glBindAttribLocation(programID, attributeIndex, uniformName.c_str());
}

GLuint ShaderProgram::GetUniformLocation(std::string uniformName)
{
	return glGetUniformLocation(programID, uniformName.c_str());
}

void ShaderProgram::loadInt(GLuint location, GLint value)
{
	glUniform1i(location, value);
}

void ShaderProgram::loadFloat(GLuint location, GLfloat value)
{
	glUniform1f(location, value);
}

void ShaderProgram::loadVec3(GLuint location, glm::vec3 & vec)
{
	glUniform3f(location, vec.x, vec.y, vec.z);
}

void ShaderProgram::loadVec3(GLuint location, float x,float y, float z)
{
	glUniform3f(location, x,y, z);
}

void ShaderProgram::loadVec4(GLuint location, glm::vec4 & vec)
{
	glUniform4f(location, vec.x, vec.y, vec.z, vec.z);
}

void ShaderProgram::loadVec4(GLuint location, float x, float y, float z,float w)
{
	glUniform4f(location, x, y, z, w);
}

void ShaderProgram::loadVec2(GLuint location, glm::vec2 & vec)
{
	glUniform2f(location, vec.x, vec.y);
}

void ShaderProgram::loadVec2(GLuint location, float x, float y)
{
	glUniform2f(location, x, y);
}

void ShaderProgram::loadBoolean(GLuint location, bool value)
{
	GLfloat toLoad = value ? 1.0f : 0.0f;
	glUniform1f(location, toLoad);
}

void ShaderProgram::loadMatrix(GLuint location, glm::mat4 & matrix)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void ShaderProgram::LinkShaderProgram(void)
{
	glLinkProgram(programID);
	glValidateProgram(programID);

	GLint iShaderProgramLinkStatus = 0;
	//reinitialize 
	GLint iInfoLogLenth = 0;
	char* szInfoLog = NULL;
	glGetProgramiv(programID, GL_LINK_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE) {
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &iInfoLogLenth);
		if (iInfoLogLenth > 0) {
			szInfoLog = (char *)malloc(iInfoLogLenth);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(programID, iInfoLogLenth, &written, szInfoLog);
				//fprintf(gpFile, "Shader Object Link Log : %s\n", szInfoLog);
				//logger("Shader Object Link Log : ", szInfoLog);
				free(szInfoLog);
			}
		}
	}
}

void ShaderProgram::start()
{
	glUseProgram(programID);
}

void ShaderProgram::stop()
{
	glUseProgram(0);
}

std::string ShaderProgram::ReadShaderSource(std::string fileName)
{
	std::ifstream inputFile(fileName, std::ios::in);

	if (!inputFile)
	{
		//logger("Failed to read ",fileName.c_str()," Shader file");
	}

	std::string shaderSource, line;

	while (!inputFile.eof())
	{
		getline(inputFile,line);
		shaderSource += line + "\n";
	}

	return std::string(shaderSource);
}


ShaderProgram::~ShaderProgram()
{
	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);
	glDeleteProgram(vertexShaderID);
	vertexShaderID = 0;
	glDeleteProgram(fragmentShaderID);
	fragmentShaderID = 0;
	glDeleteProgram(programID);
	programID = 0;
}
