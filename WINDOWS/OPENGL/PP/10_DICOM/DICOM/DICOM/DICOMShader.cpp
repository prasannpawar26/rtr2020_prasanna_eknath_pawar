#include "DICOMShader.h"

DICOMShader::DICOMShader(std::string vertexShader, std::string fragmentShader) : ShaderProgram(vertexShader, fragmentShader)
{
	BindAttributes();
	LinkShaderProgram();
	GetAllUniformLocations();
}

void DICOMShader::BindAttributes()
{
	BindAttribute(SAM_ATTRIBUTE_VERTEX, "vPosition");
}

void DICOMShader::GetAllUniformLocations()
{
	location_mvpMatrix = GetUniformLocation("u_mvp_matrix");
	location_tex0Sampler = GetUniformLocation("u_texture0_sampler");
	
}

DICOMShader::~DICOMShader()
{

}
