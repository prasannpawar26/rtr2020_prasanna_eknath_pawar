#pragma once


//Win32 includes
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

//OpenGL includes
#include <GL\glew.h>
#include <GL\GL.h>
#include <GL\wglew.h>

//basic util includes
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>

#include <vector>
#include <map>

//glm includes
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp> 
#include <glm/gtc/type_ptr.hpp>
#include <glm\gtx\string_cast.hpp>

//link to lib
#pragma comment(lib,"opengl32.lib")
//#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"glew32.lib")

enum {
	SAM_ATTRIBUTE_VERTEX = 0,
	SAM_ATTRIBUTE_COLOR,
	SAM_ATTRIBUTE_NORMAL,
	SAM_ATTRIBUTE_TEXTURE0,
	SAM_BONE_ID_LOCATION,
	SAM_BONE_WEIGHT_LOCATION,
	SAM_ATTRIBUTE_VELOCITY,
	SAM_ATTRIBUTE_TIME
};

