#include "DICOMParser.h"


DicomImageViewer::DICOMParser::DICOMParser()
{
	dic = new DicomDictionary();
	file = new DICOMFile();
	signedImage = false;
	dicomInfo = new std::vector<std::string>();
	InitializeDicom();
}

DicomImageViewer::DICOMParser::~DICOMParser()
{
}

void DicomImageViewer::DICOMParser::InitializeDicom()
{
	bitsAllocated = 0;
	width = 1;
	height = 1;
	offset = 1;
	nImages = 1;
	samplesPerPixel = 1;
	photoInterpretation = "";
	unit = "mm";
	windowCentre = 0;
	windowWidth = 0;
	signedImage = false;
	widthTagFound = false;
	heightTagFound = false;
	pixelDataTagFound = false;
	rescaleIntercept = 0.0; // Default value
	rescaleSlope = 1.0; // Default value
	typeofDicomFile = TypeOfDicomFile::NotDicom;
}

void DicomImageViewer::DICOMParser::setDicomFileName(std::string fileName)
{
	bool val = file->Open(fileName);
	if (val) {
		dicomFileName = fileName;
	}
	else {
		exit(0);
	}
	InitializeDicom();
	location = 0; // Reset the location

	if (val) {
		dicomInfo->clear();
		try
		{
			bool readResult = ReadFileInfo();
			if (readResult && widthTagFound && heightTagFound && pixelDataTagFound)
			{
				ReadPixels();
				if (dicmFound == true)
					typeofDicomFile = TypeOfDicomFile::Dicom3File;
				else
					typeofDicomFile = TypeOfDicomFile::DicomOldTypeFile;
			}
		}
		catch (...)
		{
			// Nothing here
		}
	}
	file->Close();


}

bool DicomImageViewer::DICOMParser::ReadFileInfo()
{
	long skipCount = static_cast<__int32>(ID_OFFSET);
	bitsAllocated = 16;

	file->SkipToPos(skipCount);

	location += ID_OFFSET;

	if (GetString(4) != DICM)
	{
		// This is for reading older DICOM files (Before 3.0)
		// Seek from the beginning of the file
		file->SkipToStart();
		location = 0;

		// Older DICOM files do not have the preamble and prefix
		dicmFound = false;

		// Continue reading further.
		// See whether the width, height and pixel data tags
		// are present. If these tags are present, then it we conclude that this is a 
		// DICOM file, because all DICOM files should have at least these three tags.
		// Otherwise, it is not a DICOM file.
	}
	else
	{
		// We have a DICOM 3.0 file
		dicmFound = true;
	}

	bool decodingTags = true;
	samplesPerPixel = 1;
	int planarConfiguration = 0;
	photoInterpretation = "";
	std::string modality;

	while (decodingTags)
	{
		unsigned long long tag = GetNextTag();
		if ((location & 1) != 0)
			oddLocations = true;

		if (inSequence)
		{
			AddInfo(tag, "");
			continue;
		}

		std::string s;
		switch (tag)
		{
			case (unsigned long long)(TRANSFER_SYNTAX_UID) :
			{
				s = GetString(elementLength);
				AddInfo(tag, s);

				if (s.find("1.2.4") > -1 || s.find("1.2.5") > -1)
				{
					file->Close();
					typeofDicomFile = TypeOfDicomFile::DicomUnknownTransferSyntax;
					// Return gracefully indicating that this type of 
					// Transfer Syntax cannot be handled
					return false;
				}
				if (s.find("1.2.840.10008.1.2.2") >= 0)
					bigEndianTransferSyntax = true;
			}
			break;
			case (unsigned long long)MODALITY:
			{
				modality = GetString(elementLength);
				AddInfo(tag, modality);
			}
			break;
			case (unsigned long long)(NUMBER_OF_FRAMES) :
			{
				s = GetString(elementLength);
				AddInfo(tag, s);
				double frames = stod(s);
				if (frames > 1.0)
					nImages = (int)frames;
			}
			break;
			case (unsigned long long)(SAMPLES_PER_PIXEL) :
			{	samplesPerPixel = GetShort();
				AddInfo(tag, samplesPerPixel);
			}
			break;
			case (unsigned long long)(PHOTOMETRIC_INTERPRETATION) :
			{	photoInterpretation = GetString(elementLength);
				photoInterpretation = trim(photoInterpretation);
				AddInfo(tag, photoInterpretation);
			}
			break;
			case (unsigned long long)(PLANAR_CONFIGURATION) :
			{	planarConfiguration = GetShort();
				AddInfo(tag, planarConfiguration);
			}
			break;
			case (unsigned long long)(ROWS) :
			{	height = GetShort();
				AddInfo(tag, height);
				heightTagFound = true;
			}
			break;
			case (unsigned long long)(COLUMNS) :
			{	width = GetShort();
				AddInfo(tag, width);
				widthTagFound = true;
			}
			break;
			case (unsigned long long)(PIXEL_SPACING) :
			{	std::string scale = GetString(elementLength);
				GetSpatialScale(scale);
				AddInfo(tag, scale);
			}
			break;
			case (unsigned long long)(SLICE_THICKNESS) :
				case (int)(SLICE_SPACING) :
			{	std::string spacing = GetString(elementLength);
				pixelDepth = stod(spacing);
				AddInfo(tag, spacing);
			}
			break;
			case (unsigned long long)(BITS_ALLOCATED) :
			{
				bitsAllocated = GetShort();
				AddInfo(tag, bitsAllocated);
			}
			break;
			case (unsigned long long)(PIXEL_REPRESENTATION) :
			{	pixelRepresentation = GetShort();
				AddInfo(tag, pixelRepresentation);
			}
			break;
			case (unsigned long long)(WINDOW_CENTER) :
			{
				std::string center = GetString(elementLength);
				int index = center.find('\\');
				if (index != -1) center = center.substr(index + 1);
				windowCentre = stod(center);
				AddInfo(tag, center);
			}
			break;
			case (unsigned long long)(WINDOW_WIDTH) :
			{	std::string widthS = GetString(elementLength);
				int index = widthS.find('\\');
				if (index != -1) widthS = widthS.substr(index + 1);
				windowWidth = stod(widthS);
				AddInfo(tag, widthS);
			}
			break;
			case (unsigned long long)(RESCALE_INTERCEPT) :
			{std::string intercept = GetString(elementLength);
				rescaleIntercept = stod(intercept);
				AddInfo(tag, intercept);
			}
			break;
			case (unsigned long long)(RESCALE_SLOPE) :
			{std::string slop = GetString(elementLength);
				rescaleSlope = stod(slop);
				AddInfo(tag, slop);
			}
			break;
			case (unsigned long long)(RED_PALETTE) :
			{	reds = GetLut(elementLength);
				AddInfo(tag, elementLength / 2);
			}
			break;
			case (unsigned long long)(GREEN_PALETTE) :
			{	greens = GetLut(elementLength);
				AddInfo(tag, elementLength / 2);
			}
			break;
			case (unsigned long long)(BLUE_PALETTE) :
			{	blues = GetLut(elementLength);
				AddInfo(tag, elementLength / 2);
			}
			break;
			case (unsigned long long)(PIXEL_DATA) :
			{	// Start of image data...
				if (elementLength != 0)
				{
					offset = location;
					AddInfo(tag, location);
					decodingTags = false;
				}
				else
					AddInfo(tag, "");
				pixelDataTagFound = true;
			}
			break;
			default:
				AddInfo(tag, "");
				break;
		}
	}
	return true;
}

void DicomImageViewer::DICOMParser::ReadPixels()
{
	if (samplesPerPixel == 1 && bitsAllocated == 8)
	{
		if (pixels8 != NULL)
			pixels8->clear();

		pixels8 = new std::vector<std::uint8_t>();


		int numPixels = width * height;

		std::vector<uint8_t> buf(numPixels + 1);
		file->SkipToPos(offset);
		file->Read((char*)&buf[0], numPixels);


		for (int i = 0; i < numPixels; ++i)
		{
			int pixVal = (int)(buf[i] * rescaleSlope + rescaleIntercept);
			// We internally convert all 8-bit images to the range 0 - 255
			//if (photoInterpretation.Equals("MONOCHROME1", StringComparison.OrdinalIgnoreCase))
			//    pixVal = 65535 - pixVal;
			if (photoInterpretation == "MONOCHROME1")
				pixVal = max8 - pixVal;

			pixels8->push_back((std::uint8_t)(pixelRepresentation == 1 ? pixVal : (pixVal - min8)));
		}
	}

	if (samplesPerPixel == 1 && bitsAllocated == 16)
	{

		if (pixels16 != NULL)
			pixels16->clear();

		if (pixels16Int != NULL)
			pixels16Int->clear();

		pixels16 = new std::vector<std::uint16_t>();
		pixels16Int = new std::vector<std::int16_t>();


		int numPixels = width * height;
		int pixelArraySize = numPixels * 2;

		std::uint8_t* signedData = new std::uint8_t[2];

		std::vector<uint8_t> bufByte(pixelArraySize + 1);
		file->SkipToPos(offset);
		file->Read((char*)&bufByte[0], pixelArraySize);


		std::uint16_t unsignedS;

		int i, i1, pixVal;
		std::uint8_t b0, b1;

		for (i = 0; i < numPixels; ++i)
		{
			i1 = i * 2;
			b0 = bufByte[i1];
			b1 = bufByte[i1 + 1];

			unsignedS = static_cast<std::uint16_t>((b1 << 8) + b0);

			if (pixelRepresentation == 0) // Unsigned
			{
				pixVal = (int)(unsignedS * rescaleSlope + rescaleIntercept);
				if (photoInterpretation == "MONOCHROME1")
					pixVal = max16 - pixVal;
			}
			else  // Pixel representation is 1, indicating a 2s complement image
			{
				signedData[0] = b0;
				signedData[1] = b1;
				short sVal = static_cast<short>(signedData, 0);

				// Need to consider rescale slope and intercepts to compute the final pixel value
				pixVal = (int)(sVal * rescaleSlope + rescaleIntercept);
				if (photoInterpretation == "MONOCHROME1")
					pixVal = max16 - pixVal;
			}
			pixels16Int->push_back(pixVal);
		}

		int minPixVal = *std::min_element(pixels16Int->begin(), pixels16Int->end());

		signedImage = false;
		if (minPixVal < 0) signedImage = true;

		// Use the above pixel data to populate the list pixels16 
		for (int i = 0; i < pixels16Int->size(); i++)
		{
			std::int16_t pixel = pixels16Int->at(i);
			// We internally convert all 16-bit images to the range 0 - 65535
			if (signedImage)
				pixels16->push_back((std::uint16_t)(pixel - min16));
			else
				pixels16->push_back((std::uint16_t)(pixel));
		}

		pixels16Int->clear();
	}

	// 30 July 2010 - to account for Ultrasound images
	if (samplesPerPixel == 3 && bitsAllocated == 8)
	{
		signedImage = false;

		if (pixels24 != NULL)
			pixels24->clear();

		pixels24 = new std::vector<std::uint8_t>();



		int numPixels = width * height;
		int numBytes = numPixels * samplesPerPixel;

		std::vector<uint8_t> buf(numBytes + 1);
		file->SkipToPos(offset);
		file->Read((char*)&buf[0], numBytes);


		for (int i = 0; i < numBytes; ++i)
		{
			pixels24->push_back(buf[i]);
		}
	}
}


std::string DicomImageViewer::DICOMParser::GetString(int length)
{
	std::vector<unsigned char> buf(length + 1);
	file->SkipToPos(location);
	file->Read((char*)&buf[0], length);
	location += length;
	std::string s(buf.begin(), buf.end() - 1);
	return s;
}

quadbyte DicomImageViewer::DICOMParser::GetByte() // Changed return type to byte
{
	file->SkipToPos(location);
	quadbyte buf = file->ReadNBytes(1);
	++location;
	return buf;
}

unsigned short DicomImageViewer::DICOMParser::GetShort() // Changed return type to ushort
{
	quadbyte b00 = GetByte();
	quadbyte b11 = GetByte();

	std::uint8_t  b0 = static_cast<std::uint8_t>(b00);
	std::uint8_t  b1 = static_cast<std::uint8_t>(b11);

	unsigned short s;
	if (littleEndian)
		s = static_cast<std::uint16_t>((b1 << 8) + b0);
	else
		s = static_cast<std::uint16_t>((b0 << 8) + b1);
	return s;
}

int DicomImageViewer::DICOMParser::GetInt()
{
	quadbyte b00 = GetByte();
	quadbyte b11 = GetByte();
	quadbyte b22 = GetByte();
	quadbyte b33 = GetByte();

	std::uint32_t  b0 = static_cast<std::uint32_t>(b00);
	std::uint32_t  b1 = static_cast<std::uint32_t>(b11);
	std::uint32_t  b2 = static_cast<std::uint32_t>(b22);
	std::uint32_t  b3 = static_cast<std::uint32_t>(b33);

	int i;
	if (littleEndian)
		i = (b3 << 24) + (b2 << 16) + (b1 << 8) + b0;
	else
		i = (b0 << 24) + (b1 << 16) + (b2 << 8) + b3;
	return i;
}

double DicomImageViewer::DICOMParser::GetDouble()
{
	quadbyte b00 = GetByte();
	quadbyte b11 = GetByte();
	quadbyte b22 = GetByte();
	quadbyte b33 = GetByte();
	quadbyte b44 = GetByte();
	quadbyte b55 = GetByte();
	quadbyte b66 = GetByte();
	quadbyte b77 = GetByte();

	std::uint64_t  b0 = static_cast<std::uint64_t>(b00);
	std::uint64_t  b1 = static_cast<std::uint64_t>(b11);
	std::uint64_t  b2 = static_cast<std::uint64_t>(b22);
	std::uint64_t  b3 = static_cast<std::uint64_t>(b33);
	std::uint64_t  b4 = static_cast<std::uint64_t>(b44);
	std::uint64_t  b5 = static_cast<std::uint64_t>(b55);
	std::uint64_t  b6 = static_cast<std::uint64_t>(b66);
	std::uint64_t  b7 = static_cast<std::uint64_t>(b77);

	long res = 0;
	if (littleEndian)
	{
		res += b0;
		res += (((long)b1) << 8);
		res += (((long)b2) << 16);
		res += (((long)b3) << 24);
		res += (((long)b4) << 32);
		res += (((long)b5) << 40);
		res += (((long)b6) << 48);
		res += (((long)b7) << 56);
	}
	else
	{
		res += b7;
		res += (((long)b6) << 8);
		res += (((long)b5) << 16);
		res += (((long)b4) << 24);
		res += (((long)b3) << 32);
		res += (((long)b2) << 40);
		res += (((long)b1) << 48);
		res += (((long)b0) << 56);
	}

	double d = static_cast<double>(res);
	return d;
}

float DicomImageViewer::DICOMParser::GetFloat()
{
	quadbyte b00 = GetByte();
	quadbyte b11 = GetByte();
	quadbyte b22 = GetByte();
	quadbyte b33 = GetByte();

	std::uint32_t  b0 = static_cast<std::uint32_t>(b00);
	std::uint32_t  b1 = static_cast<std::uint32_t>(b11);
	std::uint32_t  b2 = static_cast<std::uint32_t>(b22);
	std::uint32_t  b3 = static_cast<std::uint32_t>(b33);

	int res = 0;

	if (littleEndian)
	{
		res += b0;
		res += (((int)b1) << 8);
		res += (((int)b2) << 16);
		res += (((int)b3) << 24);
	}
	else
	{
		res += b3;
		res += (((int)b2) << 8);
		res += (((int)b1) << 16);
		res += (((int)b0) << 24);
	}

	float f1;
	f1 = static_cast<float>(res);
	return f1;
}

unsigned char* DicomImageViewer::DICOMParser::GetLut(int length)
{
	if ((length & 1) != 0) // odd
	{
		std::string dummy = GetString(length);
		return NULL;
	}

	length /= 2;
	unsigned char* lut = new unsigned char[length];
	for (int i = 0; i < length; ++i)
		lut[i] = static_cast<unsigned char>(GetShort() >> 8);
	return lut;
}

int DicomImageViewer::DICOMParser::GetLength()
{
	quadbyte b00 = GetByte();
	quadbyte b11 = GetByte();
	quadbyte b22 = GetByte();
	quadbyte b33 = GetByte();

	if (b00 == -1 && b11 == -1 && b22 == -1 && b33 == -1)
	{
		return -1;
	}

	std::uint32_t  b0 = static_cast<std::uint32_t>(b00);
	std::uint32_t  b1 = static_cast<std::uint32_t>(b11);
	std::uint32_t  b2 = static_cast<std::uint32_t>(b22);
	std::uint32_t  b3 = static_cast<std::uint32_t>(b33);

	// Cannot know whether the VR is implicit or explicit without the 
	// complete Dicom Data Dictionary. 
	vr = (b0 << 8) + b1;

	switch (vr)
	{
	case OB:
	case OW:
	case SQ:
	case UN:
	case UT:
		// Explicit VR with 32-bit length if other two bytes are zero
		if ((b2 == 0) || (b3 == 0)) return GetInt();
		// Implicit VR with 32-bit length
		vr = IMPLICIT_VR;
		if (littleEndian)
			return ((b3 << 24) + (b2 << 16) + (b1 << 8) + b0);
		else
			return ((b0 << 24) + (b1 << 16) + (b2 << 8) + b3);
		// break; // Not necessary
	case AE:
	case AS:
	case AT:
	case CS:
	case DA:
	case DS:
	case DT:
	case FD:
	case FL:
	case IS:
	case LO:
	case LT:
	case PN:
	case SH:
	case SL:
	case SS:
	case ST:
	case TM:
	case UI:
	case UL:
	case US:
	case QQ:
	case RT:
		// Explicit vr with 16-bit length
		if (littleEndian)
			return ((b3 << 8) + b2);
		else
			return ((b2 << 8) + b3);
	default:
		// Implicit VR with 32-bit length...
		vr = IMPLICIT_VR;
		if (littleEndian)
			return ((b3 << 24) + (b2 << 16) + (b1 << 8) + b0);
		else
			return ((b0 << 24) + (b1 << 16) + (b2 << 8) + b3);
	}
}

unsigned long long DicomImageViewer::DICOMParser::GetNextTag()
{
	unsigned long long  groupWord = GetShort();
	if (groupWord == 0x0800 && bigEndianTransferSyntax)
	{
		littleEndian = false;
		groupWord = 0x0008;
	}
	unsigned long long elementWord = GetShort();
	unsigned long long tag = groupWord << 16 | elementWord;

	elementLength = GetLength();

	// Hack to read some GE files
	if (elementLength == 13 && !oddLocations)
		elementLength = 10;

	// "Undefined" element length.
	// This is a sort of bracket that encloses a sequence of elements.
	if (elementLength == -1)
	{
		elementLength = 0;
		inSequence = true;
	}
	return tag;
}

std::string DicomImageViewer::DICOMParser::GetHeaderInfo(int tag, std::string value)
{
	std::string str = int_to_hex(tag);

	if (str == ITEM_DELIMITATION || str == SEQUENCE_DELIMITATION)
	{
		inSequence = false;
		return "";
	}

	std::string id = "";

	if (dic->dict.count(str))
	{
		id = dic->dict[str];
		if (!id.empty())
		{
			if (vr == IMPLICIT_VR)
				vr = (id[0] << 8) + id[1];
			id = id.substr(2);
		}
	}

	if (str == ITEM)
		return (!id.empty() ? id : ":null");
	if (!value.empty())
		return id + ": " + value;

	switch (vr)
	{
	case FD:
	{
		for (int i = 0; i < elementLength; ++i)
			GetByte();
	}	break;
	case FL:
	{
		for (int i = 0; i < elementLength; i++)
			GetByte();
	}	break;
	case AE:
	case AS:
	case AT:
	case CS:
	case DA:
	case DS:
	case DT:
	case IS:
	case LO:
	case LT:
	case PN:
	case SH:
	case ST:
	case TM:
	case UI:
	{
		value = GetString(elementLength);
	}break;
	case US:
	{	if (elementLength == 2)
		value = std::to_string(GetShort());
	else
	{
		value = "";
		int n = elementLength / 2;
		for (int i = 0; i < n; i++)
			value += std::to_string(GetShort()) + " ";
	}
	}	break;
	case IMPLICIT_VR:
	{	value = GetString(elementLength);
	if (elementLength > 44)
		value = "null";
	}	break;
	case SQ:
	{	value = "";
	bool privateTag = ((tag >> 16) & 1) != 0;
	if (tag != ICON_IMAGE_SEQUENCE && !privateTag)
		break;
	location += elementLength;
	value = "";
	}	break;
	default:
	{	location += elementLength;
	value = "";
	}	break;
	}

	if (!value.empty() && id.empty() && value != "")
		return "---: " + value;
	else if (id.empty())
		return "";
	else
		return id + ": " + value;
}

void DicomImageViewer::DICOMParser::AddInfo(int tag, std::string value)
{
	std::string info = GetHeaderInfo(tag, value);

	std::string str = int_to_hex(tag);

	//std::string strPadded = str.insert(0,8, '0');

	std::string strInfo;
	if (inSequence && !info.empty() && vr != SQ)
		info = ">" + info;

	if (!info.empty() && str != ITEM)
	{
		if (info.find("---") != std::string::npos)
			//strInfo = replace(info, "---", "Private Tag");
			//strInfo = ">Private Tag: " + info.substr(info.find(":") + 1);
			strInfo = info;
		else
			strInfo = info;
		
		dicomInfo->push_back("G: " + str.substr(0,4)+ "     E: " + str.substr(4, 8)  + "         " + strInfo);
	}
}

void DicomImageViewer::DICOMParser::AddInfo(int tag, int value)
{
	AddInfo(tag, std::to_string(value));
}

void DicomImageViewer::DICOMParser::GetSpatialScale(std::string scale)
{
	double xscale = 0, yscale = 0;
	int i = scale.find('\\');
	if (i >= 1) // Aug 2012, Fixed an issue found while opening some images
	{
		yscale = stod(scale.substr(0, i));
		xscale = stod(scale.substr(i + 1));
	}
	if (xscale != 0.0 && yscale != 0.0)
	{
		pixelWidth = xscale;
		pixelHeight = yscale;
		unit = "mm";
	}
}
