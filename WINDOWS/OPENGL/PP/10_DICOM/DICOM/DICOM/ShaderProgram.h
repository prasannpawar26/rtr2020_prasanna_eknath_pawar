#pragma once

#include "GLUtil.h"

class ShaderProgram
{
private:
	std::string vertexFile;
	std::string fragmentFile;
	
	GLuint programID;
	GLuint vertexShaderID;
	GLuint fragmentShaderID;

	std::string ReadShaderSource(std::string fileName);
	GLuint LoadAndCompileShader(std::string fileName,GLenum shaderType);
public:
	//load compile attach
	ShaderProgram(std::string vertexShaderFile,std::string fragmentShaderFile);
	virtual ~ShaderProgram();
	GLuint getShaderProgramID()
	{
		return programID;
	}
	void LinkShaderProgram(void);
	void start();
	void stop();
	void BindAttribute(GLuint attributeIndex, std::string uniformName);
	GLuint GetUniformLocation(std::string uniformName);

	void loadInt(GLuint location, GLint value);
	void loadFloat(GLuint location, GLfloat value);
	void loadVec3(GLuint location, glm::vec3& vec);
	void loadVec3(GLuint location, float x, float y, float z);
	void loadVec4(GLuint location, glm::vec4& vec);
	void loadVec4(GLuint location, float x, float y, float z, float w);
	void loadVec2(GLuint location, glm::vec2& vec);
	void loadVec2(GLuint location, float x, float y);
	void loadBoolean(GLuint location, bool value);
	void loadMatrix(GLuint location, glm::mat4& matrix);

};

