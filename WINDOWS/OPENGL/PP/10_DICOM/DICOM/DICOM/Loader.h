#pragma once

#include "RawModel.h"
#include <vector>
using namespace std;

class Loader
{
public:
	Loader();
	virtual ~Loader();
	
	RawModel* LoadToVAO(std::vector<glm::vec3> &position, std::vector<GLuint> &indices);
	RawModel* LoadToVAO(std::vector<glm::vec3> &positions,
		std::vector<glm::vec3> &colors,
		std::vector<glm::vec3> &normals,
		std::vector<glm::vec2> &textureCoords,
		std::vector<GLuint> &indices);
	void CleanUp();

	RawModel* LoadToVAO_VBO_DICOM();

private:
	GLuint CreateVAO(void);
	GLuint CreateVBO(GLenum target);
	//void storeDataIntoAttributeList(int attributeNumber, std::vector<glm::vec3> &data);
	void StoreDataIntoAttributeList(int attributeNumber,std::vector<glm::vec3> &data);
	//for vertex, color, normals
	void StoreDataIntoAttributeList(int attributeNumber, GLint dimension, std::vector<glm::vec3> &data);
	//for texture coord
	void StoreDataIntoAttributeList(int attributeNumber, GLint dimension, std::vector<glm::vec2> & data);
	//unbind
	void UnbindVAO();
	//void bindIndicesBuffer(std::vector<GLint>&indices);
	GLuint BindIndicesBuffer(std::vector<GLuint> &indices);

	std::vector<GLuint> *vaos;
	std::vector<GLuint> *vbos;
	std::vector<GLuint> *textures;
};

