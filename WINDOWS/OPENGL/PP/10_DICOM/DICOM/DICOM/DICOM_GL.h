#pragma once
#include "GLUtil.h"

//#ifndef STBI_MSC_SECURE_CRT
//#define STBI_MSC_SECURE_CRT
//#endif // !STBI_MSC_SECURE_CRT
//
//#ifndef STB_IMAGE_IMPLEMENTATION
//#define STB_IMAGE_IMPLEMENTATION
//#endif // !STB_IMAGE_IMPLEMENTATION
//
//
//#include "stb_image.h"
//#ifndef STB_IMAGE_WRITE_IMPLEMENTATION
//#define STB_IMAGE_WRITE_IMPLEMENTATION
//#endif // !STB_IMAGE_WRITE_IMPLEMENTATION
//
//#include "stb_image_write.h"
#include "DICOMParser.h"

class DICOM_GL
{
public:

	GLuint gTexture_DICOM;
	float fChangeBrightness = 0.15f;
	float fChangeContrast = 12.2f;
	float fChangeVibrance = 1.3f;

	DICOM_GL();
	~DICOM_GL();
	std::vector<uint8_t> networkSerialize(const std::vector<uint16_t>& input);
	void ParseDICOM_And_LoadGLTextures(GLuint* texture, std::string dicomFILEPath, std::vector<std::string>&);

};

