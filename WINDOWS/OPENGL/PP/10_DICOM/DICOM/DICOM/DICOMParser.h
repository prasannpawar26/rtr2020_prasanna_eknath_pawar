#pragma once

#include "DICOMDictionary.h"
#include <vector>
#include <fstream>
#include <cstddef>
#include <sstream>
#include <iomanip>
#include <algorithm>    // std::min_element, std::max_element
#include "DICOMFile.h"

namespace DicomImageViewer
{
	enum class TypeOfDicomFile
	{
		NotDicom,
		Dicom3File,
		DicomOldTypeFile,
		DicomUnknownTransferSyntax
	};

	class DICOMParser
	{
	public:
		std::vector<std::uint16_t>* pixels16;
		std::vector<std::uint8_t>* pixels8;
		std::vector<std::string>* dicomInfo;
		int width;
		int height;
		int nImages;
		int samplesPerPixel;
		double pixelDepth = 1.0;
		double pixelWidth = 1.0;
		double pixelHeight = 1.0;

		DICOMParser(void);
		~DICOMParser();
		void InitializeDicom();
		void setDicomFileName(std::string fileName);
		bool ReadFileInfo();
		void ReadPixels();
		std::string GetString(int length);
		quadbyte GetByte();
		unsigned short GetShort();
		int GetInt();
		double GetDouble();
		float GetFloat();
		unsigned char* GetLut(int length);
		int GetLength();
		unsigned long long GetNextTag();
		std::string GetHeaderInfo(int tag, std::string value);
		void AddInfo(int tag, std::string value);
		void AddInfo(int tag, int value);
		void GetSpatialScale(std::string scale);
		/*void GetPixels8(std::vector<uint8_t>& pixels);
		void GetPixels16(std::vector<uint16_t>& pixels);
		void GetPixels24(std::vector<uint8_t>& pixels);*/

	private:
		static const unsigned int PIXEL_REPRESENTATION = 0x00280103;
		static const unsigned int TRANSFER_SYNTAX_UID = 0x00020010;
		static const unsigned int MODALITY = 0x00080060;
		static const unsigned int SLICE_THICKNESS = 0x00180050;
		static const unsigned int SLICE_SPACING = 0x00180088;
		static const unsigned int SAMPLES_PER_PIXEL = 0x00280002;
		static const unsigned int PHOTOMETRIC_INTERPRETATION = 0x00280004;
		static const unsigned int PLANAR_CONFIGURATION = 0x00280006;
		static const unsigned int NUMBER_OF_FRAMES = 0x00280008;
		static const unsigned int ROWS = 0x00280010;
		static const unsigned int COLUMNS = 0x00280011;
		static const unsigned int PIXEL_SPACING = 0x00280030;
		static const unsigned int BITS_ALLOCATED = 0x00280100;
		static const unsigned int WINDOW_CENTER = 0x00281050;
		static const unsigned int WINDOW_WIDTH = 0x00281051;
		static const unsigned int RESCALE_INTERCEPT = 0x00281052;
		static const unsigned int RESCALE_SLOPE = 0x00281053;
		static const unsigned int RED_PALETTE = 0x00281201;
		static const unsigned int GREEN_PALETTE = 0x00281202;
		static const unsigned int BLUE_PALETTE = 0x00281203;
		static const unsigned int ICON_IMAGE_SEQUENCE = 0x00880200;
		static const unsigned int PIXEL_DATA = 0x7FE00010;
		const std::string ITEM = "FFFEE000";
		const std::string ITEM_DELIMITATION = "FFFEE00D";
		const std::string SEQUENCE_DELIMITATION = "FFFEE0DD";
		static const int
			AE = 0x4145,
			AS = 0x4153,
			AT = 0x4154,
			CS = 0x4353,
			DA = 0x4441,
			DS = 0x4453,
			DT = 0x4454,
			FD = 0x4644,
			FL = 0x464C,
			IS = 0x4953,
			LO = 0x4C4F,
			LT = 0x4C54,
			PN = 0x504E,
			SH = 0x5348,
			SL = 0x534C,
			SS = 0x5353,
			ST = 0x5354,
			TM = 0x544D,
			UI = 0x5549,
			UL = 0x554C,
			US = 0x5553,
			UT = 0x5554,
			OB = 0x4F42,
			OW = 0x4F57,
			SQ = 0x5351,
			UN = 0x554E,
			QQ = 0x3F3F,
			RT = 0x5254;
		const int ID_OFFSET = 128;  //location of "DICM"
		static const int IMPLICIT_VR = 0x2D2D; // '--' 
		const std::string DICM = "DICM";
		int bitsAllocated;

		int offset;

		std::string unit;
		double windowCentre, windowWidth;
		bool signedImage;
		TypeOfDicomFile typeofDicomFile;

		bool dicmFound; // "DICM" found at offset 128
		DicomDictionary* dic;
		//std::ifstream file;
		DICOMFile* file;
		std::string dicomFileName;
		std::string photoInterpretation;
		bool littleEndian = true;
		bool oddLocations;  // one or more tags at odd locations
		bool bigEndianTransferSyntax = false;
		bool inSequence;
		bool widthTagFound;
		bool heightTagFound;
		bool pixelDataTagFound;
		int location = 0;
		unsigned long long elementLength;
		int vr;  // Value Representation
		int min8 = 0; //byte min
		int max8 = 255; //byte max
		int min16 = -32768; //short min
		int max16 = 65535; //ushort max
		int pixelRepresentation;
		double rescaleIntercept;
		double rescaleSlope;
		std::uint8_t* reds; //byte []
		std::uint8_t* greens;
		std::uint8_t* blues;
		std::uint8_t vrLetters[2];
		std::vector<std::uint8_t>* pixels24; // 8 bits bit depth, 3 samples per pixel
		std::vector<std::int16_t>* pixels16Int;

		template< typename T >
		std::string int_to_hex(T i)
		{
			// Ensure this function is called with a template parameter that makes sense. Note: static_assert is only available in C++11 and higher.
			static_assert(std::is_integral<T>::value, "Template argument 'T' must be a fundamental integer type (e.g. int, short, etc..).");

			std::stringstream stream;
			//stream << "0x" << std::setfill('0') << std::setw(sizeof(T) * 2) << std::hex;
			stream << std::setfill('0') << std::setw(sizeof(T) * 2) << std::hex;

			// If T is an 8-bit integer type (e.g. uint8_t or int8_t) it will be 
			// treated as an ASCII code, giving the wrong result. So we use C++17's
			// "if constexpr" to have the compiler decides at compile-time if it's 
			// converting an 8-bit int or not.
			if constexpr (std::is_same_v<std::uint8_t, T>)
			{
				// Unsigned 8-bit unsigned int type. Cast to int (thanks Lincoln) to 
				// avoid ASCII code interpretation of the int. The number of hex digits 
				// in the  returned string will still be two, which is correct for 8 bits, 
				// because of the 'sizeof(T)' above.
				stream << static_cast<int>(i);
			}
			else if (std::is_same_v<std::int8_t, T>)
			{
				// For 8-bit signed int, same as above, except we must first cast to unsigned 
				// int, because values above 127d (0x7f) in the int will cause further issues.
				// if we cast directly to int.
				stream << static_cast<int>(static_cast<uint8_t>(i));
			}
			else
			{
				// No cast needed for ints wider than 8 bits.
				stream << i;
			}

			return stream.str();
		}
		bool replace(std::string& str, const std::string& from, const std::string& to) {
			size_t start_pos = str.find(from);
			if (start_pos == std::string::npos)
				return false;
			str.replace(start_pos, from.length(), to);
			return true;
		}
		inline std::string trim(const std::string& s)
		{
			auto wsfront = std::find_if_not(s.begin(), s.end(), [](int c) {return std::isspace(c); });
			auto wsback = std::find_if_not(s.rbegin(), s.rend(), [](int c) {return std::isspace(c); }).base();
			return (wsback <= wsfront ? std::string() : std::string(wsfront, wsback));
		}
	};
};

