#include "Cloud.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

#define N 0x1000
#define NP 12   /* 2^N */
#define NM 0xfff

#define s_curve(t) ( t * t * (3.0 - 2. * t) )

#define lerp(t, a, b) ( a + t * (b - a) )

#define setup(i,b0,b1,r0,r1)\
        t = vec[i] + N;\
        b0 = ((int)t) & BM;\
        b1 = (b0+1) & BM;\
        r0 = t - (int)t;\
        r1 = r0 - 1.;

#define at2(rx,ry) ( rx * q[0] + ry * q[1] )

#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )

enum { MAXB = 0x100 };

int p[MAXB + MAXB + 2];
double g3[MAXB + MAXB + 2][3];
double g2[MAXB + MAXB + 2][2];
double g1[MAXB + MAXB + 2];

int start;
int B;
int BM;

GLuint pep_Cloud_Vao;
GLuint pep_Cloud_VboPosition;
GLuint pep_Cloud_VertexShaderObject;
GLuint pep_Cloud_FragmentShaderObject;
GLuint pep_Cloud_ShaderProgramObject;

GLuint pep_Cloud_MVPUniform;
GLuint pep_Cloud_ScaleUniform;
GLuint pep_Cloud_Sampler3DNoiseUniform;
GLuint pep_Cloud_SkyColorUniform;
GLuint pep_Cloud_CloudColorUniform;
GLuint pep_Cloud_OffsetUniform;
GLuint pep_Cloud_TilingUniform;
GLuint pep_Cloud_AlphaUniform;
GLuint pep_Cloud_BackUniform;

GLuint pep_Cloud_MathematicalTexture;
GLubyte * pep_Cloud_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_Cloud_CloudMotion = 0.0f;

void Cloud_Normalize2(double v[2])
{
	double s;

	s = sqrt(v[0] * v[0] + v[1] * v[1]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
}

void Cloud_Normalize3(double v[3])
{
	double s;

	s = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
	v[2] = v[2] / s;
}

void Cloud_InitNoise(void)
{
	int i, j, k;

	srand(30757);
	for (i = 0; i < B; i++)
	{
		p[i] = i;
		g1[i] = (double)((rand() % (B + B)) - B) / B;

		for (j = 0; j < 2; j++)
		{
			g2[i][j] = (double)((rand() % (B + B)) - B) / B;
		}

		Cloud_Normalize2(g2[i]);

		for (j = 0; j < 3; j++)
		{
			g3[i][j] = (double)((rand() % (B + B)) - B) / B;
		}

		Cloud_Normalize3(g3[i]);
	}

	while (--i)
	{
		k = p[i];
		p[i] = p[j = rand() % B];
		p[j] = k;
	}

	for (i = 0; i < B + 2; i++)
	{
		p[B + i] = p[i];
		g1[B + i] = g1[i];

		for (j = 0; j < 2; j++)
		{
			g2[B + i][j] = g2[i][j];
		}

		for (j = 0; j < 3; j++)
		{
			g3[B + i][j] = g3[i][j];
		}
	}
}

void Cloud_SetNoiseFrequency(int frequency)
{
	start = 1;
	B = frequency;
	BM = B - 1;
}

double Cloud_Noise3(double vec[3])
{
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
	double rx0, rx1, ry0, ry1, rz0, rz1, *q, sy, sz, a, b, c, d, t, u, v;
	int i, j;

	if (start) {
		start = 0;
		Cloud_InitNoise();
	}

	setup(0, bx0, bx1, rx0, rx1);
	setup(1, by0, by1, ry0, ry1);
	setup(2, bz0, bz1, rz0, rz1);

	i = p[bx0];
	j = p[bx1];

	b00 = p[i + by0];
	b10 = p[j + by0];
	b01 = p[i + by1];
	b11 = p[j + by1];

	t = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);

	q = g3[b00 + bz0]; u = at3(rx0, ry0, rz0);
	q = g3[b10 + bz0]; v = at3(rx1, ry0, rz0);
	a = lerp(t, u, v);

	q = g3[b01 + bz0]; u = at3(rx0, ry1, rz0);
	q = g3[b11 + bz0]; v = at3(rx1, ry1, rz0);
	b = lerp(t, u, v);

	c = lerp(sy, a, b);

	q = g3[b00 + bz1]; u = at3(rx0, ry0, rz1);
	q = g3[b10 + bz1]; v = at3(rx1, ry0, rz1);
	a = lerp(t, u, v);

	q = g3[b01 + bz1]; u = at3(rx0, ry1, rz1);
	q = g3[b11 + bz1]; v = at3(rx1, ry1, rz1);
	b = lerp(t, u, v);

	d = lerp(sy, a, b);

	//fprintf(stderr, "%f\n", lerp(sz, c, d));

	return lerp(sz, c, d);
}

void Cloud_Make3DNoiseTexture(void)
{
	int f, i, j, k, inc;
	int startFrequency = 4;
	int numOctaves = 4;
	double ni[3];
	double inci, incj, inck;
	int frequency = startFrequency;
	GLubyte *ptr;
	double amp = 0.5;

	if ((pep_Cloud_pNoise3DTexureData = (GLubyte *)malloc(CLOUD_IMAGE_WIDTH *
		CLOUD_IMAGE_HEIGHT *
		CLOUD_IMAGE_DEPTH * 4)) == NULL)
	{
		fprintf(pep_gpFile, "ERROR: Could not allocate 3D noise texture\n");
		exit(1);
	}

	for (f = 0, inc = 0; f < numOctaves;
		++f, frequency *= 2, ++inc, amp *= 0.5)
	{
		Cloud_SetNoiseFrequency(frequency);

		ptr = pep_Cloud_pNoise3DTexureData;

		ni[0] = ni[1] = ni[2] = 0;

		inci = 1.0 / (CLOUD_IMAGE_WIDTH / frequency);

		for (i = 0; i < CLOUD_IMAGE_WIDTH; ++i, ni[0] += inci)
		{
			incj = 1.0 / (CLOUD_IMAGE_HEIGHT / frequency);

			for (j = 0; j < CLOUD_IMAGE_HEIGHT; ++j, ni[1] += incj)
			{
				inck = 1.0 / (CLOUD_IMAGE_DEPTH / frequency);

				for (k = 0; k < CLOUD_IMAGE_DEPTH; ++k, ni[2] += inck, ptr += 4)
				{
					*(ptr + inc) = (GLubyte)(((Cloud_Noise3(ni) + 1.0) * amp)*100.0);
				}
			}
		}
	}
}

void Cloud_Init3DNoiseTexture(void)
{
	glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &pep_Cloud_MathematicalTexture);
	glBindTexture(GL_TEXTURE_3D, pep_Cloud_MathematicalTexture);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, CLOUD_IMAGE_WIDTH,
		CLOUD_IMAGE_HEIGHT, CLOUD_IMAGE_DEPTH, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, pep_Cloud_pNoise3DTexureData);

	return;
}

int Cloud_Initialize(void)
{
	pep_Cloud_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_Cloud_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_Cloud_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_Cloud_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Cloud_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_Cloud_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_Cloud_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_Cloud_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_Cloud_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec3 mcPosition;" \

		"uniform vec4 u_sky_color;" \
		"uniform vec4 u_cloud_color;" \
		"uniform sampler3D u_noise;" \
		"uniform vec3 u_offset;"\
		"uniform float u_tiling;"\
		"uniform float u_alpha;" \
		"uniform int u_back;" \

		"out vec4 FragColor;" \

		"float random(in vec2 st) { return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);}" \

		"float noise (in vec2 st) {" \
			"vec2 integer_component = floor(st);" \
			"vec2 fractional_component = fract(st);" \

			// Four corners in 2D of a tile
			"float corner_a = random(integer_component + vec2(0.0, 0.0));" \
			"float corner_b = random(integer_component + vec2(1.0, 0.0));" \
			"float corner_c = random(integer_component + vec2(0.0, 1.0));" \
			"float corner_d = random(integer_component + vec2(1.0, 1.0));" \

			//
			// Smooth Interpolation
			//

			// Cubic Hermine Curve.  Same as SmoothStep()
			"vec2 u = fractional_component * fractional_component * (3.0 - 2.0 * fractional_component); " \
			
			// Mix 4 coorners percentages
			"return mix(corner_a, corner_b, u.x) + (corner_c - corner_a)* u.y * (1.0 - u.x) + (corner_d - corner_b) * u.x * u.y;" \
		"}" \

		"void main(void)" \
		"{" \

			"if(1 == u_back) { FragColor = vec4(0.0, 0.0, 1.0, 1.0); return;}" \

			//"vec4 noise_vector = texture(u_noise, mcPosition + vec3( u_offset,0.0, u_offset));" \
			
			"vec4 noise_vector = texture(u_noise, u_tiling * mcPosition +  u_offset);" \
			"float intensity = (noise_vector[0] + noise_vector[1] + noise_vector[2] + noise_vector[3] + 0.03125) * 1.5;" \
			"vec4 color = mix(u_sky_color, u_cloud_color, intensity);" \
			"FragColor = vec4(color.rgb, u_alpha);" \

			"vec2 pos = vec2(u_tiling * mcPosition.st + u_offset.st);" \
			"float n = noise(pos);" \
			"FragColor = vec4(vec3(n), u_alpha);" \

		"}";

	glShaderSource(pep_Cloud_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_Cloud_FragmentShaderObject);
	glGetShaderiv(pep_Cloud_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_Cloud_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_Cloud_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_Cloud_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_Cloud_ShaderProgramObject, pep_Cloud_VertexShaderObject);
	glAttachShader(pep_Cloud_ShaderProgramObject, pep_Cloud_FragmentShaderObject);
	glBindAttribLocation(pep_Cloud_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_Cloud_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_Cloud_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_Cloud_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_Cloud_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_Cloud_MVPUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_mvp_matrix");
	pep_Cloud_SkyColorUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_sky_color");
	pep_Cloud_CloudColorUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_cloud_color");
	pep_Cloud_Sampler3DNoiseUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_noise");
	pep_Cloud_ScaleUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_scale");
	pep_Cloud_OffsetUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_offset");
	pep_Cloud_TilingUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_tiling");
	pep_Cloud_AlphaUniform = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_alpha");
	pep_Cloud_BackUniform   = glGetUniformLocation(pep_Cloud_ShaderProgramObject, "u_back");

	glGenVertexArrays(1, &pep_Cloud_Vao);
	glBindVertexArray(pep_Cloud_Vao);
	glGenBuffers(1, &pep_Cloud_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Cloud_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	Cloud_Make3DNoiseTexture();
	Cloud_Init3DNoiseTexture();

	return 0;
}

void Cloud_Resize(int, int)
{

}

void Cloud_Update(void)
{
	pep_Cloud_CloudMotion += 0.01f;
}

void Cloud_Uninitialize(void)
{
	if (pep_Cloud_VboPosition)
	{
		glDeleteBuffers(1, &pep_Cloud_VboPosition);
		pep_Cloud_VboPosition = 0;
	}

	if (pep_Cloud_Vao)
	{
		glDeleteVertexArrays(1, &pep_Cloud_Vao);
		pep_Cloud_Vao = 0;
	}

	if (pep_Cloud_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_Cloud_ShaderProgramObject);

		glGetProgramiv(pep_Cloud_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_Cloud_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_Cloud_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_Cloud_ShaderProgramObject);
		pep_Cloud_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void Cloud_Display(void)
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;
	glUseProgram(pep_Cloud_ShaderProgramObject);
	glUniformMatrix4fv(pep_Cloud_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(pep_Cloud_BackUniform, 1);
	glBindVertexArray(pep_Cloud_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	glUseProgram(0);
	//
	// Enable States
	//
	glEnable(GL_TEXTURE_3D);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, pep_Cloud_MathematicalTexture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_Cloud_ShaderProgramObject);
	glUniform1i(pep_Cloud_Sampler3DNoiseUniform, 0);
	glUniformMatrix4fv(pep_Cloud_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform4f(pep_Cloud_SkyColorUniform, 0.0, 0.0, 0.10, 0.0);
	glUniform4f(pep_Cloud_CloudColorUniform, 0.8, 0.8, 0.8, 1.0);
	glUniform1f(pep_Cloud_ScaleUniform, 1.0);
	//glUniform1f(pep_Cloud_OffsetUniform, pep_Cloud_CloudMotion);
	glUniform3f(pep_Cloud_OffsetUniform, pep_Cloud_CloudMotion, pep_Cloud_CloudMotion * 0.1f, pep_Cloud_CloudMotion);
	glUniform1f(pep_Cloud_TilingUniform, 0.50f);
	glUniform1f(pep_Cloud_AlphaUniform, 0.50f);
	glUniform1i(pep_Cloud_BackUniform, 0);

	glBindVertexArray(pep_Cloud_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

