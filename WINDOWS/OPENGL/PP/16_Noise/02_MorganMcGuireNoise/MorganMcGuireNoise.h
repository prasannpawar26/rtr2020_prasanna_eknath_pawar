#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int MorganMcGuireNoise_Initialize(void);
void MorganMcGuireNoise_Resize(int, int);
void MorganMcGuireNoise_Uninitialize(void);
void MorganMcGuireNoise_Update(void);
void MorganMcGuireNoise_Display(void);
