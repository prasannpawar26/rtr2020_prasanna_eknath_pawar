#include "DenseCloudNoise.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_DenseCloudNoise_Vao;
GLuint pep_DenseCloudNoise_VboPosition;
GLuint pep_DenseCloudNoise_VertexShaderObject;
GLuint pep_DenseCloudNoise_FragmentShaderObject;
GLuint pep_DenseCloudNoise_ShaderProgramObject;

GLuint pep_DenseCloudNoise_MVPUniform;
GLuint pep_DenseCloudNoise_BackUniform;
GLuint pep_DenseCloudNoise_ScaleUniform;
GLuint pep_DenseCloudNoise_OffsetUniform;
GLuint pep_DenseCloudNoise_TilingUniform;
GLuint pep_DenseCloudNoise_AlphaUniform;

GLubyte * pep_DenseCloudNoise_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_DenseCloudNoise_CloudMotion = 0.0f;

int DenseCloudNoise_Initialize(void)
{
	pep_DenseCloudNoise_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_DenseCloudNoise_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_DenseCloudNoise_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_DenseCloudNoise_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_DenseCloudNoise_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_DenseCloudNoise_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_DenseCloudNoise_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_DenseCloudNoise_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_DenseCloudNoise_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec3 mcPosition;" \

		"uniform vec3 u_offset;"\
		"uniform float u_tiling;"\
		"uniform float u_alpha;" \
		"uniform int u_back;" \
		"out vec4 FragColor;" \
		"int NUM_OCTAVES = 5;" \

		"float random (in vec2 _st) {"
			"return fract(sin(dot(_st.xy, vec2(12.9898,78.233))) * 43758.5453123);" \
		"};" \

		"float noise (in vec2 _st) {" \
			"vec2 i = floor(_st);" \
			"vec2 f = fract(_st);" \

			"float a = random(i + vec2(0.0, 0.0));" \
			"float b = random(i + vec2(1.0, 0.0));" \
			"float c = random(i + vec2(0.0, 1.0));" \
			"float d = random(i + vec2(1.0, 1.0));" \

			"vec2 u = f * f * (3.0 - 2.0 * f);" \

			" return mix(a, b, u.x) + (c - a)* u.y * (1.0 - u.x) + (d - b) * u.x * u.y;" \

		"}" \

		"float fbm ( in vec2 _st) {" \
			"float v = 0.0;" \
			"float a = 0.5;" \
			"vec2 shift = vec2(100.0);" \
			"mat2 rot = mat2(cos(0.5), sin(0.5), -sin(0.5), cos(0.50));" \

			"for (int i = 0; i < NUM_OCTAVES; ++i) {" \
				"v += a * noise(_st);" \
				"_st = rot * _st * 2.0 + shift;" \
				"a *= 0.5;" \
			"}" \

			"return v;" \
		"}" \

		"void main(void)" \
		"{" \

			"if(1 == u_back) { FragColor = vec4(0.0, 0.0, 1.0, 1.0); return;}" \
			//mcPosition => Behaving like texcoord here
			"vec2 st =  4.0 * mcPosition.xy + u_offset.xy;" \
			//"st += st * abs(sin(u_offset.x*0.1)*3.0);"\

			"vec3 color = vec3(0.0);"\

			"vec2 q = vec2(0.0);" \
			"q.x = fbm( st + 0.00*u_offset.x);" \
			"q.y = fbm( st + vec2(1.0));" \

			"vec2 r = vec2(0.);" \
			"r.x = fbm( st + 1.0*q + vec2(1.7,9.2)+ 0.15 * u_offset.x );" \
			"float f = fbm(st+r);" \

			"color = mix(vec3(0.101961,0.619608,0.666667), vec3(0.666667,0.666667,0.498039), clamp((f*f)*4.0,0.0,1.0));" \

			"color = mix(color, vec3(0.0,0.0,0.164706), clamp(length(q),0.0,1.0));" \

			"color = mix(color, vec3(0.666667,1.0,1.0), clamp(length(r.x),0.0,1.0));" \

			"FragColor = vec4((f*f*f+0.6*f*f+0.5*f)*color, u_alpha);" \
		"}";

	glShaderSource(pep_DenseCloudNoise_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_DenseCloudNoise_FragmentShaderObject);
	glGetShaderiv(pep_DenseCloudNoise_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_DenseCloudNoise_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_DenseCloudNoise_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_DenseCloudNoise_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_DenseCloudNoise_ShaderProgramObject, pep_DenseCloudNoise_VertexShaderObject);
	glAttachShader(pep_DenseCloudNoise_ShaderProgramObject, pep_DenseCloudNoise_FragmentShaderObject);
	glBindAttribLocation(pep_DenseCloudNoise_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_DenseCloudNoise_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_DenseCloudNoise_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_DenseCloudNoise_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_DenseCloudNoise_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_DenseCloudNoise_MVPUniform = glGetUniformLocation(pep_DenseCloudNoise_ShaderProgramObject, "u_mvp_matrix");
	pep_DenseCloudNoise_ScaleUniform = glGetUniformLocation(pep_DenseCloudNoise_ShaderProgramObject, "u_scale");
	pep_DenseCloudNoise_OffsetUniform = glGetUniformLocation(pep_DenseCloudNoise_ShaderProgramObject, "u_offset");
	pep_DenseCloudNoise_TilingUniform = glGetUniformLocation(pep_DenseCloudNoise_ShaderProgramObject, "u_tiling");
	pep_DenseCloudNoise_AlphaUniform = glGetUniformLocation(pep_DenseCloudNoise_ShaderProgramObject, "u_alpha");
	pep_DenseCloudNoise_BackUniform   = glGetUniformLocation(pep_DenseCloudNoise_ShaderProgramObject, "u_back");
	glGenVertexArrays(1, &pep_DenseCloudNoise_Vao);
	glBindVertexArray(pep_DenseCloudNoise_Vao);
	glGenBuffers(1, &pep_DenseCloudNoise_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_DenseCloudNoise_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void DenseCloudNoise_Resize(int, int)
{

}

void DenseCloudNoise_Update(void)
{
	pep_DenseCloudNoise_CloudMotion += 0.007f;
}

void DenseCloudNoise_Uninitialize(void)
{
	if (pep_DenseCloudNoise_VboPosition)
	{
		glDeleteBuffers(1, &pep_DenseCloudNoise_VboPosition);
		pep_DenseCloudNoise_VboPosition = 0;
	}

	if (pep_DenseCloudNoise_Vao)
	{
		glDeleteVertexArrays(1, &pep_DenseCloudNoise_Vao);
		pep_DenseCloudNoise_Vao = 0;
	}

	if (pep_DenseCloudNoise_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_DenseCloudNoise_ShaderProgramObject);

		glGetProgramiv(pep_DenseCloudNoise_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_DenseCloudNoise_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_DenseCloudNoise_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_DenseCloudNoise_ShaderProgramObject);
		pep_DenseCloudNoise_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void DenseCloudNoise_Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;
	glUseProgram(pep_DenseCloudNoise_ShaderProgramObject);
	glUniformMatrix4fv(pep_DenseCloudNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(pep_DenseCloudNoise_BackUniform, 1);
	glBindVertexArray(pep_DenseCloudNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Enable States
	//
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//mat4 modelViewMatrix;
	//mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_DenseCloudNoise_ShaderProgramObject);
	glUniformMatrix4fv(pep_DenseCloudNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1f(pep_DenseCloudNoise_ScaleUniform, 1.0);
	glUniform1i(pep_DenseCloudNoise_BackUniform, 0);
	//glUniform1f(pep_DenseCloudNoise_OffsetUniform, pep_DenseCloudNoise_CloudMotion);
	glUniform3f(pep_DenseCloudNoise_OffsetUniform, pep_DenseCloudNoise_CloudMotion, pep_DenseCloudNoise_CloudMotion * 0.1f, pep_DenseCloudNoise_CloudMotion);
	glUniform1f(pep_DenseCloudNoise_TilingUniform,20.0f);
	glUniform1f(pep_DenseCloudNoise_AlphaUniform, 0.50f);

	glBindVertexArray(pep_DenseCloudNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

