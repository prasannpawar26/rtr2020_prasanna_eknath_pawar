#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int VoroNoise_Initialize(void);
void VoroNoise_Resize(int, int);
void VoroNoise_Uninitialize(void);
void VoroNoise_Update(void);
void VoroNoise_Display(void);
