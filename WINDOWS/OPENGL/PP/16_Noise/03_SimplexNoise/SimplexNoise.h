#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int SimplexNoise_Initialize(void);
void SimplexNoise_Resize(int, int);
void SimplexNoise_Uninitialize(void);
void SimplexNoise_Update(void);
void SimplexNoise_Display(void);
