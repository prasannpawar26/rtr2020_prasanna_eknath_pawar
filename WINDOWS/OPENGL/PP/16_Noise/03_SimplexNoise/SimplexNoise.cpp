#include "SimplexNoise.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_SimplexNoise_Vao;
GLuint pep_SimplexNoise_VboPosition;
GLuint pep_SimplexNoise_VertexShaderObject;
GLuint pep_SimplexNoise_FragmentShaderObject;
GLuint pep_SimplexNoise_ShaderProgramObject;

GLuint pep_SimplexNoise_MVPUniform;
GLuint pep_SimplexNoise_ScaleUniform;
GLuint pep_SimplexNoise_Sampler3DNoiseUniform;
GLuint pep_SimplexNoise_SkyColorUniform;
GLuint pep_SimplexNoise_CloudColorUniform;
GLuint pep_SimplexNoise_OffsetUniform;
GLuint pep_SimplexNoise_TilingUniform;
GLuint pep_SimplexNoise_AlphaUniform;
GLuint pep_SimplexNoise_BackUniform;

GLuint pep_SimplexNoise_MathematicalTexture;
GLubyte * pep_SimplexNoise_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_SimplexNoise_CloudMotion = 0.0f;

int SimplexNoise_Initialize(void)
{
	pep_SimplexNoise_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_SimplexNoise_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_SimplexNoise_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_SimplexNoise_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_SimplexNoise_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_SimplexNoise_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_SimplexNoise_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_SimplexNoise_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_SimplexNoise_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	//
	//Simplex Noise
	//
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec3 mcPosition;" \

		"uniform vec3 u_offset;"\
		"uniform float u_tiling;"\
		"uniform float u_alpha;" \
		"uniform int u_back;" \

		"out vec4 FragColor;" \

		"vec2 SimplexNoise_Skew(vec2 st)"
		"{"
			"vec2 r = vec2(0.0);"
			"r.x = 1.1547*st.x;"
			"r.y = st.y+0.5*r.x;"
			"return r;"
		"}"

		"vec3 SimplexNoise_Grid(vec2 st) {"
			"vec3 xyz = vec3(0.0);"

			"vec2 p = fract(SimplexNoise_Skew(st));"
			"if (p.x > p.y) {"
			"xyz.xy = 1.0-vec2(p.x,p.y-p.x);"
			"xyz.z = p.y;"
			"} else {"
			"xyz.yz = 1.0-vec2(p.x-p.y,p.y);"
			"xyz.x = p.x;"
			"}"

			"return fract(xyz);"
		"}"

		"void main(void)" \
		"{" \
			"if(1 == u_back) { FragColor = vec4(0.0, 0.0, 1.0, 1.0); return;}" \

			"float scale = 10.0;"
			"vec2 st = mcPosition.xy * scale;"
			"vec3 color = vec3(0.0);"
			//"color.rg = fract(st);"
			//"color.rg = fract(SimplexNoise_Skew(st));"
			"color = SimplexNoise_Grid(st);"
			"FragColor = vec4(color, u_alpha);"
		"}";

	glShaderSource(pep_SimplexNoise_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_SimplexNoise_FragmentShaderObject);
	glGetShaderiv(pep_SimplexNoise_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_SimplexNoise_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_SimplexNoise_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_SimplexNoise_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_SimplexNoise_ShaderProgramObject, pep_SimplexNoise_VertexShaderObject);
	glAttachShader(pep_SimplexNoise_ShaderProgramObject, pep_SimplexNoise_FragmentShaderObject);
	glBindAttribLocation(pep_SimplexNoise_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_SimplexNoise_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_SimplexNoise_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_SimplexNoise_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_SimplexNoise_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_SimplexNoise_MVPUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_mvp_matrix");
	pep_SimplexNoise_SkyColorUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_sky_color");
	pep_SimplexNoise_CloudColorUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_cloud_color");
	pep_SimplexNoise_Sampler3DNoiseUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_noise");
	pep_SimplexNoise_ScaleUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_scale");
	pep_SimplexNoise_OffsetUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_offset");
	pep_SimplexNoise_TilingUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_tiling");
	pep_SimplexNoise_AlphaUniform = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_alpha");
	pep_SimplexNoise_BackUniform   = glGetUniformLocation(pep_SimplexNoise_ShaderProgramObject, "u_back");

	glGenVertexArrays(1, &pep_SimplexNoise_Vao);
	glBindVertexArray(pep_SimplexNoise_Vao);
	glGenBuffers(1, &pep_SimplexNoise_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_SimplexNoise_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void SimplexNoise_Resize(int, int)
{

}

void SimplexNoise_Update(void)
{
	pep_SimplexNoise_CloudMotion += 0.001f;
}

void SimplexNoise_Uninitialize(void)
{
	if (pep_SimplexNoise_VboPosition)
	{
		glDeleteBuffers(1, &pep_SimplexNoise_VboPosition);
		pep_SimplexNoise_VboPosition = 0;
	}

	if (pep_SimplexNoise_Vao)
	{
		glDeleteVertexArrays(1, &pep_SimplexNoise_Vao);
		pep_SimplexNoise_Vao = 0;
	}

	if (pep_SimplexNoise_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_SimplexNoise_ShaderProgramObject);

		glGetProgramiv(pep_SimplexNoise_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_SimplexNoise_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_SimplexNoise_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_SimplexNoise_ShaderProgramObject);
		pep_SimplexNoise_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void SimplexNoise_Display(void)
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;
	glUseProgram(pep_SimplexNoise_ShaderProgramObject);
	glUniformMatrix4fv(pep_SimplexNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(pep_SimplexNoise_BackUniform, 1);
	glBindVertexArray(pep_SimplexNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	glUseProgram(0);
	//
	// Enable States
	//
	glEnable(GL_TEXTURE_3D);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, pep_SimplexNoise_MathematicalTexture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_SimplexNoise_ShaderProgramObject);
	glUniform1i(pep_SimplexNoise_Sampler3DNoiseUniform, 0);
	glUniformMatrix4fv(pep_SimplexNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform4f(pep_SimplexNoise_SkyColorUniform, 0.0, 0.0, 0.10, 0.0);
	glUniform4f(pep_SimplexNoise_CloudColorUniform, 0.8, 0.8, 0.8, 1.0);
	glUniform1f(pep_SimplexNoise_ScaleUniform, 1.0);
	//glUniform1f(pep_SimplexNoise_OffsetUniform, pep_SimplexNoise_CloudMotion);
	glUniform3f(pep_SimplexNoise_OffsetUniform, pep_SimplexNoise_CloudMotion, pep_SimplexNoise_CloudMotion * 0.1f, pep_SimplexNoise_CloudMotion);
	glUniform1f(pep_SimplexNoise_TilingUniform, 0.750f);
	glUniform1f(pep_SimplexNoise_AlphaUniform, 1.0f);
	glUniform1i(pep_SimplexNoise_BackUniform, 0);

	glBindVertexArray(pep_SimplexNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

