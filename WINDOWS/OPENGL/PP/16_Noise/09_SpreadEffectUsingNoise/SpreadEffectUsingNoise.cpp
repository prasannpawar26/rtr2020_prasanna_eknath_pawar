#include "SpreadEffectUsingNoise.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_SpreadEffect_Vao;
GLuint pep_SpreadEffect_VboPosition;
GLuint pep_SpreadEffect_VertexShaderObject;
GLuint pep_SpreadEffect_FragmentShaderObject;
GLuint pep_SpreadEffect_ShaderProgramObject;

GLuint pep_SpreadEffect_MVPUniform;
GLuint pep_SpreadEffect_BackUniform;
GLuint pep_SpreadEffect_ScaleUniform;
GLuint pep_SpreadEffect_OffsetUniform;
GLuint pep_SpreadEffect_TilingUniform;
GLuint pep_SpreadEffect_AlphaUniform;

GLubyte * pep_SpreadEffect_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_SpreadEffect_CloudMotion = 0.0f;

int SpreadEffect_Initialize(void)
{
	pep_SpreadEffect_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_SpreadEffect_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_SpreadEffect_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_SpreadEffect_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_SpreadEffect_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_SpreadEffect_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_SpreadEffect_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_SpreadEffect_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_SpreadEffect_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"const float PI = 3.14159265359;" \
		"const float TWO_PI = 6.28318530718;" \
		"const float F3 = 0.3333333;" \
		"const float G3 = 0.1666667;" \
		"const float spread_randomness = 175.0;" \
		"const float spread_speed = 1.0;" \

		"in vec3 mcPosition;" \

		"uniform vec3 u_offset;"\
		"uniform float u_tiling;"\
		"uniform float u_alpha;" \
		"uniform int u_back;" \

		"out vec4 FragColor;" \

		"float shape(vec2 st, int N)" \
		"{" \
			"st = st*2.0-1.;" \
			"float a = atan(st.x, st.y) + PI;" \
			"float r = TWO_PI/float(N);" \
			"return cos(floor(0.5+a/r)*r-a)*length(st);" \
		"}" \

		"float box(vec2 st, vec2 size)" \
		"{" \
			"return shape(st*size, 4);" \
		"}" \

		"float hex(vec2 st, bool f)" \
		"{" \
			"st = st*vec2(2.,6.);" \

			"vec2 fpos = fract(st);" \
			"vec2 ipos = floor(st);" \

			"if (ipos.x == 1.0)" \
			"{" \
				"fpos.x = 1.0-fpos.x;" \
			"}" \

			/*"if (ipos.y < 6.0)" \
			"{" \*/
				"return f ? box(fpos-vec2(0.03,0.), vec2(1.)) : box(fpos, vec2(0.84,1.));" \
			/*"}" \*/

			"return 0.0;" \
		"}" \

		"float hex(vec2 st, float N)" \
		"{" \
			"bool b[6];" \
			"float remain = floor(mod(N,128.0));" \

			"for(int i = 5; i < 6; i++)" \
			"{" \
				"b[i] = mod(remain, 2.0)==1.0 ? true : false;" \
				"remain = ceil(remain / 2.0);" \
			"}" \

			"return hex(st, b[5]);" \
		"}" \

		"vec3 random3(vec3 c)" \
		"{" \
			"float j = 4096.0*sin(dot(c,vec3(17.0, 59.4, 15.0)));" \
			"vec3 r;" \

			"r.z = fract(512.0*j);" \
			"j *= .125;" \
			"r.x = fract(512.0*j);" \
			"j *= .125;" \
			"r.y = fract(512.0*j);" \

			"return r-0.5;" \
		"}" \

		"float snoise(vec3 p)" \
		"{" \
			"vec3 s = floor(p + dot(p, vec3(F3)));" \
			"vec3 x = p - s + dot(s, vec3(G3));" \

			"vec3 e = step(vec3(0.0), x - x.yzx);" \
			"vec3 i1 = e*(1.0 - e.zxy);" \
			"vec3 i2 = 1.0 - e.zxy*(1.0 - e);" \

			"vec3 x1 = x - i1 + G3;" \
			"vec3 x2 = x - i2 + 2.0*G3;" \
			"vec3 x3 = x - 1.0 + 3.0*G3;" \

			"vec4 w, d;" \

			"w.x = dot(x, x);" \
			"w.y = dot(x1, x1);" \
			"w.z = dot(x2, x2);" \
			"w.w = dot(x3, x3);" \

			"w = max(0.6 - w, 0.0);" \

			"d.x = dot(random3(s), x);" \
			"d.y = dot(random3(s + i1), x1);" \
			"d.z = dot(random3(s + i2), x2);" \
			"d.w = dot(random3(s + 1.0), x3);" \

			"w *= w;" \
			"w *= w;" \
			"d *= w;" \

			"return dot(d, vec4(52.0));" \
		"}" \

		"vec2 u_resolution = vec2(1920.0, 1080.0);" \

		"void main(void)" \
		"{" \

			"if(1 == u_back) { FragColor = vec4(0.0, 0.0, 1.0, 1.0); return;}" \

			"vec2 st = gl_FragCoord.xy / u_resolution.xy;" \
			"st.y *= u_resolution.y / u_resolution.x;" \

			"float t = u_offset.x * spread_speed;" \
			"float df = 2.0;" \
			
			"df = mix(hex(st, t), hex(st, t+1.0), fract(t));" \

			"df += snoise(vec3(st * spread_randomness, t*0.1)) * 0.03;" \
			
			"FragColor = vec4(mix(vec3(0.0), vec3(1.0), step(0.7,df)), u_alpha);"
		"}";

	glShaderSource(pep_SpreadEffect_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_SpreadEffect_FragmentShaderObject);
	glGetShaderiv(pep_SpreadEffect_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_SpreadEffect_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_SpreadEffect_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_SpreadEffect_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_SpreadEffect_ShaderProgramObject, pep_SpreadEffect_VertexShaderObject);
	glAttachShader(pep_SpreadEffect_ShaderProgramObject, pep_SpreadEffect_FragmentShaderObject);
	glBindAttribLocation(pep_SpreadEffect_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_SpreadEffect_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_SpreadEffect_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_SpreadEffect_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_SpreadEffect_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_SpreadEffect_MVPUniform = glGetUniformLocation(pep_SpreadEffect_ShaderProgramObject, "u_mvp_matrix");
	pep_SpreadEffect_ScaleUniform = glGetUniformLocation(pep_SpreadEffect_ShaderProgramObject, "u_scale");
	pep_SpreadEffect_OffsetUniform = glGetUniformLocation(pep_SpreadEffect_ShaderProgramObject, "u_offset");
	pep_SpreadEffect_TilingUniform = glGetUniformLocation(pep_SpreadEffect_ShaderProgramObject, "u_tiling");
	pep_SpreadEffect_AlphaUniform = glGetUniformLocation(pep_SpreadEffect_ShaderProgramObject, "u_alpha");
	pep_SpreadEffect_BackUniform   = glGetUniformLocation(pep_SpreadEffect_ShaderProgramObject, "u_back");
	glGenVertexArrays(1, &pep_SpreadEffect_Vao);
	glBindVertexArray(pep_SpreadEffect_Vao);
	glGenBuffers(1, &pep_SpreadEffect_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_SpreadEffect_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void SpreadEffect_Resize(int, int)
{

}

void SpreadEffect_Update(void)
{
	pep_SpreadEffect_CloudMotion += 0.007f;
}

void SpreadEffect_Uninitialize(void)
{
	if (pep_SpreadEffect_VboPosition)
	{
		glDeleteBuffers(1, &pep_SpreadEffect_VboPosition);
		pep_SpreadEffect_VboPosition = 0;
	}

	if (pep_SpreadEffect_Vao)
	{
		glDeleteVertexArrays(1, &pep_SpreadEffect_Vao);
		pep_SpreadEffect_Vao = 0;
	}

	if (pep_SpreadEffect_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_SpreadEffect_ShaderProgramObject);

		glGetProgramiv(pep_SpreadEffect_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_SpreadEffect_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_SpreadEffect_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_SpreadEffect_ShaderProgramObject);
		pep_SpreadEffect_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void SpreadEffect_Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;
	glUseProgram(pep_SpreadEffect_ShaderProgramObject);
	glUniformMatrix4fv(pep_SpreadEffect_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(pep_SpreadEffect_BackUniform, 1);
	glBindVertexArray(pep_SpreadEffect_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Enable States
	//
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//mat4 modelViewMatrix;
	//mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_SpreadEffect_ShaderProgramObject);
	glUniformMatrix4fv(pep_SpreadEffect_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1f(pep_SpreadEffect_ScaleUniform, 1.0);
	glUniform1i(pep_SpreadEffect_BackUniform, 0);
	//glUniform1f(pep_SpreadEffect_OffsetUniform, pep_SpreadEffect_CloudMotion);
	glUniform3f(pep_SpreadEffect_OffsetUniform, pep_SpreadEffect_CloudMotion, pep_SpreadEffect_CloudMotion * 0.1f, pep_SpreadEffect_CloudMotion);
	glUniform1f(pep_SpreadEffect_TilingUniform,20.0f);
	glUniform1f(pep_SpreadEffect_AlphaUniform, 1.0f);

	glBindVertexArray(pep_SpreadEffect_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

