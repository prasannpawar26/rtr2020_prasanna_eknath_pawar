#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int LostSignalPatternNoise_Initialize(void);
void LostSignalPatternNoise_Resize(int, int);
void LostSignalPatternNoise_Uninitialize(void);
void LostSignalPatternNoise_Update(void);
void LostSignalPatternNoise_Display(void);
