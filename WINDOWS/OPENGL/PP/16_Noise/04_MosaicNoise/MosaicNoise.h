#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int MosaicNoise_Initialize(void);
void MosaicNoise_Resize(int, int);
void MosaicNoise_Uninitialize(void);
void MosaicNoise_Update(void);
void MosaicNoise_Display(void);
