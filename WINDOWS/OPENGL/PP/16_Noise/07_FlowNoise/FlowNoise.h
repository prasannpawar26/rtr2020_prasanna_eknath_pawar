#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int FlowNoise_Initialize(void);
void FlowNoise_Resize(int, int);
void FlowNoise_Uninitialize(void);
void FlowNoise_Update(void);
void FlowNoise_Display(void);
