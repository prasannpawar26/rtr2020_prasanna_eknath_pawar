#include <Windows.h>
#include <stdio.h>
#include <gl/GLEW.h>
#include <gl/GL.h>
#include "Sphere.h"
#include "vmath.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Sphere.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//CallBack
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global Variable Declaration
DWORD dwstyle;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
FILE* gpFile = NULL;
bool bDone = false;
bool gbFullScreen = false;
bool gbActivewindow = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

//Variable 
GLuint vao_Sphere;
GLuint vboPosition;
GLuint vboNormals;
GLuint vboElements;
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint ldUniform;
GLuint laUniform;
GLuint lsUniform;
GLuint kdUniform;
GLuint kaUniform;
GLuint ksUniform;
GLuint shininessMaterialUniform;
GLuint lightPositionUniform;
GLuint lIsPressed;
mat4 perspectiveProjectionMatrix;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];

GLint gNumVertices;
GLint gNumElements;

GLuint gvboElements_Light;

bool gbAnimation = false;
bool gbLighting = false;

GLfloat lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[4] = { 0.0f, 0.0f, 100.0f, 1.0f };

GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
//GLfloat materialShininess = 50.0f;
GLfloat materialShininess = 120.0f;

GLuint gVertexShaderObject_GodRays;
GLuint gFragmentShaderObject_GodRays;
GLuint gShaderProgramObject_GodRays;

GLuint vaoCube;
GLuint vaoQuad;

GLuint Sampler;
GLuint ColorBuffers;
GLuint DepthBuffers;
GLuint FBO;

int SunWidth;
int SunHeight;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int initialize(void);
	void display(void);

	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	int iRet = 0;
	TCHAR szAppName[] = TEXT("OpenGL-Native 3D Rotation");

	//Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s() Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n");
	}

	//Initialization Of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szAppName;

	//Register above Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("God Rays"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat() Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext() Failed.\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed.\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded.\n");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActivewindow == true)
			{
			}
			display();
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void ToggleFullScreen(void);

	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActivewindow = true;
		break;

	case WM_KILLFOCUS:
		gbActivewindow = false;
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'L':
		case 'l':
			if (gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
			break;

		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	}

	return((DefWindowProc(hwnd, iMsg, wParam, lParam)));
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwstyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if ((GetWindowPlacement(ghwnd, &wpPrev)) && (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left),
					(mi.rcMonitor.bottom - mi.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

int initialize(void)
{
	//Variable 
	GLenum result;
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize PFD Structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if ((SetPixelFormat(ghdc, iPixelFormatIndex, &pfd)) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}

	if ((wglMakeCurrent(ghdc, ghrc)) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit Failed.\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode =
	{
		" #version 450 core																			\n"
		" 																							\n"
		" in vec4 vPosition;																		\n"
		" in vec3 vNormal;																			\n"
		" 																							\n"
		" uniform mat4 u_m_matrix;																	\n"
		" uniform mat4 u_v_matrix;																	\n"
		" uniform mat4 u_projection;																\n"
		" uniform int u_lKeyPressed;																\n"
		" uniform vec4 u_light_position;															\n"
		" 																							\n"
		" out vec3 tNorm;																			\n"
		" out vec3 lightDirection;																	\n"
		" out vec3 viewer_vector;																	\n"
		" 																							\n"
		" void main(void)																			\n"
		" { 																						\n"
		"																							\n"
		"	if(u_lKeyPressed == 1)																	\n"
		"	{																						\n"
		"		vec4 eye_coordinate = u_v_matrix  * u_m_matrix  * vPosition;						\n"
		"		tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);							\n"
		"		lightDirection = normalize(vec3(u_light_position - eye_coordinate));				\n"
		"		viewer_vector = normalize(vec3(-eye_coordinate.xyz));								\n"
		"	}																						\n"
		"																							\n"
		"	gl_Position =u_projection * u_v_matrix * u_m_matrix * vPosition;						\n"
		" }  																						\n"
	};

	//Specify above source code to vertex shader object 
	glShaderSource(gVertexShaderObject, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iLogInfoLength = 0;
	GLchar* szInfoLog = NULL;
	GLint iShaderProgramLinkStatus = 0;

	//Code
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Vertex Shader Code
	const GLchar* fragmentShaderSourceCode =
	{
		"	#version 450 core																													\n"
		"																																		\n"
		"	in vec3 tNorm;																														\n"
		"	in vec3 lightDirection;																												\n"
		"	in vec3 viewer_vector;																												\n"
		"																																		\n"
		"	uniform vec3 u_la;																													\n"
		"	uniform vec3 u_ld;																													\n"
		"	uniform vec3 u_ls;																													\n"
		"	uniform vec3 u_ka;																													\n"
		"	uniform vec3 u_kd;																													\n"
		"	uniform vec3 u_ks;																													\n"
		"	uniform float u_material_shininess;																									\n"
		"	uniform int u_lKeyPressed;																											\n"
		"																																		\n"
		"	out vec4 fragColor;																													\n"
		"	void main(void)																														\n"
		"	{																																	\n"
		"		vec3 phong_ADS_light;																											\n"
		"		if(u_lKeyPressed == 1)																											\n"
		"		{																																\n"
		"			vec3 normalized_transformed_normals = normalize(tNorm);																		\n"
		"			vec3 normalized_light_direction = normalize(lightDirection);																\n"
		"			vec3 normalized_viewer_vector = normalize(viewer_vector);																	\n"
		"			vec3 ambient = u_la * u_ka;																									\n"
		"			float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);									\n"
		"			vec3  diffuse = u_ld * u_kd * tn_dot_ld;																					\n"
		"			vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals );								\n"
		"			vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0), u_material_shininess);			\n"
		"			phong_ADS_light = ambient + diffuse + specular;																				\n"
		"		}																																\n"
		"		else																															\n"
		"		{																																\n"
		"			phong_ADS_light = vec3(1.0, 1.0, 1.0);																						\n"
		"		}																																\n"
		//"	fragColor = vec4(phong_ADS_light, 1.0) * vec4(1.0f, 0.90f, 0.80f, 1.0f);																								\n"
		"	fragColor = vec4(1.0f, 0.90f, 0.80f, 1.0f);																								\n"
		"	}																																	\n"
	};

	//Specify above source code to vertex shader object 
	glShaderSource(gFragmentShaderObject, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);
	fprintf(gpFile, "\n\nAfter Fragment Shader :\n \n\n");
	//compile the vertex shader
	glCompileShader(gFragmentShaderObject);

	iShaderCompileStatus = 0;
	iLogInfoLength = 0;
	szInfoLog = NULL;

	//Code
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program Object
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Pre-Linking Binding Vertex Attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	//Code
	glGetProgramiv(gShaderProgramObject, GL_COMPILE_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Linking Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking retriving uniform Location
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection");
	lIsPressed = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	shininessMaterialUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	//const GLfloat Vertices[] =
	//{
	//	1.0f,  1.0f,  1.0f,
	//   -1.0f,  1.0f,  1.0f,
	//   -1.0f, -1.0f,  1.0f,
	//	1.0f, -1.0f,  1.0f,
	//   -1.0f,  1.0f,  1.0f,
	//   -1.0f,  1.0f, -1.0f,
	//   -1.0f, -1.0f, -1.0f,
	//   -1.0f, -1.0f,  1.0f,
	//	1.0f,  1.0f, -1.0f,
	//	1.0f,  1.0f,  1.0f,
	//	1.0f, -1.0f,  1.0f,
	//	1.0f, -1.0f, -1.0f,
	//	1.0f,  1.0f, -1.0f,
	//   -1.0f,  1.0f, -1.0f,
	//   -1.0f, -1.0f, -1.0f,
	//	1.0f, -1.0f, -1.0f,
	//	1.0f,  1.0f, -1.0f,
	//   -1.0f,  1.0f, -1.0f,
	//   -1.0f,  1.0f,  1.0f,
	//	1.0f,  1.0f,  1.0f,
	//	1.0f, -1.0f, -1.0f,
	//   -1.0f, -1.0f, -1.0f,
	//   -1.0f, -1.0f,  1.0f,
	//	1.0f, -1.0f,  1.0f
	//};

	//const GLfloat Normals[] =
	//{
	//	 0.0f,  1.0f,  0.0f,
	//	 0.0f,  1.0f,  0.0f,
	//	 0.0f,  1.0f,  0.0f,
	//	 0.0f,  1.0f,  0.0f,
	//	 0.0f, -1.0f,  0.0f,
	//	 0.0f, -1.0f,  0.0f,
	//	 0.0f, -1.0f,  0.0f,
	//	 0.0f, -1.0f,  0.0f,
	//	 0.0f,  0.0f,  1.0f,
	//	 0.0f,  0.0f,  1.0f,
	//	 0.0f,  0.0f,  1.0f,
	//	 0.0f,  0.0f,  1.0f,
	//	 0.0f,  0.0f, -1.0f,
	//	 0.0f,  0.0f, -1.0f,
	//	 0.0f,  0.0f, -1.0f,
	//	 0.0f,  0.0f, -1.0f,
	//	-1.0f,  0.0f,  0.0f,
	//	-1.0f,  0.0f,  0.0f,
	//	-1.0f,  0.0f,  0.0f,
	//	-1.0f,  0.0f,  0.0f,
	//	 1.0f,  0.0f,  0.0f,
	//	 1.0f,  0.0f,  0.0f,
	//	 1.0f,  0.0f,  0.0f,
	//	 1.0f,  0.0f,  0.0f
	//};

	////Create vao
	//glGenVertexArrays(1, &vao_Sphere);
	//glBindVertexArray(vao_Sphere);

	//glGenBuffers(1, &vboPosition);
	//glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
	//glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);

	//glGenBuffers(1, &vboNormals);
	//glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(Normals), Normals, GL_STATIC_DRAW);
	//glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//Create vao
	glGenVertexArrays(1, &vao_Sphere);
	glBindVertexArray(vao_Sphere);

	glGenBuffers(1, &vboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vboNormals);
	glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gvboElements_Light);
	glBindBuffer(GL_ARRAY_BUFFER, gvboElements_Light);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// God Rays 
	// God Rays
	//define Vertex Shader Object
	gVertexShaderObject_GodRays = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode_GodRays =
	{
		" #version 120 								\n"
		"  											\n"
		" void main() 								\n"
		" { 										\n"
		" 	gl_TexCoord[0] = gl_Vertex; 			\n"
		" 	gl_Position = gl_Vertex * 2.0 - 1.0; 	\n"
		" } 										\n"

	};

	//Specify above source code to vertex shader object 
	glShaderSource(gVertexShaderObject_GodRays, 1, (const GLchar * *)& vertexShaderSourceCode_GodRays, NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject_GodRays);

	iShaderCompileStatus = 0;
	iLogInfoLength = 0;
	szInfoLog = NULL;
	iShaderProgramLinkStatus = 0;

	//Code
	glGetShaderiv(gVertexShaderObject_GodRays, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_GodRays, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject_GodRays, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//define Fragment Shader Object
	gFragmentShaderObject_GodRays = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Vertex Shader Code
	const GLchar* fragmentShaderSourceCode_GodRays =
	{
		" #version 120 															 \n"
		"  																		 \n"
		" uniform sampler2D ColorBuffer; 										 \n"
		"  																		 \n"
		" void main() 															 \n"
		" { 																	 \n"
		" 	int Samples = 128; 													 \n"
		" 	float Intensity = 0.125, Decay = 0.96875; 							 \n"
		" 	vec2 TexCoord = gl_TexCoord[0].st, Direction = vec2(0.5) - TexCoord; \n"
		" 	Direction /= Samples; 												 \n"
		" 	vec3 Color = texture2D(ColorBuffer, TexCoord).rgb; 					 \n"
		" 	 																	 \n"
		" 	for(int Sample = 0; Sample < Samples; Sample++) 					 \n"
		" 	{ 																     \n"
		" 		Color += texture2D(ColorBuffer, TexCoord).rgb * Intensity; 		 \n"
		" 		Intensity *= Decay; 											 \n"
		" 		TexCoord += Direction; 											 \n"
		" 	} 																	 \n"
		" 	 																     \n"
		" 	gl_FragColor = vec4(Color, 1.0); 									 \n"
		" } 																	 \n"
	};

	//Specify above source code to vertex shader object 
	glShaderSource(gFragmentShaderObject_GodRays, 1, (const GLchar * *)& fragmentShaderSourceCode_GodRays, NULL);
	//compile the vertex shader
	glCompileShader(gFragmentShaderObject_GodRays);

	iShaderCompileStatus = 0;
	iLogInfoLength = 0;
	szInfoLog = NULL;

	//Code
	glGetShaderiv(gFragmentShaderObject_GodRays, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_GodRays, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject_GodRays, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program Object
	gShaderProgramObject_GodRays = glCreateProgram();

	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject_GodRays, gVertexShaderObject_GodRays);

	//Attach Vertex Shader to Shader Program
	glAttachShader(gShaderProgramObject_GodRays, gFragmentShaderObject_GodRays);

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_GodRays);

	//Code
	glGetProgramiv(gShaderProgramObject_GodRays, GL_COMPILE_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_GodRays, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject_GodRays, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Linking Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	Sampler = glGetUniformLocation(gShaderProgramObject_GodRays, "ColorBuffer");

	const GLfloat quadVertices[] =
	{
		/*0.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f*/

		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	const GLfloat quadTexCoords[] =
	{
		 0.0f, 0.0f,
		 1.0f, 0.0f,
		 1.0f, 1.0f,
		 0.0f, 1.0
	};

	//Create vao
	glGenVertexArrays(1, &vaoQuad);
	glBindVertexArray(vaoQuad);
	glGenBuffers(1, &vboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/*glGenBuffers(1, &vboNormals);
	glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);*/
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glGenTextures(1, &ColorBuffers);
	glGenTextures(1, &DepthBuffers);

	glGenFramebuffersEXT(1, &FBO);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);



	SunWidth = width / 2;
	SunHeight = height / 2;

	glBindTexture(GL_TEXTURE_2D, ColorBuffers);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, DepthBuffers);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void display(void)
{
	// Variable 
	static GLfloat angle = 90.0f;

	//glViewport(0, 0, (GLsizei)SunWidth, (GLsizei)SunHeight);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, FBO);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, ColorBuffers, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, DepthBuffers, 0);
	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	glUniform1i(lIsPressed, 1);

	glUniform3fv(laUniform, 1, lightAmbient);
	glUniform3fv(ldUniform, 1, lightDiffuse);
	glUniform3fv(lsUniform, 1, lightSpecular);
	glUniform4fv(lightPositionUniform, 1, lightPosition);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(shininessMaterialUniform, materialShininess);

	//Declaration of Matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 modelViewProjectionMatrix;

	//Initialize above matrices to there identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do Necessary Transformation
	modelMatrix = translate(0.0f, 0.0f, -6.0f);
	modelMatrix = modelMatrix * rotate(angle, 1.0f, 0.0f, 0.0f);
	modelMatrix = modelMatrix * rotate(angle, 0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotate(angle, 0.0f, 0.0f, 1.0f);

	//Send Necessary Matrices to Shader in respective uniform
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	//Bind With vao
	//glBindVertexArray(vao_Sphere);

	//glDrawArrays(GL_TRIANGLE_FAN,  0, 4);
	//glDrawArrays(GL_TRIANGLE_FAN,  4, 4);
	//glDrawArrays(GL_TRIANGLE_FAN,  8, 4);
	//glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	//glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	//glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	////UnBind With vao
	//glBindVertexArray(0);

	glBindVertexArray(vao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gvboElements_Light);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ColorBuffers);
	glUniform1i(Sampler, 1);
	glUseProgram(gShaderProgramObject_GodRays);
	glBindVertexArray(vaoQuad);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//angle = angle + 0.1f;

	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//Code
	if (vboNormals)
	{
		glDeleteBuffers(1, &vboNormals);
		vboNormals = 0;
	}

	if (vboPosition)
	{
		glDeleteBuffers(1, &vboPosition);
		vboPosition = 0;
	}

	if (vao_Sphere)
	{
		glDeleteVertexArrays(1, &vao_Sphere);
		vao_Sphere = 0;
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		//Ask Program how many shader are Attached to You
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if (pShader)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShader[shaderNumber]);

				//Delete detach Shader
				glDeleteShader(pShader[shaderNumber]);
				pShader[shaderNumber] = 0;
			}
			free(pShader);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		glUseProgram(0);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}

}
