#include "Branch_1.h"
#include "common.h"

#include "GeometryShapes.h"

extern GLuint materialAmbientUniform;
extern GLuint materialDiffuseUniform;
extern GLuint materialSpecularUniform;
extern GLuint materialShinessUniform;

int branch_3_no_of_surface = 20;
float branch_3_top_circle_radius = 0.010f;
float branch_3_bottom_circle_radius = 0.20f;
float branch_3_height = 4.0f;

int size_of_branch_3_vertices_array;

float *g_branch_3_vertices;
float *g_branch_3_normals;

GLuint g_branch_3_vao;
GLuint g_branch_3_vbo_vertex;
GLuint g_branch_3_vbo_normal;

float g_branch_3_material_ambient[4] = {0.460f, 0.359f, 0.281f, 1.0f};
float g_branch_3_material_diffuse[4] = {0.460f, 0.359f, 0.281f, 1.0f};
float g_branch_3_material_specular[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float g_branch_3_material_shiness = 0.4f * 128;  // Also Try 128.0f

void Branch_3_Initialize(void) {
  int number_of_vertices_in_circle = branch_3_no_of_surface;
  int no_of_circles = 2;
  int no_of_vertices_in_surface = branch_3_no_of_surface * NO_VERTICES_IN_QUAD;
  /*int*/ size_of_branch_3_vertices_array =
      no_of_circles * number_of_vertices_in_circle * COORDINATE_SYSTEM_3D *
          sizeof(GLfloat) +
      no_of_vertices_in_surface * COORDINATE_SYSTEM_3D * sizeof(GLfloat);
  // int no_of_vertices_in_cricle = no_of_circles *
  // number_of_vertices_in_circle;

  g_branch_3_vertices = (GLfloat *)malloc(size_of_branch_3_vertices_array);
  g_branch_3_normals = (GLfloat *)malloc(size_of_branch_3_vertices_array);

  memset(g_branch_3_vertices, 0, size_of_branch_3_vertices_array);
  memset(g_branch_3_normals, 0, size_of_branch_3_vertices_array);

  getCylinederData(branch_3_no_of_surface, branch_3_top_circle_radius,
                   branch_3_bottom_circle_radius, branch_3_height, &g_branch_3_vertices,
                   &g_branch_3_normals);

  // cube
  glGenVertexArrays(1, &g_branch_3_vao);
  glBindVertexArray(g_branch_3_vao);

  // cube-vertex

  glGenBuffers(1, &g_branch_3_vbo_vertex);
  glBindBuffer(GL_ARRAY_BUFFER, g_branch_3_vbo_vertex);
  glBufferData(GL_ARRAY_BUFFER, size_of_branch_3_vertices_array,
               NULL /*g_branch_3_vertices*/, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenBuffers(1, &g_branch_3_vbo_normal);
  glBindBuffer(GL_ARRAY_BUFFER, g_branch_3_vbo_normal);
  glBufferData(GL_ARRAY_BUFFER, size_of_branch_3_vertices_array,
               NULL /*g_branch_3_normals*/, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);
}

void Branch_3_Display(void) {
  glBindVertexArray(g_branch_3_vao);

  int no_of_circles = 2;
  int number_of_vertices_in_circle = branch_3_no_of_surface;

  glBindBuffer(GL_ARRAY_BUFFER, g_branch_3_vbo_vertex);
  glBufferData(GL_ARRAY_BUFFER, size_of_branch_3_vertices_array,
               g_branch_3_vertices,
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, g_branch_3_vbo_normal);
  glBufferData(GL_ARRAY_BUFFER, size_of_branch_3_vertices_array,
               g_branch_3_normals,
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glDrawArrays(GL_TRIANGLE_FAN, 0, number_of_vertices_in_circle);
  glDrawArrays(GL_TRIANGLE_FAN, number_of_vertices_in_circle,
               number_of_vertices_in_circle);

  for (int i = 0; i < number_of_vertices_in_circle; i += 1) {
    glDrawArrays(GL_TRIANGLE_FAN,
                 number_of_vertices_in_circle * no_of_circles + i * 4, 4);
  }

  glBindVertexArray(0);
}

void Branch_3_Uniforms(void) {
  glUniform3fv(materialAmbientUniform, 1, g_branch_3_material_ambient);
  glUniform3fv(materialDiffuseUniform, 1, g_branch_3_material_diffuse);
  glUniform3fv(materialSpecularUniform, 1, g_branch_3_material_specular);
  glUniform1f(materialShinessUniform, g_branch_3_material_shiness);
}

void Branch_3_Update(void) {
 /* int number_of_vertices_in_circle = branch_3_no_of_surface;
  int no_of_circles = 2;
  int no_of_vertices_in_surface = branch_3_no_of_surface * NO_VERTICES_IN_QUAD;
  int size_of_vertices_array =
      no_of_circles * number_of_vertices_in_circle * COORDINATE_SYSTEM_3D *
          sizeof(GLfloat) +
      no_of_vertices_in_surface * COORDINATE_SYSTEM_3D * sizeof(GLfloat);*/
}

void Branch_3_Uninitialize(void) {
  if (g_branch_3_vbo_normal) {
    glDeleteBuffers(1, &g_branch_3_vbo_normal);
    g_branch_3_vbo_normal = 0;
  }

  if (g_branch_3_vbo_vertex) {
    glDeleteBuffers(1, &g_branch_3_vbo_vertex);
    g_branch_3_vbo_vertex = 0;
  }

  if (g_branch_3_vao) {
    glDeleteVertexArrays(1, &g_branch_3_vao);
    g_branch_3_vao = 0;
  }

  if (g_branch_3_normals) {
    free(g_branch_3_normals);
  }

  if (g_branch_3_vertices) {
    free(g_branch_3_vertices);
  }
}