#pragma once
void Trunk_Initialize(void);
void Trunk_Display(void);
void Trunk_Uniforms(void);
void Trunk_Uninitialize(void);
void Trunk_Update(void);