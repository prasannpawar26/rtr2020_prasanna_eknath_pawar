#pragma once

#define CIRCLE_DEGREE 360
#define NO_VERTICES_IN_QUAD 4
#define COORDINATE_SYSTEM_3D 3

void getCylinederData(int no_of_surface, float top_circle_radius, float bottom_circle_radius, float height, float **vertices, float **normals);