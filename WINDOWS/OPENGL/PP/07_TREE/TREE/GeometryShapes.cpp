#include "GeometryShapes.h"
#include "common.h"
#include "vmath.h"

using namespace std;
using namespace vmath;

float DegToRad(const float& deg)
{
	return deg * (M_PI / 180.0f);
}

void getCylinederData(int no_of_surface, float top_circle_radius, float bottom_circle_radius, float height, float **vertices, float **normals)
{
	// Function Declarations
	float DegToRad(const float& deg);

	// Variable Declarations
	int angle_factor = CIRCLE_DEGREE / no_of_surface;
	int number_of_vertices_in_circle = no_of_surface;
	int no_of_circles = 2;
	int no_of_vertices_in_surface = no_of_surface * NO_VERTICES_IN_QUAD;
	int size_of_vertices_array = no_of_circles * number_of_vertices_in_circle * COORDINATE_SYSTEM_3D * sizeof(float) + no_of_vertices_in_surface * COORDINATE_SYSTEM_3D * sizeof(float);
	int no_of_vertices_in_cricle = no_of_circles * number_of_vertices_in_circle;

	// Code
	float angle = 0.0f;
	for (int i = 0; i < number_of_vertices_in_circle * COORDINATE_SYSTEM_3D; i += 3) {
		(*vertices)[i + 0] = 0.0f + (float)cos(DegToRad(angle)) * top_circle_radius;
		(*vertices)[i + 1] = height / 2;
		(*vertices)[i + 2] = 0.0f + (float)sin(DegToRad(angle)) * top_circle_radius;

		(*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + i + 0] = 0.0f + (float)cos(DegToRad(angle)) * bottom_circle_radius;
		(*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + i + 1] = -height / 2;
		(*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + i + 2] = 0.0f + (float)sin(DegToRad(angle)) * bottom_circle_radius;

		(*normals)[i + 0] = 0.0f;
		(*normals)[i + 1] = height / 2;
		(*normals)[i + 2] = 0.0f;

		(*normals)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + i + 0] = 0.0f;
		(*normals)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + i + 1] = -height / 2;
		(*normals)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + i + 2] = 0.0f;

		angle += angle_factor;
	}

	int i = no_of_circles * number_of_vertices_in_circle * COORDINATE_SYSTEM_3D;
	int j = 0;
	for (int index = 0; index < number_of_vertices_in_circle; index += 1)
	{
		if (j + 3 == number_of_vertices_in_circle * 3)
		{
			(*vertices)[i + 0] = (*vertices)[0];
			(*vertices)[i + 1] = (*vertices)[1];
			(*vertices)[i + 2] = (*vertices)[2];

			// Vertex 2
			(*vertices)[i + 3] = (*vertices)[j + 0];
			(*vertices)[i + 4] = (*vertices)[j + 1];
			(*vertices)[i + 5] = (*vertices)[j + 2];

			// Vertex 3
			(*vertices)[i + 6] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 0];
			(*vertices)[i + 7] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 1];
			(*vertices)[i + 8] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 2];

			(*vertices)[i + 9] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + 0];
			(*vertices)[i + 10] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + 1];
			(*vertices)[i + 11] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + 2];
		}
		else
		{
			(*vertices)[i + 0] = (*vertices)[j + 3];
			(*vertices)[i + 1] = (*vertices)[j + 4];
			(*vertices)[i + 2] = (*vertices)[j + 5];

			// Vertex 2
			(*vertices)[i + 3] = (*vertices)[j + 0];
			(*vertices)[i + 4] = (*vertices)[j + 1];
			(*vertices)[i + 5] = (*vertices)[j + 2];

			// Vertex 3
			(*vertices)[i + 6] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 0];
			(*vertices)[i + 7] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 1];
			(*vertices)[i + 8] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 2];

			(*vertices)[i + 9] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 3];
			(*vertices)[i + 10] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 4];
			(*vertices)[i + 11] = (*vertices)[number_of_vertices_in_circle * COORDINATE_SYSTEM_3D + j + 5];
		}

		vec3 v0((*vertices)[i + 0], (*vertices)[i + 1], (*vertices)[i + 2]);
		vec3 v1((*vertices)[i + 3], (*vertices)[i + 4], (*vertices)[i + 5]);
		vec3 v2((*vertices)[i + 6], (*vertices)[i + 7], (*vertices)[i + 8]);
		vec3 normal = normalize(cross(v2 - v0, v1 - v0));
		(*normals)[i + 0] = normal[0];
		(*normals)[i + 1] = normal[1];
		(*normals)[i + 2] = normal[2];

		v0 = vec3((*vertices)[i + 3], (*vertices)[i + 4], (*vertices)[i + 5]);
		v1 = vec3((*vertices)[i + 6], (*vertices)[i + 7], (*vertices)[i + 8]);
		v2 = vec3((*vertices)[i + 9], (*vertices)[i + 10], (*vertices)[i + 11]);
		normal = normalize(cross(v2 - v0, v1 - v0));
		(*normals)[i + 3] = normal[0];
		(*normals)[i + 4] = normal[1];
		(*normals)[i + 5] = normal[2];

		v0 = vec3((*vertices)[i + 6], (*vertices)[i + 7], (*vertices)[i + 8]);
		v1 = vec3((*vertices)[i + 9], (*vertices)[i + 10], (*vertices)[i + 11]);
		v2 = vec3((*vertices)[i + 0], (*vertices)[i + 1], (*vertices)[i + 2]);
		normal = normalize(cross(v2 - v0, v1 - v0));
		(*normals)[i + 6] = normal[0];
		(*normals)[i + 7] = normal[1];
		(*normals)[i + 8] = normal[2];

		v0 = vec3((*vertices)[i + 9], (*vertices)[i + 10], (*vertices)[i + 11]);
		v1 = vec3((*vertices)[i + 0], (*vertices)[i + 1], (*vertices)[i + 2]);
		v2 = vec3((*vertices)[i + 3], (*vertices)[i + 4], (*vertices)[i + 5]);
		normal = normalize(cross(v2 - v0, v1 - v0));
		(*normals)[i + 9] = normal[0];
		(*normals)[i + 10] = normal[1];
		(*normals)[i + 11] = normal[2];

		i += 12;
		j += 3;
	}
}
