#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "common.h"

#include "vmath.h"

#include "Trunk.h"
#include "Branch_1.h"
#include "Branch_2.h"
#include "Branch_3.h"
#include "Branch_4.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *gpFile = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow;
bool gbIsFullScreen;

bool gbIsLightEnable = true;

bool gbIsVertexShader = true;

//
// Shader Related Variables
//
GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;

//
// UNIFORMS
//
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint lightAmbientUniform;
GLuint lightDiffuseUniform;
GLuint lightSpecularUniform;
GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShinessUniform;
GLuint lightPositionUniform;
GLuint keyLPressedUniform;
GLuint alphaUniform;

//
// Application Variables Associated With Uniforms
//
float lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 0.0f, 0.0f, 2.0f, 1.0f };

int lKeyPressed = 1;

//
// Matrices To Be Send To Shaders
//
mat4 modelMatrix;
mat4 viewMatrix;
mat4 perspectiveProjectionMatrix;

//
// Matrices
//
mat4 modelTrunkMatrix;
mat4 modelBranch_1Matrix; // X-rotation
mat4 modelBranch_2Matrix; // X-rotation
mat4 modelBranch_3Matrix; // Y-Rotation
mat4 modelBranch_4Matrix; // Y-Rotation

extern float trunk_rotate;

const GLchar *gc_PerFragment_vertexShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec4 vPosition;" \
"in vec3 vNormal;" \
"struct matrices {" \
"mat4 model_matrix;" \
"mat4 view_matrix;" \
"mat4 projection_matrix;" \
"};" \
"uniform matrices u_matrices;" \
"uniform int u_key_L_pressed;" \
"struct light_configuration {" \
"vec3 light_ambient;" \
"vec3 light_diffuse;" \
"vec3 light_specular;" \
"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration;" \
"out vec3 light_direction;" \
"out vec3 tranformation_matrix;" \
"out vec3 viewer_vector;" \
"void main(void)" \
"{" \
"if(1 == u_key_L_pressed)" \
"{" \
"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" \
"light_direction = vec3(u_light_configuration.light_position - eye_coordinates);" \
"viewer_vector = vec3(-eye_coordinates);" \
"}" \

"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * " \
"vPosition;" \
"}";

const GLchar *gc_PerFragment_fragmentShaderSourceCode =
"#version 450 core" \
"\n" \
"in vec3 light_direction;" \
"in vec3 tranformation_matrix;" \
"in vec3 viewer_vector;" \
"uniform int u_key_L_pressed;" \
"uniform float u_alpha;" \
"struct light_configuration {" \
"vec3 light_ambient;" \
"vec3 light_diffuse;" \
"vec3 light_specular;" \
"vec4 light_position;" \
"};" \
"uniform light_configuration u_light_configuration;" \

"struct material_configuration {"
"vec3 material_ambient;" \
"vec3 material_diffuse;" \
"vec3 material_specular;" \
"float material_shiness;" \
"};" \
"uniform material_configuration u_material_configuration;" \

"out vec4 FragColor;" \
"vec3 phong_ads_light;" \
"void main(void)" \
"{" \
"if(1 == u_key_L_pressed)" \
"{" \
"vec3 light_direction_normalize = normalize(light_direction);" \
"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
"vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \
"vec3 viewer_vector_normal = normalize(viewer_vector);" \
"float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \
"vec3 ambient = u_light_configuration.light_ambient * u_material_configuration.material_ambient;" \
"vec3 diffuse = u_light_configuration.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction;" \
"vec3 specular = u_light_configuration.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" \
"phong_ads_light= ambient + diffuse + specular;" \
"}" \
"else" \
"{" \
"phong_ads_light = vec3(1.0f * u_alpha, 1.0f * u_alpha, 1.0f * u_alpha);" \
"}" \
"FragColor = vec4(phong_ads_light[0] *u_alpha, phong_ads_light[1] *u_alpha, phong_ads_light[2] * u_alpha , 1.0 * u_alpha);" \
"}";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow) {
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);
	void ToggledFullScreen(void);
	void ReSize(int, int);

	// variable declarations
	bool bDone = false;
	MSG msg = { 0 };
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("Humanoid Robot");

	// code
	if (0 != fopen_s(&gpFile, "log.txt", "w")) {
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("Humanoid Robot"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, 800, 600, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (-1 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-2 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Set Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-3 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglCrateContext "
			"Failed.\nExitting Now... "
			"Successfully\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-4 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : wglMakeCurrent "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-5 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : glewInit Failed. "
			"\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-6 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-7 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-8 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-9 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCompileShader "
			"Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else if (-10 == iRet) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	ToggledFullScreen();

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (WM_QUIT == msg.message) {
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow) {
			}
			Update();
			Display();
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// function declarations
  void ToggledFullScreen(void);
  void Uninitialize(void);
  void ReSize(int, int);
  int Initialize_Shaders(const GLchar *, const GLchar *);
  void Uninitialize_Shaders(void);

  // code
  switch (iMsg)
  {

    case WM_DESTROY: 
    {
      Uninitialize();
      PostQuitMessage(0);
    } break;


    case WM_ERASEBKGND:
    {
      return 0;
    } break;

    case WM_CLOSE:
    {
      DestroyWindow(hwnd);
    } break;

    case WM_KILLFOCUS:
    {
      gbActiveWindow = false;
    } break;

    case WM_SETFOCUS:
    {
      gbActiveWindow = true;
    } break;

    case WM_SIZE:
    {
      ReSize(LOWORD(lParam), HIWORD(lParam));
    } break;

    case WM_KEYDOWN: {

      switch (wParam)
      {
        case VK_ESCAPE:
        {
          DestroyWindow(hwnd);
        } break;

        case 'L':
        case 'l':
        {
          if (false == gbIsLightEnable) {
            gbIsLightEnable = true;
            lKeyPressed = 1;
          } else {
            gbIsLightEnable = false;
            lKeyPressed = 0;
          }
        } break;

      } // Switch Case End

    } break; // WM_KEYDOWN
  } // Switch Case End

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen) {
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle) {
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void ReSize(int width, int height) {
	// code
	if (0 == height) {
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix =
		perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	return;
}

void Matrix_Uniforms(void)
{
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
		perspectiveProjectionMatrix);
}

void Light_Uniforms(void)
{
	
	if (gbIsLightEnable) {
		glUniform1i(keyLPressedUniform, lKeyPressed);	
		glUniform3fv(lightAmbientUniform, 1, lightAmbient);
		glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
		glUniform3fv(lightSpecularUniform, 1, lightSpecular);
		glUniform4fv(lightPositionUniform, 1, lightPosition);
	}
	else {
		glUniform1i(keyLPressedUniform, lKeyPressed);
	}
}

void Display(void) 
{
	void Light_Uniforms(void);
	void Matrix_Uniforms(void);
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(giShaderProgramObject);

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	viewMatrix = lookat(vec3(0.0f, 0.0f, 10.0f),
		vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	glUniform1f(alphaUniform, 1.0f);
  Light_Uniforms();

  modelTrunkMatrix = mat4::identity();
  modelTrunkMatrix = translate(0.0f, -2.0f, -3.0f) * rotate(trunk_rotate, 0.0f, 1.0f, 0.0f);
  modelMatrix = modelTrunkMatrix;
  Matrix_Uniforms();
  Trunk_Uniforms();
  Trunk_Display();

  modelBranch_1Matrix = mat4::identity();
  modelBranch_1Matrix = modelTrunkMatrix * translate(0.0f, 2.0f, 1.2f);
  modelMatrix = modelBranch_1Matrix * rotate(-45.0f, -1.0f, 0.0f, 0.0f);
  Matrix_Uniforms();
  Branch_1_Uniforms();
  Branch_1_Display();

  modelBranch_2Matrix = mat4::identity();
  modelBranch_2Matrix = modelTrunkMatrix * translate(0.0f, 1.75f, -1.2f);
  modelMatrix = modelBranch_2Matrix * rotate(-45.0f, 1.0f, 0.0f, 0.0f);
  Matrix_Uniforms();
  Branch_2_Uniforms();
  Branch_2_Display();

  modelBranch_3Matrix = mat4::identity();
  modelBranch_3Matrix = modelTrunkMatrix * translate(1.2f, 2.0f, 0.0f);
  modelMatrix = modelBranch_3Matrix * rotate(-40.0f, 0.0f, 0.0f, 1.0f);
  Matrix_Uniforms();
  Branch_3_Uniforms();
  Branch_3_Display();

  modelBranch_4Matrix = mat4::identity();
  modelBranch_4Matrix = modelTrunkMatrix * translate(-1.2f, 1.75f, 0.0f);
  modelMatrix = modelBranch_4Matrix * rotate(-40.0f, 0.0f, 0.0f, -1.0f);
  Matrix_Uniforms();
  Branch_4_Uniforms();
  Branch_4_Display();

	glUseProgram(0);

	SwapBuffers(gHdc);
}

void Uninitialize_Shaders(void)
{
	if (giShaderProgramObject) {
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(giShaderProgramObject);

		glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders) {
			glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(giShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(giShaderProgramObject);
		giShaderProgramObject = 0;

		glUseProgram(0);
	}

	return;

}
void Uninitialize(void) {
	// function declarations
	void Uninitialize_Shaders(void);

	Uninitialize_Shaders();

	if (wglGetCurrentContext() == gHglrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc) {
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc) {
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != gpFile) {
		fclose(gpFile);
	}

	return;
}

int Initialize_Shaders(const GLchar *vertexShaderSourceCode,
	const GLchar *fragmentShaderSourceCode) {

	// code
	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -7;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFragmentShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader glCreateShader "
		"Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	glShaderSource(giFragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(giFragmentShaderObject);

	glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -9;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);
	glAttachShader(giShaderProgramObject, giFragmentShaderObject);

	// Attributes Binding Before Linking
	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		fprintf(
			gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -10;
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	modelMatrixUniform =
		glGetUniformLocation(giShaderProgramObject, "u_matrices.model_matrix");
	viewMatrixUniform =
		glGetUniformLocation(giShaderProgramObject, "u_matrices.view_matrix");
	projectionMatrixUniform =
		glGetUniformLocation(giShaderProgramObject, "u_matrices.projection_matrix");

	lightAmbientUniform =
		glGetUniformLocation(giShaderProgramObject, "u_light_configuration.light_ambient");
	lightDiffuseUniform =
		glGetUniformLocation(giShaderProgramObject, "u_light_configuration.light_diffuse");
	lightSpecularUniform =
		glGetUniformLocation(giShaderProgramObject, "u_light_configuration.light_specular");
	lightPositionUniform =
		glGetUniformLocation(giShaderProgramObject, "u_light_configuration.light_position");
	materialAmbientUniform =
		glGetUniformLocation(giShaderProgramObject, "u_material_configuration.material_ambient");
	materialDiffuseUniform =
		glGetUniformLocation(giShaderProgramObject, "u_material_configuration.material_diffuse");
	materialSpecularUniform =
		glGetUniformLocation(giShaderProgramObject, "u_material_configuration.material_specular");
	materialShinessUniform =
		glGetUniformLocation(giShaderProgramObject, "u_material_configuration.material_shiness");

	keyLPressedUniform =
		glGetUniformLocation(giShaderProgramObject, "u_key_L_pressed");

	alphaUniform =
		glGetUniformLocation(giShaderProgramObject, "u_alpha");

	return 0;
}

int Initialize(void) {
	// variable declarations
	int index;
	PIXELFORMATDESCRIPTOR pfd;

	void ReSize(int, int);
	void Uninitialize(void);
	int Initialize_Shaders(const GLchar *, const GLchar *);

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	index = ChoosePixelFormat(gHdc, &pfd);
	if (0 == index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, index, &pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc) {
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum result;
	result = glewInit();
	if (GLEW_OK != result) {
		return -5;
	}

	Initialize_Shaders(gc_PerFragment_vertexShaderSourceCode,
		gc_PerFragment_fragmentShaderSourceCode);

  Trunk_Initialize();
  Branch_1_Initialize();
  Branch_2_Initialize();
  Branch_3_Initialize();
  Branch_4_Initialize();

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	perspectiveProjectionMatrix = mat4::identity();

	ReSize(800, 600);

	fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}

void Update(void)
{
  Trunk_Update();
	return;
}