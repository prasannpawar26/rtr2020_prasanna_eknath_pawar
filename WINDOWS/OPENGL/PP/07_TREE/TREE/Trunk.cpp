#include "Trunk.h"
#include "common.h"

#include "GeometryShapes.h"

float trunk_rotate = 0.0f;

extern GLuint materialAmbientUniform;
extern GLuint materialDiffuseUniform;
extern GLuint materialSpecularUniform;
extern GLuint materialShinessUniform;

int trunk_no_of_surface = 20;
float trunk_top_circle_radius = 0.01f;
float trunk_bottom_circle_radius = 0.50f;
float trunk_height = 6.0f;

int size_of_vertices_array;

float *g_trunk_vertices;
float *g_trunk_normals;

GLuint g_trunk_vao;
GLuint g_trunk_vbo_vertex;
GLuint g_trunk_vbo_normal;

float g_trunk_material_ambient[4] = {0.460f, 0.359f, 0.281f, 1.0f};
float g_trunk_material_diffuse[4] = {0.460f, 0.359f, 0.281f, 1.0f};
float g_trunk_material_specular[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float g_trunk_material_shiness = 0.4f * 128;  // Also Try 128.0f

void Trunk_Initialize(void) {
  int number_of_vertices_in_circle = trunk_no_of_surface;
  int no_of_circles = 2;
  int no_of_vertices_in_surface =
      trunk_no_of_surface * NO_VERTICES_IN_QUAD;
  /*int*/ size_of_vertices_array =
      no_of_circles * number_of_vertices_in_circle * COORDINATE_SYSTEM_3D *
          sizeof(GLfloat) +
      no_of_vertices_in_surface * COORDINATE_SYSTEM_3D * sizeof(GLfloat);
  // int no_of_vertices_in_cricle = no_of_circles *
  // number_of_vertices_in_circle;

  g_trunk_vertices = (GLfloat *)malloc(size_of_vertices_array);
  g_trunk_normals = (GLfloat *)malloc(size_of_vertices_array);

  memset(g_trunk_vertices, 0, size_of_vertices_array);
  memset(g_trunk_normals, 0, size_of_vertices_array);

  getCylinederData(trunk_no_of_surface, trunk_top_circle_radius,
                   trunk_bottom_circle_radius, trunk_height,
                   &g_trunk_vertices, &g_trunk_normals);

  // cube
  glGenVertexArrays(1, &g_trunk_vao);
  glBindVertexArray(g_trunk_vao);

  // cube-vertex

  glGenBuffers(1, &g_trunk_vbo_vertex);
  glBindBuffer(GL_ARRAY_BUFFER, g_trunk_vbo_vertex);
  glBufferData(GL_ARRAY_BUFFER, size_of_vertices_array, NULL/*g_trunk_vertices*/,
               GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                        NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenBuffers(1, &g_trunk_vbo_normal);
  glBindBuffer(GL_ARRAY_BUFFER, g_trunk_vbo_normal);
  glBufferData(GL_ARRAY_BUFFER, size_of_vertices_array, NULL/*g_trunk_normals*/,
               GL_DYNAMIC_DRAW);
  glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindVertexArray(0);
}

void Trunk_Display(void) {
  glBindVertexArray(g_trunk_vao);

  int no_of_circles = 2;
  int number_of_vertices_in_circle = trunk_no_of_surface;

  glBindBuffer(GL_ARRAY_BUFFER, g_trunk_vbo_vertex);
  glBufferData(GL_ARRAY_BUFFER, size_of_vertices_array, g_trunk_vertices,
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, g_trunk_vbo_normal);
  glBufferData(GL_ARRAY_BUFFER, size_of_vertices_array, g_trunk_normals,
               GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glDrawArrays(GL_TRIANGLE_FAN, 0, number_of_vertices_in_circle);
  glDrawArrays(GL_TRIANGLE_FAN, number_of_vertices_in_circle,
               number_of_vertices_in_circle);

  for (int i = 0; i < number_of_vertices_in_circle; i += 1) {
    glDrawArrays(GL_TRIANGLE_FAN,
                 number_of_vertices_in_circle * no_of_circles + i * 4, 4);
  }

  glBindVertexArray(0);
}

void Trunk_Uniforms(void) {
  glUniform3fv(materialAmbientUniform, 1, g_trunk_material_ambient);
  glUniform3fv(materialDiffuseUniform, 1, g_trunk_material_diffuse);
  glUniform3fv(materialSpecularUniform, 1, g_trunk_material_specular);
  glUniform1f(materialShinessUniform, g_trunk_material_shiness);
}

void Trunk_Update(void)
{
  if (trunk_rotate > 360.0f)
  {
    trunk_rotate = 0.0f;
  }
  trunk_rotate += 0.5f;
}

void Trunk_Uninitialize(void) {
  if (g_trunk_vbo_normal) {
    glDeleteBuffers(1, &g_trunk_vbo_normal);
    g_trunk_vbo_normal = 0;
  }

  if (g_trunk_vbo_vertex) {
    glDeleteBuffers(1, &g_trunk_vbo_vertex);
    g_trunk_vbo_vertex = 0;
  }

  if (g_trunk_vao) {
    glDeleteVertexArrays(1, &g_trunk_vao);
    g_trunk_vao = 0;
  }

  if (g_trunk_normals) {
    free(g_trunk_normals);
  }

  if (g_trunk_vertices) {
    free(g_trunk_vertices);
  }
}