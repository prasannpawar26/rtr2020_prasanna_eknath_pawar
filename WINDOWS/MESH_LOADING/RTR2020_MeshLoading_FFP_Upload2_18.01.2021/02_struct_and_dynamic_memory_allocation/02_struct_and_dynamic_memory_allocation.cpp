#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Point3d
{
	float x;
	float y;
	float z;
};

int main(void)
{
	struct Point3d* p1 = NULL;
	float f1, f2, f3;

	p1 = (struct Point3d*)malloc(sizeof(struct Point3d));
	if (NULL == p1)
	{
		printf("Error\n");
		exit(-1);
	}

	p1->x = 1.1f;
	p1->y = 2.2f;
	p1->z = 3.3f;

	f1 = p1->x;
	f2 = p1->y;
	f3 = p1->z;

	printf("f1: %f\nf2: %f\nf3: %f\n\n", f1, f2, f3);

	free(p1);
	p1 = NULL;

	return 0;
}


