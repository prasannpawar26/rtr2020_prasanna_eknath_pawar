#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char line[] = "100,200,300";

int main(void)
{
	char* token = NULL;
	char* sep = ",";
	char* context = NULL;

	token = strtok_s(line,",", &context);
	printf("token = %s\n", token);

	token = strtok_s(NULL, sep, &context);
	printf("token = %s\n", token);

	token = strtok_s(NULL, sep, &context);
	printf("token = %s\n", token);

	return 0;
}

