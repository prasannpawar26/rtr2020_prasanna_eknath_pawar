#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int A[5];

void read_write_on_static_memory();
void allocate_read_write_free_dynamic_array();

int main(void)
{
	// Code
	read_write_on_static_memory();
	printf("\n\n");
	allocate_read_write_free_dynamic_array();

	return 0;
}

void read_write_on_static_memory()
{
	int i;
	
	for (i = 0; i < 5; i++)
	{
		A[i] = (i + 1) * 10;
	}

	for (i = 0; i < 5; i++)
	{
		printf("A[%d] = %d\n", i + 1, A[i]);
	}

	return;
}

void allocate_read_write_free_dynamic_array()
{
	/*
	Allocating memory block to hold array 5 of integers
	length of array = 5
	type of array element = 5
	sizeof type of array element = sizeof(int) = 4
	required size of array in bytes = length of array * sizeof type of array element = 5 * 4 = 20
	*/

	int *p = NULL;
	int i;
	int n;
	p = (int *)malloc(5 * sizeof(int));
	if (NULL == p)
	{
		printf("Insufficient memory\n");
		exit(-1);
	}

	// initializing all elements of array by 0
	memset(p, 0, 5 * sizeof(int));

	/*Write*/
	for (i = 0; i < 5; i++)
	{
		p[i] = (i + 1) * 100;
		*(p + i) = (i + 1) * 100;
	}

	/*Read*/
	for (i = 0; i < 5; i++)
	{
		n = p[i];
		printf("*(p + %d) : %d\n", i, *(p + i));
	}

	/*Free*/
	free(p);
	p = NULL;
}
