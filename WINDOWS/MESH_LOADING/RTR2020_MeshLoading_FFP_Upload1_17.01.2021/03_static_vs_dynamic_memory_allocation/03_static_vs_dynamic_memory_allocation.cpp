#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int n = 10;

void test(void);

int main()
{
	int m = 10; //Way 1 =  Memory Allocated to this integer when the function is called by function => dynamic time
	// not accessible outside the function

	test();

	return 0;
}

void test(void)
{
	int *p = NULL;

	// Way 2 = Dynamic allocation function : malloc, calloc, realloc
	// Lifetime of integer allocated by malloc is determined by the programmer
	// even if function returns the integer allocated by malloc will not be freed!!!!
	// Any function having pointer to this pointer can access this integer
	p = (int *)malloc(sizeof(int)); // 

	free(p);
	p = NULL;
}
