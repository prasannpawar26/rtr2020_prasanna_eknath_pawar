#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define SUCCESS 0

struct dynamic_array
{
	int size;
	int *p_data;
};

struct dynamic_array* create_dynamic_array(int size);
int set_dynamic_array_element(struct dynamic_array*, int index, int data);
int get_dynamic_array_element(struct dynamic_array*, int index, int* data);
void destroy_dynamic_array(struct dynamic_array*);

int main(void)
{
	// Code
	struct dynamic_array* p_arr = NULL;
	int status;
	int data;
	int i;

	p_arr = create_dynamic_array(10);

	/*Use*/
	status = set_dynamic_array_element(p_arr, 4, 100);
	assert(status == SUCCESS);

	status = get_dynamic_array_element(p_arr, 4, &data);
	assert(status == SUCCESS);

	for (i = 0; i < 10; i++)
	{
		status = set_dynamic_array_element(p_arr, i, (i + 1) * 100);
		assert(status == SUCCESS);
	}

	for (i = 0; i < 10; i++)
	{
		status = get_dynamic_array_element(p_arr, i, &data);
		assert(status == SUCCESS);
		printf("p_arr[%d] : %d\n", i, data);
	}

	/*destroy*/
	destroy_dynamic_array(p_arr);
	p_arr = NULL;

	return 0;
}


struct dynamic_array* create_dynamic_array(int size)
{
	struct dynamic_array* p_arr = NULL;

	p_arr = (struct dynamic_array *)malloc(sizeof(struct dynamic_array));
	memset(p_arr, 0, sizeof(struct dynamic_array));

	p_arr->size = size;

	p_arr->p_data = (int *)malloc( size * sizeof(int));
	memset(p_arr->p_data, 0,  size * sizeof(int));

	return p_arr;
}

int set_dynamic_array_element(struct dynamic_array*p_arr, int index, int data)
{
	if (NULL == p_arr)
	{
		return 1;
	}

	if (NULL == p_arr->p_data)
	{
		return 1;
	}

	if (p_arr->size <= index)
	{
		return 1;
	}

	p_arr->p_data[index] = data;

	return 0;
}

int get_dynamic_array_element(struct dynamic_array* p_arr, int index, int* data)
{
	if (NULL == p_arr)
	{
		return 1;
	}

	if (NULL == p_arr->p_data)
	{
		return 1;
	}

	if (p_arr->size <= index)
	{
		return 1;
	}

	*data = p_arr->p_data[index];

	return 0;
}

void destroy_dynamic_array(struct dynamic_array* p_arr)
{
	if (NULL != p_arr)
	{
		if (NULL != p_arr->p_data)
		{
			free(p_arr->p_data);
			p_arr->p_data = NULL;
		}

		free(p_arr);
		p_arr = NULL;
	}

	return;
}
