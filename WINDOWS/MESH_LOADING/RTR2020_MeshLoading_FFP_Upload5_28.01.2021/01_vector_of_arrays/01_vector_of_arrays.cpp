#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define SUCCESS 1
#define LINE_LENGTH 256

struct vector
{
	int** pp;
	int n;
};

struct vector* create_vector();
int push_back(struct vector* p_vec, int* p_arr);
void destroy_vector(struct vector* p_vec);

char line[LINE_LENGTH];

int main(int argc, char** argv)
{
	FILE* fp = NULL;

	struct vector* p_vec = NULL;

	char* token = NULL;

	char* sep = ",";

	int* arr = NULL;

	int i, j;

	assert(argc == 2);

	p_vec = create_vector();

	fp = fopen(argv[1], "r");

	assert(fp != NULL);

	while (fgets(line, LINE_LENGTH, fp))
	{
		arr = (int*)malloc(3 * sizeof(int));
		assert(arr != NULL);

		token = strtok(line, sep);
		arr[0] = atoi(token);

		token = strtok(NULL, sep);
		arr[1] = atoi(token);

		token = strtok(NULL, sep);
		arr[2] = atoi(token);

		push_back(p_vec, arr);
	}

	for (i = 0; i < p_vec->n; ++i)
	{
		printf("Printing array number %d\n", i + 1);

		for (j = 0; j < 3; ++j)
		{
			printf("arr[%d] : %d\n", j, p_vec->pp[i][j]);
		}
	}

	destroy_vector(p_vec);
	p_vec = NULL;

	fclose(fp);
	fp = NULL;

	return EXIT_SUCCESS;
}

struct vector* create_vector()
{
	struct vector* p_vec = NULL;
	p_vec = (struct vector*)malloc(sizeof(struct vector));
	assert(p_vec != NULL);
	p_vec->pp = NULL;
	p_vec->n = 0;

	return p_vec;
}

int push_back(struct vector* p_vec, int* p_arr)
{
	p_vec->pp = (int**)realloc(p_vec->pp, (p_vec->n + 1) * sizeof(int *));
	assert(p_vec->pp != NULL);
	p_vec->n = p_vec->n + 1;
	p_vec->pp[p_vec->n - 1] = p_arr;

	return SUCCESS;
}

void destroy_vector(struct vector* p_vec)
{
	int i;

	for (i = 0; i < p_vec->n; ++i)
	{
		free(p_vec->pp[i]);
	}

	free(p_vec->pp);
	free(p_vec);
}


