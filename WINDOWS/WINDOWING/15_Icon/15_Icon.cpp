#include <Windows.h>

#include "15_Icon.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  TCHAR pep_szAppName[] = TEXT("MyIcon");
  HWND pep_hwnd;
  MSG pep_msg;

  // Code
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.style = CS_VREDRAW | CS_HREDRAW;
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  pep_wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
  pep_wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

  RegisterClassEx(&pep_wndClass);

  pep_hwnd = CreateWindow(pep_szAppName, TEXT("MYICON"), WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                          CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_hwnd, iCmdShow);
  UpdateWindow(pep_hwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  switch (iMsg)
  {
    case WM_DESTROY:
    {
      PostQuitMessage(0);
    } break;

    default:
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

