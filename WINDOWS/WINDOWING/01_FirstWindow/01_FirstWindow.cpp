#include <windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  WNDCLASSEX wndClass;
  HWND hwnd;
  MSG msg;
  TCHAR szAppName[] = TEXT("MyApp");

  // Code
  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.style = CS_HREDRAW | CS_VREDRAW;
  wndClass.cbClsExtra = 0;
  wndClass.cbWndExtra = 0;
  wndClass.lpfnWndProc = WndProc;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszClassName = szAppName;
  wndClass.lpszMenuName = NULL;
  wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

  //Register Above Class
  RegisterClassEx(&wndClass);

  // Create Window
  hwnd = CreateWindow(szAppName,
    TEXT("PraannaPawar - FirstWindow"),
    WS_OVERLAPPEDWINDOW,
    CW_USEDEFAULT,
    CW_USEDEFAULT,
    CW_USEDEFAULT,
    CW_USEDEFAULT,
    NULL,
    NULL,
    hInstance,
    NULL);

  ShowWindow(hwnd, iCmdShow);
  UpdateWindow(hwnd);

  // Message Loop
  while (GetMessage(&msg, NULL, 0, 0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

  switch (iMsg)
  {
    case WM_CLOSE:
    {
      MessageBox(hwnd, TEXT("WM_CLOSE"), TEXT("MyMessage"), MB_OK);
    }break;

    case WM_KEYDOWN:
    {
      MessageBox(hwnd, TEXT("WM_KEYDOWN"), TEXT("MyMessage"), MB_OK);
    } break;

    case WM_CHAR:
    {
      MessageBox(hwnd, TEXT("WM_CHAR"), TEXT("MyMessage"), MB_OK);
    } break;

    case WM_DESTROY:
    {
      MessageBox(hwnd, TEXT("WM_DESTROY"), TEXT("MyMessage"), MB_OK);
      PostQuitMessage(0);
    }
    break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

