#include <windows.h>

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  HWND pep_hwnd;
  MSG pep_msg;
  TCHAR pep_szAppName[] = TEXT("WinCenter");

  int pep_width_of_screen = 0;
  int pep_height_of_screen = 0;
  int window_x_coordinates;
  int window_y_coordinates;

  // Code
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.style = CS_VREDRAW | CS_HREDRAW;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

  RegisterClassEx(&pep_wndClass);

  pep_width_of_screen = GetSystemMetrics(SM_CXSCREEN);
  pep_height_of_screen = GetSystemMetrics(SM_CYSCREEN);

  window_x_coordinates = (pep_width_of_screen / 2) - (PEP_WIDTH / 2);
  window_y_coordinates = (pep_height_of_screen / 2) - (PEP_HEIGHT / 2);

  pep_hwnd =
      CreateWindow(pep_szAppName, TEXT("WindowAtCenter"), WS_OVERLAPPEDWINDOW,
                   window_x_coordinates, window_y_coordinates,
                          PEP_WIDTH, PEP_HEIGHT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_hwnd, iCmdShow);
  UpdateWindow(pep_hwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  switch (iMsg)
  {
    case WM_DESTROY:
      PostQuitMessage(0);
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}
