#include <windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  HWND pep_hwnd;
  MSG pep_msg;
  TCHAR pep_szAppName[] = TEXT("EventHandling");

  // Code
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.style = CS_HREDRAW | CS_VREDRAW;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

  // Register
  RegisterClassEx(&pep_wndClass);

  // CreateWindow
  pep_hwnd = CreateWindow(
    pep_szAppName,
    TEXT("Prasanna Pawar - Event Handling"),
    WS_OVERLAPPEDWINDOW,
    CW_USEDEFAULT, // X- Coordinates
    CW_USEDEFAULT, // Y - Coordinates
    CW_USEDEFAULT, // Width
    CW_USEDEFAULT, // Height
    NULL,
    NULL,
    hInstance,
    NULL);

  ShowWindow(pep_hwnd, iCmdShow);
  UpdateWindow(pep_hwnd);

  // Message Loop / Game Loop
  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);  // Internally Calls WndProc
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // Code
  switch (iMsg)
  { 
      case WM_DESTROY:
        PostQuitMessage(0); // INternally Post WM_QUIT Message
        break;

      case WM_CREATE:
        MessageBox(hwnd, TEXT("WM_CREATE Recevied"), TEXT("MyMessage"), MB_OK);
        break;

      case WM_LBUTTONDOWN:
        MessageBox(hwnd, TEXT("WM_LBUTTONDOWN Recevied"), TEXT("MyMessage"), MB_OK);
        break;

      case WM_KEYDOWN:
        MessageBox(hwnd, TEXT("WM_KEYDOWN Recevied"), TEXT("MyMessage"), MB_OK);
        break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

