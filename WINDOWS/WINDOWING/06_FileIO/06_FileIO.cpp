#include <windows.h>
#include <stdio.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gp_File;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstancem, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  HWND pep_hwnd;
  MSG pep_msg;
  TCHAR pep_szAppName[] = TEXT("MyApp");

  // Code
  if (0 != fopen_s(&gp_File, "Log.txt", "w"))
  {
    MessageBox(NULL, TEXT("Log File Creation/Opening Failed"), TEXT("MyMessage"), MB_ICONINFORMATION);
    exit(0);
  }

  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  pep_wndClass.style = CS_VREDRAW | CS_HREDRAW;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

  RegisterClassEx(&pep_wndClass);

  pep_hwnd = CreateWindow(pep_szAppName, TEXT("FileIO"), WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                          CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_hwnd, iCmdShow);
  UpdateWindow(pep_hwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  fclose(gp_File);

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  switch (iMsg)
  {
    case WM_CREATE:
      fprintf(gp_File, "India Is My Country\n");
      break;

    case WM_DESTROY:
      fprintf(gp_File, "Jay Hind\n");
      PostQuitMessage(0);
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}
