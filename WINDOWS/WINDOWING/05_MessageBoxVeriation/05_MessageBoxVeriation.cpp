#include <windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  HWND pep_hwnd;
  MSG pep_msg;
  TCHAR pep_szAppName[] = TEXT("MyApp");

  // Code
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.style = CS_HREDRAW | CS_VREDRAW;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

  RegisterClassEx(&pep_wndClass);

  pep_hwnd =
      CreateWindow(pep_szAppName, TEXT("MessageBoxVeriations"),
                   WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
                   CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_hwnd, iCmdShow);
  UpdateWindow(pep_hwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  switch (iMsg)
  {
    case WM_DESTROY:
      MessageBox(hwnd, TEXT("Using MB_ICONINFORMATION"), TEXT("MyMessage"),
                 MB_ICONINFORMATION | MB_OK);
      PostQuitMessage(0);
      break;

    case WM_CREATE:
      MessageBox(hwnd, TEXT("Using MB_TOPMOST"), TEXT("MyMessage"),
                 MB_TOPMOST | MB_OK);
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}
