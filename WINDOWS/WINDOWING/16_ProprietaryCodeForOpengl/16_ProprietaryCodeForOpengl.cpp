#include <Windows.h>
#include <stdio.h>

#include "16_ProprietaryCodeForOpengl.h"

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

// Global Function Delcarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global Variable Declarations
WINDOWPLACEMENT pep_wpPrev = {sizeof(WINDOWPLACEMENT)};
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;
bool pep_gbActiveWindow = false;

FILE *gp_File;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {

  // Function Declarations
  void Initialize(void);
  void Display(void);

  // Variable Declarations
  bool bDone = false;
  WNDCLASSEX pep_wndClass;
  TCHAR pep_szAppName[] = TEXT("ProprietaryCodeForOpengl");
  MSG pep_msg;
  HWND pep_Hwnd;

  // Code
  if (0 != fopen_s(&gp_File, "Log.txt", "w")) {
    MessageBox(NULL, TEXT("Log File Creation/Opening Failed"),
               TEXT("MyMessage"), MB_ICONINFORMATION);
    exit(0);
  }

  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
  pep_wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

  RegisterClassEx(&pep_wndClass);

  pep_Hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, pep_szAppName, TEXT("ProprietaryCodeForOpengl"),
      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
      100 /*(GetSystemMetrics(SM_CXSCREEN) / 2) - (PEP_WIDTH / 2)*/,
      100 /*(GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2)*/, PEP_WIDTH,
      PEP_HEIGHT, NULL, NULL, hInstance, NULL);
  pep_gHwnd = pep_Hwnd;
  Initialize();

  ShowWindow(pep_Hwnd, iCmdShow);
  SetForegroundWindow(pep_Hwnd);
  SetFocus(pep_Hwnd);
  UpdateWindow(pep_Hwnd);

  while (bDone == false)
  {
    if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
    {
      if (pep_msg.message == WM_QUIT) {
        bDone = true;
      } else {
        TranslateMessage(&pep_msg);
        DispatchMessage(&pep_msg);
      }
    }
    else
    {
      if (pep_gbActiveWindow == true)
      {
        // Here You Should Call Update() For OpenGL Rendeing
        // Here You Should Call Display() For OpenGL Rendeing
        Display();
      }
    }
  } // While

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // Function Declaration
  void ToggleFullScreen(void);
  void UnInitialize(void);
  void ReSize(int, int);

  switch (iMsg) {
    case WM_DESTROY: {
      UnInitialize();
      PostQuitMessage(0);
    } break;

    case WM_SETFOCUS:
      pep_gbActiveWindow = true;
      break;

    case WM_KILLFOCUS:
      pep_gbActiveWindow = false;
      break;

    case WM_SIZE:
      ReSize(LOWORD(lParam), HIWORD(lParam));
      break;

    case WM_ERASEBKGND:
      return 0;
      break;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;

    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 0x46:
        case 0x66: {
          ToggleFullScreen();
        } break;
      }
    } break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
  // Variable Declarations
  MONITORINFO pep_mi = {sizeof(MONITORINFO)};

  if (false == pep_gbFullScreen)
  {
    pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
    {
      if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &pep_mi))
      {
        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

        SetWindowPos(pep_gHwnd, HWND_TOP, pep_mi.rcMonitor.left,
                      pep_mi.rcMonitor.top,
                      pep_mi.rcMonitor.right - pep_mi.rcMonitor.left,
                      pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top,
                      SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);

    pep_gbFullScreen = true;
  }
  else
  {
    SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

    SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

    SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

    ShowCursor(TRUE);

    pep_gbFullScreen = false;
  }

  return;
}

void Initialize(void)
{
  // Function Declarations
  void ReSize(int, int);

  // Variable Declarations

  // Code
  ReSize(PEP_WIDTH, PEP_HEIGHT);

  return;
}

void ReSize(int width, int height)
{
  // Code
  if (0 == height)
  {
    height = 1;
  }

  return;
}

void Display(void)
{
  // Code
}

void UnInitialize(void)
{
  // Code
  if (gp_File)
  {
    fclose(gp_File);
    gp_File = NULL;
  }

  return;
}
