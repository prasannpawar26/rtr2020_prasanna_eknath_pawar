#include <Windows.h>

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

// Global Function Delcarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global Variable Declarations
WINDOWPLACEMENT pep_wpPrev = {sizeof(WINDOWPLACEMENT)};
DWORD pep_dwStyle;
HWND pep_gHwnd;
bool pep_gbFullScreen = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  TCHAR pep_szAppName[] = TEXT("FullScreen");
  MSG pep_msg;

  // Code
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.style = CS_HREDRAW | CS_VREDRAW;
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

  RegisterClassEx(&pep_wndClass);

  pep_gHwnd = CreateWindow(
    pep_szAppName,
    TEXT("FullScreen"),
    WS_OVERLAPPEDWINDOW,
    (GetSystemMetrics(SM_CXSCREEN)/2) - (PEP_WIDTH /2),
    (GetSystemMetrics(SM_CYSCREEN) / 2) - (PEP_HEIGHT / 2),
    PEP_WIDTH,
    PEP_HEIGHT,
    NULL,
    NULL,
    hInstance,
    NULL
  );

  ShowWindow(pep_gHwnd, iCmdShow);
  UpdateWindow(pep_gHwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // Function Declaration
  void ToggleFullScreen(void);

  switch (iMsg)
  {
    case WM_DESTROY:
    {
      PostQuitMessage(0);
    }
      break;

    case WM_KEYDOWN:
    {
      switch (wParam)
      {
        case 0x46:
        case 0x66:
        {
          ToggleFullScreen();
        } break;
      }
    }
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
  // Variable Declarations
  MONITORINFO pep_mi = {sizeof(MONITORINFO)};

  if (false == pep_gbFullScreen)
  {
    pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
    {
      if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
          GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY),
                         &pep_mi))
      {
        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);

        SetWindowPos(pep_gHwnd,
          HWND_TOP,
          pep_mi.rcMonitor.left,
          pep_mi.rcMonitor.top,
          pep_mi.rcMonitor.right - pep_mi.rcMonitor.left, 
          pep_mi.rcMonitor.bottom - pep_mi.rcMonitor.top,
          SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);

    pep_gbFullScreen = true;
  }
  else
  {
    SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_EX_OVERLAPPEDWINDOW);

    SetWindowPlacement(pep_gHwnd, &pep_wpPrev);

    SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                     SWP_NOOWNERZORDER);

    ShowCursor(TRUE);

    pep_gbFullScreen = false;
  }

  return;
}
