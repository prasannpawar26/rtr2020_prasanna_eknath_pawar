#include <Windows.h>
#include <stdio.h>

// MACRO's
#define PEP_WIDTH 800
#define PEP_HEIGHT 600

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DISPLAY_DEVICE pep_gDisplayDevice = {0};
DEVMODE pep_gDevNode = {0};
DWORD pep_dwStyle = 0;
bool pep_gbFullScreen = false;
HWND pep_gHwnd = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {

  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  MSG pep_msg;
  TCHAR pep_szAppName[] = TEXT("FullScreen");
  int pep_width_of_screen = 0;
  int pep_height_of_screen = 0;
  int window_x_coordinates;
  int window_y_coordinates;
  // Code

  // Registrations
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.style = CS_VREDRAW | CS_HREDRAW;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

  RegisterClassEx(&pep_wndClass);

  pep_width_of_screen = GetSystemMetrics(SM_CXSCREEN);
  pep_height_of_screen = GetSystemMetrics(SM_CYSCREEN);

  window_x_coordinates = 0;  //(pep_width_of_screen / 2) - (PEP_WIDTH / 2);
  window_y_coordinates = 0;
  //(pep_height_of_screen / 2) - (PEP_HEIGHT / 2);

  pep_gHwnd =
      CreateWindow(pep_szAppName, TEXT("FullScreen"), WS_OVERLAPPEDWINDOW,
                   window_x_coordinates, window_y_coordinates, PEP_WIDTH,
                   PEP_HEIGHT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_gHwnd, iCmdShow);
  UpdateWindow(pep_gHwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0)) {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // Function Declarations
  void ToggleFullScreen(void);

  // Code
  switch (iMsg)
  {
    case WM_DESTROY:
      PostQuitMessage(0);
      break;

    case WM_KEYDOWN:
    {
      switch (wParam)
      {
        case VK_ESCAPE:
        {
          if (pep_gbFullScreen)
          {
            if (DISP_CHANGE_SUCCESSFUL !=
                ChangeDisplaySettingsEx(pep_gDisplayDevice.DeviceName, NULL,
                                        NULL, 0, NULL)) {
              MessageBox(pep_gHwnd, TEXT("ChangeDisplaySettingsEx Failed"),
                         TEXT("MyMessage"), MB_OK);
            }
          }

          DestroyWindow(pep_gHwnd);
        } break;

        case 'F':
        case 'f':
        {
          ToggleFullScreen();
        } break;

        default:
          break;
      }

    } break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
  // Variable Declarations
  DISPLAY_DEVICE pep_displayDevice;
  DEVMODE pep_devMode;
  DWORD pep_dwIndex;
  bool pep_bIsModeFound = false;

  if (false == pep_gbFullScreen) {
    pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & pep_dwStyle) {

      ZeroMemory(&pep_displayDevice, sizeof(DISPLAY_DEVICE));
      pep_displayDevice.cb = sizeof(DISPLAY_DEVICE);
      pep_dwIndex = 0;

      while (EnumDisplayDevices(NULL, pep_dwIndex, &pep_displayDevice,
                                EDD_GET_DEVICE_INTERFACE_NAME)) {
        if (!(pep_displayDevice.StateFlags &
              DISPLAY_DEVICE_ATTACHED_TO_DESKTOP) &&
            !(pep_displayDevice.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE)) {
          ++pep_dwIndex;
          continue;
        }

        pep_gDisplayDevice = pep_displayDevice;

        break;
      }

      ZeroMemory(&pep_devMode, sizeof(DEVMODE));
      pep_devMode.dmSize = sizeof(DEVMODE);

      pep_dwIndex = 0;

      while (!pep_bIsModeFound &&
             EnumDisplaySettings(pep_displayDevice.DeviceName, pep_dwIndex,
                                 &pep_devMode)) {
        if (PEP_WIDTH == pep_devMode.dmPelsWidth &&
            PEP_HEIGHT == pep_devMode.dmPelsHeight) {
          pep_gDevNode = pep_devMode;
          pep_bIsModeFound = TRUE;
          break;
        }

        ++pep_dwIndex;
      } // End Of While

      if (pep_bIsModeFound) {
        if (DISP_CHANGE_SUCCESSFUL !=
            ChangeDisplaySettingsEx(pep_gDisplayDevice.DeviceName,
                                    &pep_gDevNode, NULL, CDS_FULLSCREEN,
                                    NULL)) {
          MessageBox(pep_gHwnd, TEXT("ChangeDisplaySettingsEx Failed"),
                     TEXT("MyMessage"), MB_OK);

          return;
        }
      }

      SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);
    }

    ShowCursor(FALSE);
    pep_gbFullScreen = true;
  } else {
    CloseWindow(pep_gHwnd);

    if (DISP_CHANGE_SUCCESSFUL !=
        ChangeDisplaySettingsEx(pep_gDisplayDevice.DeviceName, NULL, NULL, 0,
                                NULL)) {
      MessageBox(pep_gHwnd, TEXT("ChangeDisplaySettingsEx Failed"),
                 TEXT("MyMessage"), MB_OK);

      return;
    }

    SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);

    ShowWindow(pep_gHwnd, SW_RESTORE);

    ShowCursor(TRUE);
    pep_gbFullScreen = false;
  }

  return;
}
