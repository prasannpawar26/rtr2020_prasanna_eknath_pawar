#include <windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
  // Variable Declarations
  HWND pep_hwnd;
  TCHAR pep_szAppName[] = TEXT("SpecialDC");
  MSG pep_msg;
  WNDCLASSEX pep_wndClass;

  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.lpszClassName = pep_szAppName;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.style = CS_VREDRAW | CS_HREDRAW;    
  pep_wndClass.lpszMenuName = NULL;

  RegisterClassEx(&pep_wndClass);

  pep_hwnd = CreateWindow(pep_szAppName, TEXT("SpecialDC"), WS_OVERLAPPEDWINDOW,
                          CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                          CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_hwnd, iCmdShow);
  UpdateWindow(pep_hwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  // Variable Declarations
  RECT pep_rect;
  HDC pep_hdc;
  PAINTSTRUCT pStruct;
  TCHAR pep_str[] = TEXT("Hello World !!!");

  switch (iMsg)
  {
    case WM_PAINT:
    {
      GetClientRect(hwnd, &pep_rect);

      pep_hdc = BeginPaint(hwnd, &pStruct);

      SetBkColor(pep_hdc, RGB(0, 0, 0));

      SetTextColor(pep_hdc, RGB(0, 255, 0));

      DrawText(pep_hdc, pep_str, -1, &pep_rect,
               DT_SINGLELINE | DT_CENTER | DT_VCENTER);

      EndPaint(hwnd, &pStruct);
    } break;

    case WM_DESTROY:
      PostQuitMessage(0);
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}