#include <windows.h>

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdLine)
{
  // Variable Declarations
  WNDCLASSEX pep_wndClass;
  HWND pep_hwnd;
  TCHAR pep_szAppName[] = TEXT("WinCenter");
  MSG pep_msg;
  int iXPos;
  int iYPos;

  RECT rect;
  SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);
  iXPos = (rect.right - rect.left) /2 - (PEP_WIDTH /2);
  iYPos = (rect.bottom - rect.top) / 2 - (PEP_WIDTH / 2);
  //Code
  pep_wndClass.cbSize = sizeof(WNDCLASSEX);
  pep_wndClass.cbClsExtra = 0;
  pep_wndClass.cbWndExtra = 0;
  pep_wndClass.hInstance = hInstance;
  pep_wndClass.lpfnWndProc = WndProc;
  pep_wndClass.style = CS_VREDRAW | CS_HREDRAW;
  pep_wndClass.lpszMenuName = NULL;
  pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
  pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  pep_wndClass.lpszClassName = pep_szAppName;

  RegisterClassEx(&pep_wndClass);

  pep_hwnd = CreateWindow(pep_szAppName, TEXT("Window Center"),
                          WS_OVERLAPPEDWINDOW, iXPos, iYPos, PEP_WIDTH,
                          PEP_HEIGHT, NULL, NULL, hInstance, NULL);

  ShowWindow(pep_hwnd, iCmdLine);
  UpdateWindow(pep_hwnd);

  while (GetMessage(&pep_msg, NULL, 0, 0))
  {
    TranslateMessage(&pep_msg);
    DispatchMessage(&pep_msg);
  }

  return (int)pep_msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
  switch (iMsg)
  {
    //Cases
    case WM_DESTROY:
      PostQuitMessage(0);
      break;
  }

  return DefWindowProc(hwnd, iMsg, wParam, lParam);
}
