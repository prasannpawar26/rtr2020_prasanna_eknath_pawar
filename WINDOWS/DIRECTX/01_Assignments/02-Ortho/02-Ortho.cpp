#include <windows.h>
#include <stdio.h>
// DirectX specific header file
#include <d3d11.h>
#include <d3dcompiler.h> // For shader compilation

#pragma warning (disable: 4838)
#include "XNAMath/xnamath.h" // This file is included in all other .inl files

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool pep_gbActiveWindow = false;
bool pep_gbFullScreen = false;
bool pep_gbEscapeKeyPressed = false;

float pep_gClearColor[4]; // RGBA  => Similar To  glClear

// Below 3 Methods Are Compulsary For Interface
//	AddRef
//	Release
//	QueryInterface

IDXGISwapChain *pep_gpIDXGISwapChain = NULL; // storing (i.e buffers front and back) rendered data before presenting it to an output
ID3D11Device *pep_gpID3D11Device = NULL; // a virtual adapter; it is used to create resources
ID3D11DeviceContext *pep_gpID3D11DeviceContext = NULL; // Similar To wglDeviceContext, generates rendering commands
ID3D11RenderTargetView *pep_gpID3D11RenderTargetView = NULL; // Similar To Frame Buffer, MTR(Mutiple Target Render) 

//
// Shader Related Variables
//
ID3D11VertexShader *pep_gpID3D11VertexShader = NULL;
ID3D11PixelShader *pep_gpID3D11PixelShader = NULL;
ID3D11Buffer *pep_gpID3D11Buffer_VertexBuffer = NULL;
ID3D11Buffer *pep_gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11InputLayout *pep_gpID3D11InputLayout = NULL;

//
//Similar To  Uniforms
//
struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;   // (Direct)world = model(OpenGL)
};

XMMATRIX pep_gorthographicProjectionMatrix; // (DirectX: XNAMath)XMATRIX = MATH(OpenGL:vmath)
//XMMATRIX Internally not float4X4 but it is mapped to float4X4

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("DirectX- Ortho");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.lpfnWndProc = WndProc;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("DirectX- Ortho"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;

	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Game Loop
	//
	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapeKeyPressed)
				{
					pep_bDone = true;
				}
			}

			Display();
		}
	}

	//
	// Call Uninitialize
	//
	Uninitialize();

	return ((int)pep_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Code
	//

	switch (iMsg)
	{
	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		//
		// Call Resize Only When We Have Device Context
		//
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr); // HRESULT -> Get Upcasted Here To LONG.
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapeKeyPressed)
			{
				pep_gbEscapeKeyPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	return;
}


HRESULT Initialize(void)
{
	//
	// Function Declarations
	//
	void Uninitialize(void);
	HRESULT Resize(int, int);

	//
	// Variable Declarations
	//
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	//D3D_DRIVER_TYPE_HARDWARE -> Hardware Rendering
	// D3D_DRIVER_TYPE_REFERENCE -> Software Rendering
	D3D_DRIVER_TYPE d3dDriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_SOFTWARE};

	// Below Two Statements Are Equivalent To glew Of OpenGL
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
	// (DirectX)Feature Level = Extension(OpenGL)

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//
	// Code
	//
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	//
	// Swap Chain Descriptor
	//
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc; 
	memset((void *)& dxgiSwapChainDesc, 0, sizeof(DXGI_SWAP_CHAIN_DESC));

	//
	// STEP 1 : Craeate Back Buffer, Device Context, Swap Chain
	//
	//
	// Few Function Of OpenGL Are Replaced By 'dxgiSwapChainDesc' like
	// GetDC
	// PixelFormatDecriptor
	// ChoosePixelFormat
	// SetPixelFormat
	// wglCreateContext
	// wglMakeCurrent
	// glewInit 
	//
	dxgiSwapChainDesc.BufferCount = 1; // DirectX Gives 1 Buffer By Default (i.e Front Buffer), Hence We Only Required/Create One Extra Buffer(i.e Back Buffer) => It Means Total 2 Buffers
	dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH; // Back Buffer Width
	dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT; // Back Buffer Height
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //  Similar To PixelChangeDescriptor In OpenGL
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; // FPS in OpenGL
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // Buffer Type Or Used For Render Target Output
	dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0; // Default
	dxgiSwapChainDesc.Windowed = TRUE; // Windowing + FullScreen

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		//
		// After Successful Call Of D3D11CreateDeviceAndSwapChain Function We Will Have Following
		// Creates a device that represents the display adapter
		// 1. DeviceContext
		// 2. Back Buffer
		// 3. Device
		// 4. Swap Chain

		hr = D3D11CreateDeviceAndSwapChain(
			NULL, // Required When Mutiple Graphics Card (We Need Primary)
			d3dDriverType,
			NULL, // No Software rasterizer hence NULL
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&pep_gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Shader Code
	//

	// No Build In Variables In DirectX

	/*
	// cbuffer -> keyword
	// cbuffer mapped with CBUFFER
	// worldViewProjectionMatrix mapped with WorldViewProjectionMatrix
	// Similar To Uniform  => cbuffer ConstantBuffer

	"cbuffer ConstantBuffer" \
	"{" \
	"float4x4 worldViewProjectionMatrix;" \
	"}" \

	// (DirectX)float4 main() = void main() (OpenGL)
	// In opengl IN act as input parameter and OUt at as return value
	// In DirectX main function has input parameters and return value 
	// (DirectX)float pos: POSITION = gl_Position(OpenGL)
	// (DirextX)POSITION = AMC_ATTRIBUTE_POSITION (OpenGL)
	// (DirectX) pos = vPosition(OpenGL)
	// SV_POSITION Is Only HLSL(High Level Shading Language) Keyword

	"float4 main(float pos: POSITION) : SV_POSITION" \
	"{" \
	"float4 position = mul(worldViewProjectionMatrix, pos)" \
	"return (position);" \
	"}";

	*/

	//
	// Shader Related Calls
	// 1. Shader Source Code
	// 2. Compilation -> D3DCompile
	// 3. Create Shader -> Create***Shader (e.g CreateVertexShader)
	// 4. Attach -> **SetShader (e.g VSSetShader)
	//

	//
	// STEP 2 : Write Shader
	//

	const char *pep_vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix;" \
		"}" \

		"float4 main(float4 pos: POSITION) : SV_POSITION" \
		"{" \
		"float4 position = mul(worldViewProjectionMatrix, pos);" \
		"return (position);" \
		"}";

	ID3DBlob *pID3DBlob_vertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	//
	// STEP 3 : Compile Shader
	//

	//
	// In DirectX 3D All Shaders Called As "Effects".
	// In DirectX 3D Shaders Can Be Compile Offline As Well Using FXC.exe
	// FX In FXC Stands For Effects
	// Nothing Similar To FXC.exe In OpenGL
	// D3DCompileFormFile -> For File Base Shader(s)
	//
	// (DirectX)D3DCompile = glCompileShader (OpenGL)
	//
	hr = D3DCompile(
		pep_vertexShaderSourceCode,
		lstrlenA(pep_vertexShaderSourceCode) + 1, // +1 For nul char and shader in ASCII char. DirectX Requires Length Including nul character
		"VS", // VS => Vertex Shader
		NULL, // Not Used #define Hence NULL
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // Default , If #define Was There the It Could Have Been Different
		"main", // Entry Point Of Shader Program
		"vs_5_0", // Version Of Shader language
		0, //  How To Compile Shader Program . Zero Means Use Default
		0, // Shader Do Not Have "effect constant" Hence Zero
		&pID3DBlob_vertexShaderCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (NULL != pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			// blob data is in binary format. do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	//
	// STEP 4 : Create Shader Object
	//

	//
	// (DirextX) CreateVertexShader = glCreateShader(OpenGL)
	//
	hr = pep_gpID3D11Device->CreateVertexShader(
		pID3DBlob_vertexShaderCode->GetBufferPointer(),
		pID3DBlob_vertexShaderCode->GetBufferSize(),
		NULL, // If We Are Using Any (Class Linkage similar to extern) Variable Across Shader Then Mention Them Here. As In This shader We Do Not Have Such Variable Hence NULL
		&pep_gpID3D11VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		// blob data is in binary format. do convert it in char * we are using GetBufferPointer
		fprintf_s(pep_gpFile, "CreateVertexShader Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateVertexShader Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// STEP 5 : Attach/Plug Shader
	//

	//
	// Set Vertex Shader In Pipeline
	//
	// (DirectX) VSSetShader = glAttachShader(OpenGL)
	//
	pep_gpID3D11DeviceContext->VSSetShader(
		pep_gpID3D11VertexShader,
		0, // Shader Variable Array (Class Linkage Variables). Here In This Shader We Do Not Such Variable Hence Zero Or NULL. // Use NULL Instead Of Zero
		0 // Use NULL Instead Of Zero
	);

	ID3DBlob *pID3DBlob_pixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	//
	// (DirextX)SV_TARGET = Out (OpenGL)
	// (DirectX)return = Out (OpenGL)
	//
	// Similar To FragColor  -> "return (float4(1.0f, 1.0f, 1.0f, 1.0f));
	//
	const char *pep_pixelShaderSourceCode =
		"float4 main(void) : SV_TARGET" \
		"{" \
		"return (float4(1.0f, 1.0f, 1.0f, 1.0f));" \
		"}";

	//
	// In DirectX 3D All Shaders Called As "Effects".
	// In DirectX 3D Shaders Can Be Compile Offline As Well Using FXC.exe
	// FX In FXC Stands For Effects
	// Nothing Similar To FXC.exe In OpenGL
	// D3DCompileFormFile -> For File Base Shader(s)
	//
	// (DirectX)D3DCompile = glCompileShader (OpenGL)
	//
	hr = D3DCompile(
		pep_pixelShaderSourceCode,
		lstrlenA(pep_pixelShaderSourceCode) + 1, // +1 For nul char and shader in ASCII char. DirectX Requires Length Including nul character
		"PS", // PS => Pixel Shader
		NULL, // Not Used #define Hence NULL
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // Default , If #define Was There the It Could Have Been Different
		"main", // Entry Point Of Shader Program
		"ps_5_0", // Version Of Shader language
		0, //  How To Compile Shader Program . Zero Means Use Default
		0, // Shader Do Not Have "effect constant" Hence Zero
		&pID3DBlob_pixelShaderCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (NULL != pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			// blob data is in binary format. do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	//
	// (DirextX) CreatePixelShader = glCreateShader(OpenGL)
	//
	hr = pep_gpID3D11Device->CreatePixelShader(
		pID3DBlob_pixelShaderCode->GetBufferPointer(),
		pID3DBlob_pixelShaderCode->GetBufferSize(),
		NULL,  // If We Are Using Any (Class Linkage similar to extern) Variable Across Shader Then Mention Them Here. As In This shader We Do Not Have Such Variable Hence NULL
		&pep_gpID3D11PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreatePixelShader Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreatePixelShader Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Set Pixel Shader In Pipeline
	//
	// (DirectX) PSSetShader = glAttachShader(OpenGL)
	//
	pep_gpID3D11DeviceContext->PSSetShader(
		pep_gpID3D11PixelShader,
		0, // Shader Variable Array (Class Linkage Variables). Here In This Shader We Do Not Such Variable Hence Zero Or NULL. // Use NULL Instead Of Zero
		0 // Use NULL Instead Of Zero
	);

	//
	// InputLayout Related Code (Mapping OpenGL Code Hence Here)
	//
	D3D11_INPUT_ELEMENT_DESC pep_inputElementDesc;
	ZeroMemory(&pep_inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	//
	// STEP 6 : Create Mapping Of CPU And GPU Global Variable For Data Transfer
	//

	//
	// Per Vertex Data
	// 1. Position
	// 2. Color
	// 3. TexCoord
	// 4. Normal
	//
	// Here We Are Only Using Vertex Position
	//
	pep_inputElementDesc.SemanticName = "POSITION"; // Map With Vertex Shader "POSITION"
	pep_inputElementDesc.SemanticIndex = 0;
	pep_inputElementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT; // Specify The Components In "Vertex Position" Here It Is 3. -> 2nd Parameter Of glVertexAttribPointer
	pep_inputElementDesc.InputSlot = 0; // (DirectX)InputSlot = layout(OpenGL)  (i.e AMC_ATTRIBUTES_POSITION)
	pep_inputElementDesc.AlignedByteOffset = 0; //
	pep_inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_inputElementDesc.InstanceDataStepRate = 0;

	//
	// Similar To  glBindAttribLocation(OpenGL)
	//
	hr = pep_gpID3D11Device->CreateInputLayout(
		&pep_inputElementDesc, // 1st Parameter Of glBufferData
		1,
		pID3DBlob_vertexShaderCode->GetBufferPointer(),
		pID3DBlob_vertexShaderCode->GetBufferSize(),
		&pep_gpID3D11InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateInputLayout Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateInputLayout Successful.\n");
		fclose(pep_gpFile);
	}

	// (DirectX) IASetInputLayout = glBindAttribLocation (OpenGL)
	pep_gpID3D11DeviceContext->IASetInputLayout(pep_gpID3D11InputLayout); // IA = Input Assembly Stage

	pID3DBlob_pixelShaderCode->Release();
	pID3DBlob_pixelShaderCode = NULL;
	pID3DBlob_vertexShaderCode->Release();
	pID3DBlob_vertexShaderCode = NULL;

	//
	// Clockwise
	//
	float vertices[] =
	{
		0.0f, 50.0f, 0.0f, // apex
		50.0f, -50.0f, 0.0f, // right
		-50.0f, -50.0f, 0.0f //left
	};


	//
	// STEP 7 : Copy Vertex Data From CPU And GPU.
	//

	//
	// Create Vertex Buffer
	//
	D3D11_BUFFER_DESC pep_bufferDesc_VertexBuffer; // Vertex Buffer
	ZeroMemory(&pep_bufferDesc_VertexBuffer, sizeof(D3D11_BUFFER_DESC));

	// D3D11_BIND_VERTEX_BUFFER = Bind a buffer as a vertex buffer to the input-assembler stage
	pep_bufferDesc_VertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_VertexBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices);
	pep_bufferDesc_VertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER; // 1st Parameter Of glVertexAttribPointer i.e AMC_ATTRIBUTE_POSITION or AMC_ATTRIBUTE_NORMAL etc...
	pep_bufferDesc_VertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; // Similar To glBufferData . If Not Specify Then Vertices Will Not Pass From CPU To GPU

	//
	// SetBuffer Of Below Is Done In Draw Because We Are Using Like DYNAMIC DRAW
	// i.e Vertex Data Is Pass At RunTime Hence In Draw/Display
	//
	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexBuffer,
		NULL, // Passing NULL Means DYNAMIC_DRAW
		&pep_gpID3D11Buffer_VertexBuffer // Vertex Buffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Copy Vertex Position Data Into pep_gpID3D11Buffer_VertexBuffer
	//

	D3D11_MAPPED_SUBRESOURCE pep_mappedSubresource; // Display Driver Does CPU To GPU Memory Mapping
	ZeroMemory(&pep_mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	pep_gpID3D11DeviceContext->Map(
		pep_gpID3D11Buffer_VertexBuffer,
		NULL, // Zero 0
		D3D11_MAP_WRITE_DISCARD,
		NULL, // Zero 0
		&pep_mappedSubresource
	);

	memcpy(
		pep_mappedSubresource.pData,
		vertices,
		sizeof(vertices)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpID3D11Buffer_VertexBuffer,
		NULL
	);

	//
	// STEP 8 : Mapping Of Uniform Variables.
	//

	//
	// Define And Set Constant Buffer 
	// Similar To glGetUniformLocation In OpenGL
	//
	D3D11_BUFFER_DESC pep_bufferDesc_ConstantBuffer;
	ZeroMemory(&pep_bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));

	pep_bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER); // mapped with cbuffer of vertex shader
	pep_bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // Bind a buffer as a constant buffer to a shader stage

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_ConstantBuffer,
		nullptr,
		&pep_gpID3D11Buffer_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Similar To glGetUniformLocation Of OpenGL
	//
	// CBUFFER Gets Mapped With ConstBuffer In Vertex Shader
	//
	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0, // Which Slot In Shader
		1, // No of Buffers
		&pep_gpID3D11Buffer_ConstantBuffer
	);

	//
	// pep_gpID3D11RenderTargetView
	// Similar To glClearColor
	//
	pep_gClearColor[0] = 0.0f;
	pep_gClearColor[1] = 0.0f;
	pep_gClearColor[2] = 1.0f;
	pep_gClearColor[3] = 1.0f;

	//
	// (DirectX)  XMMatrixIdentity = mat4::identity() (OpenGL)
	//
	pep_gorthographicProjectionMatrix = XMMatrixIdentity();

	//
	// Call Resize First Time
	//
	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Successful.\n");
		fclose(pep_gpFile);
	}

	return S_OK;
}


void Display(void)
{
	//
	// Code
	//

	//
	// First 0th Index Buffer Gets Render Then 1st Index
	//
	// DirectX (ClearRenderTargetView) =  glClear (OpenGL)
	//
	pep_gpID3D11DeviceContext->ClearRenderTargetView(pep_gpID3D11RenderTargetView, pep_gClearColor);

	// DirectX Is Internally Multi-Threaded

	//
	// Select Which Vertex Buffer To Display
	//
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	//
	// STEP 7 : Part Of Step 7 In Function Initialize()...
	//

	//
	// pep_gpID3D11Buffer_VertexBuffer Mapped With MSR
	//
	// (DirectX) IASetVertexBuffers = glVertexAttribPointer + glEnableVertexAttribArray (OpenGL)
	//
	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0, // Slot (1st Parameter Of glVertexAttribPointer i.e AMC_ATTRIBUTES_POSITION In OpenGL)
		1, // No Of Buffer
		&pep_gpID3D11Buffer_VertexBuffer,
		&stride, // 2nd Parameter of glVertexAttribPointer
		&offset
	); // This Function Can Be Call In Initialize As Well (One Time Activity In Case Of "Static Draw")

	//
	// (DirectX) IASetPrimitiveTopology = 1st Paramter glDraw* (OpenGL)
	//
	// (DirectX) D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST = GL_TRIANGLES (OpenGL)
	//
	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
	);

	//
	//
	//
	XMMATRIX worldMatrix = XMMatrixIdentity(); // (DirectX) worldMatrix = modelMatrix(OpenGL)
	XMMATRIX viewMatrix = XMMatrixIdentity();

	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * pep_gorthographicProjectionMatrix;

	//
	// STEP 8 : Part Of Step 8 In Initialize.
	//

	//
	// Load The Data Into Constant Buffer
	//
	// (DirectX) UpdateSubResource = glUniform* (OpenGL)
	//
	// UpdateSubresource => Internally Map->memcpy->unmap
	//
	CBUFFER pep_constantBuffer;
	pep_constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_gpID3D11Buffer_ConstantBuffer,
		0, // Slot In Shader
		NULL,
		&pep_constantBuffer,
		0,
		0
	);

	//
	// Draw Vertex Buffer To Render Target
	//
	// (DirectX) Draw = 2nd and 3rd Parameter Of glDraw* (OpenGL)
	// 
	pep_gpID3D11DeviceContext->Draw(
		3, // No Of Vertices
		0 // Start Vertex Location
	);
	//
	// Present(0, 0)
	// 1st Parameter : Talks About Frame Getting Present That Sync With Vertical Refersh Rate Or Not
	// Giving 0 Means Use Default Best And Dont Sync
	// 2nd Parameter : Which Buffer To Display , O Means Display All Buffers In Frames
	//
	pep_gpIDXGISwapChain->Present(0, 0); // Similar To SwapBuffers And glxSwapBuffer

	return;
}

HRESULT Resize(int width, int height)
{
	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;

	//
	// Code
	//
	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	// Resizing Default Buffer
	pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0 // Get Default Best
		);

	// Get back Buffer From Swap Chain

	//
	// We Are Using Texture Buffer/Memory Because
	// 1. Fastest Memory On GPU
	// 2. Most Quickly Accessible Memory On GPU
	//
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer = NULL;
	pep_gpIDXGISwapChain->GetBuffer(
		0, // 0th Index
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pID3D11Texture2D_BackBuffer
	);

	hr = pep_gpID3D11Device->CreateRenderTargetView(
		pID3D11Texture2D_BackBuffer, // Back Buffer Index Is 0 (Zero)
		NULL,
		&pep_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//
	// (DirectX)RenderTarget = FrameBuffer(OpenGL)
	// Render Target View (RTV) Changes At OM Stage
	//

	//
	// PipeLine Is Control By pep_gpID3D11DeviceContext
	//

	//
	// Now Set Updated Render Target View In The Pipeline
	//
	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1, // We Only Have One Render Target
		&pep_gpID3D11RenderTargetView,
		NULL
	);  // OM = Output Merger Stage

	//
	// Viewport
	// Note : Viewport Can Be Mutiple
	//
	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f; // Depth Test In OpenGL

	pep_gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport); // RS = Rasterizer Stage

	//
	// Set Orthographic Matrix
	//
	if (width <= height)
	{
		// LH = Left Handed
		// OffCenter => Customize Matrix
		pep_gorthographicProjectionMatrix = XMMatrixOrthographicOffCenterLH(
			-100.0f,
			100.0f,
			-100.0f * ((float)height / (float)width),
			100.0f * ((float)height / (float)width),
			-100.0f,
			100.0f
		);
	}
	else
	{
		// LH = Left Handed
		// OffCenter => Customize Matrix
		pep_gorthographicProjectionMatrix = XMMatrixOrthographicOffCenterLH(
			-100.0f * ((float)width / (float)height),
			100.0f * ((float)width / (float)height),
			-100.0f,
			100.0f,
			-100.0f,
			100.0f
			);
	}

	return hr;
}

void Uninitialize(void)
{
	//
	// Code
	//
	if (NULL != pep_gpID3D11Buffer_ConstantBuffer)
	{
		pep_gpID3D11Buffer_ConstantBuffer->Release();
		pep_gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (NULL != pep_gpID3D11InputLayout)
	{
		pep_gpID3D11InputLayout->Release();
		pep_gpID3D11InputLayout = NULL;
	}

	if (NULL != pep_gpID3D11Buffer_VertexBuffer)
	{
		pep_gpID3D11Buffer_VertexBuffer->Release();
		pep_gpID3D11Buffer_VertexBuffer = NULL;
	}

	if (NULL != pep_gpID3D11PixelShader)
	{
		pep_gpID3D11PixelShader->Release();
		pep_gpID3D11PixelShader = NULL;
	}

	if (NULL != pep_gpID3D11VertexShader)
	{
		pep_gpID3D11VertexShader->Release();
		pep_gpID3D11VertexShader = NULL;
	}

	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	if (pep_gpFile)
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "uninitialize Success.\n");
		fprintf_s(pep_gpFile, "Log File Close Success.\n");
		fclose(pep_gpFile);
	}

	return;
}
