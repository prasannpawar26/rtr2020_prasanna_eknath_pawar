#include <windows.h>
#include <stdio.h>
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838)
#include "XNAMath/xnamath.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool pep_gbActiveWindow = false;
bool pep_gbFullScreen = false;
bool pep_gbEscapeKeyPressed = false;

float pep_gClearColor[4];

ID3D11Device* pep_gpID3D11Device = NULL;  // Buffer Creation
ID3D11DeviceContext* pep_gpID3D11DeviceContext = NULL; // Setting State/Sending Command In Pipeline
IDXGISwapChain* pep_gpIDXGISwapChain = NULL; // Render Target View Related Operations
ID3D11RenderTargetView* pep_gpID3D11RenderTargetView = NULL; // Render Target View

ID3D11InputLayout* pep_gpID3D11InputLayout = NULL; // Setting Attribute

ID3D11VertexShader* pep_gpID3D11VertexShader = NULL;
ID3D11PixelShader* pep_gpID3D11PixelShader = NULL;

ID3D11DepthStencilView* pep_gpID3D11DepthStencilView = NULL;

ID3D11Buffer* pep_gpId3D11Buffer_Triangle_Position = NULL;
ID3D11Buffer* pep_gpId3D11Buffer_Triangle_Color = NULL;

ID3D11Buffer* pep_gpId3D11Buffer_Rectangle_Position = NULL;
ID3D11Buffer* pep_gpId3D11Buffer_Rectangle_Color = NULL;

ID3D11Buffer* pep_gpID3D11Buffer_VertexShader_ConstantBuffer = NULL;

ID3D11RasterizerState* pep_gpID3D11RasterizerState = NULL; // For Culling Off

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX pep_gpPerspeoctiveProjectionMatrix;

float pep_gTriangle_Rotation;
float pep_gRectangle_Rotation;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("10_Perspective_Color3DPyramidAndCube");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.lpfnWndProc = WndProc;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("10_Perspective_Color3DPyramidAndCube"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;

	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Game Loop
	//
	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapeKeyPressed)
				{
					pep_bDone = true;
				}
			}

			Update();
			Display();
		}
	}

	//
	// Call Uninitialize
	//
	Uninitialize();

	return ((int)pep_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Code
	//

	switch (iMsg)
	{
	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		//
		// Call Resize Only When We Have Device Context
		//
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr); // HRESULT -> Get Upcasted Here To LONG.
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapeKeyPressed)
			{
				pep_gbEscapeKeyPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	return;
}

void Uninitialize(void)
{
	//
	// Code
	//
	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_gpID3D11RasterizerState)
	{
		pep_gpID3D11RasterizerState->Release();
		pep_gpID3D11RasterizerState = NULL;
	}

	if (NULL != pep_gpID3D11Buffer_VertexShader_ConstantBuffer)
	{
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer->Release();
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer = NULL;
	}

	if (NULL != pep_gpId3D11Buffer_Rectangle_Color)
	{
		pep_gpId3D11Buffer_Rectangle_Color->Release();
		pep_gpId3D11Buffer_Rectangle_Color = NULL;
	}

	if (NULL != pep_gpId3D11Buffer_Rectangle_Position)
	{
		pep_gpId3D11Buffer_Rectangle_Position->Release();
		pep_gpId3D11Buffer_Rectangle_Position = NULL;
	}

	if (NULL != pep_gpId3D11Buffer_Triangle_Color)
	{
		pep_gpId3D11Buffer_Triangle_Color->Release();
		pep_gpId3D11Buffer_Triangle_Color = NULL;
	}

	if (NULL != pep_gpId3D11Buffer_Triangle_Position)
	{
		pep_gpId3D11Buffer_Triangle_Position->Release();
		pep_gpId3D11Buffer_Triangle_Position = NULL;
	}

	if (NULL != pep_gpID3D11InputLayout)
	{
		pep_gpID3D11InputLayout->Release();
		pep_gpID3D11InputLayout = NULL;
	}

	if (NULL != pep_gpID3D11PixelShader)
	{
		pep_gpID3D11PixelShader->Release();
		pep_gpID3D11PixelShader = NULL;
	}

	if (NULL != pep_gpID3D11VertexShader)
	{
		pep_gpID3D11VertexShader->Release();
		pep_gpID3D11VertexShader = NULL;
	}

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	return;
}

HRESULT Resize(int width, int height)
{
	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;

	//
	// Code
	//

	if (NULL != pep_gpID3D11DepthStencilView)
	{
		pep_gpID3D11DepthStencilView->Release();
		pep_gpID3D11DepthStencilView = NULL;
	}

	if (NULL != pep_gpID3D11DepthStencilView)
	{
		pep_gpID3D11DepthStencilView->Release();
		pep_gpID3D11DepthStencilView = NULL;
	}

	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	hr = pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Success.\n");
		fclose(pep_gpFile);
	}

	ID3D11Texture2D* pep_pID3D11Texture2D_BackBuffer = NULL;
	hr = pep_gpIDXGISwapChain->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pep_pID3D11Texture2D_BackBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Success.\n");
		fclose(pep_gpFile);
	}

	hr = pep_gpID3D11Device->CreateRenderTargetView(
		pep_pID3D11Texture2D_BackBuffer,
		NULL,
		&pep_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_BackBuffer->Release();
	pep_pID3D11Texture2D_BackBuffer = NULL;

	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1,
		&pep_gpID3D11RenderTargetView,
		NULL
	);

	// Create Depth Stencil Buffer -> Using Texture2D Buffer As Depth Buffer
	D3D11_TEXTURE2D_DESC pep_DepthBufferDesc;
	ZeroMemory(&pep_DepthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));

	pep_DepthBufferDesc.Width = (UINT)width;
	pep_DepthBufferDesc.Height = (UINT)height;
	pep_DepthBufferDesc.ArraySize = 1;
	pep_DepthBufferDesc.MipLevels = 1;
	pep_DepthBufferDesc.SampleDesc.Count = 1;
	pep_DepthBufferDesc.SampleDesc.Quality = 0;
	pep_DepthBufferDesc.Format = DXGI_FORMAT_D32_FLOAT;
	pep_DepthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	pep_DepthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	pep_DepthBufferDesc.CPUAccessFlags = 0;
	pep_DepthBufferDesc.MiscFlags = 0;

	ID3D11Texture2D* pep_pID3D11Texture2D_DepthBuffer = NULL;
	hr = pep_gpID3D11Device->CreateTexture2D(&pep_DepthBufferDesc, NULL, &pep_pID3D11Texture2D_DepthBuffer);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateTexture2D Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateTexture2D Success.\n");
		fclose(pep_gpFile);
	}

	// Create Depth Stencil View From Above Depth Stencil Buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC pep_depthStencilViewDesc;
	ZeroMemory(&pep_depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	pep_depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	pep_depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	hr = pep_gpID3D11Device->CreateDepthStencilView(pep_pID3D11Texture2D_DepthBuffer, &pep_depthStencilViewDesc, &pep_gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateDepthStencilView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateDepthStencilView Success.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_DepthBuffer->Release();
	pep_pID3D11Texture2D_DepthBuffer = NULL;

	pep_gpID3D11DeviceContext->OMSetRenderTargets(1, &pep_gpID3D11RenderTargetView, pep_gpID3D11DepthStencilView);

	D3D11_VIEWPORT pep_d3dViewport;
	pep_d3dViewport.TopLeftX = 0;
	pep_d3dViewport.TopLeftY = 0;
	pep_d3dViewport.Width = (float)width;
	pep_d3dViewport.Height = (float)height;
	pep_d3dViewport.MinDepth = 0.0f;
	pep_d3dViewport.MaxDepth = 1.0f;

	pep_gpID3D11DeviceContext->RSSetViewports(
		1,
		&pep_d3dViewport
	);

	pep_gpPerspeoctiveProjectionMatrix = XMMatrixPerspectiveFovLH(
		XMConvertToRadians(45.0f),
		(float)width / (float)height,
		0.1f,
		100.0f
	);

	return hr;
}

void Display(void)
{
	//
	// Code
	//
	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_gpID3D11RenderTargetView,
		pep_gClearColor
	);

	pep_gpID3D11DeviceContext->ClearDepthStencilView(pep_gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);


	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvpMatrix = XMMatrixIdentity();
	CBUFFER pep_ConstantBuffer;

	// Triangle
	UINT pep_stride = sizeof(float) * 3;
	UINT pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0,
		1,
		&pep_gpId3D11Buffer_Triangle_Position,
		&pep_stride,
		&pep_offset
	);

	pep_stride = sizeof(float) * 3;
	pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		1,
		1,
		&pep_gpId3D11Buffer_Triangle_Color,
		&pep_stride,
		&pep_offset
	);

	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
	);

	translationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(
		-2.0f,
		0.0f,
		7.0f
	);

	rotationMatrix = XMMatrixRotationY(
		-pep_gTriangle_Rotation
	);

	worldMatrix = rotationMatrix * translationMatrix;

	wvpMatrix = worldMatrix * viewMatrix * pep_gpPerspeoctiveProjectionMatrix;

	pep_ConstantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer,
		0,
		NULL,
		&pep_ConstantBuffer,
		0,
		0
	);

	pep_gpID3D11DeviceContext->Draw(
		12,
		0
	);

	// Rectangle
	pep_stride = sizeof(float) * 3;
	pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0,
		1,
		&pep_gpId3D11Buffer_Rectangle_Position,
		&pep_stride,
		&pep_offset
	);

	pep_stride = sizeof(float) * 3;
	pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		1,
		1,
		&pep_gpId3D11Buffer_Rectangle_Color,
		&pep_stride,
		&pep_offset
	);

	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);

	translationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(
		2.0f,
		0.0f,
		7.0f
	);

	rotationMatrix = XMMatrixRotationX(
		-pep_gRectangle_Rotation
	);
	rotationMatrix = rotationMatrix * XMMatrixRotationY(
		-pep_gRectangle_Rotation
	);
	rotationMatrix = rotationMatrix * XMMatrixRotationZ(
		-pep_gRectangle_Rotation
	);

	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();

	worldMatrix = rotationMatrix * translationMatrix;

	wvpMatrix = worldMatrix * viewMatrix * pep_gpPerspeoctiveProjectionMatrix;

	pep_ConstantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer,
		0,
		NULL,
		&pep_ConstantBuffer,
		0,
		0
	);

	pep_gpID3D11DeviceContext->Draw(4, 0);
	pep_gpID3D11DeviceContext->Draw(4, 4);
	pep_gpID3D11DeviceContext->Draw(4, 8);
	pep_gpID3D11DeviceContext->Draw(4, 12);
	pep_gpID3D11DeviceContext->Draw(4, 16);
	pep_gpID3D11DeviceContext->Draw(4, 20);

	pep_gpIDXGISwapChain->Present(
		0, 
		0
	);

	return;
}

void Update(void)
{
	//
	// Code
	//
	if (pep_gTriangle_Rotation > 360)
	{
		pep_gTriangle_Rotation = 0.0f;
	}
	pep_gTriangle_Rotation += 0.0001f;

	if (pep_gRectangle_Rotation > 360)
	{
		pep_gRectangle_Rotation = 0.0f;
	}
	pep_gRectangle_Rotation += 0.0001f;

}

HRESULT Initialize(void)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);

	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;
	D3D_DRIVER_TYPE pep_d3dDriverType;
	D3D_DRIVER_TYPE pep_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//
	// Code
	//
	numDriverTypes = sizeof(pep_d3dDriverTypes) / sizeof(pep_d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC pep_dxgiSwapChainDesc;
	ZeroMemory(
		&pep_dxgiSwapChainDesc,
		sizeof(DXGI_SWAP_CHAIN_DESC)
	);

	pep_dxgiSwapChainDesc.BufferCount = 1;
	pep_dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH;
	pep_dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT;
	pep_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	pep_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	pep_dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	pep_dxgiSwapChainDesc.SampleDesc.Count = 1;
	pep_dxgiSwapChainDesc.SampleDesc.Quality = 0;
	pep_dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT index = 0; index < numDriverTypes; index++)
	{
		pep_d3dDriverType = pep_d3dDriverTypes[index];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			pep_d3dDriverType,
			NULL,
			createDeviceFlags,
			&pep_d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&pep_dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&pep_d3dFeatureLevel_acquired,
			&pep_gpID3D11DeviceContext
		);
		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Succeeded\n");
		fclose(pep_gpFile);
	}

	//
	// Vertex Shader
	//
	const char* pep_szVertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix;" \
		"}" \

		"struct VertexDataOutput" \
		"{" \
		"float4 position : SV_POSITION;" \
		"float4 color : COLOR;"
		"};" \

		"VertexDataOutput main(float4 pos : POSITION, float4 color : COLOR)" \
		"{" \
			"VertexDataOutput output;" \

			"output.position = mul(worldViewProjectionMatrix, pos);" \
			"output.color = color;" \

			"return (output);" \
		"}";

	ID3DBlob* pep_pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob* pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_szVertexShaderSourceCode,
		lstrlenA(pep_szVertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pep_pID3DBlob_VertexShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreateVertexShader(
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,
		&pep_gpID3D11VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->VSSetShader(
		pep_gpID3D11VertexShader,
		0,
		0
	);

	//
	// Pixel Shader
	//
	const char* pep_szPixelShaderSourceCode =
		"float4 main(float4 pos : SV_POSITION, float4 color : COLOR) : SV_TARGET" \
		"{" \
			"return (color);" \
		"}";

	ID3DBlob* pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_szPixelShaderSourceCode,
		lstrlenA(pep_szPixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pep_pID3DBlob_PixelShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreatePixelShader(
		pep_pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pep_pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&pep_gpID3D11PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->PSSetShader(
		pep_gpID3D11PixelShader,
		0,
		0
	);

	//
	// Passing Per-Vertex Data(Position, Color, TexCoord, Normal) To DirectX Pipeline
	//
	D3D11_INPUT_ELEMENT_DESC pep_InputElementDesc[2];
	ZeroMemory(
		pep_InputElementDesc,
		sizeof(D3D11_INPUT_ELEMENT_DESC) * 2
	);

	pep_InputElementDesc[0].SemanticName = "POSITION";
	pep_InputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_InputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	pep_InputElementDesc[0].InputSlot = 0;
	pep_InputElementDesc[0].AlignedByteOffset = 0;
	pep_InputElementDesc[0].SemanticIndex = 0;
	pep_InputElementDesc[0].InstanceDataStepRate = 0;

	pep_InputElementDesc[1].SemanticName = "COLOR";
	pep_InputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_InputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	pep_InputElementDesc[1].InputSlot = 1;
	pep_InputElementDesc[1].AlignedByteOffset = 0;
	pep_InputElementDesc[1].SemanticIndex = 0;
	pep_InputElementDesc[1].InstanceDataStepRate = 0;
	
	hr = pep_gpID3D11Device->CreateInputLayout(
		pep_InputElementDesc,
		_ARRAYSIZE(pep_InputElementDesc),
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		&pep_gpID3D11InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->IASetInputLayout(
		pep_gpID3D11InputLayout
	);

	pep_pID3DBlob_PixelShaderCode->Release();
	pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_VertexShaderCode->Release();
	pep_pID3DBlob_VertexShaderCode = NULL;

	// Create Vertex Buffer - Clockwise Order
	float vertices_pyramid[] = {
		// front
		0.0f, 1.0f, 0.0f,    // top
		-1.0f, -1.0f, 1.0f,  // left
		1.0f, -1.0f, 1.0f,   // right

		0.0f, 1.0f, 0.0f,    // top
		1.0, -1.0f, 1.0f,    // left
		1.0f, -1.0f, -1.0f,  // right

		0.0f, 1.0f, 0.0f,     // top
		1.0f, -1.0f, -1.0f,   // right
		-1.0f, -1.0f, -1.0f,  // left

		0.0f, 1.0f, 0.0f,    // top
		-1.0, -1.0f, -1.0f,  // left
		-1.0f, -1.0f, 1.0f   // right
	};

	D3D11_BUFFER_DESC pep_bufferDesc_Triangle_Position;
	ZeroMemory(
		&pep_bufferDesc_Triangle_Position,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Triangle_Position.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Triangle_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Triangle_Position.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices_pyramid);
	pep_bufferDesc_Triangle_Position.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Triangle_Position,
		NULL,
		&pep_gpId3D11Buffer_Triangle_Position
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Triangle VertexBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Triangle VertexBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Triangle_Postion;
	pep_gpID3D11DeviceContext->Map(
		pep_gpId3D11Buffer_Triangle_Position,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Triangle_Postion
	);

	memcpy(pep_MappedSubResource_Triangle_Postion.pData, vertices_pyramid, sizeof(vertices_pyramid));

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpId3D11Buffer_Triangle_Position,
		NULL
	);

	float color_pyramid[] = {// front
		1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
		// right
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
		// back
		1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
		// left
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
		0.0f};

	D3D11_BUFFER_DESC pep_bufferDesc_Triangle_Color;
	ZeroMemory(
		&pep_bufferDesc_Triangle_Color,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Triangle_Color.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Triangle_Color.ByteWidth = sizeof(float) * _ARRAYSIZE(color_pyramid);
	pep_bufferDesc_Triangle_Color.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Triangle_Color.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Triangle_Color,
		NULL,
		&pep_gpId3D11Buffer_Triangle_Color
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Triangle VertexColorBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Triangle VertexColorBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Triangle_Color;
	pep_gpID3D11DeviceContext->Map(
		pep_gpId3D11Buffer_Triangle_Color,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Triangle_Color
	);

	memcpy(
		pep_MappedSubResource_Triangle_Color.pData,
		color_pyramid,
		sizeof(color_pyramid)
	);
	pep_gpID3D11DeviceContext->Unmap(
		pep_gpId3D11Buffer_Triangle_Color,
		NULL
	);

	// Create Vertex Buffer - Clockwise Order
	float vertices_cube[] = {
		//TOP
		-1.0f, 1.0f, 1.0f, //LT
		1.0f, 1.0f,  1.0f, // RT
		-1.0f, 1.0f, -1.0f, // LB
		1.0f, 1.0f, -1.0f, // RB
		//Bottom
		-1.0f, -1.0f, 1.0f, //LT
		1.0f, -1.0f,  1.0f, // RT
		-1.0f, -1.0f, -1.0f, // LB
		1.0f, -1.0f, -1.0f, // RB
		//FRONT
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		//BACK
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		//Right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		//Left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f
	};

	D3D11_BUFFER_DESC pep_bufferDesc_Rectangle_Position;
	ZeroMemory(
		&pep_bufferDesc_Rectangle_Position,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Rectangle_Position.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Rectangle_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Rectangle_Position.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices_cube);
	pep_bufferDesc_Rectangle_Position.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Rectangle_Position,
		NULL,
		&pep_gpId3D11Buffer_Rectangle_Position
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Rectangle_Postion;
	pep_gpID3D11DeviceContext->Map(
		pep_gpId3D11Buffer_Rectangle_Position,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Rectangle_Postion
	);

	memcpy(pep_MappedSubResource_Rectangle_Postion.pData, vertices_cube, sizeof(vertices_cube));

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpId3D11Buffer_Rectangle_Position,
		NULL
	);


	// Create Vertex Buffer - Clockwise Order
	float color_cube[] = {
		// TOP
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		// BOTTOM
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		// FRONT
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		// BACK
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		// RIGHT
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		// LEFT
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f
	};

	D3D11_BUFFER_DESC pep_bufferDesc_Rectangle_Color;
	ZeroMemory(
		&pep_bufferDesc_Rectangle_Color,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Rectangle_Color.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Rectangle_Color.ByteWidth = sizeof(float) * _ARRAYSIZE(color_cube);
	pep_bufferDesc_Rectangle_Color.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Rectangle_Color.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Rectangle_Color,
		NULL,
		&pep_gpId3D11Buffer_Rectangle_Color
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexColorBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexColorBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Rectangle_Color;
	pep_gpID3D11DeviceContext->Map(
		pep_gpId3D11Buffer_Rectangle_Color,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Rectangle_Color
	);

	memcpy(
		pep_MappedSubResource_Rectangle_Color.pData,
		color_cube,
		sizeof(color_cube)
	);
	pep_gpID3D11DeviceContext->Unmap(
		pep_gpId3D11Buffer_Rectangle_Color,
		NULL
	);

	D3D11_BUFFER_DESC pep_bufferDesc_VertexShaderConstantBuffer;
	ZeroMemory(
		&pep_bufferDesc_VertexShaderConstantBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_VertexShaderConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_VertexShaderConstantBuffer.ByteWidth = sizeof(CBUFFER);
	pep_bufferDesc_VertexShaderConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexShaderConstantBuffer,
		NULL,
		&pep_gpID3D11Buffer_VertexShader_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer ConstantBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer ConstantBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0,
		1,
		&pep_gpID3D11Buffer_VertexShader_ConstantBuffer
	);

	// Rasterizer Culling Off
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = pep_gpID3D11Device->CreateRasterizerState(
		&rasterizerDesc,
		&pep_gpID3D11RasterizerState
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRasterizerState Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRasterizerState Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->RSSetState(pep_gpID3D11RasterizerState);

	pep_gClearColor[0] = 0.0f;
	pep_gClearColor[1] = 0.0f;
	pep_gClearColor[2] = 0.0f;
	pep_gClearColor[3] = 0.0f;

	pep_gpPerspeoctiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Succeeded\n");
		fclose(pep_gpFile);
	}

	return S_OK;
}
