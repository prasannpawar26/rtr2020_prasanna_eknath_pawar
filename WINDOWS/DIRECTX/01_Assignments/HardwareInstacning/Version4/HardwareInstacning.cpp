#include <stdio.h>
#include <windows.h>
// directx specific header file
#include <d3d11.h>
#include <d3dcompiler.h>  //For Shader Compilation

#pragma warning(disable : 4838)
#include "XNAMath/xnamath.h"  // This File Is Included in All Other .inl File.

#include "Sphere.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "Sphere.lib")

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable
FILE *pep_gpFile = NULL;
char pep_gszLogFileName[] = "Log.txt";

HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

float gClearColor[4];  // RGBA

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

IDXGISwapChain *pep_gpIDXGISwapChain = NULL;
ID3D11Device *pep_gpID3D11Device = NULL;
ID3D11DeviceContext *pep_gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *pep_gpID3D11RenderTargetView = NULL;

ID3D11Buffer *pep_gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
ID3D11Buffer *pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
ID3D11Buffer *pep_gpID3D11Buffer_InputBuffer_Sphere = NULL;

ID3D11Buffer* pep_gpInstancedBuffer;

float pep_gLightAngle_Zero = 0.0f;

// Depth Related Code
ID3D11DepthStencilView *pep_gpID3D11DepthStencilView = NULL;

// For Culling OFF
ID3D11RasterizerState *pep_gpID3D11RasterizerState = NULL;

// Uniforms
struct CBUFFER {
    XMMATRIX ViewMatrix;
    XMMATRIX ProjectionMatrix;

    XMVECTOR La_0;
    XMVECTOR Ld_0;
    XMVECTOR Ls_0;
    XMVECTOR Light_Position_0;

    unsigned int Light_Enabled;
};

struct InstancedData
{
    XMFLOAT4X4 world;
    XMFLOAT4 Ka;
    XMFLOAT4 Kd;
    XMFLOAT4 Ks;
    FLOAT Material_Shininess;
};
InstancedData pep_InstancedData[4];

bool pep_gbIsLightOn = false;

XMMATRIX gPerspecctiveProjectionMatrix;

float pep_gLightAmbinent_0[] = {0.0f, 0.0f, 0.0f, 1.0f};
float pep_gLightDiffuse_0[] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_gLightSpecular_0[] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_gLightPosition_0[] = {0.0f, 0.0f, 0.0f, 1.0f};

// per Pixel - Shader Related Variables
ID3D11VertexShader *pep_gpPerPixel_ID3D11VertexShader = NULL;
ID3D11PixelShader *pep_gpPerPixel_ID3D11PixelShader = NULL;

ID3D11Buffer *pep_gpPerPixel_ID3D11Buffer_ConstantBuffer = NULL;
ID3D11InputLayout *pep_gpPerPixel_ID3D11InputLayout = NULL;

const char *pep_gPerPixel_VertexShaderSourceCode =
    "cbuffer ConstantBuffer"
    "{"
        "float4x4 viewMatrix;"
        "float4x4 projectionMatrix;"
        "float4 la_0;"
        "float4 ld_0;"
        "float4 ls_0;"
        "float4 light_position_0;"
        "uint light_enabled;"
    "}"

    "struct VertexInput"
    "{"
        "float4 pos : POSITION;"
        "float4 normal : NORMAL;"
        "row_major float4x4 world  : WORLD;"
        "float4 ka : KA;"
        "float4 kd : KD;"
        "float4 ks : KS;"
        "float material_shininess : MS;"
        "uint InstanceId : SV_InstanceID;"
    "};"

    "struct VertexOutput"
    "{"
        "float4 position : SV_POSITION;"
        "float3 transformedNormal_0 : NORMAL0;"  // Using As
                                                // "InterShaderNamedVariable"
                                                // NORMAL0, 1,2 are just name , not
                                                // used
        "float3 lightDirection_0 : NORMAL1;"     // Using As InterShaderNamedVariable"
        "float3 viewerVector_0 : NORMAL2;"       // Using As InterShaderNamedVariable"
        "float4 ka : KA;"
        "float4 kd : KD;"
        "float4 ks : KS;"
        "float material_shininess : MS;"
    "};"

// POSITION and COLOR are from inputElementDesc(InputLayout),
// POSITION Is Simillar To AMC_ATTRIBUTES_POSITION and color Is
// Similar To AMC_ATTRIBUTES_COLOR

    "VertexOutput main(VertexInput vertexIn)"
    "{"
        "VertexOutput output;"

        "if(1 == light_enabled)"
        "{"
            "float4 eye_coordinates = mul(vertexIn.world, vertexIn.pos);"
            "eye_coordinates = mul(viewMatrix, eye_coordinates);"
            "output.transformedNormal_0 =  (float3)(mul(mul((float3x3)viewMatrix, (float3x3)vertexIn.world), (float3)vertexIn.normal));"
            "output.lightDirection_0 = (float3)(light_position_0 - eye_coordinates);"
            "output.viewerVector_0 = -eye_coordinates.xyz;"

            "output.ka = vertexIn.ka;"
            "output.kd = vertexIn.kd;"
            "output.ks = vertexIn.ks;"
            "output.material_shininess = vertexIn.material_shininess;"
        "}"

        "output.position = mul(projectionMatrix, mul(viewMatrix, mul(vertexIn.world, vertexIn.pos)));"

        "return (output);"
    "}";

const char *pep_gPerPixel_PixelShaderSourceCode =
"cbuffer ConstantBuffer"
"{"
    "float4x4 viewMatrix;"
    "float4x4 projectionMatrix;"

    "float4 la_0;"
    "float4 ld_0;"
    "float4 ls_0;"
    "float4 light_position_0;"

    "uint light_enabled;"
"}"

"struct VertexOutput"
"{"
    "float4 position : SV_POSITION;"
    "float3 transformedNormal_0 : NORMAL0;"
    "float3 lightDirection_0 : NORMAL1;"
    "float3 viewerVector_0 : NORMAL2;"
    "float4 ka : KA;"
    "float4 kd : KD;"
    "float4 ks : KS;"
    "float material_shininess : MS;"
"};"

"float4 main(float4 pos : SV_POSITION, VertexOutput vs_output_as_in) : "
"SV_TARGET"
"{"
    "float4 phong_ads_color;"

    "if(1 == light_enabled)"
    "{"
        "float3 light_direction_normalize_0 = normalize(vs_output_as_in.lightDirection_0);"
        "float3 tranformation_matrix_normalize_0 = normalize(vs_output_as_in.transformedNormal_0);"
        "float3 viewer_vector_normal_0 = normalize(vs_output_as_in.viewerVector_0);"

        "float t_normal_dot_light_direction_0 = max(dot(tranformation_matrix_normalize_0, light_direction_normalize_0), 0.0f);"
        "float4 ambinent_0 = la_0 * vs_output_as_in.ka;"
        "float4 diffuse_0 = ld_0 * vs_output_as_in.kd * t_normal_dot_light_direction_0;"
        "float3 reflectionVector_0 = reflect(-light_direction_normalize_0, tranformation_matrix_normalize_0);"
        "float4 specular_0 = ls_0 * vs_output_as_in.ks * pow(max(dot(reflectionVector_0, viewer_vector_normal_0), 0.0), vs_output_as_in.material_shininess);"

        "phong_ads_color = ambinent_0 + diffuse_0 + specular_0;"
    "}"
    "else"
    "{"
        "phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);"
    "}"

    "return (phong_ads_color);"
"}";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpszCmdLine, int iCmdShow)
{
    // Function Prototypes
    HRESULT Initialize(void);
    void UnInitialize(void);
    void Display(void);
    void Update(void);

    // variable declarations
    bool bDone = false;
    MSG msg = {0};
    int iRet = 0;
    HWND hwnd;
    WNDCLASSEX wndClass;
    TCHAR szAppName[] = TEXT("HardwareInstancing");

    // code
    if (0 != fopen_s(&pep_gpFile, pep_gszLogFileName, "w"))
    {
        MessageBox(NULL, TEXT("Log FIle Creation Failed"), TEXT("Error"), MB_OK);
        exit(0);
    }

    fprintf_s(pep_gpFile, "Log File Created Successfully\n");
    fclose(pep_gpFile);

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.cbWndExtra = 0;
    wndClass.cbClsExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hIconSm = LoadCursor(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szAppName;
    wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wndClass.lpfnWndProc = WndProc;

    RegisterClassEx(&wndClass);

    hwnd = CreateWindowEx(
        WS_EX_APPWINDOW,
        szAppName, 
        TEXT("HardwareInstancing"),
        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
        100,
        100,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL
    );

    gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    HRESULT hr;
    hr = Initialize();
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "Initialize failed. Exitting Now...\n");
        fclose(pep_gpFile);
        DestroyWindow(hwnd);
        hwnd = NULL;
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "Initialize succeeded\n");
        fclose(pep_gpFile);
    }

    while (false == bDone)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (WM_QUIT == msg.message)
            {
                bDone = true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow)
            {
            }

            Update();
            Display();
        }
    }  // End Of While

    UnInitialize();

    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // function declarations
    void ToggledFullScreen(void);
    void UnInitialize(void);
    HRESULT ReSize(int, int);

    // variable delcarations
    HRESULT hr;

    // code
    switch (iMsg)
    {
        case WM_DESTROY:
            UnInitialize();
            PostQuitMessage(0);
            break;

        case WM_ERASEBKGND:
            return 0;
            break;

        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;

        case WM_SIZE:
        {
            if (NULL != pep_gpID3D11DeviceContext)
            {
                hr = ReSize(LOWORD(lParam), HIWORD(lParam));
                if (FAILED(hr))
                {
                    fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
                    fprintf_s(pep_gpFile, "ReSize failed. Exitting Now...\n");
                    fclose(pep_gpFile);
                    hwnd = NULL;
                }
                else
                {
                    fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
                    fprintf_s(pep_gpFile, "ReSize succeeded\n");
                    fclose(pep_gpFile);
                }
            }
        }
        break;

        case WM_KILLFOCUS:
            gbActiveWindow = false;
            break;

        case WM_SETFOCUS:
            gbActiveWindow = true;
            break;

        case WM_KEYDOWN:
        {
            switch (wParam)
            {
            case VK_ESCAPE:
                DestroyWindow(hwnd);
                break;

            case 'F':
            case 'f':
                ToggledFullScreen();
                break;

            case 'L':
            case 'l':
                if (pep_gbIsLightOn)
                {
                    pep_gbIsLightOn = false;
                } else {
                    pep_gbIsLightOn = true;
                }

                break;
            }
        }
        break;
    }  // Switch End

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
    // variable declarations
    MONITORINFO mi;

    // code
    if (false == gbIsFullScreen)
    {
        dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & dwStyle)
        {
            mi = {sizeof(MONITORINFO)};

            if (GetWindowPlacement(gHwnd, &gWpPrev) &&
                GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);
        gbIsFullScreen = true;

    }
    else
    {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(gHwnd, &gWpPrev);
        SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
            SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
            SWP_NOMOVE | SWP_NOSIZE);

        gbIsFullScreen = false;
        ShowCursor(TRUE);
    }

    return;
}

HRESULT Initialize_Sphere()
{
    HRESULT hr;
    // Shpere-Start
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
        sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Position;
    ZeroMemory(
        &bufferDesc_VertexBuffer_Sphere_Position,
        sizeof(D3D11_BUFFER_DESC)
    );

    bufferDesc_VertexBuffer_Sphere_Position.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_VertexBuffer_Sphere_Position.ByteWidth =
        sizeof(float) * _ARRAYSIZE(sphere_vertices);
    bufferDesc_VertexBuffer_Sphere_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_VertexBuffer_Sphere_Position.CPUAccessFlags =
        D3D11_CPU_ACCESS_WRITE;

    hr = pep_gpID3D11Device->CreateBuffer(
        &bufferDesc_VertexBuffer_Sphere_Position,
        NULL,
        &pep_gpID3D11Buffer_VertexBuffer_Sphere_Position
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Vertex Failed\n");
        fclose(pep_gpFile);
        return (hr);
    } 
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Vertex Succeeded\n");
        fclose(pep_gpFile);
    }

    // Copy Vertices Into Above Buffer
    D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexSpherePosition;
    ZeroMemory(
        &mappedSubresource_VertexSpherePosition,
        sizeof(D3D11_MAPPED_SUBRESOURCE)
    );
    pep_gpID3D11DeviceContext->Map(
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Position,
        NULL,
        D3D11_MAP_WRITE_DISCARD,
        NULL,
        &mappedSubresource_VertexSpherePosition
    );

    memcpy(
        mappedSubresource_VertexSpherePosition.pData,
        sphere_vertices,
        sizeof(sphere_vertices)
    );

    pep_gpID3D11DeviceContext->Unmap(
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Position,
        NULL
    );

    // VertexPositionBlock-End

    // VertexNormalBlock-Start

    D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Normal;
    ZeroMemory(
        &bufferDesc_VertexBuffer_Sphere_Normal,
        sizeof(D3D11_BUFFER_DESC)
    );
    bufferDesc_VertexBuffer_Sphere_Normal.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_VertexBuffer_Sphere_Normal.ByteWidth =
        sizeof(float) * _ARRAYSIZE(sphere_normals);
    bufferDesc_VertexBuffer_Sphere_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_VertexBuffer_Sphere_Normal.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr =
        pep_gpID3D11Device->CreateBuffer(
            &bufferDesc_VertexBuffer_Sphere_Normal,
            NULL,
            &pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal
        );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Normal Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Normal Succeeded\n");
        fclose(pep_gpFile);
    }

    // Copy Vertices Into Above Buffer
    D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexSphereNormal;
    ZeroMemory(
        &mappedSubresource_VertexSphereNormal,
        sizeof(D3D11_MAPPED_SUBRESOURCE)
    );

    pep_gpID3D11DeviceContext->Map(
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal,
        NULL,
        D3D11_MAP_WRITE_DISCARD,
        NULL,
        &mappedSubresource_VertexSphereNormal
    );

    memcpy(
        mappedSubresource_VertexSphereNormal.pData,
        sphere_normals,
        sizeof(sphere_normals)
    );

    pep_gpID3D11DeviceContext->Unmap(
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal,
        NULL
    );

    D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_IndexBuffer;
    ZeroMemory(
        &bufferDesc_VertexBuffer_Sphere_IndexBuffer,
        sizeof(D3D11_BUFFER_DESC)
    );

    bufferDesc_VertexBuffer_Sphere_IndexBuffer.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_VertexBuffer_Sphere_IndexBuffer.ByteWidth =
        gNumElements * sizeof(short);
    bufferDesc_VertexBuffer_Sphere_IndexBuffer.BindFlags =
        D3D11_BIND_INDEX_BUFFER;
    bufferDesc_VertexBuffer_Sphere_IndexBuffer.CPUAccessFlags =
        D3D11_CPU_ACCESS_WRITE;

    hr = pep_gpID3D11Device->CreateBuffer(
        &bufferDesc_VertexBuffer_Sphere_IndexBuffer,
        NULL,
        &pep_gpID3D11Buffer_InputBuffer_Sphere
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-InputBuffer Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-InputBuffer Succeeded\n");
        fclose(pep_gpFile);
    }

    // Copy InputBuffer Into Above Buffer
    D3D11_MAPPED_SUBRESOURCE mappedSubresource_InputBufferSphere;
    ZeroMemory(
        &mappedSubresource_InputBufferSphere,
        sizeof(D3D11_MAPPED_SUBRESOURCE)
    );

    pep_gpID3D11DeviceContext->Map(
        pep_gpID3D11Buffer_InputBuffer_Sphere,
        NULL,
        D3D11_MAP_WRITE_DISCARD,
        NULL,
        &mappedSubresource_InputBufferSphere
    );

    memcpy(
        mappedSubresource_InputBufferSphere.pData,
        sphere_elements,
        sizeof(short) * gNumElements
    );

    pep_gpID3D11DeviceContext->Unmap(
        pep_gpID3D11Buffer_InputBuffer_Sphere,
        NULL
    );

    // VertexNormalBlock-End
    // Sphere End

    return hr;
}

HRESULT Initialize_PerPixelShaders(void)
{
    HRESULT hr;
    // Shader -> Vertex

    // Vertex Shader Compilation
    ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
    ID3DBlob *pID3DBlob_Error = NULL;

    hr = D3DCompile(
        pep_gPerPixel_VertexShaderSourceCode,
        lstrlenA(pep_gPerPixel_VertexShaderSourceCode) + 1,
        "VS",
        NULL,
        D3D_COMPILE_STANDARD_FILE_INCLUDE,
        "main",
        "vs_5_0",
        0,
        0,
        &pID3DBlob_VertexShaderCode,
        &pID3DBlob_Error
    );
    if (FAILED(hr))
    {
        if (pID3DBlob_Error != NULL)
        {
            fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
            fprintf_s(pep_gpFile, "D3DCompile(): Vertex Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
            fclose(pep_gpFile);
            pID3DBlob_Error->Release();
            pID3DBlob_Error = NULL;
            return (hr);
        }
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "D3DCompile(): Vertex Shader Compiled Succeeded\n");
        fclose(pep_gpFile);
    }

    // CreateVertexShader => Similar To glCreateShader(GL_VERTEX_SHADER)
    hr = pep_gpID3D11Device->CreateVertexShader(
        pID3DBlob_VertexShaderCode->GetBufferPointer(),
        pID3DBlob_VertexShaderCode->GetBufferSize(),
        NULL,
        &pep_gpPerPixel_ID3D11VertexShader
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateVertexShader: Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateVertexShader: Succeeded\n");
        fclose(pep_gpFile);
    }

    pep_gpID3D11DeviceContext->VSSetShader(
        pep_gpPerPixel_ID3D11VertexShader,
        0,
        0
    );

    // Shader-Pixel Shader

    // Pixel Shader Compilation
    ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
    pID3DBlob_Error = NULL;
    hr = D3DCompile(
        pep_gPerPixel_PixelShaderSourceCode,
        lstrlenA(pep_gPerPixel_PixelShaderSourceCode) + 1,
        "PS",
        NULL,
        D3D_COMPILE_STANDARD_FILE_INCLUDE,
        "main",
        "ps_5_0",
        0,
        0,
        &pID3DBlob_PixelShaderCode,
        &pID3DBlob_Error
    );
    if (FAILED(hr))
    {
        if (pID3DBlob_Error != NULL)
        {
            fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
            fprintf_s(pep_gpFile, "D3DCompile(): Pixel Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
            fclose(pep_gpFile);
            pID3DBlob_Error->Release();
            pID3DBlob_Error = NULL;
            return (hr);
        }
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "D3DCompile(): Pixel Shader Compiled Succeeded\n");
        fclose(pep_gpFile);
    }

    // CreatePixelShader => Similar To glCreateShader(GL_FRAGMENT_SHADER)
    hr = pep_gpID3D11Device->CreatePixelShader(
        pID3DBlob_PixelShaderCode->GetBufferPointer(),
        pID3DBlob_PixelShaderCode->GetBufferSize(),
        NULL,
        &pep_gpPerPixel_ID3D11PixelShader
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreatePixelShader Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreatePixelShader Succeeded\n");
        fclose(pep_gpFile);
    }
    pep_gpID3D11DeviceContext->PSSetShader(pep_gpPerPixel_ID3D11PixelShader, 0, 0);

    // Input Layout Related Code
    D3D11_INPUT_ELEMENT_DESC pep_InputElementDesc[10];
    ZeroMemory(
        pep_InputElementDesc,
        sizeof(D3D11_INPUT_ELEMENT_DESC) * 10
    );

    pep_InputElementDesc[0].SemanticName = "POSITION";
    pep_InputElementDesc[0].SemanticIndex = 0;
    pep_InputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    pep_InputElementDesc[0].InputSlot = 0;
    pep_InputElementDesc[0].AlignedByteOffset = 0;
    pep_InputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    pep_InputElementDesc[0].InstanceDataStepRate = 0;

    pep_InputElementDesc[1].SemanticName =
        "NORMAL";                           // NORMAL: used in shader as binding
    pep_InputElementDesc[1].SemanticIndex = 0;  // 1st(0th index) in Texture Buffer
    pep_InputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    pep_InputElementDesc[1].InputSlot = 1;
    pep_InputElementDesc[1].AlignedByteOffset = 0;
    pep_InputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    pep_InputElementDesc[1].InstanceDataStepRate = 0;

    pep_InputElementDesc[2].SemanticName = "WORLD";
    pep_InputElementDesc[2].SemanticIndex = 0;
    pep_InputElementDesc[2].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[2].InputSlot = 2;
    pep_InputElementDesc[2].AlignedByteOffset = 0;
    pep_InputElementDesc[2].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[2].InstanceDataStepRate = 1;

    //{ "WORLD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
    pep_InputElementDesc[3].SemanticName = "WORLD";
    pep_InputElementDesc[3].SemanticIndex = 1;
    pep_InputElementDesc[3].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[3].InputSlot = 2;
    pep_InputElementDesc[3].AlignedByteOffset = 16;
    pep_InputElementDesc[3].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[3].InstanceDataStepRate = 1;

    // { "WORLD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
    pep_InputElementDesc[4].SemanticName = "WORLD";
    pep_InputElementDesc[4].SemanticIndex = 2;
    pep_InputElementDesc[4].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[4].InputSlot = 2;
    pep_InputElementDesc[4].AlignedByteOffset = 32;
    pep_InputElementDesc[4].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[4].InstanceDataStepRate = 1;

    // { "WORLD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
    pep_InputElementDesc[5].SemanticName = "WORLD";
    pep_InputElementDesc[5].SemanticIndex = 3;
    pep_InputElementDesc[5].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[5].InputSlot = 2;
    pep_InputElementDesc[5].AlignedByteOffset = 48;
    pep_InputElementDesc[5].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[5].InstanceDataStepRate = 1;

    pep_InputElementDesc[6].SemanticName = "KA";
    pep_InputElementDesc[6].SemanticIndex = 0;
    pep_InputElementDesc[6].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[6].InputSlot = 2;
    pep_InputElementDesc[6].AlignedByteOffset = 64;
    pep_InputElementDesc[6].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[6].InstanceDataStepRate = 1;

    pep_InputElementDesc[7].SemanticName = "KD";
    pep_InputElementDesc[7].SemanticIndex = 0;
    pep_InputElementDesc[7].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[7].InputSlot = 2;
    pep_InputElementDesc[7].AlignedByteOffset = 80;
    pep_InputElementDesc[7].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[7].InstanceDataStepRate = 1;

    pep_InputElementDesc[8].SemanticName = "KS";
    pep_InputElementDesc[8].SemanticIndex = 0;
    pep_InputElementDesc[8].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    pep_InputElementDesc[8].InputSlot = 2;
    pep_InputElementDesc[8].AlignedByteOffset = 96;
    pep_InputElementDesc[8].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[8].InstanceDataStepRate = 1;

    pep_InputElementDesc[9].SemanticName = "MS";
    pep_InputElementDesc[9].SemanticIndex = 0;
    pep_InputElementDesc[9].Format = DXGI_FORMAT_R32_FLOAT;
    pep_InputElementDesc[9].InputSlot = 2;
    pep_InputElementDesc[9].AlignedByteOffset = 112;
    pep_InputElementDesc[9].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
    pep_InputElementDesc[9].InstanceDataStepRate = 1;

    hr = pep_gpID3D11Device->CreateInputLayout(
        pep_InputElementDesc,
        _ARRAYSIZE(pep_InputElementDesc),
        pID3DBlob_VertexShaderCode->GetBufferPointer(),
        pID3DBlob_VertexShaderCode->GetBufferSize(),
        &pep_gpPerPixel_ID3D11InputLayout
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateInputLayout Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateInputLayout Succeeded\n");
        fclose(pep_gpFile);
    }
    pep_gpID3D11DeviceContext->IASetInputLayout(pep_gpPerPixel_ID3D11InputLayout);

    // TODO: Check Position Once Again
    pID3DBlob_VertexShaderCode->Release();
    pID3DBlob_VertexShaderCode = NULL;
    pID3DBlob_PixelShaderCode->Release();
    pID3DBlob_PixelShaderCode = NULL;

    // Define And Set The Constant Buffer
    D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
    ZeroMemory(
        &bufferDesc_ConstantBuffer,
        sizeof(D3D11_BUFFER_DESC)
    );

    bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
    bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

    hr = pep_gpID3D11Device->CreateBuffer(
        &bufferDesc_ConstantBuffer,
        nullptr,
        &pep_gpPerPixel_ID3D11Buffer_ConstantBuffer
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Constant Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Constant Succeeded\n");
        fclose(pep_gpFile);
    }

    pep_gpID3D11DeviceContext->VSSetConstantBuffers(
        0,
        1,
        &pep_gpPerPixel_ID3D11Buffer_ConstantBuffer
    );
    pep_gpID3D11DeviceContext->PSSetConstantBuffers(
        0,
        1,
        &pep_gpPerPixel_ID3D11Buffer_ConstantBuffer
    );

    return hr;
}

HRESULT Initialize(void) {

    // function declarations
    void UnInitialize(void);
    HRESULT Initialize_Sphere(void);
    HRESULT Initialize_PerPixelShaders(void);
    HRESULT ReSize(int, int);

    // Variable Declarations
    HRESULT hr;
    D3D_DRIVER_TYPE d3DriverType;
    D3D_DRIVER_TYPE d3DriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE};
    D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
    D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
    UINT createDeviceflags = 0;
    UINT numDriverTypes = 0;
    UINT numFeatureLevels = 1;

    // Code
    numDriverTypes = sizeof(d3DriverTypes) / sizeof(d3DriverTypes[0]);

    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
    ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
    dxgiSwapChainDesc.BufferCount = 1;
    dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
    dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
    dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    dxgiSwapChainDesc.OutputWindow = gHwnd;
    dxgiSwapChainDesc.SampleDesc.Count = 1;
    dxgiSwapChainDesc.SampleDesc.Quality = 0;
    dxgiSwapChainDesc.Windowed = TRUE;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes;
        driverTypeIndex++)
    {
        d3DriverType = d3DriverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain(
            NULL,
            d3DriverType,
            NULL,
            createDeviceflags,
            &d3dFeatureLevel_required,
            numFeatureLevels,
            D3D11_SDK_VERSION,
            &dxgiSwapChainDesc,
            &pep_gpIDXGISwapChain,
            &pep_gpID3D11Device,
            &d3dFeatureLevel_acquired,
            &pep_gpID3D11DeviceContext
        );
        if (SUCCEEDED(hr))
        {
            break;
        }
    }  // End Of For Loop

    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain failed.\n");
        fclose(pep_gpFile);
        return hr;
    }

    Initialize_Sphere();
    // Intialize Shaders

    Initialize_PerPixelShaders();

    // Rasterizer State
    // In D3D, backface culling is by default ON
    D3D11_RASTERIZER_DESC rasterizerDesc;
    ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
    rasterizerDesc.AntialiasedLineEnable = FALSE;
    rasterizerDesc.CullMode = D3D11_CULL_NONE;
    rasterizerDesc.DepthBias = 0;
    rasterizerDesc.DepthBiasClamp = 0.0f;
    rasterizerDesc.DepthClipEnable = TRUE;
    rasterizerDesc.FillMode = D3D11_FILL_SOLID;
    rasterizerDesc.FrontCounterClockwise = FALSE;
    rasterizerDesc.MultisampleEnable = FALSE;
    rasterizerDesc.ScissorEnable = FALSE;
    rasterizerDesc.SlopeScaledDepthBias = 0.0f;

    hr = pep_gpID3D11Device->CreateRasterizerState(
        &rasterizerDesc,
        &pep_gpID3D11RasterizerState
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateRasterizerState Failed\n");
        fclose(pep_gpFile);
        return (hr);
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device:CreateRasterizerState Succeeded\n");
        fclose(pep_gpFile);
    }
    pep_gpID3D11DeviceContext->RSSetState(pep_gpID3D11RasterizerState);

    // d3d clear color(blue)
    gClearColor[0] = 0.0f;
    gClearColor[1] = 0.0f;
    gClearColor[2] = 0.0f;
    gClearColor[3] = 0.0f;

    //
    // Allocating Instance Data Buffer To Pass Per Instance Data
    //
    pep_InstancedData[0].world = XMFLOAT4X4(
        1.0f, 0.0f, 0.0f, -1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 5.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[0].Ka = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[0].Kd = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[0].Ks = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[0].Material_Shininess = 50.0f;

    pep_InstancedData[1].world = XMFLOAT4X4(
        1.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 5.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[1].Ka = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[1].Kd = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
    pep_InstancedData[1].Ks = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
    pep_InstancedData[1].Material_Shininess = 50.0f;

    pep_InstancedData[2].world = XMFLOAT4X4(
        1.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, -1.0f,
        0.0f, 0.0f, 1.0f, 5.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[2].Ka = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[2].Kd = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
    pep_InstancedData[2].Ks = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
    pep_InstancedData[2].Material_Shininess = 50.0f;

    pep_InstancedData[3].world = XMFLOAT4X4(
        1.0f, 0.0f, 0.0f, -1.0f,
        0.0f, 1.0f, 0.0f, -1.0f,
        0.0f, 0.0f, 1.0f, 5.0f,
        0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[3].Ka = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
    pep_InstancedData[3].Kd = XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f);
    pep_InstancedData[3].Ks = XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f);
    pep_InstancedData[3].Material_Shininess = 50.0f;

    D3D11_BUFFER_DESC pep_bufferDesc_InstanceBuffer;;
    pep_bufferDesc_InstanceBuffer.Usage = D3D11_USAGE_DYNAMIC;
    pep_bufferDesc_InstanceBuffer.ByteWidth = sizeof(InstancedData) * 4;// number of instance count;
    pep_bufferDesc_InstanceBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    pep_bufferDesc_InstanceBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    pep_bufferDesc_InstanceBuffer.MiscFlags = 0;
    pep_bufferDesc_InstanceBuffer.StructureByteStride = 0;

    hr = pep_gpID3D11Device->CreateBuffer(
        &pep_bufferDesc_InstanceBuffer,
        NULL,
        &pep_gpInstancedBuffer
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf(pep_gpFile, "CreateBuffer InstancedBuffer Failed\n");
        fclose(pep_gpFile);
        return hr;
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf(pep_gpFile, "CreateBuffer InstancedBuffer Succeeded\n");
        fclose(pep_gpFile);
    }

    // Set Projection Matrix To Identity Matrix
    gPerspecctiveProjectionMatrix = XMMatrixIdentity();

    // call resize for first time
    hr = ReSize(WIN_WIDTH, WIN_HEIGHT);
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ReSize failed. Exitting Now...\n");
        fclose(pep_gpFile);
        return hr;
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ReSize succeeded\n");
        fclose(pep_gpFile);
    }

    return S_OK;
}

HRESULT ReSize(int width, int height)
{
    // code
    HRESULT hr = S_OK;

    //depth related code
    if (pep_gpID3D11DepthStencilView)
    {
        pep_gpID3D11DepthStencilView->Release();
        pep_gpID3D11DepthStencilView = NULL;
    }

    // free any size-dependent resources
    if (pep_gpID3D11RenderTargetView)
    {
        pep_gpID3D11RenderTargetView->Release();
        pep_gpID3D11RenderTargetView = NULL;
    }

    // resize swap chain buffers accordingly
    pep_gpIDXGISwapChain->ResizeBuffers(
        1,
        width,
        height,
        DXGI_FORMAT_R8G8B8A8_UNORM,
        0
    );

    // again get back buffer form swap chain
    ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
    pep_gpIDXGISwapChain->GetBuffer(
        0,
        __uuidof(ID3D11Texture2D),
        (LPVOID *)&pID3D11Texture2D_BackBuffer
    );

    // again get render target view from d3d11 device using above back buffer
    hr = pep_gpID3D11Device->CreateRenderTargetView(
        pID3D11Texture2D_BackBuffer,
        NULL,
        &pep_gpID3D11RenderTargetView
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device::CreateRenderTargetView failed.\n");
        fclose(pep_gpFile);
        return hr;
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device::CreateRenderTargetView succeeded\n");
        fclose(pep_gpFile);
    }
    pID3D11Texture2D_BackBuffer->Release();
    pID3D11Texture2D_BackBuffer = NULL;

    // set render target view as render target
    pep_gpID3D11DeviceContext->OMSetRenderTargets(
        1,
        &pep_gpID3D11RenderTargetView,
        NULL
    );

    // Depth Related Code

    // create depth stencil buffer(or xbuffer)
    D3D11_TEXTURE2D_DESC textureDesc;
    ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

    textureDesc.Width = (UINT)width;
    textureDesc.Height = (UINT)height;
    textureDesc.ArraySize = 1;
    textureDesc.MipLevels = 1;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.SampleDesc.Quality = 0;
    textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;

    ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
    hr = pep_gpID3D11Device->CreateTexture2D(
        &textureDesc,
        NULL,
        &pID3D11Texture2D_DepthBuffer
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device::CreateTexture2D failed.\n");
        fclose(pep_gpFile);
        return hr;
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device::CreateTexture2D succeeded\n");
        fclose(pep_gpFile);
    }

    // create depth stencil view from above depth stencil buffer
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    ZeroMemory(
        &depthStencilViewDesc,
        sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC)
    );

    depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
    hr = pep_gpID3D11Device->CreateDepthStencilView(
        pID3D11Texture2D_DepthBuffer,
        &depthStencilViewDesc,
        &pep_gpID3D11DepthStencilView
    );
    if (FAILED(hr))
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device::CreateDepthStencilView failed.\n");
        fclose(pep_gpFile);
        return hr;
    }
    else
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "ID3D11Device::CreateDepthStencilView succeeded\n");
        fclose(pep_gpFile);
    }
    pID3D11Texture2D_DepthBuffer->Release();
    pID3D11Texture2D_DepthBuffer = NULL;

    // Set Render Target View As Render Target
    pep_gpID3D11DeviceContext->OMSetRenderTargets(
        1,
        &pep_gpID3D11RenderTargetView,
        pep_gpID3D11DepthStencilView
    );

    //

    // set viewport
    D3D11_VIEWPORT d3dViewPort;
    d3dViewPort.TopLeftX = 0;
    d3dViewPort.TopLeftY = 0;
    d3dViewPort.Width = (float)width;
    d3dViewPort.Height = (float)height;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    pep_gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

    // Set Perspecctive Projection Matrix
    gPerspecctiveProjectionMatrix = XMMatrixPerspectiveFovLH(
        XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

    return hr;
}

void Display(void)
{
    void Update(void);

    // code

    // Clear Render Target View To A Chosen Color
    pep_gpID3D11DeviceContext->ClearRenderTargetView(
        pep_gpID3D11RenderTargetView,
        gClearColor
    );

    // Clear The  Depth/Stencil View
    pep_gpID3D11DeviceContext->ClearDepthStencilView(
        pep_gpID3D11DepthStencilView,
        D3D11_CLEAR_DEPTH,
        1.0f,
        0
    );

    // Sphere-Start
    // Select Which Vertex Buffer To Display
    // Vertex Position Buffer
    // Sphere-Start
    // Select Which Vertex Buffer To Display
    // Vertex Position Buffer
    UINT stride = sizeof(float) * 3;
    UINT offset = 0;
    pep_gpID3D11DeviceContext->IASetVertexBuffers(
        0,
        1,
        &pep_gpID3D11Buffer_VertexBuffer_Sphere_Position,
        &stride,
        &offset
    );  // => Like GL_DYNAMIC_DRAW

                   // Vertex Normal Buffer
    stride = sizeof(float) * 3;
    offset = 0;
    pep_gpID3D11DeviceContext->IASetVertexBuffers(
        1,
        1,
        &pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal,
        &stride,
        &offset
    );  // => Like GL_DYNAMIC_DRAW

    D3D11_MAPPED_SUBRESOURCE mappedInstanceData; 
    pep_gpID3D11DeviceContext->Map(pep_gpInstancedBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedInstanceData);

    InstancedData* pInstanceDataView = reinterpret_cast<InstancedData*>(mappedInstanceData.pData);

    //*pInstanceDataView = pep_InstancedData;

    for(UINT i = 0; i < 4; ++i)
    {
        pInstanceDataView[i] = pep_InstancedData[i];
    }

    pep_gpID3D11DeviceContext->Unmap(pep_gpInstancedBuffer, 0);

    stride = sizeof(InstancedData);
    offset = 0;
    pep_gpID3D11DeviceContext->IASetVertexBuffers(
        2,
        1,
        &pep_gpInstancedBuffer,
        &stride,
        &offset
    );

    // Sphere- InputBuffer
    pep_gpID3D11DeviceContext->IASetIndexBuffer(
        pep_gpID3D11Buffer_InputBuffer_Sphere,
        DXGI_FORMAT_R16_UINT,
        0
    );


    // Select Geometry Primitive
    pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // => glDrawArray's 1st Parameter

                                                 // Translation Is Concerned With World Matrix Transformation

    XMMATRIX viewMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixTranslation(0.0f, 0.0f, 0.0f);

    XMMATRIX vMatrix = viewMatrix;

    // Load The Data Into The Constant Buffer
    CBUFFER constantBuffer;
    memset(&constantBuffer, 0, sizeof(CBUFFER));

    constantBuffer.ViewMatrix = vMatrix;
    constantBuffer.ProjectionMatrix = gPerspecctiveProjectionMatrix;
    if (pep_gbIsLightOn)
    {
        constantBuffer.Light_Enabled = 1;
        constantBuffer.La_0 = XMVectorSet(
            pep_gLightAmbinent_0[0],
            pep_gLightAmbinent_0[1],
            pep_gLightAmbinent_0[2],
            pep_gLightAmbinent_0[3]
        );
        constantBuffer.Ld_0 = XMVectorSet(
            pep_gLightDiffuse_0[0],
            pep_gLightDiffuse_0[1],
            pep_gLightDiffuse_0[2],
            pep_gLightDiffuse_0[3]
        );
        constantBuffer.Ls_0 = XMVectorSet(
            pep_gLightSpecular_0[0],
            pep_gLightSpecular_0[1],
            pep_gLightSpecular_0[2],
            pep_gLightSpecular_0[3]
        );

        pep_gLightPosition_0[0] = cos(pep_gLightAngle_Zero) - sin(pep_gLightAngle_Zero);
        pep_gLightPosition_0[1] = cos(pep_gLightAngle_Zero) + sin(pep_gLightAngle_Zero);
        pep_gLightPosition_0[2] = 0.0f;
        pep_gLightPosition_0[3] = 1.0f;

        pep_gLightPosition_0[0] = 10.0f;
        pep_gLightPosition_0[1] = 10.0f;
        pep_gLightPosition_0[2] = 10.0f;
        pep_gLightPosition_0[3] = 1.0f;

        constantBuffer.Light_Position_0 =
            XMVectorSet(
                pep_gLightPosition_0[0],
                pep_gLightPosition_0[1],
                pep_gLightPosition_0[2],
                pep_gLightPosition_0[3]
            );
    }
    else
    {
        constantBuffer.Light_Enabled = 0;
    }

    pep_gpID3D11DeviceContext->UpdateSubresource(
        pep_gpPerPixel_ID3D11Buffer_ConstantBuffer,
        0,
        NULL,
        &constantBuffer,
        0,
        0
    );

    // Draw Vertex Buffer To Render Target

    pep_gpID3D11DeviceContext->DrawIndexedInstanced(gNumElements, 4, 0, 0, 0);

    // Sphere-End

    // Switch between Front And Back Buffers
    pep_gpIDXGISwapChain->Present(0, 0);

    return;
}

void UnInitialize_PerPixel(void)
{
    if (pep_gpPerPixel_ID3D11InputLayout)
    {
        pep_gpPerPixel_ID3D11InputLayout->Release();
        pep_gpPerPixel_ID3D11InputLayout = NULL;
    }

    if (pep_gpPerPixel_ID3D11PixelShader)
    {
        pep_gpPerPixel_ID3D11PixelShader->Release();
        pep_gpPerPixel_ID3D11PixelShader = NULL;
    }

    if (pep_gpPerPixel_ID3D11Buffer_ConstantBuffer)
    {
        pep_gpPerPixel_ID3D11Buffer_ConstantBuffer->Release();
        pep_gpPerPixel_ID3D11Buffer_ConstantBuffer = NULL;
    }

    if (pep_gpPerPixel_ID3D11VertexShader)
    {
        pep_gpPerPixel_ID3D11VertexShader->Release();
        pep_gpPerPixel_ID3D11VertexShader = NULL;
    }
}

void UnInitialize(void)
{
    // function declarations
    void UnInitialize_PerPixel(void);

    // code

    // depth related code
    if (pep_gpID3D11DepthStencilView)
    {
        pep_gpID3D11DepthStencilView->Release();
        pep_gpID3D11DepthStencilView = NULL;
    }

    if (pep_gpID3D11RasterizerState)
    {
        pep_gpID3D11RasterizerState->Release();
        pep_gpID3D11RasterizerState = NULL;
    }

    UnInitialize_PerPixel();

    if (pep_gpID3D11Buffer_InputBuffer_Sphere)
    {
        pep_gpID3D11Buffer_InputBuffer_Sphere->Release();
        pep_gpID3D11Buffer_InputBuffer_Sphere = NULL;
    }

    if (pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal)
    {
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal->Release();
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
    }

    if (pep_gpID3D11Buffer_VertexBuffer_Sphere_Position)
    {
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Position->Release();
        pep_gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
    }

    if (pep_gpID3D11RenderTargetView)
    {
        pep_gpID3D11RenderTargetView->Release();
        pep_gpID3D11RenderTargetView = NULL;
    }

    if (pep_gpIDXGISwapChain)
    {
        pep_gpIDXGISwapChain->Release();
        pep_gpIDXGISwapChain = NULL;
    }

    if (pep_gpID3D11DeviceContext)
    {
        pep_gpID3D11DeviceContext->Release();
        pep_gpID3D11DeviceContext = NULL;
    }

    if (pep_gpID3D11Device)
    {
        pep_gpID3D11Device->Release();
        pep_gpID3D11Device = NULL;
    }

    if (pep_gpFile)
    {
        fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
        fprintf_s(pep_gpFile, "Uninitialize succeeded.\n");
        fprintf_s(pep_gpFile, "Log File Is Successfully Closed.\n");
        fclose(pep_gpFile);
    }

    return;
}

void Update(void)
{

    if (pep_gLightAngle_Zero >= 360.0f)
    {
        pep_gLightAngle_Zero = 0.0f;
    }
    pep_gLightAngle_Zero += 0.0002f;

    return;
}
