#include <windows.h>
#include <stdio.h>
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning (disable : 4838)
#include "XNAMath/xnamath.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool pep_gbActiveWindow = false;
bool pep_gbFullScreen = false;
bool pep_gbEscapeKeyPressed = false;

float pep_gClearColor[4];

/*
	ID3D11Device : To Create Shader, Buffers (RenderTargetView And Buffers), InputLayout
*/

ID3D11Device* pep_gpID3D11Device = NULL; 

/*
	ID3D11DeviceContext : Used For Pipeline Related Operations(Setting Shaders, Setting Topology & Draw, Pass Vertex Related Data In PipeLines, Pass Constant Buffer, Viewport Setting, Set RTV)
*/
ID3D11DeviceContext* pep_gpID3D11DeviceContext = NULL;

/*
	IDXGISwapChain : Operations Set and Resize Buffers Related Rendertargetview
*/
IDXGISwapChain* pep_gpIDXGISwapChain = NULL;
ID3D11RenderTargetView* pep_gpID3D11RenderTargetView = NULL;

ID3D11InputLayout* pep_gpID3D11_InputLayout = NULL;

ID3D11VertexShader* pep_gpID3D11_VertexShader = NULL;
ID3D11PixelShader* pep_gpID3D11_PixelShader = NULL;

ID3D11Buffer* pep_gpID3D11Buffer_Rectangle_VertexPositionBuffer = NULL;
ID3D11Buffer* pep_gpID3D11Buffer_Rectangle_VertexColorBuffer = NULL;

ID3D11Buffer* pep_gpId3D11Buffer_VertexShader_ConstantBuffer = NULL;

ID3D11Buffer* pep_gpInstancedBuffer;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
};

struct InstancedData
{
	XMFLOAT4X4 world;
};
InstancedData pep_InstancedData;

XMMATRIX pep_gpPerspectiveProjectionMatrix;

// For Culling OFF
ID3D11RasterizerState* pep_gpID3D11RasterizerState = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("07_Perspective_2DShapesColor");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.lpfnWndProc = WndProc;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("07_Perspective_2DShapesColor"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;

	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Game Loop
	//
	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapeKeyPressed)
				{
					pep_bDone = true;
				}
			}

			Display();
		}
	}

	//
	// Call Uninitialize
	//
	Uninitialize();

	return ((int)pep_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Code
	//

	switch (iMsg)
	{
	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		//
		// Call Resize Only When We Have Device Context
		//
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr); // HRESULT -> Get Upcasted Here To LONG.
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapeKeyPressed)
			{
				pep_gbEscapeKeyPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	return;
}

HRESULT Initialize(void)
{
	// Function Declarations
	HRESULT Resize(int, int);

	// Variable Declarations
	HRESULT hr;
	D3D_DRIVER_TYPE pep_d3dDriverType;
	D3D_DRIVER_TYPE pep_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	// Code
	numDriverTypes = sizeof(pep_d3dDriverTypes) / sizeof(pep_d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC pep_dxgiSwapChainDesc;
	ZeroMemory(
		&pep_dxgiSwapChainDesc,
		sizeof(DXGI_SWAP_CHAIN_DESC)
		);

	pep_dxgiSwapChainDesc.BufferCount = 1;
	pep_dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH;
	pep_dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT;
	pep_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	pep_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	pep_dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	pep_dxgiSwapChainDesc.SampleDesc.Count = 1;
	pep_dxgiSwapChainDesc.SampleDesc.Quality = 0;
	pep_dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		pep_d3dDriverType = pep_d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			pep_d3dDriverType,
			NULL,
			createDeviceFlags,
			&pep_d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&pep_dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&pep_d3dFeatureLevel_acquired,
			&pep_gpID3D11DeviceContext
		);
		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Succeeded\n");
		fclose(pep_gpFile);
	}

	const char* pep_szVertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
			"float4x4 worldMatrix;" \
			"float4x4 viewMatrix;" \
			"float4x4 projectionMatrix;" \
		"}" \

		"struct VertexDataIn" \
		"{"
			"float4 pos : POSITION;" \
			//"float4 color : COLOR;" \

			"row_major float4x4 world  : WORLD;" \
			"uint InstanceId : SV_InstanceID;" \
		"};" \

		"struct VertexDataOutput" \
		"{"
			"float4 position : SV_POSITION;" \
		//	"float4 color : COLOR;" \

		"};" \

		"VertexDataOutput main(VertexDataIn vertexIn)" \
		"{" \
			"VertexDataOutput output;" \

			"float4x4 translateCenter = { 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 6.0, 0.0, 0.0, 0.0, 1.0};" \

			/*"float4 pos = mul(vertexIn.pos, vertexIn.world);"\
			"output.position = mul(pos, worldViewProjectionMatrix);"\*/

			/*"row_major float4x4 temp = mul(vertexIn.world, worldViewProjectionMatrix);" \
			"output.position = mul(temp, vertexIn.pos);" \*/


			//"output.position = mul(mul(projectionMatrix, mul(viewMatrix, worldMatrix)), vertexIn.pos);" \

			"output.position = mul(mul(projectionMatrix, mul(viewMatrix, vertexIn.world)), vertexIn.pos);" \


			//"output.position = mul(worldViewProjectionMatrix, vertexIn.pos);" \

			//"output.color = vertexIn.color;" \

			"return (output);" \
		"}";

	//row_major
	ID3DBlob* pep_pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob* pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_szVertexShaderSourceCode,
		lstrlenA(pep_szVertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pep_pID3DBlob_VertexShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreateVertexShader(
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,
		&pep_gpID3D11_VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->VSSetShader(
		pep_gpID3D11_VertexShader,
		0,
		0
	);

	const char* pep_szPixelShaderSourceCode =

		//"float4 main(float4 pos : SV_POSITION, float4 color : COLOR) : SV_TARGET" \


		"float4 main(float4 pos : SV_POSITION) : SV_TARGET" \
		"{" \
			"float4 color = float4(1.0, 1.0, 0.0, 1.0);"
			"return (color);" \
		"}";

	ID3DBlob* pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_szPixelShaderSourceCode,
		lstrlenA(pep_szPixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pep_pID3DBlob_PixelShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreatePixelShader(
		pep_pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pep_pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&pep_gpID3D11_PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->PSSetShader(
		pep_gpID3D11_PixelShader,
		0,
		0
	);

	D3D11_INPUT_ELEMENT_DESC pep_InputElementDesc[5];
	ZeroMemory(
		pep_InputElementDesc,
		sizeof(D3D11_INPUT_ELEMENT_DESC) * 5
	);


	//
	// Per-Vertex Data
	// 1. Position
	// 2. Color
	// 3. TexCoord
	// 4. Normals
	//
	// In This Application We Are Only Sending Position And Color Vertex-Data
	//
	pep_InputElementDesc[0].SemanticName = "POSITION";
	pep_InputElementDesc[0].SemanticIndex = 0;
	pep_InputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	pep_InputElementDesc[0].InputSlot = 0;
	pep_InputElementDesc[0].AlignedByteOffset = 0;
	pep_InputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_InputElementDesc[0].InstanceDataStepRate = 0;

	//pep_InputElementDesc[1].SemanticName = "COLOR";
	//pep_InputElementDesc[1].SemanticIndex = 0;
	//pep_InputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	//pep_InputElementDesc[1].InputSlot = 1;
	//pep_InputElementDesc[1].AlignedByteOffset = 0;
	//pep_InputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	//pep_InputElementDesc[1].InstanceDataStepRate = 0;

	pep_InputElementDesc[1].SemanticName = "WORLD";
	pep_InputElementDesc[1].SemanticIndex = 0;
	pep_InputElementDesc[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pep_InputElementDesc[1].InputSlot = 1;
	pep_InputElementDesc[1].AlignedByteOffset = 0;
	pep_InputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	pep_InputElementDesc[1].InstanceDataStepRate = 1;

	//{ "WORLD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	pep_InputElementDesc[2].SemanticName = "WORLD";
	pep_InputElementDesc[2].SemanticIndex = 1;
	pep_InputElementDesc[2].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pep_InputElementDesc[2].InputSlot = 1;
	pep_InputElementDesc[2].AlignedByteOffset = 16;
	pep_InputElementDesc[2].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	pep_InputElementDesc[2].InstanceDataStepRate = 1;

	// { "WORLD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	pep_InputElementDesc[3].SemanticName = "WORLD";
	pep_InputElementDesc[3].SemanticIndex = 2;
	pep_InputElementDesc[3].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pep_InputElementDesc[3].InputSlot = 1;
	pep_InputElementDesc[3].AlignedByteOffset = 32;
	pep_InputElementDesc[3].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	pep_InputElementDesc[3].InstanceDataStepRate = 1;

	// { "WORLD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	pep_InputElementDesc[4].SemanticName = "WORLD";
	pep_InputElementDesc[4].SemanticIndex = 3;
	pep_InputElementDesc[4].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pep_InputElementDesc[4].InputSlot = 1;
	pep_InputElementDesc[4].AlignedByteOffset = 48;
	pep_InputElementDesc[4].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	pep_InputElementDesc[4].InstanceDataStepRate = 1;

	// { "WORLD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	//pep_InputElementDesc[5].SemanticName = "WORLD";
	//pep_InputElementDesc[5].SemanticIndex = 3;
	//pep_InputElementDesc[5].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//pep_InputElementDesc[5].InputSlot = 2;
	//pep_InputElementDesc[5].AlignedByteOffset = 48;
	//pep_InputElementDesc[5].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	//pep_InputElementDesc[5].InstanceDataStepRate = 1;

	hr = pep_gpID3D11Device->CreateInputLayout(
		pep_InputElementDesc,
		_ARRAYSIZE(pep_InputElementDesc),
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		&pep_gpID3D11_InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Failed\n");

		TCHAR* szErrMsg; 

		if(FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM, 
			NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR)&szErrMsg, 0, NULL) != 0) 
		{ 
			fprintf(pep_gpFile, ("ErrorMessage : %s"), szErrMsg); 
			LocalFree(szErrMsg); 
		} else 
			fprintf(pep_gpFile, "[Could not find a description for error # %#x.]\n", hr); 

		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Succeeded\n");
		fclose(pep_gpFile);
	}

	//pep_gpID3D11DeviceContext->IASetInputLayout(
	//	pep_gpID3D11_InputLayout
	//);

	pep_pID3DBlob_PixelShaderCode->Release();
	pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_VertexShaderCode->Release();
	pep_pID3DBlob_VertexShaderCode = NULL;

	//rectangle data
	float vertices_rectangle[] =
	{
		-1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	D3D11_BUFFER_DESC pep_bufferDesc_Rectangle_VertexBuffer;
	ZeroMemory(
		&pep_bufferDesc_Rectangle_VertexBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Rectangle_VertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Rectangle_VertexBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices_rectangle);
	pep_bufferDesc_Rectangle_VertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Rectangle_VertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Rectangle_VertexBuffer,
		NULL,
		&pep_gpID3D11Buffer_Rectangle_VertexPositionBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Rectangle;
	pep_gpID3D11DeviceContext->Map(
		pep_gpID3D11Buffer_Rectangle_VertexPositionBuffer,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Rectangle
	);

	memcpy(
		pep_MappedSubResource_Rectangle.pData,
		vertices_rectangle,
		sizeof(vertices_rectangle)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpID3D11Buffer_Rectangle_VertexPositionBuffer,
		NULL
	);


	//float rectangle_color[] =
	//{
	//	0.0f, 0.0f, 1.0f,
	//	0.0f, 0.0f, 1.0f,
	//	0.0f, 0.0f, 1.0f,
	//	0.0f, 0.0f, 1.0f,
	//	0.0f, 0.0f, 1.0f,
	//	0.0f, 0.0f, 1.0f
	//};

	//D3D11_BUFFER_DESC pep_bufferDesc_Rectangle_VertexColorBuffer;
	//ZeroMemory(
	//	&pep_bufferDesc_Rectangle_VertexColorBuffer,
	//	sizeof(D3D11_BUFFER_DESC)
	//);

	//pep_bufferDesc_Rectangle_VertexColorBuffer.Usage = D3D11_USAGE_DYNAMIC;
	//pep_bufferDesc_Rectangle_VertexColorBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(rectangle_color);
	//pep_bufferDesc_Rectangle_VertexColorBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	//pep_bufferDesc_Rectangle_VertexColorBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//hr = pep_gpID3D11Device->CreateBuffer(
	//	&pep_bufferDesc_Rectangle_VertexColorBuffer,
	//	NULL,
	//	&pep_gpID3D11Buffer_Rectangle_VertexColorBuffer
	//);
	//if (FAILED(hr))
	//{
	//	fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
	//	fprintf(pep_gpFile, "CreateBuffer Rectangle VertexColorBuffer Failed\n");
	//	fclose(pep_gpFile);
	//	return hr;
	//}
	//else
	//{
	//	fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
	//	fprintf(pep_gpFile, "CreateBuffer Rectangle VertexColorBuffer Succeeded\n");
	//	fclose(pep_gpFile);
	//}

	//D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_RectangleColor;
	//pep_gpID3D11DeviceContext->Map(
	//	pep_gpID3D11Buffer_Rectangle_VertexColorBuffer,
	//	NULL,
	//	D3D11_MAP_WRITE_DISCARD,
	//	NULL,
	//	&pep_MappedSubResource_RectangleColor
	//);

	//memcpy(
	//	pep_MappedSubResource_RectangleColor.pData,
	//	rectangle_color,
	//	sizeof(rectangle_color)
	//);

	//pep_gpID3D11DeviceContext->Unmap(
	//	pep_gpID3D11Buffer_Rectangle_VertexColorBuffer,
	//	NULL
	//);

	// constant buffer
	D3D11_BUFFER_DESC pep_bufferDesc_VertexShaderConstantBuffer;
	ZeroMemory(
		&pep_bufferDesc_VertexShaderConstantBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_VertexShaderConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_VertexShaderConstantBuffer.ByteWidth = sizeof(CBUFFER);
	pep_bufferDesc_VertexShaderConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexShaderConstantBuffer,
		NULL,
		&pep_gpId3D11Buffer_VertexShader_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer ConstantBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer ConstantBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	//
	// Allocating Instance Data Buffer To Pass Per Instance Data
	//

	pep_InstancedData.world = XMFLOAT4X4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 3.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
	fprintf(pep_gpFile, "sizeof(InstancedData) : %zd\n", sizeof(InstancedData));
	fclose(pep_gpFile);

	D3D11_BUFFER_DESC pep_bufferDesc_InstanceBuffer;;
	pep_bufferDesc_InstanceBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_InstanceBuffer.ByteWidth = sizeof(InstancedData) * 1;// number of instance count;
	pep_bufferDesc_InstanceBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_InstanceBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pep_bufferDesc_InstanceBuffer.MiscFlags = 0;
	pep_bufferDesc_InstanceBuffer.StructureByteStride = 0;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_InstanceBuffer,
		NULL,
		&pep_gpInstancedBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer InstancedBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer InstancedBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	////
	//// Passing Instance Data
	////
	//D3D11_MAPPED_SUBRESOURCE mappedInstanceData; 
	//pep_gpID3D11DeviceContext->Map(pep_gpInstancedBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedInstanceData);

	//InstancedData* pInstanceDataView = reinterpret_cast<InstancedData*>(mappedInstanceData.pData);

	//XMVECTOR detView = XMMatrixDeterminant(mCam.View());
	//XMMATRIX invView = XMMatrixInverse(&detView, mCam.View());

	//XMMATRIX W = XMLoadFloat4x4(&pep_InstancedData.world);
	//XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);
	//XMMATRIX toLocal = XMMatrixMultiply(invView, invWorld);

	//*pInstanceDataView = pep_InstancedData;

	////memcpy(
	////	mappedInstanceData.pData,
	////	(void *)&pep_InstancedData.world,
	////	sizeof(InstancedData)
	////);

	///*InstancedData* temp = pInstanceDataView;
	//for (int i = 0; i < 4; i += 1)
	//{
	//	for (int j = 0; j < 4; j += 1)
	//	{
	//		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
	//		fprintf(pep_gpFile, "i = %d %f\n", i / 4, temp->world.m[i][j]);
	//		fclose(pep_gpFile);
	//	}
	//}*/

	//pep_gpID3D11DeviceContext->Unmap(pep_gpInstancedBuffer, 0);

	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0,
		1,
		&pep_gpId3D11Buffer_VertexShader_ConstantBuffer
	);

	// Culling related code
	D3D11_RASTERIZER_DESC pep_RasterizerDesc;
	ZeroMemory(
		&pep_RasterizerDesc,
		sizeof(D3D11_RASTERIZER_DESC)
	);

	pep_RasterizerDesc.AntialiasedLineEnable = FALSE;
	pep_RasterizerDesc.CullMode = D3D11_CULL_NONE;
	pep_RasterizerDesc.DepthBias = 0;
	pep_RasterizerDesc.DepthBiasClamp = 0.0f;
	pep_RasterizerDesc.FillMode = D3D11_FILL_SOLID;
	pep_RasterizerDesc.FrontCounterClockwise = FALSE;
	pep_RasterizerDesc.MultisampleEnable = FALSE;
	pep_RasterizerDesc.ScissorEnable = FALSE;
	pep_RasterizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = pep_gpID3D11Device->CreateRasterizerState(&pep_RasterizerDesc, &pep_gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRasterizerState Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRasterizerState Succeeded.\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->RSSetState(pep_gpID3D11RasterizerState);

	pep_gClearColor[0] = 0.0f;
	pep_gClearColor[1] = 0.0f;
	pep_gClearColor[2] = 0.0f;
	pep_gClearColor[3] = 0.0f;

	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Succeeded\n");
		fclose(pep_gpFile);
	}

	return S_OK;
}

void Display(void)
{
	//
	// Code
	//
	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_gpID3D11RenderTargetView,
		pep_gClearColor
	);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvpMatrix = XMMatrixIdentity();
	CBUFFER pep_ConstantBuffer;

	pep_gpID3D11DeviceContext->IASetInputLayout(
		pep_gpID3D11_InputLayout
	);

	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
	);

	// rectangle

	UINT pep_stride = sizeof(float) * 3;
	UINT pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0,
		1,
		&pep_gpID3D11Buffer_Rectangle_VertexPositionBuffer,
		&pep_stride,
		&pep_offset
	);

	//pep_stride = sizeof(float) * 3;
	//pep_offset = 0;

	//pep_gpID3D11DeviceContext->IASetVertexBuffers(
	//	1,
	//	1,
	//	&pep_gpID3D11Buffer_Rectangle_VertexColorBuffer,
	//	&pep_stride,
	//	&pep_offset
	//);

	//
	// Passing Instance Data
	//
	D3D11_MAPPED_SUBRESOURCE mappedInstanceData; 
	pep_gpID3D11DeviceContext->Map(pep_gpInstancedBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedInstanceData);

	InstancedData* pInstanceDataView = reinterpret_cast<InstancedData*>(mappedInstanceData.pData);

	//XMFLOAT4X4 mView = XMFLOAT4X4(
	//	1.0f, 0.0f, 0.0f, 0.0f,
	//	0.0f, 1.0f, 0.0f, 0.0f,
	//	0.0f, 0.0f, 1.0f, 0.0f,
	//	0.0f, 0.0f, 6.0f, 1.0f);

	//XMVECTOR detView = XMMatrixDeterminant(XMLoadFloat4x4(&mView));
	//XMMATRIX invView = XMMatrixInverse(&detView, XMLoadFloat4x4(&mView));

	//XMMATRIX W = XMLoadFloat4x4(&pep_InstancedData.world);
	//XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);
	//XMMATRIX toLocal = XMMatrixMultiply(invView, invWorld);

	//XMVECTOR scale;
	//XMVECTOR rotQuat;
	//XMVECTOR translation;
	//XMMatrixDecompose(&scale, &rotQuat, &translation, toLocal);

	*pInstanceDataView = pep_InstancedData;

	//memcpy(
	//	mappedInstanceData.pData,
	//	(void *)&pep_InstancedData.world,
	//	sizeof(InstancedData)
	//);

	/*InstancedData* temp = pInstanceDataView;
	for (int i = 0; i < 4; i += 1)
	{
	for (int j = 0; j < 4; j += 1)
	{
	fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
	fprintf(pep_gpFile, "i = %d %f\n", i / 4, temp->world.m[i][j]);
	fclose(pep_gpFile);
	}
	}*/

	pep_gpID3D11DeviceContext->Unmap(pep_gpInstancedBuffer, 0);

	pep_stride = sizeof(InstancedData);
	pep_offset = 0;
	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		1,
		1,
		&pep_gpInstancedBuffer,
		&pep_stride,
		&pep_offset
	);

	worldMatrix = XMMatrixTranslation(
		0.0f,
		0.0f,
		0.0f
	);

	/*wvpMatrix = XMMatrixIdentity();
	wvpMatrix = worldMatrix * viewMatrix * pep_gpPerspectiveProjectionMatrix;*/

	memset(&pep_ConstantBuffer, 0, sizeof(CBUFFER));
	pep_ConstantBuffer.WorldMatrix = worldMatrix;
	pep_ConstantBuffer.ViewMatrix = viewMatrix;
	pep_ConstantBuffer.ProjectionMatrix = pep_gpPerspectiveProjectionMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_gpId3D11Buffer_VertexShader_ConstantBuffer,
		0,
		NULL,
		&pep_ConstantBuffer,
		0,
		0
	);

	pep_gpID3D11DeviceContext->Draw(
		6,
		0
	);

	pep_gpIDXGISwapChain->Present(
		0,
		0
	);

	return;
}

HRESULT Resize(int width, int height)
{
	//
	// Variable
	//
	HRESULT hr = S_OK;

	//
	// Code
	//
	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	hr = pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0
	);

	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Success.\n");
		fclose(pep_gpFile);
	}

	ID3D11Texture2D* pep_pID3D11Texture2D_BackBuffer = NULL;
	hr = pep_gpIDXGISwapChain->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pep_pID3D11Texture2D_BackBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Success.\n");
		fclose(pep_gpFile);
	}

	hr = pep_gpID3D11Device->CreateRenderTargetView(
		pep_pID3D11Texture2D_BackBuffer,
		NULL,
		&pep_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_BackBuffer->Release();
	pep_pID3D11Texture2D_BackBuffer = NULL;

	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1,
		&pep_gpID3D11RenderTargetView,
		NULL
	);

	D3D11_VIEWPORT pep_d3dViewport;
	pep_d3dViewport.TopLeftX = 0;
	pep_d3dViewport.TopLeftY = 0;
	pep_d3dViewport.Width = (float)width;
	pep_d3dViewport.Height = (float)height;
	pep_d3dViewport.MinDepth = 0.0;
	pep_d3dViewport.MaxDepth = 1.0f;

	pep_gpID3D11DeviceContext->RSSetViewports(
		1,
		&pep_d3dViewport
	);

	pep_gpPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(
		XMConvertToRadians(45.0f),
		(float)width / (float)height,
		0.1f,
		100.0f
	);

	return hr;
}

void Uninitialize(void)
{

	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_gpID3D11RasterizerState)
	{
		pep_gpID3D11RasterizerState->Release();
		pep_gpID3D11RasterizerState = NULL;
	}

	if (NULL != pep_gpId3D11Buffer_VertexShader_ConstantBuffer)
	{
		pep_gpId3D11Buffer_VertexShader_ConstantBuffer->Release();
		pep_gpId3D11Buffer_VertexShader_ConstantBuffer = NULL;
	}

	if (NULL != pep_gpID3D11_InputLayout)
	{
		pep_gpID3D11_InputLayout->Release();
		pep_gpID3D11_InputLayout = NULL;
	}

	if (NULL != pep_gpID3D11_PixelShader)
	{
		pep_gpID3D11_PixelShader->Release();
		pep_gpID3D11_PixelShader = NULL;
	}

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	return;
}

