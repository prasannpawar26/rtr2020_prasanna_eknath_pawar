#include <windows.h>
#include <stdio.h>

#include <d3d11.h>

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment (lib, "d3d11.lib") // import lib of DirectX 3D => d3d11.dll ->: lib of DirectX 3D

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = {sizeof(WINDOWPLACEMENT)};

bool pep_gbActiveWindow = false;
bool pep_gbEscapekeyIsPressed = false;
bool pep_gbFullScreen = false;

float pep_gClearColor[4]; // RGBA  => Similar To  glClear

//DXGI (DirextX Graphics Infrastructure)
IDXGISwapChain *pep_gpIDXGISwapChain = NULL; // SwapChain Buffer
ID3D11Device *pep_gpID3D11Device = NULL;
ID3D11DeviceContext *pep_gpID3D11DeviceContext = NULL; // Similar To wglDeviceContext
ID3D11RenderTargetView *pep_gpID3D11RenderTargetView = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("Direct3D11");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.lpfnWndProc = WndProc;

	// register WNDCLASSEX
	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;
	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	// Message Loop
	while (pep_bDone == false)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (pep_msg.message == WM_QUIT)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			// Render
			Display();

			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapekeyIsPressed)
				{
					pep_bDone = true;
				}
			}
		}
	}

	//
	// Uninitialize
	//
	Uninitialize();

	return((int)(pep_msg.wParam));
}

LRESULT CALLBACK WndProc(HWND pep_hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Variable Declarations
	//
	

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_ERASEBKGND:
		return (0);
		break;

	case WM_SIZE:
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapekeyIsPressed)
			{
				pep_gbEscapekeyIsPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(pep_hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) && GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}
}

HRESULT Initialize(void)
{
	//
	// Function Declarations
	//
	void Uninitialize(void);
	HRESULT Resize(int, int);

	//
	// Variable Declarations
	//
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	//D3D_DRIVER_TYPE_HARDWARE -> Hardware Rendering
	// D3D_DRIVER_TYPE_REFERENCE -> Software Rendering
	D3D_DRIVER_TYPE d3dDriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE};

	// Below Two Statement Are Equivalent glew Of openGL
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
	// (DirectX)Feature Level = Extension(OpenGL)

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc; // Swap Chain 
	memset((void *)&dxgiSwapChainDesc, 0, sizeof(DXGI_SWAP_CHAIN_DESC));

	//
	// Few Function Of OpenGL Are Replaced By 'dxgiSwapChainDesc' like
	// PixelFormatDecriptor , SetPixelFormat, wglCreateContext, wglMakeCurrent, glewInit 
	//
	dxgiSwapChainDesc.BufferCount = 1; // DirectX Gives 1 Buffer By Default, Hence We Only Required Only One Extra Buffer => It Means Total 2 Buffers
	dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //  Similar To PixelChangeDescriptor In OpenGL
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; // FPS In OpenGL
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // Buffer Type Or Used For Render Target Output
	dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0; // Default 
	dxgiSwapChainDesc.Windowed = TRUE; // Windowing + FullScreen

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL, // Required When Mutiple Graphics Card (We Need Primary)
			d3dDriverType,
			NULL, // No Software Rasterizer Hence NULL
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&pep_gpID3D11DeviceContext
			);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// pep_gpID3D11RenderTargetView
	//
	pep_gClearColor[0] = 0.0f;
	pep_gClearColor[1] = 0.0f;
	pep_gClearColor[2] = 1.0f;
	pep_gClearColor[3] = 1.0f;

	//
	// Shader Code
	//

	//
	// Call Resize First Time
	//
	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Success.\n");
		fclose(pep_gpFile);
	}

	return(S_OK);
}

HRESULT Resize(int width, int height)
{
	HRESULT hr = S_OK;

	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	// Resizing Default Buffer
	pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0 // Get Default Best
	);

	// Get back Buffer From Swap Chain

	//
	// We Are Getting Texture Buffer/Memory Because
	// 1. Fastest Memory On GPU
	// 2. Most Quickly Accessible Memory On GPU
	//
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer = NULL;
	pep_gpIDXGISwapChain->GetBuffer(
		0, // 0th Index
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pID3D11Texture2D_BackBuffer
	);

	hr = pep_gpID3D11Device->CreateRenderTargetView(
		pID3D11Texture2D_BackBuffer, // Back Buffer Index Is 0(Zero)
		NULL, // Get Default Best And Mipmap Level = 0
		&pep_gpID3D11RenderTargetView
	);

	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//
	// (DirectX)RenderTarget = FrameBuffer(OpenGL)
	// Render Target View (RTV) Changes At OM Stage
	//

	//
	// PipeLine Is Control By pep_gpID3D11DeviceContext
	//

	//
	// Now Set Updated Render Target View In The Pipeline
	//
	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1, // We Only Have One Render Target
		&pep_gpID3D11RenderTargetView,
		NULL
	);

	//
	// Viewport
	// Note : Viewport Can Be Multiple
	//
	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f; // Depth Test In OpenGL

	pep_gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport); // RS = Rasterizer Stage

	return hr;
}

void Display(void)
{
	//
	// Code
	//

	//
	// First 0th Index Gets Render Then 1st Index
	//
	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_gpID3D11RenderTargetView,
		pep_gClearColor
	);  // Clear FBO

	// DirectX Is Internally Multi-Threaded

	//
	// Present(0, 0)
	// 1st Parameter : Talks About Frame Getting Present That Sync With Vertical Refersh Rate Or Not
	// Giving 0 Means Use Default Best And Dont Sync
	// 2nd Parameter : Which Buffer To Display , O Means Display All Buffers In Frames
	//
	pep_gpIDXGISwapChain->Present(0, 0); // Similar To SwapBuffers And glxSwapBuffer

	return;
}

void Uninitialize(void)
{
	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	if (pep_gpFile)
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "uninitialize Success.\n");
		fprintf_s(pep_gpFile, "Log File Close Success.\n");
		fclose(pep_gpFile);
	}
}

