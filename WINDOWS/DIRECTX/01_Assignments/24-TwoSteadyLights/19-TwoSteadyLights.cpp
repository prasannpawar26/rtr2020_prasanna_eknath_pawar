#include <stdio.h>
#include <windows.h>
// directx specific header file
#include <d3d11.h>
#include <d3dcompiler.h>  //For Shader Compilation

#pragma warning(disable : 4838)
#include "XNAMath/xnamath.h"  // This File Is Included in All Other .inl File.

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable
FILE *gpFile = NULL;
char gszLogFIleName[] = "Log.txt";

HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

float gClearColor[4];  // RGBA

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Pyramid_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Pyramid_Normal = NULL;

float gPyramidRotationAngle = 0.0f;

// Depth Related Code
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

// For Culling OFF
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

// Uniforms
struct CBUFFER {
  XMMATRIX WorldMatrix;
  XMMATRIX ViewMatrix;
  XMMATRIX ProjectionMatrix;

  XMVECTOR La_0;
  XMVECTOR Ld_0;
  XMVECTOR Ls_0;
  XMVECTOR Light_Position_0;

  XMVECTOR La_1;
  XMVECTOR Ld_1;
  XMVECTOR Ls_1;
  XMVECTOR Light_Position_1;

  XMVECTOR Ka;
  XMVECTOR Kd;
  XMVECTOR Ks;
  float Material_Shininess;

  unsigned int Light_Enabled;
};

bool gbIsLightOn = false;

XMMATRIX gPerspecctiveProjectionMatrix;

float gLightAmbinent_0[] = {0.0f, 0.0f, 0.0f, 1.0f};
float gLightDiffuse_0[] = {1.0f, 0.0f, 0.0f, 1.0f};
float gLightSpecular_0[] = {1.0f, 0.0f, 0.0f, 1.0f};
float gLightPosition_0[] = {-2.0f, 0.0f, 0.0f, 1.0f};

float gLightAmbinent_1[] = {0.0f, 0.0f, 0.0f, 1.0f};
float gLightDiffuse_1[] = {0.0f, 0.0f, 1.0f, 1.0f};
float gLightSpecular_1[] = {0.0f, 0.0f, 1.0f, 1.0f};
float gLightPosition_1[] = {2.0f, 0.0f, 0.0f, 1.0f};

float gMaterialAmbinent[] = {0.0f, 0.0f, 0.0f, 1.0f};
float gMaterialDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
float gMaterialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
float gmaterialShininess = 50.0f;

 // per Pixel - Shader Related Variables
 ID3D11VertexShader *gpPerPixel_ID3D11VertexShader = NULL;
 ID3D11PixelShader *gpPerPixel_ID3D11PixelShader = NULL;

 ID3D11Buffer *gpPerPixel_ID3D11Buffer_ConstantBuffer = NULL;
 ID3D11InputLayout *gpPerPixel_ID3D11InputLayout = NULL;

 const char *gPerPixel_VertexShaderSourceCode =
    "cbuffer ConstantBuffer"
    "{"
    "float4x4 worldMatrix;"
    "float4x4 viewMatrix;"
    "float4x4 projectionMatrix;"

    "float4 la_0;"
    "float4 ld_0;"
    "float4 ls_0;"
    "float4 light_position_0;"
    "float4 la_1;"
    "float4 ld_1;"
    "float4 ls_1;"
    "float4 light_position_1;"

    "float4 ka;"
    "float4 kd;"
    "float4 ks;"
    "float material_shininess;"

    "uint light_enabled;"
    "}"

    "struct VertexOutput"
    "{"
      "float4 position : SV_POSITION;"
      "float3 transformedNormal_0 : NORMAL0;"  // Using As
                                             // "InterShaderNamedVariable"
                                             // NORMAL0, 1,2 are just name , not
                                             // used
      "float3 lightDirection_0 : NORMAL1;"     // Using As InterShaderNamedVariable"
      "float3 viewerVector_0 : NORMAL2;"       // Using As InterShaderNamedVariable"

     "float3 transformedNormal_1 : NORMAL3;"
     "float3 lightDirection_1 : NORMAL4;"
     "float3 viewerVector_1 : NORMAL5;"

    "};"

    // POSITION and COLOR are from inputElementDesc(InputLayout),
    // POSITION Is Simillar To AMC_ATTRIBUTES_POSITION and color Is
    // Similar To AMC_ATTRIBUTES_COLOR

    "VertexOutput main(float4 pos : POSITION, float4 normal : NORMAL)"
    "{"
    "VertexOutput output;"

    "if(1 == light_enabled)"
    "{"
      "float4 eye_coordinates = mul(worldMatrix, pos);"
      "eye_coordinates = mul(viewMatrix, eye_coordinates);"

      "output.transformedNormal_0 =  (float3)(mul(mul((float3x3)viewMatrix, "
      "(float3x3)worldMatrix), (float3)normal));"

      "output.lightDirection_0 = (float3)(light_position_0 - eye_coordinates);"

      "output.viewerVector_0 = -eye_coordinates.xyz;"

      "output.transformedNormal_1 =  (float3)(mul(mul((float3x3)viewMatrix, "
       "(float3x3)worldMatrix), (float3)normal));"

      "output.lightDirection_1 = (float3)(light_position_1 - eye_coordinates);"

      "output.viewerVector_1 = -eye_coordinates.xyz;"

    "}"

    "output.position = mul(projectionMatrix, mul(viewMatrix, "
    "mul(worldMatrix, pos)));"

    "return (output);"
    "}";

 const char *gPerPixel_PixelShaderSourceCode =
    "cbuffer ConstantBuffer"
    "{"
      "float4x4 worldMatrix;"
      "float4x4 viewMatrix;"
      "float4x4 projectionMatrix;"

      "float4 la_0;"
      "float4 ld_0;"
      "float4 ls_0;"
      "float4 light_position_0;"

      "float4 la_1;"
      "float4 ld_1;"
      "float4 ls_1;"
      "float4 light_position_1;"

      "float4 ka;"
      "float4 kd;"
      "float4 ks;"
      "float material_shininess;"

      "uint light_enabled;"
    "}"

    "struct VertexOutput"
    "{"
      "float4 position : SV_POSITION;"
      "float3 transformedNormal_0 : NORMAL0;"
      "float3 lightDirection_0 : NORMAL1;"
      "float3 viewerVector_0 : NORMAL2;"
      "float3 transformedNormal_1 : NORMAL3;"
      "float3 lightDirection_1 : NORMAL4;"
      "float3 viewerVector_1 : NORMAL5;"
    "};"

    "float4 main(float4 pos : SV_POSITION, VertexOutput vs_output_as_in) : "
    "SV_TARGET"
    "{"
    "float4 phong_ads_color;"

    "if(1 == light_enabled)"
    "{"
      "float3 light_direction_normalize_0 = "
      "normalize(vs_output_as_in.lightDirection_0);"
      "float3 tranformation_matrix_normalize_0 = "
      "normalize(vs_output_as_in.transformedNormal_0);"
      "float3 viewer_vector_normal_0 = normalize(vs_output_as_in.viewerVector_0);"

      "float t_normal_dot_light_direction_0 = "
      "max(dot(tranformation_matrix_normalize_0, light_direction_normalize_0), 0.0f);"

      "float4 ambinent_0 = la_0 * ka;"
      "float4 diffuse_0 = ld_0 * kd * t_normal_dot_light_direction_0;"
      "float3 reflectionVector_0 = reflect(-light_direction_normalize_0, "
      "tranformation_matrix_normalize_0);"

      "float4 specular_0 = ls_0 * ks * pow(max(dot(reflectionVector_0, "
      "viewer_vector_normal_0), 0.0), material_shininess);"

      "float3 light_direction_normalize_1 = "
      "normalize(vs_output_as_in.lightDirection_1);"
      "float3 tranformation_matrix_normalize_1 = "
        "normalize(vs_output_as_in.transformedNormal_1);"
      "float3 viewer_vector_normal_1 = "
        "normalize(vs_output_as_in.viewerVector_1);"

      "float t_normal_dot_light_direction_1 = "
        "max(dot(tranformation_matrix_normalize_1, light_direction_normalize_1), "
        "0.0f);"

      "float4 ambinent_1 = la_1 * ka;"
      "float4 diffuse_1 = ld_1 * kd * t_normal_dot_light_direction_1;"
      "float3 reflectionVector_1 = reflect(-light_direction_normalize_1, "
        "tranformation_matrix_normalize_1);"

      "float4 specular_1 = ls_1 * ks * pow(max(dot(reflectionVector_1, "
        "viewer_vector_normal_1), 0.0), material_shininess);"

      "phong_ads_color = ambinent_0 + diffuse_0 + specular_0 + ambinent_1 + diffuse_1 + specular_1;"
    "}"
    "else"
    "{"
    "phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);"
    "}"

    "return (phong_ads_color);"
    "}";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {
  // Function Prototypes
  HRESULT Initialize(void);
  void UnInitialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("DirectX11_19-TwoSteadyLights");

  // code
  if (0 != fopen_s(&gpFile, gszLogFIleName, "w")) {
    MessageBox(NULL, TEXT("Log FIle Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf_s(gpFile, "Log File Created Successfully\n");
  fclose(gpFile);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadCursor(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("DirectX11_19-TwoSteadyLights"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  HRESULT hr;
  hr = Initialize();
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Initialize failed. Exitting Now...\n");
    fclose(gpFile);
    DestroyWindow(hwnd);
    hwnd = NULL;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Initialize succeeded\n");
    fclose(gpFile);
  }

  while (false == bDone) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Update();
      Display();
    }
  }  // End Of While

  UnInitialize();

  return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // function declarations
  void ToggledFullScreen(void);
  void UnInitialize(void);
  HRESULT ReSize(int, int);

  // variable delcarations
  HRESULT hr;

  // code
  switch (iMsg) {
    case WM_DESTROY:
      UnInitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_SIZE: {
      if (NULL != gpID3D11DeviceContext) {
        hr = ReSize(LOWORD(lParam), HIWORD(lParam));
        if (FAILED(hr)) {
          fopen_s(&gpFile, gszLogFIleName, "a+");
          fprintf_s(gpFile, "ReSize failed. Exitting Now...\n");
          fclose(gpFile);
          hwnd = NULL;
        } else {
          fopen_s(&gpFile, gszLogFIleName, "a+");
          fprintf_s(gpFile, "ReSize succeeded\n");
          fclose(gpFile);
        }
      }
    }

    break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case 'L':
        case 'l':
          if (gbIsLightOn)
          {
            gbIsLightOn = false;
          } else {
            gbIsLightOn = true;
          }

          break;
      }
    } break;
  }  // Switch End

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen) {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.left,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }

  return;
}

HRESULT Initialize_Pyramid() {

  HRESULT hr;
  // Create Vertex Buffer - Clockwise Order
  float vertices_pyramid[] = {
      // front
      0.0f, 1.0f, 0.0f,    // top
      -1.0f, -1.0f, 1.0f,  // left
      1.0f, -1.0f, 1.0f,   // right
      // right
      0.0f, 1.0f, 0.0f,    // top
      1.0, -1.0f, 1.0f,    // left
      1.0f, -1.0f, -1.0f,  // right
      // back
      0.0f, 1.0f, 0.0f,     // top
      1.0f, -1.0f, -1.0f,   // right
      -1.0f, -1.0f, -1.0f,  // left
      // left
      0.0f, 1.0f, 0.0f,    // top
      -1.0, -1.0f, -1.0f,  // left
      -1.0f, -1.0f, 1.0f   // right
  };

   D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Pyramid_Position;
  ZeroMemory(&bufferDesc_VertexBuffer_Pyramid_Position,
             sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Pyramid_Position.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Pyramid_Position.ByteWidth =
      sizeof(float) * _ARRAYSIZE(vertices_pyramid);
  bufferDesc_VertexBuffer_Pyramid_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bufferDesc_VertexBuffer_Pyramid_Position.CPUAccessFlags =
      D3D11_CPU_ACCESS_WRITE;

  hr = gpID3D11Device->CreateBuffer(
      &bufferDesc_VertexBuffer_Pyramid_Position, NULL,
      &gpID3D11Buffer_VertexBuffer_Pyramid_Position);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Vertex Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Vertex Succeeded\n");
    fclose(gpFile);
  }

  // Copy Vertices Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexPyramidPosition;
  ZeroMemory(&mappedSubresource_VertexPyramidPosition,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Pyramid_Position, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_VertexPyramidPosition);
  memcpy(mappedSubresource_VertexPyramidPosition.pData, vertices_pyramid,
         sizeof(vertices_pyramid));
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Pyramid_Position,
                               NULL);

  // Create Vertex Buffer - Clockwise Order
  float normal_pyramid[] = {// front
                             0.0f, 0.447214f, 0.894427f,
                             0.0f, 0.447214f, 0.894427f,
                             0.0f, 0.447214f, 0.894427f,
                             // right
                             0.894427f, 0.447214f, 0.0f,
                             0.894427f, 0.447214f, 0.0f,
                             0.894427f, 0.447214f, 0.0f,
                             // back
                             0.0f, 0.447214f, -0.894427f,
                             0.0f, 0.447214f, -0.894427f,
                             0.0f, 0.447214f, -0.894427f,
                             // left
                             -0.894427f, 0.447214f, 0.0f,
                             -0.894427f, 0.447214f, 0.0f,
                             -0.894427f, 0.447214f, 0.0f
                           };

   D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Pyramid_Normal;
  ZeroMemory(&bufferDesc_VertexBuffer_Pyramid_Normal, sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Pyramid_Normal.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Pyramid_Normal.ByteWidth =
      sizeof(float) * _ARRAYSIZE(normal_pyramid);
  bufferDesc_VertexBuffer_Pyramid_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bufferDesc_VertexBuffer_Pyramid_Normal.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

  hr =
      gpID3D11Device->CreateBuffer(&bufferDesc_VertexBuffer_Pyramid_Normal, NULL,
                                   &gpID3D11Buffer_VertexBuffer_Pyramid_Normal);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Color Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Color Succeeded\n");
    fclose(gpFile);
  }

  // Copy Vertices Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexPyramidNormal;
  ZeroMemory(&mappedSubresource_VertexPyramidNormal,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Pyramid_Normal, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_VertexPyramidNormal);
  memcpy(mappedSubresource_VertexPyramidNormal.pData, normal_pyramid,
         sizeof(normal_pyramid));
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Pyramid_Normal, NULL);

  return hr;
}

HRESULT Initialize_PerPixelShaders(void) {
  HRESULT hr;
  // Shader -> Vertex

  // Vertex Shader Compilation
  ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
  ID3DBlob *pID3DBlob_Error = NULL;

  hr = D3DCompile(gPerPixel_VertexShaderSourceCode,
                  lstrlenA(gPerPixel_VertexShaderSourceCode) + 1, "VS", NULL,
                  D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "vs_5_0", 0, 0,
                  &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreateVertexShader => Similar To glCreateShader(GL_VERTEX_SHADER)
  hr = gpID3D11Device->CreateVertexShader(
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(), NULL,
      &gpPerPixel_ID3D11VertexShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->VSSetShader(gpPerPixel_ID3D11VertexShader, 0, 0);

  // Shader-Pixel Shader

  // Pixel Shader Compilation
  ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
  pID3DBlob_Error = NULL;
  hr = D3DCompile(gPerPixel_PixelShaderSourceCode,
                  lstrlenA(gPerPixel_PixelShaderSourceCode) + 1, "PS", NULL,
                  D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "ps_5_0", 0, 0,
                  &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreatePixelShader => Similar To glCreateShader(GL_FRAGMENT_SHADER)
  hr = gpID3D11Device->CreatePixelShader(
      pID3DBlob_PixelShaderCode->GetBufferPointer(),
      pID3DBlob_PixelShaderCode->GetBufferSize(), NULL,
      &gpPerPixel_ID3D11PixelShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->PSSetShader(gpPerPixel_ID3D11PixelShader, 0, 0);

  // Input Layout Related Code
  D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
  inputElementDesc[0].SemanticName = "POSITION";
  inputElementDesc[0].SemanticIndex = 0;
  inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[0].InputSlot = 0;
  inputElementDesc[0].AlignedByteOffset = 0;
  inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[0].InstanceDataStepRate = 0;

  inputElementDesc[1].SemanticName =
      "NORMAL";                           // NORMAL: used in shader as binding
  inputElementDesc[1].SemanticIndex = 0;  // 1st(0th index) in Texture Buffer
  inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[1].InputSlot = 1;
  inputElementDesc[1].AlignedByteOffset = 0;
  inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[1].InstanceDataStepRate = 0;

  hr = gpID3D11Device->CreateInputLayout(
      inputElementDesc, _ARRAYSIZE(inputElementDesc),
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(),
      &gpPerPixel_ID3D11InputLayout);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->IASetInputLayout(gpPerPixel_ID3D11InputLayout);

  // TODO: Check Position Once Again
  pID3DBlob_VertexShaderCode->Release();
  pID3DBlob_VertexShaderCode = NULL;
  pID3DBlob_PixelShaderCode->Release();
  pID3DBlob_PixelShaderCode = NULL;

  // Define And Set The Constant Buffer
  D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
  ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
  bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
  bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
  bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

  hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr,
                                    &gpPerPixel_ID3D11Buffer_ConstantBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Succeeded\n");
    fclose(gpFile);
  }

  gpID3D11DeviceContext->VSSetConstantBuffers(
      0, 1, &gpPerPixel_ID3D11Buffer_ConstantBuffer);
  gpID3D11DeviceContext->PSSetConstantBuffers(
      0, 1, &gpPerPixel_ID3D11Buffer_ConstantBuffer);

  return hr;
}

HRESULT Initialize(void) {

  // function declarations
  void UnInitialize(void);
  HRESULT Initialize_Pyramid(void);
  HRESULT Initialize_PerPixelShaders(void);
  HRESULT ReSize(int, int);

  // Variable Declarations
  HRESULT hr;
  D3D_DRIVER_TYPE d3DriverType;
  D3D_DRIVER_TYPE d3DriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE,
                                     D3D_DRIVER_TYPE_WARP,
                                     D3D_DRIVER_TYPE_REFERENCE};
  D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
  D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
  UINT createDeviceflags = 0;
  UINT numDriverTypes = 0;
  UINT numFeatureLevels = 1;

  // Code
  numDriverTypes = sizeof(d3DriverTypes) / sizeof(d3DriverTypes[0]);

  DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
  ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
  dxgiSwapChainDesc.BufferCount = 1;
  dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
  dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
  dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
  dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
  dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  dxgiSwapChainDesc.OutputWindow = gHwnd;
  dxgiSwapChainDesc.SampleDesc.Count = 1;
  dxgiSwapChainDesc.SampleDesc.Quality = 0;
  dxgiSwapChainDesc.Windowed = TRUE;

  for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes;
       driverTypeIndex++) {
    d3DriverType = d3DriverTypes[driverTypeIndex];
    hr = D3D11CreateDeviceAndSwapChain(
        NULL, d3DriverType, NULL, createDeviceflags, &d3dFeatureLevel_required,
        numFeatureLevels, D3D11_SDK_VERSION, &dxgiSwapChainDesc,
        &gpIDXGISwapChain, &gpID3D11Device, &d3dFeatureLevel_acquired,
        &gpID3D11DeviceContext);
    if (SUCCEEDED(hr)) {
      break;
    }
  }  // End Of For Loop

  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain failed.\n");
    fclose(gpFile);
    return hr;
  }

  fopen_s(&gpFile, gszLogFIleName, "a+");
  fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain succeeded\n");
  fprintf_s(gpFile, "The Chosen Driver Is Of: \n");
  if (d3DriverType == D3D_DRIVER_TYPE_HARDWARE) {
    fprintf_s(gpFile, " Hardware Type\n");
  } else if (d3DriverType == D3D_DRIVER_TYPE_WARP) {
    fprintf_s(gpFile, " Warp Type\n");
  } else if (d3DriverType == D3D_DRIVER_TYPE_SOFTWARE) {
    fprintf_s(gpFile, " Software Type\n");
  } else {
    fprintf_s(gpFile, " Unknown Type\n");
  }

  fprintf_s(gpFile, "The Supported Highest Feature Level Is: \n");
  if (d3DriverType == D3D_FEATURE_LEVEL_11_0) {
    fprintf_s(gpFile, " 11.0\n");
  } else if (d3DriverType == D3D_FEATURE_LEVEL_10_1) {
    fprintf_s(gpFile, " 10.1\n");
  } else if (d3DriverType == D3D_FEATURE_LEVEL_10_0) {
    fprintf_s(gpFile, " 10.0\n");
  } else {
    fprintf_s(gpFile, "Unknown\n");
  }
  fclose(gpFile);

  Initialize_Pyramid();
  // Intialize Shaders

  Initialize_PerPixelShaders();

  // Rasterizer State
  // In D3D, backface culling is by default ON
  D3D11_RASTERIZER_DESC rasterizerDesc;
  ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
  rasterizerDesc.AntialiasedLineEnable = FALSE;
  rasterizerDesc.CullMode = D3D11_CULL_NONE;
  rasterizerDesc.DepthBias = 0;
  rasterizerDesc.DepthBiasClamp = 0.0f;
  rasterizerDesc.DepthClipEnable = TRUE;
  rasterizerDesc.FillMode = D3D11_FILL_SOLID;
  rasterizerDesc.FrontCounterClockwise = FALSE;
  rasterizerDesc.MultisampleEnable = FALSE;
  rasterizerDesc.ScissorEnable = FALSE;
  rasterizerDesc.SlopeScaledDepthBias = 0.0f;

  hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc,
                                             &gpID3D11RasterizerState);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateRasterizerState Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateRasterizerState Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

  // d3d clear color(blue)
  gClearColor[0] = 0.0f;
  gClearColor[1] = 0.0f;
  gClearColor[2] = 0.0f;
  gClearColor[3] = 0.0f;

  // Set Projection Matrix To Identity Matrix
  gPerspecctiveProjectionMatrix = XMMatrixIdentity();

  // call resize for first time
  hr = ReSize(WIN_WIDTH, WIN_HEIGHT);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ReSize failed. Exitting Now...\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ReSize succeeded\n");
    fclose(gpFile);
  }

  return S_OK;
}

HRESULT ReSize(int width, int height) {
  // code
  HRESULT hr = S_OK;

  //depth related code
  if (gpID3D11DepthStencilView)
  {
    gpID3D11DepthStencilView->Release();
    gpID3D11DepthStencilView = NULL;
  }

  // free any size-dependent resources
  if (gpID3D11RenderTargetView) {
    gpID3D11RenderTargetView->Release();
    gpID3D11RenderTargetView = NULL;
  }

  // resize swap chain buffers accordingly
  gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM,
                                  0);

  // again get back buffer form swap chain
  ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
  gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
                              (LPVOID *)&pID3D11Texture2D_BackBuffer);

  // again get render target view from d3d11 device using above back buffer
  hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL,
                                              &gpID3D11RenderTargetView);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView succeeded\n");
    fclose(gpFile);
  }
  pID3D11Texture2D_BackBuffer->Release();
  pID3D11Texture2D_BackBuffer = NULL;

  // set render target view as render target
  gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

  // Depth Related Code

  // create depth stencil buffer(or xbuffer)
  D3D11_TEXTURE2D_DESC textureDesc;
  ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

  textureDesc.Width = (UINT)width;
  textureDesc.Height = (UINT)height;
  textureDesc.ArraySize = 1;
  textureDesc.MipLevels = 1;
  textureDesc.SampleDesc.Count = 1;
  textureDesc.SampleDesc.Quality = 0;
  textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
  textureDesc.Usage = D3D11_USAGE_DEFAULT;
  textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
  textureDesc.CPUAccessFlags = 0;
  textureDesc.MiscFlags = 0;
  
  ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
  hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL,
                                  &pID3D11Texture2D_DepthBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateTexture2D failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateTexture2D succeeded\n");
    fclose(gpFile);
  }

  // create depth stencil view from above depth stencil buffer
  D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
  ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

  depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
  depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
  hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer,
                                              &depthStencilViewDesc, &gpID3D11DepthStencilView);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView succeeded\n");
    fclose(gpFile);
  }
  pID3D11Texture2D_DepthBuffer->Release();
  pID3D11Texture2D_DepthBuffer = NULL;

  // Set Render Target View As Render Target
  gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView,
                                            gpID3D11DepthStencilView);

  //

  // set viewport
  D3D11_VIEWPORT d3dViewPort;
  d3dViewPort.TopLeftX = 0;
  d3dViewPort.TopLeftY = 0;
  d3dViewPort.Width = (float)width;
  d3dViewPort.Height = (float)height;
  d3dViewPort.MinDepth = 0.0f;
  d3dViewPort.MaxDepth = 1.0f;
  gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

  // Set Perspecctive Projection Matrix
  gPerspecctiveProjectionMatrix = XMMatrixPerspectiveFovLH(
      XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

  return hr;
}

void Display(void) {
  void Update(void);

  // code

  // Clear Render Target View To A Chosen Color
  gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,
                                               gClearColor);

  // Clear The  Depth/Stencil View
  gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,
                                               D3D11_CLEAR_DEPTH, 1.0f, 0);

  // Sphere-Start
  // Select Which Vertex Buffer To Display
  // Vertex Position Buffer
  UINT stride = sizeof(float) * 3;
  UINT offset = 0;
  gpID3D11DeviceContext->IASetVertexBuffers(
      0, 1, &gpID3D11Buffer_VertexBuffer_Pyramid_Position, &stride,
      &offset);  // => Like GL_DYNAMIC_DRAW

  // Vertex Normal Buffer
  stride = sizeof(float) * 3;
  offset = 0;
  gpID3D11DeviceContext->IASetVertexBuffers(
      1, 1, &gpID3D11Buffer_VertexBuffer_Pyramid_Normal, &stride,
      &offset);  // => Like GL_DYNAMIC_DRAW

  // Select Geometry Primitive
  gpID3D11DeviceContext->IASetPrimitiveTopology(
      D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // => glDrawArray's 1st Parameter

  // Translation Is Concerned With World Matrix Transformation
  XMMATRIX rotationMatrix = XMMatrixIdentity();
  XMMATRIX translationMatrix = XMMatrixIdentity();
  XMMATRIX worldMatrix = XMMatrixIdentity();
  XMMATRIX viewMatrix = XMMatrixIdentity();

  rotationMatrix = XMMatrixRotationY(-gPyramidRotationAngle);
  viewMatrix = XMMatrixTranslation(0.0f, 0.0f, 4.0f);

  worldMatrix = rotationMatrix * translationMatrix;

  // Final WorldViewProjection Matrix
  XMMATRIX wMatrix = worldMatrix;
  XMMATRIX vMatrix = viewMatrix;

  // Load The Data Into The Constant Buffer
  CBUFFER constantBuffer;
  memset(&constantBuffer, 0, sizeof(CBUFFER));

  constantBuffer.WorldMatrix = wMatrix;
  constantBuffer.ViewMatrix = vMatrix;
  constantBuffer.ProjectionMatrix = gPerspecctiveProjectionMatrix;
  if (gbIsLightOn)
  {
    constantBuffer.Light_Enabled = 1;
    constantBuffer.La_0 = XMVectorSet(gLightAmbinent_0[0], gLightAmbinent_0[1],
                                    gLightAmbinent_0[2], gLightAmbinent_0[3]);
    constantBuffer.Ld_0 = XMVectorSet(gLightDiffuse_0[0], gLightDiffuse_0[1],
                                    gLightDiffuse_0[2], gLightDiffuse_0[3]);
    constantBuffer.Ls_0 = XMVectorSet(gLightSpecular_0[0], gLightSpecular_0[1],
                                    gLightSpecular_0[2], gLightSpecular_0[3]);
    constantBuffer.Light_Position_0 =
        XMVectorSet(gLightPosition_0[0], gLightPosition_0[1],
                    gLightPosition_0[2], gLightPosition_0[3]);

    constantBuffer.La_1 = XMVectorSet(gLightAmbinent_1[0], gLightAmbinent_1[1],
                                      gLightAmbinent_1[2], gLightAmbinent_1[3]);
    constantBuffer.Ld_1 = XMVectorSet(gLightDiffuse_1[0], gLightDiffuse_1[1],
                                      gLightDiffuse_1[2], gLightDiffuse_1[3]);
    constantBuffer.Ls_1 = XMVectorSet(gLightSpecular_1[0], gLightSpecular_1[1],
                                      gLightSpecular_1[2], gLightSpecular_1[3]);
    constantBuffer.Light_Position_1 =
        XMVectorSet(gLightPosition_1[0], gLightPosition_1[1],
                    gLightPosition_1[2], gLightPosition_1[3]);

    constantBuffer.Ka = XMVectorSet(gMaterialAmbinent[0], gMaterialAmbinent[1],
                                    gMaterialAmbinent[2], gMaterialAmbinent[3]);
    constantBuffer.Kd = XMVectorSet(gMaterialDiffuse[0], gMaterialDiffuse[1],
                                    gMaterialDiffuse[2], gMaterialDiffuse[3]);
    constantBuffer.Ks = XMVectorSet(gMaterialSpecular[0], gMaterialSpecular[1],
                                    gMaterialSpecular[2], gMaterialSpecular[3]);
    constantBuffer.Material_Shininess = gmaterialShininess;

  }
  else
  {
    constantBuffer.Light_Enabled = 0;
  }

  gpID3D11DeviceContext->UpdateSubresource(
        gpPerPixel_ID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0,
        0);

  // Draw Vertex Buffer To Render Target
  gpID3D11DeviceContext->Draw(12, 0);

  // Sphere-End

  // Switch between Front And Back Buffers
  gpIDXGISwapChain->Present(0, 0);

  return;
}

void UnInitialize_PerPixel(void) {
  if (gpPerPixel_ID3D11InputLayout) {
    gpPerPixel_ID3D11InputLayout->Release();
    gpPerPixel_ID3D11InputLayout = NULL;
  }

  if (gpPerPixel_ID3D11PixelShader) {
    gpPerPixel_ID3D11PixelShader->Release();
    gpPerPixel_ID3D11PixelShader = NULL;
  }

  if (gpPerPixel_ID3D11Buffer_ConstantBuffer) {
    gpPerPixel_ID3D11Buffer_ConstantBuffer->Release();
    gpPerPixel_ID3D11Buffer_ConstantBuffer = NULL;
  }

  if (gpPerPixel_ID3D11VertexShader) {
    gpPerPixel_ID3D11VertexShader->Release();
    gpPerPixel_ID3D11VertexShader = NULL;
  }
}

void UnInitialize(void) {

  // function declarations
  void UnInitialize_PerPixel(void);

  // code

  // depth related code
  if (gpID3D11DepthStencilView) {
    gpID3D11DepthStencilView->Release();
    gpID3D11DepthStencilView = NULL;
  }

  if (gpID3D11RasterizerState) {
    gpID3D11RasterizerState->Release();
    gpID3D11RasterizerState = NULL;
  }

  UnInitialize_PerPixel();

  if (gpID3D11Buffer_VertexBuffer_Pyramid_Normal) {
    gpID3D11Buffer_VertexBuffer_Pyramid_Normal->Release();
    gpID3D11Buffer_VertexBuffer_Pyramid_Normal = NULL;
  }

  if (gpID3D11Buffer_VertexBuffer_Pyramid_Position) {
    gpID3D11Buffer_VertexBuffer_Pyramid_Position->Release();
    gpID3D11Buffer_VertexBuffer_Pyramid_Position = NULL;
  }

  if (gpID3D11RenderTargetView) {
    gpID3D11RenderTargetView->Release();
    gpID3D11RenderTargetView = NULL;
  }

  if (gpIDXGISwapChain) {
    gpIDXGISwapChain->Release();
    gpIDXGISwapChain = NULL;
  }

  if (gpID3D11DeviceContext) {
    gpID3D11DeviceContext->Release();
    gpID3D11DeviceContext = NULL;
  }

  if (gpID3D11Device) {
    gpID3D11Device->Release();
    gpID3D11Device = NULL;
  }

  if (gpFile) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Uninitialize succeeded.\n");
    fprintf_s(gpFile, "Log File Is Successfully Closed.\n");
    fclose(gpFile);
  }

  return;
}

void Update(void) {

  if (gPyramidRotationAngle > 360.0f) {
    gPyramidRotationAngle = 0.0f;
  }
  gPyramidRotationAngle += 0.0001f;

  return;
}
