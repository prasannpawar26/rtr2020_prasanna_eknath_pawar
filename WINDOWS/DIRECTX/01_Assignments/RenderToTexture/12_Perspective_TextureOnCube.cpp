/*
	Algorithm To For Render To Tecture:
		1. Set RenderTargetView For OffScreenBuffer(Texture) And DepthStencilBuffer
		2. Display -> For Inner Shader Code
		3. Changes RenderTargetView From Offscreen Buffer To Back Buffer And Set DepthStencilBuffer
		4. Set OffScreenBuffer As Texture In Pipeline
		5. Create And Set Sampler
		6. Display -> For Outer Shader Code

		For Render To Texture -> Same Texture2D Texture Will Be Used As
		1. RenderTargetView  -> This Will Be Used As Buffer For Inner Shader
		2. ShaderResourceView -> This Will Be Used As Texture For Outer Shader

	Technical Details :
	1. CreateDepthStencilView -> Can Use Same DepthBuffer For Both Inner(OffScreen Buffer) And Outer(Back Buffer)
	2. CBUFFER -> Can Use Same (Note : This May Change Depending On The Inner And Outer Shaders)
	3. Set DirectX Pipeline Related Things In Resepective Display (i.e Inner / Outer)
		a. SetInputLayout
		b. RenderTargetView
		c. DepthStencilView
		d. XXSetShader
		e. Passing Of Vertex-Data
		f. XXSetConstantBuffer
		g. XXSetShaderResources
		h. XXSetSamplers
		i. IASetPrimitiveTopology

*/

#include <windows.h>
#include <stdio.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include "WICTextureLoader.h"

#pragma warning (disable : 4838)
#include "XNAMath/xnamath.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "DirectXTK.lib")

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool pep_gbActiveWindow = false;
bool pep_gbFullScreen = false;
bool pep_gbEscapeKeyPressed = false;

ID3D11Device* pep_gpID3D11Device = NULL;  // Buffer Creation
ID3D11DeviceContext* pep_gpID3D11DeviceContext = NULL; // Setting State/Sending Command In Pipeline
IDXGISwapChain* pep_gpIDXGISwapChain = NULL; // Render Target View Related Operations
ID3D11RasterizerState* pep_gpID3D11RasterizerState = NULL; // For Culling Off

//
// Inner
//
float pep_Inner_gClearColor[4];
float pep_Inner_gCube_Rotation;

//Shader
ID3D11InputLayout* pep_Inner_gpID3D11InputLayout = NULL; // Setting Attribute
ID3D11VertexShader* pep_Inner_gpID3D11VertexShader = NULL;
ID3D11PixelShader* pep_Inner_gpID3D11PixelShader = NULL;
//Texture
ID3D11ShaderResourceView* pep_Inner_gpID3D11ShaderResourceView_Texture_Cube= NULL;
ID3D11SamplerState* pep_Inner_gpID3D11SamplerState_Texture_Cube = NULL;
//Primitive
ID3D11Buffer* pep_Inner_gpId3D11Buffer_Cube_Position = NULL;
ID3D11Buffer* pep_Inner_gpId3D11Buffer_Cube_Texcoord = NULL;
//Uniform
ID3D11Buffer* pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer = NULL;

// Create Vertex Buffer - Clockwise Order
float inner_vertices_cube[] = {
	//TOP
	-1.0f, 1.0f, 1.0f, //LT
	1.0f, 1.0f,  1.0f, // RT
	-1.0f, 1.0f, -1.0f, // LB
	1.0f, 1.0f, -1.0f, // RB
	//Bottom
	-1.0f, -1.0f, 1.0f, //LT
	1.0f, -1.0f,  1.0f, // RT
	-1.0f, -1.0f, -1.0f, // LB
	1.0f, -1.0f, -1.0f, // RB
	//FRONT
	-1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	//BACK
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	//Right
	1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, 1.0f,
	//Left
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f
};

// Create Vertex Buffer - Clockwise Order
float inner_texcoord_cube[] = {
	// TOP
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	// BOTTOM
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	// FRONT
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	// BACK
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	// RIGHT
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	// LEFT
	0.0f, 0.0f,
	0.0f, 1.0f,
	1.0f, 0.0f,
	1.0f, 1.0f
};

//
// Used In Both Inner And Outer
//
ID3D11RenderTargetView* pep_Inner_gpID3D11RenderTargetView = NULL; // Render Target View
ID3D11ShaderResourceView* pep_Inner_gpID3D11ShaderResourceView = NULL; // Shader Resource View  ->Data Of This Buffer/Resource View Will Be Consider As Texture In Outer Shader
ID3D11DepthStencilView* pep_gpID3D11DepthStencilView = NULL;

//
// Outer
//
float pep_Outer_gClearColor[4];

ID3D11InputLayout* pep_Outer_gpID3D11InputLayout = NULL; // Use To Send Data To Shader(i.e In DirectX Pipeline)
ID3D11VertexShader *pep_Outer_gpID3D11VertexShader = NULL;
ID3D11PixelShader* pep_Outer_gpID3D11PixelShader = NULL; // (DirectX) PixelShader = FragmentShader(OpenGL)
ID3D11Buffer *pep_Outer_gpID3D11Buffer_VertexBuffer = NULL; // (DirectX) VertexBuffer =  VBO(OpenGL)
ID3D11Buffer *pep_Outer_gpID3D11Buffer_Quad_Texcoord = NULL;
ID3D11RenderTargetView* pep_Outer_gpID3D11RenderTargetView = NULL; // Render Target View
ID3D11Buffer* pep_Outer_gpID3D11Buffer_ConstantBuffer = NULL; // (DirectX) ConstantBuffer = Uniforms(OpenGL)
ID3D11SamplerState* pep_Outer_gpID3D11SamplerState = NULL;

float Outer_vertices[] =
{
		//FRONT
		// triangle-1
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,

		//// triangle-2
		1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
};

// Create Vertex Buffer - Clockwise Order
float Outer_texcoord[] = {
  // FRONT
  // triangle-1
  0.0f, 0.0f,
  0.0f, 1.0f,
  1.0f, 0.0f,
  // triangle-2
  1.0f, 0.0f,
  0.0f, 1.0f,
  1.0f, 1.0f,
};

//
// Common In Both Inner And Outer Shaders
//
struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX pep_gpPerspeoctiveProjectionMatrix;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("12_Perspective_TextureOnCube");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.lpfnWndProc = WndProc;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("12_Perspective_TextureOnCube"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;

	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Game Loop
	//
	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapeKeyPressed)
				{
					pep_bDone = true;
				}
			}

			Update();
			Display();
		}
	}

	//
	// Call Uninitialize
	//
	Uninitialize();

	return ((int)pep_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Code
	//

	switch (iMsg)
	{
	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		//
		// Call Resize Only When We Have Device Context
		//
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr); // HRESULT -> Get Upcasted Here To LONG.
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapeKeyPressed)
			{
				pep_gbEscapeKeyPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	return;
}


void Inner_Uninitialize(void)
{
	//
	// Code
	//

	if (NULL != pep_Inner_gpID3D11ShaderResourceView_Texture_Cube)
	{
		pep_Inner_gpID3D11ShaderResourceView_Texture_Cube->Release();
		pep_Inner_gpID3D11ShaderResourceView_Texture_Cube = NULL;
	}

	if (NULL != pep_Inner_gpID3D11SamplerState_Texture_Cube)
	{
		pep_Inner_gpID3D11SamplerState_Texture_Cube->Release();
		pep_Inner_gpID3D11SamplerState_Texture_Cube = NULL;
	}

	if (NULL != pep_Inner_gpID3D11ShaderResourceView)
	{
		pep_Inner_gpID3D11ShaderResourceView->Release();
		pep_Inner_gpID3D11ShaderResourceView = NULL;
	}

	if (NULL != pep_Inner_gpID3D11RenderTargetView)
	{
		pep_Inner_gpID3D11RenderTargetView->Release();
		pep_Inner_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_gpID3D11RasterizerState)
	{
		pep_gpID3D11RasterizerState->Release();
		pep_gpID3D11RasterizerState = NULL;
	}

	if (NULL != pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer)
	{
		pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer->Release();
		pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer = NULL;
	}

	if (NULL != pep_Inner_gpId3D11Buffer_Cube_Texcoord)
	{
		pep_Inner_gpId3D11Buffer_Cube_Texcoord->Release();
		pep_Inner_gpId3D11Buffer_Cube_Texcoord = NULL;
	}

	if (NULL != pep_Inner_gpId3D11Buffer_Cube_Position)
	{
		pep_Inner_gpId3D11Buffer_Cube_Position->Release();
		pep_Inner_gpId3D11Buffer_Cube_Position = NULL;
	}

	if (NULL != pep_Inner_gpID3D11InputLayout)
	{
		pep_Inner_gpID3D11InputLayout->Release();
		pep_Inner_gpID3D11InputLayout = NULL;
	}

	if (NULL != pep_Inner_gpID3D11PixelShader)
	{
		pep_Inner_gpID3D11PixelShader->Release();
		pep_Inner_gpID3D11PixelShader = NULL;
	}

	if (NULL != pep_Inner_gpID3D11VertexShader)
	{
		pep_Inner_gpID3D11VertexShader->Release();
		pep_Inner_gpID3D11VertexShader = NULL;
	}

	return;
}

void Outer_Uninitialize(void)
{
	//
	// Code
	//
	if (NULL != pep_Outer_gpID3D11RenderTargetView)
	{
		pep_Outer_gpID3D11RenderTargetView->Release();
		pep_Outer_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_Outer_gpID3D11SamplerState)
	{
		pep_Outer_gpID3D11SamplerState->Release();
		pep_Outer_gpID3D11SamplerState = NULL;
	}

	if (NULL != pep_Outer_gpID3D11Buffer_ConstantBuffer)
	{
		pep_Outer_gpID3D11Buffer_ConstantBuffer->Release();
		pep_Outer_gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (NULL != pep_Outer_gpID3D11Buffer_Quad_Texcoord)
	{
		pep_Outer_gpID3D11Buffer_Quad_Texcoord->Release();
		pep_Outer_gpID3D11Buffer_Quad_Texcoord = NULL;
	}

	if (NULL != pep_Outer_gpID3D11Buffer_VertexBuffer)
	{
		pep_Outer_gpID3D11Buffer_VertexBuffer->Release();
		pep_Outer_gpID3D11Buffer_VertexBuffer = NULL;
	}

	if (NULL != pep_Outer_gpID3D11PixelShader)
	{
		pep_Outer_gpID3D11PixelShader->Release();
		pep_Outer_gpID3D11PixelShader = NULL;
	}

	if (NULL != pep_Outer_gpID3D11VertexShader)
	{
		pep_Outer_gpID3D11VertexShader->Release();
		pep_Outer_gpID3D11VertexShader = NULL;
	}

	if (NULL != pep_Outer_gpID3D11InputLayout)
	{
		pep_Outer_gpID3D11InputLayout->Release();
		pep_Outer_gpID3D11InputLayout = NULL;
	}

	return;
}

void Uninitialize(void)
{

	//
	//
	//
	void Outer_Uninitialize(void);
	void Inner_Uninitialize(void);

	//
	// Code
	//
	Outer_Uninitialize();

	Inner_Uninitialize();

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	return;
}

HRESULT Inner_Resize(int width, int height)
{
	HRESULT hr = S_OK;

	if (NULL != pep_Inner_gpID3D11RenderTargetView)
	{
		pep_Inner_gpID3D11RenderTargetView->Release();
		pep_Inner_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_Inner_gpID3D11ShaderResourceView)
	{
		pep_Inner_gpID3D11ShaderResourceView->Release();
		pep_Inner_gpID3D11ShaderResourceView = NULL;
	}

	D3D11_TEXTURE2D_DESC pep_TextureBufferDesc;
	ZeroMemory(&pep_TextureBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));

	pep_TextureBufferDesc.Width = (UINT)width;
	pep_TextureBufferDesc.Height = (UINT)height;
	pep_TextureBufferDesc.ArraySize = 1;
	pep_TextureBufferDesc.MipLevels = 1;
	pep_TextureBufferDesc.SampleDesc.Count = 1;
	pep_TextureBufferDesc.SampleDesc.Quality = 0;
	pep_TextureBufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pep_TextureBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	pep_TextureBufferDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	pep_TextureBufferDesc.CPUAccessFlags = 0;
	pep_TextureBufferDesc.MiscFlags = 0;

	ID3D11Texture2D* pep_Inner_pID3D11Texture2D = NULL;
	hr = pep_gpID3D11Device->CreateTexture2D(
		&pep_TextureBufferDesc,
		NULL,
		&pep_Inner_pID3D11Texture2D
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Inner CreateTexture2D Failed.\n");

		hr = HRESULT_CODE(hr); 
		TCHAR* szErrMsg; 

		if(FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM, 
			NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR)&szErrMsg, 0, NULL) != 0) 
		{ 
			fprintf_s(pep_gpFile, "Error : %s", szErrMsg); 
			LocalFree(szErrMsg); 
		}
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Inner CreateTexture2D Success.\n");
		fclose(pep_gpFile);
	}

	hr = pep_gpID3D11Device->CreateRenderTargetView( // BackBuffer -> Step 2
		pep_Inner_pID3D11Texture2D,
		NULL,
		&pep_Inner_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Inner CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Inner CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;
	hr = pep_gpID3D11Device->CreateShaderResourceView(
		pep_Inner_pID3D11Texture2D,
		&srvDesc,
		&pep_Inner_gpID3D11ShaderResourceView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Inner CreateShaderResourceView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Inner CreateShaderResourceView Success.\n");
		fclose(pep_gpFile);
	}

	pep_Inner_pID3D11Texture2D->Release();
	pep_Inner_pID3D11Texture2D = NULL;

	return hr;
}

HRESULT Outer_Resize(int width, int height)
{
	HRESULT hr = S_OK;

	if (NULL != pep_gpID3D11DepthStencilView)
	{
		pep_gpID3D11DepthStencilView->Release();
		pep_gpID3D11DepthStencilView = NULL;
	}

	if (NULL != pep_Outer_gpID3D11RenderTargetView)
	{
		pep_Outer_gpID3D11RenderTargetView->Release();
		pep_Outer_gpID3D11RenderTargetView = NULL;
	}

	hr = pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Success.\n");
		fclose(pep_gpFile);
	}

	ID3D11Texture2D* pep_pID3D11Texture2D_BackBuffer = NULL;
	hr = pep_gpIDXGISwapChain->GetBuffer(   // BackBuffer -> Step 1  Accesses one of the swap-chain's back buffers
		0,
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pep_pID3D11Texture2D_BackBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Success.\n");
		fclose(pep_gpFile);
	}

	hr = pep_gpID3D11Device->CreateRenderTargetView( // BackBuffer -> Step 2
		pep_pID3D11Texture2D_BackBuffer,
		NULL,
		&pep_Outer_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_BackBuffer->Release();
	pep_pID3D11Texture2D_BackBuffer = NULL;

	// Create Depth Stencil Buffer -> Using Texture2D Buffer As Depth Buffer
	D3D11_TEXTURE2D_DESC pep_DepthBufferDesc;
	ZeroMemory(&pep_DepthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));

	pep_DepthBufferDesc.Width = (UINT)width;
	pep_DepthBufferDesc.Height = (UINT)height;
	pep_DepthBufferDesc.ArraySize = 1;
	pep_DepthBufferDesc.MipLevels = 1;
	pep_DepthBufferDesc.SampleDesc.Count = 1;
	pep_DepthBufferDesc.SampleDesc.Quality = 0;
	pep_DepthBufferDesc.Format = DXGI_FORMAT_D32_FLOAT;
	pep_DepthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	pep_DepthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	pep_DepthBufferDesc.CPUAccessFlags = 0;
	pep_DepthBufferDesc.MiscFlags = 0;

	ID3D11Texture2D* pep_pID3D11Texture2D_DepthBuffer = NULL;
	hr = pep_gpID3D11Device->CreateTexture2D(&pep_DepthBufferDesc, NULL, &pep_pID3D11Texture2D_DepthBuffer);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateTexture2D Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateTexture2D Success.\n");
		fclose(pep_gpFile);
	}

	// Create Depth Stencil View From Above Depth Stencil Buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC pep_depthStencilViewDesc;
	ZeroMemory(&pep_depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	pep_depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	pep_depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = pep_gpID3D11Device->CreateDepthStencilView(
		pep_pID3D11Texture2D_DepthBuffer,
		&pep_depthStencilViewDesc,
		&pep_gpID3D11DepthStencilView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateDepthStencilView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateDepthStencilView Success.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_DepthBuffer->Release();
	pep_pID3D11Texture2D_DepthBuffer = NULL;

	return S_OK;
}

HRESULT Resize(int width, int height)
{
	//
	// Function Declarations
	//
	HRESULT Inner_Resize(int, int);
	HRESULT Outer_Resize(int, int);

	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;

	//
	// Code
	//
	Inner_Resize(width, height);

	Outer_Resize(width, height);

	D3D11_VIEWPORT pep_d3dViewport;
	pep_d3dViewport.TopLeftX = 0;
	pep_d3dViewport.TopLeftY = 0;
	pep_d3dViewport.Width = (float)width;
	pep_d3dViewport.Height = (float)height;
	pep_d3dViewport.MinDepth = 0.0f;
	pep_d3dViewport.MaxDepth = 1.0f;

	pep_gpID3D11DeviceContext->RSSetViewports(
		1,
		&pep_d3dViewport
	);

	pep_gpPerspeoctiveProjectionMatrix = XMMatrixPerspectiveFovLH(
		XMConvertToRadians(45.0f),
		(float)width / (float)height,
		0.1f,
		100.0f
	);

	return hr;
}


void Outer_Display(void)
{
	//
	// Code
	//
	pep_gpID3D11DeviceContext->VSSetShader(
		pep_Outer_gpID3D11VertexShader,
		0, // Shader Variable Array (Class Linkage Variables). Here In This Shader We Do Not Such Variable Hence Zero Or NULL. // Use NULL Instead Of Zero
		0 // Use NULL Instead Of Zero
	);

	pep_gpID3D11DeviceContext->PSSetShader(
		pep_Outer_gpID3D11PixelShader,
		0, // Shader Variable Array (Class Linkage Variables). Here In This Shader We Do Not Such Variable Hence Zero Or NULL. // Use NULL Instead Of Zero
		0
	);

	// (DirectX) IASetInputLayout = glBindAttribLocation (OpenGL)
	pep_gpID3D11DeviceContext->IASetInputLayout( // IA = Input Assembly Stage
		pep_Outer_gpID3D11InputLayout
	);

	// Copy Vertex Position Data Into pep_Outer_gpID3D11Buffer_VertexBuffer
	D3D11_MAPPED_SUBRESOURCE pep_mappedSubResource;
	pep_gpID3D11DeviceContext->Map(
		pep_Outer_gpID3D11Buffer_VertexBuffer,
		NULL, // Zero
		D3D11_MAP_WRITE_DISCARD,
		NULL, // Zero
		&pep_mappedSubResource
	);

	memcpy(
		pep_mappedSubResource.pData,
		Outer_vertices,
		sizeof(Outer_vertices)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_Outer_gpID3D11Buffer_VertexBuffer,
		NULL
	);

	// Copy Vertices Into Above Buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexCubeTexcoord;
	ZeroMemory(&mappedSubresource_VertexCubeTexcoord,
	           sizeof(D3D11_MAPPED_SUBRESOURCE));

	pep_gpID3D11DeviceContext->Map(
		pep_Outer_gpID3D11Buffer_Quad_Texcoord,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&mappedSubresource_VertexCubeTexcoord
	);

	memcpy(
		mappedSubresource_VertexCubeTexcoord.pData,
		Outer_texcoord,
		sizeof(Outer_texcoord)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_Outer_gpID3D11Buffer_Quad_Texcoord,
		NULL);

	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0, // Which Slot In Shader
		1, // No. Of Buffers
		&pep_Outer_gpID3D11Buffer_ConstantBuffer
	);


	UINT pep_stride = sizeof(float) * 3;
	UINT pep_offset = 0;

	// (DirectX) IASetVertexBuffers = glVertexAttribPointer + glEnableVertexAttribArray (OpenGL)
	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0, // Slot (1st Parameter Of glVertexAttribPointer (i.e AMC_ATTRIBUTE_POSITION) In OpenGL
		1, // No Of Buffer
		&pep_Outer_gpID3D11Buffer_VertexBuffer, // 2nd Parameter Of glVertexAttributePointer
		&pep_stride,
		&pep_offset
	); // This Function Can Be Called In Initialize As Well

	pep_stride = sizeof(float) * 2;
	pep_offset = 0;
	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		1,
		1,
		&pep_Outer_gpID3D11Buffer_Quad_Texcoord,
		&pep_stride,
		&pep_offset
	);  // => Like GL_DYNAMIC_DRAW

	pep_gpID3D11DeviceContext->PSSetShaderResources(0, 1, &pep_Inner_gpID3D11ShaderResourceView);

	pep_gpID3D11DeviceContext->PSSetSamplers(0, 1, &pep_Outer_gpID3D11SamplerState);

	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
		/*D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP*/
	);

	XMMATRIX worldMatrix = XMMatrixIdentity(); // (DirectX) worldMatrix = modelMatrix(OpenGL)
	XMMATRIX viewMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f); // (DirectX) 3.0f i.e +Z Axis = -3.0f i.e -Z Axis (OpenGL)

	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * pep_gpPerspeoctiveProjectionMatrix;

	// Load The Data Into Constant Buffer
	CBUFFER pep_ConstantBuffer;
	pep_ConstantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_Outer_gpID3D11Buffer_ConstantBuffer,
		0, // Slot In Shader
		NULL,
		&pep_ConstantBuffer,
		0,
		0
	);

	// Draw Vertex Buffer To Render Buffer
	pep_gpID3D11DeviceContext->Draw(
		6, // No Of Vertices
		0 // Start Of Vertex Position Location
	);

	return;
}

void Inner_Display(void)
{
	//
	// Code
	//
	pep_gpID3D11DeviceContext->VSSetShader(
		pep_Inner_gpID3D11VertexShader,
		0,
		0
	);

	pep_gpID3D11DeviceContext->PSSetShader(
		pep_Inner_gpID3D11PixelShader,
		0,
		0
	);

	pep_gpID3D11DeviceContext->IASetInputLayout(
		pep_Inner_gpID3D11InputLayout
	);

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Rectangle_Postion;
	pep_gpID3D11DeviceContext->Map(
		pep_Inner_gpId3D11Buffer_Cube_Position,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Rectangle_Postion
	);

	memcpy(pep_MappedSubResource_Rectangle_Postion.pData, inner_vertices_cube, sizeof(inner_vertices_cube));

	pep_gpID3D11DeviceContext->Unmap(
		pep_Inner_gpId3D11Buffer_Cube_Position,
		NULL
	);

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_Rectangle_Texcoord;
	pep_gpID3D11DeviceContext->Map(
		pep_Inner_gpId3D11Buffer_Cube_Texcoord,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_Rectangle_Texcoord
	);

	memcpy(
		pep_MappedSubResource_Rectangle_Texcoord.pData,
		inner_texcoord_cube,
		sizeof(inner_texcoord_cube)
	);
	pep_gpID3D11DeviceContext->Unmap(
		pep_Inner_gpId3D11Buffer_Cube_Texcoord,
		NULL
	);

	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0,
		1,
		&pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer
	);

	XMMATRIX translationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvpMatrix = XMMatrixIdentity();
	CBUFFER pep_ConstantBuffer;

	// Cube
	UINT pep_stride = sizeof(float) * 3;
	UINT pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0,
		1,
		&pep_Inner_gpId3D11Buffer_Cube_Position,
		&pep_stride,
		&pep_offset
	);

	pep_stride = sizeof(float) * 2;
	pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		1,
		1,
		&pep_Inner_gpId3D11Buffer_Cube_Texcoord,
		&pep_stride,
		&pep_offset
	);

	pep_gpID3D11DeviceContext->PSSetShaderResources(
		0,
		1,
		&pep_Inner_gpID3D11ShaderResourceView_Texture_Cube
	);

	pep_gpID3D11DeviceContext->PSSetSamplers(
		0,
		1,
		&pep_Inner_gpID3D11SamplerState_Texture_Cube
	);

	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);

	translationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(
		0.0f,
		0.0f,
		5.0f
	);

	rotationMatrix = XMMatrixRotationX(
		-pep_Inner_gCube_Rotation
	);
	rotationMatrix = rotationMatrix * XMMatrixRotationY(
		-pep_Inner_gCube_Rotation
	);
	rotationMatrix = rotationMatrix * XMMatrixRotationZ(
		-pep_Inner_gCube_Rotation
	);

	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();

	worldMatrix = rotationMatrix * translationMatrix;

	wvpMatrix = worldMatrix * viewMatrix * pep_gpPerspeoctiveProjectionMatrix;

	pep_ConstantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer,
		0,
		NULL,
		&pep_ConstantBuffer,
		0,
		0
	);

	pep_gpID3D11DeviceContext->Draw(4, 0);
	pep_gpID3D11DeviceContext->Draw(4, 4);
	pep_gpID3D11DeviceContext->Draw(4, 8);
	pep_gpID3D11DeviceContext->Draw(4, 12);
	pep_gpID3D11DeviceContext->Draw(4, 16);
	pep_gpID3D11DeviceContext->Draw(4, 20);

	return;
}

void Display(void)
{
	//
	// Function Declarations
	//
	void Inner_Display(void);
	void Outer_Display(void);

	//
	// Code
	//
	pep_gpID3D11DeviceContext->OMSetRenderTargets( // BackBuffer -> Step 3
		1,
		&pep_Inner_gpID3D11RenderTargetView,
		NULL
	);

	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1,
		&pep_Inner_gpID3D11RenderTargetView,
		pep_gpID3D11DepthStencilView
	);

	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_Inner_gpID3D11RenderTargetView,
		pep_Inner_gClearColor
	);

	pep_gpID3D11DeviceContext->ClearDepthStencilView(
		pep_gpID3D11DepthStencilView,
		D3D11_CLEAR_DEPTH,
		1.0f,
		0
	);

	Inner_Display();
	
	pep_gpID3D11DeviceContext->OMSetRenderTargets( // BackBuffer -> Step 3
		1,
		&pep_Outer_gpID3D11RenderTargetView,
		NULL
	);

	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1,
		&pep_Outer_gpID3D11RenderTargetView,
		pep_gpID3D11DepthStencilView
	);

	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_Outer_gpID3D11RenderTargetView,
		pep_Outer_gClearColor
	);

	pep_gpID3D11DeviceContext->ClearDepthStencilView(
		pep_gpID3D11DepthStencilView,
		D3D11_CLEAR_DEPTH,
		1.0f,
		0
	);

	Outer_Display();

	pep_gpIDXGISwapChain->Present( // BackBuffer is presented -> Step 4
		0, 
		0
	);

	/*
		1. Creating another texture, creating a render target view to it, and
		binding it to the OM stage of the rendering pipeline.
		2. Using Different Off-Screen Buffer Instead Of Back Buffer  -> This Technique Is Call As Render To Texture.
	*/
	return;
}

void Inner_Update(void)
{
	//
	// Code
	//
	if (pep_Inner_gCube_Rotation > 360)
	{
		pep_Inner_gCube_Rotation = 0.0f;
	}
	pep_Inner_gCube_Rotation += 0.00025f;

	return;
}

void Update(void)
{
	//
	// Function Clearations
	//
	void Inner_Update(void);

	Inner_Update();

	return;
}

HRESULT Initialize(void)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	HRESULT Load3DTexture(const wchar_t*, ID3D11ShaderResourceView **);
	HRESULT Inner_Initialize(void);
	HRESULT Outer_Initialize(void);

	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;
	D3D_DRIVER_TYPE pep_d3dDriverType;
	D3D_DRIVER_TYPE pep_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//
	// Code
	//
	numDriverTypes = sizeof(pep_d3dDriverTypes) / sizeof(pep_d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC pep_dxgiSwapChainDesc;
	ZeroMemory(
		&pep_dxgiSwapChainDesc,
		sizeof(DXGI_SWAP_CHAIN_DESC)
	);

	pep_dxgiSwapChainDesc.BufferCount = 1;
	pep_dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH;
	pep_dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT;
	pep_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	pep_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	pep_dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	pep_dxgiSwapChainDesc.SampleDesc.Count = 1;
	pep_dxgiSwapChainDesc.SampleDesc.Quality = 0;
	pep_dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT index = 0; index < numDriverTypes; index++)
	{
		pep_d3dDriverType = pep_d3dDriverTypes[index];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			pep_d3dDriverType,
			NULL,
			createDeviceFlags,
			&pep_d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&pep_dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&pep_d3dFeatureLevel_acquired,
			&pep_gpID3D11DeviceContext
		);
		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Succeeded\n");
		fclose(pep_gpFile);
	}

	// Inner 
	hr = Inner_Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Inner_Initialize Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Inner_Initialize Succeeded\n");
		fclose(pep_gpFile);
	}

	// Inner 
	hr = Outer_Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Outer_Initialize Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Outer_Initialize Succeeded\n");
		fclose(pep_gpFile);
	}

	// Rasterizer Culling Off
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = pep_gpID3D11Device->CreateRasterizerState(
		&rasterizerDesc,
		&pep_gpID3D11RasterizerState
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRasterizerState Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRasterizerState Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->RSSetState(pep_gpID3D11RasterizerState);

	pep_gpPerspeoctiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Succeeded\n");
		fclose(pep_gpFile);
	}

	return S_OK;
}

HRESULT Load3DTexture(const wchar_t *textureFileName, ID3D11ShaderResourceView **ppID3D11ShaderResourceView )
{
	HRESULT hr;

	hr = DirectX::CreateWICTextureFromFile(pep_gpID3D11Device, pep_gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateWICTextureFromFile Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateWICTextureFromFile Succeeded\n");
		fclose(pep_gpFile);
	}

	return hr;
}

HRESULT Outer_Initialize(void)
{
	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;

	//
	// Code
	//
	const char* pep_szVertexshaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix;" \
		"}" \

		"struct VertexDataOutput" \
		"{" \
		"float4 position : SV_POSITION;" \
		"float2 texcoord : TEXCOORD;"
		"};" \

		"VertexDataOutput main(float4 pos : POSITION, float2 texcoord : TEXCOORD)" \
		"{" \
		"VertexDataOutput output;" \

		"output.position = mul(worldViewProjectionMatrix, pos);" \
		"output.texcoord = texcoord;" \

		"return (output);" \
		"}";

	ID3DBlob* pep_pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob* pep_pID3DBlobError = NULL;

	hr = D3DCompile(
		pep_szVertexshaderSourceCode,
		lstrlenA(pep_szVertexshaderSourceCode) + 1, // +1 For Nul Char -> DirectX Required Length Including nul Char
		"VS", // VS = Vertex Shader
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main", // Entry Function
		"vs_5_0", //Version Of Shader Language
		0, // How To Compile , Zero Means Default
		0, // Shader Do Not Have "effect constant" Hence Zero
		&pep_pID3DBlob_VertexShaderCode,
		&pep_pID3DBlobError
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlobError)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pep_pID3DBlobError->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlobError->Release();
			pep_pID3DBlobError = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	// (DirectX) CreateVertexShader = glCreateShader (OpenGL)
	hr = pep_gpID3D11Device->CreateVertexShader(
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL, // No Class Linkage Variables(e.g extern) Across The Shaders Hence NULL. If Any Such Variables Then Mention Them Here
		&pep_Outer_gpID3D11VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Failed.\n");
		fclose(pep_gpFile);
		return (hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Succeeded.\n");
		fclose(pep_gpFile);
	}

	ID3DBlob* pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlobError = NULL;

	const char* pep_szPixelShaderSourceCode =
		"Texture2D myTexture2D;" \
		"SamplerState mySamplerState;" \

		"float4 main(float4 pos : SV_POSITION, float2 texcoord : TEXCOORD) : SV_TARGET" \
		"{" \
		"float4 color = myTexture2D.Sample(mySamplerState, texcoord);" \
		"return (color);" \
		"}";

	hr = D3DCompile(
		pep_szPixelShaderSourceCode,
		lstrlenA(pep_szPixelShaderSourceCode) + 1, // +1 For nul char, DirectX Requires Length Including nul character
		"PS", // PS -> Pixel Shader
		NULL, // Not used #define hence NULL
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pep_pID3DBlob_PixelShaderCode,
		&pep_pID3DBlobError
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlobError)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			// blob data is in binary format. do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pep_pID3DBlobError->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlobError->Release();
			pep_pID3DBlobError = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	// (DirectX) CreatePixelShader = glCreateShader (OpenGL)
	hr = pep_gpID3D11Device->CreatePixelShader(
		pep_pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pep_pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,  // No Class Linkage Variables(e.g extern) Across The Shaders Hence NULL. If Any Such Variables Then Mention Them Here
		&pep_Outer_gpID3D11PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreatePixelShader Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreatePixelShader Successful.\n");
		fclose(pep_gpFile);
	}

	// InputLayout Relate  Code
	D3D11_INPUT_ELEMENT_DESC pep_inputElementDesc[2];
	ZeroMemory(&pep_inputElementDesc, 2 * sizeof(D3D11_INPUT_ELEMENT_DESC));

	//
	// Per Vertex Data
	// 1. Position
	// 2. Color
	// 3. TexCoord
	// 4. Normals
	//
	// here We Are Only Using Vertex Position
	//
	pep_inputElementDesc[0].SemanticName = "POSITION"; // Map With Vertex Shader "POSITION"
	pep_inputElementDesc[0].SemanticIndex = 0;
	pep_inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT; // Specify The Components In "Vertex Position" Here It Is 3. -> 2nd Parameter Of glVertexAttribPointer
	pep_inputElementDesc[0].InputSlot = 0; // (DirectX) InputSlot = layout (OpenGL) (i.e AMC_ATTRIBUTE_POSITION)
	pep_inputElementDesc[0].AlignedByteOffset = 0;
	pep_inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_inputElementDesc[0].InstanceDataStepRate = 0;

	pep_inputElementDesc[1].SemanticName = "TEXCOORD"; // TEXCOORD: used in shader as binding
	pep_inputElementDesc[1].SemanticIndex = 0; // 1st(0th index) in Texture Buffer
	pep_inputElementDesc[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	pep_inputElementDesc[1].InputSlot = 1;
	pep_inputElementDesc[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	pep_inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_inputElementDesc[1].InstanceDataStepRate = 0;

	// Similar To glBindAttribLocation (OpenGL)
	hr = pep_gpID3D11Device->CreateInputLayout(
		pep_inputElementDesc, // 1st Parameter Of glBufferData
		_ARRAYSIZE(pep_inputElementDesc),
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		&pep_Outer_gpID3D11InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateInputLayout Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateInputLayout Successful.\n");
		fclose(pep_gpFile);
	}

	// Release Blob's
	pep_pID3DBlob_PixelShaderCode->Release();
	pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_VertexShaderCode->Release();
	pep_pID3DBlob_VertexShaderCode = NULL;

	// Step 7: Copy Vertex Data From CPU To GPU
	D3D11_BUFFER_DESC pep_bufferDesc_VertexBuffer;
	ZeroMemory(&pep_bufferDesc_VertexBuffer, sizeof(D3D11_BUFFER_DESC));

	pep_bufferDesc_VertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_VertexBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(Outer_vertices);
	pep_bufferDesc_VertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER; // 1st Parameter Of glVertexAttribPointer i.e AMC_ATTRIBUTE_POSITION or AMC_ATTRIBUTE_NORMAL etc...
	pep_bufferDesc_VertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; // Similar To glBufferData. If Not Specify Then Vertices Will Not Pass From CPU To GPU

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexBuffer, // Buffer Description
		NULL, // NULL Means Dynamic Draw 
		&pep_Outer_gpID3D11Buffer_VertexBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer VertexBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer VertexBuffer Successful.\n");
		fclose(pep_gpFile);
	}

	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Cube_Texcoord;
	ZeroMemory(&bufferDesc_VertexBuffer_Cube_Texcoord,
		sizeof(D3D11_BUFFER_DESC));
	bufferDesc_VertexBuffer_Cube_Texcoord.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Cube_Texcoord.ByteWidth =
		sizeof(float) * 12 /* _ARRAYSIZE(texcoord_cube)*/;
	bufferDesc_VertexBuffer_Cube_Texcoord.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Cube_Texcoord.CPUAccessFlags =
		D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&bufferDesc_VertexBuffer_Cube_Texcoord,
		NULL,
		&pep_Outer_gpID3D11Buffer_Quad_Texcoord);
	if (FAILED(hr)) {
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Texcoord Failed\n");
		fclose(pep_gpFile);
		return (hr);
	} else {
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ID3D11Device:CreateBuffer-Texcoord Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_BUFFER_DESC pep_bufferDesc_ConstantBuffer;
	ZeroMemory(
		&pep_bufferDesc_ConstantBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);
	pep_bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);  // mapped with cbuffer of vertex shader
	pep_bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // // Bind a buffer as a constant buffer to a shader stage

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_ConstantBuffer,
		nullptr,
		&pep_Outer_gpID3D11Buffer_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer ConstantBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer ConstantBuffer Successful.\n");
		fclose(pep_gpFile);
	}

	// Create Sampler State
	D3D11_SAMPLER_DESC pep_samplerDesc;
	ZeroMemory(&pep_samplerDesc, sizeof(D3D11_SAMPLER_DESC));

	pep_samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	pep_samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	pep_samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	pep_samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = pep_gpID3D11Device->CreateSamplerState(&pep_samplerDesc, &pep_Outer_gpID3D11SamplerState);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Outer CreateSamplerState Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Outer CreateSamplerState Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_Outer_gClearColor[0] = 0.0f;
	pep_Outer_gClearColor[1] = 0.0f;
	pep_Outer_gClearColor[2] = 0.0f;
	pep_Outer_gClearColor[3] = 0.0f;

	return S_OK;
}

HRESULT Inner_Initialize(void)
{

	HRESULT hr = S_OK;

	//
	// Vertex Shader
	//
	const char* pep_szVertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix;" \
		"}" \

		"struct VertexDataOutput" \
		"{" \
		"float4 position : SV_POSITION;" \
		"float2 texcoord : TEXCOORD;"
		"};" \

		"VertexDataOutput main(float4 pos : POSITION, float2 texcoord : TEXCOORD)" \
		"{" \
		"VertexDataOutput output;" \

		"output.position = mul(worldViewProjectionMatrix, pos);" \
		"output.texcoord = texcoord;" \

		"return (output);" \
		"}";

	ID3DBlob* pep_pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob* pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_szVertexShaderSourceCode,
		lstrlenA(pep_szVertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pep_pID3DBlob_VertexShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreateVertexShader(
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,
		&pep_Inner_gpID3D11VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Succeeded\n");
		fclose(pep_gpFile);
	}

	//
	// Pixel Shader
	//
	const char* pep_szPixelShaderSourceCode =
		"Texture2D myTexture2D;" \
		"SamplerState mySamplerState;" \

		"float4 main(float4 pos : SV_POSITION, float2 texcoord : TEXCOORD) : SV_TARGET" \
		"{" \
		"float4 color = myTexture2D.Sample(mySamplerState, texcoord);" \
		"return (color);" \
		"}";

	ID3DBlob* pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_szPixelShaderSourceCode,
		lstrlenA(pep_szPixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pep_pID3DBlob_PixelShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreatePixelShader(
		pep_pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pep_pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&pep_Inner_gpID3D11PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_INPUT_ELEMENT_DESC pep_InputElementDesc[2];
	ZeroMemory(
		pep_InputElementDesc,
		sizeof(D3D11_INPUT_ELEMENT_DESC) * 2
	);

	pep_InputElementDesc[0].SemanticName = "POSITION";
	pep_InputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_InputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	pep_InputElementDesc[0].InputSlot = 0;
	pep_InputElementDesc[0].AlignedByteOffset = 0;
	pep_InputElementDesc[0].SemanticIndex = 0;
	pep_InputElementDesc[0].InstanceDataStepRate = 0;

	pep_InputElementDesc[1].SemanticName = "TEXCOORD";
	pep_InputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_InputElementDesc[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	pep_InputElementDesc[1].InputSlot = 1;
	pep_InputElementDesc[1].AlignedByteOffset = 0;
	pep_InputElementDesc[1].SemanticIndex = 0;
	pep_InputElementDesc[1].InstanceDataStepRate = 0;

	hr = pep_gpID3D11Device->CreateInputLayout(
		pep_InputElementDesc,
		_ARRAYSIZE(pep_InputElementDesc),
		pep_pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_VertexShaderCode->GetBufferSize(),
		&pep_Inner_gpID3D11InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_pID3DBlob_PixelShaderCode->Release();
	pep_pID3DBlob_PixelShaderCode = NULL;
	pep_pID3DBlob_VertexShaderCode->Release();
	pep_pID3DBlob_VertexShaderCode = NULL;

	D3D11_BUFFER_DESC pep_bufferDesc_Rectangle_Position;
	ZeroMemory(
		&pep_bufferDesc_Rectangle_Position,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Rectangle_Position.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Rectangle_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Rectangle_Position.ByteWidth = sizeof(float) * _ARRAYSIZE(inner_vertices_cube);
	pep_bufferDesc_Rectangle_Position.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Rectangle_Position,
		NULL,
		&pep_Inner_gpId3D11Buffer_Cube_Position
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_BUFFER_DESC pep_bufferDesc_Rectangle_Texcoord;
	ZeroMemory(
		&pep_bufferDesc_Rectangle_Texcoord,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_Rectangle_Texcoord.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_Rectangle_Texcoord.ByteWidth = sizeof(float) * _ARRAYSIZE(inner_texcoord_cube);
	pep_bufferDesc_Rectangle_Texcoord.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_Rectangle_Texcoord.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_Rectangle_Texcoord,
		NULL,
		&pep_Inner_gpId3D11Buffer_Cube_Texcoord
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexColorBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer Rectangle VertexColorBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	D3D11_BUFFER_DESC pep_bufferDesc_VertexShaderConstantBuffer;
	ZeroMemory(
		&pep_bufferDesc_VertexShaderConstantBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_VertexShaderConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_VertexShaderConstantBuffer.ByteWidth = sizeof(CBUFFER);
	pep_bufferDesc_VertexShaderConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexShaderConstantBuffer,
		NULL,
		&pep_Inner_gpID3D11Buffer_VertexShader_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer ConstantBuffer Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer ConstantBuffer Succeeded\n");
		fclose(pep_gpFile);
	}

	hr = Load3DTexture(L"Vijay_Kundali.bmp", &pep_Inner_gpID3D11ShaderResourceView_Texture_Cube);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Load3DTexture Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Load3DTexture Succeeded\n");
		fclose(pep_gpFile);
	}

	// Create Sampler State
	D3D11_SAMPLER_DESC pep_samplerDesc;
	ZeroMemory(&pep_samplerDesc, sizeof(D3D11_SAMPLER_DESC));

	pep_samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	pep_samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	pep_samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	pep_samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = pep_gpID3D11Device->CreateSamplerState(&pep_samplerDesc, &pep_Inner_gpID3D11SamplerState_Texture_Cube);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateSamplerState Failed\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateSamplerState Succeeded\n");
		fclose(pep_gpFile);
	}

	pep_Inner_gClearColor[0] = 0.0f;
	pep_Inner_gClearColor[1] = 0.0f;
	pep_Inner_gClearColor[2] = 1.0f;
	pep_Inner_gClearColor[3] = 0.0f;

	return S_OK;
}
