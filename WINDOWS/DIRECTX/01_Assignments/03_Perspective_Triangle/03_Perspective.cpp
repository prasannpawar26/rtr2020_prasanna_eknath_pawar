#include <windows.h>
#include <stdio.h>
// DirectX Specific Header File
#include <d3d11.h>
#include <d3dcompiler.h>  // For shader compilation

#pragma warning (disable: 4838)
#include "XNAMath/xnamath.h" // This file is included in all other .inl files

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool pep_gbActiveWindow = false;
bool pep_gbFullScreen = false;
bool pep_gbEscapeKeyPressed = false;

float pep_gClearColor[4]; // RGBA -> Similar To glClear

IDXGISwapChain *pep_gpIDXGISwapChain = NULL; // storing (i.e buffers front and back) rendered data before presenting it to an output
ID3D11Device *pep_gpID3D11Device = NULL;
ID3D11DeviceContext *pep_gpID3D11DeviceContext = NULL; // Similar To wglDeviceContext, generates rendering commands
ID3D11RenderTargetView *pep_gpID3D11RenderTargetView = NULL; // Similar To Frame Buffer

ID3D11InputLayout *pep_gpID3D11InputLayout = NULL;
// Vertex Shader Related Variables
ID3D11VertexShader *pep_gpID3D11VertexShader = NULL;
ID3D11Buffer *pep_gpID3D11Buffer_VertexBuffer = NULL;
// Pixel Shader Related Variables
ID3D11PixelShader *pep_gpID3D11PixelShader = NULL;
// Uniforms
ID3D11Buffer *pep_gpID3D11Buffer_ConstantBuffer = NULL;

// Similar To Uniforms
struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix; // (DirectX)world = model(OpenGL)
};

XMMATRIX pep_gPerspectiveProjectionMatrix;  // (DirectX: XNAMath)XMATRIX = MATH(OpenGL:vmath)

//XMMATRIX Internally not float4X4 but it is mapped to float4X4

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("DirectX- Prespective");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.lpfnWndProc = WndProc;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("DirectX- Prespective"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;

	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Game Loop
	//
	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapeKeyPressed)
				{
					pep_bDone = true;
				}
			}

			Display();
		}
	}

	//
	// Call Uninitialize
	//
	Uninitialize();

	return ((int)pep_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Code
	//

	switch (iMsg)
	{
	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		//
		// Call Resize Only When We Have Device Context
		//
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr); // HRESULT -> Get Upcasted Here To LONG.
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapeKeyPressed)
			{
				pep_gbEscapeKeyPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	return;
}

HRESULT Initialize(void)
{
	// 
	// Function Declarations
	//
	HRESULT Resize(int, int);

	//
	// Variable Declarations
	//
	HRESULT hr;
	D3D_DRIVER_TYPE pep_d3dDriverType;
	D3D_DRIVER_TYPE pep_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE, // Hardware Rendering
		D3D_DRIVER_TYPE_WARP,
		//D3D_DRIVER_TYPE_SOFTWARE, // Software Rendering
		D3D_DRIVER_TYPE_REFERENCE
		
	};

	// (DirectX)Feature Level = Extension(OpenGL)
	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//
	// Code
	//
	numDriverTypes = sizeof(pep_d3dDriverTypes) / sizeof(pep_d3dDriverTypes[0]);

	//Swap Chain Descriptor
	DXGI_SWAP_CHAIN_DESC pep_dxgiSwapChainDesc;
	ZeroMemory(&pep_dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	// STEP 1 : Create Back Buffer, Device Context, Swap Chain
	pep_dxgiSwapChainDesc.BufferCount = 1; // DirectX Gives 1 Buffer By Default (i.e Front Buffer), Hence We Only Required/Create One Extra Buffer(i.e Back Buffer) => It Means Total 2 Buffers
	pep_dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH; // Back Buffer Width
	pep_dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT; // Back Buffer Height
	pep_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //  Similar To PixelChangeDescriptor In OpenGL
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60 ; // FPS in OpenGL
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	pep_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // Buffer Type Or Used For Render Target Output
	pep_dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	pep_dxgiSwapChainDesc.SampleDesc.Count = 1;
	pep_dxgiSwapChainDesc.SampleDesc.Quality = 0; // Use Default
	pep_dxgiSwapChainDesc.Windowed = TRUE; // Windowing + FullScreen

	//
	// Few Function Of OpenGL Are Replaced By 'dxgiSwapChainDesc' (and D3D11CreateDeviceAndSwapChain) like
	// GetDC
	// PixelFormatDecriptor
	// ChoosePixelFormat
	// SetPixelFormat
	// wglCreateContext
	// wglMakeCurrent
	// glewInit 
	//
	
	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		pep_d3dDriverType = pep_d3dDriverTypes[driverTypeIndex];

		//
		// After Successful Call Of D3D11CreateDeviceAndSwapChain Function We Will Have Following
		// Creates a device that represents the display adapter
		// 1. DeviceContext
		// 2. Back Buffer
		// 3. Device
		// 4. Swap Chain
		//
		hr = D3D11CreateDeviceAndSwapChain(
			NULL, // Primar Graphics Card
			pep_d3dDriverType,
			NULL, // No Software Rasterizer Hence NULL
			createDeviceFlags,
			&pep_d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&pep_dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&pep_d3dFeatureLevel_acquired,
			&pep_gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "D3D11CreateDeviceAndSwapChain Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Shader Related Code
	//

	// No Build In Variables In DirectX

	/*
	// cbuffer -> keyword
	// cbuffer mapped with CBUFFER
	// worldViewProjectionMatrix mapped with WorldViewProjectionMatrix
	// Similar To Uniform  => cbuffer ConstantBuffer

	"cbuffer ConstantBuffer" \
	"{" \
	"float4x4 worldViewProjectionMatrix;" \
	"}" \

	// (DirectX)float4 main() = void main() (OpenGL)
	// In opengl IN act as input parameter and OUt at as return value
	// In DirectX main function has input parameters and return value 
	// (DirectX)float pos: POSITION = gl_Position(OpenGL)
	// (DirextX)POSITION = AMC_ATTRIBUTE_POSITION (OpenGL)
	// (DirectX) pos = vPosition(OpenGL)
	// SV_POSITION Is Only HLSL(High Level Shading Language) Keyword

	"float4 main(float pos: POSITION) : SV_POSITION" \
	"{" \
	"float4 position = mul(worldViewProjectionMatrix, pos)" \
	"return (position);" \
	"}";
	*/

	//
	// Shader Related Calls
	// 1. Shader Source Code
	// 2. Compilation -> D3DCompile
	// 3. Create Shader -> Create***Shader (e.g CreateVertexShader)
	// 4. Attach -> **SetShader (e.g VSSetShader)
	//

	const char *pep_vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
			"float4x4 worldViewProjectionMatrix;" \
		"}" \

		"float4 main(float4 pos : POSITION) : SV_POSITION" \
		"{" \
			"float4 position = mul(worldViewProjectionMatrix, pos);" \
			"return (position);" \
		"}";

	//
	// STEP 3 : Compile Shader
	//

	//
	// In DirectX 3D All Shaders Called As "Effects".
	// In DirectX 3D Shaders Can Be Compile Offline As Well Using FXC.exe
	// FX In FXC Stands For Effects
	// Nothing Similar To FXC.exe In OpenGL
	// D3DCompileFormFile -> For File Base Shader(s)
	//
	// (DirectX)D3DCompile = glCompileShader (OpenGL)
	//
	ID3DBlob *pep_pID3DBlob_vertexShaderCode = NULL;
	ID3DBlob *pep_pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pep_vertexShaderSourceCode,
		lstrlenA(pep_vertexShaderSourceCode) + 1, // +1 For nul char and shader in ASCII char. DirectX Requires Length Including nul character
		"VS", // VS => Vertex Shader
		NULL, 
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // Default , If #define Was There the It Could Have Been Different
		"main",  // Entry Point Of Shader Program
		"vs_5_0", // Version Of Shader language
		0, //  How To Compile Shader Program . Zero Means Use Default
		0, // Shader Do Not Have "effect constant" Hence Zero
		&pep_pID3DBlob_vertexShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			// blob data is in binary format. do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	//
	// STEP 4 : Create Shader Object
	//

	// (DirextX) CreateVertexShader = glCreateShader(OpenGL)
	hr = pep_gpID3D11Device->CreateVertexShader(
		pep_pID3DBlob_vertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_vertexShaderCode->GetBufferSize(),
		NULL,  // If We Are Using Any (Class Linkage similar to extern) Variable Across Shader Then Mention Them Here. As In This shader We Do Not Have Such Variable Hence NULL
		&pep_gpID3D11VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateVertexShader Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateVertexShader Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// STEP 5 : Attach/Plug Shader
	//

	// Set Vertex Shader In Pipeline
	//
	// (DirectX) VSSetShader = glAttachShader(OpenGL)
	pep_gpID3D11DeviceContext->VSSetShader(
		pep_gpID3D11VertexShader,
		0, // Shader Variable Array (Class Linkage Variables). Here In This Shader We Do Not Such Variable Hence Zero Or NULL. // Use NULL Instead Of Zero
		0 // Use NULL Instead Of Zero
	);

	ID3DBlob *pep_pID3DBlob_pixelShaderCode = NULL;
	pep_pID3DBlob_Error = NULL;

	//
	// (DirectX) SV_TARGET = Out (OpenGL)
	// (DirectX) return = Out(OpenGL)
	//
	// (DirectX) "return (float4(1.0f, 1.0f, 1.0f, 1.0f))" = FragColor (OpenGL)
	//
	const char *pep_pixelShaderSourceCode =
		"float4 main(void) : SV_TARGET" \
		"{" \
			"return (float4(1.0f, 1.0f, 1.0f, 1.0f));" \
		"}";

	//
	// In DirectX 3D All Shaders Called As "Effects".
	// In DirectX 3D Shaders Can Be Compile Offline As Well Using FXC.exe
	// FX In FXC Stands For Effects
	// Nothing Similar To FXC.exe In OpenGL
	// D3DCompileFormFile -> For File Base Shader(s)
	//
	// (DirectX)D3DCompile = glCompileShader (OpenGL)
	//
	hr = D3DCompile(
		pep_pixelShaderSourceCode,
		lstrlenA(pep_pixelShaderSourceCode) + 1, // +1 For nul char and shader in ASCII char. DirectX Requires Length Including nul character
		"PS", // PS => Pixel Shader
		NULL, // Not Used #define Hence NULL
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // Default , If #define Was There the It Could Have Been Different
		"main", // Entry Point Function
		"ps_5_0",
		0, // How To Compile Shader Program . Zero Means Use Default
		0, // Shader Do Not Have "effect constant" Hence Zero
		&pep_pID3DBlob_pixelShaderCode,
		&pep_pID3DBlob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3DBlob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			// blob data is in binary format. do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pep_pID3DBlob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3DBlob_Error->Release();
			pep_pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	// (DirextX) CreatePixelShader = glCreateShader(OpenGL)
	hr = pep_gpID3D11Device->CreatePixelShader(
		pep_pID3DBlob_pixelShaderCode->GetBufferPointer(),
		pep_pID3DBlob_pixelShaderCode->GetBufferSize(),
		NULL, // If We Are Using Any (Class Linkage similar to extern) Variable Across Shader Then Mention Them Here. As In This shader We Do Not Have Such Variable Hence NULL
		&pep_gpID3D11PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreatePixelShader Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreatePixelShader Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Set Pixel Shader In Pipeline
	//
	// (DirectX) PSSetShader = glAttachShader(OpenGL)
	//
	pep_gpID3D11DeviceContext->PSSetShader(
		pep_gpID3D11PixelShader,
		0, // Shader Variable Array (Class Linkage Variables). Here In This Shader We Do Not Such Variable Hence Zero Or NULL. // Use NULL Instead Of Zero
		0 // Use NULL Instead Of Zero
	);

	// STEP 6 : Create Mapping Of CPU And GPU Global Variable For Data Transfer

	// InputLayout Related Code
	D3D11_INPUT_ELEMENT_DESC pep_inputElementDesc;
	//ZeroMemory(&pep_inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	//
	// Per Vertex Data
	// 1. Position
	// 2. Color
	// 3. TexCoord
	// 4. Normal
	//
	// Here We Are Only Using Vertex Position
	//
	pep_inputElementDesc.SemanticName = "POSITION"; // Map With Vertex Shader "POSITION"
	pep_inputElementDesc.SemanticIndex = 0;
	pep_inputElementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT; // Specify The Components In "Vertex Position" Here It Is 3. -> 2nd Parameter Of glVertexAttribPointer
	pep_inputElementDesc.InputSlot = 0; // (DirectX)InputSlot = layout(OpenGL)  (i.e AMC_ATTRIBUTES_POSITION)
	pep_inputElementDesc.AlignedByteOffset = 0;
	pep_inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_inputElementDesc.InstanceDataStepRate = 0;

	// Similar To  glBindAttribLocation(OpenGL)
	hr = pep_gpID3D11Device->CreateInputLayout(
		&pep_inputElementDesc, // 1st Parameter Of glBufferData
		1,
		pep_pID3DBlob_vertexShaderCode->GetBufferPointer(),
		pep_pID3DBlob_vertexShaderCode->GetBufferSize(),
		&pep_gpID3D11InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateInputLayout Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateInputLayout Successful.\n");
		fclose(pep_gpFile);
	}

	// (DirectX) IASetInputLayout = glBindAttribLocation (OpenGL)
	pep_gpID3D11DeviceContext->IASetInputLayout( // IA = Input Assembly Stage
		pep_gpID3D11InputLayout
	);

	// Release Blob's
	pep_pID3DBlob_pixelShaderCode->Release();
	pep_pID3DBlob_pixelShaderCode = NULL;
	pep_pID3DBlob_vertexShaderCode->Release();
	pep_pID3DBlob_vertexShaderCode = NULL;

	// vertex-position data - in clockwise order
	float vertices[] = {
		0.0f, 1.0f, 0.0,
		1.0f, -1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f
	};

	// STEP 7 : Copy Vertex Data From CPU And GPU.

	// Create Vertex Buffer
	D3D11_BUFFER_DESC pep_bufferDesc_VertexBuffer;
	ZeroMemory(&pep_bufferDesc_VertexBuffer, sizeof(D3D11_BUFFER_DESC));

	pep_bufferDesc_VertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_VertexBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices);
	pep_bufferDesc_VertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER; // 1st Parameter Of glVertexAttribPointer i.e AMC_ATTRIBUTE_POSITION or AMC_ATTRIBUTE_NORMAL etc...
	pep_bufferDesc_VertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; // Similar To glBufferData . If Not Specify Then Vertices Will Not Pass From CPU To GPU

	// Set Buffer of Below Is Done In Draw Because We Are Using Like
	//
	// IASetVertexBuffers  => This Will Be Used In Dispaly Function To Send Vertex Data To Spcific Slot
	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexBuffer, // Buffer Description
		NULL, // Passing NULL Means DYNAMIC_DRAW
		&pep_gpID3D11Buffer_VertexBuffer // Vertex Buffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Successful.\n");
		fclose(pep_gpFile);
	}

	// Copy Vertex Position Data Into pep_gpID3D11Buffer_VertexBuffer
	D3D11_MAPPED_SUBRESOURCE pep_mappedSubResource; // Display Driver Does CPU To GPU Memory Mapping
	ZeroMemory(&pep_mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	pep_gpID3D11DeviceContext->Map(
		pep_gpID3D11Buffer_VertexBuffer,
		NULL, // Zero 0
		D3D11_MAP_WRITE_DISCARD,
		NULL, // Zero 0
		&pep_mappedSubResource
	);

	memcpy(
		pep_mappedSubResource.pData,
		vertices,
		sizeof(vertices)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpID3D11Buffer_VertexBuffer,
		NULL
	);

	//
	// STEP 8 : Mapping Of Uniform Variables.
	//

	// Define And Set Constant Buffer
	// Similar To glUniformLocation In OpenGL
	D3D11_BUFFER_DESC pep_bufferDesc_ConstantBuffer;
	ZeroMemory(&pep_bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	pep_bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER); // mapped with cbuffer of vertex shader
	pep_bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // Bind a buffer as a constant buffer to a shader stage

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_ConstantBuffer,
		nullptr,
		&pep_gpID3D11Buffer_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateBuffer Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Similar To glGetUniformLocation Of OpenGL
	//
	// CBUFFER Gets Mapped With ConstantBuffer In Vertex Shader
	//
	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0, // Which Slot In Shader
		1, // No. Of Buffers
		&pep_gpID3D11Buffer_ConstantBuffer
	);

	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "VSSetConstantBuffers Successful.\n");
		fclose(pep_gpFile);
	}

	//
	// Similat To glClearColor
	//
	pep_gClearColor[0] = 0.0f;
	pep_gClearColor[1] = 0.0f;
	pep_gClearColor[2] = 1.0f;
	pep_gClearColor[3] = 1.0f;

	//
	// (DirectX)  XMMatrixIdentity = mat4::identity() (OpenGL)
	//
	pep_gPerspectiveProjectionMatrix = XMMatrixIdentity();

	//
	// Call Resize First Time
	//
	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Successful.\n");
		fclose(pep_gpFile);
	}

	return S_OK;
}

void Display(void)
{
	//
	// Code
	//

	//
	// First 0th Index Buffer Gets Render Then 1st Index
	//
	// DirectX (ClearRenderTargetView) =  glClear (OpenGL)
	//
	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_gpID3D11RenderTargetView,
		pep_gClearColor
	);

	// DirectX Is Internally Multi-Threaded

	UINT pep_stride = sizeof(float) * 3;
	UINT pep_offset = 0;

	// Part of Step 7

	// (DirectX) IASetVertexBuffers = glVertexAttribPointer + glEnableVertexAttribArray (OpenGL)
	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0, // Slot (1st Parameter Of glVertexAttribPointer i.e AMC_ATTRIBUTES_POSITION In OpenGL)
		1, // No. Of. Buffer
		&pep_gpID3D11Buffer_VertexBuffer,
		&pep_stride, // 2nd parameter Of glVertexAttributePointer
		&pep_offset
	); // This Function Can Be Call In Initialize As Well (One Time Activity In Case Of "Static Draw")

	// (DirectX) IASetPrimitiveTopology = 1st parameter glDraw* (OpenGL)
	// (DirectX) D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST = GL_TRIANGLES (OpenGL)
	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
	);

	//
	XMMATRIX worldMatrix = XMMatrixIdentity(); // (DirectX) worldMatrix = modelMatrix(OpenGL)
	XMMATRIX viewMatrix = XMMatrixIdentity();

	worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f); // (DirectX) 3.0f i.e +Z Axis = -3.0f (OpenGL) i.e -Z Axis

	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * pep_gPerspectiveProjectionMatrix;

	// Part of Step 8

	// Load The Data Into Constant Buffer
	CBUFFER pep_constantBuffer;
	pep_constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_gpID3D11Buffer_ConstantBuffer,
		0, // Slot In Shader
		NULL,
		&pep_constantBuffer,
		0,
		0
	);

	// Draw Vertex Buffer To Render Target
	pep_gpID3D11DeviceContext->Draw(
		3, // No Of Vertices
		0 // Start Of Vertex Position Location
	);

	//
	// Present(0, 0)
	// 1st Parameter : Talks About Frame Getting Present That Sync With Vertical Refersh Rate Or Not
	// Giving 0 Means Use Default Best And Dont Sync
	// 2nd Parameter : Which Buffer To Display , O Means Display All Buffers In Frames
	//
	pep_gpIDXGISwapChain->Present(0, 0); // Similar To SwapBuffers And glSwapBuffer

	return;
}

HRESULT Resize(int width, int height)
{
	//
	// variable delcarations
	//
	HRESULT hr = S_OK;

	//
	// Code
	//

	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Resize Enter.\n");
		fclose(pep_gpFile);
	}

	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	// Resizing Default Buffer
	hr = pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0 // Get Default Best
	);

	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "ResizeBuffers Success.\n");
		fclose(pep_gpFile);
	}

	// Get back Buffer From Swap Chain

	//
	// We Are Using Texture Buffer/Memory Because
	// 1. Fastest Memory On GPU
	// 2. Most Quickly Accessible Memory On GPU
	//
	ID3D11Texture2D *pep_pID3D11Texture2D_BackBuffer = NULL;
	hr = pep_gpIDXGISwapChain->GetBuffer(
		0, // 0th Index
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pep_pID3D11Texture2D_BackBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "GetBuffer Success.\n");
		fclose(pep_gpFile);
	}

	// (DirectX)RenderTarget = FrameBuffer(OpenGL)
	hr = pep_gpID3D11Device->CreateRenderTargetView(
		pep_pID3D11Texture2D_BackBuffer, // Back Buffer Index IS 0(Zero)
		NULL,
		&pep_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "CreateRenderTargetView Success.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_BackBuffer->Release();
	pep_pID3D11Texture2D_BackBuffer = NULL;

	//
	// Render Target View (RTV) Changes At OM Stage
	//
	// PipeLine Is Control By pep_gpID3D11DeviceContext
	//
	// Now Set Updated Render Target View In The Pipeline
	//
	pep_gpID3D11DeviceContext->OMSetRenderTargets( // OM = Output Merger Stage
		1, // We Have Only One Render Target
		&pep_gpID3D11RenderTargetView,
		NULL
	);

	// Viewport
	D3D11_VIEWPORT d3dViewport;
	d3dViewport.TopLeftX = 0;
	d3dViewport.TopLeftY = 0;
	d3dViewport.Width = (float)width;
	d3dViewport.Height = (float)height;
	d3dViewport.MinDepth = 0.0f;
	d3dViewport.MaxDepth = 1.0f; // Depth Test In OpenGL glDepthTest()

	pep_gpID3D11DeviceContext->RSSetViewports(1, &d3dViewport); // RS = Rasterizer Stage

	// Set Perspective Projection Matrix
	pep_gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(
		XMConvertToRadians(45.0f),
		(float)width / (float)height,
		0.1f,
		100.0f
	);

	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Perspective Projection Matrix Set  Success.\n");
		fclose(pep_gpFile);
	}

	return hr;
}

void Uninitialize(void)
{
	//
	// Code
	//
	if (NULL != pep_gpID3D11Buffer_ConstantBuffer)
	{
		pep_gpID3D11Buffer_ConstantBuffer->Release();
		pep_gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (NULL != pep_gpID3D11Buffer_VertexBuffer)
	{
		pep_gpID3D11Buffer_VertexBuffer->Release();
		pep_gpID3D11Buffer_VertexBuffer = NULL;
	}

	if (NULL != pep_gpID3D11InputLayout)
	{
		pep_gpID3D11InputLayout->Release();
		pep_gpID3D11InputLayout = NULL;
	}

	if (NULL != pep_gpID3D11PixelShader)
	{
		pep_gpID3D11PixelShader->Release();
		pep_gpID3D11PixelShader = NULL;
	}

	if (NULL != pep_gpID3D11VertexShader)
	{
		pep_gpID3D11VertexShader->Release();
		pep_gpID3D11VertexShader = NULL;
	}

	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	if (pep_gpFile)
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "uninitialize Success.\n");
		fprintf_s(pep_gpFile, "Log File Close Success.\n");
		fclose(pep_gpFile);
	}

	return;
}
