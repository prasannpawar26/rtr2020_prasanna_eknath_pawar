#include <stdio.h>
#include <windows.h>
// directx specific header file
#include <d3d11.h>
#include <d3dcompiler.h>  //For Shader Compilation

#pragma warning(disable : 4838)
#include "XNAMath/xnamath.h"  // This File Is Included in All Other .inl File.

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable
FILE *gpFile = NULL;
char gszLogFIleName[] = "Log.txt";

HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

float gClearColor[4];  // RGBA

IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

// Shader Related Variables
ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Cube_Normal = NULL;

ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;

// Depth Related Code
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

float gCubeRotationAngle = 0.0f;

// For Culling OFF
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

// Uniforms
struct CBUFFER {
  XMMATRIX WorldViewMatrix;
  XMMATRIX ProjectionMatrix;
  XMVECTOR Ld;
  XMVECTOR Kd;
  XMVECTOR LightPosition;
  unsigned int KeyPressed;
};

bool gbIsLightOn = false;

XMMATRIX gPerspecctiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {
  // Function Prototypes
  HRESULT Initialize(void);
  void UnInitialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("DirectX11_14-DiffuseLightOnCube");

  // code
  if (0 != fopen_s(&gpFile, gszLogFIleName, "w")) {
    MessageBox(NULL, TEXT("Log FIle Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf_s(gpFile, "Log File Created Successfully\n");
  fclose(gpFile);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadCursor(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("DirectX11_14-DiffuseLightOnCube"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  HRESULT hr;
  hr = Initialize();
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Initialize failed. Exitting Now...\n");
    fclose(gpFile);
    DestroyWindow(hwnd);
    hwnd = NULL;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Initialize succeeded\n");
    fclose(gpFile);
  }

  while (false == bDone) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Update();
      Display();
    }
  }  // End Of While

  UnInitialize();

  return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // function declarations
  void ToggledFullScreen(void);
  void UnInitialize(void);
  HRESULT ReSize(int, int);

  // variable delcarations
  HRESULT hr;

  // code
  switch (iMsg) {
    case WM_DESTROY:
      UnInitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_SIZE: {
      if (NULL != gpID3D11DeviceContext) {
        hr = ReSize(LOWORD(lParam), HIWORD(lParam));
        if (FAILED(hr)) {
          fopen_s(&gpFile, gszLogFIleName, "a+");
          fprintf_s(gpFile, "ReSize failed. Exitting Now...\n");
          fclose(gpFile);
          hwnd = NULL;
        } else {
          fopen_s(&gpFile, gszLogFIleName, "a+");
          fprintf_s(gpFile, "ReSize succeeded\n");
          fclose(gpFile);
        }
      }
    }

    break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case 'L':
        case 'l':
          if (gbIsLightOn)
          {
            gbIsLightOn = false;
          } else {
            gbIsLightOn = true;
          }

          break;

      }
    } break;
  }  // Switch End

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen) {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.left,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }

  return;
}

HRESULT Initialize(void) {
  // function declarations
  void UnInitialize(void);
  HRESULT ReSize(int, int);

  // Variable Declarations
  HRESULT hr;
  D3D_DRIVER_TYPE d3DriverType;
  D3D_DRIVER_TYPE d3DriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE,
                                     D3D_DRIVER_TYPE_WARP,
                                     D3D_DRIVER_TYPE_REFERENCE};
  D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
  D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
  UINT createDeviceflags = 0;
  UINT numDriverTypes = 0;
  UINT numFeatureLevels = 1;

  // Code
  numDriverTypes = sizeof(d3DriverTypes) / sizeof(d3DriverTypes[0]);

  DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
  ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
  dxgiSwapChainDesc.BufferCount = 1;
  dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
  dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
  dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
  dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
  dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  dxgiSwapChainDesc.OutputWindow = gHwnd;
  dxgiSwapChainDesc.SampleDesc.Count = 1;
  dxgiSwapChainDesc.SampleDesc.Quality = 0;
  dxgiSwapChainDesc.Windowed = TRUE;

  for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes;
       driverTypeIndex++) {
    d3DriverType = d3DriverTypes[driverTypeIndex];
    hr = D3D11CreateDeviceAndSwapChain(
        NULL, d3DriverType, NULL, createDeviceflags, &d3dFeatureLevel_required,
        numFeatureLevels, D3D11_SDK_VERSION, &dxgiSwapChainDesc,
        &gpIDXGISwapChain, &gpID3D11Device, &d3dFeatureLevel_acquired,
        &gpID3D11DeviceContext);
    if (SUCCEEDED(hr)) {
      break;
    }
  }  // End Of For Loop

  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain failed.\n");
    fclose(gpFile);
    return hr;
  }

  fopen_s(&gpFile, gszLogFIleName, "a+");
  fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain succeeded\n");
  fprintf_s(gpFile, "The Chosen Driver Is Of: \n");
  if (d3DriverType == D3D_DRIVER_TYPE_HARDWARE) {
    fprintf_s(gpFile, " Hardware Type\n");
  } else if (d3DriverType == D3D_DRIVER_TYPE_WARP) {
    fprintf_s(gpFile, " Warp Type\n");
  } else if (d3DriverType == D3D_DRIVER_TYPE_SOFTWARE) {
    fprintf_s(gpFile, " Software Type\n");
  } else {
    fprintf_s(gpFile, " Unknown Type\n");
  }

  fprintf_s(gpFile, "The Supported Highest Feature Level Is: \n");
  if (d3DriverType == D3D_FEATURE_LEVEL_11_0) {
    fprintf_s(gpFile, " 11.0\n");
  } else if (d3DriverType == D3D_FEATURE_LEVEL_10_1) {
    fprintf_s(gpFile, " 10.1\n");
  } else if (d3DriverType == D3D_FEATURE_LEVEL_10_0) {
    fprintf_s(gpFile, " 10.0\n");
  } else {
    fprintf_s(gpFile, "Unknown\n");
  }
  fclose(gpFile);

  // Initialize Shader, Input Layouts, Constant Buffers etc.
  const char *vertexShaderSourceCode =
      "cbuffer ConstantBuffer"
      "{"
        "float4x4 worldViewMatrix;"
        "float4x4 projectionMatrix;"
        "float4 ld;"
        "float4 kd;"
        "float4 lightPosition;"
        "uint keyPressed;"
      "}"

      "struct VertexOutput"
      "{"
        "float4 position : SV_POSITION;"
        "float4 diffuse_light : COLOR;"      // Using As "InterShaderNamedVariable"
        
      "};"

      // POSITION and COLOR are from inputElementDesc(InputLayout),
      // POSITION Is Simillar To AMC_ATTRIBUTES_POSITION and color Is
      // Similar To AMC_ATTRIBUTES_COLOR

      "VertexOutput main(float4 pos : POSITION, float4 normal : NORMAL)"
      "{"
        "VertexOutput output;"
        
        "if(1 == keyPressed)"
        "{"
            "float4 eye_coordinates = mul(worldViewMatrix, pos);"
            "float3 t_normal = normalize(mul((float3x3)worldViewMatrix, (float3)normal));"
            "float3 lightSource = (float3)normalize(lightPosition - eye_coordinates);"
            "output.diffuse_light = ld * kd *max(dot(lightSource, t_normal), 0.0);"
        "}"
        "else"
        "{"
          "output.diffuse_light = float4(1.0, 1.0, 1.0, 1.0);"
        "}"
        "output.position = mul(projectionMatrix, mul(worldViewMatrix, pos));"

        "return (output);"
      "}";

  // Vertex Shader Compilation
  ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
  ID3DBlob *pID3DBlob_Error = NULL;

  hr =
      D3DCompile(vertexShaderSourceCode, lstrlenA(vertexShaderSourceCode) + 1,
                 "VS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main",
                 "vs_5_0", 0, 0, &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreateVertexShader => Similar To glCreateShader(GL_VERTEX_SHADER)
  hr = gpID3D11Device->CreateVertexShader(
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

  const char *pixelShaderSourceCode =
      "float4 main(float4 pos : SV_POSITION, float4 diffuse_light_color : COLOR) : SV_TARGET"
      "{"
         "float4 color = diffuse_light_color;"
         "return (color);"
      "}";

  // Pixel Shader Compilation
  ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
  pID3DBlob_Error = NULL;
  hr = D3DCompile(pixelShaderSourceCode, lstrlenA(pixelShaderSourceCode) + 1,
                  "PS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main",
                  "ps_5_0", 0, 0, &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreatePixelShader => Similar To glCreateShader(GL_FRAGMENT_SHADER)
  hr = gpID3D11Device->CreatePixelShader(
      pID3DBlob_PixelShaderCode->GetBufferPointer(),
      pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);

  // Input Layout Related Code
  D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
  inputElementDesc[0].SemanticName = "POSITION";
  inputElementDesc[0].SemanticIndex = 0;
  inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[0].InputSlot = 0;
  inputElementDesc[0].AlignedByteOffset = 0;
  inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[0].InstanceDataStepRate = 0;

  inputElementDesc[1].SemanticName = "NORMAL"; // NORMAL: used in shader as binding
  inputElementDesc[1].SemanticIndex = 0; // 1st(0th index) in Texture Buffer
  inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[1].InputSlot = 1;
  inputElementDesc[1].AlignedByteOffset = 0;
  inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[1].InstanceDataStepRate = 0;

  hr = gpID3D11Device->CreateInputLayout(
      inputElementDesc, _ARRAYSIZE(inputElementDesc),
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

  // TODO: Check Position Once Again
  pID3DBlob_VertexShaderCode->Release();
  pID3DBlob_VertexShaderCode = NULL;
  pID3DBlob_PixelShaderCode->Release();
  pID3DBlob_PixelShaderCode = NULL;

  // Cube-Start
  // VertexPositionBlock-Start

  // Create Vertex Buffer - Clockwise Order
  float vertices_cube[] = {
    //TOP
    // triangle-1
    -1.0f, 1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, -1.0f,
    // triangle-2
    -1.0f, 1.0f, -1.0f,
    1.0f, 1.0f, 1.0f,
    1.0f, 1.0f, -1.0f,

   //Bottom
   // triangle-1
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, 1.0f,
    -1.0f, -1.0f, -1.0f,
    // triangle-2
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, 1.0f,
    -1.0f, -1.0f, 1.0f,

   //FRONT
   // triangle-1
    -1.0f, 1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
    // triangle-2
    -1.0f, -1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,

   //BACK
   // triangle-1
    1.0f, -1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, -1.0f, 1.0f,
    // triangle-2
    -1.0f, -1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,

    //Left
    // triangle-1
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, -1.0f,
    -1.0f, -1.0f, 1.0f,
    // triangle-2
    -1.0f, -1.0f, 1.0f,
    -1.0f, 1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
   
   //RIGHT
   // triangle-1
    1.0f, -1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,
    1.0f, -1.0f, 1.0f,
    // triangle-2
    1.0f, -1.0f, 1.0f,
    1.0f, 1.0f, -1.0f,
    1.0f, 1.0f, 1.0f,
  };

  D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Cube_Position;
  ZeroMemory(&bufferDesc_VertexBuffer_Cube_Position,
             sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Cube_Position.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Cube_Position.ByteWidth =
      sizeof(float) * _ARRAYSIZE(vertices_cube);
  bufferDesc_VertexBuffer_Cube_Position.BindFlags =
      D3D11_BIND_VERTEX_BUFFER;
  bufferDesc_VertexBuffer_Cube_Position.CPUAccessFlags =
      D3D11_CPU_ACCESS_WRITE;

  hr = gpID3D11Device->CreateBuffer(
      &bufferDesc_VertexBuffer_Cube_Position, NULL,
      &gpID3D11Buffer_VertexBuffer_Cube_Position);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Vertex Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Vertex Succeeded\n");
    fclose(gpFile);
  }

  // Copy Vertices Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexCubePosition;
  ZeroMemory(&mappedSubresource_VertexCubePosition,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube_Position, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_VertexCubePosition);
  memcpy(mappedSubresource_VertexCubePosition.pData, vertices_cube,
         sizeof(vertices_cube));
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube_Position,
                               NULL);

  // VertexPositionBlock-End

  // VertexColorBlock-Start

  // Create Vertex Buffer - Clockwise Order
  float normals_cube[] = {
    //TOP
    //triangle-1
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    //triangle-2
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,

    //BOTTOM
    //triangle-1
    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,
    //triangle-2
    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,

    //FRONT
    //triangle-1
    0.0f, 0.0f, -1.0f,
    0.0f, 0.0f, -1.0f,
    0.0f, 0.0f, -1.0f,
    //triangle-2
    0.0f, 0.0f, -1.0f,
    0.0f, 0.0f, -1.0f,
    0.0f, 0.0f, -1.0f,

    //BACK
    //triangle-1
    0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f,
    //triangle-2
    0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f,

    //LEFT
    //triangle-1
    -1.0f, 0.0f, 0.0f,
    -1.0f, 0.0f, 0.0f,
    -1.0f, 0.0f, 0.0f,
    //triangle-2
    -1.0f, 0.0f, 0.0f,
    -1.0f, 0.0f, 0.0f,
    -1.0f, 0.0f, 0.0f,

    //RIGHT
    //triangle-1
    1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    //triangle-2
    1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f
  };

  D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Cube_Normal;
  ZeroMemory(&bufferDesc_VertexBuffer_Cube_Normal,
             sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Cube_Normal.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Cube_Normal.ByteWidth =
      sizeof(float) * _ARRAYSIZE(normals_cube);
  bufferDesc_VertexBuffer_Cube_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bufferDesc_VertexBuffer_Cube_Normal.CPUAccessFlags =
      D3D11_CPU_ACCESS_WRITE;

  hr = gpID3D11Device->CreateBuffer(&bufferDesc_VertexBuffer_Cube_Normal,
                                    NULL,
                                    &gpID3D11Buffer_VertexBuffer_Cube_Normal);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Normal Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Normal Succeeded\n");
    fclose(gpFile);
  }

  // Copy Vertices Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexCubeNormal;
  ZeroMemory(&mappedSubresource_VertexCubeNormal,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Cube_Normal, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_VertexCubeNormal);
  memcpy(mappedSubresource_VertexCubeNormal.pData, normals_cube,
         sizeof(normals_cube));
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Cube_Normal,
                               NULL);

  // VertexColorBlock-End
  // Cube-End

  // Define And Set The Constant Buffer
  D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
  ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
  bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
  bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
  bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

  hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr,
                                    &gpID3D11Buffer_ConstantBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->VSSetConstantBuffers(0, 1,
                                              &gpID3D11Buffer_ConstantBuffer);

  // Rasterizer State
  // In D3D, backface culling is by default ON
  D3D11_RASTERIZER_DESC rasterizerDesc;
  ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
  rasterizerDesc.AntialiasedLineEnable = FALSE;
  rasterizerDesc.CullMode = D3D11_CULL_NONE;
  rasterizerDesc.DepthBias = 0;
  rasterizerDesc.DepthBiasClamp = 0.0f;
  rasterizerDesc.DepthClipEnable = TRUE;
  rasterizerDesc.FillMode = D3D11_FILL_SOLID;
  rasterizerDesc.FrontCounterClockwise = FALSE;
  rasterizerDesc.MultisampleEnable = FALSE;
  rasterizerDesc.ScissorEnable = FALSE;
  rasterizerDesc.SlopeScaledDepthBias = 0.0f;

  hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc,
                                             &gpID3D11RasterizerState);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateRasterizerState Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateRasterizerState Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

  // d3d clear color(blue)
  gClearColor[0] = 0.0f;
  gClearColor[1] = 0.0f;
  gClearColor[2] = 0.0f;
  gClearColor[3] = 0.0f;

  // Set Projection Matrix To Identity Matrix
  gPerspecctiveProjectionMatrix = XMMatrixIdentity();

  // call resize for first time
  hr = ReSize(WIN_WIDTH, WIN_HEIGHT);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ReSize failed. Exitting Now...\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ReSize succeeded\n");
    fclose(gpFile);
  }

  return S_OK;
}

HRESULT ReSize(int width, int height) {
  // code
  HRESULT hr = S_OK;

  //depth related code
  if (gpID3D11DepthStencilView)
  {
    gpID3D11DepthStencilView->Release();
    gpID3D11DepthStencilView = NULL;
  }

  // free any size-dependent resources
  if (gpID3D11RenderTargetView) {
    gpID3D11RenderTargetView->Release();
    gpID3D11RenderTargetView = NULL;
  }

  // resize swap chain buffers accordingly
  gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM,
                                  0);

  // again get back buffer form swap chain
  ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
  gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
                              (LPVOID *)&pID3D11Texture2D_BackBuffer);

  // again get render target view from d3d11 device using above back buffer
  hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL,
                                              &gpID3D11RenderTargetView);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView succeeded\n");
    fclose(gpFile);
  }
  pID3D11Texture2D_BackBuffer->Release();
  pID3D11Texture2D_BackBuffer = NULL;

  // set render target view as render target
  gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

  // Depth Related Code

  // create depth stencil buffer(or xbuffer)
  D3D11_TEXTURE2D_DESC textureDesc;
  ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

  textureDesc.Width = (UINT)width;
  textureDesc.Height = (UINT)height;
  textureDesc.ArraySize = 1;
  textureDesc.MipLevels = 1;
  textureDesc.SampleDesc.Count = 1;
  textureDesc.SampleDesc.Quality = 0;
  textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
  textureDesc.Usage = D3D11_USAGE_DEFAULT;
  textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
  textureDesc.CPUAccessFlags = 0;
  textureDesc.MiscFlags = 0;
  
  ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
  hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL,
                                  &pID3D11Texture2D_DepthBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateTexture2D failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateTexture2D succeeded\n");
    fclose(gpFile);
  }

  // create depth stencil view from above depth stencil buffer
  D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
  ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

  depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
  depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
  hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer,
                                              &depthStencilViewDesc, &gpID3D11DepthStencilView);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView succeeded\n");
    fclose(gpFile);
  }
  pID3D11Texture2D_DepthBuffer->Release();
  pID3D11Texture2D_DepthBuffer = NULL;

  // Set Render Target View As Render Target
  gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView,
                                            gpID3D11DepthStencilView);

  //

  // set viewport
  D3D11_VIEWPORT d3dViewPort;
  d3dViewPort.TopLeftX = 0;
  d3dViewPort.TopLeftY = 0;
  d3dViewPort.Width = (float)width;
  d3dViewPort.Height = (float)height;
  d3dViewPort.MinDepth = 0.0f;
  d3dViewPort.MaxDepth = 1.0f;
  gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

  // Set Perspecctive Projection Matrix
  gPerspecctiveProjectionMatrix = XMMatrixPerspectiveFovLH(
      XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

  return hr;
}

void Display(void) {
  void Update(void);

  // code
  // Clear Render Target View To A Chosen Color
  gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,
                                               gClearColor);

  // Clear The  Depth/Stencil View
  gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,
                                               D3D11_CLEAR_DEPTH, 1.0f, 0);

  // Cube-Start
  // Select Which Vertex Buffer To Display
  // Vertex Position Buffer
  UINT stride = sizeof(float) * 3;
  UINT offset = 0;
  gpID3D11DeviceContext->IASetVertexBuffers(
      0, 1, &gpID3D11Buffer_VertexBuffer_Cube_Position, &stride,
      &offset);  // => Like GL_DYNAMIC_DRAW

  // Vertex Normal Buffer
  stride = sizeof(float) * 3;
  offset = 0;
  gpID3D11DeviceContext->IASetVertexBuffers(
      1, 1, &gpID3D11Buffer_VertexBuffer_Cube_Normal, &stride,
      &offset);  // => Like GL_DYNAMIC_DRAW

  // Select Geometry Primitive
  gpID3D11DeviceContext->IASetPrimitiveTopology(
      D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // => glDrawArray's 1st Parameter

  // Translation Is Concerned With World Matrix Transformation
  XMMATRIX rotationMatrix = XMMatrixIdentity();
  XMMATRIX translationMatrix = XMMatrixIdentity();
  XMMATRIX worldMatrix = XMMatrixIdentity();
  XMMATRIX viewMatrix = XMMatrixIdentity();

  translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 5.0f);
  rotationMatrix = XMMatrixRotationX(-gCubeRotationAngle);
  rotationMatrix = rotationMatrix * XMMatrixRotationY(-gCubeRotationAngle);
  rotationMatrix = rotationMatrix * XMMatrixRotationZ(-gCubeRotationAngle);

  worldMatrix = rotationMatrix * translationMatrix;

  // Final WorldViewProjection Matrix
  XMMATRIX wvMatrix = worldMatrix * viewMatrix;

  // Load The Data Into The Constant Buffer
  CBUFFER constantBuffer;
  memset(&constantBuffer, 0, sizeof(CBUFFER));

  constantBuffer.WorldViewMatrix = wvMatrix;
  constantBuffer.ProjectionMatrix = gPerspecctiveProjectionMatrix;
  if (gbIsLightOn)
  {
    constantBuffer.KeyPressed = 1;
    constantBuffer.Ld = XMVectorSet(1.0, 1.0, 1.0, 1.0);
    constantBuffer.Kd = XMVectorSet(0.50, 0.50, 0.50, 1.0);
    constantBuffer.LightPosition = XMVectorSet(0.0, 0.0, -2.0, 1.0); // Directional Light
  }
  else
  {
    constantBuffer.KeyPressed = 0;
  }
  gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0,
                                           NULL, &constantBuffer, 0, 0);

  // Draw Vertex Buffer To Render Target
  gpID3D11DeviceContext->Draw(6, 0);
  gpID3D11DeviceContext->Draw(6, 6);
  gpID3D11DeviceContext->Draw(6, 12);
  gpID3D11DeviceContext->Draw(6, 18);
  gpID3D11DeviceContext->Draw(6, 24);
  gpID3D11DeviceContext->Draw(6, 30);

  // Triangle-End

  // Switch between Front And Back Buffers
  gpIDXGISwapChain->Present(0, 0);

  return;
}

void UnInitialize(void) {
  // code

  // depth related code
  if (gpID3D11DepthStencilView) {
    gpID3D11DepthStencilView->Release();
    gpID3D11DepthStencilView = NULL;
  }

  if (gpID3D11RasterizerState) {
    gpID3D11RasterizerState->Release();
    gpID3D11RasterizerState = NULL;
  }

  if (gpID3D11Buffer_ConstantBuffer) {
    gpID3D11Buffer_ConstantBuffer->Release();
    gpID3D11Buffer_ConstantBuffer = NULL;
  }

  if (gpID3D11Buffer_VertexBuffer_Cube_Normal) {
    gpID3D11Buffer_VertexBuffer_Cube_Normal->Release();
    gpID3D11Buffer_VertexBuffer_Cube_Normal = NULL;
  }

  if (gpID3D11Buffer_VertexBuffer_Cube_Position) {
    gpID3D11Buffer_VertexBuffer_Cube_Position->Release();
    gpID3D11Buffer_VertexBuffer_Cube_Position = NULL;
  }

  if (gpID3D11InputLayout) {
    gpID3D11InputLayout->Release();
    gpID3D11InputLayout = NULL;
  }

  if (gpID3D11PixelShader) {
    gpID3D11PixelShader->Release();
    gpID3D11PixelShader = NULL;
  }

  if (gpID3D11VertexShader) {
    gpID3D11VertexShader->Release();
    gpID3D11VertexShader = NULL;
  }

  if (gpID3D11RenderTargetView) {
    gpID3D11RenderTargetView->Release();
    gpID3D11RenderTargetView = NULL;
  }

  if (gpIDXGISwapChain) {
    gpIDXGISwapChain->Release();
    gpIDXGISwapChain = NULL;
  }

  if (gpID3D11DeviceContext) {
    gpID3D11DeviceContext->Release();
    gpID3D11DeviceContext = NULL;
  }

  if (gpID3D11Device) {
    gpID3D11Device->Release();
    gpID3D11Device = NULL;
  }

  if (gpFile) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Uninitialize succeeded.\n");
    fprintf_s(gpFile, "Log File Is Successfully Closed.\n");
    fclose(gpFile);
  }

  return;
}

void Update(void) {
  if (gCubeRotationAngle > 360.0f) {
    gCubeRotationAngle = 0.0f;
  }
  gCubeRotationAngle += 0.0001f;
}
