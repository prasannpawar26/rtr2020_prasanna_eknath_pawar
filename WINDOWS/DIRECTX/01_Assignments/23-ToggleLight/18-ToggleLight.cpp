#include <stdio.h>
#include <windows.h>
// directx specific header file
#include <d3d11.h>
#include <d3dcompiler.h>  //For Shader Compilation

#pragma warning(disable : 4838)
#include "XNAMath/xnamath.h"  // This File Is Included in All Other .inl File.

#include "Sphere.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "Sphere.lib")

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable
FILE *gpFile = NULL;
char gszLogFIleName[] = "Log.txt";

HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbIsFullScreen = false;

float gClearColor[4];  // RGBA

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;



ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
ID3D11Buffer *gpID3D11Buffer_InputBuffer_Sphere = NULL;


// Depth Related Code
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

// For Culling OFF
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

// Uniforms
struct CBUFFER {
  XMMATRIX WorldMatrix;
  XMMATRIX ViewMatrix;
  XMMATRIX ProjectionMatrix;

  XMVECTOR La;
  XMVECTOR Ld;
  XMVECTOR Ls;
  XMVECTOR Light_Position;

  XMVECTOR Ka;
  XMVECTOR Kd;
  XMVECTOR Ks;
  float Material_Shininess;

  unsigned int Light_Enabled;
};

bool gbIsLightOn = false;
bool gbToggleShader = false;

XMMATRIX gPerspecctiveProjectionMatrix;

float gLightAmbinent[] = {0.0f, 0.0f, 0.0f, 1.0f};
float gLightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
float gLightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
float gLightPosition[] = {100.0f, 100.0f, -100.0f, 1.0f};

float gMaterialAmbinent[] = {0.0f, 0.0f, 0.0f, 1.0f};
float gMaterialDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
float gMaterialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
float gmaterialShininess = 50.0f;


// per Vertex - Shader Related Variables
ID3D11VertexShader *gpPerVertex_ID3D11VertexShader = NULL;
ID3D11PixelShader *gpPerVertex_ID3D11PixelShader = NULL;
ID3D11Buffer *gpPerVertex_ID3D11Buffer_ConstantBuffer = NULL;
ID3D11InputLayout *gpPerVertex_ID3D11InputLayout = NULL;

const char *gPerVertex_VertexShaderSourceCode =
    "cbuffer ConstantBuffer"
    "{"
    "float4x4 worldMatrix;"
    "float4x4 viewMatrix;"
    "float4x4 projectionMatrix;"

    "float4 la;"
    "float4 ld;"
    "float4 ls;"
    "float4 light_position;"

    "float4 ka;"
    "float4 kd;"
    "float4 ks;"
    "float material_shininess;"

    "uint light_enabled;"
    "}"

    "struct VertexOutput"
    "{"
    "float4 position : SV_POSITION;"
    "float4 phong_ads_color : COLOR;"  // Using As "InterShaderNamedVariable"

    "};"

    // POSITION and COLOR are from inputElementDesc(InputLayout),
    // POSITION Is Simillar To AMC_ATTRIBUTES_POSITION and color Is
    // Similar To AMC_ATTRIBUTES_COLOR

    "VertexOutput main(float4 pos : POSITION, float4 normal : NORMAL)"
    "{"
    "VertexOutput output;"

    "if(1 == light_enabled)"
    "{"
    "float4 eye_coordinates = mul(worldMatrix, pos);"
    "eye_coordinates = mul(viewMatrix, eye_coordinates);"

    "float3 transformedNormal = "
    "(float3)normalize((mul(mul((float3x3)viewMatrix, "
    "(float3x3)worldMatrix), (float3)normal)));"

    "float3 light_direction = (float3)normalize(light_position - "
    "eye_coordinates);"
    "float tn_dot_ld = max(dot(transformedNormal, light_direction), 0.0);"
    "float4 ambinent = la * ka;"
    "float4 diffuse = ld * kd * tn_dot_ld;"
    "float3 reflectionVector = reflect(-light_direction, transformedNormal);"
    "float3 viewerVector = normalize(-eye_coordinates.xyz);"
    "float4 specular = ls * ks * pow(max(dot(reflectionVector, "
    "viewerVector), 0.0), material_shininess);"
    "output.phong_ads_color = ambinent + diffuse + specular;"
    "}"
    "else"
    "{"
    "output.phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);"
    "}"

    "output.position = mul(projectionMatrix, mul(viewMatrix, "
    "mul(worldMatrix, pos)));"

    "return (output);"
    "}";

 const char *gPerVertex_PixelShaderSourceCode =
    "float4 main(float4 pos : SV_POSITION, float4 phong_ads_color : COLOR) : "
    "SV_TARGET"
    "{"
        "float4 color = phong_ads_color;"
        "return (color);"
    "}";


 // per Pixel - Shader Related Variables
 ID3D11VertexShader *gpPerPixel_ID3D11VertexShader = NULL;
 ID3D11PixelShader *gpPerPixel_ID3D11PixelShader = NULL;
 ID3D11Buffer *gpPerPixel_ID3D11Buffer_ConstantBuffer = NULL;
 ID3D11InputLayout *gpPerPixel_ID3D11InputLayout = NULL;

 const char *gPerPixel_VertexShaderSourceCode =
    "cbuffer ConstantBuffer"
    "{"
    "float4x4 worldMatrix;"
    "float4x4 viewMatrix;"
    "float4x4 projectionMatrix;"

    "float4 la;"
    "float4 ld;"
    "float4 ls;"
    "float4 light_position;"

    "float4 ka;"
    "float4 kd;"
    "float4 ks;"
    "float material_shininess;"

    "uint light_enabled;"
    "}"

    "struct VertexOutput"
    "{"
    "float4 position : SV_POSITION;"
    "float3 transformedNormal : NORMAL0;"  // Using As
                                           // "InterShaderNamedVariable"
                                           // NORMAL0, 1,2 are just name , not
                                           // used
    "float3 lightDirection : NORMAL1;"     // Using As InterShaderNamedVariable"
    "float3 viewerVector : NORMAL2;"       // Using As InterShaderNamedVariable"
    "};"

    // POSITION and COLOR are from inputElementDesc(InputLayout),
    // POSITION Is Simillar To AMC_ATTRIBUTES_POSITION and color Is
    // Similar To AMC_ATTRIBUTES_COLOR

    "VertexOutput main(float4 pos : POSITION, float4 normal : NORMAL)"
    "{"
    "VertexOutput output;"

    "if(1 == light_enabled)"
    "{"
    "float4 eye_coordinates = mul(worldMatrix, pos);"
    "eye_coordinates = mul(viewMatrix, eye_coordinates);"

    "output.transformedNormal =  (float3)(mul(mul((float3x3)viewMatrix, "
    "(float3x3)worldMatrix), (float3)normal));"

    "output.lightDirection = (float3)(light_position - eye_coordinates);"

    "output.viewerVector = -eye_coordinates.xyz;"

    "}"

    "output.position = mul(projectionMatrix, mul(viewMatrix, "
    "mul(worldMatrix, pos)));"

    "return (output);"
    "}";

 const char *gPerPixel_PixelShaderSourceCode =
    "cbuffer ConstantBuffer"
    "{"
    "float4x4 worldMatrix;"
    "float4x4 viewMatrix;"
    "float4x4 projectionMatrix;"

    "float4 la;"
    "float4 ld;"
    "float4 ls;"
    "float4 light_position;"

    "float4 ka;"
    "float4 kd;"
    "float4 ks;"
    "float material_shininess;"

    "uint light_enabled;"
    "}"

    "struct VertexOutput"
    "{"
    "float4 position : SV_POSITION;"
    "float3 transformedNormal : NORMAL0;"
    "float3 lightDirection : NORMAL1;"
    "float3 viewerVector : NORMAL2;"
    "};"

    "float4 main(float4 pos : SV_POSITION, VertexOutput vs_output_as_in) : "
    "SV_TARGET"
    "{"
    "float4 phong_ads_color;"

    "if(1 == light_enabled)"
    "{"
    "float3 light_direction_normalize = "
    "normalize(vs_output_as_in.lightDirection);"
    "float3 tranformation_matrix_normalize = "
    "normalize(vs_output_as_in.transformedNormal);"
    "float3 viewer_vector_normal = normalize(vs_output_as_in.viewerVector);"

    "float t_normal_dot_light_direction = "
    "max(dot(tranformation_matrix_normalize, light_direction_normalize), 0.0f);"

    "float4 ambinent = la * ka;"
    "float4 diffuse = ld * kd * t_normal_dot_light_direction;"
    "float3 reflectionVector = reflect(-light_direction_normalize, "
    "tranformation_matrix_normalize);"

    "float4 specular = ls * ks * pow(max(dot(reflectionVector, "
    "viewer_vector_normal), 0.0), material_shininess);"
    "phong_ads_color = ambinent + diffuse + specular;"
    "}"
    "else"
    "{"
    "phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);"
    "}"

    "return (phong_ads_color);"
    "}";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int iCmdShow) {
  // Function Prototypes
  HRESULT Initialize(void);
  void UnInitialize(void);
  void Display(void);
  void Update(void);

  // variable declarations
  bool bDone = false;
  MSG msg = {0};
  int iRet = 0;
  HWND hwnd;
  WNDCLASSEX wndClass;
  TCHAR szAppName[] = TEXT("DirectX11_18-ToggleLight");

  // code
  if (0 != fopen_s(&gpFile, gszLogFIleName, "w")) {
    MessageBox(NULL, TEXT("Log FIle Creation Failed"), TEXT("Error"), MB_OK);
    exit(0);
  }

  fprintf_s(gpFile, "Log File Created Successfully\n");
  fclose(gpFile);

  wndClass.cbSize = sizeof(WNDCLASSEX);
  wndClass.cbWndExtra = 0;
  wndClass.cbClsExtra = 0;
  wndClass.hInstance = hInstance;
  wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wndClass.hIconSm = LoadCursor(NULL, IDI_APPLICATION);
  wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndClass.lpszMenuName = NULL;
  wndClass.lpszClassName = szAppName;
  wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
  wndClass.lpfnWndProc = WndProc;

  RegisterClassEx(&wndClass);

  hwnd = CreateWindowEx(
      WS_EX_APPWINDOW, szAppName, TEXT("DirectX11_18-ToggleLight"),
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
      100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

  gHwnd = hwnd;

  ShowWindow(hwnd, iCmdShow);
  SetForegroundWindow(hwnd);
  SetFocus(hwnd);

  HRESULT hr;
  hr = Initialize();
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Initialize failed. Exitting Now...\n");
    fclose(gpFile);
    DestroyWindow(hwnd);
    hwnd = NULL;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Initialize succeeded\n");
    fclose(gpFile);
  }

  while (false == bDone) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (WM_QUIT == msg.message) {
        bDone = true;
      } else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    } else {
      if (gbActiveWindow) {
      }

      Update();
      Display();
    }
  }  // End Of While

  UnInitialize();

  return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
  // function declarations
  void ToggledFullScreen(void);
  void UnInitialize(void);
  HRESULT ReSize(int, int);

  // variable delcarations
  HRESULT hr;

  // code
  switch (iMsg) {
    case WM_DESTROY:
      UnInitialize();
      PostQuitMessage(0);
      break;
    case WM_ERASEBKGND:
      return 0;
      break;
    case WM_CLOSE:
      DestroyWindow(hwnd);
      break;
    case WM_SIZE: {
      if (NULL != gpID3D11DeviceContext) {
        hr = ReSize(LOWORD(lParam), HIWORD(lParam));
        if (FAILED(hr)) {
          fopen_s(&gpFile, gszLogFIleName, "a+");
          fprintf_s(gpFile, "ReSize failed. Exitting Now...\n");
          fclose(gpFile);
          hwnd = NULL;
        } else {
          fopen_s(&gpFile, gszLogFIleName, "a+");
          fprintf_s(gpFile, "ReSize succeeded\n");
          fclose(gpFile);
        }
      }
    }

    break;
    case WM_KILLFOCUS:
      gbActiveWindow = false;
      break;
    case WM_SETFOCUS:
      gbActiveWindow = true;
      break;
    case WM_KEYDOWN: {
      switch (wParam) {
        case VK_ESCAPE:
          DestroyWindow(hwnd);
          break;

        case 'F':
        case 'f':
          ToggledFullScreen();
          break;

        case 'L':
        case 'l':
          if (gbIsLightOn)
          {
            gbIsLightOn = false;
          } else {
            gbIsLightOn = true;
          }

          break;

        case 'T':
        case 't':
          if (gbToggleShader) {
            gbToggleShader = false;
          } else {
            gbToggleShader = true;
          }

          break;

      }
    } break;
  }  // Switch End

  return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void) {
  // variable declarations
  MONITORINFO mi;

  // code
  if (false == gbIsFullScreen) {
    dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

    if (WS_OVERLAPPEDWINDOW & dwStyle) {
      mi = {sizeof(MONITORINFO)};

      if (GetWindowPlacement(gHwnd, &gWpPrev) &&
          GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi)) {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                     mi.rcMonitor.right - mi.rcMonitor.left,
                     mi.rcMonitor.bottom - mi.rcMonitor.top,
                     SWP_NOZORDER | SWP_FRAMECHANGED);
      }
    }

    ShowCursor(FALSE);
    gbIsFullScreen = true;
  } else {
    SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(gHwnd, &gWpPrev);
    SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
                 SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
                     SWP_NOMOVE | SWP_NOSIZE);

    gbIsFullScreen = false;
    ShowCursor(TRUE);
  }

  return;
}

HRESULT Initialize_Sphere() {
  HRESULT hr;
  // Shpere-Start
  getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
                      sphere_elements);
  gNumVertices = getNumberOfSphereVertices();
  gNumElements = getNumberOfSphereElements();

  D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Position;
  ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Position,
             sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Sphere_Position.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Sphere_Position.ByteWidth =
      sizeof(float) * _ARRAYSIZE(sphere_vertices);
  bufferDesc_VertexBuffer_Sphere_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bufferDesc_VertexBuffer_Sphere_Position.CPUAccessFlags =
      D3D11_CPU_ACCESS_WRITE;

  hr = gpID3D11Device->CreateBuffer(
      &bufferDesc_VertexBuffer_Sphere_Position, NULL,
      &gpID3D11Buffer_VertexBuffer_Sphere_Position);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Vertex Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Vertex Succeeded\n");
    fclose(gpFile);
  }

  // Copy Vertices Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexSpherePosition;
  ZeroMemory(&mappedSubresource_VertexSpherePosition,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Position, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_VertexSpherePosition);
  memcpy(mappedSubresource_VertexSpherePosition.pData, sphere_vertices,
         sizeof(sphere_vertices));
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Position,
                               NULL);

  // VertexPositionBlock-End

  // VertexNormalBlock-Start

  D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_Normal;
  ZeroMemory(&bufferDesc_VertexBuffer_Sphere_Normal, sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Sphere_Normal.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Sphere_Normal.ByteWidth =
      sizeof(float) * _ARRAYSIZE(sphere_normals);
  bufferDesc_VertexBuffer_Sphere_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bufferDesc_VertexBuffer_Sphere_Normal.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

  hr =
      gpID3D11Device->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_Normal, NULL,
                                   &gpID3D11Buffer_VertexBuffer_Sphere_Normal);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Normal Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Normal Succeeded\n");
    fclose(gpFile);
  }

  // Copy Vertices Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_VertexSphereNormal;
  ZeroMemory(&mappedSubresource_VertexSphereNormal,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Sphere_Normal, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_VertexSphereNormal);
  memcpy(mappedSubresource_VertexSphereNormal.pData, sphere_normals,
         sizeof(sphere_normals));
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Sphere_Normal, NULL);

  D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Sphere_IndexBuffer;
  ZeroMemory(&bufferDesc_VertexBuffer_Sphere_IndexBuffer,
             sizeof(D3D11_BUFFER_DESC));
  bufferDesc_VertexBuffer_Sphere_IndexBuffer.Usage = D3D11_USAGE_DYNAMIC;
  bufferDesc_VertexBuffer_Sphere_IndexBuffer.ByteWidth =
      gNumElements * sizeof(short);
  bufferDesc_VertexBuffer_Sphere_IndexBuffer.BindFlags =
      D3D11_BIND_INDEX_BUFFER;
  bufferDesc_VertexBuffer_Sphere_IndexBuffer.CPUAccessFlags =
      D3D11_CPU_ACCESS_WRITE;
  hr = gpID3D11Device->CreateBuffer(&bufferDesc_VertexBuffer_Sphere_IndexBuffer,
                                    NULL, &gpID3D11Buffer_InputBuffer_Sphere);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-InputBuffer Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-InputBuffer Succeeded\n");
    fclose(gpFile);
  }

  // Copy InputBuffer Into Above Buffer
  D3D11_MAPPED_SUBRESOURCE mappedSubresource_InputBufferSphere;
  ZeroMemory(&mappedSubresource_InputBufferSphere,
             sizeof(D3D11_MAPPED_SUBRESOURCE));
  gpID3D11DeviceContext->Map(gpID3D11Buffer_InputBuffer_Sphere, NULL,
                             D3D11_MAP_WRITE_DISCARD, NULL,
                             &mappedSubresource_InputBufferSphere);
  memcpy(mappedSubresource_InputBufferSphere.pData, sphere_elements,
         sizeof(short) * gNumElements);
  gpID3D11DeviceContext->Unmap(gpID3D11Buffer_InputBuffer_Sphere, NULL);

  // VertexNormalBlock-End
  // Sphere End

  return hr;
}

HRESULT Initialize_PerVertexShaders(void)
{
  HRESULT hr;
  // Shader -> Vertex

  // Vertex Shader Compilation
  ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
  ID3DBlob *pID3DBlob_Error = NULL;

  hr = D3DCompile(gPerVertex_VertexShaderSourceCode,
                  lstrlenA(gPerVertex_VertexShaderSourceCode) + 1,
                 "VS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main",
                 "vs_5_0", 0, 0, &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreateVertexShader => Similar To glCreateShader(GL_VERTEX_SHADER)
  hr = gpID3D11Device->CreateVertexShader(
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(), NULL,
      &gpPerVertex_ID3D11VertexShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->VSSetShader(gpPerVertex_ID3D11VertexShader, 0, 0);

  // Shader-Pixel Shader

  // Pixel Shader Compilation
  ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
  pID3DBlob_Error = NULL;
  hr = D3DCompile(gPerVertex_PixelShaderSourceCode,
                  lstrlenA(gPerVertex_PixelShaderSourceCode) + 1,
                  "PS", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main",
                  "ps_5_0", 0, 0, &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreatePixelShader => Similar To glCreateShader(GL_FRAGMENT_SHADER)
  hr = gpID3D11Device->CreatePixelShader(
      pID3DBlob_PixelShaderCode->GetBufferPointer(),
      pID3DBlob_PixelShaderCode->GetBufferSize(), NULL,
      &gpPerVertex_ID3D11PixelShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->PSSetShader(gpPerVertex_ID3D11PixelShader, 0, 0);

  // Input Layout Related Code
  D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
  inputElementDesc[0].SemanticName = "POSITION";
  inputElementDesc[0].SemanticIndex = 0;
  inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[0].InputSlot = 0;
  inputElementDesc[0].AlignedByteOffset = 0;
  inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[0].InstanceDataStepRate = 0;

  inputElementDesc[1].SemanticName =
      "NORMAL";                           // NORMAL: used in shader as binding
  inputElementDesc[1].SemanticIndex = 0;  // 1st(0th index) in Texture Buffer
  inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[1].InputSlot = 1;
  inputElementDesc[1].AlignedByteOffset = 0;
  inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[1].InstanceDataStepRate = 0;

  hr = gpID3D11Device->CreateInputLayout(
      inputElementDesc, _ARRAYSIZE(inputElementDesc),
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(),
      &gpPerVertex_ID3D11InputLayout);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->IASetInputLayout(gpPerVertex_ID3D11InputLayout);

  // TODO: Check Position Once Again
  pID3DBlob_VertexShaderCode->Release();
  pID3DBlob_VertexShaderCode = NULL;
  pID3DBlob_PixelShaderCode->Release();
  pID3DBlob_PixelShaderCode = NULL;

  // Define And Set The Constant Buffer
  D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
  ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
  bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
  bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
  bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

  hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr,
                                    &gpPerVertex_ID3D11Buffer_ConstantBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->VSSetConstantBuffers(0, 1,
      &gpPerVertex_ID3D11Buffer_ConstantBuffer);

  return hr;
}

HRESULT Initialize_PerPixelShaders(void) {
  HRESULT hr;
  // Shader -> Vertex

  // Vertex Shader Compilation
  ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
  ID3DBlob *pID3DBlob_Error = NULL;

  hr = D3DCompile(gPerPixel_VertexShaderSourceCode,
                  lstrlenA(gPerPixel_VertexShaderSourceCode) + 1, "VS", NULL,
                  D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "vs_5_0", 0, 0,
                  &pID3DBlob_VertexShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Vertex Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreateVertexShader => Similar To glCreateShader(GL_VERTEX_SHADER)
  hr = gpID3D11Device->CreateVertexShader(
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(), NULL,
      &gpPerPixel_ID3D11VertexShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateVertexShader: Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->VSSetShader(gpPerPixel_ID3D11VertexShader, 0, 0);

  // Shader-Pixel Shader

  // Pixel Shader Compilation
  ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
  pID3DBlob_Error = NULL;
  hr = D3DCompile(gPerPixel_PixelShaderSourceCode,
                  lstrlenA(gPerPixel_PixelShaderSourceCode) + 1, "PS", NULL,
                  D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", "ps_5_0", 0, 0,
                  &pID3DBlob_PixelShaderCode, &pID3DBlob_Error);
  if (FAILED(hr)) {
    if (pID3DBlob_Error != NULL) {
      fopen_s(&gpFile, gszLogFIleName, "a+");
      fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Failed : %s\n",
                (char *)pID3DBlob_Error->GetBufferPointer());
      fclose(gpFile);
      pID3DBlob_Error->Release();
      pID3DBlob_Error = NULL;
      return (hr);
    }
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3DCompile(): Pixel Shader Compiled Succeeded\n");
    fclose(gpFile);
  }

  // CreatePixelShader => Similar To glCreateShader(GL_FRAGMENT_SHADER)
  hr = gpID3D11Device->CreatePixelShader(
      pID3DBlob_PixelShaderCode->GetBufferPointer(),
      pID3DBlob_PixelShaderCode->GetBufferSize(), NULL,
      &gpPerPixel_ID3D11PixelShader);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreatePixelShader Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->PSSetShader(gpPerPixel_ID3D11PixelShader, 0, 0);

  // Input Layout Related Code
  D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
  inputElementDesc[0].SemanticName = "POSITION";
  inputElementDesc[0].SemanticIndex = 0;
  inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[0].InputSlot = 0;
  inputElementDesc[0].AlignedByteOffset = 0;
  inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[0].InstanceDataStepRate = 0;

  inputElementDesc[1].SemanticName =
      "NORMAL";                           // NORMAL: used in shader as binding
  inputElementDesc[1].SemanticIndex = 0;  // 1st(0th index) in Texture Buffer
  inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
  inputElementDesc[1].InputSlot = 1;
  inputElementDesc[1].AlignedByteOffset = 0;
  inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
  inputElementDesc[1].InstanceDataStepRate = 0;

  hr = gpID3D11Device->CreateInputLayout(
      inputElementDesc, _ARRAYSIZE(inputElementDesc),
      pID3DBlob_VertexShaderCode->GetBufferPointer(),
      pID3DBlob_VertexShaderCode->GetBufferSize(),
      &gpPerPixel_ID3D11InputLayout);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateInputLayout Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->IASetInputLayout(gpPerPixel_ID3D11InputLayout);

  // TODO: Check Position Once Again
  pID3DBlob_VertexShaderCode->Release();
  pID3DBlob_VertexShaderCode = NULL;
  pID3DBlob_PixelShaderCode->Release();
  pID3DBlob_PixelShaderCode = NULL;

  // Define And Set The Constant Buffer
  D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
  ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
  bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
  bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
  bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

  hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr,
                                    &gpPerPixel_ID3D11Buffer_ConstantBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateBuffer-Constant Succeeded\n");
    fclose(gpFile);
  }

  gpID3D11DeviceContext->VSSetConstantBuffers(
      0, 1, &gpPerPixel_ID3D11Buffer_ConstantBuffer);
  gpID3D11DeviceContext->PSSetConstantBuffers(
      0, 1, &gpPerPixel_ID3D11Buffer_ConstantBuffer);

  return hr;
}

HRESULT Initialize(void) {

  // function declarations
  void UnInitialize(void);
  HRESULT Initialize_PerVertexShaders(void);
  HRESULT Initialize_PerPixelShaders(void);
  HRESULT ReSize(int, int);

  // Variable Declarations
  HRESULT hr;
  D3D_DRIVER_TYPE d3DriverType;
  D3D_DRIVER_TYPE d3DriverTypes[] = {D3D_DRIVER_TYPE_HARDWARE,
                                     D3D_DRIVER_TYPE_WARP,
                                     D3D_DRIVER_TYPE_REFERENCE};
  D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
  D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;
  UINT createDeviceflags = 0;
  UINT numDriverTypes = 0;
  UINT numFeatureLevels = 1;

  // Code
  numDriverTypes = sizeof(d3DriverTypes) / sizeof(d3DriverTypes[0]);

  DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
  ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
  dxgiSwapChainDesc.BufferCount = 1;
  dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
  dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
  dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
  dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
  dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  dxgiSwapChainDesc.OutputWindow = gHwnd;
  dxgiSwapChainDesc.SampleDesc.Count = 1;
  dxgiSwapChainDesc.SampleDesc.Quality = 0;
  dxgiSwapChainDesc.Windowed = TRUE;

  for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes;
       driverTypeIndex++) {
    d3DriverType = d3DriverTypes[driverTypeIndex];
    hr = D3D11CreateDeviceAndSwapChain(
        NULL, d3DriverType, NULL, createDeviceflags, &d3dFeatureLevel_required,
        numFeatureLevels, D3D11_SDK_VERSION, &dxgiSwapChainDesc,
        &gpIDXGISwapChain, &gpID3D11Device, &d3dFeatureLevel_acquired,
        &gpID3D11DeviceContext);
    if (SUCCEEDED(hr)) {
      break;
    }
  }  // End Of For Loop

  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain failed.\n");
    fclose(gpFile);
    return hr;
  }

  fopen_s(&gpFile, gszLogFIleName, "a+");
  fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain succeeded\n");
  fprintf_s(gpFile, "The Chosen Driver Is Of: \n");
  if (d3DriverType == D3D_DRIVER_TYPE_HARDWARE) {
    fprintf_s(gpFile, " Hardware Type\n");
  } else if (d3DriverType == D3D_DRIVER_TYPE_WARP) {
    fprintf_s(gpFile, " Warp Type\n");
  } else if (d3DriverType == D3D_DRIVER_TYPE_SOFTWARE) {
    fprintf_s(gpFile, " Software Type\n");
  } else {
    fprintf_s(gpFile, " Unknown Type\n");
  }

  fprintf_s(gpFile, "The Supported Highest Feature Level Is: \n");
  if (d3DriverType == D3D_FEATURE_LEVEL_11_0) {
    fprintf_s(gpFile, " 11.0\n");
  } else if (d3DriverType == D3D_FEATURE_LEVEL_10_1) {
    fprintf_s(gpFile, " 10.1\n");
  } else if (d3DriverType == D3D_FEATURE_LEVEL_10_0) {
    fprintf_s(gpFile, " 10.0\n");
  } else {
    fprintf_s(gpFile, "Unknown\n");
  }
  fclose(gpFile);

  Initialize_Sphere();
  // Intialize Shaders
  Initialize_PerVertexShaders();
  Initialize_PerPixelShaders();

  // Rasterizer State
  // In D3D, backface culling is by default ON
  D3D11_RASTERIZER_DESC rasterizerDesc;
  ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
  rasterizerDesc.AntialiasedLineEnable = FALSE;
  rasterizerDesc.CullMode = D3D11_CULL_NONE;
  rasterizerDesc.DepthBias = 0;
  rasterizerDesc.DepthBiasClamp = 0.0f;
  rasterizerDesc.DepthClipEnable = TRUE;
  rasterizerDesc.FillMode = D3D11_FILL_SOLID;
  rasterizerDesc.FrontCounterClockwise = FALSE;
  rasterizerDesc.MultisampleEnable = FALSE;
  rasterizerDesc.ScissorEnable = FALSE;
  rasterizerDesc.SlopeScaledDepthBias = 0.0f;

  hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc,
                                             &gpID3D11RasterizerState);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateRasterizerState Failed\n");
    fclose(gpFile);
    return (hr);
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device:CreateRasterizerState Succeeded\n");
    fclose(gpFile);
  }
  gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

  // d3d clear color(blue)
  gClearColor[0] = 0.0f;
  gClearColor[1] = 0.0f;
  gClearColor[2] = 0.0f;
  gClearColor[3] = 0.0f;

  // Set Projection Matrix To Identity Matrix
  gPerspecctiveProjectionMatrix = XMMatrixIdentity();

  // call resize for first time
  hr = ReSize(WIN_WIDTH, WIN_HEIGHT);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ReSize failed. Exitting Now...\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ReSize succeeded\n");
    fclose(gpFile);
  }

  return S_OK;
}

HRESULT ReSize(int width, int height) {
  // code
  HRESULT hr = S_OK;

  //depth related code
  if (gpID3D11DepthStencilView)
  {
    gpID3D11DepthStencilView->Release();
    gpID3D11DepthStencilView = NULL;
  }

  // free any size-dependent resources
  if (gpID3D11RenderTargetView) {
    gpID3D11RenderTargetView->Release();
    gpID3D11RenderTargetView = NULL;
  }

  // resize swap chain buffers accordingly
  gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM,
                                  0);

  // again get back buffer form swap chain
  ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
  gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
                              (LPVOID *)&pID3D11Texture2D_BackBuffer);

  // again get render target view from d3d11 device using above back buffer
  hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL,
                                              &gpID3D11RenderTargetView);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView succeeded\n");
    fclose(gpFile);
  }
  pID3D11Texture2D_BackBuffer->Release();
  pID3D11Texture2D_BackBuffer = NULL;

  // set render target view as render target
  gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

  // Depth Related Code

  // create depth stencil buffer(or xbuffer)
  D3D11_TEXTURE2D_DESC textureDesc;
  ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));

  textureDesc.Width = (UINT)width;
  textureDesc.Height = (UINT)height;
  textureDesc.ArraySize = 1;
  textureDesc.MipLevels = 1;
  textureDesc.SampleDesc.Count = 1;
  textureDesc.SampleDesc.Quality = 0;
  textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
  textureDesc.Usage = D3D11_USAGE_DEFAULT;
  textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
  textureDesc.CPUAccessFlags = 0;
  textureDesc.MiscFlags = 0;
  
  ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
  hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL,
                                  &pID3D11Texture2D_DepthBuffer);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateTexture2D failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateTexture2D succeeded\n");
    fclose(gpFile);
  }

  // create depth stencil view from above depth stencil buffer
  D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
  ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

  depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
  depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
  hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer,
                                              &depthStencilViewDesc, &gpID3D11DepthStencilView);
  if (FAILED(hr)) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView failed.\n");
    fclose(gpFile);
    return hr;
  } else {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView succeeded\n");
    fclose(gpFile);
  }
  pID3D11Texture2D_DepthBuffer->Release();
  pID3D11Texture2D_DepthBuffer = NULL;

  // Set Render Target View As Render Target
  gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView,
                                            gpID3D11DepthStencilView);

  //

  // set viewport
  D3D11_VIEWPORT d3dViewPort;
  d3dViewPort.TopLeftX = 0;
  d3dViewPort.TopLeftY = 0;
  d3dViewPort.Width = (float)width;
  d3dViewPort.Height = (float)height;
  d3dViewPort.MinDepth = 0.0f;
  d3dViewPort.MaxDepth = 1.0f;
  gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

  // Set Perspecctive Projection Matrix
  gPerspecctiveProjectionMatrix = XMMatrixPerspectiveFovLH(
      XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

  return hr;
}

void Display(void) {
  void Update(void);

  // code
  if (gbToggleShader) {
    // Per-Pixel Lighting
    gpID3D11DeviceContext->VSSetShader(gpPerPixel_ID3D11VertexShader, 0, 0);
    gpID3D11DeviceContext->PSSetShader(gpPerPixel_ID3D11PixelShader, 0, 0);
    gpID3D11DeviceContext->IASetInputLayout(gpPerPixel_ID3D11InputLayout);
    gpID3D11DeviceContext->VSSetConstantBuffers(
        0, 1, &gpPerPixel_ID3D11Buffer_ConstantBuffer);
    gpID3D11DeviceContext->PSSetConstantBuffers(
        0, 1, &gpPerPixel_ID3D11Buffer_ConstantBuffer);
  } else {
    // Per-Vertex Lighting
    gpID3D11DeviceContext->VSSetShader(gpPerVertex_ID3D11VertexShader, 0, 0);
    gpID3D11DeviceContext->PSSetShader(gpPerVertex_ID3D11PixelShader, 0, 0);
    gpID3D11DeviceContext->IASetInputLayout(gpPerVertex_ID3D11InputLayout);
    gpID3D11DeviceContext->VSSetConstantBuffers(
        0, 1, &gpPerVertex_ID3D11Buffer_ConstantBuffer);
  }


  // Clear Render Target View To A Chosen Color
  gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView,
                                               gClearColor);

  // Clear The  Depth/Stencil View
  gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,
                                               D3D11_CLEAR_DEPTH, 1.0f, 0);

  // Sphere-Start
  // Select Which Vertex Buffer To Display
  // Vertex Position Buffer
  UINT stride = sizeof(float) * 3;
  UINT offset = 0;
  gpID3D11DeviceContext->IASetVertexBuffers(
      0, 1, &gpID3D11Buffer_VertexBuffer_Sphere_Position, &stride,
      &offset);  // => Like GL_DYNAMIC_DRAW

  // Vertex Normal Buffer
  stride = sizeof(float) * 3;
  offset = 0;
  gpID3D11DeviceContext->IASetVertexBuffers(
      1, 1, &gpID3D11Buffer_VertexBuffer_Sphere_Normal, &stride,
      &offset);  // => Like GL_DYNAMIC_DRAW

  // Sphere- InputBuffer
  gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_InputBuffer_Sphere, DXGI_FORMAT_R16_UINT, 0);

  // Select Geometry Primitive
  gpID3D11DeviceContext->IASetPrimitiveTopology(
      D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);  // => glDrawArray's 1st Parameter

  // Translation Is Concerned With World Matrix Transformation
  XMMATRIX worldMatrix = XMMatrixIdentity();
  XMMATRIX viewMatrix = XMMatrixIdentity();

  worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 0.0f);
  viewMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f);
  // Final WorldViewProjection Matrix
  XMMATRIX wMatrix = worldMatrix;
  XMMATRIX vMatrix = viewMatrix;

  // Load The Data Into The Constant Buffer
  CBUFFER constantBuffer;
  memset(&constantBuffer, 0, sizeof(CBUFFER));

  constantBuffer.WorldMatrix = wMatrix;
  constantBuffer.ViewMatrix = vMatrix;
  constantBuffer.ProjectionMatrix = gPerspecctiveProjectionMatrix;
  if (gbIsLightOn)
  {
    constantBuffer.Light_Enabled = 1;
    constantBuffer.La = XMVectorSet(gLightAmbinent[0], gLightAmbinent[1],
                                    gLightAmbinent[2], gLightAmbinent[3]);
    constantBuffer.Ld = XMVectorSet(gLightDiffuse[0], gLightDiffuse[1],
                                    gLightDiffuse[2], gLightDiffuse[3]);
    constantBuffer.Ls = XMVectorSet(gLightSpecular[0], gLightSpecular[1],
                                    gLightSpecular[2], gLightSpecular[3]);
    constantBuffer.Light_Position = XMVectorSet(gLightPosition[0], gLightPosition[1],
                                    gLightPosition[2], gLightPosition[3]);

    constantBuffer.Ka = XMVectorSet(gMaterialAmbinent[0], gMaterialAmbinent[1],
                                    gMaterialAmbinent[2], gMaterialAmbinent[3]);
    constantBuffer.Kd = XMVectorSet(gMaterialDiffuse[0], gMaterialDiffuse[1],
                                    gMaterialDiffuse[2], gMaterialDiffuse[3]);
    constantBuffer.Ks = XMVectorSet(gMaterialSpecular[0], gMaterialSpecular[1],
                                    gMaterialSpecular[2], gMaterialSpecular[3]);
    constantBuffer.Material_Shininess = gmaterialShininess;

  }
  else
  {
    constantBuffer.Light_Enabled = 0;
  }

  if (gbToggleShader)
  {
    gpID3D11DeviceContext->UpdateSubresource(
        gpPerPixel_ID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0,
        0);
  } else {
    gpID3D11DeviceContext->UpdateSubresource(
        gpPerVertex_ID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0,
        0);
  }

  // Draw Vertex Buffer To Render Target
  gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

  // Sphere-End

  // Switch between Front And Back Buffers
  gpIDXGISwapChain->Present(0, 0);

  return;
}

void UnInitialize_PerVertex(void)
{
  if (gpPerVertex_ID3D11InputLayout) {
    gpPerVertex_ID3D11InputLayout->Release();
    gpPerVertex_ID3D11InputLayout = NULL;
  }

  if (gpPerVertex_ID3D11PixelShader) {
    gpPerVertex_ID3D11PixelShader->Release();
    gpPerVertex_ID3D11PixelShader = NULL;
  }

  if (gpPerVertex_ID3D11Buffer_ConstantBuffer) {
    gpPerVertex_ID3D11Buffer_ConstantBuffer->Release();
    gpPerVertex_ID3D11Buffer_ConstantBuffer = NULL;
  }

  if (gpPerVertex_ID3D11VertexShader) {
    gpPerVertex_ID3D11VertexShader->Release();
    gpPerVertex_ID3D11VertexShader = NULL;
  }
}

void UnInitialize_PerPixel(void) {
  if (gpPerVertex_ID3D11InputLayout) {
    gpPerVertex_ID3D11InputLayout->Release();
    gpPerVertex_ID3D11InputLayout = NULL;
  }

  if (gpPerVertex_ID3D11PixelShader) {
    gpPerVertex_ID3D11PixelShader->Release();
    gpPerVertex_ID3D11PixelShader = NULL;
  }

  if (gpPerVertex_ID3D11Buffer_ConstantBuffer) {
    gpPerVertex_ID3D11Buffer_ConstantBuffer->Release();
    gpPerVertex_ID3D11Buffer_ConstantBuffer = NULL;
  }

  if (gpPerVertex_ID3D11VertexShader) {
    gpPerVertex_ID3D11VertexShader->Release();
    gpPerVertex_ID3D11VertexShader = NULL;
  }
}

void UnInitialize(void) {

  // function declarations
  void UnInitialize_PerVertex(void);
  void UnInitialize_PerPixel(void);

  // code

  // depth related code
  if (gpID3D11DepthStencilView) {
    gpID3D11DepthStencilView->Release();
    gpID3D11DepthStencilView = NULL;
  }

  if (gpID3D11RasterizerState) {
    gpID3D11RasterizerState->Release();
    gpID3D11RasterizerState = NULL;
  }

  UnInitialize_PerVertex();
  UnInitialize_PerPixel();

  if (gpID3D11Buffer_InputBuffer_Sphere) {
    gpID3D11Buffer_InputBuffer_Sphere->Release();
    gpID3D11Buffer_InputBuffer_Sphere = NULL;
  }

  if (gpID3D11Buffer_VertexBuffer_Sphere_Normal) {
    gpID3D11Buffer_VertexBuffer_Sphere_Normal->Release();
    gpID3D11Buffer_VertexBuffer_Sphere_Normal = NULL;
  }

  if (gpID3D11Buffer_VertexBuffer_Sphere_Position) {
    gpID3D11Buffer_VertexBuffer_Sphere_Position->Release();
    gpID3D11Buffer_VertexBuffer_Sphere_Position = NULL;
  }

  if (gpID3D11RenderTargetView) {
    gpID3D11RenderTargetView->Release();
    gpID3D11RenderTargetView = NULL;
  }

  if (gpIDXGISwapChain) {
    gpIDXGISwapChain->Release();
    gpIDXGISwapChain = NULL;
  }

  if (gpID3D11DeviceContext) {
    gpID3D11DeviceContext->Release();
    gpID3D11DeviceContext = NULL;
  }

  if (gpID3D11Device) {
    gpID3D11Device->Release();
    gpID3D11Device = NULL;
  }

  if (gpFile) {
    fopen_s(&gpFile, gszLogFIleName, "a+");
    fprintf_s(gpFile, "Uninitialize succeeded.\n");
    fprintf_s(gpFile, "Log File Is Successfully Closed.\n");
    fclose(gpFile);
  }

  return;
}

void Update(void) {

  return;
}
