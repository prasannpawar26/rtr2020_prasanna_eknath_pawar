#include <windows.h>
#include <stdio.h>
// DirectX Specific Header file
#include <d3d11.h>
#include <d3dcompiler.h> // For Shader Compilation

#pragma warning (disable : 4838)
#include "XNAMath/xnamath.h" // This File Is Included In All Other inl files

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define PEP_WIN_WIDTH 800
#define PEP_WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *pep_gpFile = NULL;

char pep_gszLogFileName[] = "Log.txt";

HWND pep_gHwnd = NULL;

DWORD pep_gDwStyle;

WINDOWPLACEMENT pep_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool pep_gbActiveWindow = false;
bool pep_gbFullScreen = false;
bool pep_gbEscapeKeyPressed = false;

float pep_gClearColor[4]; //RGBA

ID3D11Device* pep_gpID3D11Device = NULL;
ID3D11DeviceContext* pep_gpID3D11DeviceContext = NULL;
IDXGISwapChain* pep_gpIDXGISwapChain = NULL;
ID3D11RenderTargetView* pep_gpID3D11RenderTargetView = NULL;

ID3D11InputLayout* pep_gpID3D11InputLayout = NULL;
ID3D11VertexShader* pep_gpID3D11VertexShader = NULL;
ID3D11PixelShader* pep_gpID3D11PixelShader = NULL;

ID3D11Buffer* pep_gpID3D11Buffer_VertexPosition = NULL;
ID3D11Buffer* pep_gpID3D11Buffer_VertexColor = NULL;
ID3D11Buffer* pep_gpID3D11Buffer_VertexShader_ConstantBuffer = NULL;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX pep_gPerspectiveProjectionMatrix;

float pep_gTriangleRotation;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//
	// Function Declarations
	//
	HRESULT Initialize(void);
	void Uninitialize(void);
	void Display(void);
	void Update(void);

	//
	// Variable Declarations
	//
	WNDCLASSEX pep_wndClass;
	HWND pep_hwnd;
	MSG pep_msg;
	TCHAR pep_szClassName[] = TEXT("DirectX- ColorTriangle");
	bool pep_bDone = false;

	//
	// Code
	//
	if (fopen_s(&pep_gpFile, pep_gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(pep_gpFile, "Log File Is Successfully Opened.\n");
		fclose(pep_gpFile);
	}

	pep_wndClass.cbSize = sizeof(WNDCLASSEX);
	pep_wndClass.cbClsExtra = 0;
	pep_wndClass.cbWndExtra = 0;
	pep_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	pep_wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	pep_wndClass.lpfnWndProc = WndProc;
	pep_wndClass.hInstance = hInstance;
	pep_wndClass.lpszMenuName = NULL;
	pep_wndClass.lpszClassName = pep_szClassName;
	pep_wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	pep_wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&pep_wndClass);

	pep_hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		pep_szClassName,
		TEXT("DirectX-ColorTriangle"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		PEP_WIN_WIDTH,
		PEP_WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	pep_gHwnd = pep_hwnd;

	ShowWindow(pep_hwnd, iCmdShow);
	SetForegroundWindow(pep_hwnd);
	SetFocus(pep_hwnd);

	//
	// Call Initialize
	//
	HRESULT hr;

	hr = Initialize();
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Failed.\n");
		fclose(pep_gpFile);
		DestroyWindow(pep_hwnd);
		return(hr);
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf_s(pep_gpFile, "Initialize Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Game Loop
	//
	while (false == pep_bDone)
	{
		if (PeekMessage(&pep_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == pep_msg.message)
			{
				pep_bDone = true;
			}
			else
			{
				TranslateMessage(&pep_msg);
				DispatchMessage(&pep_msg);
			}
		}
		else
		{
			if (true == pep_gbActiveWindow)
			{
				if (true == pep_gbEscapeKeyPressed)
				{
					pep_bDone = true;
				}
			}

			Update();
			Display();
		}
	}

	//
	// Call Uninitialize
	//
	Uninitialize();

	return ((int)pep_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);
	void ToggleFullScreen(void);
	void Uninitialize(void);

	//
	// Code
	//

	switch (iMsg)
	{
	case WM_ERASEBKGND:
		return 0;
		break;

	case WM_ACTIVATE:
		if (0 == HIWORD(wParam))
		{
			pep_gbActiveWindow = true;
		}
		else
		{
			pep_gbActiveWindow = false;
		}
		break;

	case WM_SIZE:
		//
		// Call Resize Only When We Have Device Context
		//
		if (NULL != pep_gpID3D11DeviceContext)
		{
			HRESULT hr;
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Failed.\n");
				fclose(pep_gpFile);
				return(hr); // HRESULT -> Get Upcasted Here To LONG.
			}
			else
			{
				fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
				fprintf_s(pep_gpFile, "Resize Succeeded.\n");
				fclose(pep_gpFile);
			}
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (false == pep_gbEscapeKeyPressed)
			{
				pep_gbEscapeKeyPressed = true;
			}
			break;

		case 0x46:
			if (false == pep_gbFullScreen)
			{
				ToggleFullScreen();
				pep_gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				pep_gbFullScreen = false;
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

void ToggleFullScreen(void)
{
	//
	// Variable Declarations
	//
	MONITORINFO mi;

	//
	// Code
	//
	if (false == pep_gbFullScreen)
	{
		pep_gDwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

		if (pep_gDwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(pep_gHwnd, &pep_wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(pep_gHwnd, GWL_STYLE, pep_gDwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(pep_gHwnd, &pep_wpPrev);
		SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	return;
}

HRESULT Initialize(void)
{
	//
	// Function Declarations
	//
	HRESULT Resize(int, int);

	//
	// Variable Initialize
	//
	HRESULT hr;
	D3D_DRIVER_TYPE pep_d3dDriverType;
	D3D_DRIVER_TYPE pep_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL pep_d3dFeatureLevel_accuired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//
	// Code
	//
	numDriverTypes = sizeof(pep_d3dDriverTypes) / sizeof(pep_d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC pep_dxgiSwapChainDesc;
	ZeroMemory(
		&pep_dxgiSwapChainDesc,
		sizeof(DXGI_SWAP_CHAIN_DESC)
	);

	pep_dxgiSwapChainDesc.BufferCount = 1;
	pep_dxgiSwapChainDesc.BufferDesc.Width = PEP_WIN_WIDTH;
	pep_dxgiSwapChainDesc.BufferDesc.Height = PEP_WIN_HEIGHT;
	pep_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	pep_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	pep_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	pep_dxgiSwapChainDesc.OutputWindow = pep_gHwnd;
	pep_dxgiSwapChainDesc.SampleDesc.Count = 1;
	pep_dxgiSwapChainDesc.SampleDesc.Quality = 0;
	pep_dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		pep_d3dDriverType = pep_d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			pep_d3dDriverType,
			NULL,
			createDeviceFlags,
			&pep_d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&pep_dxgiSwapChainDesc,
			&pep_gpIDXGISwapChain,
			&pep_gpID3D11Device,
			&pep_d3dFeatureLevel_accuired,
			&pep_gpID3D11DeviceContext
		);
		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "D3D11CreateDeviceAndSwapChain Succeeded.\n");
		fclose(pep_gpFile);
	}

	//
	// Vertex Shader
	//
	const char* pep_szVertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
			"float4x4 worldViewProjectionMatrix;" \
		"}" \

		"struct VertexDataOutput" \
		"{" \
			"float4 position : SV_POSITION;" \
			"float4 color : COLOR;" \
		"};" \

		"VertexDataOutput main(float4 pos : POSITION, float4 color : COLOR)" \
		"{" \
			"VertexDataOutput output;" \

			"output.position = mul(worldViewProjectionMatrix, pos);" \
			"output.color = color;" \

			"return (output);" \
		"}";

	ID3DBlob* pep_pID3DBlob_VertexSourceCode = NULL;
	ID3DBlob* pep_pID3D11Blob_Error = NULL;

	hr = D3DCompile(
		pep_szVertexShaderSourceCode,
		lstrlenA(pep_szVertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pep_pID3DBlob_VertexSourceCode,
		&pep_pID3D11Blob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3D11Blob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Vertex Shader : %s.\n", (char *)pep_pID3D11Blob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3D11Blob_Error->Release();
			pep_pID3D11Blob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Vertex Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreateVertexShader(
		pep_pID3DBlob_VertexSourceCode->GetBufferPointer(),
		pep_pID3DBlob_VertexSourceCode->GetBufferSize(),
		NULL,
		&pep_gpID3D11VertexShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateVertexShader Succeeded.\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->VSSetShader(
		pep_gpID3D11VertexShader,
		0,
		0
	);

	//
	// Pixel Shader
	//
	const char* pep_szPixelShaderSourceCode =
		"float4 main(float4 pos : SV_POSITION, float4 color : COLOR) : SV_TARGET" \
		"{" \
			"return (color);"
		"}";

	ID3DBlob* pep_pID3DBlob_PixelSourceCode = NULL;
	pep_pID3D11Blob_Error = NULL;

	hr = D3DCompile(
		pep_szPixelShaderSourceCode,
		lstrlenA(pep_szPixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pep_pID3DBlob_PixelSourceCode,
		&pep_pID3D11Blob_Error
	);
	if (FAILED(hr))
	{
		if (NULL != pep_pID3D11Blob_Error)
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			//blob data is in binary format do convert it in char * we are using GetBufferPointer
			fprintf_s(pep_gpFile, "D3DCompile Failed For Pixel Shader : %s.\n", (char *)pep_pID3D11Blob_Error->GetBufferPointer());
			fclose(pep_gpFile);
			pep_pID3D11Blob_Error->Release();
			pep_pID3D11Blob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
			fprintf_s(pep_gpFile, "D3DCompile Successful For Pixel Shader.\n");
			fclose(pep_gpFile);
		}
	}

	hr = pep_gpID3D11Device->CreatePixelShader(
		pep_pID3DBlob_PixelSourceCode->GetBufferPointer(),
		pep_pID3DBlob_PixelSourceCode->GetBufferSize(),
		NULL,
		&pep_gpID3D11PixelShader
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreatePixelShader Succeeded.\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->PSSetShader(
		pep_gpID3D11PixelShader,
		0,
		0
	);

	//
	// Data Passing Code
	//
	D3D11_INPUT_ELEMENT_DESC pep_inputElementDesc[2];
	ZeroMemory(
		&pep_inputElementDesc,
		sizeof(D3D11_INPUT_ELEMENT_DESC) * 2
	);

	pep_inputElementDesc[0].SemanticName = "POSITION";
	pep_inputElementDesc[0].SemanticIndex = 0;
	pep_inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	pep_inputElementDesc[0].InputSlot = 0;
	pep_inputElementDesc[0].AlignedByteOffset = 0;
	pep_inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_inputElementDesc[0].InstanceDataStepRate = 0;

	pep_inputElementDesc[1].SemanticName = "COLOR";
	pep_inputElementDesc[1].SemanticIndex = 0;
	pep_inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	pep_inputElementDesc[1].InputSlot = 1;
	pep_inputElementDesc[1].AlignedByteOffset = 0;
	pep_inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	pep_inputElementDesc[1].InstanceDataStepRate = 0;

	hr = pep_gpID3D11Device->CreateInputLayout(
		pep_inputElementDesc,
		_ARRAYSIZE(pep_inputElementDesc),
		pep_pID3DBlob_VertexSourceCode->GetBufferPointer(),
		pep_pID3DBlob_VertexSourceCode->GetBufferSize(),
		&pep_gpID3D11InputLayout
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateInputLayout Succeeded.\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->IASetInputLayout(
		pep_gpID3D11InputLayout
	);

	pep_pID3DBlob_PixelSourceCode->Release();
	pep_pID3DBlob_PixelSourceCode = NULL;
	pep_pID3DBlob_VertexSourceCode->Release();
	pep_pID3DBlob_VertexSourceCode = NULL;

	float vertices[] =
	{
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f
	};

	D3D11_BUFFER_DESC pep_bufferDesc_VertexPositionBuffer;
	ZeroMemory (
		&pep_bufferDesc_VertexPositionBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_VertexPositionBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_VertexPositionBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices);
	pep_bufferDesc_VertexPositionBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_VertexPositionBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexPositionBuffer,
		NULL,
		&pep_gpID3D11Buffer_VertexPosition
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer For Vertex Position Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer For Vertex Position Succeeded.\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_mappedSubResource;
	pep_gpID3D11DeviceContext->Map(
		pep_gpID3D11Buffer_VertexPosition,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_mappedSubResource
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Map For Vertex Position Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Map For Vertex Position Succeeded.\n");
		fclose(pep_gpFile);
	}

	memcpy(
		pep_mappedSubResource.pData,
		vertices,
		sizeof(vertices)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpID3D11Buffer_VertexPosition,
		NULL
	);

	// Color
	float colors[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};

	D3D11_BUFFER_DESC pep_bufferDesc_VertexColorBuffer;
	ZeroMemory(
		&pep_bufferDesc_VertexColorBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_VertexColorBuffer.Usage = D3D11_USAGE_DYNAMIC;
	pep_bufferDesc_VertexColorBuffer.ByteWidth = sizeof(float) * _ARRAYSIZE(colors);
	pep_bufferDesc_VertexColorBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	pep_bufferDesc_VertexColorBuffer.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexColorBuffer,
		NULL,
		&pep_gpID3D11Buffer_VertexColor
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer For Vertex Color Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer For Vertex Color Succeeded.\n");
		fclose(pep_gpFile);
	}

	D3D11_MAPPED_SUBRESOURCE pep_MappedSubResource_VertexColor;
	ZeroMemory(
		&pep_MappedSubResource_VertexColor,
		sizeof(D3D11_MAPPED_SUBRESOURCE)
	);

	pep_gpID3D11DeviceContext->Map(
		pep_gpID3D11Buffer_VertexColor,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&pep_MappedSubResource_VertexColor
	);

	memcpy(
		pep_MappedSubResource_VertexColor.pData,
		colors,
		sizeof(colors)
	);

	pep_gpID3D11DeviceContext->Unmap(
		pep_gpID3D11Buffer_VertexColor,
		NULL
	);

	// Uniforms
	D3D11_BUFFER_DESC pep_bufferDesc_VertexShader_ConstantBuffer;
	ZeroMemory(
		&pep_bufferDesc_VertexShader_ConstantBuffer,
		sizeof(D3D11_BUFFER_DESC)
	);

	pep_bufferDesc_VertexShader_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	pep_bufferDesc_VertexShader_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	pep_bufferDesc_VertexShader_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = pep_gpID3D11Device->CreateBuffer(
		&pep_bufferDesc_VertexShader_ConstantBuffer,
		nullptr,
		&pep_gpID3D11Buffer_VertexShader_ConstantBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer For VertexShader ConstantBuffer Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateBuffer For VertexShader ConstantBuffer Succeeded.\n");
		fclose(pep_gpFile);
	}

	pep_gpID3D11DeviceContext->VSSetConstantBuffers(
		0,
		1,
		&pep_gpID3D11Buffer_VertexShader_ConstantBuffer
	);


	// Culling related code
	D3D11_RASTERIZER_DESC pep_RasterizerDesc;
	ZeroMemory(
		&pep_RasterizerDesc,
		sizeof(D3D11_RASTERIZER_DESC)
	);

	pep_RasterizerDesc.AntialiasedLineEnable = FALSE;
	pep_RasterizerDesc.CullMode = D3D11_CULL_NONE;
	pep_RasterizerDesc.DepthBias = 0;
	pep_RasterizerDesc.DepthBiasClamp = 0.0f;
	pep_RasterizerDesc.FillMode = D3D11_FILL_SOLID;
	pep_RasterizerDesc.FrontCounterClockwise = FALSE;
	pep_RasterizerDesc.MultisampleEnable = FALSE;
	pep_RasterizerDesc.ScissorEnable = FALSE;
	pep_RasterizerDesc.SlopeScaledDepthBias = 0.0f;

	pep_gClearColor[0] = 0.0f;
	pep_gClearColor[1] = 0.0f;
	pep_gClearColor[2] = 0.0f;
	pep_gClearColor[3] = 0.0f;

	pep_gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(PEP_WIN_WIDTH, PEP_WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "Resize Succeeded.\n");
		fclose(pep_gpFile);
	}

	return S_OK;
}

HRESULT Resize(int width, int height)
{
	//
	// Variable Declarations
	//
	HRESULT hr = S_OK;

	//
	// Code
	//
	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}

	hr = pep_gpIDXGISwapChain->ResizeBuffers(
		1,
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "ResizeBuffers Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "ResizeBuffers Succeeded.\n");
		fclose(pep_gpFile);
	}

	ID3D11Texture2D* pep_pID3D11Texture2D_BackBuffer = NULL;
	hr = pep_gpIDXGISwapChain->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(LPVOID *)&pep_pID3D11Texture2D_BackBuffer
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "GetBuffer Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "GetBuffer Succeeded.\n");
		fclose(pep_gpFile);
	}

	hr = pep_gpID3D11Device->CreateRenderTargetView(
		pep_pID3D11Texture2D_BackBuffer,
		NULL,
		&pep_gpID3D11RenderTargetView
	);
	if (FAILED(hr))
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRenderTargetView Failed.\n");
		fclose(pep_gpFile);
		return hr;
	}
	else
	{
		fopen_s(&pep_gpFile, pep_gszLogFileName, "a+");
		fprintf(pep_gpFile, "CreateRenderTargetView Succeeded.\n");
		fclose(pep_gpFile);
	}

	pep_pID3D11Texture2D_BackBuffer->Release();
	pep_pID3D11Texture2D_BackBuffer = NULL;

	pep_gpID3D11DeviceContext->OMSetRenderTargets(
		1,
		&pep_gpID3D11RenderTargetView,
		NULL
	);

	D3D11_VIEWPORT pep_d3dViewport;
	pep_d3dViewport.TopLeftX = 0;
	pep_d3dViewport.TopLeftY = 0;
	pep_d3dViewport.MinDepth = 0.0f;
	pep_d3dViewport.MaxDepth = 1.0f;
	pep_d3dViewport.Width = width;
	pep_d3dViewport.Height = height;

	pep_gpID3D11DeviceContext->RSSetViewports(
		1,
		&pep_d3dViewport
	);

	pep_gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(
		XMConvertToRadians(45.0f),
		(float)width / (float)height,
		0.1f,
		100.0f
	);

	return hr;
}

void Display(void)
{
	//
	// Code
	//
	pep_gpID3D11DeviceContext->ClearRenderTargetView(
		pep_gpID3D11RenderTargetView,
		pep_gClearColor
	);

	UINT pep_stride = sizeof(float) * 3;
	UINT pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		0,
		1,
		&pep_gpID3D11Buffer_VertexPosition,
		&pep_stride,
		&pep_offset
	);

	pep_stride = sizeof(float) * 3;
	pep_offset = 0;

	pep_gpID3D11DeviceContext->IASetVertexBuffers(
		1,
		1,
		&pep_gpID3D11Buffer_VertexColor,
		&pep_stride,
		&pep_offset
	);

	pep_gpID3D11DeviceContext->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
	);


	XMMATRIX pep_translationMatrix = XMMatrixIdentity();
	XMMATRIX pep_rotationMatrix = XMMatrixIdentity();

	XMMATRIX pep_worldMatrix = XMMatrixIdentity();
	XMMATRIX pep_viewMatrix = XMMatrixIdentity();

	pep_translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 4.0f);
	pep_rotationMatrix = XMMatrixRotationY(-pep_gTriangleRotation);

	pep_worldMatrix = pep_rotationMatrix * pep_translationMatrix;

	XMMATRIX pep_wvpMatrix = pep_worldMatrix * pep_viewMatrix * pep_gPerspectiveProjectionMatrix;

	CBUFFER pep_VertexShader_ConstantBuffer;
	pep_VertexShader_ConstantBuffer.WorldViewProjectionMatrix = pep_wvpMatrix;

	pep_gpID3D11DeviceContext->UpdateSubresource(
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer,
		0,
		NULL,
		&pep_VertexShader_ConstantBuffer,
		0,
		0
	);

	pep_gpID3D11DeviceContext->Draw(
		3,
		0
	);

	pep_gpIDXGISwapChain->Present(
		0,
		0
	);

	return;
}

void Uninitialize(void)
{
	if (NULL != pep_gpID3D11RenderTargetView)
	{
		pep_gpID3D11RenderTargetView->Release();
		pep_gpID3D11RenderTargetView = NULL;
	}
	
	if (NULL != pep_gpID3D11Buffer_VertexShader_ConstantBuffer)
	{
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer->Release();
		pep_gpID3D11Buffer_VertexShader_ConstantBuffer = NULL;
	}

	if (NULL != pep_gpID3D11Buffer_VertexPosition)
	{
		pep_gpID3D11Buffer_VertexPosition->Release();
		pep_gpID3D11Buffer_VertexPosition = NULL;
	}

	if (NULL != pep_gpID3D11InputLayout)
	{
		pep_gpID3D11InputLayout->Release();
		pep_gpID3D11InputLayout = NULL;
	}

	if (NULL != pep_gpID3D11PixelShader)
	{
		pep_gpID3D11PixelShader->Release();
		pep_gpID3D11PixelShader = NULL;
	}

	if (NULL != pep_gpID3D11VertexShader)
	{
		pep_gpID3D11VertexShader->Release();
		pep_gpID3D11VertexShader = NULL;
	}

	if (NULL != pep_gpIDXGISwapChain)
	{
		pep_gpIDXGISwapChain->Release();
		pep_gpIDXGISwapChain = NULL;
	}

	if (NULL != pep_gpID3D11DeviceContext)
	{
		pep_gpID3D11DeviceContext->Release();
		pep_gpID3D11DeviceContext = NULL;
	}

	if (NULL != pep_gpID3D11Device)
	{
		pep_gpID3D11Device->Release();
		pep_gpID3D11Device = NULL;
	}

	return;
}

void Update(void)
{
	if (pep_gTriangleRotation > 360.0f)
	{
		pep_gTriangleRotation = 0.0f;
	}
	pep_gTriangleRotation += 0.0001f;
}
