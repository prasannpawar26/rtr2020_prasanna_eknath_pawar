#include "MosaicNoise.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_MosaicNoise_Vao;
GLuint pep_MosaicNoise_VboPosition;
GLuint pep_MosaicNoise_VertexShaderObject;
GLuint pep_MosaicNoise_FragmentShaderObject;
GLuint pep_MosaicNoise_ShaderProgramObject;

GLuint pep_MosaicNoise_MVPUniform;
GLuint pep_MosaicNoise_ScaleUniform;
GLuint pep_MosaicNoise_Sampler3DNoiseUniform;
GLuint pep_MosaicNoise_SkyColorUniform;
GLuint pep_MosaicNoise_CloudColorUniform;
GLuint pep_MosaicNoise_OffsetUniform;
GLuint pep_MosaicNoise_TilingUniform;
GLuint pep_MosaicNoise_AlphaUniform;
GLuint pep_MosaicNoise_BackUniform;

GLuint pep_MosaicNoise_MathematicalTexture;
GLubyte * pep_MosaicNoise_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_MosaicNoise_CloudMotion = 0.0f;

int MosaicNoise_Initialize(void)
{
	pep_MosaicNoise_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_MosaicNoise_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_MosaicNoise_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_MosaicNoise_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_MosaicNoise_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_MosaicNoise_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_MosaicNoise_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_MosaicNoise_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_MosaicNoise_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec3 mcPosition;" \
		"uniform float u_alpha;" \
		"uniform int u_back;" \

		"out vec4 FragColor;" \

		"float random(in vec2 st) { return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);}" \

		"void main(void)" \
		"{" \

			"if(1 == u_back) { FragColor = vec4(0.0, 0.0, 1.0, 1.0); return;}" \

			//
			// Mosaic
			//
			"vec2 integer_component = floor(mcPosition.st * 15);" \
			"vec3 color = vec3(random(integer_component));" \
			"FragColor = vec4(color, u_alpha);" \

		"}";

	glShaderSource(pep_MosaicNoise_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_MosaicNoise_FragmentShaderObject);
	glGetShaderiv(pep_MosaicNoise_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_MosaicNoise_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_MosaicNoise_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_MosaicNoise_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_MosaicNoise_ShaderProgramObject, pep_MosaicNoise_VertexShaderObject);
	glAttachShader(pep_MosaicNoise_ShaderProgramObject, pep_MosaicNoise_FragmentShaderObject);
	glBindAttribLocation(pep_MosaicNoise_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_MosaicNoise_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_MosaicNoise_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_MosaicNoise_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_MosaicNoise_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_MosaicNoise_MVPUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_mvp_matrix");
	pep_MosaicNoise_SkyColorUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_sky_color");
	pep_MosaicNoise_CloudColorUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_cloud_color");
	pep_MosaicNoise_Sampler3DNoiseUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_noise");
	pep_MosaicNoise_ScaleUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_scale");
	pep_MosaicNoise_OffsetUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_offset");
	pep_MosaicNoise_TilingUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_tiling");
	pep_MosaicNoise_AlphaUniform = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_alpha");
	pep_MosaicNoise_BackUniform   = glGetUniformLocation(pep_MosaicNoise_ShaderProgramObject, "u_back");

	glGenVertexArrays(1, &pep_MosaicNoise_Vao);
	glBindVertexArray(pep_MosaicNoise_Vao);
	glGenBuffers(1, &pep_MosaicNoise_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_MosaicNoise_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void MosaicNoise_Resize(int, int)
{

}

void MosaicNoise_Update(void)
{
	pep_MosaicNoise_CloudMotion += 0.001f;
}

void MosaicNoise_Uninitialize(void)
{
	if (pep_MosaicNoise_VboPosition)
	{
		glDeleteBuffers(1, &pep_MosaicNoise_VboPosition);
		pep_MosaicNoise_VboPosition = 0;
	}

	if (pep_MosaicNoise_Vao)
	{
		glDeleteVertexArrays(1, &pep_MosaicNoise_Vao);
		pep_MosaicNoise_Vao = 0;
	}

	if (pep_MosaicNoise_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_MosaicNoise_ShaderProgramObject);

		glGetProgramiv(pep_MosaicNoise_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_MosaicNoise_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_MosaicNoise_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_MosaicNoise_ShaderProgramObject);
		pep_MosaicNoise_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void MosaicNoise_Display(void)
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;
	glUseProgram(pep_MosaicNoise_ShaderProgramObject);
	glUniformMatrix4fv(pep_MosaicNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(pep_MosaicNoise_BackUniform, 1);
	glBindVertexArray(pep_MosaicNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	glUseProgram(0);
	//
	// Enable States
	//
	glEnable(GL_TEXTURE_3D);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, pep_MosaicNoise_MathematicalTexture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_MosaicNoise_ShaderProgramObject);
	glUniform1i(pep_MosaicNoise_Sampler3DNoiseUniform, 0);
	glUniformMatrix4fv(pep_MosaicNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform4f(pep_MosaicNoise_SkyColorUniform, 0.0, 0.0, 0.10, 0.0);
	glUniform4f(pep_MosaicNoise_CloudColorUniform, 0.8, 0.8, 0.8, 1.0);
	glUniform1f(pep_MosaicNoise_ScaleUniform, 1.0);
	//glUniform1f(pep_MosaicNoise_OffsetUniform, pep_MosaicNoise_CloudMotion);
	glUniform3f(pep_MosaicNoise_OffsetUniform, pep_MosaicNoise_CloudMotion, pep_MosaicNoise_CloudMotion * 0.1f, pep_MosaicNoise_CloudMotion);
	glUniform1f(pep_MosaicNoise_TilingUniform, 0.750f);
	glUniform1f(pep_MosaicNoise_AlphaUniform, 1.0f);
	glUniform1i(pep_MosaicNoise_BackUniform, 0);

	glBindVertexArray(pep_MosaicNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

