#include "Common.h"
#include "Cloud.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC gHdc = NULL;
HGLRC gHglrc = NULL;
HWND gHwnd = NULL;
FILE *pep_gpFile = NULL;

DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool gbActiveWindow;
bool gbIsFullScreen;

mat4 pep_perspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpSzCmdLine, int iCmdShow)
{
    // function prototype
    int Initialize(void);
    void Display(void);
    void Update(void);

    // variable declarations
    bool bDone = false;
    MSG msg = {0};
    int iRet = 0;
    HWND hwnd;
    WNDCLASSEX wndClass;
    TCHAR szAppName[] = TEXT("Cloud");

    // code
    if (0 != fopen_s(&pep_gpFile, "log.txt", "w"))
    {
        MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Cloud"), MB_OK);
        exit(0);
    }
    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);
    fflush(pep_gpFile);

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.cbWndExtra = 0;
    wndClass.cbClsExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szAppName;
    wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wndClass.lpfnWndProc = WndProc;

    RegisterClassEx(&wndClass);

    hwnd = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName, TEXT("Cloud"),
        WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
        100, 800, 600, NULL, NULL, hInstance, NULL);
    gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    iRet = Initialize();
    if (-1 == iRet)
   {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Failed.\n",
            __FILE__, __LINE__, __FUNCTION__);
        DestroyWindow(hwnd);
    }
    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
        "Successful.\n",
        __FILE__, __LINE__, __FUNCTION__);

    while (bDone == false)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (WM_QUIT == msg.message)
            {
                bDone = true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            if (gbActiveWindow)
            {
            }

            Update();

            Display();
        }
    }

    return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // function declarations
    void ToggledFullScreen(void);
    void Uninitialize(void);
    void ReSize(int, int);

    // code
    switch (iMsg)
    {
        case WM_DESTROY:
            Uninitialize();
            PostQuitMessage(0);
            break;
        case WM_ERASEBKGND:
            return 0;
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_KILLFOCUS:
            gbActiveWindow = false;
            break;
        case WM_SETFOCUS:
            gbActiveWindow = true;
            break;
        case WM_SIZE:
            ReSize(LOWORD(lParam), HIWORD(lParam));
            break;
        case WM_KEYDOWN:
        {
            switch (wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;

                case 'F':
                case 'f':
                    ToggledFullScreen();
                    break;
            }
        } break;
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
    // variable declarations
    MONITORINFO mi;

    // code
    if (false == gbIsFullScreen)
    {
        dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & dwStyle)
        {
            mi = {sizeof(MONITORINFO)};

            if (GetWindowPlacement(gHwnd, &gWpPrev) &&
                GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.top,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);
        gbIsFullScreen = true;
    }
    else
    {
        SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(gHwnd, &gWpPrev);
        SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
            SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
            SWP_NOMOVE | SWP_NOSIZE);

        gbIsFullScreen = false;
        ShowCursor(TRUE);
    }
}

void ReSize(int width, int height)
{
    // code
    if (0 == height)
    {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    pep_perspectiveProjectionMatrix =
        perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    return;
}

int Initialize(void)
{
    // variable delcarations
    int index;
    PIXELFORMATDESCRIPTOR pfd;

    // function declarations
    void ReSize(int, int);
    void Uninitialize(void);

    // code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.cColorBits = 32;
    pfd.cDepthBits = 8;
    pfd.cRedBits = 8;
    pfd.cGreenBits = 8;
    pfd.cBlueBits = 8;
    pfd.cAlphaBits = 8;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

    fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
        __FILE__, __LINE__, __FUNCTION__);
    fflush(pep_gpFile);

    gHdc = GetDC(gHwnd);

    index = ChoosePixelFormat(gHdc, &pfd);
    if (0 == index)
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : ChoosePixelFormat "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    if (FALSE == SetPixelFormat(gHdc, index, &pfd))
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : SetPixelFormat "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    gHglrc = wglCreateContext(gHdc);
    if (NULL == gHglrc)
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : wglCreateContext "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    if (FALSE == wglMakeCurrent(gHdc, gHglrc))
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : wglMakeCurrent "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
        __FILE__, __LINE__, __FUNCTION__);

    GLenum result;
    result = glewInit();
    if (GLEW_OK != result)
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glewInit "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    Cloud_Initialize();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    pep_perspectiveProjectionMatrix = mat4::identity();

    ReSize(800, 600);

    fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
        __FILE__, __LINE__, __FUNCTION__);

    return 0;
}

void Display(void)
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Cloud_Display();

    SwapBuffers(gHdc);

    return;
}

void Uninitialize(void)
{
    Cloud_Uninitialize();

    if (wglGetCurrentContext() == gHglrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    if (NULL != gHglrc)
    {
        wglDeleteContext(gHglrc);
    }

    if (NULL != gHdc)
    {
        ReleaseDC(gHwnd, gHdc);
    }

    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully ",
        __FILE__, __LINE__, __FUNCTION__);

    if (NULL != pep_gpFile)
    {
        fclose(pep_gpFile);
    }

    return;
}

void Update(void)
{
    Cloud_Update();
}
