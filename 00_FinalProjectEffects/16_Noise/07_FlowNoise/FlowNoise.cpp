#include "FlowNoise.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_FlowNoise_Vao;
GLuint pep_FlowNoise_VboPosition;
GLuint pep_FlowNoise_VertexShaderObject;
GLuint pep_FlowNoise_FragmentShaderObject;
GLuint pep_FlowNoise_ShaderProgramObject;

GLuint pep_FlowNoise_MVPUniform;
GLuint pep_FlowNoise_ScaleUniform;
GLuint pep_FlowNoise_OffsetUniform;
GLuint pep_FlowNoise_TilingUniform;
GLuint pep_FlowNoise_AlphaUniform;

GLubyte * pep_FlowNoise_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_FlowNoise_CloudMotion = 0.0f;

int FlowNoise_Initialize(void)
{
	pep_FlowNoise_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_FlowNoise_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_FlowNoise_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_FlowNoise_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_FlowNoise_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_FlowNoise_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_FlowNoise_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_FlowNoise_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_FlowNoise_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec3 mcPosition;" \

		"uniform vec4 u_sky_color;" \
		"uniform vec4 u_cloud_color;" \
		"uniform sampler3D u_noise;" \
		"uniform vec3 u_offset;"\
		"uniform float u_tiling;"\
		"uniform float u_alpha;" \
		"uniform float uGlobalTime;" \
		"out vec4 FragColor;" \

		"vec3 mod289(vec3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }" \
		"vec2 mod289(vec2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }" \
		"vec3 permute(vec3 x) { return mod289(((x*34.0)+1.0)*x); }" \

		"float snoise(vec2 v) {" \
			"const vec4 C = vec4(0.211324865405187, 0.366025403784439, -0.577350269189626,0.024390243902439);" \
			"vec2 i  = floor(v + dot(v, C.yy) );" \
			"vec2 x0 = v -   i + dot(i, C.xx);" \
			" vec2 i1;" \
			"i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);" \
			"vec4 x12 = x0.xyxy + C.xxzz;" \
			"x12.xy -= i1;" \
			" i = mod289(i);" \
			"vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 )) + i.x + vec3(0.0, i1.x, 1.0 ));" \
			"vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);" \
			" m = m*m ;" \
			" m = m*m ;" \
			"vec3 x = 2.0 * fract(p * C.www) - 1.0;" \
			"vec3 h = abs(x) - 0.5;" \
			"vec3 ox = floor(x + 0.5);" \
			"vec3 a0 = x - ox;" \
			" m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );" \
			"vec3 g;" \
			"g.x  = a0.x  * x0.x  + h.x  * x0.y;" \
			" g.yz = a0.yz * x12.xz + h.yz * x12.yw;" \
			"return 130.0 * dot(m, g);" \
		"}																				" \

		"void main(void)" \
		"{" \
			//mcPosition => Behaving like texcoord here
			"vec2 st =  mcPosition.xy * 3.0;" \
			"vec3 color = vec3(0.0);"\
			"vec2 pos = vec2(st*3.);" \
			"float DF = 0.0;" \
			" float a = 0.0;" \
			"vec2 vel = vec2(u_offset.x * 0.1);" \
			"DF += snoise(pos+vel)*.25+.25;" \
			"a = snoise(pos*vec2(cos(u_offset.x*0.15),sin(u_offset.x*0.1))*0.1)*3.1415;" \
			"vel = vec2(cos(a),sin(a));" \
			"DF += snoise(pos+vel)*.25+.25;" \
			"color = vec3( smoothstep(0.7,.75,fract(DF)) );" \
			"FragColor = vec4(color, u_alpha);" \
		"}";

	glShaderSource(pep_FlowNoise_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_FlowNoise_FragmentShaderObject);
	glGetShaderiv(pep_FlowNoise_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_FlowNoise_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_FlowNoise_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_FlowNoise_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_FlowNoise_ShaderProgramObject, pep_FlowNoise_VertexShaderObject);
	glAttachShader(pep_FlowNoise_ShaderProgramObject, pep_FlowNoise_FragmentShaderObject);
	glBindAttribLocation(pep_FlowNoise_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_FlowNoise_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_FlowNoise_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_FlowNoise_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_FlowNoise_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_FlowNoise_MVPUniform = glGetUniformLocation(pep_FlowNoise_ShaderProgramObject, "u_mvp_matrix");
	pep_FlowNoise_ScaleUniform = glGetUniformLocation(pep_FlowNoise_ShaderProgramObject, "u_scale");
	pep_FlowNoise_OffsetUniform = glGetUniformLocation(pep_FlowNoise_ShaderProgramObject, "u_offset");
	pep_FlowNoise_TilingUniform = glGetUniformLocation(pep_FlowNoise_ShaderProgramObject, "u_tiling");
	pep_FlowNoise_AlphaUniform = glGetUniformLocation(pep_FlowNoise_ShaderProgramObject, "u_alpha");

	glGenVertexArrays(1, &pep_FlowNoise_Vao);
	glBindVertexArray(pep_FlowNoise_Vao);
	glGenBuffers(1, &pep_FlowNoise_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_FlowNoise_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void FlowNoise_Resize(int, int)
{

}

void FlowNoise_Update(void)
{
	pep_FlowNoise_CloudMotion += 0.07f;
}

void FlowNoise_Uninitialize(void)
{
	if (pep_FlowNoise_VboPosition)
	{
		glDeleteBuffers(1, &pep_FlowNoise_VboPosition);
		pep_FlowNoise_VboPosition = 0;
	}

	if (pep_FlowNoise_Vao)
	{
		glDeleteVertexArrays(1, &pep_FlowNoise_Vao);
		pep_FlowNoise_Vao = 0;
	}

	if (pep_FlowNoise_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_FlowNoise_ShaderProgramObject);

		glGetProgramiv(pep_FlowNoise_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_FlowNoise_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_FlowNoise_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_FlowNoise_ShaderProgramObject);
		pep_FlowNoise_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void FlowNoise_Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//
	// Enable States
	//
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_FlowNoise_ShaderProgramObject);
	glUniformMatrix4fv(pep_FlowNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1f(pep_FlowNoise_ScaleUniform, 1.0);
	//glUniform1f(pep_FlowNoise_OffsetUniform, pep_FlowNoise_CloudMotion);
	glUniform3f(pep_FlowNoise_OffsetUniform, pep_FlowNoise_CloudMotion, pep_FlowNoise_CloudMotion * 0.1f, pep_FlowNoise_CloudMotion);
	glUniform1f(pep_FlowNoise_TilingUniform,20.0f);
	glUniform1f(pep_FlowNoise_AlphaUniform, 1.0f);

	glBindVertexArray(pep_FlowNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

