#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int DenseCloudNoise_Initialize(void);
void DenseCloudNoise_Resize(int, int);
void DenseCloudNoise_Uninitialize(void);
void DenseCloudNoise_Update(void);
void DenseCloudNoise_Display(void);
