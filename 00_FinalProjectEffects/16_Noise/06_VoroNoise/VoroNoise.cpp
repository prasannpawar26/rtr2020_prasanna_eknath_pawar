#include "VoroNoise.h"

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_VoroNoise_Vao;
GLuint pep_VoroNoise_VboPosition;
GLuint pep_VoroNoise_VertexShaderObject;
GLuint pep_VoroNoise_FragmentShaderObject;
GLuint pep_VoroNoise_ShaderProgramObject;

GLuint pep_VoroNoise_MVPUniform;
GLuint pep_VoroNoise_ScaleUniform;
GLuint pep_VoroNoise_OffsetUniform;
GLuint pep_VoroNoise_TilingUniform;
GLuint pep_VoroNoise_AlphaUniform;

GLubyte * pep_VoroNoise_pNoise3DTexureData = NULL;

float vertices[12] = { 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

float pep_VoroNoise_CloudMotion = 0.0f;

int VoroNoise_Initialize(void)
{
	pep_VoroNoise_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_VoroNoise_VertexShaderObject)
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec4 vPosition;" \

		"uniform mat4 u_mvp_matrix;" \
		"uniform float u_scale;" \

		"out vec3 mcPosition;" \

		"void main(void)" \
		"{" \
			"mcPosition = vec3(vPosition) * u_scale;" \
			"gl_Position = u_mvp_matrix * vPosition;" \
			"mcPosition = vPosition.xyz;" \
		"}";

	glShaderSource(pep_VoroNoise_VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(pep_VoroNoise_VertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_VoroNoise_VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_VoroNoise_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_VoroNoise_VertexShaderObject, iInfoLogLength, &written,
					szInfoLog);
				fprintf(pep_gpFile, "Error: Vertex Shader:\n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(pep_gpFile, "Vertex Shader Compiled Successfully\n");

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_VoroNoise_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_VoroNoise_FragmentShaderObject) 
	{
		fprintf(pep_gpFile, "glCreateShader Failed.\nExitting Now...\n");
		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \

		"in vec3 mcPosition;" \

		"uniform vec4 u_sky_color;" \
		"uniform vec4 u_cloud_color;" \
		"uniform sampler3D u_noise;" \
		"uniform vec3 u_offset;"\
		"uniform float u_tiling;"\
		"uniform float u_alpha;" \
		"uniform float uGlobalTime;" \
		"out vec4 FragColor;" \

		"vec3 hash3(in vec3 p) {														" \
		"	vec3 q = vec3(dot(p, vec3(127.1, 311.7, 189.2)),							" \
		"		dot(p, vec3(269.5, 183.3, 324.7)),										" \
		"		dot(p, vec3(419.2, 371.9, 128.5)));										" \
		"	return fract(sin(q) * 43758.5453);											" \
		"}																				" \
		"																				" \
		"float noise(in vec3 x, float v) {												" \
		"																				" \
		"	vec3 p = floor(x);															" \
		"	vec3 f = fract(x);															" \
		"																				" \
		"	float s = 1.0 + 0.0 * v;													" \
		"	float va = 0.0;																" \
		"	float wt = 0.0;																" \
		"	for (int k = -2; k <= 1; k++)												" \
		"		for (int j = -2; j <= 1; j++)											" \
		"			for (int i = -2; i <= 1; i++) {										" \
		"				vec3 g = vec3(float(i), float(j), float(k));					" \
		"				vec3 o = hash3(p + g);											" \
		"				vec3 r = g - f + o + 0.5;										" \
		"				float d = dot(r, r);											" \
		"				float w = pow(1.0 - smoothstep(0.0, 1.414, sqrt(d)), s);		" \
		"				va += o.z * w;													" \
		"				wt += w;														" \
		"			}																	" \
		"	return va / wt;																" \
		"}																				" \
		"																				" \
		"float fBm(in vec3 p, float v) {												" \
		"	float sum = 0.0;															" \
		"	float amp = 1.0;															" \
		"	for (int i = 0; i < 2; i++) {												" \
		"		sum += amp * noise(p, v);												" \
		"		amp *= 0.4;																" \
		"		p *= 2.0;																" \
		"	}																			" \
		"	return sum;																	" \
		"}																				" \

		"void main(void)" \
		"{" \
			//mcPosition => Behaving like texcoord here
			"vec2 p =  mcPosition.xy * u_tiling;" \
			"vec3 rd = normalize(vec3(p.xy, 1.0));"\
		
			"vec3 pos1 =   vec3(-1.0, 0.0, 0.0) * u_offset.x + rd * 8.0;" \

			"vec3 temp_color = vec3(0.0,0.0,0.0);" \
			"temp_color = vec3(0.5 * fBm(pos1, 0.05),0.5 * fBm(pos1, 0.05),0.5 * fBm(pos1, 0.05));" \
			"FragColor = vec4(temp_color.rgb + vec3(0.0, 0.0, 0.0), u_alpha);" \
		"}";

	glShaderSource(pep_VoroNoise_FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_VoroNoise_FragmentShaderObject);
	glGetShaderiv(pep_VoroNoise_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_VoroNoise_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_VoroNoise_FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "Error - Fragment Shader: \n\t%s\n", szInfoLog);
				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(pep_gpFile, "Fragment Shader Compiled Successfully\n");

	pep_VoroNoise_ShaderProgramObject = glCreateProgram();

	glAttachShader(pep_VoroNoise_ShaderProgramObject, pep_VoroNoise_VertexShaderObject);
	glAttachShader(pep_VoroNoise_ShaderProgramObject, pep_VoroNoise_FragmentShaderObject);
	glBindAttribLocation(pep_VoroNoise_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glLinkProgram(pep_VoroNoise_ShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_VoroNoise_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_VoroNoise_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_VoroNoise_ShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "Error - Shader Program: \n\t%s\n", szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}

	fprintf(pep_gpFile, "Shader Program Linked Successfully\n");

	pep_VoroNoise_MVPUniform = glGetUniformLocation(pep_VoroNoise_ShaderProgramObject, "u_mvp_matrix");
	pep_VoroNoise_ScaleUniform = glGetUniformLocation(pep_VoroNoise_ShaderProgramObject, "u_scale");
	pep_VoroNoise_OffsetUniform = glGetUniformLocation(pep_VoroNoise_ShaderProgramObject, "u_offset");
	pep_VoroNoise_TilingUniform = glGetUniformLocation(pep_VoroNoise_ShaderProgramObject, "u_tiling");
	pep_VoroNoise_AlphaUniform = glGetUniformLocation(pep_VoroNoise_ShaderProgramObject, "u_alpha");

	glGenVertexArrays(1, &pep_VoroNoise_Vao);
	glBindVertexArray(pep_VoroNoise_Vao);
	glGenBuffers(1, &pep_VoroNoise_VboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_VoroNoise_VboPosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return 0;
}

void VoroNoise_Resize(int, int)
{

}

void VoroNoise_Update(void)
{
	pep_VoroNoise_CloudMotion += 0.07f;
}

void VoroNoise_Uninitialize(void)
{
	if (pep_VoroNoise_VboPosition)
	{
		glDeleteBuffers(1, &pep_VoroNoise_VboPosition);
		pep_VoroNoise_VboPosition = 0;
	}

	if (pep_VoroNoise_Vao)
	{
		glDeleteVertexArrays(1, &pep_VoroNoise_Vao);
		pep_VoroNoise_Vao = 0;
	}

	if (pep_VoroNoise_ShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_VoroNoise_ShaderProgramObject);

		glGetProgramiv(pep_VoroNoise_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_VoroNoise_ShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_VoroNoise_ShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_VoroNoise_ShaderProgramObject);
		pep_VoroNoise_ShaderProgramObject = 0;

		glUseProgram(0);
	}
}


void VoroNoise_Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//
	// Enable States
	//
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
	modelViewProjectionMatrix = pep_perspectiveProjectionMatrix * modelViewMatrix;

	glUseProgram(pep_VoroNoise_ShaderProgramObject);
	glUniformMatrix4fv(pep_VoroNoise_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1f(pep_VoroNoise_ScaleUniform, 1.0);
	//glUniform1f(pep_VoroNoise_OffsetUniform, pep_VoroNoise_CloudMotion);
	glUniform3f(pep_VoroNoise_OffsetUniform, pep_VoroNoise_CloudMotion, pep_VoroNoise_CloudMotion * 0.1f, pep_VoroNoise_CloudMotion);
	glUniform1f(pep_VoroNoise_TilingUniform,20.0f);
	glUniform1f(pep_VoroNoise_AlphaUniform, 1.0f);

	glBindVertexArray(pep_VoroNoise_Vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	//
	// Disable States
	//
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
}

