#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int SpreadEffect_Initialize(void);
void SpreadEffect_Resize(int, int);
void SpreadEffect_Uninitialize(void);
void SpreadEffect_Update(void);
void SpreadEffect_Display(void);
