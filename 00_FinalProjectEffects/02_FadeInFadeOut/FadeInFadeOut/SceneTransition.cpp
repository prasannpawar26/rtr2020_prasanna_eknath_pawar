#include "Common.h"
#include "SceneTransition.h"

extern FILE *pep_gpFile;
extern bool pep_bFadeOut;
extern mat4 pep_Perspective_ProjectionMatrix;
extern int pep_AssignmentDisplay;

GLuint pep_SceneTransition_gVertexShaderObject;
GLuint pep_SceneTransition_gFragmentShaderObject;
GLuint pep_SceneTransition_gShaderProgramObject;

GLuint pep_SceneTransition_Square_vao;
GLuint pep_SceneTransition_Square_vbo_position;
GLuint pep_SceneTransition_Square_vbo_color;

GLuint pep_SceneTransition_mvpUniform;
GLuint pep_SceneTransition_AlphaUniform;
GLuint pep_SceneTransition_AlphaUniform1;

GLfloat pep_SceneTransition_Alpha = 1.0;

int SceneTransition_Initialize(void)
{
    fprintf(pep_gpFile, "\nSceneTransition\n");

    /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
    fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_SceneTransition_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec4 vColor;" \

        "uniform mat4 u_mvp_matrix;" \

        "out vec4 out_color;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_color = vColor;" \
        "}";

    glShaderSource(pep_SceneTransition_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_SceneTransition_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_SceneTransition_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SceneTransition_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SceneTransition_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_SceneTransition_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 out_color;" \
        "out vec4 FragColor;" \

        "uniform float u_alpha;" \

        "void main(void)" \
        "{" \
            "FragColor = vec4(0.0, 0.0, 0.0, u_alpha);" \
        "}";

    glShaderSource(pep_SceneTransition_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_SceneTransition_gFragmentShaderObject);

    glGetShaderiv(pep_SceneTransition_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SceneTransition_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SceneTransition_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SceneTransition_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_SceneTransition_gShaderProgramObject, pep_SceneTransition_gVertexShaderObject);
    glAttachShader(pep_SceneTransition_gShaderProgramObject, pep_SceneTransition_gFragmentShaderObject);

    glBindAttribLocation(pep_SceneTransition_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_SceneTransition_gShaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_SceneTransition_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_SceneTransition_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_SceneTransition_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_SceneTransition_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SceneTransition_mvpUniform = glGetUniformLocation(pep_SceneTransition_gShaderProgramObject, "u_mvp_matrix");
    pep_SceneTransition_AlphaUniform  = glGetUniformLocation(pep_SceneTransition_gShaderProgramObject, "u_alpha");
    //
    // Square
    //
    const GLfloat squareVertices[] = {1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
    const GLfloat squareColor[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &pep_SceneTransition_Square_vao);
    glBindVertexArray(pep_SceneTransition_Square_vao);

    glGenBuffers(1, &pep_SceneTransition_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_SceneTransition_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_SceneTransition_Square_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_SceneTransition_Square_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
    /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

	return 0;
}

void SceneTransition_Display(bool fade_in, float alpha_value)
{
    //glEnable(GL_ALPHA_TEST);
    //glAlphaFunc(GL_GREATER, 0.05f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);

    // Render
    glUseProgram(pep_SceneTransition_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Square
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -0.15f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    //
    // Here We Are Using Black Color Square For Scene Trantision
    // 1. If Alpha Of That Suqare Is Equal To 1 => Then Orginal Scene Will Not Be Visible
    // 2. If Alpha Of That Suqare Is Equal To 0 => Then Orginal Scene Will Be Visible
    // 3. It Means That Transition From 0.0 To 1.0 => We are Doing Fade-Out And Transition From 1.0 To 0.0 => We are Doing Fade-In
    //
    if (fade_in)  // 0.0 To 1.0  => Fade Out
    {
        pep_SceneTransition_Alpha += alpha_value;
        if (pep_SceneTransition_Alpha > 1.0)
        {
            pep_SceneTransition_Alpha = 1.0;
            pep_bFadeOut = false;
            pep_AssignmentDisplay += 1;
        }
    }
    else // 0.0 To 1.0  => Fade In
    {
        pep_SceneTransition_Alpha += alpha_value;
        if (0.0 > pep_SceneTransition_Alpha)
        {
            pep_SceneTransition_Alpha = 0.0;
            pep_bFadeOut = true;
        }
    }
   

    glUniformMatrix4fv(pep_SceneTransition_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1f(pep_SceneTransition_AlphaUniform, pep_SceneTransition_Alpha);
    glBindVertexArray(pep_SceneTransition_Square_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_SceneTransition_Square_vbo_color);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_SceneTransition_Square_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindVertexArray(0);

    glUseProgram(0);

    glDisable(GL_BLEND);
    //glDisable(GL_ALPHA_TEST);
    return;
}

void SceneTransition_Update(void)
{
    if (pep_SceneTransition_Alpha < 1.0f)
    {
        //pep_SceneTransition_Alpha += 0.001f;
    }

    return;
}

void SceneTransition_ReSize(int width, int height)
{

}

void SceneTransition_Uninitialize(void)
{
    if (pep_SceneTransition_Square_vbo_color)
    {
        glDeleteBuffers(1, &pep_SceneTransition_Square_vbo_color);
        pep_SceneTransition_Square_vbo_color = 0;
    }

    if (pep_SceneTransition_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_SceneTransition_Square_vbo_position);
        pep_SceneTransition_Square_vbo_position = 0;
    }

    if (pep_SceneTransition_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_SceneTransition_Square_vao);
        pep_SceneTransition_Square_vao = 0;
    }

    if (pep_SceneTransition_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_SceneTransition_gShaderProgramObject);

        glGetProgramiv(pep_SceneTransition_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_SceneTransition_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_SceneTransition_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_SceneTransition_gShaderProgramObject);
        pep_SceneTransition_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

