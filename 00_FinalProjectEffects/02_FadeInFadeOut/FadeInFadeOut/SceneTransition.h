#pragma once

int SceneTransition_Initialize(void);
void SceneTransition_Display(bool fade_in, float alpha_value);
void SceneTransition_Update(void);
void SceneTransition_ReSize(int width, int height);
void SceneTransition_Uninitialize(void);
