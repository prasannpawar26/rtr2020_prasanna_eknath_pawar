#include "Common.h"
#include "Terrain.h"
//#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define PEP_TERRAIN_SIZE 800
#define PEP_TERRAIN_VERTEX_COUNT_X 1024
#define PEP_TERRAIN_VERTEX_COUNT_Z 1024

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

extern Camera camera;
extern mat4 cameraMatrix;
extern vmath::vec3 cameraPosition;
extern vmath::vec3 cameraFront;

GLuint pep_Terrain_gVertexShaderObject;
GLuint pep_Terrain_gFragmentShaderObject;
GLuint pep_Terrain_gShaderProgramObject;

GLuint pep_Terrain_vao;
GLuint pep_Terrain_vbo_position;
GLuint pep_Terrain_vbo_normal;
GLuint pep_Terrain_vbo_texcoord;
GLuint pep_Terrain_vbo_indices;
GLuint pep_Terrain_vbo_tangent;
GLuint pep_Terrain_vbo_bitangent;

GLuint pep_Terrain_Texture;
GLuint pep_Terrain_Texture_JPG;
GLuint pep_Terrain_Texture_Normal_JPG;
GLuint pep_Terrain_Texture_Ao_JPG;

GLuint pep_Terrain_SamplerUniform;
GLuint pep_Terrain_SamplerNormalUniform;
GLuint pep_Terrain_SamplerAoUniform;
GLuint pep_Terrain_modelUniform;
GLuint pep_Terrain_viewUniform;

GLuint pep_Terrain_projectionUniform;
GLuint pep_Terrain_lightAmbientUniform;
GLuint pep_Terrain_lightDiffuseUniform;
GLuint pep_Terrain_lightSpecularUniform;
GLuint pep_Terrain_lightPositionUniform;

extern float pep_Global_lightAmbient[4];
extern float pep_Global_lightDiffuse[4];
extern float pep_Global_lightSpecular[4];
extern float pep_Global_lightPosition[4];

float* pep_Terrain_vertices = NULL;
float* pep_Terrain_normals = NULL;
float* pep_Terrain_texCoords = NULL;
unsigned int* pep_Terrain_indices = NULL;
float* pep_Terrain_tangent = NULL;
float* pep_Terrain_bitangent = NULL;

#define PEP_TERRAIN_BITMAP_ID 0x4D42		// the universal bitmap ID
unsigned char*  pep_Terrain_Hitmap = NULL;
BITMAPINFOHEADER pep_Terrain_BitmapHeader;

int Terrain_Hitmap(const char *filename, BITMAPINFOHEADER *bitmapInfoHeader)
{
    FILE *filePtr;
    BITMAPFILEHEADER	bitmapFileHeader;
    int					imageIdx = 0;
    unsigned char		tempRGB;


    fopen_s(&filePtr, filename, "rb");
    if (filePtr == NULL)
    {
        return -1;
    }

    fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

    if (bitmapFileHeader.bfType != PEP_TERRAIN_BITMAP_ID)
    {
        fclose(filePtr);
        return NULL;
    }

    fread(bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

    fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

    pep_Terrain_Hitmap = (unsigned char*)malloc(bitmapInfoHeader->biSizeImage);

    if (!pep_Terrain_Hitmap)
    {
        free(pep_Terrain_Hitmap);
        fclose(filePtr);
        return -1;
    }

    fread(pep_Terrain_Hitmap, 1, bitmapInfoHeader->biSizeImage, filePtr);

    if (pep_Terrain_Hitmap == NULL)
    {
        fclose(filePtr);
        return -1;
    }

    // swap the R and B values to get RGB since the bitmap color format is in BGR
    for (imageIdx = 0; imageIdx < (int)bitmapInfoHeader->biSizeImage; imageIdx += 3)
    {
        tempRGB = pep_Terrain_Hitmap[imageIdx];
        pep_Terrain_Hitmap[imageIdx] = pep_Terrain_Hitmap[imageIdx + 2];
        pep_Terrain_Hitmap[imageIdx + 2] = tempRGB;
    }

    fclose(filePtr);

    return 0;
}

int Terrain_Generate(void)
{
    pep_Terrain_vertices = (float*)malloc(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));
    memset(pep_Terrain_vertices, 0, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));

    pep_Terrain_normals = (float*)malloc(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));
    memset(pep_Terrain_normals, 0, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));

    pep_Terrain_tangent = (float*)malloc(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));
    memset(pep_Terrain_tangent, 0, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));

    pep_Terrain_bitangent = (float*)malloc(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));
    memset(pep_Terrain_bitangent, 0, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float));

    pep_Terrain_texCoords = (float*)malloc(PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 2 * sizeof(float));
    memset(pep_Terrain_texCoords, 0, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 2 * sizeof(float));

    pep_Terrain_indices = (unsigned*)malloc(6 * (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1) * sizeof( unsigned int));
    memset(pep_Terrain_indices, 0, 6 * (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1) * sizeof( unsigned int));

    int vertexPointer = 0;
    for (int i = 0; i < PEP_TERRAIN_VERTEX_COUNT_Z; i++)
    {
        for (int j = 0; j < PEP_TERRAIN_VERTEX_COUNT_X; j++)
        {
            pep_Terrain_vertices[vertexPointer * 3 + 0] = /*-*/(float)j /*/ ((float)VERTEX_COUNT)*//* * PEP_TERRAIN_SIZE*/;//  j < (VERTEX_COUNT/2 ) ? -(VERTEX_COUNT/2 - j) : (VERTEX_COUNT/2 - j)
            pep_Terrain_vertices[vertexPointer * 3 + 1] = (float)pep_Terrain_Hitmap[(j * PEP_TERRAIN_VERTEX_COUNT_Z + i) * 3] * 0.40f;
            //pep_Terrain_vertices[vertexPointer * 3 + 1] = 1.0f;
            pep_Terrain_vertices[vertexPointer * 3 + 2] = /*-*/(float)i/* / ((float)VERTEX_COUNT)*/ /** PEP_TERRAIN_SIZE*/;

            //pep_Terrain_normals[vertexPointer * 3 + 0] = 0;
            //pep_Terrain_normals[vertexPointer * 3 + 1] = 1;
            //pep_Terrain_normals[vertexPointer * 3 + 2] = 0;

            pep_Terrain_texCoords[vertexPointer * 2 + 0] = (float)j / ((float)PEP_TERRAIN_VERTEX_COUNT_X - 1);
            pep_Terrain_texCoords[vertexPointer * 2 + 1] = (float)i / ((float)PEP_TERRAIN_VERTEX_COUNT_Z - 1);

            vertexPointer++;
        }
    }

    int pointer = 0;
    for (int gz = 0; gz < PEP_TERRAIN_VERTEX_COUNT_Z - 1; gz++)
    {
        for (int gx = 0; gx < PEP_TERRAIN_VERTEX_COUNT_X - 1; gx++)
        {
            int vertex_index = gz * PEP_TERRAIN_VERTEX_COUNT_Z + gx;
            pep_Terrain_indices[pointer + 0] = vertex_index;
            pep_Terrain_indices[pointer + 1] =  vertex_index + PEP_TERRAIN_VERTEX_COUNT_Z + 1;
            pep_Terrain_indices[pointer + 2] = vertex_index + 1;

            pep_Terrain_indices[pointer + 3] = vertex_index;
            pep_Terrain_indices[pointer + 4] = vertex_index + PEP_TERRAIN_VERTEX_COUNT_Z;
            pep_Terrain_indices[pointer + 5] = vertex_index + PEP_TERRAIN_VERTEX_COUNT_Z + 1;


            pointer += 6;
        }
    }

    for (int gz = 0; gz < (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1) * 6; gz +=3)
    {
        //fprintf(pep_gpFile, "\pep_Terrain_indices[%d] : %d\n",gz, pep_Terrain_indices[gz]);
        float pos0[3];
        int vertex_0_Index = pep_Terrain_indices[gz + 0];
        int vertex_1_Index = pep_Terrain_indices[gz + 1];
        int vertex_2_Index = pep_Terrain_indices[gz + 2];

        pos0[0] = pep_Terrain_vertices[vertex_0_Index * 3 + 0];
        pos0[1] = pep_Terrain_vertices[vertex_0_Index * 3 + 1];
        pos0[2] = pep_Terrain_vertices[vertex_0_Index * 3 + 2];

        float pos1[3];
        pos1[0] = pep_Terrain_vertices[vertex_1_Index * 3 + 0];
        pos1[1] = pep_Terrain_vertices[vertex_1_Index * 3 + 1];
        pos1[2] = pep_Terrain_vertices[vertex_1_Index * 3 + 2];

        float pos2[3];
        pos2[0] = pep_Terrain_vertices[vertex_2_Index * 3 + 0];
        pos2[1] = pep_Terrain_vertices[vertex_2_Index * 3 + 1];
        pos2[2] = pep_Terrain_vertices[vertex_2_Index * 3 + 2];

        float tex0[2];
        tex0[0] = pep_Terrain_texCoords[vertex_0_Index * 2 + 0];
        tex0[1] = pep_Terrain_texCoords[vertex_0_Index * 2 + 1];

        float tex1[2];
        tex1[0] = pep_Terrain_texCoords[vertex_1_Index * 2 + 0];
        tex1[1] = pep_Terrain_texCoords[vertex_1_Index * 2 + 1];

        float tex2[2];
        tex2[0] = pep_Terrain_texCoords[vertex_2_Index * 2 + 0];
        tex2[1] = pep_Terrain_texCoords[vertex_2_Index * 2 + 1];

        float edge1[3];
        edge1[0] = pos1[0] - pos0[0];
        edge1[1] = pos1[1] - pos0[1];
        edge1[2] = pos1[2] - pos0[2];

        float edge2[3];
        edge2[0] = pos2[0] - pos0[0];
        edge2[1] = pos2[1] - pos0[1];
        edge2[2] = pos2[2] - pos0[2];

        vec3 v0 = vec3(pos0[0], pos0[1], pos0[2]);
        vec3 v1 = vec3(pos1[0], pos1[1], pos1[2]);
        vec3 v2 = vec3(pos2[0], pos2[1], pos2[2]);

        vec3 normal = normalize(cross(v1 - v0, v2 - v0));
        pep_Terrain_normals[vertex_0_Index * 3 + 0] = normal[0];
        pep_Terrain_normals[vertex_0_Index * 3 + 1] = normal[1];
        pep_Terrain_normals[vertex_0_Index * 3 + 2] = normal[2];
        pep_Terrain_normals[vertex_1_Index * 3 + 0] = normal[0];
        pep_Terrain_normals[vertex_1_Index * 3 + 1] = normal[1];
        pep_Terrain_normals[vertex_1_Index * 3 + 2] = normal[2];
        pep_Terrain_normals[vertex_2_Index * 3 + 0] = normal[0];
        pep_Terrain_normals[vertex_2_Index * 3 + 1] = normal[1];
        pep_Terrain_normals[vertex_2_Index * 3 + 2] = normal[2];


        float DeltaU1 = tex1[0] - tex0[0];
        float DeltaV1 = tex1[1] - tex0[1];
        float DeltaU2 = tex2[0] - tex0[0];
        float DeltaV2 = tex2[1] - tex0[1];

        float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

        float Tangent[3];
        Tangent[0] = f * (DeltaV2 * edge1[0] - DeltaV1 * edge2[0]);
        Tangent[1] = f * (DeltaV2 * edge1[1] - DeltaV1 * edge2[1]);
        Tangent[2] = f * (DeltaV2 * edge1[2] - DeltaV1 * edge2[2]);

        pep_Terrain_tangent[vertex_0_Index * 3 + 0] += Tangent[0];
        pep_Terrain_tangent[vertex_0_Index * 3 + 1] += Tangent[1];
        pep_Terrain_tangent[vertex_0_Index * 3 + 2] += Tangent[2];
        pep_Terrain_tangent[vertex_1_Index * 3 + 0] += Tangent[0];
        pep_Terrain_tangent[vertex_1_Index * 3 + 1] += Tangent[1];
        pep_Terrain_tangent[vertex_1_Index * 3 + 2] += Tangent[2];
        pep_Terrain_tangent[vertex_2_Index * 3 + 0] += Tangent[0];
        pep_Terrain_tangent[vertex_2_Index * 3 + 1] += Tangent[1];
        pep_Terrain_tangent[vertex_2_Index * 3 + 2] += Tangent[2];

        float biTangent[3];
        biTangent[0] = f * (-DeltaU2 * edge1[0] + DeltaU1 * edge2[0]);
        biTangent[1] = f * (-DeltaU2 * edge1[1] + DeltaU1 * edge2[1]);
        biTangent[2] = f * (-DeltaU2 * edge1[2] + DeltaU1 * edge2[2]);

        pep_Terrain_bitangent[vertex_0_Index * 3 + 0] += biTangent[0];
        pep_Terrain_bitangent[vertex_0_Index * 3 + 1] += biTangent[1];
        pep_Terrain_bitangent[vertex_0_Index * 3 + 2] += biTangent[2];
        pep_Terrain_bitangent[vertex_1_Index * 3 + 0] += biTangent[0];
        pep_Terrain_bitangent[vertex_1_Index * 3 + 1] += biTangent[1];
        pep_Terrain_bitangent[vertex_1_Index * 3 + 2] += biTangent[2];
        pep_Terrain_bitangent[vertex_2_Index * 3 + 0] += biTangent[0];
        pep_Terrain_bitangent[vertex_2_Index * 3 + 1] += biTangent[1];
        pep_Terrain_bitangent[vertex_2_Index * 3 + 2] += biTangent[2];
    }

    return 0;
}

int Terrain_Initialize(void)
{
    fprintf(pep_gpFile, "\nTerrain\n");

    pep_Terrain_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "in vec2 vTexCoord;" \
        "in vec3 vTangent;" \
        "in vec3 vBitangent;" \

        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_position;" \

        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \

        "out vec2 out_texcoord;" \
        "out mat3 TBN;" \
        "void main(void)" \
        "{" \
            "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \

            "mat3 normal_matrix = transpose(inverse(mat3(u_model_matrix)));" \

            "vec3 vertexNormal = normalize(normal_matrix * vNormal);"
            "vec3 vertexTangent = normalize(normal_matrix * vTangent);"
            "vertexTangent = normalize(vertexTangent - dot(vertexTangent, vertexNormal) * vertexNormal);" \
            "vec3 vertexBitangent = cross(vertexNormal, vertexTangent);"
            "TBN = transpose(mat3(vertexTangent, vertexBitangent, vertexNormal));"

            "light_direction = TBN * vec3(u_light_position - eye_coordinates);" \

            "viewer_vector = TBN * vec3(-eye_coordinates);" \

            "out_texcoord = vTexCoord;" \

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";

    glShaderSource(pep_Terrain_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_Terrain_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Terrain_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Terrain_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Terrain_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_Terrain_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \
        "in mat3 TBN;" \
        "in vec3 light_direction;" \
        "vec3 tranformation_matrix;" \
        "in vec3 viewer_vector;" \

        "uniform vec3 u_light_ambient;" \
        "uniform vec3 u_light_diffuse;" \
        "uniform vec3 u_light_specular;" \

        "uniform sampler2D u_sampler;" \
        "uniform sampler2D u_sampler_normal;" \
        "uniform sampler2D u_sampler_ao;" \
        
        "vec3 phong_ads_light;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "vec4 Diffuse = texture(u_sampler, out_texcoord * 10.0);" \

            "vec3 TextureNormal = texture(u_sampler_normal, out_texcoord * 10.0).xyz;" \

            "float Ambient = texture(u_sampler_ao, out_texcoord * 10.0).r;" \

            // transform normal vector to range [-1,1]
            "TextureNormal = normalize(TextureNormal * 2.0 - 1.0);" \

            "vec3 light_direction_normalize = normalize(light_direction);" \

            "vec3 tranformation_matrix_normalize = TextureNormal;" \

            "vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \

            "vec3 viewer_vector_normal = normalize(viewer_vector);" \

            "float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \

            "vec3 diffuse = u_light_diffuse * Diffuse.rgb * t_normal_dot_light_direction;" \

            "vec3 specular = u_light_specular * Diffuse.rgb * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), 0.25f * 128.0f);" \

            "vec3 ambient = vec3(0.03) * Ambient;"
            "phong_ads_light= ambient + diffuse + specular;" \

            //"phong_ads_light= (u_light_ambient * Ambient) + diffuse + specular;" \

            "float gamma = 2.2;" \
            "phong_ads_light = pow(phong_ads_light.rgb, vec3(1.0/gamma));" \

            "FragColor = vec4(phong_ads_light, 1.0);" \
            
           // "FragColor = Diffuse;" \

        "}";

    glShaderSource(pep_Terrain_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_Terrain_gFragmentShaderObject);

    glGetShaderiv(pep_Terrain_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Terrain_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Terrain_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Terrain_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_Terrain_gShaderProgramObject, pep_Terrain_gVertexShaderObject);
    glAttachShader(pep_Terrain_gShaderProgramObject, pep_Terrain_gFragmentShaderObject);

    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");
    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_TANGENT, "vTangent");
    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_BITANGENT, "vBitangent");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_Terrain_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Terrain_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_Terrain_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Terrain_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Terrain_modelUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_model_matrix");
    pep_Terrain_viewUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_view_matrix");
    pep_Terrain_projectionUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_projection_matrix");
    pep_Terrain_SamplerUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_sampler");
    pep_Terrain_SamplerNormalUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_sampler_normal");
    pep_Terrain_SamplerAoUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_sampler_ao");
    pep_Terrain_lightAmbientUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_light_ambient");
    pep_Terrain_lightDiffuseUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_light_diffuse");
    pep_Terrain_lightSpecularUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_light_specular");
    pep_Terrain_lightPositionUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_light_position");

    Terrain_Hitmap("unnamed.bmp"/*"Terrain-Heightmap-Result.bmp"*/, &pep_Terrain_BitmapHeader);
    Terrain_Generate();

    //
    // Cube
    //

    glGenVertexArrays(1, &pep_Terrain_vao);
    glBindVertexArray(pep_Terrain_vao);

    glGenBuffers(1, &pep_Terrain_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float), pep_Terrain_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float), pep_Terrain_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_texcoord);
    glBufferData(GL_ARRAY_BUFFER, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 2 * sizeof(float), pep_Terrain_texCoords, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_indices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Terrain_vbo_indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1) * sizeof(unsigned int), pep_Terrain_indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_tangent);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_tangent);
    glBufferData(GL_ARRAY_BUFFER, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float), pep_Terrain_tangent, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TANGENT);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_bitangent);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_bitangent);
    glBufferData(GL_ARRAY_BUFFER, PEP_TERRAIN_VERTEX_COUNT_X * PEP_TERRAIN_VERTEX_COUNT_Z * 3 * sizeof(float), pep_Terrain_bitangent, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_BITANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_BITANGENT);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //Terrain_LoadTexture(&pep_Terrain_Texture, MAKEINTRESOURCEA(THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_VIJAY_KUNDALI));

    Terrain_LoadTextureSTB(&pep_Terrain_Texture_JPG, "aerial_grass_rock_diff_4k.jpg");
    Terrain_LoadTextureSTB(&pep_Terrain_Texture_Normal_JPG, "aerial_grass_rock_nor_4k.jpg");
    Terrain_LoadTextureSTB(&pep_Terrain_Texture_Ao_JPG, "aerial_grass_rock_rough_ao_4k.jpg");

    return 0;
}

void Terrain_Display(void)
{
    cameraMatrix		= camera.GetViewMatrix();
    //camera.Position	+= camera.Front * 0.5f;
    cameraPosition		= camera.GetCameraPosition();
    cameraFront			= camera.GetCameraFront();

    glClearDepth(1.0f);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE_MODE);
    glDepthFunc(GL_LEQUAL);

    // Render
    glUseProgram(pep_Terrain_gShaderProgramObject);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    //
    // Cube
    //

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_Terrain_SamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_Terrain_Texture_JPG);

    glActiveTexture(GL_TEXTURE1);
    glUniform1i(pep_Terrain_SamplerNormalUniform, 1);
    glBindTexture(GL_TEXTURE_2D, pep_Terrain_Texture_Normal_JPG);

    glActiveTexture(GL_TEXTURE2);
    glUniform1i(pep_Terrain_SamplerAoUniform, 2);
    glBindTexture(GL_TEXTURE_2D, pep_Terrain_Texture_Ao_JPG);

    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    modelMatrix = translate(-128.0f, 0.0f, -128.0f);

    viewMatrix = cameraMatrix;

    glUniformMatrix4fv(pep_Terrain_modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_Terrain_viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_Terrain_projectionUniform, 1, GL_FALSE, pep_Perspective_ProjectionMatrix);

    glUniform3fv(pep_Terrain_lightAmbientUniform, 1, pep_Global_lightAmbient);
    glUniform3fv(pep_Terrain_lightDiffuseUniform, 1, pep_Global_lightDiffuse);
    glUniform3fv(pep_Terrain_lightSpecularUniform, 1, pep_Global_lightSpecular);
    glUniform4fv(pep_Terrain_lightPositionUniform, 1, pep_Global_lightPosition);


    glBindVertexArray(pep_Terrain_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Terrain_vbo_indices);
    glDrawElements(GL_TRIANGLES, 6 * (PEP_TERRAIN_VERTEX_COUNT_X - 1) * (PEP_TERRAIN_VERTEX_COUNT_Z - 1), GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE_MODE);

    return;
}

void Terrain_Update(void)
{
    return;
}

void Terrain_ReSize(int width, int height)
{

}

void Terrain_Uninitialize(void)
{
    if (pep_Terrain_vbo_texcoord)
    {
        glDeleteBuffers(1, &pep_Terrain_vbo_texcoord);
        pep_Terrain_vbo_texcoord = 0;
    }

    if (pep_Terrain_vbo_position)
    {
        glDeleteBuffers(1, &pep_Terrain_vbo_position);
        pep_Terrain_vbo_position = 0;
    }

    if (pep_Terrain_vao)
    {
        glDeleteVertexArrays(1, &pep_Terrain_vao);
        pep_Terrain_vao = 0;
    }

    if (pep_Terrain_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Terrain_gShaderProgramObject);

        glGetProgramiv(pep_Terrain_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Terrain_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Terrain_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Terrain_gShaderProgramObject);
        pep_Terrain_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

BOOL Terrain_LoadTextureSTB(GLuint *texture, const char *filename)
{
    int width, height, nrChannels;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        fprintf(pep_gpFile, "Failed To Load Texture");
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    return true;
}
BOOL Terrain_LoadTexture(GLuint *texture, CHAR imageResourceID[])
{
    // variable declarations
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;

    // code
    hBitmap = (HBITMAP)LoadImageA(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (NULL == hBitmap)
    {
        return bStatus;
    }

    bStatus = TRUE;

    GetObject(hBitmap, sizeof(bmp), &bmp);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    DeleteObject(hBitmap);

    return bStatus;

}