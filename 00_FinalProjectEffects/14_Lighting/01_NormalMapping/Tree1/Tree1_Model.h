#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TREE1_MODEL_VERTEX_IN_3D_PLANE 3
#define TREE1_MODEL_TEXCOORD 2
#define TREE1_MODEL_NORMAL_IN_3D_PLANE 3
#define TREE1_MODEL_FACE_TUPLE 3
#define TREE1_MODEL_LINE_LENGTH 256

typedef struct _TREE1_TREE1_MODEL_DATA
{
	int data_count;
	float *pVertices;
	float *pTexcoords;
	float *pNormals;
	float *pTangent;
	float *pBitangent;
} TREE1_MODEL_DATA;

int Tree1_LoadModel(void);

int Tree1_Initialize(void);
void Tree1_Display(void);
void Tree1_Update(void);
void Tree1_ReSize(int width, int height);
void Tree1_Uninitialize(void);
BOOL Tree1_LoadTexture(GLuint* texture, CHAR imageResourceID[]);
BOOL Tree1_LoadTextureSTBDiffuse(GLuint* texture, const char *filename);
BOOL Tree1_LoadTextureSTBNormal(GLuint* texture, const char *filename);
BOOL Tree1_LoadTextureSTBAo(GLuint* texture, const char *filename);
BOOL Tree1_LoadTextureSTBAo2(GLuint* texture, const char *filename);
