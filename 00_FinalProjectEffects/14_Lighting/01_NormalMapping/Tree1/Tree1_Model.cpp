#include "..\Common.h"
#include "Tree1_Model.h"
#define STB_IMAGE_IMPLEMENTATION
#include "..\stb_image.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;
extern Camera camera;
extern mat4 cameraMatrix;
extern vmath::vec3 cameraPosition;
extern vmath::vec3 cameraFront;

extern float pep_Global_lightPosition[4];
extern float pep_Global_lightAmbient[4];
extern float pep_Global_lightDiffuse[4];
extern float pep_Global_lightSpecular[4];


TREE1_MODEL_DATA *tree1_gpDataArray;
int tree1_gObjectCount;

FILE *tree1_gpFileObj = NULL;

GLuint pep_Tree1_gVertexShaderObject;
GLuint pep_Tree1_gFragmentShaderObject;
GLuint pep_Tree1_gShaderProgramObject;

GLuint *pep_Tree1_pVao;
GLuint *pep_Tree1_pVbo_position;
GLuint *pep_Tree1_pVbo_normal;
GLuint *pep_Tree1_pVbo_texcoord;
GLuint *pep_Tree1_pVbo_indices;
GLuint *pep_Tree1_pVbo_tangent;
GLuint *pep_Tree1_pVbo_bitangent;

GLuint pep_Tree1_Trunk_Texture_JPG;
GLuint pep_Tree1_Trunk_Texture_Normal_JPG;
GLuint pep_Tree1_Trunk_Texture_Ao_JPG;

GLuint pep_Tree1_Leaves_Texture_JPG;
GLuint pep_Tree1_Leaves_Texture_Normal_JPG;
GLuint pep_Tree1_Leaves_Texture_Ao_JPG;

GLuint pep_Tree1_SamplerUniform;
GLuint pep_Tree1_SamplerNormalUniform;
GLuint pep_Tree1_SamplerAoUniform;

GLuint pep_Tree1_lightAmbientUniform;
GLuint pep_Tree1_lightDiffuseUniform;
GLuint pep_Tree1_lightSpecularUniform;
GLuint pep_Tree1_lightPositionUniform;

GLuint pep_Tree1_materialAmbientUniform;
GLuint pep_Tree1_materialDiffuseUniform;
GLuint pep_Tree1_materialSpecularUniform;
GLuint pep_Tree1_materialPositionUniform;

GLuint pep_Tree1_modelUniform;
GLuint pep_Tree1_viewUniform;
GLuint pep_Tree1_projectionUniform;

int Tree1_LoadModel(void)
{
	typedef struct _MODEL
	{
		int object_count;
		//
		// Complete Vertices Data (Not Properly arrange)  -> To Get Proper Faces/Arranged Data Of Each Object Refer Per Object Arrays And Use Elements/Indices To Get Proper Value of Vertices Data In Below Mentioned Arrays
		// 1. Vertex Position
		// 2. Normals
		// 3. TexCoords
		//
		int v_count; // count without vec3 touple
		float *pVerticesArray;

		int vn_count;
		float *pNormalsArray;

		int vt_count;
		float *pTexcoordsArray;

	} MODEL;

	MODEL model;

	typedef struct _OBJECT
	{
		// Face/Indices To Used In  Model Arrays
		int element_count; // count without vec3 touple
		int *pElementVertices;
		int *pElementTexcoord;
		int *pElementNormal;

	} OBJECT;
	OBJECT *pObjectArray = NULL;

	char buf[TREE1_MODEL_LINE_LENGTH];

	if (0 != fopen_s(&tree1_gpFileObj, ".\\Tree1\\Tree1.obj", "r"))
	{
		return 1;
	}

	memset(&model, 0, sizeof(MODEL));

	unsigned int line_index = 0;
	unsigned int object_count = 0;
	memset(buf, 0, TREE1_MODEL_LINE_LENGTH);
	while (NULL != fgets(buf, TREE1_MODEL_LINE_LENGTH, tree1_gpFileObj))
	{
		char *context = NULL;
		char *token = strtok_s(buf, " ", &context);

		if (0 == strcmp(token, "o"))
		{
			if (NULL == pObjectArray)
			{
				pObjectArray = (OBJECT *)malloc(sizeof(OBJECT));
				memset(pObjectArray, 0, sizeof(OBJECT));
				model.object_count = 1;
			}
			else
			{
				model.object_count += 1;
				pObjectArray = (OBJECT *)realloc(pObjectArray, model.object_count * sizeof(OBJECT));
				memset(pObjectArray + model.object_count - 1, 0, sizeof(OBJECT));
			}
		}
		else if (0 == strcmp(token, "v"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pVerticesArray)
			{
				model.v_count = 3;
				model.pVerticesArray = (float *)malloc(model.v_count * sizeof(float));
				memset(model.pVerticesArray, 0, sizeof(model.v_count * sizeof(float)));
			}
			else
			{
				model.v_count += 3;
				model.pVerticesArray = (float *)realloc(model.pVerticesArray, model.v_count * sizeof(float));
			}

			model.pVerticesArray[model.v_count - 3] = x;
			model.pVerticesArray[model.v_count - 2] = y;
			model.pVerticesArray[model.v_count - 1] = z;

		}
		else if (0 == strcmp(token, "vn"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pNormalsArray)
			{
				model.vn_count = 3;
				model.pNormalsArray = (float *)malloc(model.vn_count * sizeof(float));
				memset(model.pNormalsArray, 0, sizeof(model.vn_count * sizeof(float)));
			}
			else
			{
				model.vn_count += 3;
				model.pNormalsArray = (float *)realloc(model.pNormalsArray, model.vn_count * sizeof(float));
			}

			model.pNormalsArray[model.vn_count - 3] = x;
			model.pNormalsArray[model.vn_count - 2] = y;
			model.pNormalsArray[model.vn_count - 1] = z;

		}
		else if (0 == strcmp(token, "vt"))
		{
			float x, y;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pTexcoordsArray)
			{
				model.vt_count = 2;
				model.pTexcoordsArray = (float *)malloc(model.vt_count * sizeof(float));
				memset(model.pTexcoordsArray, 0, sizeof(model.vt_count * sizeof(float)));
			}
			else
			{
				model.vt_count += 2;
				model.pTexcoordsArray = (float *)realloc(model.pTexcoordsArray, model.vt_count * sizeof(float));
			}

			model.pTexcoordsArray[model.vt_count - 2] = x;
			model.pTexcoordsArray[model.vt_count - 1] = y;
		}

		else if (0 == strcmp(token, "f"))
		{
			char *first_token = strtok_s(NULL, " ", &context);
			char *second_token = strtok_s(NULL, " ", &context);
			char *third_token = strtok_s(NULL, " ", &context);

			char *first_context = NULL;
			int v1 = (int)atoi(strtok_s(first_token, "/", &first_context));
			int vt1 = (int)atoi(strtok_s(NULL, "/", &first_context));
			int vn1 = (int)atoi(strtok_s(NULL, "/", &first_context));

			char *second_context = NULL;
			int v2 = (int)atoi(strtok_s(second_token, "/", &second_context));
			int vt2 = (int)atoi(strtok_s(NULL, "/", &second_context));
			int vn2 = (int)atoi(strtok_s(NULL, "/", &second_context));

			char *third_context = NULL;
			int v3 = (int)atoi(strtok_s(third_token, "/", &third_context));
			int vt3 = (int)atoi(strtok_s(NULL, "/", &third_context));
			int vn3 = (int)atoi(strtok_s(NULL, "/", &third_context));

			pObjectArray[model.object_count - 1].element_count += 3;
			if (NULL == pObjectArray[model.object_count - 1].pElementVertices)
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementVertices, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)realloc(pObjectArray[model.object_count - 1].pElementVertices, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 3] = v1;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 2] = v2;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 1] = v3;

			if (NULL == pObjectArray[model.object_count - 1].pElementNormal)
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementNormal, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)realloc(pObjectArray[model.object_count - 1].pElementNormal, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 3] = vn1;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 2] = vn2;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 1] = vn3;

			if (NULL == pObjectArray[model.object_count - 1].pElementTexcoord)
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementTexcoord, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)realloc(pObjectArray[model.object_count - 1].pElementTexcoord, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 3] = vt1;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 2] = vt2;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 1] = vt3;
		}

		memset(buf, 0, TREE1_MODEL_LINE_LENGTH);
	}


	//tree1_gObjectCount = model.object_count;
	tree1_gObjectCount = model.object_count;

	//tree1_gObjectCount = model.object_count;
	tree1_gpDataArray = (TREE1_MODEL_DATA *)malloc(sizeof(TREE1_MODEL_DATA) * model.object_count);
	memset(tree1_gpDataArray, 0, sizeof(TREE1_MODEL_DATA) * model.object_count);

	for (int i = 0; i <tree1_gObjectCount; i++)
	{
		tree1_gpDataArray[i].data_count = pObjectArray[i].element_count / TREE1_MODEL_FACE_TUPLE;

		tree1_gpDataArray[i].pVertices = (float *)malloc(sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_VERTEX_IN_3D_PLANE);
		memset(tree1_gpDataArray[i].pVertices, 0, sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_VERTEX_IN_3D_PLANE);

		tree1_gpDataArray[i].pTexcoords = (float *)malloc(sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_TEXCOORD);
		memset(tree1_gpDataArray[i].pTexcoords, 0, sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_TEXCOORD);

		tree1_gpDataArray[i].pNormals = (float *)malloc(sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_NORMAL_IN_3D_PLANE);
		memset(tree1_gpDataArray[i].pNormals, 0, sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_NORMAL_IN_3D_PLANE);

		tree1_gpDataArray[i].pTangent = (float *)malloc(sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_NORMAL_IN_3D_PLANE);
		memset(tree1_gpDataArray[i].pTangent, 0, sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_NORMAL_IN_3D_PLANE);

		tree1_gpDataArray[i].pBitangent = (float *)malloc(sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_NORMAL_IN_3D_PLANE);
		memset(tree1_gpDataArray[i].pBitangent, 0, sizeof(float) * tree1_gpDataArray[i].data_count * TREE1_MODEL_FACE_TUPLE * TREE1_MODEL_NORMAL_IN_3D_PLANE);

		int vi = 0;
		int vt = 0;
		int vn = 0;
		for (int fn = 0; fn < pObjectArray[i].element_count / TREE1_MODEL_FACE_TUPLE; fn++)
		{
			int v1 = pObjectArray[i].pElementVertices[fn * TREE1_MODEL_FACE_TUPLE + 0] - 1; // Index is array start with Zero Hence "-1" From The Element Index Which Is Start From 1
			int v2 = pObjectArray[i].pElementVertices[fn * TREE1_MODEL_FACE_TUPLE + 1] - 1;
			int v3 = pObjectArray[i].pElementVertices[fn * TREE1_MODEL_FACE_TUPLE + 2] - 1;

			tree1_gpDataArray[i].pVertices[vi + 0] = model.pVerticesArray[v1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			tree1_gpDataArray[i].pVertices[vi + 1] = model.pVerticesArray[v1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			tree1_gpDataArray[i].pVertices[vi + 2] = model.pVerticesArray[v1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];
			tree1_gpDataArray[i].pVertices[vi + 3] = model.pVerticesArray[v2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			tree1_gpDataArray[i].pVertices[vi + 4] = model.pVerticesArray[v2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			tree1_gpDataArray[i].pVertices[vi + 5] = model.pVerticesArray[v2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];
			tree1_gpDataArray[i].pVertices[vi + 6] = model.pVerticesArray[v3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			tree1_gpDataArray[i].pVertices[vi + 7] = model.pVerticesArray[v3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			tree1_gpDataArray[i].pVertices[vi + 8] = model.pVerticesArray[v3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];

			int t1 = pObjectArray[i].pElementTexcoord[fn * TREE1_MODEL_FACE_TUPLE + 0] - 1;
			int t2 = pObjectArray[i].pElementTexcoord[fn * TREE1_MODEL_FACE_TUPLE + 1] - 1;
			int t3 = pObjectArray[i].pElementTexcoord[fn * TREE1_MODEL_FACE_TUPLE + 2] - 1;

			tree1_gpDataArray[i].pTexcoords[vt + 0] = model.pTexcoordsArray[t1 * TREE1_MODEL_TEXCOORD + 0];
			tree1_gpDataArray[i].pTexcoords[vt + 1] = model.pTexcoordsArray[t1 * TREE1_MODEL_TEXCOORD + 1];
			tree1_gpDataArray[i].pTexcoords[vt + 2] = model.pTexcoordsArray[t2 * TREE1_MODEL_TEXCOORD + 0];
			tree1_gpDataArray[i].pTexcoords[vt + 3] = model.pTexcoordsArray[t2 * TREE1_MODEL_TEXCOORD + 1];
			tree1_gpDataArray[i].pTexcoords[vt + 4] = model.pTexcoordsArray[t3 * TREE1_MODEL_TEXCOORD + 0];
			tree1_gpDataArray[i].pTexcoords[vt + 5] = model.pTexcoordsArray[t3 * TREE1_MODEL_TEXCOORD + 1];

			int n1 = pObjectArray[i].pElementNormal[fn * TREE1_MODEL_FACE_TUPLE + 0] - 1;
			int n2 = pObjectArray[i].pElementNormal[fn * TREE1_MODEL_FACE_TUPLE + 1] - 1;
			int n3 = pObjectArray[i].pElementNormal[fn * TREE1_MODEL_FACE_TUPLE + 2] - 1;

			tree1_gpDataArray[i].pNormals[vn + 0] = model.pNormalsArray[n1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			tree1_gpDataArray[i].pNormals[vn + 1] = model.pNormalsArray[n1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			tree1_gpDataArray[i].pNormals[vn + 2] = model.pNormalsArray[n1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];
			tree1_gpDataArray[i].pNormals[vn + 3] = model.pNormalsArray[n2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			tree1_gpDataArray[i].pNormals[vn + 4] = model.pNormalsArray[n2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			tree1_gpDataArray[i].pNormals[vn + 5] = model.pNormalsArray[n2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];
			tree1_gpDataArray[i].pNormals[vn + 6] = model.pNormalsArray[n3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			tree1_gpDataArray[i].pNormals[vn + 7] = model.pNormalsArray[n3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			tree1_gpDataArray[i].pNormals[vn + 8] = model.pNormalsArray[n3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];

			float pos0[3];
			pos0[0] = tree1_gpDataArray[i].pVertices[vi + 0];
			pos0[1] = tree1_gpDataArray[i].pVertices[vi + 1];
			pos0[2] = tree1_gpDataArray[i].pVertices[vi + 2];

			float pos1[3];
			pos1[0] = tree1_gpDataArray[i].pVertices[vi + 3];
			pos1[1] = tree1_gpDataArray[i].pVertices[vi + 4];
			pos1[2] = tree1_gpDataArray[i].pVertices[vi + 5];

			float pos2[3];
			pos2[0] = tree1_gpDataArray[i].pVertices[vi + 6];
			pos2[1] = tree1_gpDataArray[i].pVertices[vi + 7];
			pos2[2] = tree1_gpDataArray[i].pVertices[vi + 8];

			float tex0[2];
			tex0[0] = tree1_gpDataArray[i].pTexcoords[vt + 0];
			tex0[1] = tree1_gpDataArray[i].pTexcoords[vt + 1];

			float tex1[2];
			tex1[0] = tree1_gpDataArray[i].pTexcoords[vt + 2];
			tex1[1] = tree1_gpDataArray[i].pTexcoords[vt + 3];

			float tex2[2];
			tex2[0] = tree1_gpDataArray[i].pTexcoords[vt + 4];
			tex2[1] = tree1_gpDataArray[i].pTexcoords[vt + 5];

			float edge1[3];
			edge1[0] = pos1[0] - pos0[0];
			edge1[1] = pos1[1] - pos0[1];
			edge1[2] = pos1[2] - pos0[2];

			float edge2[3];
			edge2[0] = pos2[0] - pos0[0];
			edge2[1] = pos2[1] - pos0[1];
			edge2[2] = pos2[2] - pos0[2];


			float DeltaU1 = tex1[0] - tex0[0];
			float DeltaV1 = tex1[1] - tex0[1];
			float DeltaU2 = tex2[0] - tex0[0];
			float DeltaV2 = tex2[1] - tex0[1];

			float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

			float Tangent[3];
			Tangent[0] = f * (DeltaV2 * edge1[0] - DeltaV1 * edge2[0]);
			Tangent[1] = f * (DeltaV2 * edge1[1] - DeltaV1 * edge2[1]);
			Tangent[2] = f * (DeltaV2 * edge1[2] - DeltaV1 * edge2[2]);

			tree1_gpDataArray[i].pTangent[vi + 0] += Tangent[0];
			tree1_gpDataArray[i].pTangent[vi + 1] += Tangent[1];
			tree1_gpDataArray[i].pTangent[vi + 2] += Tangent[2];
			tree1_gpDataArray[i].pTangent[vi + 3] += Tangent[0];
			tree1_gpDataArray[i].pTangent[vi + 4] += Tangent[1];
			tree1_gpDataArray[i].pTangent[vi + 5] += Tangent[2];
			tree1_gpDataArray[i].pTangent[vi + 6] += Tangent[0];
			tree1_gpDataArray[i].pTangent[vi + 7] += Tangent[1];
			tree1_gpDataArray[i].pTangent[vi + 8] += Tangent[2];

			float biTangent[3];
			biTangent[0] = f * (-DeltaU2 * edge1[0] + DeltaU1 * edge2[0]);
			biTangent[1] = f * (-DeltaU2 * edge1[1] + DeltaU1 * edge2[1]);
			biTangent[2] = f * (-DeltaU2 * edge1[2] + DeltaU1 * edge2[2]);

			tree1_gpDataArray[i].pBitangent[vi + 0] += biTangent[0];
			tree1_gpDataArray[i].pBitangent[vi + 1] += biTangent[1];
			tree1_gpDataArray[i].pBitangent[vi + 2] += biTangent[2];
			tree1_gpDataArray[i].pBitangent[vi + 3] += biTangent[0];
			tree1_gpDataArray[i].pBitangent[vi + 4] += biTangent[1];
			tree1_gpDataArray[i].pBitangent[vi + 5] += biTangent[2];
			tree1_gpDataArray[i].pBitangent[vi + 6] += biTangent[0];
			tree1_gpDataArray[i].pBitangent[vi + 7] += biTangent[1];
			tree1_gpDataArray[i].pBitangent[vi + 8] += biTangent[2];

			vi += 9;
			vt += 6;
			vn += 9;
		}
	}

	/*
	for (int i = 0; i < tree1_gObjectCount; i++)
	{
		for (unsigned int fn = 0; fn < pObjectArray[i].element_count / TREE1_MODEL_FACE_TUPLE; fn += 3)
		{
			int v1 = pObjectArray[i].pElementVertices[fn * TREE1_MODEL_FACE_TUPLE + 0] -1; // Index is array start with Zero Hence "-1" From The Element Index Which Is Start From 1
			int v2 = pObjectArray[i].pElementVertices[fn * TREE1_MODEL_FACE_TUPLE + 1] -1;
			int v3 = pObjectArray[i].pElementVertices[fn * TREE1_MODEL_FACE_TUPLE + 2] -1;

			float pos0[3];
			pos0[0] = tree1_gpDataArray[i].pVertices[v1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			pos0[1] = tree1_gpDataArray[i].pVertices[v1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			pos0[2] = tree1_gpDataArray[i].pVertices[v1 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];

			float pos1[3];
			pos1[0] = tree1_gpDataArray[i].pVertices[v2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			pos1[1] = tree1_gpDataArray[i].pVertices[v2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			pos1[2] = tree1_gpDataArray[i].pVertices[v2 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];

			float pos2[3];
			pos2[0] = tree1_gpDataArray[i].pVertices[v3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 0];
			pos2[1] = tree1_gpDataArray[i].pVertices[v3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 1];
			pos2[2] = tree1_gpDataArray[i].pVertices[v3 * TREE1_MODEL_VERTEX_IN_3D_PLANE + 2];

			float tex0[2];
			tex0[0] = tree1_gpDataArray[i].pTexcoords[v1 * TREE1_MODEL_TEXCOORD + 0];
			tex0[1] = tree1_gpDataArray[i].pTexcoords[v1 * TREE1_MODEL_TEXCOORD + 1];

			float tex1[2];
			tex1[0] = tree1_gpDataArray[i].pTexcoords[v2 * TREE1_MODEL_TEXCOORD + 0];
			tex1[1] = tree1_gpDataArray[i].pTexcoords[v2 * TREE1_MODEL_TEXCOORD + 1];

			float tex2[2];
			tex2[0] = tree1_gpDataArray[i].pTexcoords[v3 * TREE1_MODEL_TEXCOORD + 0];
			tex2[1] = tree1_gpDataArray[i].pTexcoords[v3 * TREE1_MODEL_TEXCOORD + 1];

			float edge1[3];
			edge1[0] = pos1[0] - pos0[0];
			edge1[1] = pos1[1] - pos0[1];
			edge1[2] = pos1[2] - pos0[2];

			float edge2[3];
			edge2[0] = pos2[0] - pos0[0];
			edge2[1] = pos2[1] - pos0[1];
			edge2[2] = pos2[2] - pos0[2];

			vec3 vertex0 = vec3(pos0[0], pos0[1], pos0[2]);
			vec3 vertex1 = vec3(pos1[0], pos1[1], pos1[2]);
			vec3 vertex2 = vec3(pos2[0], pos2[1], pos2[2]);

			float DeltaU1 = tex1[0] - tex0[0];
			float DeltaV1 = tex1[1] - tex0[1];
			float DeltaU2 = tex2[0] - tex0[0];
			float DeltaV2 = tex2[1] - tex0[1];

			float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

			float Tangent[3];
			Tangent[0] = f * (DeltaV2 * edge1[0] - DeltaV1 * edge2[0]);
			Tangent[1] = f * (DeltaV2 * edge1[1] - DeltaV1 * edge2[1]);
			Tangent[2] = f * (DeltaV2 * edge1[2] - DeltaV1 * edge2[2]);

			tree1_gpDataArray[i].pTangent[v1 * 3 + 0] += Tangent[0];
			tree1_gpDataArray[i].pTangent[v1 * 3 + 1] += Tangent[1];
			tree1_gpDataArray[i].pTangent[v1 * 3 + 2] += Tangent[2];
			tree1_gpDataArray[i].pTangent[v2 * 3 + 0] += Tangent[0];
			tree1_gpDataArray[i].pTangent[v2 * 3 + 1] += Tangent[1];
			tree1_gpDataArray[i].pTangent[v2 * 3 + 2] += Tangent[2];
			tree1_gpDataArray[i].pTangent[v3 * 3 + 0] += Tangent[0];
			tree1_gpDataArray[i].pTangent[v3 * 3 + 1] += Tangent[1];
			tree1_gpDataArray[i].pTangent[v3 * 3 + 2] += Tangent[2];

			float biTangent[3];
			biTangent[0] = f * (-DeltaU2 * edge1[0] + DeltaU1 * edge2[0]);
			biTangent[1] = f * (-DeltaU2 * edge1[1] + DeltaU1 * edge2[1]);
			biTangent[2] = f * (-DeltaU2 * edge1[2] + DeltaU1 * edge2[2]);

			tree1_gpDataArray[i].pBitangent[v1 * 3 + 0] += biTangent[0];
			tree1_gpDataArray[i].pBitangent[v1 * 3 + 1] += biTangent[1];
			tree1_gpDataArray[i].pBitangent[v1 * 3 + 2] += biTangent[2];
			tree1_gpDataArray[i].pBitangent[v2 * 3 + 0] += biTangent[0];
			tree1_gpDataArray[i].pBitangent[v2 * 3 + 1] += biTangent[1];
			tree1_gpDataArray[i].pBitangent[v2 * 3 + 2] += biTangent[2];
			tree1_gpDataArray[i].pBitangent[v3 * 3 + 0] += biTangent[0];
			tree1_gpDataArray[i].pBitangent[v3 * 3 + 1] += biTangent[1];
			tree1_gpDataArray[i].pBitangent[v3 * 3 + 2] += biTangent[2];
		}
	}
	*/

	fclose(tree1_gpFileObj);

	return 0;
}

int Tree1_Initialize(void)
{
	Tree1_LoadModel();

	pep_Tree1_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCode =
		"#version 450 core"
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"in vec2 vTexCoord;" \
		"in vec3 vTangent;" \
		"in vec3 vBitangent;" \

		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform vec4 u_light_position;" \

		"out vec2 out_texcoord;" \
		"out vec3 light_direction;" \

		//"out vec3 tranformation_matrix;" \

		"out vec3 viewer_vector;" \


		"void main(void)" \
		"{" \

			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \

			"mat3 normal_matrix = transpose(inverse(mat3(u_model_matrix)));" \
			"vec3 vertexNormal = normalize(normal_matrix * vNormal);"
			"vec3 vertexTangent = normalize(normal_matrix * vTangent);"
			"vertexTangent = normalize(vertexTangent - dot(vertexTangent, vertexNormal) * vertexNormal);" \
			"vec3 vertexBitangent = cross(vertexNormal, vertexTangent);"
			"mat3 TBN = transpose(mat3(vertexTangent, vertexBitangent, vertexNormal));"

			"light_direction = TBN * vec3(u_light_position - eye_coordinates);" \
			"viewer_vector = TBN * vec3(-eye_coordinates);" \

			"out_texcoord = vTexCoord;" \

			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(pep_Tree1_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_Tree1_gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Tree1_gVertexShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_Tree1_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_Tree1_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}
	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
	pep_Tree1_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
		"#version 450 core"
		"\n" \
		"in vec2 out_texcoord;" \
		"in vec3 light_direction;" \
		"in vec3 tranformation_matrix;" \
		"in vec3 viewer_vector;" \

		"uniform vec3 u_light_ambient;" \
		"uniform vec3 u_light_diffuse;" \
		"uniform vec3 u_light_specular;" \

		"uniform vec3 u_material_ambient;" \
		"uniform vec3 u_material_diffuse;" \
		"uniform vec3 u_material_specular;" \
		"uniform float u_material_shiness;" \

		"uniform sampler2D u_sampler_diffuse;" \
		"uniform sampler2D u_sampler_normal;" \
		"uniform sampler2D u_sampler_ao;" \

		"out vec4 FragColor;" \
		"vec3 phong_ads_light;" \

		"void main(void)" \
		"{" \
			"vec4 Diffuse = texture(u_sampler_diffuse, out_texcoord);" \
			"vec3 TextureNormal = texture(u_sampler_normal, out_texcoord).xyz;" \
			"float Ambient = texture(u_sampler_ao, out_texcoord * 10.0).r;" \

			// transform normal vector to range [-1,1]
			"TextureNormal = normalize(TextureNormal * 2.0 - 1.0);" \

			"vec3 light_direction_normalize = normalize(light_direction);" \

			"vec3 tranformation_matrix_normalize = TextureNormal;" \

			"vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \

			"vec3 viewer_vector_normal = normalize(viewer_vector);" \

			"float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \

			"vec3 diffuse = u_light_diffuse * Diffuse.rgb * t_normal_dot_light_direction;" \

			"vec3 specular = u_light_specular * Diffuse.rgb * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), 0.25f * 128.0f);" \

			"vec3 ambient = vec3(0.03) * Ambient;"

			"phong_ads_light= ambient + diffuse + specular;" \

			"float gamma = 2.2;" \
			"phong_ads_light = pow(phong_ads_light.rgb, vec3(1.0/gamma));" \

			"FragColor = vec4(phong_ads_light, 1.0);" \
		"}";

	glShaderSource(pep_Tree1_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_Tree1_gFragmentShaderObject);

	glGetShaderiv(pep_Tree1_gFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_Tree1_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_Tree1_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}
	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
		"sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

	pep_Tree1_gShaderProgramObject = glCreateProgram();
	glAttachShader(pep_Tree1_gShaderProgramObject, pep_Tree1_gVertexShaderObject);
	glAttachShader(pep_Tree1_gShaderProgramObject, pep_Tree1_gFragmentShaderObject);

	glBindAttribLocation(pep_Tree1_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glBindAttribLocation(pep_Tree1_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
	glBindAttribLocation(pep_Tree1_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");
	glBindAttribLocation(pep_Tree1_gShaderProgramObject, AMC_ATTRIBUTES_TANGENT, "vTangent");
	glBindAttribLocation(pep_Tree1_gShaderProgramObject, AMC_ATTRIBUTES_BITANGENT, "vBitangent");

	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
	glLinkProgram(pep_Tree1_gShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_Tree1_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus)
	{
		glGetProgramiv(pep_Tree1_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_Tree1_gShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

				free(szInfoLog);
				return -1;
			}
		}
	}

	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

	pep_Tree1_modelUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_model_matrix");
	pep_Tree1_viewUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_view_matrix");
	pep_Tree1_projectionUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_projection_matrix");

	pep_Tree1_SamplerUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_sampler_diffuse");
	pep_Tree1_SamplerNormalUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_sampler_normal");
	pep_Tree1_SamplerAoUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_sampler_ao");

	pep_Tree1_lightAmbientUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_light_ambient");
	pep_Tree1_lightDiffuseUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_light_diffuse");
	pep_Tree1_lightSpecularUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_light_specular");
	pep_Tree1_lightPositionUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_light_position");

	pep_Tree1_materialAmbientUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_material_ambient");
	pep_Tree1_materialDiffuseUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_material_diffuse");
	pep_Tree1_materialSpecularUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_material_specular");
	pep_Tree1_materialPositionUniform = glGetUniformLocation(pep_Tree1_gShaderProgramObject, "u_material_shiness");

	pep_Tree1_pVao = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVao, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVao);

	pep_Tree1_pVbo_position = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVbo_position, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVbo_position);

	pep_Tree1_pVbo_normal = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVbo_normal, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVbo_normal);

	pep_Tree1_pVbo_texcoord = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVbo_texcoord, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVbo_texcoord);

	/*pep_Tree1_pVbo_indices = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVbo_indices, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVbo_indices);*/

	pep_Tree1_pVbo_tangent = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVbo_tangent, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVbo_tangent);

	pep_Tree1_pVbo_bitangent = (GLuint *)malloc(sizeof(GLuint) * tree1_gObjectCount);
	memset(pep_Tree1_pVbo_bitangent, 0, sizeof(GLuint) * tree1_gObjectCount);
	glGenVertexArrays(tree1_gObjectCount, pep_Tree1_pVbo_bitangent);

	for (int objectIndex = 0; objectIndex < tree1_gObjectCount; objectIndex++)
	{
		glGenVertexArrays(1, &pep_Tree1_pVao[objectIndex]);
		glBindVertexArray(pep_Tree1_pVao[objectIndex]);

		glGenBuffers(1, &pep_Tree1_pVbo_position[objectIndex]);
		glBindBuffer(GL_ARRAY_BUFFER, pep_Tree1_pVbo_position[objectIndex]);
		glBufferData(GL_ARRAY_BUFFER,  sizeof(float) * tree1_gpDataArray[objectIndex].data_count * TREE1_MODEL_VERTEX_IN_3D_PLANE * TREE1_MODEL_FACE_TUPLE, tree1_gpDataArray[objectIndex].pVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &pep_Tree1_pVbo_normal[objectIndex]);
		glBindBuffer(GL_ARRAY_BUFFER, pep_Tree1_pVbo_normal[objectIndex]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * tree1_gpDataArray[objectIndex].data_count * TREE1_MODEL_NORMAL_IN_3D_PLANE * TREE1_MODEL_FACE_TUPLE, tree1_gpDataArray[objectIndex].pNormals, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &pep_Tree1_pVbo_texcoord[objectIndex]);
		glBindBuffer(GL_ARRAY_BUFFER, pep_Tree1_pVbo_texcoord[objectIndex]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * tree1_gpDataArray[objectIndex].data_count * TREE1_MODEL_TEXCOORD * TREE1_MODEL_FACE_TUPLE, tree1_gpDataArray[objectIndex].pTexcoords, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &pep_Tree1_pVbo_tangent[objectIndex]);
		glBindBuffer(GL_ARRAY_BUFFER, pep_Tree1_pVbo_tangent[objectIndex]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * tree1_gpDataArray[objectIndex].data_count * TREE1_MODEL_VERTEX_IN_3D_PLANE * TREE1_MODEL_FACE_TUPLE, tree1_gpDataArray[objectIndex].pTangent, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_TANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_TANGENT);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &pep_Tree1_pVbo_bitangent[objectIndex]);
		glBindBuffer(GL_ARRAY_BUFFER, pep_Tree1_pVbo_bitangent[objectIndex]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * tree1_gpDataArray[objectIndex].data_count * TREE1_MODEL_VERTEX_IN_3D_PLANE * TREE1_MODEL_FACE_TUPLE, tree1_gpDataArray[objectIndex].pBitangent, GL_STATIC_DRAW);
		glVertexAttribPointer(AMC_ATTRIBUTES_BITANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_BITANGENT);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	}

	Tree1_LoadTextureSTBDiffuse(&pep_Tree1_Trunk_Texture_JPG, ".\\Tree1\\bark_brown_02_diff.png");
	Tree1_LoadTextureSTBNormal(&pep_Tree1_Trunk_Texture_Normal_JPG, ".\\Tree1\\bark_brown_02_nor.png");
	Tree1_LoadTextureSTBAo2(&pep_Tree1_Trunk_Texture_Ao_JPG, ".\\Tree1\\bark_brown_02_ao.png"/*".\\Tree1\\tree1_trunk_ao_1k.jpg"*/);

	Tree1_LoadTextureSTBDiffuse(&pep_Tree1_Leaves_Texture_JPG, ".\\Tree1\\Leafs_diffuse.png");
	Tree1_LoadTextureSTBNormal(&pep_Tree1_Leaves_Texture_Normal_JPG, ".\\Tree1\\Leafs_normal.png");
	Tree1_LoadTextureSTBAo(&pep_Tree1_Leaves_Texture_Ao_JPG, ".\\Tree1\\Leafs_ao.png");

	return 0;
}

void Tree1_Display(void)
{
	cameraMatrix		= camera.GetViewMatrix();
	//camera.Position	+= camera.Front * 0.5f;
	cameraPosition		= camera.GetCameraPosition();
	cameraFront			= camera.GetCameraFront();

	glClearDepth(1.0f);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE_MODE);
	glDepthFunc(GL_LEQUAL);

	float materialAmbient_0[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	float materialDiffuse_0[4] = {0.185098f, 0.367059f, 0.040784f, 1.0f};
	float materialSpecular_0[4] = {0.185098f, 0.367059f, 0.040784f, 1.0f};
	float materialShiness_0 = 19.607843f;  // Also Try 128.0f

	float materialAmbient_1[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	float materialDiffuse_1[4] = {0.335686f, 0.150588f, 0.018824f, 1.0f};
	float materialSpecular_1[4] = {0.335686f, 0.150588f, 0.018824f, 1.0f};
	float materialShiness_1 = 19.607843f; // Also Try 128.0f

	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	modelMatrix = translate(-7.48780632f, 37.4315796f, -17.8030224f);
	viewMatrix = cameraMatrix;

	glUseProgram(pep_Tree1_gShaderProgramObject);

	glUniformMatrix4fv(pep_Tree1_modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_Tree1_viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_Tree1_projectionUniform, 1, GL_FALSE, pep_Perspective_ProjectionMatrix);

	glUniform3fv(pep_Tree1_lightAmbientUniform, 1, pep_Global_lightAmbient);
	glUniform3fv(pep_Tree1_lightDiffuseUniform, 1, pep_Global_lightDiffuse);
	glUniform3fv(pep_Tree1_lightSpecularUniform, 1, pep_Global_lightSpecular);
	glUniform4fv(pep_Tree1_lightPositionUniform, 1, pep_Global_lightPosition);

	for (int objectIndex = 0; objectIndex < tree1_gObjectCount; objectIndex++)
	{
		if (1 == objectIndex)
		{
			glUniform3fv(pep_Tree1_materialAmbientUniform, 1, materialAmbient_1);
			glUniform3fv(pep_Tree1_materialDiffuseUniform, 1, materialDiffuse_1);
			glUniform3fv(pep_Tree1_materialSpecularUniform, 1, materialSpecular_1);
			glUniform1f(pep_Tree1_materialPositionUniform, materialShiness_1);

			glActiveTexture(GL_TEXTURE0);
			glUniform1i(pep_Tree1_SamplerUniform, 0);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Trunk_Texture_JPG);

			glActiveTexture(GL_TEXTURE1);
			glUniform1i(pep_Tree1_SamplerNormalUniform, 1);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Trunk_Texture_Normal_JPG);

			glActiveTexture(GL_TEXTURE2);
			glUniform1i(pep_Tree1_SamplerAoUniform, 2);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Trunk_Texture_Ao_JPG);

			/*glActiveTexture(GL_TEXTURE1);
			glUniform1i(pep_Tree1_SamplerNormalUniform, 1);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Trunk_Texture_Normal_JPG);*/

		
		}
		else
		{
			glUniform3fv(pep_Tree1_materialAmbientUniform, 1, materialAmbient_0);
			glUniform3fv(pep_Tree1_materialDiffuseUniform, 1, materialDiffuse_0);
			glUniform3fv(pep_Tree1_materialSpecularUniform, 1, materialSpecular_0);
			glUniform1f(pep_Tree1_materialPositionUniform, materialShiness_0);

			glActiveTexture(GL_TEXTURE0);
			glUniform1i(pep_Tree1_SamplerUniform, 0);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Leaves_Texture_JPG);

			glActiveTexture(GL_TEXTURE1);
			glUniform1i(pep_Tree1_SamplerNormalUniform, 1);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Leaves_Texture_Normal_JPG);

			glActiveTexture(GL_TEXTURE2);
			glUniform1i(pep_Tree1_SamplerAoUniform, 2);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Leaves_Texture_Ao_JPG);

			/*glActiveTexture(GL_TEXTURE1);
			glUniform1i(pep_Tree1_SamplerNormalUniform, 1);
			glBindTexture(GL_TEXTURE_2D, pep_Tree1_Leaves_Texture_Normal_JPG);*/

			
		}

		glBindVertexArray(pep_Tree1_pVao[objectIndex]);
		glDrawArrays(GL_TRIANGLES, 0, tree1_gpDataArray[objectIndex].data_count  * TREE1_MODEL_FACE_TUPLE);
		glBindVertexArray(0);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glUseProgram(0);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE_MODE);
}

void Tree1_Update(void)
{

}

void Tree1_ReSize(int width, int height)
{

}

void Tree1_Uninitialize(void)
{

	if (pep_Tree1_pVbo_bitangent) {
		glDeleteBuffers(tree1_gObjectCount, pep_Tree1_pVbo_bitangent);
		pep_Tree1_pVbo_bitangent = NULL;
	}

	if (pep_Tree1_pVbo_tangent) {
		glDeleteBuffers(tree1_gObjectCount, pep_Tree1_pVbo_tangent);
		pep_Tree1_pVbo_tangent = NULL;
	}

	if (pep_Tree1_pVbo_normal) {
		glDeleteBuffers(tree1_gObjectCount, pep_Tree1_pVbo_normal);
		pep_Tree1_pVbo_normal = NULL;
	}

	if (pep_Tree1_pVbo_texcoord) {
		glDeleteBuffers(tree1_gObjectCount, pep_Tree1_pVbo_texcoord);
		pep_Tree1_pVbo_texcoord = NULL;
	}

	if (pep_Tree1_pVbo_position) {
		glDeleteBuffers(tree1_gObjectCount, pep_Tree1_pVbo_position);
		pep_Tree1_pVbo_position = NULL;
	}

	if (pep_Tree1_pVao)
	{
		glDeleteVertexArrays(tree1_gObjectCount, pep_Tree1_pVao);
		pep_Tree1_pVao = NULL;
	}

	free(tree1_gpDataArray->pNormals);
	free(tree1_gpDataArray->pTexcoords);
	free(tree1_gpDataArray->pVertices);
	free(tree1_gpDataArray);
}

BOOL Tree1_LoadTexture(GLuint* texture, CHAR imageResourceID[])
{
	return true;
}

BOOL Tree1_LoadTextureSTBDiffuse(GLuint* texture, const char* filename)
{
	int width, height, nrChannels;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(pep_gpFile, "Failed To Load Texture");
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

BOOL Tree1_LoadTextureSTBNormal(GLuint* texture, const char* filename)
{
	int width, height, nrChannels;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(pep_gpFile, "Failed To Load Texture");
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

BOOL Tree1_LoadTextureSTBAo(GLuint* texture, const char* filename)
{
	int width, height, nrChannels;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(pep_gpFile, "Failed To Load Texture");
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

BOOL Tree1_LoadTextureSTBAo2(GLuint* texture, const char* filename)
{
	int width, height, nrChannels;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R, width, height, 0, GL_R, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(pep_gpFile, "Failed To Load Texture");
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}