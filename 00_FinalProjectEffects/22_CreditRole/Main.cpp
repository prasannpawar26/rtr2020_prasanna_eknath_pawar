﻿#include "Common.h"

#include "Camera.h"
#include "Credits\Shadow.h"
#include "Credits\Letters.h"

#include "SceneTransition.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "Winmm.lib")

bool gbTsSetCameraPosition = false;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

float lightPosition[4] = {64.0f, 50.0f, 64.0f/*-0.699999332f, 5.88960028f, 14.0132608f*/, 1.00000000f}/*{0.0f, 3.589553f, -2.286757f, 1.000000f}*/;
//0x00007ff7ef845698 {12.2000113, 5.88960028, 14.0132608, 1.00000000}
HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND gHwnd = NULL;
DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};
mat4 pep_perspectiveProjectionMatrix;
bool pep_bFadeOut;
bool gbIsFullScreen;
int pep_gWidth = 800;
int pep_gHeight = 600;
bool pep_CubeMarching_wireframe = true;

int pep_Scene = 0;

extern float gFontColor[3];

mat4 pep_Perspective_ProjectionMatrix;

Camera camera(vec3(64.0f, 50.0f, 64.0f)); // Camera Initial Position
mat4 cameraMatrix;
vec3 cameraPosition(vec3(64.0f, 0.0f, 64.0f));
vec3 cameraFront(vec3(0.0f, 0.0f, 0.0f));

float pep_Global_lightPosition[4] = {64.0f, 50.0f, 64.0f, 1.0f/*-128.0f, 50.0f, -128.0f, 1.0f*/};
//float pep_Global_lightPosition[4] = {0.0f, 10.0f, 100.0f, 1.0f/*-128.0f, 50.0f, -128.0f, 1.0f*/};
float pep_Global_lightAmbient[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float pep_Global_lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Global_lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};

float pep_Global_WorldReferenceposition[3] = {/*-128.0f*/0.0f, 0.0f, /*-128.0f*/0.0f};

// Animation
float gfTsBird1_x, gfTsBird1_y, gfTsBird1_z = 0.0f;
float gfTsBird2_x, gfTsBird2_y, gfTsBird2_z = 25.0f;
float gfTsBird3_x, gfTsBird3_y, gfTsBird3_z = 50.0f;

bool gbTsPep_Scene = true;

DWORD  (WINAPI *pThreadFunction[31]) (LPVOID lpParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow)
{
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);
	void ToggledFullScreen(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("MatrixGroup");

	// code
	if (0 != fopen_s(&pep_gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("MatrixGroup"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, pep_gWidth, pep_gHeight, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//if (!PlaySoundW(L"Music2.wav", NULL,
	//	SND_NODEFAULT | SND_ASYNC | SND_FILENAME/* | SND_LOOP*/)) {
	//	fprintf(pep_gpFile, "PlaySound FAILED\n");
	//}

	iRet = Initialize();
	if (0 != iRet)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	if (!PlaySoundW(L"Music2.wav", NULL,
		SND_NODEFAULT | SND_ASYNC | SND_FILENAME/* | SND_LOOP*/)) {
		fprintf(pep_gpFile, "PlaySound FAILED\n");
	}

	ToggledFullScreen();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// Variable Declarations
	POINT pt;

	static int old_x_pos;
	static int old_y_pos;

	static int new_x_pos;
	static int new_y_pos;

	static int x_offset;
	static int y_offset;

	// code
	switch (iMsg)
	{
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
		case WM_ERASEBKGND:
			return 0;
			break;
		case WM_MOUSEMOVE:
			new_x_pos = GET_X_LPARAM(lParam);
			new_y_pos = GET_Y_LPARAM(lParam);


			x_offset = new_x_pos - old_x_pos;
			y_offset = new_y_pos - old_y_pos;


			if (new_x_pos == 0 && old_x_pos < 10)
			{
				SetCursorPos(1920, new_y_pos);
				new_x_pos = 1920;
			}
			if (new_x_pos == 1919 && old_x_pos > 1900)
			{
				SetCursorPos(0, new_y_pos);
				new_x_pos = 0;
			}

			old_x_pos = new_x_pos;
			old_y_pos = new_y_pos;


			camera.ProcessMouseMovement(x_offset, y_offset);

			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_SIZE:
			ReSize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;

			case 'F':
			case 'f':
				ToggledFullScreen();
				break;
			case 'P':
			case 'p':
				pep_Scene++;
				break;
			case VK_UP:
				pep_Global_lightPosition[2] -= 6.0f; //0x00007ff6b5ae8470 {37.0000000, -55.0000000, -422.000000, 1.00000000}  => Sun Rise Value

				break;
			case VK_DOWN:
				pep_Global_lightPosition[2] += 6.0f;
				break;
			case VK_LEFT:
				pep_Global_lightPosition[0] -= 6.0f;
				break;
			case VK_RIGHT:
				pep_Global_lightPosition[0] += 6.0f;
				break;
			case 'T':
			case 't':
				//pep_Global_lightPosition[1] += 6.0f;
				fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Camera Position: vec3(%f, %f %f), Front: vec3(%f, %f, %f) Yaw: %f, Pitch: %f\n",
					__FILE__, __LINE__, __FUNCTION__, camera.Position[0], camera.Position[1], camera.Position[2], camera.Front[0], camera.Front[1], camera.Front[2], camera.Yaw, camera.Pitch);
				(gbTsPep_Scene) ? gbTsPep_Scene = false : gbTsPep_Scene = true;
				break;
			/*case 'G':
			case 'g':
				pep_Global_lightPosition[1] -= 6.0f;
				break;*/
			case 'A':
			case 'a':
				// if (gbIsCameraMovement)
				camera.ProcessKeyboard(LEFT);
				break;

			case 'W':
			case 'w':
				//if (gbIsCameraMovement)
				camera.ProcessKeyboard(FORWARD);
				break;

			case 'D':
			case 'd':
				//if (gbIsCameraMovement)
				camera.ProcessKeyboard(RIGHT);
				break;

			case 'S':
			case 's':
				//if (gbIsCameraMovement)
				camera.ProcessKeyboard(BACKWARD);
				break;
			}
		} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void Credit_Display()
{
	Shadow_Display();
}

void Display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//pep_Scene = 2;

	if (0 == pep_Scene)
	{
		Credit_Display(); // AstroMediComp
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.0027f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.0027f); // 1 to 0
		}
	}
	else if (1 == pep_Scene)
	{
		Credit_Display(); //Matrix Group
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.0027f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.0027f); // 1 to 0
		}
	}
	else if (2 == pep_Scene)
	{
		Credit_Display(); // Project Title
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.0027f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.0027f); // 1 to 0
		}
	}
	else if (3 == pep_Scene)
	{
		//City
		if (!gbTsSetCameraPosition)
		{
			// Set Camera Position
			//camera.SetCameraPosition(vec3(300.557739f, 700.000000f, 948.881348f));
			camera.SetCameraPosition(vec3(226.494247f, 229.359787f, 1328.895264f));
			//camera.SetCameraFront(vec3(0.091334f, -0.147810f, -0.984790f));
			camera.SetCameraFront(vec3(0.335344f, 0.026177f, -0.941732f));
			camera.Yaw = -804.701294f;
			camera.Pitch = -8.500031f;
			gbTsSetCameraPosition = true;
		}
		//City_Display(); //first bird dead
	
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (4 == pep_Scene)
	{
		if (gbTsSetCameraPosition)
		{
			camera.SetCameraPosition(vec3(64.000000f, 50.000000f, 64.000000f));
			camera.SetCameraFront(vec3(0.831656f, 0.371368f, 0.412837f));
			camera.Yaw = -300.699829f;
			camera.Pitch = 18.199997f;			
			gbTsSetCameraPosition = false;
		}

		//City
		 //second bird dead
		
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (5 == pep_Scene)
	{
		//City
		//village viewport -> bird image the village
		
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (6 == pep_Scene)
	{
		//Village
		if (!gbTsSetCameraPosition)
		{
			camera.SetCameraPosition(vec3(13.537865f, 47.400600f, 224.404388f));
			//camera.ProcessKeyboard(FORWARD);
			camera.SetCameraFront(vec3(0.985917f, 0.161603f, -0.043045f));
			camera.Yaw = -1025.800781f;
			camera.Pitch = 1.899969f;
			gbTsSetCameraPosition = true;
		}
		//Village_Display();
		
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
		//static bool pep_scene_part1 = false;
		//static bool pep_scene_part2 = false;
		//static bool pep_scene_part3 = false;
		//static bool pep_scene_part4 = false;
		//static bool pep_scene_part5 = false;

		//Shadow_Display();

		//if (pep_bFadeOut)
		//{
		//	if (!pep_scene_part1)
		//	{
		//		if (lightPosition[0] < 12.2000113f)
		//		{
		//			lightPosition[0] += 0.053;
		//		}
		//		else
		//		{
		//			pep_scene_part1 = true;
		//		}
		//	}
		//	else if (!pep_scene_part2)
		//	{
		//		if (lightPosition[2] > -7.68675232f)
		//		{
		//			lightPosition[2] -= 0.053;
		//		}
		//		else
		//		{
		//			pep_scene_part2 = true;
		//		}
		//	}
		//	else if (!pep_scene_part3)
		//	{
		//		if (lightPosition[0] > -8.79991913f)
		//		{
		//			lightPosition[0] -= 0.053;
		//		}
		//		else
		//		{
		//			pep_scene_part3 = true;
		//		}
		//	}
		//	else if (!pep_scene_part4)
		//	{
		//		if (lightPosition[2] < 13.5131483f)
		//		{
		//			lightPosition[2] += 0.053;
		//		}
		//		else
		//		{
		//			pep_scene_part4 = true;
		//		}
		//	}
		//	else if (!pep_scene_part5)
		//	{
		//		if (lightPosition[0] < -0.699999332f)
		//		{
		//			lightPosition[0] += 0.053;
		//		}
		//		else
		//		{
		//			pep_scene_part5 = true;
		//		}
		//	}

		//	if (pep_scene_part5)
		//	{
		//		SceneTransition_Display(true, 0.004f); // 0 to 1 => 
		//	}
		//}
		//else
		//{
		//	SceneTransition_Display(false, -0.004f); // 1 to 0
		//}
	}
	else if (7 == pep_Scene)
	{
		if (gbTsSetCameraPosition)
		{
			// Set Camera Position
			camera.SetCameraPosition(vec3(877.491394f, 120.472740f, 900.019653f));
			camera.SetCameraFront(vec3(-0.827775f, 0.026176f, -0.560449f));
			camera.Yaw = -1195.900513f;
			camera.Pitch = -0.000032f;
			gbTsSetCameraPosition = false;
		}

		//Village

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (8 == pep_Scene)
	{
		//City = > Poem after this scene
		//City_Display();

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (9 == pep_Scene)
	{
		Credit_Display(); // Effects

		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (10 == pep_Scene)
	{
		Credit_Display(); // Effects
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (11 == pep_Scene)
	{
		Credit_Display(); // Concept Used
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (12 == pep_Scene)
	{
		Credit_Display(); // References
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (13 == pep_Scene)
	{
		Credit_Display();
		// References
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (14 == pep_Scene)
	{
		Credit_Display(); // Technolgy Used
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (15 == pep_Scene)
	{
		Credit_Display(); // Tools Used
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (16 == pep_Scene)
	{
		Credit_Display(); // Music
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (17 == pep_Scene)
	{
		Credit_Display(); // Music
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (18 == pep_Scene)
	{
		Credit_Display(); // Music Edit By Kajal
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (19 == pep_Scene)
	{
		Credit_Display(); // Group Members
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (20 == pep_Scene)
	{
		// Group Leaders
		Credit_Display();
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (21 == pep_Scene)
	{
	// Special Thanks To RTR3 Group Leaders
		Credit_Display();
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}
	else if (22 == pep_Scene)
	{
		// Thanks You
		Credit_Display();
		if (pep_bFadeOut)
		{
			SceneTransition_Display(true, 0.004f); // 0 to 1
		}
		else
		{
			SceneTransition_Display(false, -0.004f); // 1 to 0
		}
	}

	SwapBuffers(gHdc);

	return;
}

void Credit_ReSize(int width, int height)
{
	Shadow_Resize(width, height);
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}
	pep_gWidth = width;
	pep_gHeight = height;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	pep_Perspective_ProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 5000.0f);

	SceneTransition_ReSize(width, height);

	Credit_ReSize(width, height);
	return;
}

void Credit_Uninitialize()
{
	Shadow_Uninitialize();
	UpperLetters_Uninitialize();
}

void Uninitialize(void)
{
	// code

	Credit_Uninitialize();

	SceneTransition_Uninitialize();

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}

	return;
}

void Scene_Update()
{
	if (pep_Scene == 3)
	{
		// Camera Position
		if (camera.Position[0] <= 542.587280f)
		{
			camera.Position[0] += 1.0f;
		}
		else
		{
			camera.Position[0];
		}
		if (camera.Position[1] >= 167.903183f)
		{
			camera.Position[1] -= 1.0f;
		}
		if (camera.Position[2] >= 188.175735f)
		{
			camera.Position[2] -= 1.0f;
		}
		else
		{
			camera.Position[0];
		}

		// Camera Front 0.029597, 0.055821, -0.998002 
		if (camera.Front[0] >= -0.324211f)
		{
			camera.Front[0] -= 0.01f;
		}

		if (camera.Front[1] <= 0.219846f)
		{
			camera.Front[1] += 0.01f;
		}

		if (camera.Front[2] <= -0.920084f)
		{
			camera.Front[2] += 0.01f;
		}

		camera.updateCameraVectors();

		//Birds_Update();

	}
	else if (pep_Scene == 4)
	{
		// Camera Position
		if (camera.Position[0] <= 461.746094f)
		{
			camera.Position[0] += 1.0f;
		}
		if (camera.Position[1] <= 150.503906f)
		{
			camera.Position[1] += 1.0f;
		}
		if (camera.Position[2] <= 79.606415f)
		{
			camera.Position[2] += 1.0f;
		}

		// Camera Front 0.029597, 0.055821, -0.998002 
		if (camera.Front[0] >= -0.033910f)
		{
			camera.Front[0] -= 0.01f;
		}

		/*if (camera.Front[1] <= 0.219846f)
		camera.Front[1] += 0.01f;*/

		if (camera.Front[2] <= 0.970957f)
		{
			camera.Front[2] += 0.01f;
		}

		camera.updateCameraVectors();

		//Birds_Update();		
	}
	else if (pep_Scene == 6)
	{
		// Camera Position
		if (camera.Position[0] <= 724.107239f)
		{
			camera.Position[0] += 1.0f;
		}
		if (camera.Position[1] <= 38.687447f)
		{
			camera.Position[1] += 1.0f;
		}
		if (camera.Position[2] > 221.512146f)
		{
			camera.Position[2] -= 1.0f;
		}

		// Camera Front 0.029597, 0.055821, -0.998002 
		if (camera.Front[0] >= -0.359764f)
		{
			camera.Front[0] -= 0.01f;
		}

		/*if (camera.Front[1] <= 0.219846f)
		camera.Front[1] += 0.01f;*/

		if (camera.Front[2] >= -0.932454f)
		{
			camera.Front[2] -= 0.01f;
		}

		camera.updateCameraVectors();

		//Birds_Update();		
	}
	else if (pep_Scene == 7)
	{
		// Camera Position
		if (camera.Position[0] >= 468.845306f)
		{
			camera.Position[0] -= 1.0f;
		}
		if (camera.Position[1] >= 48.188179f)
		{
			camera.Position[1] -= 1.0f;
		}
		if (camera.Position[2] > 630.814880f)
		{
			camera.Position[2] -= 1.0f;
		}

		// Camera Front 0.029597, 0.055821, -0.998002 
		if (camera.Front[0] >= -0.999879f)
		{
			camera.Front[0] -= 0.01f;
		}
		if (camera.Front[1] <= 0.013962f)
		{
			camera.Front[1] += 0.001f;
		}

		if (camera.Front[2] >= 0.006932f)
		{
			camera.Front[2] -= 0.01f;
		}

		camera.updateCameraVectors();

		//Birds_Update();
	}
}

void Update(void)
{
	camera.updateCameraVectors();
#ifndef DEBUG
#else
	Terrain_Update();
	Cloud_Update();
	WoodenHouse_Update();
	LowPolyHouse_Update();
	Medieval1House_Update();
	CottageHouse_Update();
#endif
	//Ocean_Update();
	SceneTransition_Update();
	
	Scene_Update();
}

void Credit_Initialize()
{
	UpperLetters_Initialize();
	Shadow_Initialize(pep_gWidth, pep_gHeight);
}

int Initialize(void)
{
	// variable declarations
	int pep_CubeMarching_index;
	PIXELFORMATDESCRIPTOR pep_CubeMarching_pfd;

	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pep_CubeMarching_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_CubeMarching_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_CubeMarching_pfd.nVersion = 1;
	pep_CubeMarching_pfd.cColorBits = 32;
	pep_CubeMarching_pfd.cDepthBits = 8;
	pep_CubeMarching_pfd.cRedBits = 8;
	pep_CubeMarching_pfd.cGreenBits = 8;
	pep_CubeMarching_pfd.cBlueBits = 8;
	pep_CubeMarching_pfd.cAlphaBits = 8;
	pep_CubeMarching_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	pep_CubeMarching_index = ChoosePixelFormat(gHdc, &pep_CubeMarching_pfd);
	if (0 == pep_CubeMarching_index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, pep_CubeMarching_index, &pep_CubeMarching_pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum pep_CubeMarching_result;
	pep_CubeMarching_result = glewInit();
	if (GLEW_OK != pep_CubeMarching_result)
	{
		return -5;
	}	
	
	Credit_Initialize();

	SceneTransition_Initialize();

	//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearColor(0.5294f, 0.8078f, 0.921f, 1.0f); //skyblue

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	pep_perspectiveProjectionMatrix = mat4::identity();

	ReSize(pep_gWidth, pep_gHeight);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}
