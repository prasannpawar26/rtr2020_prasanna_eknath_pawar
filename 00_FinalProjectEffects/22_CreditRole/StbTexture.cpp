#include "Common.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "StbTexture.h"

extern FILE *pep_gpFile;

BOOL LoadTextureSTB(GLuint* texture, const char* filename)
{
	int width, height, nrChannels;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, texture);
	glBindTexture(GL_TEXTURE_2D, *texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
	if (data)
	{
		GLint internalformat = GL_RGBA;
		GLenum format = GL_RGBA;
		if (1 == nrChannels)
		{
			internalformat = GL_R;
			format = GL_R;
		}
		else if (2 == nrChannels)
		{
			internalformat = GL_RG;
			format = GL_RG;
		}
		else if (3 == nrChannels)
		{
			internalformat = GL_RGB;
			format = GL_RGB;
		}
		else if (4 == nrChannels)
		{
			internalformat = GL_RGBA;
			format = GL_RGBA;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		fprintf(pep_gpFile, "Failed To Load Texture: %s\n", filename);
		return false;
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}
