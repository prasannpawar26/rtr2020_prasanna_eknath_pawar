#pragma once

#include <GL/glew.h>
#include<gl/GL.h>
#include <stdio.h>
#include<stdlib.h>
#include <windows.h>
#include <windowsx.h>
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys\timeb.h> 
#include "vmath.h"
#include "Camera.h"



using namespace std;
using namespace vmath;

enum {
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0,
	AMC_ATTRIBUTES_TANGENT,
	AMC_ATTRIBUTES_BITANGENT,
	AMC_ATTRIBUTES_INSTANCE0,
	AMC_ATTRIBUTES_INSTANCE1,
	AMC_ATTRIBUTES_INSTANCE2,
	AMC_ATTRIBUTES_INSTANCE3,
};