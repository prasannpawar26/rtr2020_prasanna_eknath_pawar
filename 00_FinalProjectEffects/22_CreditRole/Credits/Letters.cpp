
#include "Camera.h"
#include "Common.h"
#include "Letters.h"

#include "ModelLoaderForLetters.h"

#include "StbTexture.h"

#define UPPER_LETTER_COUNT 29

LETTERS_MODEL_DATA *UpperLetters_gpDataArray[UPPER_LETTER_COUNT];
int UpperLetters_gObjectCount[UPPER_LETTER_COUNT];

GLuint *pep_UpperLetters_pVao[UPPER_LETTER_COUNT];
GLuint *pep_UpperLetters_pVbo_position[UPPER_LETTER_COUNT];
GLuint *pep_UpperLetters_pVbo_normal[UPPER_LETTER_COUNT];
GLuint *pep_UpperLetters_pVbo_texcoord[UPPER_LETTER_COUNT];
GLuint *pep_UpperLetters_pVbo_indices[UPPER_LETTER_COUNT];
GLuint *pep_UpperLetters_pVbo_tangent[UPPER_LETTER_COUNT];
GLuint *pep_UpperLetters_pVbo_bitangent[UPPER_LETTER_COUNT];

int UpperLetters_Initialize(void)
{
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\A\\A.obj", (char *)"o", &UpperLetters_gpDataArray[0], &UpperLetters_gObjectCount[0]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\B\\B.obj", (char *)"o", &UpperLetters_gpDataArray[1], &UpperLetters_gObjectCount[1]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\C\\C.obj", (char *)"o", &UpperLetters_gpDataArray[2], &UpperLetters_gObjectCount[2]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\D\\D.obj", (char *)"o", &UpperLetters_gpDataArray[3], &UpperLetters_gObjectCount[3]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\E\\E.obj", (char *)"o", &UpperLetters_gpDataArray[4], &UpperLetters_gObjectCount[4]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\F\\F.obj", (char *)"o", &UpperLetters_gpDataArray[5], &UpperLetters_gObjectCount[5]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\G\\G.obj", (char *)"o", &UpperLetters_gpDataArray[6], &UpperLetters_gObjectCount[6]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\H\\H.obj", (char *)"o", &UpperLetters_gpDataArray[7], &UpperLetters_gObjectCount[7]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\I\\I.obj", (char *)"o", &UpperLetters_gpDataArray[8], &UpperLetters_gObjectCount[8]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\J\\J.obj", (char *)"o", &UpperLetters_gpDataArray[9], &UpperLetters_gObjectCount[9]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\K\\K.obj", (char *)"o", &UpperLetters_gpDataArray[10], &UpperLetters_gObjectCount[10]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\L\\L.obj", (char *)"o", &UpperLetters_gpDataArray[11], &UpperLetters_gObjectCount[11]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\M\\M.obj", (char *)"o", &UpperLetters_gpDataArray[12], &UpperLetters_gObjectCount[12]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\N\\N.obj", (char *)"o", &UpperLetters_gpDataArray[13], &UpperLetters_gObjectCount[13]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\O\\O.obj", (char *)"o", &UpperLetters_gpDataArray[14], &UpperLetters_gObjectCount[14]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\P\\P.obj", (char *)"o", &UpperLetters_gpDataArray[15], &UpperLetters_gObjectCount[15]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\Q\\Q.obj", (char *)"o", &UpperLetters_gpDataArray[16], &UpperLetters_gObjectCount[16]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\R\\R.obj", (char *)"o", &UpperLetters_gpDataArray[17], &UpperLetters_gObjectCount[17]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\S\\S.obj", (char *)"o", &UpperLetters_gpDataArray[18], &UpperLetters_gObjectCount[18]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\T\\T.obj", (char *)"o", &UpperLetters_gpDataArray[19], &UpperLetters_gObjectCount[19]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\U\\U.obj", (char *)"o", &UpperLetters_gpDataArray[20], &UpperLetters_gObjectCount[20]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\V\\V.obj", (char *)"o", &UpperLetters_gpDataArray[21], &UpperLetters_gObjectCount[21]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\W\\W.obj", (char *)"o", &UpperLetters_gpDataArray[22], &UpperLetters_gObjectCount[22]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\X\\X.obj", (char *)"o", &UpperLetters_gpDataArray[23], &UpperLetters_gObjectCount[23]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\Y\\Y.obj", (char *)"o", &UpperLetters_gpDataArray[24], &UpperLetters_gObjectCount[24]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\Z\\Z.obj", (char *)"o", &UpperLetters_gpDataArray[25], &UpperLetters_gObjectCount[25]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\Colon\\Colon.obj", (char *)"o", &UpperLetters_gpDataArray[26], &UpperLetters_gObjectCount[26]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\Zero\\zero.obj", (char *)"o", &UpperLetters_gpDataArray[27], &UpperLetters_gObjectCount[27]);
	ModelLoaderForLetters((char *)".\\Credits\\UpperCase\\1\\one.obj", (char *)"o", &UpperLetters_gpDataArray[28], &UpperLetters_gObjectCount[28]);

	for (int letterIndex = 0; letterIndex < UPPER_LETTER_COUNT; letterIndex++)
	{
		pep_UpperLetters_pVao[letterIndex] = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		memset(pep_UpperLetters_pVao[letterIndex], 0, sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		glGenVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVao[letterIndex]);

		pep_UpperLetters_pVbo_position[letterIndex] = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		memset(pep_UpperLetters_pVbo_position[letterIndex], 0, sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		glGenVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_position[letterIndex]);

		pep_UpperLetters_pVbo_normal[letterIndex] = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		memset(pep_UpperLetters_pVbo_normal[letterIndex], 0, sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		glGenVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_normal[letterIndex]);

		pep_UpperLetters_pVbo_texcoord[letterIndex] = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		memset(pep_UpperLetters_pVbo_texcoord[letterIndex], 0, sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		glGenVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_texcoord[letterIndex]);

		/*pep_UpperLetters_pVbo_indices = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount);
		memset(pep_UpperLetters_pVbo_indices, 0, sizeof(GLuint) * UpperLetters_gObjectCount);
		glGenVertexArrays(UpperLetters_gObjectCount, pep_UpperLetters_pVbo_indices);*/

		pep_UpperLetters_pVbo_tangent[letterIndex] = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		memset(pep_UpperLetters_pVbo_tangent[letterIndex], 0, sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		glGenVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_tangent[letterIndex]);

		pep_UpperLetters_pVbo_bitangent[letterIndex] = (GLuint *)malloc(sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		memset(pep_UpperLetters_pVbo_bitangent[letterIndex], 0, sizeof(GLuint) * UpperLetters_gObjectCount[letterIndex]);
		glGenVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_bitangent[letterIndex]);


		for (int objectIndex = 0; objectIndex < UpperLetters_gObjectCount[letterIndex]; objectIndex++)
		{
			glGenVertexArrays(1, &pep_UpperLetters_pVao[letterIndex][objectIndex]);
			glBindVertexArray(pep_UpperLetters_pVao[letterIndex][objectIndex]);

			glGenBuffers(1, &pep_UpperLetters_pVbo_position[letterIndex][objectIndex]);
			glBindBuffer(GL_ARRAY_BUFFER, pep_UpperLetters_pVbo_position[letterIndex][objectIndex]);
			glBufferData(GL_ARRAY_BUFFER,  sizeof(float) * UpperLetters_gpDataArray[letterIndex][objectIndex].data_count * LETTERS_MODEL_VERTEX_IN_3D_PLANE * LETTERS_MODEL_FACE_TUPLE, UpperLetters_gpDataArray[letterIndex][objectIndex].pVertices, GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1, &pep_UpperLetters_pVbo_normal[letterIndex][objectIndex]);
			glBindBuffer(GL_ARRAY_BUFFER, pep_UpperLetters_pVbo_normal[letterIndex][objectIndex]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * UpperLetters_gpDataArray[letterIndex][objectIndex].data_count * LETTERS_MODEL_NORMAL_IN_3D_PLANE * LETTERS_MODEL_FACE_TUPLE, UpperLetters_gpDataArray[letterIndex][objectIndex].pNormals, GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1, &pep_UpperLetters_pVbo_texcoord[letterIndex][objectIndex]);
			glBindBuffer(GL_ARRAY_BUFFER, pep_UpperLetters_pVbo_texcoord[letterIndex][objectIndex]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * UpperLetters_gpDataArray[letterIndex][objectIndex].data_count * LETTERS_MODEL_TEXCOORD * LETTERS_MODEL_FACE_TUPLE, UpperLetters_gpDataArray[letterIndex][objectIndex].pTexcoords, GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1, &pep_UpperLetters_pVbo_tangent[letterIndex][objectIndex]);
			glBindBuffer(GL_ARRAY_BUFFER, pep_UpperLetters_pVbo_tangent[letterIndex][objectIndex]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * UpperLetters_gpDataArray[letterIndex][objectIndex].data_count * LETTERS_MODEL_VERTEX_IN_3D_PLANE * LETTERS_MODEL_FACE_TUPLE, UpperLetters_gpDataArray[letterIndex][objectIndex].pTangent, GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTES_TANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTES_TANGENT);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1, &pep_UpperLetters_pVbo_bitangent[letterIndex][objectIndex]);
			glBindBuffer(GL_ARRAY_BUFFER, pep_UpperLetters_pVbo_bitangent[letterIndex][objectIndex]);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * UpperLetters_gpDataArray[letterIndex][objectIndex].data_count * LETTERS_MODEL_VERTEX_IN_3D_PLANE * LETTERS_MODEL_FACE_TUPLE, UpperLetters_gpDataArray[letterIndex][objectIndex].pBitangent, GL_STATIC_DRAW);
			glVertexAttribPointer(AMC_ATTRIBUTES_BITANGENT, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(AMC_ATTRIBUTES_BITANGENT);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glBindVertexArray(0);
		}
	}

	return 0;
}

void UpperLetters_Uninitialize(void)
{

	for (int letterIndex = 0; letterIndex < UPPER_LETTER_COUNT; letterIndex++)
	{
		if (pep_UpperLetters_pVbo_bitangent[letterIndex]) {
			glDeleteBuffers(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_bitangent[letterIndex]);
			pep_UpperLetters_pVbo_bitangent[letterIndex] = NULL;
		}

		if (pep_UpperLetters_pVbo_tangent[letterIndex]) {
			glDeleteBuffers(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_tangent[letterIndex]);
			pep_UpperLetters_pVbo_tangent[letterIndex] = NULL;
		}

		if (pep_UpperLetters_pVbo_normal) {
			glDeleteBuffers(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_normal[letterIndex]);
			pep_UpperLetters_pVbo_normal[letterIndex] = NULL;
		}

		if (pep_UpperLetters_pVbo_texcoord[letterIndex]) {
			glDeleteBuffers(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_texcoord[letterIndex]);
			pep_UpperLetters_pVbo_texcoord[letterIndex] = NULL;
		}

		if (pep_UpperLetters_pVbo_position[letterIndex]) {
			glDeleteBuffers(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVbo_position[letterIndex]);
			pep_UpperLetters_pVbo_position[letterIndex] = NULL;
		}

		if (pep_UpperLetters_pVao[letterIndex])
		{
			glDeleteVertexArrays(UpperLetters_gObjectCount[letterIndex], pep_UpperLetters_pVao[letterIndex]);
			pep_UpperLetters_pVao[letterIndex] = NULL;
		}

		free(UpperLetters_gpDataArray[letterIndex]);
	}
}

void UpperLetters_Display(int LetterIndexToDisplay)
{
	for (int objectIndex = 0; objectIndex < UpperLetters_gObjectCount[LetterIndexToDisplay]; objectIndex++)
	{
		glBindVertexArray(pep_UpperLetters_pVao[LetterIndexToDisplay][objectIndex]);
		glDrawArrays(GL_TRIANGLES, 0, UpperLetters_gpDataArray[LetterIndexToDisplay][objectIndex].data_count  * LETTERS_MODEL_FACE_TUPLE);
		//glDrawArraysInstanced(GL_TRIANGLES, 0, UpperLetters_gpDataArray[LetterIndexToDisplay][objectIndex].data_count * LETTERS_MODEL_FACE_TUPLE, 1/*WOODENLETTERS_INSTANCE_COUNT*/);
		glBindVertexArray(0);
	}
}

