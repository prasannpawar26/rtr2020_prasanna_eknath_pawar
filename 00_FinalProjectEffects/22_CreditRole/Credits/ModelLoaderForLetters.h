#pragma once

#define LETTERS_MODEL_LINE_LENGTH 256
#define LETTERS_MODEL_VERTEX_IN_3D_PLANE 3
#define LETTERS_MODEL_TEXCOORD 2
#define LETTERS_MODEL_NORMAL_IN_3D_PLANE 3
#define LETTERS_MODEL_FACE_TUPLE 3

typedef struct _LETTERS_MODEL_DATA
{
	int data_count;
	float *pVertices;
	float *pTexcoords;
	float *pNormals;
	float *pTangent;
	float *pBitangent;
	char *pMaterialName;
} LETTERS_MODEL_DATA;

int ModelLoaderForLetters(char *, char *, LETTERS_MODEL_DATA **, int *);
