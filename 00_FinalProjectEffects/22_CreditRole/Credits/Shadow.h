#pragma once

int Shadow_Initialize(int width, int height);
void Shadow_Uninitialize();
void Shadow_Update();
void Shadow_Resize(int width, int height);
void Shadow_Display(void);
