#include "..\Common.h"
#include "Shadow.h"
#include "Letters.h"
#include "ModelLoaderForLetters.h"

/*
we render the scene from the light's point of view and everything we see from the light's perspective is lit
and everything we can't see must be in shadow

The first pass requires us to generate a depth map. The depth map is the depth texture as rendered from the light's perspective
that we'll be using for testing for shadows

=====================PASS 1: Light Pass=======================================
Framebuffer ->
	We only need the depth information when rendering the scene from the light's perspective
	so there is no need for a color buffer.

	A framebuffer object however is not complete without a color buffer so we need to explicitly tell OpenGL we're not going to render any color data.
	We do this by setting both the read and draw buffer to GL_NONE with glDrawBuffer and glReadbuffer

Light space transform ->
	Projection Matrix:
	1. all its light rays are parallel -> ortho projection ->used for directional lights
	2. all its light rays are not parallel -> prespective projection -> used spotlights and point lights
	
	View Matrix
	Transform each object so they're visible from the light's point of view. -> use lightposition instead of camera position for lookat

	Combining these two gives us a light space transformation matrix that
	transforms each world-space vector into the space as visible from the light source =>  render the depth map.

Fragment Shader:
	Since we have no color buffer and disabled the draw and read buffers,
	the resulting fragments do not require any processing so we can simply use an empty fragment shader.

	Empty fragment shader does no processing whatsoever, and at the end of its run the depth buffer is updated

=================================PASS 2: =======================================

======
Shadow acne
	Because the shadow map is limited by resolution, multiple fragments can sample the same value from the depth map.

	can solve this issue with a small little hack called a shadow bias where we simply offset the depth of the surface.

Peter panning
	A disadvantage of using a shadow bias is that you're applying an offset to the actual depth of objects.
	This shadow artifact is called peter panning since objects seem slightly detached from their shadows.

	to solve most of the peter panning issue by using front face culling when rendering the depth map => but only for solid

Over sampling

PCF [percentage-closer filtering]
	Because the depth map has a fixed resolution, the depth frequently usually spans more than one fragment per texel.
	As a result, multiple fragments sample the same depth value from the depth map and
	come to the same shadow conclusions, which produces these jagged blocky edges.

	**simple implementation of PCF is to simply sample the surrounding texels of the depth map and average the results**

	By using more samples and/or varying the texelSize variable you can increase the quality of the soft shadows
*/
extern FILE *pep_gpFile;
extern int pep_gWidth;
extern int pep_gHeight;
extern int pep_Scene;

float pep_LetterRotationApply = 271.0f;

GLuint pep_Shadow_VertexShader;
GLuint pep_Shadow_FragmentShader;
GLuint pep_Shadow_ShaderProgram;

mat4 pep_Shadow_ProjectionMatrix;
mat4 pep_Shadow_ViewMatrix;

GLuint pep_Shadow_FrameBuffer;
GLuint pep_Shadow_FrameBuffer_ShadowMapTexture;

GLuint pep_Shadow_UniformModelMatrix;
GLuint pep_Shadow_UniformViewMatrix;
GLuint pep_Shadow_UniformProjectionMatrix;
GLuint pep_Shadow_UniformShadowMatrix;
GLuint pep_Shadow_UniformIsLightPass;
GLuint pep_Shadow_UniformShadowMapTexture;
GLuint pep_Shadow_UniformLightPosition;
GLuint pep_Shadow_UniformLightAmbient;
GLuint pep_Shadow_UniformLightDiffuse;
GLuint pep_Shadow_UniformLightSpecular;
GLuint pep_Shadow_UniformMaterialAmbient;
GLuint pep_Shadow_UniformMaterialDiffuse;
GLuint pep_Shadow_UniformMaterialSpecular;
GLuint pep_Shadow_UniformMaterialShiness;

/*************************Matrix Related Variables****************************/
mat4 pep_Shadow_LightViewMatrix;
mat4 pep_Shadow_LightProjectionMatrix;
mat4 pep_Shadow_LightBiasMatrix;
mat4 pep_Shadow_LightBiasProjectionMatrix;
mat4 pep_Shadow_LightShadowMatrix;

GLuint pep_Shadow_GroundVao;
GLuint pep_Shadow_GroundPositionVbo;
GLuint pep_Shadow_GroundNormalVbo;

float pep_Shadow_lightPosition[4] = {-0.124088f, 3.589553f, 5.786757f, 1.000000f/*-0.624088f, 3.589553f, 1.286757f, 1.000000f*//*-2.624089f, 3.589553f, 2.286757f, 1.000000f*/};
//float pep_Shadow_lightPosition[4] = {2.624089f, 3.589553f, -2.286757f, 1.000000f};
//Light Position: vec4(-2.124088, 3.589553 5.786757 1.000000)

float pep_Shadow_lightAmbient[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Shadow_lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Shadow_lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};

//pearl
float pep_Shadow_materialAmbient_pearl[4] = {0.25f, 0.20725f, 0.20725f, 1.0f};
float pep_Shadow_materialDiffuse_pearl[4] = {1.0f, 0.829f, 0.829f, 1.0f};
float pep_Shadow_materialSpecular_pearl[4] = {0.296648f, 0.296648f, 0.296648f, 1.0f};
float pep_Shadow_materialShiness_pearl = 0.088f * 128.0f;  // Also Try 128.0f

//emerald 
float pep_Shadow_materialAmbient_emerald[4] = {0.0215f, 0.1745f, 0.0215f, 1.0f};
float pep_Shadow_materialDiffuse_emerald[4] = {0.07568f, 0.61424f, 0.07568f, 1.0f};
float pep_Shadow_materialSpecular_emerald[4] = {0.633f, 0.727811f, 0.633, 1.0f};
float pep_Shadow_materialShiness_emerald = 0.6f * 128.0f;  // Also Try 128.0f

int Shadow_InitializeFbo(int width, int height)
{

	glGenFramebuffers(1, &pep_Shadow_FrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, pep_Shadow_FrameBuffer);

	glGenTextures(1, &pep_Shadow_FrameBuffer_ShadowMapTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR /*GL_NEAREST*/);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR/*GL_NEAREST*/);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE/*GL_CLAMP_TO_BORDER*/);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE/*GL_CLAMP_TO_BORDER*/);
	// Remove Over sampling
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	GLfloat border[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32/*GL_DEPTH_COMPONENT24*/, width, height, 0,
		GL_DEPTH_COMPONENT, GL_FLOAT/*GL_UNSIGNED_BYTE*/, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n",
			__FILE__, __LINE__, __FUNCTION__);
	} else {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Complete\n",
			__FILE__, __LINE__, __FUNCTION__);
	}
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	return 0;
}

void Shadow_InitializeModels(void)
{
	// Ground
	glGenVertexArrays(1, &pep_Shadow_GroundVao);

	glBindVertexArray(pep_Shadow_GroundVao);

	// ground-vertex
	const GLfloat groundVertices[] = {
		// BOTTOM
		12.0f, -2.0f,  -12.0f,
		-12.0f, -2.0f, -12.0f,
		-12.0f, -2.0f, 12.0f,
		12.0f, -2.0f,  12.0f,

		//Back
		6.0f, 6.0f,  -6.0f,
		-6.0f, 6.0f, -6.0f,
		-6.0f, -1.0f, -6.0f,
		6.0f, -1.0f,  -6.0f
	};

	glGenBuffers(1, &pep_Shadow_GroundPositionVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_GroundPositionVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(groundVertices), groundVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	const GLfloat groundNormals[] = {
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f
	};

	glGenBuffers(1, &pep_Shadow_GroundNormalVbo);
	glBindBuffer(GL_ARRAY_BUFFER, pep_Shadow_GroundNormalVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(groundNormals), groundNormals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return;
}

int Shadow_InitializeShaderProgram(void)
{
	const GLchar *vertexShaderSourceCode =
		"#version 450 core"
		"\n"

		"in vec4 vVertex;"
		"in vec3 vNormal;"

		"uniform mat4 uShadow_ModelMatrix;"
		"uniform mat4 uShadow_ViewMatrix;"
		"uniform mat4 uShadow_ProjectionMatrix;"
		"uniform mat4 uShadow_ShadowMatrix;"
		"uniform vec4 uShadow_LightPosition;"

		"out vec3 tranformation_matrix;"  // tranformation_matrix
		"out vec3 light_direction;"
		"out vec4 eye_coordinates;"
		"out vec4 vShadowCoords;"
		"out vec3 viewer_vector;"

		"void main(void)"
		"{"
			//eye space vertex position
			"vec4 eye_coordinates = uShadow_ViewMatrix * uShadow_ModelMatrix *vVertex;"

			//eye space normal
			"tranformation_matrix   = normalize(mat3(uShadow_ViewMatrix * uShadow_ModelMatrix) * vNormal);"

			//the shadow coordinates
			"vShadowCoords = uShadow_ShadowMatrix * ( uShadow_ModelMatrix * vVertex);"

			"light_direction = vec3(uShadow_LightPosition - eye_coordinates);"

			"viewer_vector = vec3(-eye_coordinates);"

			"gl_Position = uShadow_ProjectionMatrix * uShadow_ViewMatrix * uShadow_ModelMatrix * vVertex;"
		"}";

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core"
		"\n"

		"out vec4 vFragColor;"

		"uniform mat4 uShadow_ModelMatrix;"
		"uniform mat4 uShadow_ViewMatrix;"
		"uniform vec4 uShadow_LightPosition;"
		"uniform sampler2DShadow uShadow_ShadowMapTexture;"
		"uniform int uShadow_IsLightPass;"

		"in vec3 tranformation_matrix;"  // tranformation_matrix
		"in vec3 light_direction;"
		"in vec4 eye_coordinates;"
		"in vec4 vShadowCoords;"
		"in vec3 viewer_vector;"

		"const float k0 = 1.0;"
		"const float k1 = 0.0;"
		"const float k2 = 0.0;"

		"uniform vec3 uShadow_LightAmbient;"
		"uniform vec3 uShadow_LightDiffuse;"
		"uniform vec3 uShadow_LightSpecular;"

		"uniform vec3 uShadow_MaterialAmbient;"
		"uniform vec3 uShadow_MaterialDiffuse;"
		"uniform vec3 uShadow_MaterialSpecular;"
		"uniform float uShadow_MaterialShiness;"

		"vec3 phong_ads_light;"

		"float random(vec4 seed) {"
			"float dot_product = dot(seed, vec4(12.9898,78.233,45.164,94.673));"
			"return fract(sin(dot_product) * 43758.5453);"
		"}"

		"void main()"
		"{"
			"if(1 == uShadow_IsLightPass)"
			"{"
				"return;"
			"}"

			"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);"

			"vec3 viewer_vector_normal = normalize(viewer_vector);"

			"vec3 light_direction_normalize = normalize(light_direction);"
			"float d = length(light_direction);"

			"vec3 reflection_vector = reflect(-light_direction_normalize, "
			"tranformation_matrix_normalize);"

			"float t_normal_dot_light_direction = max(dot(light_direction_normalize, "
			"tranformation_matrix_normalize), 0.0f);"

			//"float attenuationAmount = 1.0/(k0 + (k1*d) + (k2*d*d));"
			"float attenuationAmount = 1.0;"

			"float diffuse_1 = t_normal_dot_light_direction * attenuationAmount;"

			"vec3 ambient = uShadow_LightAmbient * uShadow_MaterialAmbient;"

			"vec3 diffuse = uShadow_LightDiffuse * uShadow_MaterialDiffuse * "
			"t_normal_dot_light_direction * attenuationAmount ;"

			"vec3 specular = uShadow_LightSpecular * uShadow_MaterialSpecular * "
			"pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), "
			"uShadow_MaterialShiness);"

			"float shadow;"
			"if(vShadowCoords.w > 1)"
			"{"
				////=============OPTION 1=======================///////
				/*"float bias = max(0.05 * (1.0 - t_normal_dot_light_direction), 0.005);"
				"shadow = textureProj(uShadow_ShadowMapTexture, vShadowCoords);"
				"diffuse_1 = mix(diffuse_1, diffuse_1*shadow, 0.5); "*/

				////=============OPTION 2=======================///////
				/*"float sum = 0;"
				"shadow = 1;"

				"for(int i=0;i<16;i++) {"
					"float indexA = (random(vec4(gl_FragCoord.xyx, i))*0.25);"
					"float indexB = (random(vec4(gl_FragCoord.yxy, i))*0.25); "
					"sum += textureProj(uShadow_ShadowMapTexture, vShadowCoords+vec4(indexA, indexB, 0, 0));"
				"}"
				"shadow = sum/16.0;"
				"diffuse_1 = mix(diffuse_1, diffuse_1*shadow, 0.5);"*/

				////=============OPTION 3: using 3x3 neighborhood=======================///////
				/*"float sum = 0;"
				"float shadow = 1;"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 0));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 2));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 0,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 0, 0));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 0, 2));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 0));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 2));"
				"shadow = sum/9.0;"
				"diffuse_1 = mix(diffuse_1, diffuse_1*shadow, 0.5);"*/

				////=============OPTION 4: using 4x4 neighborhood=======================///////
				"float sum = 0;"
				"float shadow = 1;"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1,-2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2,-2));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2,-1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1,-1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1,-1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2,-1));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1, 1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1, 1));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 1));"

				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-2, 2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2(-1, 2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 1, 2));"
				"sum += textureProjOffset(uShadow_ShadowMapTexture, vShadowCoords, ivec2( 2, 2));"

				"shadow = sum/16.0;"
				"diffuse_1 = mix(diffuse_1, diffuse_1 * shadow, 0.5);"
			"}"

			"phong_ads_light= ambient + diffuse + specular;"

			"float gamma = 2.2;" \

			"phong_ads_light = pow(phong_ads_light.rgb, vec3(1.0/gamma));" \

			"vFragColor = diffuse_1 * vec4(phong_ads_light, 1);"
		"}";

	pep_Shadow_VertexShader = glCreateShader(GL_VERTEX_SHADER);
	if (0 == pep_Shadow_VertexShader) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -1;
	}

	glShaderSource(pep_Shadow_VertexShader, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_Shadow_VertexShader);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_Shadow_VertexShader, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetShaderiv(pep_Shadow_VertexShader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Shadow_VertexShader, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -1;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	pep_Shadow_FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == pep_Shadow_FragmentShader) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -2;
	}

	glShaderSource(pep_Shadow_FragmentShader, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(pep_Shadow_FragmentShader);

	glGetShaderiv(pep_Shadow_FragmentShader, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader "
			"Compilation Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);
		glGetShaderiv(pep_Shadow_FragmentShader, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(pep_Shadow_FragmentShader, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -3;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	pep_Shadow_ShaderProgram = glCreateProgram();

	glAttachShader(pep_Shadow_ShaderProgram, pep_Shadow_VertexShader);
	glAttachShader(pep_Shadow_ShaderProgram, pep_Shadow_FragmentShader);

	glBindAttribLocation(pep_Shadow_ShaderProgram, AMC_ATTRIBUTES_POSITION, "vVertex");
	glBindAttribLocation(pep_Shadow_ShaderProgram, AMC_ATTRIBUTES_NORMAL, "vNormal");

	glLinkProgram(pep_Shadow_ShaderProgram);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(pep_Shadow_ShaderProgram, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		fprintf(
			pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n",
			__FILE__, __LINE__, __FUNCTION__);

		glGetProgramiv(pep_Shadow_ShaderProgram, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetProgramInfoLog(pep_Shadow_ShaderProgram, iInfoLogLength, &written,
					szInfoLog);

				fprintf(pep_gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
			}
		}

		return -4;
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	// Uniform Binding After Linking
	pep_Shadow_UniformModelMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ModelMatrix");
	pep_Shadow_UniformViewMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ViewMatrix");
	pep_Shadow_UniformProjectionMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ProjectionMatrix");
	pep_Shadow_UniformShadowMatrix = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ShadowMatrix");
	pep_Shadow_UniformLightPosition = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightPosition");
	pep_Shadow_UniformIsLightPass = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_IsLightPass");
	pep_Shadow_UniformShadowMapTexture = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_ShadowMapTexture");

	pep_Shadow_UniformLightAmbient = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightAmbient");
	pep_Shadow_UniformLightDiffuse = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightDiffuse");
	pep_Shadow_UniformLightSpecular = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_LightSpecular");

	pep_Shadow_UniformMaterialAmbient = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialAmbient");
	pep_Shadow_UniformMaterialDiffuse = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialDiffuse");
	pep_Shadow_UniformMaterialSpecular = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialSpecular");
	pep_Shadow_UniformMaterialShiness = glGetUniformLocation(pep_Shadow_ShaderProgram, "uShadow_MaterialShiness");

	return 0;
}

int Shadow_Initialize(int width, int height)
{
	Shadow_InitializeFbo(width, height);

	Shadow_InitializeShaderProgram();

	Shadow_InitializeModels();

	return 0;
}

void Shadow_Uninitialize()
{
	// code
	if (pep_Shadow_FrameBuffer_ShadowMapTexture)
	{
		glDeleteTextures(1, &pep_Shadow_FrameBuffer_ShadowMapTexture);
		pep_Shadow_FrameBuffer_ShadowMapTexture = 0;
	}

	if (pep_Shadow_FrameBuffer)
	{
		glDeleteFramebuffers(1, &pep_Shadow_FrameBuffer);
		pep_Shadow_FrameBuffer = 0;
	}

	if (pep_Shadow_GroundPositionVbo)
	{
		glDeleteBuffers(1, &pep_Shadow_GroundPositionVbo);
		pep_Shadow_GroundPositionVbo = 0;
	}

	if (pep_Shadow_GroundNormalVbo)
	{
		glDeleteBuffers(1, &pep_Shadow_GroundNormalVbo);
		pep_Shadow_GroundNormalVbo = 0;
	}

	if (pep_Shadow_GroundVao)
	{
		glDeleteVertexArrays(1, &pep_Shadow_GroundVao);
		pep_Shadow_GroundVao = 0;
	}

	if (pep_Shadow_ShaderProgram) {
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_Shadow_ShaderProgram);

		glGetProgramiv(pep_Shadow_ShaderProgram, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders) {
			glGetAttachedShaders(pep_Shadow_ShaderProgram, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(pep_Shadow_ShaderProgram, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_Shadow_ShaderProgram);
		pep_Shadow_ShaderProgram = 0;

		glUseProgram(0);
	}

}

void Shadow_Update()
{
}

void Shadow_Resize(int width, int height)
{
	if (pep_Shadow_FrameBuffer)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pep_Shadow_FrameBuffer);
		glBindTexture(GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32/*GL_DEPTH_COMPONENT24*/, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT/*GL_UNSIGNED_BYTE*/, NULL);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void ASTROMEDiCOMP_Display()
{
	/*
	Light Position: vec4(-3.624088, 3.589553 5.786757 1.000000)
	Light Position: vec4(2.875912, 3.589553 5.786757 1.000000)
	*/
	//glUniform4fv(pep_Shadow_UniformLightPosition, 1, vec4(-3.624088f, 3.589553f, 5.786757f, 1.000000f));

	float translateASTROMEDICOMP[3] = { -3.75f, -0.50f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//A
	UpperLetters_Display(0);

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 0.7f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//S
	UpperLetters_Display(18);

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 1.2f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//T
	UpperLetters_Display(19);

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 1.8f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//R
	UpperLetters_Display(17);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 2.4f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 3.25f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//glUniform4fv(pep_Shadow_UniformLightPosition, 1, vec4(2.875912f, 3.589553f, 5.786757f, 1.000000f));
	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 4.05f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 4.55f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 5.25f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, pep_Shadow_materialAmbient_pearl); //bronze 
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, pep_Shadow_materialDiffuse_pearl);
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1,pep_Shadow_materialSpecular_pearl);
	glUniform1f(pep_Shadow_UniformMaterialShiness, pep_Shadow_materialShiness_pearl);

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, pep_Shadow_materialAmbient_emerald);
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, pep_Shadow_materialDiffuse_emerald);
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1, pep_Shadow_materialSpecular_emerald);
	glUniform1f(pep_Shadow_UniformMaterialShiness, pep_Shadow_materialShiness_emerald);

	//C
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 5.55f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 6.35f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 7.15f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 7.90f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);
}

void MATRIXGROUP_Display()
{
	float translateASTROMEDICOMP[3] = { -3.50f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;
	//M
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.85f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//T
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 1.40f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//R
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.0f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//I
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.7f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, pep_Shadow_materialAmbient_pearl); //bronze 
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, pep_Shadow_materialDiffuse_pearl);
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1,pep_Shadow_materialSpecular_pearl);
	glUniform1f(pep_Shadow_UniformMaterialShiness, pep_Shadow_materialShiness_pearl);

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, pep_Shadow_materialAmbient_emerald);
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, pep_Shadow_materialDiffuse_emerald);
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1, pep_Shadow_materialSpecular_emerald);
	glUniform1f(pep_Shadow_UniformMaterialShiness, pep_Shadow_materialShiness_emerald);

	//X
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.05f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(23);

	//G
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 4.3f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//R
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 5.05f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//O
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 5.65f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//U
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0] + 6.55f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//P
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 7.3f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);
}

void PRESENTS_Display()
{
	float translateASTROMEDICOMP[3] = { -1.50f, -0.75f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;
	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.4f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.88f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 1.30f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 1.65f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.0f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.57f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.02f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);
}

void CONCEPTUSED_Display()
{
	float tsTranslate[3] = { -3.75f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//C
	UpperLetters_Display(2);

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//O
	UpperLetters_Display(14);

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//N
	UpperLetters_Display(13);

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	//C
	UpperLetters_Display(2);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);	
}

void EFFECTSUSED_Display()
{
	float tsTranslate[3] = { -3.75f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	//E
	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
	
	UpperLetters_Display(5);

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
	
	UpperLetters_Display(5);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//C
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);
}

void FOG_Display()
{
	float tsTranslate[3] = { -2.60f, -0.75f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);
}

void TERRAIN_Display()
{
	float tsTranslate[3] = { -2.60f, -3.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;

	//T
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//E
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//R
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.95f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//R
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.45f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//I
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.550f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.750f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.75f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);
}

void LIGHTS_Display()
{
	float tsTranslate[3] = { -2.60f, -1.5f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//H
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(7);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);
}

void FRAMEBUFFERS_Display()
{
	float tsTranslate[3] = { -2.60f, -2.25f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.45f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.05f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.85f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.30f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.85f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.250f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.650f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.050f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.50f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);
}

void BLENDING_Display()
{
	float tsTranslate[3] = { -2.60f, -3.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.950f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 01.40f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 02.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 02.60f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 02.80f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 03.40f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);
}

void MODELLOADING_Display()
{
	float tsTranslate[3] = { -2.60f, -1.5f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.75f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.75f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);
}

void NOISE_Display()
{
	float tsTranslate[3] = { -2.60f, -2.25f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.35f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);
}

void NORMALMAPPING_Display()
{
	float tsTranslate[3] = { -2.60f, -0.75f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.95f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);
}

void SHADOW_Display()
{
	float tsTranslate[3] = { -2.60f, -1.5f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//H
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(7);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//W
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(22);
}

void TEXTURES_Display()
{
	float tsTranslate[3] = { -2.60f, -0.75f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//X
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(23);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);	
}

void WATER_Display()
{
	float tsTranslate[3] = { -2.60f, -2.250f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//W
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(22);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);
}

void TECHNOLOGYUSED_Display()
{
	float tsTranslate[3] = { -4.75f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	// T
	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);
	
	UpperLetters_Display(19);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// C
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);

	// H
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(7);

	// N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	// O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	// O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	// Y
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(24);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 9.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 9.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);
}

void RENDERINGOPENGL_Display()
{
	float tsTranslate[3] = { -3.60f, -0.75f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//Colon
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.9f, tsTranslate[1] + 0.05f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(26);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);	

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 9.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);
}

void LANGUAGEC_Display()
{
	float tsTranslate[3] = { -3.60f, -1.5f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);	

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//Colon
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.6f, tsTranslate[1] + 0.05f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(26);

	//C
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);
}

void OSWINDOWS10_Display()
{
	float tsTranslate[3] = { -3.60f, -2.25f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//Colon
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.05f, tsTranslate[1] + 0.05f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(26);

	//W
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(22);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// W
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(22);

	// S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	// 1
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(28);

	// 0
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(27);
}

void REFERENCES_Display()
{
	float translateASTROMEDICOMP[3] = { -3.0f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 0.65f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 1.20f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 1.7f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 2.25f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 2.95f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 3.45f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//C
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 4.22f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 4.95f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0]+ 5.550f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.0f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);
}

void ASSIGNMENTS_Display()
{
	float translateASTROMEDICOMP[3] = { -2.0f, -0.750f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.55f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.950f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 1.350f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 1.55f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.10f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.70f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.3f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.75f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 4.4f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 4.95f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);
}

void REDBOOK_Display()
{
	float translateASTROMEDICOMP[3] = { -2.0f, -1.50f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.45f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.85f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 1.7f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.15f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 2.75f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//K
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.35f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);
}

void SUPERBIBLE_Display()
{
	float translateASTROMEDICOMP[3] = { -2.0f, -0.750f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.4f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 0.950f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 01.35f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 01.75f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 02.20f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 02.65f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 02.85f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 03.30f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 03.7f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 4.3f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 4.75f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 5.4f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//K
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 6.0f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);
}

void ORANGE_Display()
{
	float translateASTROMEDICOMP[3] = { -2.0f, -1.50f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0], translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0] + 0.6f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0] + 1.1f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0] + 1.6f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0] + 2.2f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(translateASTROMEDICOMP[0] + 2.75f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.45f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 3.9f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 4.5f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//K
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T =  translate(translateASTROMEDICOMP[0] + 5.1f, translateASTROMEDICOMP[1], translateASTROMEDICOMP[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.75f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);
}

void SPECIALTHANKSTORTR3GROUPLEADERS_Display()
{
	float tsTranslate[3] = { -4.0f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//s
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.45f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.90f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//C
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.35f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.80f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.60f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//H
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.10f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(7);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.70f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.20f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//K
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.85f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.40f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//T
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.10f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	// O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.60f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] - 1.0f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	// L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] - 0.30f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	// L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.30f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	// G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.20f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	// R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.95f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	// O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.55f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.4f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	// P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.1f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	// L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.0f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.55f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.15f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	// D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.85f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.6f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.15f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	// S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.8f, tsTranslate[1] - 1.0f, tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

}

void THANKYOU_Display()
{
	float tsTranslate[3] = { -3.0f, -0.250f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;

	//T
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//H
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.70f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(7);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.50f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//N
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.20f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//K
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.05f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);

	//Y
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(24);

	//Y
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//Y
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);
}

void BIRDLIFE_Display()
{
	float tsTranslate[3] = { -3.0f, -0.250f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.15f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.95f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	//L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.5f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//I
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//F
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(1.25f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);
}


void GROUPMEMBERS_Display()
{
	// G
	float tsTranslate[3] = { -5.50f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.20f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	// R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	// O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.65f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	// P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.30f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	// M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.40f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// M
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.75f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	// B
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(1);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.85f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	// S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 9.65f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);
}

void TAUFIKSAYYED_Display()
{
	float tsTranslate[3] = { -3.0f, -1.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;

	//T
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.50f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//U
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.10f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//F
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.70f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(5);

	//I
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.15f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//K
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);

	//S
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.75f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//Y
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(24);

	//E
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//E
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);
}

void AJINKYANIKAM_Display()
{
	float tsTranslate[3] = { -3.0f, -1.90f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//J
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.65f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(9);

	//I
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.9f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//N
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.15f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//K
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);

	//Y
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.35f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(24);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.75f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//N
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.7f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//I
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.35f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//K
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(10);

	//A
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.2f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//M
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(0.8f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);
}

void JINESHSHAHA_Display()
{

}

void GROUPLEADER_Display()
{
	// G
	float tsTranslate[3] = { -4.0f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//G
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.20f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(6);

	// R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.85f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	// O
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.37f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	// U
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.1f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	// P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.70f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	// L
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.50f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	// D
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.150f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);

	// E
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.81f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	// R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.32f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R * scale(0.8f);
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);
}

void PRASANNAPAWAR_Display()
{
	
	float tsTranslate[3] = { -3.750f, -1.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 l_M;
	mat4 l_MV;

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.6f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.3f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//S
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.05f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//N
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(13);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.80f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//P
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.0f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(15);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 6.60f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//W
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 7.20f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(22);

	//A
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.20f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(0);

	//R
	T = mat4::identity();
	R = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 8.95f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	l_M = T * R;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(17);
}

void TOOLSUSED_Display()
{
	float tsTranslate[3] = { -3.0f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;

	//T
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(19);

	//O
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//O
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.4f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(14);

	//L
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.25f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(11);

	//S
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.80f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//U
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 3.80f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//S
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 4.60f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//E
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.10f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(4);

	//D
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 5.70f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(3);
}

void MUSIC_Display()
{
	float tsTranslate[3] = { -1.250f, 0.0f, -1.0f };
	mat4 T;
	mat4 R;
	mat4 S;
	mat4 l_M;
	mat4 l_MV;

	//M
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0], tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(12);

	//M
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 0.8f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(20);

	//S
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 1.55f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(18);

	//I
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.05f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(8);

	//C
	T = mat4::identity();
	R = mat4::identity();
	S = mat4::identity();
	l_M = mat4::identity();
	l_MV = mat4::identity();

	T = translate(tsTranslate[0] + 2.35f, tsTranslate[1], tsTranslate[2]);
	R = rotate(pep_LetterRotationApply, 0.0f, 1.0f, 0.0f);
	S = scale(1.0f);
	l_M = T * R * S;
	l_MV = pep_Shadow_ViewMatrix;

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, l_M);
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, l_MV);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	UpperLetters_Display(2);
}

void Shadow_DisplayScene(void)
{
	
	pep_Shadow_ProjectionMatrix = mat4::identity();
	pep_Shadow_ViewMatrix = mat4::identity();

	pep_Shadow_ProjectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / pep_gHeight, 0.1f, 1000.f);
	pep_Shadow_ViewMatrix = lookat(vec3(0.0f, 02.50f, 8.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	glUseProgram(pep_Shadow_ShaderProgram);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pep_Shadow_FrameBuffer_ShadowMapTexture);
	glUniform1f(pep_Shadow_UniformShadowMapTexture, 0);


	//
	// ground
	//
	glUniform1i(pep_Shadow_UniformIsLightPass, 0);

	glUniformMatrix4fv(pep_Shadow_UniformModelMatrix, 1, GL_FALSE, mat4::identity() * scale(4.0f));
	glUniformMatrix4fv(pep_Shadow_UniformViewMatrix, 1, GL_FALSE, pep_Shadow_ViewMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformProjectionMatrix, 1, GL_FALSE, pep_Shadow_ProjectionMatrix);
	glUniformMatrix4fv(pep_Shadow_UniformShadowMatrix, 1, GL_FALSE, pep_Shadow_LightShadowMatrix);

	glUniform3fv(pep_Shadow_UniformLightAmbient, 1, pep_Shadow_lightAmbient);
	glUniform3fv(pep_Shadow_UniformLightDiffuse, 1, pep_Shadow_lightDiffuse);
	glUniform3fv(pep_Shadow_UniformLightSpecular, 1, pep_Shadow_lightSpecular);
	glUniform4fv(pep_Shadow_UniformLightPosition, 1, pep_Shadow_lightPosition);

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, pep_Shadow_materialAmbient_pearl); //bronze 
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, pep_Shadow_materialDiffuse_pearl);
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1,pep_Shadow_materialSpecular_pearl);
	glUniform1f(pep_Shadow_UniformMaterialShiness, pep_Shadow_materialShiness_pearl);

	glBindVertexArray(pep_Shadow_GroundVao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	//glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glBindVertexArray(0);

	glUniform3fv(pep_Shadow_UniformMaterialAmbient, 1, pep_Shadow_materialAmbient_emerald);
	glUniform3fv(pep_Shadow_UniformMaterialDiffuse, 1, pep_Shadow_materialDiffuse_emerald);
	glUniform3fv(pep_Shadow_UniformMaterialSpecular, 1, pep_Shadow_materialSpecular_emerald);
	glUniform1f(pep_Shadow_UniformMaterialShiness, pep_Shadow_materialShiness_emerald);

	if (0 == pep_Scene)
	{
		ASTROMEDiCOMP_Display();
	}
	else if (1 == pep_Scene)
	{
		MATRIXGROUP_Display();
		PRESENTS_Display();
	}
	else if (2 == pep_Scene)
	{
		//Title:
		BIRDLIFE_Display();
	}
	else if (3 == pep_Scene)
	{
		//City
	}
	else if (4 == pep_Scene)
	{
		//City
	}
	else if (5 == pep_Scene)
	{
		//City
	}
	else if (6 == pep_Scene)
	{
		//Village
	}
	else if (7 == pep_Scene)
	{
		//Village
	}
	else if (8 == pep_Scene)
	{
		//City
	}
	else if (9 == pep_Scene)
	{
		EFFECTSUSED_Display();
		NORMALMAPPING_Display();
		SHADOW_Display();
		WATER_Display();
		TERRAIN_Display();
	}
	else if (10 == pep_Scene)
	{
		EFFECTSUSED_Display();
		FOG_Display();
		MODELLOADING_Display();
		NOISE_Display();
	}
	else if (11 == pep_Scene)
	{
		CONCEPTUSED_Display();
		TEXTURES_Display();
		LIGHTS_Display();
		FRAMEBUFFERS_Display();
		BLENDING_Display();
	}
	else if (12 == pep_Scene)
	{
		//Reference
		REFERENCES_Display();
		ASSIGNMENTS_Display();
		REDBOOK_Display();
	}
	else if (13 == pep_Scene)
	{
		//Reference
		REFERENCES_Display();
		SUPERBIBLE_Display();
		ORANGE_Display();
	}
	else if (14 == pep_Scene)
	{
		TECHNOLOGYUSED_Display();
		RENDERINGOPENGL_Display();
		LANGUAGEC_Display();
		OSWINDOWS10_Display();
	}
	else if (15 == pep_Scene)
	{
		//Tools Used
		TOOLSUSED_Display();
		//AudaCity
		//Blender
	}
	else if (16 == pep_Scene)
	{
		//Music
		MUSIC_Display();
	}
	else if (17 == pep_Scene)
	{
		//Music
		MUSIC_Display();
	}
	else if (18 == pep_Scene)
	{
		//Music Edit By Kajal Tingare
	}
	else if (19 == pep_Scene)
	{
		GROUPMEMBERS_Display();
		TAUFIKSAYYED_Display();
		AJINKYANIKAM_Display();
		JINESHSHAHA_Display();
	}
	else if (20 == pep_Scene)
	{
		GROUPLEADER_Display();
		PRASANNAPAWAR_Display();
	}
	else if (21 == pep_Scene)
	{
		//Special Thanks To RTR3 Group Leaders
		SPECIALTHANKSTORTR3GROUPLEADERS_Display();
	}
	else if (22 == pep_Scene)
	{
		// THanks You
		THANKYOU_Display();
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);

	return;
}

void Shadow_DisplayLightPass(void)
{
	mat4 T = mat4::identity();
	mat4 R = mat4::identity();
	mat4 l_M = mat4::identity();
	mat4 l_MV = mat4::identity();
	mat4 l_MVP = mat4::identity();

	glUseProgram(pep_Shadow_ShaderProgram);

	//
	// ground: Not Required

	if (0 == pep_Scene)
	{
		ASTROMEDiCOMP_Display();
	}
	else if (1 == pep_Scene)
	{
		MATRIXGROUP_Display();
		PRESENTS_Display();
	}
	else if (2 == pep_Scene)
	{
		//Title:
		BIRDLIFE_Display();
	}
	else if (3 == pep_Scene)
	{
		//City
	}
	else if (4 == pep_Scene)
	{
		//City
	}
	else if (5 == pep_Scene)
	{
		//City
	}
	else if (6 == pep_Scene)
	{
		//Village
	}
	else if (7 == pep_Scene)
	{
		//Village
	}
	else if (8 == pep_Scene)
	{
		//City
	}
	else if (9 == pep_Scene)
	{
		EFFECTSUSED_Display();
		NORMALMAPPING_Display();
		SHADOW_Display();
		WATER_Display();
		TERRAIN_Display();
	}
	else if (10 == pep_Scene)
	{
		EFFECTSUSED_Display();
		FOG_Display();
		MODELLOADING_Display();
		NOISE_Display();
	}
	else if (11 == pep_Scene)
	{
		CONCEPTUSED_Display();
		TEXTURES_Display();
		LIGHTS_Display();
		FRAMEBUFFERS_Display();
		BLENDING_Display();
	}
	else if (12 == pep_Scene)
	{
		//Reference
		REFERENCES_Display();
		ASSIGNMENTS_Display();
		REDBOOK_Display();
	}
	else if (13 == pep_Scene)
	{
		//Reference
		REFERENCES_Display();
		SUPERBIBLE_Display();
		ORANGE_Display();
	}
	else if (14 == pep_Scene)
	{
		TECHNOLOGYUSED_Display();
		RENDERINGOPENGL_Display();
		LANGUAGEC_Display();
		OSWINDOWS10_Display();
	}
	else if (15 == pep_Scene)
	{
		//Tools Used
		TOOLSUSED_Display();
		//AudaCity
	}
	else if (16 == pep_Scene)
	{
		//Music
		MUSIC_Display();
	}
	else if (17 == pep_Scene)
	{
		//Music
		MUSIC_Display();
	}
	else if (18 == pep_Scene)
	{
		//Music Edit By Kajal Tingare
	}
	else if (19 == pep_Scene)
	{
		GROUPMEMBERS_Display();
		TAUFIKSAYYED_Display();
		AJINKYANIKAM_Display();
		JINESHSHAHA_Display();
	}
	else if (20 == pep_Scene)
	{
		GROUPLEADER_Display();
		PRASANNAPAWAR_Display();
	}
	else if (21 == pep_Scene)
	{
		//Special Thanks To RTR3 Group Leaders
		SPECIALTHANKSTORTR3GROUPLEADERS_Display();
	}
	else if (22 == pep_Scene)
	{
		// THanks You
		THANKYOU_Display();
	}

	// Other Models, Geometries etc

	glUseProgram(0);

	return;
}

void Shadow_Display()
{
	pep_Shadow_LightViewMatrix = mat4::identity();
	pep_Shadow_LightViewMatrix = lookat(vec3(pep_Shadow_lightPosition[0], pep_Shadow_lightPosition[1], pep_Shadow_lightPosition[2]),
		vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

	pep_Shadow_LightProjectionMatrix = mat4::identity();
	pep_Shadow_LightProjectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / pep_gHeight, 0.1f,
		1000.f); 

	pep_Shadow_LightBiasMatrix = mat4::identity();
	pep_Shadow_LightBiasMatrix = pep_Shadow_LightBiasMatrix * translate(0.5f, 0.5f, 0.5f);
	pep_Shadow_LightBiasMatrix = pep_Shadow_LightBiasMatrix * scale(0.5f, 0.5f, 0.5f);

	pep_Shadow_LightBiasProjectionMatrix = mat4::identity();
	pep_Shadow_LightBiasProjectionMatrix = pep_Shadow_LightBiasMatrix * pep_Shadow_LightProjectionMatrix;

	pep_Shadow_LightShadowMatrix = mat4::identity();
	pep_Shadow_LightShadowMatrix = pep_Shadow_LightBiasProjectionMatrix * pep_Shadow_LightViewMatrix;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// 1) Render scene from the light's POV
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pep_Shadow_FrameBuffer);
		glClear(GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);
		// fix peter panning - cull front faces
		glCullFace(GL_FRONT);
		Shadow_DisplayLightPass();
		glCullFace(GL_BACK); //=> Set to default again
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDrawBuffer(GL_BACK_LEFT);
	}

	// Second pass we render the scene as normal and use the generated depth map to calculate whether fragments are in shadow
	{
		glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Shadow_DisplayScene();
	}

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}
