#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int UpperLetters_Initialize(void);
void UpperLetters_Display(int letterIndex);
void UpperLetters_Uninitialize(void);
