#include "ModelLoaderForLetters.h"
#include "..\Common.h"

FILE *pModelLoaderForLetters_FileObj = NULL;

int ModelLoaderForLetters(char *pFileName, char *pObjectSeperater, LETTERS_MODEL_DATA **ppDataArray, int *pObjectCount)
{
	typedef struct _MODEL
	{
		int object_count;
		//
		// Complete Vertices Data (Not Properly arrange)  -> To Get Proper Faces/Arranged Data Of Each Object Refer Per Object Arrays And Use Elements/Indices To Get Proper Value of Vertices Data In Below Mentioned Arrays
		// 1. Vertex Position
		// 2. Normals
		// 3. TexCoords
		//
		int v_count; // count without vec3 touple
		float *pVerticesArray;

		int vn_count;
		float *pNormalsArray;

		int vt_count;
		float *pTexcoordsArray;
		char * materialName;

	} MODEL;

	MODEL model;

	typedef struct _OBJECT
	{
		// Face/Indices To Used In  Model Arrays
		int element_count; // count without vec3 touple
		int *pElementVertices;
		int *pElementTexcoord;
		int *pElementNormal;
		char *pElementMaterialName;
		char objectName[256];

	} OBJECT;
	OBJECT *pObjectArray = NULL;

	char buf[LETTERS_MODEL_LINE_LENGTH];

	if (0 != fopen_s(&pModelLoaderForLetters_FileObj, pFileName, "r"))
	{
		return 1;
	}

	memset(&model, 0, sizeof(MODEL));

	unsigned int line_index = 0;
	unsigned int object_count = 0;
	memset(buf, 0, LETTERS_MODEL_LINE_LENGTH);
	while (NULL != fgets(buf, LETTERS_MODEL_LINE_LENGTH, pModelLoaderForLetters_FileObj))
	{
		char *context = NULL;
		char *token = strtok_s(buf, " ", &context);
		char *objectName;
		//if (0 == strcmp(token, "o"))
		if (0 == strcmp(token, pObjectSeperater))
		{
			objectName = strtok_s(NULL, " ", &context);

			bool allocateNew = true;
			for (int i = 0; i < model.object_count; i++)
			{
				if (0 == strcmpi(pObjectArray[model.object_count - 1].objectName, objectName))
				{
					allocateNew = false;
					break;
				}
			}

			if (allocateNew)
			{
				if (NULL == pObjectArray)
				{
					pObjectArray = (OBJECT *)malloc(sizeof(OBJECT));
					memset(pObjectArray, 0, sizeof(OBJECT));
					model.object_count = 1;
					memcpy(pObjectArray[model.object_count - 1].objectName, objectName, strlen(objectName));
				}
				else
				{
					model.object_count += 1;
					pObjectArray = (OBJECT *)realloc(pObjectArray, model.object_count * sizeof(OBJECT));
					memset(pObjectArray + model.object_count - 1, 0, sizeof(OBJECT));
					memcpy(pObjectArray[model.object_count - 1].objectName, objectName, strlen(objectName));
				}
			}

			if(0 == strcmp(token, "usemtl"))
			{				
				model.materialName = (char *)malloc(strlen(objectName) * sizeof(char));
				memset(model.materialName, 0, sizeof(strlen(objectName) * sizeof(char)));
				strcpy(model.materialName, objectName);
			}
		}
		else if (0 == strcmp(token, "v"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pVerticesArray)
			{
				model.v_count = 3;
				model.pVerticesArray = (float *)malloc(model.v_count * sizeof(float));
				memset(model.pVerticesArray, 0, sizeof(model.v_count * sizeof(float)));
			}
			else
			{
				model.v_count += 3;
				model.pVerticesArray = (float *)realloc(model.pVerticesArray, model.v_count * sizeof(float));
			}

			model.pVerticesArray[model.v_count - 3] = x;
			model.pVerticesArray[model.v_count - 2] = y;
			model.pVerticesArray[model.v_count - 1] = z;

		}
		else if (0 == strcmp(token, "vn"))
		{
			float x, y, z;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));
			z = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pNormalsArray)
			{
				model.vn_count = 3;
				model.pNormalsArray = (float *)malloc(model.vn_count * sizeof(float));
				memset(model.pNormalsArray, 0, sizeof(model.vn_count * sizeof(float)));
			}
			else
			{
				model.vn_count += 3;
				model.pNormalsArray = (float *)realloc(model.pNormalsArray, model.vn_count * sizeof(float));
			}

			model.pNormalsArray[model.vn_count - 3] = x;
			model.pNormalsArray[model.vn_count - 2] = y;
			model.pNormalsArray[model.vn_count - 1] = z;

		}
		else if (0 == strcmp(token, "vt"))
		{
			float x, y;
			x = (float)atof(strtok_s(NULL, " ", &context));
			y = (float)atof(strtok_s(NULL, " ", &context));

			if (NULL == model.pTexcoordsArray)
			{
				model.vt_count = 2;
				model.pTexcoordsArray = (float *)malloc(model.vt_count * sizeof(float));
				memset(model.pTexcoordsArray, 0, sizeof(model.vt_count * sizeof(float)));
			}
			else
			{
				model.vt_count += 2;
				model.pTexcoordsArray = (float *)realloc(model.pTexcoordsArray, model.vt_count * sizeof(float));
			}

			model.pTexcoordsArray[model.vt_count - 2] = x;
			model.pTexcoordsArray[model.vt_count - 1] = y;
		}

		else if (0 == strcmp(token, "f"))
		{
			char *first_token = strtok_s(NULL, " ", &context);
			char *second_token = strtok_s(NULL, " ", &context);
			char *third_token = strtok_s(NULL, " ", &context);

			char *first_context = NULL;
			int v1 = (int)atoi(strtok_s(first_token, "/", &first_context));
			int vt1 = -1;
			int vn1 = -1;
			if('/' != *first_context)
			{
				vt1 = (int)atoi(strtok_s(NULL, "/", &first_context));
			}
			if('/' != *first_context)
			{
				vn1 = (int)atoi(strtok_s(NULL, "/", &first_context));
			}

			char *second_context = NULL;
			int v2 = (int)atoi(strtok_s(second_token, "/", &second_context));
			int vt2 = -1;
			int vn2 = -1;
			if('/' != *second_context)
			{
				vt2 = (int)atoi(strtok_s(NULL, "/", &second_context));
			}
			if('/' != *second_context)
			{
				vn2 = (int)atoi(strtok_s(NULL, "/", &second_context));
			}

			char *third_context = NULL;
			int v3 = (int)atoi(strtok_s(third_token, "/", &third_context));
			int vt3 = -1;
			int vn3 = -1;
			if ('/' != *third_context)
			{
				vt3 = (int)atoi(strtok_s(NULL, "/", &third_context));
			}
			if ('/' != *third_context)
			{
				vn3 = (int)atoi(strtok_s(NULL, "/", &third_context));
			}

			pObjectArray[model.object_count - 1].element_count += 3;
			if (NULL == pObjectArray[model.object_count - 1].pElementVertices)
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementVertices, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementVertices = (int *)realloc(pObjectArray[model.object_count - 1].pElementVertices, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 3] = v1;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 2] = v2;
			pObjectArray[model.object_count - 1].pElementVertices[pObjectArray[model.object_count - 1].element_count - 1] = v3;

			if (NULL == pObjectArray[model.object_count - 1].pElementNormal)
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementNormal, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementNormal = (int *)realloc(pObjectArray[model.object_count - 1].pElementNormal, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 3] = vn1;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 2] = vn2;
			pObjectArray[model.object_count - 1].pElementNormal[pObjectArray[model.object_count - 1].element_count - 1] = vn3;

			if (NULL == pObjectArray[model.object_count - 1].pElementTexcoord)
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)malloc(pObjectArray[model.object_count - 1].element_count * sizeof(int));
				memset(pObjectArray[model.object_count - 1].pElementTexcoord, 0, sizeof(pObjectArray[model.object_count - 1].element_count * sizeof(int)));
			}
			else
			{
				pObjectArray[model.object_count - 1].pElementTexcoord = (int *)realloc(pObjectArray[model.object_count - 1].pElementTexcoord, pObjectArray[model.object_count - 1].element_count * sizeof(int));
			}
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 3] = vt1;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 2] = vt2;
			pObjectArray[model.object_count - 1].pElementTexcoord[pObjectArray[model.object_count - 1].element_count - 1] = vt3;

			// Material
			if (NULL != model.materialName)
			{
				pObjectArray[model.object_count - 1].pElementMaterialName = (char *)malloc(strlen(model.materialName) * sizeof(char));
				memset(pObjectArray[model.object_count - 1].pElementMaterialName, 0, sizeof(strlen(model.materialName) * sizeof(char)));
				strcpy(pObjectArray[model.object_count - 1].pElementMaterialName, model.materialName);
			}
		}

		memset(buf, 0, LETTERS_MODEL_LINE_LENGTH);
	}

	//letterA_gObjectCount = model.object_count;
	*pObjectCount = model.object_count;

	//letterA_gObjectCount = model.object_count;
	*ppDataArray = (LETTERS_MODEL_DATA *)malloc(sizeof(LETTERS_MODEL_DATA) * model.object_count);
	memset(*ppDataArray, 0, sizeof(LETTERS_MODEL_DATA) * model.object_count);

	for (int i = 0; i < *pObjectCount; i++)
	{
		(*ppDataArray)[i].data_count = pObjectArray[i].element_count / LETTERS_MODEL_FACE_TUPLE;

		(*ppDataArray)[i].pVertices = (float *)malloc(sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_VERTEX_IN_3D_PLANE);
		memset((*ppDataArray)[i].pVertices, 0, sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_VERTEX_IN_3D_PLANE);

		(*ppDataArray)[i].pTexcoords = (float *)malloc(sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_TEXCOORD);
		memset((*ppDataArray)[i].pTexcoords, 0, sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_TEXCOORD);

		(*ppDataArray)[i].pNormals = (float *)malloc(sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);
		memset((*ppDataArray)[i].pNormals, 0, sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);

		(*ppDataArray)[i].pTangent = (float *)malloc(sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);
		memset((*ppDataArray)[i].pTangent, 0, sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);

		(*ppDataArray)[i].pBitangent = (float *)malloc(sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);
		memset((*ppDataArray)[i].pBitangent, 0, sizeof(float) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);

		// Material
		(*ppDataArray)[i].pMaterialName = (char *)malloc(sizeof(char) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);
		memset((*ppDataArray)[i].pMaterialName, 0, sizeof(char) * (*ppDataArray)[i].data_count * LETTERS_MODEL_FACE_TUPLE * LETTERS_MODEL_NORMAL_IN_3D_PLANE);

		int vi = 0;
		int vt = 0;
		int vn = 0;
		for (int fn = 0; fn < pObjectArray[i].element_count / LETTERS_MODEL_FACE_TUPLE; fn++)
		{
			// Material from MTL
			if (NULL != pObjectArray[i].pElementMaterialName)
			{
				strcpy((*ppDataArray)[i].pMaterialName, pObjectArray[i].pElementMaterialName);
			}

			int v1 = pObjectArray[i].pElementVertices[fn * LETTERS_MODEL_FACE_TUPLE + 0] - 1; // Index is array start with Zero Hence "-1" From The Element Index Which Is Start From 1
			int v2 = pObjectArray[i].pElementVertices[fn * LETTERS_MODEL_FACE_TUPLE + 1] - 1;
			int v3 = pObjectArray[i].pElementVertices[fn * LETTERS_MODEL_FACE_TUPLE + 2] - 1;

			(*ppDataArray)[i].pVertices[vi + 0] = model.pVerticesArray[v1 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 0];
			(*ppDataArray)[i].pVertices[vi + 1] = model.pVerticesArray[v1 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 1];
			(*ppDataArray)[i].pVertices[vi + 2] = model.pVerticesArray[v1 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 2];
			(*ppDataArray)[i].pVertices[vi + 3] = model.pVerticesArray[v2 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 0];
			(*ppDataArray)[i].pVertices[vi + 4] = model.pVerticesArray[v2 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 1];
			(*ppDataArray)[i].pVertices[vi + 5] = model.pVerticesArray[v2 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 2];
			(*ppDataArray)[i].pVertices[vi + 6] = model.pVerticesArray[v3 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 0];
			(*ppDataArray)[i].pVertices[vi + 7] = model.pVerticesArray[v3 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 1];
			(*ppDataArray)[i].pVertices[vi + 8] = model.pVerticesArray[v3 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 2];

			if (NULL != model.pTexcoordsArray)
			{
				int t1 = pObjectArray[i].pElementTexcoord[fn * LETTERS_MODEL_FACE_TUPLE + 0] - 1;
				int t2 = pObjectArray[i].pElementTexcoord[fn * LETTERS_MODEL_FACE_TUPLE + 1] - 1;
				int t3 = pObjectArray[i].pElementTexcoord[fn * LETTERS_MODEL_FACE_TUPLE + 2] - 1;

				(*ppDataArray)[i].pTexcoords[vt + 0] = model.pTexcoordsArray[t1 * LETTERS_MODEL_TEXCOORD + 0];
				(*ppDataArray)[i].pTexcoords[vt + 1] = model.pTexcoordsArray[t1 * LETTERS_MODEL_TEXCOORD + 1];
				(*ppDataArray)[i].pTexcoords[vt + 2] = model.pTexcoordsArray[t2 * LETTERS_MODEL_TEXCOORD + 0];
				(*ppDataArray)[i].pTexcoords[vt + 3] = model.pTexcoordsArray[t2 * LETTERS_MODEL_TEXCOORD + 1];
				(*ppDataArray)[i].pTexcoords[vt + 4] = model.pTexcoordsArray[t3 * LETTERS_MODEL_TEXCOORD + 0];
				(*ppDataArray)[i].pTexcoords[vt + 5] = model.pTexcoordsArray[t3 * LETTERS_MODEL_TEXCOORD + 1];
			}

			if (NULL != model.pNormalsArray)
			{
				int n1 = pObjectArray[i].pElementNormal[fn * LETTERS_MODEL_FACE_TUPLE + 0] - 1;
				int n2 = pObjectArray[i].pElementNormal[fn * LETTERS_MODEL_FACE_TUPLE + 1] - 1;
				int n3 = pObjectArray[i].pElementNormal[fn * LETTERS_MODEL_FACE_TUPLE + 2] - 1;

				(*ppDataArray)[i].pNormals[vn + 0] = model.pNormalsArray[n1 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 0];
				(*ppDataArray)[i].pNormals[vn + 1] = model.pNormalsArray[n1 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 1];
				(*ppDataArray)[i].pNormals[vn + 2] = model.pNormalsArray[n1 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 2];
				(*ppDataArray)[i].pNormals[vn + 3] = model.pNormalsArray[n2 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 0];
				(*ppDataArray)[i].pNormals[vn + 4] = model.pNormalsArray[n2 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 1];
				(*ppDataArray)[i].pNormals[vn + 5] = model.pNormalsArray[n2 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 2];
				(*ppDataArray)[i].pNormals[vn + 6] = model.pNormalsArray[n3 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 0];
				(*ppDataArray)[i].pNormals[vn + 7] = model.pNormalsArray[n3 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 1];
				(*ppDataArray)[i].pNormals[vn + 8] = model.pNormalsArray[n3 * LETTERS_MODEL_VERTEX_IN_3D_PLANE + 2];
			}

			float pos0[3];
			pos0[0] = (*ppDataArray)[i].pVertices[vi + 0];
			pos0[1] = (*ppDataArray)[i].pVertices[vi + 1];
			pos0[2] = (*ppDataArray)[i].pVertices[vi + 2];

			float pos1[3];
			pos1[0] = (*ppDataArray)[i].pVertices[vi + 3];
			pos1[1] = (*ppDataArray)[i].pVertices[vi + 4];
			pos1[2] = (*ppDataArray)[i].pVertices[vi + 5];

			float pos2[3];
			pos2[0] = (*ppDataArray)[i].pVertices[vi + 6];
			pos2[1] = (*ppDataArray)[i].pVertices[vi + 7];
			pos2[2] = (*ppDataArray)[i].pVertices[vi + 8];

			float tex0[2];
			tex0[0] = (*ppDataArray)[i].pTexcoords[vt + 0];
			tex0[1] = (*ppDataArray)[i].pTexcoords[vt + 1];

			float tex1[2];
			tex1[0] = (*ppDataArray)[i].pTexcoords[vt + 2];
			tex1[1] = (*ppDataArray)[i].pTexcoords[vt + 3];

			float tex2[2];
			tex2[0] = (*ppDataArray)[i].pTexcoords[vt + 4];
			tex2[1] = (*ppDataArray)[i].pTexcoords[vt + 5];

			float edge1[3];
			edge1[0] = pos1[0] - pos0[0];
			edge1[1] = pos1[1] - pos0[1];
			edge1[2] = pos1[2] - pos0[2];

			float edge2[3];
			edge2[0] = pos2[0] - pos0[0];
			edge2[1] = pos2[1] - pos0[1];
			edge2[2] = pos2[2] - pos0[2];


			float DeltaU1 = tex1[0] - tex0[0];
			float DeltaV1 = tex1[1] - tex0[1];
			float DeltaU2 = tex2[0] - tex0[0];
			float DeltaV2 = tex2[1] - tex0[1];

			float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

			float Tangent[3];
			Tangent[0] = f * (DeltaV2 * edge1[0] - DeltaV1 * edge2[0]);
			Tangent[1] = f * (DeltaV2 * edge1[1] - DeltaV1 * edge2[1]);
			Tangent[2] = f * (DeltaV2 * edge1[2] - DeltaV1 * edge2[2]);

			(*ppDataArray)[i].pTangent[vi + 0] += Tangent[0];
			(*ppDataArray)[i].pTangent[vi + 1] += Tangent[1];
			(*ppDataArray)[i].pTangent[vi + 2] += Tangent[2];
			(*ppDataArray)[i].pTangent[vi + 3] += Tangent[0];
			(*ppDataArray)[i].pTangent[vi + 4] += Tangent[1];
			(*ppDataArray)[i].pTangent[vi + 5] += Tangent[2];
			(*ppDataArray)[i].pTangent[vi + 6] += Tangent[0];
			(*ppDataArray)[i].pTangent[vi + 7] += Tangent[1];
			(*ppDataArray)[i].pTangent[vi + 8] += Tangent[2];

			float biTangent[3];
			biTangent[0] = f * (-DeltaU2 * edge1[0] + DeltaU1 * edge2[0]);
			biTangent[1] = f * (-DeltaU2 * edge1[1] + DeltaU1 * edge2[1]);
			biTangent[2] = f * (-DeltaU2 * edge1[2] + DeltaU1 * edge2[2]);

			(*ppDataArray)[i].pBitangent[vi + 0] += biTangent[0];
			(*ppDataArray)[i].pBitangent[vi + 1] += biTangent[1];
			(*ppDataArray)[i].pBitangent[vi + 2] += biTangent[2];
			(*ppDataArray)[i].pBitangent[vi + 3] += biTangent[0];
			(*ppDataArray)[i].pBitangent[vi + 4] += biTangent[1];
			(*ppDataArray)[i].pBitangent[vi + 5] += biTangent[2];
			(*ppDataArray)[i].pBitangent[vi + 6] += biTangent[0];
			(*ppDataArray)[i].pBitangent[vi + 7] += biTangent[1];
			(*ppDataArray)[i].pBitangent[vi + 8] += biTangent[2];

			vi += 9;
			vt += 6;
			vn += 9;
		}
	}

	for (int i = 0; i < model.object_count; i++)
	{
		free(pObjectArray[i].pElementNormal);
		pObjectArray[i].pElementNormal = NULL;

		free(pObjectArray[i].pElementTexcoord);
		pObjectArray[i].pElementTexcoord = NULL;

		free(pObjectArray[i].pElementVertices);
		pObjectArray[i].pElementVertices = NULL;
	}
	free(pObjectArray);
	
	fclose(pModelLoaderForLetters_FileObj);

	return 0;
}
