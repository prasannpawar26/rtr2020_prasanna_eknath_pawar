#include "Common.h"
#include "MultipleRenderTargets.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_MultipleRenderTargets_gVertexShaderObject;
GLuint pep_MultipleRenderTargets_gFragmentShaderObject;
GLuint pep_MultipleRenderTargets_gShaderProgramObject;
GLuint pep_MultipleRenderTargets_mvpUniform;
GLuint pep_MultipleRenderTargets_samplerUniform;

GLuint pep_MultipleRenderTargets_Square_vao;
GLuint pep_MultipleRenderTargets_Square_vbo_position;
GLuint pep_MultipleRenderTargets_Square_vbo_texcoords;

//
// FrameBuffer
//
GLuint pep_MultipleRenderTargets_Fbo;
GLuint pep_MultipleRenderTargets_Fbo_TextureBuffer[2];
GLuint pep_MultipleRenderTargets_Fbo_RenderBuffer;

//
// Render To Texture Variables
//
GLuint pep_MultipleRenderTargets_RTT_VertexShaderObject;
GLuint pep_MultipleRenderTargets_RTT_FragmentShaderObject;
GLuint pep_MultipleRenderTargets_RTT_ShaderProgramObject;
GLuint pep_MultipleRenderTargets_RTT_mvpUniform;

GLuint pep_MultipleRenderTargets_RTT_Square_vao;
GLuint pep_MultipleRenderTargets_RTT_Square_vbo_position;
GLuint pep_MultipleRenderTargets_RTT_Square_vbo_color;

GLuint pep_MultipleRenderTargets_RTT_Triangle_vao;
GLuint pep_MultipleRenderTargets_RTT_Triangle_vbo_position;
GLuint pep_MultipleRenderTargets_RTT_Triangle_vbo_color;


int MultipleRenderTargets_FBO_Initialize(int width, int height)
{
    glGenFramebuffers(1, &pep_MultipleRenderTargets_Fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, pep_MultipleRenderTargets_Fbo);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    glGenTextures(2, pep_MultipleRenderTargets_Fbo_TextureBuffer);

    for (int i = 0; i < 2; i++)
    {
        glBindTexture(GL_TEXTURE_2D, pep_MultipleRenderTargets_Fbo_TextureBuffer[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, pep_MultipleRenderTargets_Fbo_TextureBuffer[i], 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glGenRenderbuffers(1, &pep_MultipleRenderTargets_Fbo_RenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, pep_MultipleRenderTargets_Fbo_RenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, pep_MultipleRenderTargets_Fbo_RenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        fprintf( pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n", __FILE__, __LINE__, __FUNCTION__);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        return -1;
    }

    unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, attachments);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return 0;
}

void MultipleRenderTargets_FBO_ReSize(int width, int height)
{
    if (pep_MultipleRenderTargets_Fbo)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, pep_MultipleRenderTargets_Fbo);
        for (int i = 0; i < 2; i++)
        {
            glBindTexture(GL_TEXTURE_2D, pep_MultipleRenderTargets_Fbo_TextureBuffer[i]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, 0);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, pep_MultipleRenderTargets_Fbo_TextureBuffer[i], 0);
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glBindRenderbuffer(GL_RENDERBUFFER, pep_MultipleRenderTargets_Fbo_RenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, pep_MultipleRenderTargets_Fbo_RenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    return;
}

void MultipleRenderTargets_FBO_Uinitialize(void)
{
    if (pep_MultipleRenderTargets_Fbo_RenderBuffer)
    {
        glDeleteRenderbuffers(1, &pep_MultipleRenderTargets_Fbo_RenderBuffer);
        pep_MultipleRenderTargets_Fbo_RenderBuffer = 0;
    }

    for (int i = 0; i < 2; i++)
    {
        if (pep_MultipleRenderTargets_Fbo_TextureBuffer[i]) 
        {
            glDeleteRenderbuffers(1, &pep_MultipleRenderTargets_Fbo_TextureBuffer[i]);
            pep_MultipleRenderTargets_Fbo_TextureBuffer[i] = 0;
        }
    }

    if (pep_MultipleRenderTargets_Fbo)
    {
        glDeleteFramebuffers(1, &pep_MultipleRenderTargets_Fbo);
        pep_MultipleRenderTargets_Fbo = 0;
    }
}

int MultipleRenderTargets_RenderToTexture_Initialize(void)
{
    const GLchar *rendertotexture_VertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 RTT_vPosition;"
        "in vec4 RTT_vColor;"

        "uniform mat4 u_rtt_mvp_matrix;"

        "out vec4 color;"
        "void main(void)"
        "{"
            "gl_Position = u_rtt_mvp_matrix * RTT_vPosition;"
            "color = RTT_vColor;"
        "}";

    pep_MultipleRenderTargets_RTT_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == pep_MultipleRenderTargets_RTT_VertexShaderObject)
    {
        fprintf(pep_gpFile,"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader Failed For RTT_VertexShaderObject\n",__FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    glShaderSource(pep_MultipleRenderTargets_RTT_VertexShaderObject, 1, (const GLchar **)&rendertotexture_VertexShaderSourceCode, NULL);
    glCompileShader(pep_MultipleRenderTargets_RTT_VertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_MultipleRenderTargets_RTT_VertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader Compilation Failed.\n", __FILE__, __LINE__, __FUNCTION__);

        glGetShaderiv(pep_MultipleRenderTargets_RTT_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_MultipleRenderTargets_RTT_VertexShaderObject, iInfoLogLength, &written, szInfoLog);

                fprintf( pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }

    pep_MultipleRenderTargets_RTT_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == pep_MultipleRenderTargets_RTT_FragmentShaderObject)
    {
        fprintf(pep_gpFile,"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader Failed For RTT_VertexShaderObject\n",__FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    const GLchar *rendertotexture_FragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "in vec4 color;"

        "layout (location = 0) out vec4 RedFragColor;" \
        "layout (location = 1) out vec4 BlueFragColor;" \

        "void main(void)" \
        "{" \
            "if(color.r > 0.75)"
            "{"
                "RedFragColor = color;" \
            "}"
            "else"
            "{"
                "BlueFragColor = color;" \
            "}"
        "}";

    glShaderSource(pep_MultipleRenderTargets_RTT_FragmentShaderObject, 1, (const GLchar **)&rendertotexture_FragmentShaderSourceCode, NULL);
    glCompileShader(pep_MultipleRenderTargets_RTT_FragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(pep_MultipleRenderTargets_RTT_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (FALSE == iShaderCompileStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader Compilation Failed.\n", __FILE__, __LINE__, __FUNCTION__);
        glGetShaderiv(pep_MultipleRenderTargets_RTT_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_MultipleRenderTargets_RTT_FragmentShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }

    pep_MultipleRenderTargets_RTT_ShaderProgramObject = glCreateProgram();

    glAttachShader(pep_MultipleRenderTargets_RTT_ShaderProgramObject, pep_MultipleRenderTargets_RTT_VertexShaderObject);
    glAttachShader(pep_MultipleRenderTargets_RTT_ShaderProgramObject, pep_MultipleRenderTargets_RTT_FragmentShaderObject);
    glBindAttribLocation(pep_MultipleRenderTargets_RTT_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "RTT_vPosition");
    glBindAttribLocation(pep_MultipleRenderTargets_RTT_ShaderProgramObject, AMC_ATTRIBUTES_COLOR, "RTT_vColor");
    glLinkProgram(pep_MultipleRenderTargets_RTT_ShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_MultipleRenderTargets_RTT_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n", __FILE__, __LINE__, __FUNCTION__);

        glGetProgramiv(pep_MultipleRenderTargets_RTT_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_MultipleRenderTargets_RTT_ShaderProgramObject, iInfoLogLength, &written, szInfoLog);

                fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }
    pep_MultipleRenderTargets_RTT_mvpUniform = glGetUniformLocation(pep_MultipleRenderTargets_RTT_ShaderProgramObject, "u_rtt_mvp_matrix");

    //
    // Square
    //
    const GLfloat squareVertices[] = {1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
    const GLfloat squareColor[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &pep_MultipleRenderTargets_RTT_Square_vao);
    glBindVertexArray(pep_MultipleRenderTargets_RTT_Square_vao);

    glGenBuffers(1, &pep_MultipleRenderTargets_RTT_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_MultipleRenderTargets_RTT_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_MultipleRenderTargets_RTT_Square_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_MultipleRenderTargets_RTT_Square_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //
    // Triangle
    //
    const GLfloat triangleVertices[] = {0.0f,  1.0f,  0.0f, 1.0f, -1.0f,  0.0f, -1.0f, -1.0f, 0.0f};
    const GLfloat triangleColor[] = {1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f};

    glGenVertexArrays(1, &pep_MultipleRenderTargets_RTT_Triangle_vao);
    glBindVertexArray(pep_MultipleRenderTargets_RTT_Triangle_vao);

    glGenBuffers(1, &pep_MultipleRenderTargets_RTT_Triangle_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_MultipleRenderTargets_RTT_Triangle_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_MultipleRenderTargets_RTT_Triangle_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_MultipleRenderTargets_RTT_Triangle_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void MultipleRenderTargets_RenderToTexture_Uninitialize(void)
{
    if (pep_MultipleRenderTargets_RTT_Triangle_vbo_color)
    {
        glDeleteBuffers(1, &pep_MultipleRenderTargets_RTT_Triangle_vbo_color);
        pep_MultipleRenderTargets_RTT_Triangle_vbo_color = 0;
    }

    if (pep_MultipleRenderTargets_RTT_Triangle_vbo_position)
    {
        glDeleteBuffers(1, &pep_MultipleRenderTargets_RTT_Triangle_vbo_position);
        pep_MultipleRenderTargets_RTT_Triangle_vbo_position = 0;
    }

    if (pep_MultipleRenderTargets_RTT_Triangle_vao)
    {
        glDeleteVertexArrays(1, &pep_MultipleRenderTargets_RTT_Triangle_vao);
        pep_MultipleRenderTargets_RTT_Triangle_vao = 0;
    }

    if (pep_MultipleRenderTargets_RTT_Square_vbo_color)
    {
        glDeleteBuffers(1, &pep_MultipleRenderTargets_RTT_Square_vbo_color);
        pep_MultipleRenderTargets_RTT_Square_vbo_color = 0;
    }

    if (pep_MultipleRenderTargets_RTT_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_MultipleRenderTargets_RTT_Square_vbo_position);
        pep_MultipleRenderTargets_RTT_Square_vbo_position = 0;
    }

    if (pep_MultipleRenderTargets_RTT_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_MultipleRenderTargets_RTT_Square_vao);
        pep_MultipleRenderTargets_RTT_Square_vao = 0;
    }

    if (pep_MultipleRenderTargets_RTT_ShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_MultipleRenderTargets_RTT_ShaderProgramObject);

        glGetProgramiv(pep_MultipleRenderTargets_RTT_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_MultipleRenderTargets_RTT_ShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_MultipleRenderTargets_RTT_ShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_MultipleRenderTargets_RTT_ShaderProgramObject);
        pep_MultipleRenderTargets_RTT_ShaderProgramObject = 0;

        glUseProgram(0);
    }
}

void MultipleRenderTargets_RenderToTexture_Display()
{
    // Render
    glBindFramebuffer(GL_FRAMEBUFFER, pep_MultipleRenderTargets_Fbo);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glUseProgram(pep_MultipleRenderTargets_RTT_ShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    mat4 perspectiveMatrix;

    perspectiveMatrix = mat4::identity();
    perspectiveMatrix = perspective(45.0f, 1, 0.1f, 100.0f);

    //
    // Triangle
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(-2.0f, 0.0f, -8.0f);
    modelViewProjectionMatrix = perspectiveMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_MultipleRenderTargets_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(pep_MultipleRenderTargets_RTT_Triangle_vao);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    //
    // Square
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(2.0f, 0.0f, -8.0f);
    modelViewProjectionMatrix = perspectiveMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_MultipleRenderTargets_RTT_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(pep_MultipleRenderTargets_RTT_Square_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return;
}

////////////////////////////////////////////////////////////////////////////////////
// GLOBAL FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////
int MultipleRenderTargets_Initialize(int width, int height)
{
    fprintf(pep_gpFile, "\nMultipleRenderTargets\n");

    if (-1 == MultipleRenderTargets_FBO_Initialize(width, height))
    {
        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - MultipleRenderTargets_FBO_Initialize: \n", __DATE__, __TIME__, __FILE__, __LINE__);

        return -1;
    }

    if (-1 == MultipleRenderTargets_RenderToTexture_Initialize())
    {
        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - MultipleRenderTargets_RenderToTexture_Initialize: \n", __DATE__, __TIME__, __FILE__, __LINE__);

        return -1;
    }

    pep_MultipleRenderTargets_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;"

        "uniform mat4 u_mvp_matrix;" \

        "out vec2 out_texcoord;"

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_texcoord = vTexCoord;"
        "}";

    glShaderSource(pep_MultipleRenderTargets_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_MultipleRenderTargets_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_MultipleRenderTargets_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_MultipleRenderTargets_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_MultipleRenderTargets_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_MultipleRenderTargets_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \
        "uniform sampler2D u_sampler;" \

        "out vec4 FragColor;"

        "void main(void)" \
        "{" \
            "FragColor = texture(u_sampler, out_texcoord);" \
        "}";

    glShaderSource(pep_MultipleRenderTargets_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_MultipleRenderTargets_gFragmentShaderObject);

    glGetShaderiv(pep_MultipleRenderTargets_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_MultipleRenderTargets_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_MultipleRenderTargets_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_MultipleRenderTargets_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_MultipleRenderTargets_gShaderProgramObject, pep_MultipleRenderTargets_gVertexShaderObject);
    glAttachShader(pep_MultipleRenderTargets_gShaderProgramObject, pep_MultipleRenderTargets_gFragmentShaderObject);

    glBindAttribLocation(pep_MultipleRenderTargets_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_MultipleRenderTargets_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_MultipleRenderTargets_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_MultipleRenderTargets_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_MultipleRenderTargets_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_MultipleRenderTargets_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_MultipleRenderTargets_mvpUniform = glGetUniformLocation(pep_MultipleRenderTargets_gShaderProgramObject, "u_mvp_matrix");
    pep_MultipleRenderTargets_samplerUniform = glGetUniformLocation(pep_MultipleRenderTargets_gShaderProgramObject, "u_sampler");

    //
    // Square
    //
    const GLfloat squareVertices[] = {1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
    const GLfloat squaretexcoords[] = {
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f};

    glGenVertexArrays(1, &pep_MultipleRenderTargets_Square_vao);
    glBindVertexArray(pep_MultipleRenderTargets_Square_vao);

    glGenBuffers(1, &pep_MultipleRenderTargets_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_MultipleRenderTargets_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_MultipleRenderTargets_Square_vbo_texcoords);
    glBindBuffer(GL_ARRAY_BUFFER, pep_MultipleRenderTargets_Square_vbo_texcoords);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squaretexcoords), squaretexcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
	return 0;
}

void MultipleRenderTargets_Display(int pep_gToggleFboColorAttachement)
{
    // Render

    MultipleRenderTargets_RenderToTexture_Display();

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pep_MultipleRenderTargets_Fbo_TextureBuffer[pep_gToggleFboColorAttachement]);
    glUseProgram(pep_MultipleRenderTargets_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.50f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_MultipleRenderTargets_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1i(pep_MultipleRenderTargets_samplerUniform, 0);

    glBindVertexArray(pep_MultipleRenderTargets_Square_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    glUseProgram(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    return;
}

void MultipleRenderTargets_Update(void)
{
    return;
}

void MultipleRenderTargets_ReSize(int width, int height)
{
    MultipleRenderTargets_FBO_ReSize(width, height);

    return;
}

void MultipleRenderTargets_Uninitialize(void)
{
    MultipleRenderTargets_FBO_Uinitialize();
    MultipleRenderTargets_RenderToTexture_Uninitialize();

    if (pep_MultipleRenderTargets_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_MultipleRenderTargets_gShaderProgramObject);

        glGetProgramiv(pep_MultipleRenderTargets_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_MultipleRenderTargets_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_MultipleRenderTargets_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_MultipleRenderTargets_gShaderProgramObject);
        pep_MultipleRenderTargets_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

