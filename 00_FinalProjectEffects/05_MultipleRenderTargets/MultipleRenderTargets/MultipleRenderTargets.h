#pragma once

int MultipleRenderTargets_Initialize(int width, int height);
void MultipleRenderTargets_Display(int);
void MultipleRenderTargets_Update(void);
void MultipleRenderTargets_ReSize(int width, int height);
void MultipleRenderTargets_Uninitialize(void);
