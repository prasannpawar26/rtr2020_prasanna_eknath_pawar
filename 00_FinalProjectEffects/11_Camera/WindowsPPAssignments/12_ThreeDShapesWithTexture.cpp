#include "Common.h"
#include "12_ThreeDShapesWithTexture.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

extern Camera camera;
extern mat4 cameraMatrix;
extern vmath::vec3 cameraPosition;
extern vmath::vec3 cameraFront;

GLuint pep_ThreeDShapesWithTexture_gVertexShaderObject;
GLuint pep_ThreeDShapesWithTexture_gFragmentShaderObject;
GLuint pep_ThreeDShapesWithTexture_gShaderProgramObject;

GLuint pep_ThreeDShapesWithTexture_Pyramid_vao;
GLuint pep_ThreeDShapesWithTexture_Pyramid_vbo_position;
GLuint pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord;
GLfloat pep_ThreeDShapesWithTexture_PyramidRotation;
GLuint pep_ThreeDShapesWithTexture_TextureStone;

GLuint pep_ThreeDShapesWithTexture_Cube_vao;
GLuint pep_ThreeDShapesWithTexture_Cube_vbo_position;
GLuint pep_ThreeDShapesWithTexture_Cube_vbo_texcoord;
GLfloat pep_ThreeDShapesWithTexture_CubeRotation;
GLuint pep_ThreeDShapesWithTexture_TextureKundali;

GLuint pep_ThreeDShapesWithTexture_SamplerUniform;
GLuint pep_ThreeDShapesWithTexture_modelMatrixUniform;
GLuint pep_ThreeDShapesWithTexture_viewMatrixUniform;
GLuint pep_ThreeDShapesWithTexture_projectionMatrixUniform;

int ThreeDShapesWithTexture_Initialize(void)
{
    fprintf(pep_gpFile, "\nThreeDShapesWithTexture\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_ThreeDShapesWithTexture_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \

        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \

        "out vec2 out_texcoord;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix* vPosition;" \
            "out_texcoord = vTexCoord;" \
        "}";

    glShaderSource(pep_ThreeDShapesWithTexture_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_ThreeDShapesWithTexture_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_ThreeDShapesWithTexture_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ThreeDShapesWithTexture_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ThreeDShapesWithTexture_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_ThreeDShapesWithTexture_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \
        
        "uniform sampler2D u_sampler;" \
        
        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = texture(u_sampler, out_texcoord);" \
        "}";

    glShaderSource(pep_ThreeDShapesWithTexture_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_ThreeDShapesWithTexture_gFragmentShaderObject);

    glGetShaderiv(pep_ThreeDShapesWithTexture_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_ThreeDShapesWithTexture_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_ThreeDShapesWithTexture_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ThreeDShapesWithTexture_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_ThreeDShapesWithTexture_gShaderProgramObject, pep_ThreeDShapesWithTexture_gVertexShaderObject);
    glAttachShader(pep_ThreeDShapesWithTexture_gShaderProgramObject, pep_ThreeDShapesWithTexture_gFragmentShaderObject);

    glBindAttribLocation(pep_ThreeDShapesWithTexture_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_ThreeDShapesWithTexture_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_ThreeDShapesWithTexture_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_ThreeDShapesWithTexture_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_ThreeDShapesWithTexture_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_ThreeDShapesWithTexture_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_ThreeDShapesWithTexture_modelMatrixUniform = glGetUniformLocation(pep_ThreeDShapesWithTexture_gShaderProgramObject, "u_model_matrix");
    pep_ThreeDShapesWithTexture_viewMatrixUniform = glGetUniformLocation(pep_ThreeDShapesWithTexture_gShaderProgramObject, "u_view_matrix");
    pep_ThreeDShapesWithTexture_projectionMatrixUniform = glGetUniformLocation(pep_ThreeDShapesWithTexture_gShaderProgramObject, "u_projection_matrix");
    pep_ThreeDShapesWithTexture_SamplerUniform = glGetUniformLocation(pep_ThreeDShapesWithTexture_gShaderProgramObject, "u_sampler");
    //
    // Pyramid
    //
    const GLfloat triangleVertices[] = {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };
    const GLfloat triangleTexcoord[] = {
        0.5, 1.0, // front-top
        0.0, 0.0, // front-left
        1.0, 0.0, // front-right

        0.5, 1.0, // right-top
        1.0, 0.0, // right-left
        0.0, 0.0, // right-right

        0.5, 1.0, // back-top
        1.0, 0.0, // back-left
        0.0, 0.0, // back-right

        0.5, 1.0, // left-top
        0.0, 0.0, // left-left
        1.0, 0.0, // left-right
    };

    glGenVertexArrays(1, &pep_ThreeDShapesWithTexture_Pyramid_vao);
    glBindVertexArray(pep_ThreeDShapesWithTexture_Pyramid_vao);

    glGenBuffers(1, &pep_ThreeDShapesWithTexture_Pyramid_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Pyramid_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleTexcoord), triangleTexcoord, GL_STATIC_DRAW);
   /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //
    // Cube
    //
    const GLfloat squareVertices[] = {
        // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,  

        // bottom
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,

        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // back
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, -1.0f, -1.0f, 
        -1.0f, -1.0f, 1.0f, 
    };
    const GLfloat squareTexCoord[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    glGenVertexArrays(1, &pep_ThreeDShapesWithTexture_Cube_vao);
    glBindVertexArray(pep_ThreeDShapesWithTexture_Cube_vao);

    glGenBuffers(1, &pep_ThreeDShapesWithTexture_Cube_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Cube_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_ThreeDShapesWithTexture_Cube_vbo_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Cube_vbo_texcoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareTexCoord), squareTexCoord, GL_STATIC_DRAW);
    /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    ThreeDShapesWithTexture_LoadTexture(&pep_ThreeDShapesWithTexture_TextureStone, MAKEINTRESOURCEA(THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_STONE));
    ThreeDShapesWithTexture_LoadTexture(&pep_ThreeDShapesWithTexture_TextureKundali, MAKEINTRESOURCEA(THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_VIJAY_KUNDALI));

    return 0;
}

void ThreeDShapesWithTexture_Display(void)
{
    // update cameraMatrix
    cameraMatrix		= camera.GetViewMatrix();
    //camera.Position	+= camera.Front * 0.5f;
    cameraPosition		= camera.GetCameraPosition();
    cameraFront			= camera.GetCameraFront();

    glEnable(GL_TEXTURE_2D);

    // Render
    glUseProgram(pep_ThreeDShapesWithTexture_gShaderProgramObject);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_ThreeDShapesWithTexture_SamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_ThreeDShapesWithTexture_TextureStone);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;

    //
    // Pyramid
    //
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    modelMatrix = translate(-2.0f, 0.0f, -16.0f) * rotate(pep_ThreeDShapesWithTexture_PyramidRotation, 0.0f, 1.0f, 0.0f);
    viewMatrix = cameraMatrix;
    projectionMatrix = pep_Perspective_ProjectionMatrix;

    glUniformMatrix4fv(pep_ThreeDShapesWithTexture_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_ThreeDShapesWithTexture_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_ThreeDShapesWithTexture_projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);

    glBindVertexArray(pep_ThreeDShapesWithTexture_Pyramid_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Pyramid_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    //
    // Cube
    //

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_ThreeDShapesWithTexture_SamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_ThreeDShapesWithTexture_TextureKundali);

    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    modelMatrix = translate(2.0f, 0.0f, -16.0f) * rotate(pep_ThreeDShapesWithTexture_PyramidRotation, 1.0f, 0.0f, 0.0f) * rotate(pep_ThreeDShapesWithTexture_PyramidRotation, 0.0f, 1.0f, 0.0f) * rotate(pep_ThreeDShapesWithTexture_PyramidRotation, 0.0f, 0.0f, 1.0f) * scale(0.75f, 0.75f, 0.75f);
    viewMatrix = cameraMatrix;
    projectionMatrix = pep_Perspective_ProjectionMatrix;

    glUniformMatrix4fv(pep_ThreeDShapesWithTexture_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_ThreeDShapesWithTexture_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_ThreeDShapesWithTexture_projectionMatrixUniform, 1, GL_FALSE, projectionMatrix);


    glBindVertexArray(pep_ThreeDShapesWithTexture_Cube_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Cube_vbo_texcoord);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_ThreeDShapesWithTexture_Cube_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_TEXTURE_2D);

    return;
}

void ThreeDShapesWithTexture_Update(void)
{
    pep_ThreeDShapesWithTexture_PyramidRotation += 0.5f;
    if (pep_ThreeDShapesWithTexture_PyramidRotation > 360.0f)
    {
        pep_ThreeDShapesWithTexture_PyramidRotation = 0.0f;
    }

    pep_ThreeDShapesWithTexture_CubeRotation += 0.5f;
    if (pep_ThreeDShapesWithTexture_CubeRotation > 360.0f)
    {
        pep_ThreeDShapesWithTexture_CubeRotation = 0.0f;
    }

    return;
}

void ThreeDShapesWithTexture_ReSize(int width, int height)
{

}

void ThreeDShapesWithTexture_Uninitialize(void)
{
    if (pep_ThreeDShapesWithTexture_Cube_vbo_texcoord)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesWithTexture_Cube_vbo_texcoord);
        pep_ThreeDShapesWithTexture_Cube_vbo_texcoord = 0;
    }

    if (pep_ThreeDShapesWithTexture_Cube_vbo_position)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesWithTexture_Cube_vbo_position);
        pep_ThreeDShapesWithTexture_Cube_vbo_position = 0;
    }

    if (pep_ThreeDShapesWithTexture_Cube_vao)
    {
        glDeleteVertexArrays(1, &pep_ThreeDShapesWithTexture_Cube_vao);
        pep_ThreeDShapesWithTexture_Cube_vao = 0;
    }

    if (pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord);
        pep_ThreeDShapesWithTexture_Pyramid_vbo_texcoord = 0;
    }

    if (pep_ThreeDShapesWithTexture_Pyramid_vbo_position)
    {
        glDeleteBuffers(1, &pep_ThreeDShapesWithTexture_Pyramid_vbo_position);
        pep_ThreeDShapesWithTexture_Pyramid_vbo_position = 0;
    }

    if (pep_ThreeDShapesWithTexture_Pyramid_vao)
    {
        glDeleteVertexArrays(1, &pep_ThreeDShapesWithTexture_Pyramid_vao);
        pep_ThreeDShapesWithTexture_Pyramid_vao = 0;
    }

    if (pep_ThreeDShapesWithTexture_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_ThreeDShapesWithTexture_gShaderProgramObject);

        glGetProgramiv(pep_ThreeDShapesWithTexture_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_ThreeDShapesWithTexture_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_ThreeDShapesWithTexture_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_ThreeDShapesWithTexture_gShaderProgramObject);
        pep_ThreeDShapesWithTexture_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

BOOL ThreeDShapesWithTexture_LoadTexture(GLuint *texture, CHAR imageResourceID[])
{
    // variable declarations
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;

    // code
    hBitmap = (HBITMAP)LoadImageA(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (NULL == hBitmap)
    {
        return bStatus;
    }

    bStatus = TRUE;

    GetObject(hBitmap, sizeof(bmp), &bmp);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    DeleteObject(hBitmap);

    return bStatus;

}