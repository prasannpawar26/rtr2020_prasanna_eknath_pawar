#include "Common.h"
#include "BirdModel.h"

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include "StbTexture.h"

#pragma comment(lib, "assimp-vc142-mtd.lib")

using namespace std;

//
// types for parsing model
//
// vertex data of animated model
struct BIRD_MODEL_VERTEX 
{
	vec3 position;
	vec3 normal;
	vec2 uv;
	ivec4 boneIDs = {0, 0, 0, 0};
	vec4 boneWeights = {0.0f, 0.0f, 0.0f, 0.0f};
};

// struct to hold bone tree i.e. birdModel_gSkeleton
struct BIRD_MODEL_BONE
{
	int id = 0;
	string name = "";
	mat4 offset = mat4::identity();
	vector<BIRD_MODEL_BONE> child = {};
};

// struct to represent an birdModel_gAnimationInfo track
struct BIRD_MODEL_BONETRANSFORMTRACK
{
	vector<float> positionTimestamps = {};
	vector<float> rotationTimestamps = {};
	vector<float> scaleTimestamps = {};

	vector<vec3> positions = {};
	vector<vec4> rotations = {};
	vector<vec3> scales = {};
};

// struct to contain the birdModel_gAnimationInfo information
struct BIRD_MODEL_ANIMATIONINFO
{
	float duration = 0.0f;
	float ticksPerSecond = 1.0f;
	unordered_map<string, BIRD_MODEL_BONETRANSFORMTRACK> boneTransform = {};
};

//////////////////////////////////////////////////////////////////
extern FILE* pep_gpFile;
extern int pep_gWidth;;
extern int pep_gHeight;
extern mat4 pep_Perspective_ProjectionMatrix;

extern Camera camera;
extern mat4 cameraMatrix;
extern vmath::vec3 cameraPosition;
extern vmath::vec3 cameraFront;

//////////////////////////////////////////////////////////////////

// model related global data
unsigned int birdModel_gBoneCount = 0;
BIRD_MODEL_ANIMATIONINFO birdModel_gAnimationInfo;
BIRD_MODEL_BONE birdModel_gSkeleton;
mat4 birdModel_globalInverseTransform;
unsigned int birdModel_gNumElements;

bool bLight = true;

//float birdModel_gLightAmbient[4] = { 0.2f, 0.2f, 0.2f, 1.0f };
//float birdModel_gLightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
//float birdModel_gLightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
//float birdModel_gLightPosition[4] = { 0.0f, 0.0f, -5.0f, 1.0f };
//
//float birdModel_gMaterialAmbient[4] = { 0.2f, 0.2f, 0.2f, 1.0f };
//float birdModel_gMaterialDiffuse[4] = { 0.8f, 0.8f, 0.8f, 0.8f };
//float birdModel_gMaterialSpecular[4] = { 0.8f, 0.8f, 0.8f, 0.8f };
//float birdModel_gMaterialShininess = 128.0f;

GLuint pep_birdModel_gVertexShaderObject;
GLuint pep_birdModel_gFragmentShaderObject;
GLuint pep_birdModel_gShaderProgramObject;

GLuint pep_birdModel_pVao;
GLuint pep_birdModel_pVbo_position;
GLuint pep_birdModel_pVbo_normal;
GLuint pep_birdModel_pVbo_texcoord;
GLuint pep_birdModel_pVbo_boneids;
GLuint pep_birdModel_pVbo_boneweights;
GLuint pep_birdModel_pVbo_elements;

GLuint pep_birdModel_modelUniform;
GLuint pep_birdModel_viewUniform;
GLuint pep_birdModel_projectionUniform;
GLuint pep_birdModel_boneMatrixUniform[100];

GLuint pep_birdModel_SamplerDiffuseUniform;

//GLuint pep_birdModel_lightAmbientUniform;
//GLuint pep_birdModel_lightDiffuseUniform;
//GLuint pep_birdModel_lightSpecularUniform;
//GLuint pep_birdModel_lightPositionUniform;
//
//GLuint pep_birdModel_materialAmbientUniform;
//GLuint pep_birdModel_materialDiffuseUniform;
//GLuint pep_birdModel_materialSpecularUniform;
//GLuint pep_birdModel_materialShinessUniform;

GLuint pep_birdModel_Texture;
mat4 AssimpToXMMATRIX(aiMatrix4x4 m)
{
	return mat4(
		vec4(m.a1, m.b1, m.c1, m.d1),
		vec4(m.a2, m.b2, m.c2, m.d2),
		vec4(m.a3, m.b3, m.c3, m.d3),
		vec4(m.a4, m.b4, m.c4, m.d4)
	);
}
mat4 assimpTomat4(aiMatrix4x4 m)
{
	return mat4(
		vec4(m.a1, m.b1, m.c1, m.d1),
		vec4(m.a2, m.b2, m.c2, m.d2),
		vec4(m.a3, m.b3, m.c3, m.d3),
		vec4(m.a4, m.b4, m.c4, m.d4)
	);
}

vec3 AssimpToXMFLOAT3(aiVector3D v)
{
	return vec3(v.x, v.y, v.z);
}

vec4 AssimpToXMVECTOR(aiQuaternion q)
{
	return vec4(q.x, q.y, q.z, q.w);
}

vec3 Lerp(vec3 a, vec3 b, float t)
{
	return vec3(((1.0f - t) * a) + (b * t));
}

// a recursive function to read all bones and form birdModel_gSkeleton
bool ReadSkeleton(BIRD_MODEL_BONE *boneOutput, aiNode *node, unordered_map<string, pair<int, mat4>> *boneInfoTable)
{
	// check if the node is bone
	if (boneInfoTable->find(node->mName.C_Str()) != boneInfoTable->end())
	{
		boneOutput->name = node->mName.C_Str();
		boneOutput->id = (*boneInfoTable)[boneOutput->name].first;
		boneOutput->offset = (*boneInfoTable)[boneOutput->name].second;

		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			BIRD_MODEL_BONE child;
			ReadSkeleton(&child, node->mChildren[i], boneInfoTable);
			boneOutput->child.push_back(child);
		}
		return true;
	}
	else // find bones in childer
	{
		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			if (ReadSkeleton(boneOutput, node->mChildren[i], boneInfoTable))
			{
				return true;
			}
		}
	}
	return false;
}

void LoadModel(const aiScene *scene, aiMesh *mesh, vector<BIRD_MODEL_VERTEX> *verticesOutput, vector<unsigned int> *indicesOutput, BIRD_MODEL_BONE *skeletonOutput, unsigned int *nBoneCount)
{
	*verticesOutput = {};
	*indicesOutput = {};

	// load position, normal, texcoord
	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		// process position
		BIRD_MODEL_VERTEX vertex;
		vec3 vec;
		vec[0] = mesh->mVertices[i][0];
		vec[1] = mesh->mVertices[i][1];
		vec[2] = mesh->mVertices[i][2];
		vertex.position = vec;

		// process normal
		vec[0] = mesh->mNormals[i][0];
		vec[1] = mesh->mNormals[i][1];
		vec[2] = mesh->mNormals[i][2];
		vertex.normal = vec;

		// process uv
		/*if (mesh->mTextureCoords[0])
		{
			vec2 v;
			v[0] = mesh->mTextureCoords[0][i][0];
			v[0] = mesh->mTextureCoords[0][i][1];
			vertex.uv = v;
		}
		else {
			vertex.uv = { 0.0,0.0 };
		}*/
		vec2 v;
		v[0] = mesh->mTextureCoords[0][i][0];
		v[0] = mesh->mTextureCoords[0][i][1];
		vertex.uv = v;

		vertex.boneIDs = ivec4(0, 0, 0, 0);
		vertex.boneWeights = vec4(0.0f, 0.0f, 0.0f, 0.0f);

		verticesOutput->push_back(vertex);
	}

	// load boneData to vertices
	unordered_map<string, pair<int, mat4>> boneInfo = {};
	vector<unsigned int> boneCounts;
	boneCounts.resize(verticesOutput->size(), 0);
	*nBoneCount = mesh->mNumBones;

	// loop through each bone
	for (unsigned int i = 0; i < *nBoneCount; i++)
	{
		aiBone *bone = mesh->mBones[i];
		mat4 m = assimpTomat4(bone->mOffsetMatrix);
		boneInfo[bone->mName.C_Str()] = { i, m };

		// loop through each vertex that have that bone
		for (unsigned int j = 0; j < bone->mNumWeights; j++)
		{
			unsigned int id = bone->mWeights[j].mVertexId;
			float weight = bone->mWeights[j].mWeight;
			boneCounts[id]++;

			switch (boneCounts[id])
			{
			case 1:
				(*verticesOutput)[id].boneIDs[0] = i;
				(*verticesOutput)[id].boneWeights[0] = weight;
				break;

			case 2:
				(*verticesOutput)[id].boneIDs[1] = i;
				(*verticesOutput)[id].boneWeights[1] = weight;
				break;

			case 3:
				(*verticesOutput)[id].boneIDs[2] = i;
				(*verticesOutput)[id].boneWeights[2] = weight;
				break;

			case 4:
				(*verticesOutput)[id].boneIDs[3] = i;
				(*verticesOutput)[id].boneWeights[3] = weight;
				break;

			default:
				break;
			}
		}
	}

	// normalize weights to make all weights sum 1
	for (unsigned int i = 0; i < verticesOutput->size(); i++)
	{
		vec4 &boneWeights = (*verticesOutput)[i].boneWeights;
		float totalWeight = boneWeights[0] + boneWeights[1] + boneWeights[2] + boneWeights[3];
		if (totalWeight > 0.0f) 
		{
			(*verticesOutput)[i].boneWeights[0] /= totalWeight;
			(*verticesOutput)[i].boneWeights[1] /= totalWeight;
			(*verticesOutput)[i].boneWeights[2] /= totalWeight;
			(*verticesOutput)[i].boneWeights[3] /= totalWeight;
		}
	}

	// load indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace &face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++)
			indicesOutput->push_back(face.mIndices[j]);
	}

	// create bone tree
	ReadSkeleton(skeletonOutput, scene->mRootNode, &boneInfo);

	return;
}

void LoadAnimation(const aiScene *scene, BIRD_MODEL_ANIMATIONINFO& birdModel_gAnimationInfo)
{
	// loading first birdModel_gAnimationInfo
	aiAnimation *anim = scene->mAnimations[0];

	if (anim->mTicksPerSecond != 0.0f)
		birdModel_gAnimationInfo.ticksPerSecond = (float)anim->mTicksPerSecond;
	else
		birdModel_gAnimationInfo.ticksPerSecond = 1;

	birdModel_gAnimationInfo.duration = (float)anim->mDuration * (float)anim->mTicksPerSecond;
	birdModel_gAnimationInfo.boneTransform = {};

	// load position, rotation and scale for each bone
	// each channel represents each bone
	for (unsigned int i = 0; i < anim->mNumChannels; i++)
	{
		aiNodeAnim *channel = anim->mChannels[i];
		BIRD_MODEL_BONETRANSFORMTRACK track;

		for (unsigned int j = 0; j < channel->mNumPositionKeys; j++)
		{
			track.positionTimestamps.push_back((float)channel->mPositionKeys[j].mTime);
			track.positions.push_back(AssimpToXMFLOAT3(channel->mPositionKeys[j].mValue));
		}

		for (unsigned int j = 0; j < channel->mNumRotationKeys; j++)
		{
			track.rotationTimestamps.push_back((float)channel->mRotationKeys[j].mTime);
			track.rotations.push_back(AssimpToXMVECTOR(channel->mRotationKeys[j].mValue));
		}

		for (unsigned int j = 0; j < channel->mNumScalingKeys; j++)
		{
			track.scaleTimestamps.push_back((float)channel->mScalingKeys[j].mTime);
			track.scales.push_back(AssimpToXMFLOAT3(channel->mScalingKeys[j].mValue));
		}

		birdModel_gAnimationInfo.boneTransform[channel->mNodeName.C_Str()] = track;		
	}
}

pair<unsigned int, float> GetTimeFraction(vector<float>& times, float& dt)
{
	unsigned int segment = 0;
	while (dt > times[segment])
		segment++;

	float start = times[segment - 1];
	float end = times[segment];
	float frac = (dt - start) / (end - start);
	return {segment, frac};
}

vec4 quatSlerp(vec4 q1, vec4 q2, float t)
{
	float cosom = q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3];

	if (cosom < 0.0f)
	{
		cosom = -cosom;
		q2 *= -1.0f;
	}

	float sclq1, sclq2;
	if ((1.0f - cosom) > 0.0001f)
	{
		// standard case (slerp)
		float omega, sinom;
		omega = acosf(cosom);
		sinom = sinf(omega);
		sclq1 = sin((1.0f - t) * omega) / sinom;
		sclq2 = sin(t * omega) / sinom;
	}
	else
	{
		// very close, perform linear interpolation
		sclq1 = 1.0f - t;
		sclq2 = t;
	}

	return (sclq1 * q1) + (sclq2 * q2);
}

mat4 affineTransform(vec3 scale, quaternion rotate, vec3 translate)
{
	mat4 m = rotate.asMatrix();

	m[0][0] *= scale[0];
	m[1][0] *= scale[0];
	m[2][0] *= scale[0];
	m[3][0] = translate[0];

	m[0][1] *= scale[1];
	m[1][1] *= scale[1];
	m[2][1] *= scale[1];
	m[3][1] = translate[1];

	m[0][2] *= scale[2];
	m[1][2] *= scale[2];
	m[2][2] *= scale[2];
	m[3][2] = translate[2];

	return m;
}

void GetPose(BIRD_MODEL_ANIMATIONINFO& birdModel_gAnimationInfo, BIRD_MODEL_BONE& birdModel_gSkeleton, float dt, vector<mat4>& output, mat4 &parentTransform, mat4 &globalInverseTransform)
{
	BIRD_MODEL_BONETRANSFORMTRACK &btt = birdModel_gAnimationInfo.boneTransform[birdModel_gSkeleton.name];

	// timestamp for which pose is required
	dt = fmod(dt, birdModel_gAnimationInfo.duration);
	pair<unsigned int, float> fp;

	// calculate interpolated position
	fp = GetTimeFraction(btt.positionTimestamps, dt);
	vec3 position1 = btt.positions[fp.first - 1];
	vec3 position2 = btt.positions[fp.first];
	vec3 position = Lerp(position1, position2, fp.second);

	// calculate interpolated rotation
	fp = GetTimeFraction(btt.rotationTimestamps, dt);
	vec4 rotation1 = btt.rotations[fp.first - 1];
	vec4 rotation2 = btt.rotations[fp.first];
	vec4 rotation = quatSlerp(rotation1, rotation2, fp.second);

	// calculate interpolated scale
	fp = GetTimeFraction(btt.scaleTimestamps, dt);
	vec3 scale1 = btt.scales[fp.first - 1];
	vec3 scale2 = btt.scales[fp.first];
	vec3 scale = Lerp(scale1, scale2, fp.second);

	vec4 zero = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	mat4 localTransform = affineTransform(scale, vmath::quaternion(rotation), position);

	mat4 globalTransform = parentTransform * localTransform; //correct
	output[birdModel_gSkeleton.id] =  ((mat4)birdModel_globalInverseTransform) * ((mat4)globalTransform) * ((mat4)birdModel_gSkeleton.offset); //correct

	// update value for child bones
	for (BIRD_MODEL_BONE& child: birdModel_gSkeleton.child)
	{
		if (child.name != "")
		{
			GetPose(birdModel_gAnimationInfo, child, dt, output, globalTransform, birdModel_globalInverseTransform);
		}
	}
}

HRESULT BirdModel_Initialize(void)
{
	
	// variables
	HRESULT hr = S_OK;

	// code
	// initialize shaders, input layouts, constant buffers etc..

	//// vertex shader ////////////////////////////////////////////////////////////
	pep_birdModel_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const char* vertexShaderSourceCode =
		"#version 450 core"
		"\n" \

		"layout (location = 0) in vec3 vPosition;\n" \
		"layout (location = 1) in vec3 vNormal;\n" \
		"layout (location = 2) in vec2 vTexCoord;\n" \
		"layout (location = 3) in ivec4 vBoneIds;\n" \
		"layout (location = 4) in vec4 vBoneWeights;\n" \

		"uniform mat4 u_model_matrix;\n" \
		"uniform mat4 u_view_matrix;\n" \
		"uniform mat4 u_projection_matrix;\n" \
		"uniform mat4 u_boneMatrix[100];\n" \

		"out vec2 out_texcoord;\n" \

		"void main(void)\n" \
		"{\n" \
			"mat4 boneTransform;\n" \

			"vec4 totalPosition = vec4(0.0f);"\
			"mat4 tempMat4;"
			"for(int i=0;i< 4 ;i++)"\
			"{"\
				"if(vBoneIds[i] == -1)"\
				"continue;"

				"if(vBoneIds[i] >= 100)"\
				"{"\
					"totalPosition = vec4(vPosition, 1.0) ;"\
					"break;"\
				"}"\

				"vec4 localPosition = u_boneMatrix[vBoneIds[i]] * vec4(vPosition, 1.0);"\
				"totalPosition += localPosition * vBoneWeights[i];"\
			"}"\

			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * totalPosition;\n" \

			/*	"boneTransform = u_boneMatrix[vBoneIds.x] * vBoneWeights.x;\n" \
			"boneTransform += u_boneMatrix[vBoneIds.y] * vBoneWeights.y;\n" \
			"boneTransform += u_boneMatrix[vBoneIds.z] * vBoneWeights.z;\n" \
			"boneTransform += u_boneMatrix[vBoneIds.w] * vBoneWeights.w;\n" \*/

			//"vec4 pos = vec4(vPosition.xyz, 1.0)\n;"

			//"vec4 pos = boneTransform * vec4(vPosition.xyz, 1.0)\n;"
		
			/*	"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * pos;\n" \*/

			"out_texcoord = vTexCoord;\n" \

		"}\n";

	glShaderSource(pep_birdModel_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
	glCompileShader(pep_birdModel_gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(pep_birdModel_gVertexShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus)
	{
		glGetShaderiv(pep_birdModel_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog)
			{
				GLsizei written;
				glGetShaderInfoLog(pep_birdModel_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}
	fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

	//// pixel shader /////////////////////////////////////////////////////////////
	pep_birdModel_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const char* fragmentShaderSourceCode =

		"#version 450 core"
		"\n" \
		"in vec2 out_texcoord;\n" \

		"uniform sampler2D u_sampler_diffuse;\n" \

		"out vec4 FragColor;\n" \

		"void main(void)\n" \
		"{\n" \
			"vec3 Diffuse = texture(u_sampler_diffuse, out_texcoord).rgb;\n" \

			"FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n" \

			"FragColor = vec4(Diffuse, 1.0);\n" \
		"}\n";

		glShaderSource(pep_birdModel_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
		glCompileShader(pep_birdModel_gFragmentShaderObject);

		glGetShaderiv(pep_birdModel_gFragmentShaderObject, GL_COMPILE_STATUS,
			&iShaderCompileStatus);
		if (GL_FALSE == iShaderCompileStatus)
		{
			glGetShaderiv(pep_birdModel_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (0 < iInfoLogLength)
			{
				szInfoLog = (GLchar *)malloc(iInfoLogLength);
				if (NULL != szInfoLog)
				{
					GLsizei written;
					glGetShaderInfoLog(pep_birdModel_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
					free(szInfoLog);
					return -1;
				}
			}
		}
		fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

	///////////////////////////////////////////////////////////////////////////////
		fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
			"sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

		pep_birdModel_gShaderProgramObject = glCreateProgram();
		glAttachShader(pep_birdModel_gShaderProgramObject, pep_birdModel_gVertexShaderObject);
		glAttachShader(pep_birdModel_gShaderProgramObject, pep_birdModel_gFragmentShaderObject);

		glBindAttribLocation(pep_birdModel_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
		glBindAttribLocation(pep_birdModel_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
		glBindAttribLocation(pep_birdModel_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");
		glBindAttribLocation(pep_birdModel_gShaderProgramObject, AMC_ATTRIBUTES_BONEIDS, "vBoneIds");
		glBindAttribLocation(pep_birdModel_gShaderProgramObject, AMC_ATTRIBUTES_BONEWEIGHTS, "vBoneWeights");

		fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
		glLinkProgram(pep_birdModel_gShaderProgramObject);

		GLint iProgramLinkStatus = 0;
		iInfoLogLength = 0;
		szInfoLog = NULL;

		glGetProgramiv(pep_birdModel_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
		if (GL_FALSE == iProgramLinkStatus)
		{
			glGetProgramiv(pep_birdModel_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
			if (0 < iInfoLogLength)
			{
				szInfoLog = (GLchar *)malloc(iInfoLogLength);
				if (NULL != szInfoLog)
				{
					GLsizei written;

					glGetProgramInfoLog(pep_birdModel_gShaderProgramObject, iInfoLogLength, &written,
						szInfoLog);

					fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

					free(szInfoLog);
					return -1;
				}
			}
		}

		fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

		/*Uniforms*/
		{
			pep_birdModel_modelUniform = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_model_matrix");
			pep_birdModel_viewUniform = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_view_matrix");
			pep_birdModel_projectionUniform = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_projection_matrix");

			pep_birdModel_SamplerDiffuseUniform = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_sampler_diffuse");

			pep_birdModel_boneMatrixUniform[0] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[0]");
			pep_birdModel_boneMatrixUniform[1] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[1]");
			pep_birdModel_boneMatrixUniform[2] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[2]");
			pep_birdModel_boneMatrixUniform[3] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[3]");
			pep_birdModel_boneMatrixUniform[4] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[4]");
			pep_birdModel_boneMatrixUniform[5] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[5]");
			pep_birdModel_boneMatrixUniform[6] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[6]");
			pep_birdModel_boneMatrixUniform[7] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[7]");
			pep_birdModel_boneMatrixUniform[8] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[8]");
			pep_birdModel_boneMatrixUniform[9] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[9]");
			pep_birdModel_boneMatrixUniform[10] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[10]");
			pep_birdModel_boneMatrixUniform[11] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[11]");
			pep_birdModel_boneMatrixUniform[12] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[12]");
			pep_birdModel_boneMatrixUniform[13] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[13]");
			pep_birdModel_boneMatrixUniform[14] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[14]");
			pep_birdModel_boneMatrixUniform[15] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[15]");
			pep_birdModel_boneMatrixUniform[16] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[16]");
			pep_birdModel_boneMatrixUniform[17] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[17]");
			pep_birdModel_boneMatrixUniform[18] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[18]");
			pep_birdModel_boneMatrixUniform[19] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[19]");
			pep_birdModel_boneMatrixUniform[20] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[20]");
			pep_birdModel_boneMatrixUniform[21] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[21]");
			pep_birdModel_boneMatrixUniform[22] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[22]");
			pep_birdModel_boneMatrixUniform[23] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[23]");
			pep_birdModel_boneMatrixUniform[24] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[24]");
			pep_birdModel_boneMatrixUniform[25] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[25]");
			pep_birdModel_boneMatrixUniform[26] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[26]");
			pep_birdModel_boneMatrixUniform[27] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[27]");
			pep_birdModel_boneMatrixUniform[28] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[28]");
			pep_birdModel_boneMatrixUniform[29] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[29]");
			pep_birdModel_boneMatrixUniform[30] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[30]");
			pep_birdModel_boneMatrixUniform[31] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[31]");
			pep_birdModel_boneMatrixUniform[32] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[32]");
			pep_birdModel_boneMatrixUniform[33] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[33]");
			pep_birdModel_boneMatrixUniform[34] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[34]");
			pep_birdModel_boneMatrixUniform[35] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[35]");
			pep_birdModel_boneMatrixUniform[36] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[36]");
			pep_birdModel_boneMatrixUniform[37] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[37]");
			pep_birdModel_boneMatrixUniform[38] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[38]");
			pep_birdModel_boneMatrixUniform[39] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[39]");
			pep_birdModel_boneMatrixUniform[40] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[40]");
			pep_birdModel_boneMatrixUniform[41] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[41]");
			pep_birdModel_boneMatrixUniform[42] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[42]");
			pep_birdModel_boneMatrixUniform[43] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[43]");
			pep_birdModel_boneMatrixUniform[44] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[44]");
			pep_birdModel_boneMatrixUniform[45] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[45]");
			pep_birdModel_boneMatrixUniform[46] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[46]");
			pep_birdModel_boneMatrixUniform[47] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[47]");
			pep_birdModel_boneMatrixUniform[48] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[48]");
			pep_birdModel_boneMatrixUniform[49] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[49]");
			pep_birdModel_boneMatrixUniform[50] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[50]");
			pep_birdModel_boneMatrixUniform[51] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[51]");
			pep_birdModel_boneMatrixUniform[52] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[52]");
			pep_birdModel_boneMatrixUniform[53] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[53]");
			pep_birdModel_boneMatrixUniform[54] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[54]");
			pep_birdModel_boneMatrixUniform[55] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[55]");
			pep_birdModel_boneMatrixUniform[56] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[56]");
			pep_birdModel_boneMatrixUniform[57] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[57]");
			pep_birdModel_boneMatrixUniform[58] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[58]");
			pep_birdModel_boneMatrixUniform[59] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[59]");
			pep_birdModel_boneMatrixUniform[60] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[60]");
			pep_birdModel_boneMatrixUniform[61] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[61]");
			pep_birdModel_boneMatrixUniform[62] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[62]");
			pep_birdModel_boneMatrixUniform[63] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[63]");
			pep_birdModel_boneMatrixUniform[64] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[64]");
			pep_birdModel_boneMatrixUniform[65] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[65]");
			pep_birdModel_boneMatrixUniform[66] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[66]");
			pep_birdModel_boneMatrixUniform[67] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[67]");
			pep_birdModel_boneMatrixUniform[68] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[68]");
			pep_birdModel_boneMatrixUniform[69] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[69]");
			pep_birdModel_boneMatrixUniform[70] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[70]");
			pep_birdModel_boneMatrixUniform[71] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[71]");
			pep_birdModel_boneMatrixUniform[72] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[72]");
			pep_birdModel_boneMatrixUniform[73] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[73]");
			pep_birdModel_boneMatrixUniform[74] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[74]");
			pep_birdModel_boneMatrixUniform[75] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[75]");
			pep_birdModel_boneMatrixUniform[76] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[76]");
			pep_birdModel_boneMatrixUniform[77] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[77]");
			pep_birdModel_boneMatrixUniform[78] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[78]");
			pep_birdModel_boneMatrixUniform[79] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[79]");
			pep_birdModel_boneMatrixUniform[80] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[80]");
			pep_birdModel_boneMatrixUniform[81] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[81]");
			pep_birdModel_boneMatrixUniform[82] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[82]");
			pep_birdModel_boneMatrixUniform[83] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[83]");
			pep_birdModel_boneMatrixUniform[84] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[84]");
			pep_birdModel_boneMatrixUniform[85] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[85]");
			pep_birdModel_boneMatrixUniform[86] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[86]");
			pep_birdModel_boneMatrixUniform[87] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[87]");
			pep_birdModel_boneMatrixUniform[88] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[88]");
			pep_birdModel_boneMatrixUniform[89] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[89]");
			pep_birdModel_boneMatrixUniform[90] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[90]");
			pep_birdModel_boneMatrixUniform[91] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[91]");
			pep_birdModel_boneMatrixUniform[92] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[92]");
			pep_birdModel_boneMatrixUniform[93] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[93]");
			pep_birdModel_boneMatrixUniform[94] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[94]");
			pep_birdModel_boneMatrixUniform[95] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[95]");
			pep_birdModel_boneMatrixUniform[96] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[96]");
			pep_birdModel_boneMatrixUniform[97] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[97]");
			pep_birdModel_boneMatrixUniform[98] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[98]");
			pep_birdModel_boneMatrixUniform[99] = glGetUniformLocation(pep_birdModel_gShaderProgramObject, "u_boneMatrix[99]");

		}

	//// vertex data from model ///////////////////////////////////////////////////

	Assimp::Importer importer;
	/*const char* filePath = "model/human.dae";*/
	const char* filePath = "model/eagle.dae";
	const aiScene* scene = importer.ReadFile(filePath,  aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices /*aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenSmoothNormals*/ /*| aiProcess_MakeLeftHanded*/ /*| aiProcess_FlipWindingOrder*/);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		fprintf_s(pep_gpFile, "Assimp Error: ");
		fprintf_s(pep_gpFile, "%s\n", importer.GetErrorString());
	}

	fprintf_s(pep_gpFile, "Assimp: file parsing done..\n");

	aiMesh* mesh = scene->mMeshes[0];

	vector<BIRD_MODEL_VERTEX> vertices = {};
	vector<unsigned int> indices = {};

	// inverse the global transformation matrix
	vec4 det;
//	birdModel_globalInverseTransform = AssimpToXMMATRIX(scene->mRootNode->mTransformation);
	birdModel_globalInverseTransform = assimpTomat4(scene->mRootNode->mTransformation.Inverse());

	LoadModel(scene, mesh, &vertices, &indices, &birdModel_gSkeleton, &birdModel_gBoneCount);
	//fopen_s(&pep_gpFile, "log.txt", "a+");
	fprintf_s(pep_gpFile, "Assimp: Model loading finished..\n");
	//fclose(pep_gpFile);

	LoadAnimation(scene, birdModel_gAnimationInfo);
	//fopen_s(&pep_gpFile, "log.txt", "a+");
	fprintf_s(pep_gpFile, "Assimp: BIRD_MODEL_ANIMATIONINFO loading finished..\n");
	//fclose(pep_gpFile);

	birdModel_gNumElements = (unsigned int)indices.size();

	/*Per Vertex Data*/
	{
		vector<vec3> position;
		vector<vec3> normal;
		vector<vec2> uv;
		vector<ivec4> boneIDs;
		vector<vec4> boneWeights;

		for (auto& it : vertices)
		{
			position.push_back(it.position);
			normal.push_back(it.normal);
			uv.push_back(it.uv);
			boneIDs.push_back(it.boneIDs);
			boneWeights.push_back(it.boneWeights);
		}

//		glGenVertexArrays(1, &pep_birdModel_pVao);
//		glBindVertexArray(pep_birdModel_pVao);
//
////		"layout (location = 0) in vec3 vPosition;\n" \
//
//		UINT size = (UINT)sizeof(vec3) * (UINT)position.size();
//		glGenBuffers(1, &pep_birdModel_pVbo_position);
//		glBindBuffer(GL_ARRAY_BUFFER, pep_birdModel_pVbo_position);
//		//glBufferData(GL_ARRAY_BUFFER, size, &position[0], GL_STATIC_DRAW);
//		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(struct BIRD_MODEL_VERTEX), &vertices[0], GL_STATIC_DRAW);
//		glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(struct BIRD_MODEL_VERTEX), (void*)0);
//		glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//
////		"layout (location = 1) in vec3 vNormal;\n" \
//			
//		size = (UINT)sizeof(vec3) * (UINT)normal.size();
//		glGenBuffers(1, &pep_birdModel_pVbo_normal);
//		glBindBuffer(GL_ARRAY_BUFFER, pep_birdModel_pVbo_normal);
//		//glBufferData(GL_ARRAY_BUFFER, size, &normal[0], GL_STATIC_DRAW);
//		glBufferData(GL_ARRAY_BUFFER, size, &normal[0], GL_STATIC_DRAW);
//		glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
//		glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//
////		"layout (location = 2) in vec2 vTexCoord;\n" \
//		
//		size = (UINT)sizeof(vec2) * (UINT)uv.size();
//		glGenBuffers(1, &pep_birdModel_pVbo_texcoord);
//		glBindBuffer(GL_ARRAY_BUFFER, pep_birdModel_pVbo_texcoord);
//		glBufferData(GL_ARRAY_BUFFER,size , &uv[0], GL_STATIC_DRAW);
//		//glBufferData(GL_ARRAY_BUFFER,size , &uv[0], GL_STATIC_DRAW);
//		glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
//		glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//
////		"layout (location = 3) in ivec4 vBoneIds;\n" \
//		
//		size = (UINT)sizeof(ivec4) * (UINT)boneIDs.size();
//		glGenBuffers(1, &pep_birdModel_pVbo_boneids);
//		glBindBuffer(GL_ARRAY_BUFFER, pep_birdModel_pVbo_boneids);
//		glBufferData(GL_ARRAY_BUFFER, size, &boneIDs[0], GL_STATIC_DRAW);
//		//glBufferData(GL_ARRAY_BUFFER, size, &boneIDs[0], GL_STATIC_DRAW);
//		glVertexAttribIPointer(AMC_ATTRIBUTES_BONEIDS, 4, GL_UNSIGNED_INT, /*GL_FALSE,*/ 0, NULL);
//		glEnableVertexAttribArray(AMC_ATTRIBUTES_BONEIDS);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//
////		"layout (location = 4) in vec4 vBoneWeights;\n" \
//
//		size = (UINT)sizeof(vec4) * (UINT)boneWeights.size();
//		glGenBuffers(1, &pep_birdModel_pVbo_boneweights);
//		glBindBuffer(GL_ARRAY_BUFFER, pep_birdModel_pVbo_boneweights);
//		glBufferData(GL_ARRAY_BUFFER, size, &boneWeights[0], GL_STATIC_DRAW);
//		//glBufferData(GL_ARRAY_BUFFER, size, &boneWeights[0], GL_STATIC_DRAW);
//		glVertexAttribPointer(AMC_ATTRIBUTES_BONEWEIGHTS, 4, GL_FLOAT, GL_FALSE, 0, NULL);
//		glEnableVertexAttribArray(AMC_ATTRIBUTES_BONEWEIGHTS);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//		size = (UINT)indices.size() * sizeof(unsigned int);
//		glGenBuffers(1, &pep_birdModel_pVbo_elements);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_birdModel_pVbo_elements);
//		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, &indices[0], GL_STATIC_DRAW);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//
//		glBindVertexArray(0);

		glGenVertexArrays(1, &pep_birdModel_pVao);
		glBindVertexArray(pep_birdModel_pVao);

		//		"layout (location = 0) in vec3 vPosition;\n" \

		glGenBuffers(1, &pep_birdModel_pVbo_position);
		glBindBuffer(GL_ARRAY_BUFFER, pep_birdModel_pVbo_position);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(struct BIRD_MODEL_VERTEX), &vertices[0], GL_STATIC_DRAW);

		glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(struct BIRD_MODEL_VERTEX), (void*)0);
		glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

		glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(struct BIRD_MODEL_VERTEX), (void*)offsetof(struct BIRD_MODEL_VERTEX, normal));
		glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);

		/*glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(struct BIRD_MODEL_VERTEX), (void*)offsetof(struct BIRD_MODEL_VERTEX, uv));
		glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);*/

		glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(struct BIRD_MODEL_VERTEX), (void*)offsetof(struct BIRD_MODEL_VERTEX, uv));
		glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);

		//BoneIDs
		glVertexAttribIPointer(AMC_ATTRIBUTES_BONEIDS, 4, GL_UNSIGNED_INT, sizeof(struct BIRD_MODEL_VERTEX), (void*)offsetof(struct BIRD_MODEL_VERTEX, boneIDs));
		glEnableVertexAttribArray(AMC_ATTRIBUTES_BONEIDS);

		//Weights
		glVertexAttribPointer(AMC_ATTRIBUTES_BONEWEIGHTS, 4, GL_FLOAT, GL_FALSE, sizeof(struct BIRD_MODEL_VERTEX), (void*)offsetof(struct BIRD_MODEL_VERTEX, boneWeights));
		glEnableVertexAttribArray(AMC_ATTRIBUTES_BONEWEIGHTS);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &pep_birdModel_pVbo_elements);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_birdModel_pVbo_elements);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (UINT)indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	}

	//// diffuse texture //////////////////////////////////////////////////////////

	LoadTextureSTB(&pep_birdModel_Texture, "model//eagle.jpg");

	return (hr);
}

void BirdModel_Display(void)
{
	// code
	cameraMatrix		= camera.GetViewMatrix();
	cameraPosition		= camera.GetCameraPosition();
	cameraFront		= camera.GetCameraFront();

	glEnable(GL_TEXTURE_2D);
	

	// translation is concerned with world matrix transformation
	mat4 identity = mat4::identity();
	mat4 worldMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 rotationMatrix1 = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	// translation
	static float deltaY = 0.0f;
	static float deltaZ = 0.0f;
	//translationMatrix = translate(0.0f, -0.50f+deltaY,  2.0f/* 15.0f*/-deltaZ);

	translationMatrix = translate(0.0f, -1.0f, -10.0f) * rotate(-110.0f, 1.0f, 1.0f, 0.0f);/** rotate(270.0f, 1.0f, 0.0f, 0.0f) *  rotate(270.0f, 0.0f, 1.0f, 0.0f)*/;
	//deltaY += 0.005f;
	//deltaZ += 0.001f;

	// this order of multiplication is important!
	worldMatrix = translationMatrix * scale(0.2f)/* * rotationMatrix * rotationMatrix1 * scaleMatrix*/;
	viewMatrix = cameraMatrix;

	// get the current pos
	vector<mat4> currentPose = {};
	currentPose.resize(birdModel_gBoneCount, identity);
	static float elapsedTime = 0.000005f;
	//static float elapsedTime = 1.0f;
	GetPose(birdModel_gAnimationInfo, birdModel_gSkeleton, elapsedTime, currentPose, identity, birdModel_globalInverseTransform);
	elapsedTime += 0.01f;

	glUseProgram(pep_birdModel_gShaderProgramObject);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pep_birdModel_Texture);

	glUniform1i(pep_birdModel_SamplerDiffuseUniform, 0);

	glUniformMatrix4fv(pep_birdModel_modelUniform, 1, GL_FALSE, worldMatrix);
	glUniformMatrix4fv(pep_birdModel_viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_birdModel_projectionUniform, 1, GL_FALSE, pep_Perspective_ProjectionMatrix);

	for (unsigned int i = 0; i < birdModel_gBoneCount; i++)
	{
		glUniformMatrix4fv(pep_birdModel_boneMatrixUniform[i], 1, GL_FALSE, currentPose[i]);
	}

	glBindVertexArray(pep_birdModel_pVao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_birdModel_pVbo_elements);
	glDrawElements(GL_TRIANGLES, birdModel_gNumElements, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	
}

void BirdModel_Update(void)
{

}

void BirdModel_Uninitialize(void)
{
	if (pep_birdModel_pVbo_elements) {
		glDeleteBuffers(1, &pep_birdModel_pVbo_elements);
		pep_birdModel_pVbo_elements = NULL;
	}

	if (pep_birdModel_pVbo_boneweights) {
		glDeleteBuffers(1, &pep_birdModel_pVbo_boneweights);
		pep_birdModel_pVbo_boneweights = NULL;
	}

	if (pep_birdModel_pVbo_boneids) {
		glDeleteBuffers(1, &pep_birdModel_pVbo_boneids);
		pep_birdModel_pVbo_boneids = NULL;
	}

	if (pep_birdModel_pVbo_texcoord) {
		glDeleteBuffers(1, &pep_birdModel_pVbo_texcoord);
		pep_birdModel_pVbo_texcoord = NULL;
	}

	if (pep_birdModel_pVbo_normal) {
		glDeleteBuffers(1, &pep_birdModel_pVbo_normal);
		pep_birdModel_pVbo_normal = NULL;
	}

	if (pep_birdModel_pVbo_position) {
		glDeleteBuffers(1, &pep_birdModel_pVbo_position);
		pep_birdModel_pVbo_position = NULL;
	}

	if (pep_birdModel_pVao)
	{
		glDeleteVertexArrays(1, &pep_birdModel_pVao);
		pep_birdModel_pVao = NULL;
	}

	if (pep_birdModel_gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_birdModel_gShaderProgramObject);

		glGetProgramiv(pep_birdModel_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(pep_birdModel_gShaderProgramObject, shaderCount, &shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_birdModel_gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			free(pShaders);
		}

		glDeleteProgram(pep_birdModel_gShaderProgramObject);
		pep_birdModel_gShaderProgramObject = 0;

		glUseProgram(0);
	}
}

