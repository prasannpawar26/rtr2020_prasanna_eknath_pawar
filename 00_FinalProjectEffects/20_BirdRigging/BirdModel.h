#pragma once

#include "Common.h"

HRESULT BirdModel_Initialize(void);
void BirdModel_Display(void);
void BirdModel_Update(void);
void BirdModel_Uninitialize(void);


