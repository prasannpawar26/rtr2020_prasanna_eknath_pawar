#pragma once

#include <GL/glew.h>
#include <GL/wglew.h>
#include<gl/GL.h>
#include <stdio.h>
#include<stdlib.h>
#include <windows.h>
#include <Windowsx.h>
#include <stdio.h>
#include "vmath.h"
#include "Camera.h"

using namespace std;
using namespace vmath;

enum {
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXCOORD0,
	AMC_ATTRIBUTES_BONEIDS,
	AMC_ATTRIBUTES_BONEWEIGHTS
};
