#pragma once

enum {
    texture_diffuse_24 = 0,
    texture_normal_24,
    texture_rough_8,
    texture_ao_8,
    texture_spec_8
};

int Blur_RenderToTexture_Initialize();
void Blur_RenderToTexture_Display();
void Blur_RenderToTexture_Update(void);
void Blur_RenderToTexture_ReSize(int width, int height);
void Blur_RenderToTexture_Uninitialize(void);
BOOL Blur_RenderToTexture_LoadTextureSTB(int type, const char*, GLuint*);