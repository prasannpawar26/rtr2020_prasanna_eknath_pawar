/*
Step 1: 
    - Attach pep_Blur_Fbo Into Pipeline
    - Render Scene [i.e Pyramid and Cube]
Step 2:
    - Attach pep_BlurPostProcessing_Fbo Into Pipeline
    - Apply Color Attachment Of pep_Blur_Fbo As Texture
    - Apply Blur Post Processing
Step 3:
    - Apply Color Attachment of pep_BlurPostProcessing_Fbo As Texture
    - God Rays Effect Calculation Inside Shader
*/

#include "Common.h"
#include "Bloom.h"
#include "Sphere.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;
extern int pep_gWidth;
extern int pep_gHeight;

//
// Render To Texture Variables
//
GLuint pep_Blur_RTT_VertexShaderObject;
GLuint pep_Blur_RTT_FragmentShaderObject;
GLuint pep_Blur_RTT_ShaderProgramObject;

GLuint pep_Blur_RTT_modelUniform;
GLuint pep_Blur_RTT_viewUniform;
GLuint pep_Blur_RTT_projectionUniform;

GLuint pep_Blur_RTT_lightAmbientUniform;
GLuint pep_Blur_RTT_lightDiffuseUniform;
GLuint pep_Blur_RTT_lightSpecularUniform;
GLuint pep_Blur_RTT_materialAmbientUniform;
GLuint pep_Blur_RTT_materialDiffuseUniform;
GLuint pep_Blur_RTT_materialSpecularUniform;
GLuint pep_Blur_RTT_materialShinessUniform;
GLuint pep_Blur_RTT_lightPositionUniform;

GLuint pep_Blur_RTT_diffuseTextureUniform;
GLuint pep_Blur_RTT_normalTextureUniform;
GLuint pep_Blur_RTT_roughTextureUniform;
GLuint pep_Blur_RTT_aoTextureUniform;
GLuint pep_Blur_RTT_specTextureUniform;

GLuint pep_Blur_RTT_diffuseTexture;
GLuint pep_Blur_RTT_normalTexture;
GLuint pep_Blur_RTT_roughTexture;
GLuint pep_Blur_RTT_aoTexture;
GLuint pep_Blur_RTT_specTexture;

float pep_Blur_RTT_lightAmbient[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float pep_Blur_RTT_lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Blur_RTT_lightSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Blur_RTT_lightPosition[4] = {0.0f, 0.0f, 5.0f, 1.0f};

float pep_Blur_RTT_materialAmbient[4] = {0.0f, 0.0f, 0.0f, 1.0f};
float pep_Blur_RTT_materialDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Blur_RTT_materialSpecular[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float pep_Blur_RTT_materialShiness = 150.0f;  // Also Try 128.0f

GLuint pep_Blur_RTT_Square_vao;
GLuint pep_Blur_RTT_Square_vbo_position;
GLuint pep_Blur_RTT_Square_vbo_element;
GLuint pep_Blur_RTT_Square_vbo_texcoords;
GLuint pep_Blur_RTT_Square_vbo_normal;
GLfloat pep_Blur_RTT_Square_rotation = 0.0f;



float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;



int Blur_RenderToTexture_Initialize(void)
{
    const GLchar *rendertotexture_VertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 RTT_vPosition;"
        "in vec3 RTT_vNormal;"
        "in vec2 RTT_vTexcoords;"

        "uniform mat4 u_rtt_model_matrix;"
        "uniform mat4 u_rtt_view_matrix;"
        "uniform mat4 u_rtt_projection_matrix;"

        "uniform vec4 u_rtt_light_position;" \

        "out vec3 light_direction;" \
        "out vec3 tranformation_matrix;" \
        "out vec3 viewer_vector;" \
        "out vec2 out_vTexcoords;" \

        "out vec3 WorldPos;" \
        "out vec3 Normal;" \
        "out vec2 TexCoords;" \

        "void main(void)"
        "{"
            "vec4 eye_coordinates = u_rtt_view_matrix * u_rtt_model_matrix * RTT_vPosition;" \
            "tranformation_matrix = mat3(u_rtt_view_matrix * u_rtt_model_matrix) * RTT_vNormal;" \
            "light_direction = vec3(u_rtt_light_position - eye_coordinates);" \
            "viewer_vector = vec3(-eye_coordinates);" \
            "out_vTexcoords = RTT_vTexcoords;" \

            "WorldPos = vec3(u_rtt_model_matrix * RTT_vPosition);" \
            "Normal = mat3(u_rtt_model_matrix) * RTT_vNormal;" \
            "TexCoords = RTT_vTexcoords;" \

            "gl_Position = u_rtt_projection_matrix * u_rtt_view_matrix * u_rtt_model_matrix * RTT_vPosition;"
        "}";

    pep_Blur_RTT_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == pep_Blur_RTT_VertexShaderObject)
    {
        fprintf(pep_gpFile,"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader Failed For RTT_VertexShaderObject\n",__FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    glShaderSource(pep_Blur_RTT_VertexShaderObject, 1, (const GLchar **)&rendertotexture_VertexShaderSourceCode, NULL);
    glCompileShader(pep_Blur_RTT_VertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Blur_RTT_VertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader Compilation Failed.\n", __FILE__, __LINE__, __FUNCTION__);

        glGetShaderiv(pep_Blur_RTT_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_Blur_RTT_VertexShaderObject, iInfoLogLength, &written, szInfoLog);

                fprintf( pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }

    pep_Blur_RTT_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == pep_Blur_RTT_FragmentShaderObject)
    {
        fprintf(pep_gpFile,"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader Failed For RTT_VertexShaderObject\n",__FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    const GLchar *rendertotexture_FragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "\nin vec3 WorldPos;" \
        "\nin vec3 Normal;" \
        "\nin vec2 TexCoords;" \

        "\nin vec3 light_direction;" \
        "\nin vec3 tranformation_matrix;" \
        "\nin vec3 viewer_vector;" \
        "\nin vec2 out_vTexcoords;"

        "\nuniform vec3 u_rtt_light_ambient;" \
        "\nuniform vec3 u_rtt_light_diffuse;" \
        "\nuniform vec3 u_light_specular;" \
        "\nuniform vec4 u_rtt_light_position;" \

        "\nuniform vec3 u_rtt_material_ambient;" \
        "\nuniform vec3 u_rtt_material_diffuse;" \
        "\nuniform vec3 u_rtt_material_specular;" \
        "\nuniform float u_rtt_material_shiness;" \

        "\nuniform sampler2D u_sampler_diffuse;" \
        "\nuniform sampler2D u_sampler_normal;" \
        "\nuniform sampler2D u_sampler_rough;" \
        "\nuniform sampler2D u_sampler_ao;" \
        "\nuniform sampler2D u_sampler_spec;" \

        "\nuniform vec3 camPos = vec3(0.0, 0.0, 5.0);" \

        "\nout vec4 FragColor;" \

       // "\nlayout (location = 0) out vec4 GreenFragColor;" \
       // "\nlayout (location = 1) out vec4 RedFragColor;" \

        "const float PI = 3.14159265359;" \

        "\nvec3 getNormalFromMap()" \
        "\n{ "\
            "\nvec3 tangentNormal = texture(u_sampler_normal, TexCoords).xyz * 2.0 - 1.0;"\

            "\nvec3 Q1  = dFdx(WorldPos);" \
            "\nvec3 Q2  = dFdy(WorldPos);" \
            "\nvec2 st1 = dFdx(TexCoords);" \
            "\nvec2 st2 = dFdy(TexCoords);" \

            "\nvec3 N   = normalize(Normal);"\
            "\nvec3 T  = normalize(Q1*st2.t - Q2*st1.t);"\
            "\nvec3 B  = -normalize(cross(N, T));"\
            "\nmat3 TBN = mat3(T, B, N);" \

            "\nreturn normalize(TBN * tangentNormal);"\
        "\n}"\

        "\nfloat DistributionGGX(vec3 N, vec3 H, float roughness)" \
        "\n{"\
            "\nfloat a = roughness*roughness;"\
            "\nfloat a2 = a*a;"\
            "\nfloat NdotH = max(dot(N, H), 0.0);"\
            "\nfloat NdotH2 = NdotH*NdotH;"\

            "\nfloat nom   = a2;"\
            "\nfloat denom = (NdotH2 * (a2 - 1.0) + 1.0);"\
            "\ndenom = PI * denom * denom;"\

            "\nreturn nom / denom;"\
        "\n}"\

        "\nfloat GeometrySchlickGGX(float NdotV, float roughness)"\
        "\n{"\
            "\nfloat r = (roughness + 1.0);"\
            "\nfloat k = (r*r) / 8.0;"\

            "\nfloat nom   = NdotV;"\
            "\nfloat denom = NdotV * (1.0 - k) + k;"\

            "\nreturn nom / denom;"\
        "\n}"\

        "\nfloat GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)"\
        "\n{"\
            "\nfloat NdotV = max(dot(N, V), 0.0);"\
            "\nfloat NdotL = max(dot(N, L), 0.0);"\

            "\nfloat ggx2 = GeometrySchlickGGX(NdotV, roughness);" \
            "\nfloat ggx1 = GeometrySchlickGGX(NdotL, roughness);"\

            "\nreturn ggx1 * ggx2;" \
        "\n}"\

        "\nvec3 fresnelSchlick(float cosTheta, vec3 F0)" \
        "\n{"\
            "\nreturn F0 + (1.0 - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);"\
        "\n}"\

        "\nvoid main(void)" \
        "\n{" \
            "\nvec3 albedo     = pow(texture(u_sampler_diffuse, TexCoords).rgb, vec3(2.2));" \
            "\nfloat metallic  = texture(u_sampler_spec, out_vTexcoords).r;"
            "\nfloat roughness = texture(u_sampler_rough, out_vTexcoords).r;"
            "\nfloat ao        = texture(u_sampler_ao, out_vTexcoords).r;"

            "\nvec3 N          = getNormalFromMap();" \
            "\nvec3 V = normalize(camPos - WorldPos);" \

            "\nvec3 F0 = vec3(0.04); " \
            "\nF0 = mix(F0, albedo, metallic);" \
            "\nvec3 Lo = vec3(0.0);" \

            "\nvec3 L = normalize(u_rtt_light_position.xyz - WorldPos);"\
            "\nvec3 H = normalize(V + L);"\
            "\nfloat distance = length(u_rtt_light_position.xyz - WorldPos);"\
            "\nfloat attenuation = 1.0 / (distance * distance);"\
            "\nvec3 radiance = u_rtt_light_diffuse.rgb * attenuation;" \

            "\nfloat NDF = DistributionGGX(N, H, roughness);   " \
            "\nfloat G   = GeometrySmith(N, V, L, roughness);      "\
            "\nvec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);"\

            "\nvec3 numerator    = NDF * G * F; "\
            "\nfloat denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001;" \
            "\nvec3 specular = numerator / denominator;"\

            "\nvec3 kS = F;" \
            "\nvec3 kD = vec3(1.0) - kS;" \
            "\nkD *= 1.0 - metallic;	" \
            "\nfloat NdotL = max(dot(N, L), 0.0); " \
            "\nLo += (kD * albedo / PI + specular) * radiance * NdotL;" \

            "\nvec3 ambient = vec3(0.03) * albedo * ao;" \
            "\nvec3 color = ambient + Lo;" \
            "\ncolor = color / (color + vec3(1.0));" \
            "\ncolor = pow(color, vec3(1.0/2.2)); " \

            //"\nRedFragColor = vec4(color, 1.0);" \

            //"\nGreenFragColor = vec4(color, 1.0);" \

            "FragColor = vec4(color, 1.0);" \

            "vec3 phong_ads_light;" \
            "vec3 light_direction_normalize = normalize(light_direction);" \
            "vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
            "vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \
            "vec3 viewer_vector_normal = normalize(viewer_vector);" \
            "float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \
            "vec3 ambient1 = u_rtt_light_ambient * u_rtt_material_ambient;" \
            "vec3 diffuse1 = u_rtt_light_diffuse * u_rtt_material_diffuse * t_normal_dot_light_direction;" \
            "vec3 specular1 = u_light_specular * u_rtt_material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), u_rtt_material_shiness);" \

             "diffuse1 = u_rtt_light_diffuse * albedo;" \
            "phong_ads_light= ambient1 + diffuse1 + specular1;" \

            "FragColor = vec4(phong_ads_light, 1.0);" \

            /*"RedFragColor = vec4(phong_ads_light, 1.0);;" \
            "GreenFragColor = vec4(phong_ads_light, 1.0);;" \*/
        "}";

    glShaderSource(pep_Blur_RTT_FragmentShaderObject, 1, (const GLchar **)&rendertotexture_FragmentShaderSourceCode, NULL);
    glCompileShader(pep_Blur_RTT_FragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(pep_Blur_RTT_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (FALSE == iShaderCompileStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader Compilation Failed.\n", __FILE__, __LINE__, __FUNCTION__);
        glGetShaderiv(pep_Blur_RTT_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_Blur_RTT_FragmentShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }

    pep_Blur_RTT_ShaderProgramObject = glCreateProgram();

    glAttachShader(pep_Blur_RTT_ShaderProgramObject, pep_Blur_RTT_VertexShaderObject);
    glAttachShader(pep_Blur_RTT_ShaderProgramObject, pep_Blur_RTT_FragmentShaderObject);
    glBindAttribLocation(pep_Blur_RTT_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "RTT_vPosition");
    glBindAttribLocation(pep_Blur_RTT_ShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "RTT_vNormal");
    glBindAttribLocation(pep_Blur_RTT_ShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "RTT_vTexcoords");
    glLinkProgram(pep_Blur_RTT_ShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Blur_RTT_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n", __FILE__, __LINE__, __FUNCTION__);

        glGetProgramiv(pep_Blur_RTT_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Blur_RTT_ShaderProgramObject, iInfoLogLength, &written, szInfoLog);

                fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }
    pep_Blur_RTT_modelUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_model_matrix");
    pep_Blur_RTT_viewUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_view_matrix");
    pep_Blur_RTT_projectionUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_projection_matrix");

    pep_Blur_RTT_diffuseTextureUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_sampler_diffuse");
    pep_Blur_RTT_normalTextureUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_sampler_normal");
    pep_Blur_RTT_roughTextureUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_sampler_rough");
    pep_Blur_RTT_aoTextureUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_sampler_ao");
    pep_Blur_RTT_specTextureUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_sampler_spec");

    pep_Blur_RTT_lightAmbientUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_light_ambient");
    pep_Blur_RTT_lightDiffuseUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_light_diffuse");
    pep_Blur_RTT_lightSpecularUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_light_specular");
    pep_Blur_RTT_lightPositionUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_light_position");

    pep_Blur_RTT_materialAmbientUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_material_ambient");
    pep_Blur_RTT_materialDiffuseUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_material_diffuse");
    pep_Blur_RTT_materialSpecularUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_material_specular");
    pep_Blur_RTT_materialShinessUniform =
        glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_material_shiness");


    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures,
        sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    glGenVertexArrays(1, &pep_Blur_RTT_Square_vao);
    glBindVertexArray(pep_Blur_RTT_Square_vao);

    float quadVertices[] = {
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
    };
    glGenBuffers(1, &pep_Blur_RTT_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    float quadNormals[] = {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
    };
    glGenBuffers(1, &pep_Blur_RTT_Square_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadNormals), quadNormals,GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    float quadTexcoords[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };
    glGenBuffers(1, &pep_Blur_RTT_Square_vbo_texcoords);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_texcoords);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexcoords), quadTexcoords,GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    //glGenBuffers(1, &pep_Blur_RTT_Square_vbo_element);
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_element);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements),
    //    sphere_elements, GL_STATIC_DRAW);
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    Blur_RenderToTexture_LoadTextureSTB(texture_diffuse_24, "rusty_metal_02_diff_4k.jpg", &pep_Blur_RTT_diffuseTexture);
    Blur_RenderToTexture_LoadTextureSTB(texture_normal_24, "rusty_metal_02_nor_gl_4k.jpg", &pep_Blur_RTT_normalTexture);
    Blur_RenderToTexture_LoadTextureSTB(texture_rough_8, "rusty_metal_02_rough_4k.jpg", &pep_Blur_RTT_roughTexture);
    Blur_RenderToTexture_LoadTextureSTB(texture_ao_8, "rusty_metal_02_ao_4k.jpg", &pep_Blur_RTT_aoTexture);
    Blur_RenderToTexture_LoadTextureSTB(texture_spec_8, "rusty_metal_02_spec_4k.jpg", &pep_Blur_RTT_specTexture);
   
    return 0;
}

void Blur_RenderToTexture_Uninitialize(void)
{
    if (pep_Blur_RTT_Square_vbo_texcoords)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Square_vbo_texcoords);
        pep_Blur_RTT_Square_vbo_texcoords = 0;
    }

    if (pep_Blur_RTT_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Square_vbo_position);
        pep_Blur_RTT_Square_vbo_position = 0;
    }

    if (pep_Blur_RTT_Square_vbo_element)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Square_vbo_element);
        pep_Blur_RTT_Square_vbo_element = 0;
    }

    if (pep_Blur_RTT_Square_vbo_normal)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Square_vbo_normal);
        pep_Blur_RTT_Square_vbo_normal = 0;
    }

    if (pep_Blur_RTT_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_Blur_RTT_Square_vao);
        pep_Blur_RTT_Square_vao = 0;
    }

    if (pep_Blur_RTT_ShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Blur_RTT_ShaderProgramObject);

        glGetProgramiv(pep_Blur_RTT_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Blur_RTT_ShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Blur_RTT_ShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Blur_RTT_ShaderProgramObject);
        pep_Blur_RTT_ShaderProgramObject = 0;

        glUseProgram(0);
    }
}

void Blur_RenderToTexture_Update()
{

}

void Blur_RenderToTexture_Display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    Blur_RenderToTexture_Update();

    mat4 perspectiveMatrix;
    perspectiveMatrix = mat4::identity();

    glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

    perspectiveMatrix = perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 100.0f);

    //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    

    glUseProgram(pep_Blur_RTT_ShaderProgramObject);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_Blur_RTT_diffuseTextureUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_Blur_RTT_diffuseTexture);

    glActiveTexture(GL_TEXTURE1);
    glUniform1i(pep_Blur_RTT_normalTextureUniform, 1);
    glBindTexture(GL_TEXTURE_2D, pep_Blur_RTT_normalTexture);

    glActiveTexture(GL_TEXTURE2);
    glUniform1i(pep_Blur_RTT_roughTextureUniform, 2);
    glBindTexture(GL_TEXTURE_2D, pep_Blur_RTT_roughTexture);

    glActiveTexture(GL_TEXTURE3);
    glUniform1i(pep_Blur_RTT_aoTextureUniform, 3);
    glBindTexture(GL_TEXTURE_2D, pep_Blur_RTT_aoTexture);

    glActiveTexture(GL_TEXTURE4);
    glUniform1i(pep_Blur_RTT_specTextureUniform, 4);
    glBindTexture(GL_TEXTURE_2D, pep_Blur_RTT_specTexture);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 modelViewProjectionMatrix;

    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    modelMatrix = translate(0.0f, 0.0f, -6.0f);
    viewMatrix = lookat(vec3(0.0f, 0.0f, 5.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));

    glUniformMatrix4fv(pep_Blur_RTT_modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_Blur_RTT_viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_Blur_RTT_projectionUniform, 1, GL_FALSE, perspectiveMatrix);
    glUniform3fv(pep_Blur_RTT_lightAmbientUniform, 1, pep_Blur_RTT_lightAmbient);
    glUniform3fv(pep_Blur_RTT_lightDiffuseUniform, 1, pep_Blur_RTT_lightDiffuse);
    glUniform3fv(pep_Blur_RTT_lightSpecularUniform, 1, pep_Blur_RTT_lightSpecular);
    glUniform4fv(pep_Blur_RTT_lightPositionUniform, 1, pep_Blur_RTT_lightPosition);
    glUniform3fv(pep_Blur_RTT_materialAmbientUniform, 1, pep_Blur_RTT_materialAmbient);
    glUniform3fv(pep_Blur_RTT_materialDiffuseUniform, 1, pep_Blur_RTT_materialDiffuse);
    glUniform3fv(pep_Blur_RTT_materialSpecularUniform, 1, pep_Blur_RTT_materialSpecular);
    glUniform1f(pep_Blur_RTT_materialShinessUniform, pep_Blur_RTT_materialShiness);

    glBindVertexArray(pep_Blur_RTT_Square_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    /*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);*/
    glBindVertexArray(0);

    glUseProgram(0);

    return;
}

void Blur_RenderToTexture_ReSize(int width, int height)
{

}

BOOL Blur_RenderToTexture_LoadTextureSTB(int type, const char* filename, GLuint* texture)
{
    int width, height, nrChannels;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
    if (data)
    {
        switch (type)
        {
            case texture_diffuse_24:
            case texture_normal_24:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            break;

            case texture_rough_8:
            case texture_ao_8:
            case texture_spec_8:
                glTexImage2D(GL_TEXTURE_2D, 0, GL_R, width, height, 0, GL_R, GL_UNSIGNED_BYTE, data);
                break;
        }
        
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        fprintf(pep_gpFile, "Failed To Load Texture");
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    return true;
}