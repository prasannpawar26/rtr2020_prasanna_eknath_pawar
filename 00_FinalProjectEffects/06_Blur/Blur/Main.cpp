#include "Main.h"
#include "Blur.h"

#define PEP_WIDTH 800
#define PEP_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HDC pep_gHdc = NULL;
HGLRC pep_gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND pep_gHwnd = NULL;
DWORD pep_dwStyle;
WINDOWPLACEMENT pep_gWpPrev = {sizeof(WINDOWPLACEMENT)};
bool pep_gbActiveWindow = false;
bool pep_gbIsFullScreen = false;

int pep_AssignmentDisplay = 5;

mat4 pep_Perspective_ProjectionMatrix;

bool pep_bFadeOut = false;

int pep_gToggleFboColorAttachement = 0;
int pep_gToggleDisbleBlur = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    // Function Declarations
    int Initialize(void);
    void Display(void);
    void Update(void);

    // Variable Declarations
    bool bDone = false;
    MSG msg = {0};
    int iRet = 0;
    HWND hwnd;
    WNDCLASSEX wndClass;
    TCHAR szAppName[] = TEXT("pep_Blur_Fbo");

    // Code
    if (0 != fopen_s(&pep_gpFile, "Log.txt", "w"))
    {
        MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
        exit(0);
    }

    fprintf(pep_gpFile, "Log File Created Successfully\n");

    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.cbWndExtra = 0;
    wndClass.cbClsExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szAppName;
    wndClass.lpfnWndProc = WndProc;
    wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

    RegisterClassEx(&wndClass);

    hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Blur"), WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100, 100, 800, 600, NULL, NULL, hInstance, NULL);
    pep_gHwnd = hwnd;

    ShowWindow(hwnd, iCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    iRet = Initialize();
    if (-1 == iRet)
    {
        fprintf(pep_gpFile, "Choose Pixel Format Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-2 == iRet)
    {
        fprintf(pep_gpFile, "Set Pixel Format Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-3 == iRet)
    {
        fprintf(pep_gpFile, "wglCreateContext Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-4 == iRet)
    {
        fprintf(pep_gpFile, "wglMakeCurrent Failed\n");
        DestroyWindow(hwnd);
    }
    else if (-5 == iRet)
    {
        fprintf(pep_gpFile, "glewInit Failed\n");
        DestroyWindow(hwnd);
    } else {
        fprintf(pep_gpFile, "Initialization Successful\n");
    }

    // GAME LOOP
    while (false == bDone)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (WM_QUIT == msg.message)
            {
                bDone = true;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        } else
        {
            if (pep_gbActiveWindow)
            {
            }
            Update();

            Display();
        }
    }  // END OF WHILE

    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    // Function Declarations
    void ToggledFullScreen(void);
    void UnInitialize(void);
    void ReSize(int, int);

    // Variable Declarations

    // Code
    switch (iMsg)
    {
    case WM_DESTROY:
        UnInitialize();
        PostQuitMessage(0);
        break;

    case WM_ERASEBKGND:
        return 0;
        break;

    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;

    case WM_SIZE:
        ReSize(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_KILLFOCUS:
        pep_gbActiveWindow = false;
        break;

    case WM_SETFOCUS:
        pep_gbActiveWindow = true;
        break;

    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_ESCAPE:
            DestroyWindow(hwnd);
            break;

        case 'F':
        case 'f':
            ToggledFullScreen();
            break;

        case 'C':
        case 'c':
            if (pep_gToggleFboColorAttachement == 0)
            {
                pep_gToggleFboColorAttachement = 1;
            }
            else
            {
                pep_gToggleFboColorAttachement = 0;
            }
            break;

        case 'd':
        case 'D':
            if (pep_gToggleDisbleBlur == 0)
            {
                pep_gToggleDisbleBlur = 1;
            }
            else
            {
                pep_gToggleDisbleBlur = 0;
            }
            break;
        }
        break;
    }  // End Of Switch Case

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
    // Variable Declarations
    MONITORINFO mi;

    // Code
    if (false == pep_gbIsFullScreen)
    {
        pep_dwStyle = GetWindowLong(pep_gHwnd, GWL_STYLE);

        if (WS_OVERLAPPEDWINDOW & pep_dwStyle)
        {
            mi = {sizeof(MONITORINFO)};

            if (GetWindowPlacement(pep_gHwnd, &pep_gWpPrev) &&
                GetMonitorInfo(MonitorFromWindow(pep_gHwnd, MONITORINFOF_PRIMARY), &mi))
            {
                SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle & ~WS_OVERLAPPEDWINDOW);
                SetWindowPos(pep_gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
                    mi.rcMonitor.right - mi.rcMonitor.left,
                    mi.rcMonitor.bottom - mi.rcMonitor.top,
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }

        ShowCursor(FALSE);
        pep_gbIsFullScreen = true;
    }
    else
    {
        SetWindowLong(pep_gHwnd, GWL_STYLE, pep_dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(pep_gHwnd, &pep_gWpPrev);
        SetWindowPos(pep_gHwnd, HWND_TOP, 0, 0, 0, 0,
            SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
            SWP_NOMOVE | SWP_NOSIZE);

        pep_gbIsFullScreen = false;
        ShowCursor(TRUE);
    }

    return;
}

int Initialize(void)
{
    // Variable Declarations
    int index;
    PIXELFORMATDESCRIPTOR pfd;

    // Function Declarations
    void ReSize(int, int);

    // Code
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.cColorBits = 32;
    pfd.cRedBits = 8;
    pfd.cBlueBits = 8;
    pfd.cGreenBits = 8;
    pfd.cAlphaBits = 8;

    pfd.cDepthBits = 32;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

    pep_gHdc = GetDC(pep_gHwnd);

    index = ChoosePixelFormat(pep_gHdc, &pfd);
    if (0 == index)
    {
        return -1;
    }

    if (FALSE == SetPixelFormat(pep_gHdc, index, &pfd))
    {
        return -2;
    }

    pep_gHglrc = wglCreateContext(pep_gHdc);
    if (NULL == pep_gHglrc)
    {
        return -3;
    }

    if (FALSE == wglMakeCurrent(pep_gHdc, pep_gHglrc))
    {
        return -4;
    }

    GLenum result;

    result = glewInit();
    if (GLEW_OK != result)
    {
        return -5;
    }

    Blur_Initialize(PEP_WIDTH, PEP_HEIGHT);

    glClearDepth(1.0f);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    pep_Perspective_ProjectionMatrix = mat4::identity();

    ReSize(PEP_WIDTH, PEP_HEIGHT);

    return 0;
}

void UnInitialize(void)
{
    //
    // Code
    //
    Blur_Uninitialize();

    if (wglGetCurrentContext() == pep_gHglrc)
    {
        wglMakeCurrent(NULL, NULL);
    }

    if (NULL != pep_gHglrc)
    {
        wglDeleteContext(pep_gHglrc);
    }

    if (NULL != pep_gHdc)
    {
        ReleaseDC(pep_gHwnd, pep_gHdc);
    }

    fprintf(pep_gpFile, "Log File Closed Successfully\n");

    if (NULL != pep_gpFile)
    {
        fclose(pep_gpFile);
    }

    return;
}

void ReSize(int width, int height)
{
    if (0 == height)
    {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    pep_Perspective_ProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

    Blur_ReSize(width, height);
    return;
}

void Display(void)
{
    // Code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Blur_Display(pep_gToggleFboColorAttachement);

    SwapBuffers(pep_gHdc);

    return;
}

void Update(void)
{

}
