#pragma once

int Blur_Initialize(int width, int height);
void Blur_Display(int);
void Blur_Update(void);
void Blur_ReSize(int width, int height);
void Blur_Uninitialize(void);
