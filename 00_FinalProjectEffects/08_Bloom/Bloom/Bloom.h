#pragma once

int Bloom_Initialize(int width, int height);
void Bloom_Display(int);
void Bloom_Update(void);
void Bloom_ReSize(int width, int height);
void Bloom_Uninitialize(void);
