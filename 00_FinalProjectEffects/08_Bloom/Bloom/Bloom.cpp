/*
Step 1: 
    - Attach pep_Blur_Fbo Into Pipeline
    - Render Scene [i.e Pyramid and Cube]
Step 2:
    - Attach pep_BlurPostProcessing_Fbo Into Pipeline
    - Apply Color Attachment Of pep_Blur_Fbo As Texture
    - Apply Blur Post Processing
Step 3:
    - Apply Color Attachment of pep_BlurPostProcessing_Fbo As Texture
    - God Rays Effect Calculation Inside Shader
*/

#include "Common.h"
#include "Bloom.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;
extern int pep_gToggleBlur;
extern int pep_gToggleGodRays;
extern int pep_gWidth;
extern int pep_gHeight;
extern float pep_gLightPositionX;
extern float pep_gLightPositionY;

GLuint pep_Bloom_gVertexShaderObject;
GLuint pep_Bloom_gFragmentShaderObject;
GLuint pep_Bloom_gShaderProgramObject;

GLuint pep_Bloom_BlurSamplerUniform;
GLuint pep_Bloom_NonBlurSamplerUniform;

GLuint pep_Bloom_Square_vao;
GLuint pep_Bloom_Square_vbo_position;
GLuint pep_Bloom_Square_vbo_texcoords;

//
// FrameBuffer
//
GLuint pep_Blur_Fbo;
GLuint pep_Blur_Fbo_TextureBuffer[2];
GLuint pep_Blur_Fbo_RenderBuffer;

//
// Render To Texture Variables
//
GLuint pep_Blur_RTT_VertexShaderObject;
GLuint pep_Blur_RTT_FragmentShaderObject;
GLuint pep_Blur_RTT_ShaderProgramObject;
GLuint pep_Blur_RTT_mvpUniform;

GLuint pep_Blur_RTT_Square_vao;
GLuint pep_Blur_RTT_Square_vbo_position;
GLuint pep_Blur_RTT_Square_vbo_color;
GLfloat pep_Blur_RTT_Square_rotation = 0.0f;

GLuint pep_Blur_RTT_Triangle_vao;
GLuint pep_Blur_RTT_Triangle_vbo_position;
GLuint pep_Blur_RTT_Triangle_vbo_color;
GLfloat pep_Blur_RTT_Triangle_rotation = 0.0f;

//
// BlurPostProcessing
//
GLuint pep_BlurPostProcessing_gVertexShaderObject;
GLuint pep_BlurPostProcessing_gFragmentShaderObject;
GLuint pep_BlurPostProcessing_gShaderProgramObject;
GLuint pep_BlurPostProcessing_samplerUniform;
GLuint pep_BlurPostProcessing_horizontalUniform;
GLuint pep_BlurPostProcessing_disableBlurUniform;

// No Render Buffer Required
GLuint pep_BlurPostProcessing_Fbo[2];
GLuint pep_BlurPostProcessing_Fbo_TextureBuffer[2];
//GLuint pep_BlurPostProcessing_Fbo_RenderBuffer[2];

GLuint pep_BlurPostProcessing_Square_vao;
GLuint pep_BlurPostProcessing_Square_vbo_position;
GLuint pep_BlurPostProcessing_Square_vbo_texcoords;

int Blur_FBO_Initialize(int width, int height)
{
    glGenFramebuffers(1, &pep_Blur_Fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, pep_Blur_Fbo);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    glGenTextures(2, pep_Blur_Fbo_TextureBuffer);

    for (int i = 0; i < 2; i++)
    {
        glBindTexture(GL_TEXTURE_2D, pep_Blur_Fbo_TextureBuffer[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR/*GL_NEAREST*/);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR/*GL_NEAREST*/);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_FLOAT/*GL_BGR, GL_UNSIGNED_BYTE*/, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, pep_Blur_Fbo_TextureBuffer[i], 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glGenRenderbuffers(1, &pep_Blur_Fbo_RenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, pep_Blur_Fbo_RenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, pep_Blur_Fbo_RenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        fprintf( pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n", __FILE__, __LINE__, __FUNCTION__);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        return -1;
    }

    unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, attachments);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return 0;
}

void Blur_FBO_ReSize(int width, int height)
{
    if (pep_Blur_Fbo)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, pep_Blur_Fbo);
        for (int i = 0; i < 2; i++)
        {
            glBindTexture(GL_TEXTURE_2D, pep_Blur_Fbo_TextureBuffer[i]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_FLOAT/*GL_BGR, GL_UNSIGNED_BYTE*/, 0);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, pep_Blur_Fbo_TextureBuffer[i], 0);
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glBindRenderbuffer(GL_RENDERBUFFER, pep_Blur_Fbo_RenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, pep_Blur_Fbo_RenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    return;
}

void Blur_FBO_Uinitialize(void)
{
    if (pep_Blur_Fbo_RenderBuffer)
    {
        glDeleteRenderbuffers(1, &pep_Blur_Fbo_RenderBuffer);
        pep_Blur_Fbo_RenderBuffer = 0;
    }

    for (int i = 0; i < 2; i++)
    {
        if (pep_Blur_Fbo_TextureBuffer[i]) 
        {
            glDeleteRenderbuffers(1, &pep_Blur_Fbo_TextureBuffer[i]);
            pep_Blur_Fbo_TextureBuffer[i] = 0;
        }
    }

    if (pep_Blur_Fbo)
    {
        glDeleteFramebuffers(1, &pep_Blur_Fbo);
        pep_Blur_Fbo = 0;
    }
}

int Blur_RenderToTexture_Initialize(void)
{
    const GLchar *rendertotexture_VertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 RTT_vPosition;"
        "in vec4 RTT_vColor;"

        "uniform mat4 u_rtt_mvp_matrix;"

        "out vec4 color;"
        "void main(void)"
        "{"
        "gl_Position = u_rtt_mvp_matrix * RTT_vPosition;"
        "color = RTT_vColor;"
        "}";

    pep_Blur_RTT_VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == pep_Blur_RTT_VertexShaderObject)
    {
        fprintf(pep_gpFile,"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader Failed For RTT_VertexShaderObject\n",__FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    glShaderSource(pep_Blur_RTT_VertexShaderObject, 1, (const GLchar **)&rendertotexture_VertexShaderSourceCode, NULL);
    glCompileShader(pep_Blur_RTT_VertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Blur_RTT_VertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Vertex Shader Compilation Failed.\n", __FILE__, __LINE__, __FUNCTION__);

        glGetShaderiv(pep_Blur_RTT_VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_Blur_RTT_VertexShaderObject, iInfoLogLength, &written, szInfoLog);

                fprintf( pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }

    pep_Blur_RTT_FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == pep_Blur_RTT_FragmentShaderObject)
    {
        fprintf(pep_gpFile,"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader Failed For RTT_VertexShaderObject\n",__FILE__, __LINE__, __FUNCTION__);
        return -1;
    }

    const GLchar *rendertotexture_FragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "in vec4 color;"

        "layout (location = 0) out vec4 RedFragColor;" \
        "layout (location = 1) out vec4 GreenFragColor;" \

        "void main(void)" \
        "{" \
            //"RedFragColor = color;" \
            //"WhiteFragColor = color;" \

            "if(color.r > 0.95)"
            "{"
                "RedFragColor = color;" \
            "}"
            "else"
            "{"
                "GreenFragColor = color;" \
            "}"
        "}";

    glShaderSource(pep_Blur_RTT_FragmentShaderObject, 1, (const GLchar **)&rendertotexture_FragmentShaderSourceCode, NULL);
    glCompileShader(pep_Blur_RTT_FragmentShaderObject);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetShaderiv(pep_Blur_RTT_FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (FALSE == iShaderCompileStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Fragment Shader Compilation Failed.\n", __FILE__, __LINE__, __FUNCTION__);
        glGetShaderiv(pep_Blur_RTT_FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_Blur_RTT_FragmentShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }

    pep_Blur_RTT_ShaderProgramObject = glCreateProgram();

    glAttachShader(pep_Blur_RTT_ShaderProgramObject, pep_Blur_RTT_VertexShaderObject);
    glAttachShader(pep_Blur_RTT_ShaderProgramObject, pep_Blur_RTT_FragmentShaderObject);
    glBindAttribLocation(pep_Blur_RTT_ShaderProgramObject, AMC_ATTRIBUTES_POSITION, "RTT_vPosition");
    glBindAttribLocation(pep_Blur_RTT_ShaderProgramObject, AMC_ATTRIBUTES_COLOR, "RTT_vColor");
    glLinkProgram(pep_Blur_RTT_ShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Blur_RTT_ShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Linking Failed.\n", __FILE__, __LINE__, __FUNCTION__);

        glGetProgramiv(pep_Blur_RTT_ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Blur_RTT_ShaderProgramObject, iInfoLogLength, &written, szInfoLog);

                fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking Failed:\n\t%s\n", __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
            }
        }

        return -1;
    }
    pep_Blur_RTT_mvpUniform = glGetUniformLocation(pep_Blur_RTT_ShaderProgramObject, "u_rtt_mvp_matrix");

    //
    // Square
    //
    const GLfloat squareVertices[] = {// top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,  

        // bottom
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,

        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // back
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, -1.0f, -1.0f, 
        -1.0f, -1.0f, 1.0f};

    /*const GLfloat squareColor[] = {0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        1.0f, 0.5f, 0.0f,
        1.0f, 0.5f, 0.0f,
        1.0f, 0.5f, 0.0f,
        1.0f, 0.5f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f};*/

    const GLfloat squareColor[] = {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f};


    glGenVertexArrays(1, &pep_Blur_RTT_Square_vao);
    glBindVertexArray(pep_Blur_RTT_Square_vao);

    glGenBuffers(1, &pep_Blur_RTT_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Blur_RTT_Square_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Square_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    //
    // Triangle
    //
    const GLfloat triangleVertices[] = {
        // front
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // right
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // back
        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        // left
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };

    const GLfloat triangleColor[] = {
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f

       /* 1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f*/
    };

    glGenVertexArrays(1, &pep_Blur_RTT_Triangle_vao);
    glBindVertexArray(pep_Blur_RTT_Triangle_vao);

    glGenBuffers(1, &pep_Blur_RTT_Triangle_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Triangle_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Blur_RTT_Triangle_vbo_color);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Blur_RTT_Triangle_vbo_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void Blur_RenderToTexture_Uninitialize(void)
{
    if (pep_Blur_RTT_Triangle_vbo_color)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Triangle_vbo_color);
        pep_Blur_RTT_Triangle_vbo_color = 0;
    }

    if (pep_Blur_RTT_Triangle_vbo_position)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Triangle_vbo_position);
        pep_Blur_RTT_Triangle_vbo_position = 0;
    }

    if (pep_Blur_RTT_Triangle_vao)
    {
        glDeleteVertexArrays(1, &pep_Blur_RTT_Triangle_vao);
        pep_Blur_RTT_Triangle_vao = 0;
    }

    if (pep_Blur_RTT_Square_vbo_color)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Square_vbo_color);
        pep_Blur_RTT_Square_vbo_color = 0;
    }

    if (pep_Blur_RTT_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_Blur_RTT_Square_vbo_position);
        pep_Blur_RTT_Square_vbo_position = 0;
    }

    if (pep_Blur_RTT_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_Blur_RTT_Square_vao);
        pep_Blur_RTT_Square_vao = 0;
    }

    if (pep_Blur_RTT_ShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Blur_RTT_ShaderProgramObject);

        glGetProgramiv(pep_Blur_RTT_ShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Blur_RTT_ShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Blur_RTT_ShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Blur_RTT_ShaderProgramObject);
        pep_Blur_RTT_ShaderProgramObject = 0;

        glUseProgram(0);
    }
}

void Blur_RenderToTexture_Update()
{
    pep_Blur_RTT_Triangle_rotation += 0.5f;
    if (pep_Blur_RTT_Triangle_rotation > 360.0)
    {
        pep_Blur_RTT_Triangle_rotation = 0.0f;
    }

    pep_Blur_RTT_Square_rotation += 0.5f;
    if (pep_Blur_RTT_Square_rotation > 360.0)
    {
        pep_Blur_RTT_Square_rotation = 0.0f;
    }
}

void Blur_RenderToTexture_Display()
{
    Blur_RenderToTexture_Update();
    // Render
    glBindFramebuffer(GL_FRAMEBUFFER, pep_Blur_Fbo);
    mat4 perspectiveMatrix;
    perspectiveMatrix = mat4::identity();

    glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

    // We are commenting below line
    // Reason 1: because while rendering final output we do not want this to get apply on it
    // Reason 2: We are using/ apply aspect ratio here only
    // Reason 3: While Drawing Final Ouput we removed persepctive and matrix calculation from it
    //perspectiveMatrix = perspective(45.0f, 1, 0.1f, 100.0f);
    perspectiveMatrix = perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 100.0f);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT /*| GL_STENCIL_BUFFER_BIT*/);

    glUseProgram(pep_Blur_RTT_ShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Pyramid
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(-2.0f, 0.0f, -12.50f) * rotate(pep_Blur_RTT_Triangle_rotation, 0.0f, 1.0f, 0.0f);
    modelViewProjectionMatrix = perspectiveMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_Blur_RTT_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(pep_Blur_RTT_Triangle_vao);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    //
    // Cube
    //
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(2.0f, 0.0f, -12.50f)
        * rotate(pep_Blur_RTT_Square_rotation, 1.0f, 0.0f, 0.0f)
        * rotate(pep_Blur_RTT_Square_rotation, 0.0f, 1.0f, 0.0f)
        * rotate(pep_Blur_RTT_Square_rotation, 0.0f, 0.0f, 1.0f)
        * scale(0.75f, 0.75f, 0.75f);
    modelViewProjectionMatrix = perspectiveMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_Blur_RTT_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(pep_Blur_RTT_Square_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return;
}

int BlurPostProcessing_FBO_Initialize(int width, int height)
{
    for (int i = 0; i < 2; i++)
    {
        glGenFramebuffers(1, &pep_BlurPostProcessing_Fbo[i]);
        glBindFramebuffer(GL_FRAMEBUFFER, pep_BlurPostProcessing_Fbo[i]);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glGenTextures(1, &pep_BlurPostProcessing_Fbo_TextureBuffer[i]);
        glBindTexture(GL_TEXTURE_2D, pep_BlurPostProcessing_Fbo_TextureBuffer[i]);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pep_BlurPostProcessing_Fbo_TextureBuffer[i], 0);

        glBindTexture(GL_TEXTURE_2D, 0);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n", __FILE__, __LINE__, __FUNCTION__);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            return -1;
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    return 0;
}

void BlurPostProcessing_FBO_ReSize(int width, int height)
{
    for (int i = 0; i < 2; i++)
    {
        if (pep_BlurPostProcessing_Fbo[i])
        {
            glBindFramebuffer(GL_FRAMEBUFFER, pep_BlurPostProcessing_Fbo[i]);

            glBindTexture(GL_TEXTURE_2D, pep_BlurPostProcessing_Fbo_TextureBuffer[i]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, 0);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pep_BlurPostProcessing_Fbo_TextureBuffer[i], 0);
            glBindTexture(GL_TEXTURE_2D, 0);

           /* glBindRenderbuffer(GL_RENDERBUFFER, pep_BlurPostProcessing_Fbo_RenderBuffer[i]);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, pep_BlurPostProcessing_Fbo[i]);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);*/

            glBindFramebuffer(GL_FRAMEBUFFER, pep_BlurPostProcessing_Fbo[i]);
        }
    }

    return;
}

void BlurPostProcessing_FBO_Uinitialize(void)
{
    for (int i = 0; i < 2; i++)
    {
        if (pep_BlurPostProcessing_Fbo_TextureBuffer[i]) 
        {
            glDeleteRenderbuffers(1, &pep_BlurPostProcessing_Fbo_TextureBuffer[i]);
            pep_BlurPostProcessing_Fbo_TextureBuffer[i] = 0;
        }

        if (pep_BlurPostProcessing_Fbo)
        {
            glDeleteFramebuffers(1, &pep_BlurPostProcessing_Fbo[i]);
            pep_BlurPostProcessing_Fbo[i] = 0;
        }
    }
}

int BlurPostProcessing_Initialize(int width, int height)
{
    fprintf(pep_gpFile, "\nBlurPostProcessing\n");

    pep_BlurPostProcessing_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;"

        "out vec2 out_texcoord;"

        "void main(void)" \
        "{" \
            "gl_Position = vPosition;" \
            "out_texcoord = vTexCoord;" \
        "}";

    glShaderSource(pep_BlurPostProcessing_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_BlurPostProcessing_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_BlurPostProcessing_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_BlurPostProcessing_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_BlurPostProcessing_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_BlurPostProcessing_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \

        "uniform int u_horizontal_blur;" \
        "uniform int u_disable_blur;"
        "uniform sampler2D u_sampler;" \

        "float weight[5] = float[] (0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162);" \

        "out vec4 FragColor;"

        "void main(void)" \
        "{" \
            "if( 0 == u_disable_blur)"
            "{"
                "vec2 tex_offset = 1.0 / textureSize(u_sampler, 0);" \
                "vec3 result = texture(u_sampler, out_texcoord).rgb * weight[0];" \

                "if(1 == u_horizontal_blur)" \
                "{" \
                    "for(int i = 1; i < 5; ++i)" \
                    "{" \
                        "result += texture(u_sampler, out_texcoord + vec2(tex_offset.x * i, 0.0)).rgb * weight[i];" \
                        "result += texture(u_sampler, out_texcoord - vec2(tex_offset.x * i, 0.0)).rgb * weight[i];" \
                    "}" \
                "}" \
                "else" \
                "{" \
                    "for(int i = 1; i < 5; ++i)" \
                    "{" \
                        "result += texture(u_sampler, out_texcoord + vec2(0.0, tex_offset.y * i)).rgb * weight[i];" \
                        "result += texture(u_sampler, out_texcoord - vec2(0.0, tex_offset.y * i)).rgb * weight[i];;" \
                    "}" \
                "}" \

                "FragColor = vec4(result, 1.0);" \
            "}"
            "else"
             "{"
                    "FragColor = texture(u_sampler, out_texcoord);"
             "}"
        "}";

    glShaderSource(pep_BlurPostProcessing_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_BlurPostProcessing_gFragmentShaderObject);

    glGetShaderiv(pep_BlurPostProcessing_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_BlurPostProcessing_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_BlurPostProcessing_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_BlurPostProcessing_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_BlurPostProcessing_gShaderProgramObject, pep_BlurPostProcessing_gVertexShaderObject);
    glAttachShader(pep_BlurPostProcessing_gShaderProgramObject, pep_BlurPostProcessing_gFragmentShaderObject);

    glBindAttribLocation(pep_BlurPostProcessing_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_BlurPostProcessing_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_BlurPostProcessing_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_BlurPostProcessing_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_BlurPostProcessing_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_BlurPostProcessing_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_BlurPostProcessing_samplerUniform = glGetUniformLocation(pep_BlurPostProcessing_gShaderProgramObject, "u_sampler");
    pep_BlurPostProcessing_horizontalUniform = glGetUniformLocation(pep_BlurPostProcessing_gShaderProgramObject, "u_horizontal_blur");
    pep_BlurPostProcessing_disableBlurUniform = glGetUniformLocation(pep_BlurPostProcessing_gShaderProgramObject, "u_disable_blur");
    //
    // Square
    //
    const GLfloat squareVertices[] = {
        1.0f,  1.0f,  0.0f,
        -1.0f, 1.0f,  0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f
    };
    const GLfloat squaretexcoords[] = {
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f};

    glGenVertexArrays(1, &pep_BlurPostProcessing_Square_vao);
    glBindVertexArray(pep_BlurPostProcessing_Square_vao);

    glGenBuffers(1, &pep_BlurPostProcessing_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_BlurPostProcessing_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_BlurPostProcessing_Square_vbo_texcoords);
    glBindBuffer(GL_ARRAY_BUFFER, pep_BlurPostProcessing_Square_vbo_texcoords);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squaretexcoords), squaretexcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void BlurPostProcessing_Uninitialize(void)
{
    if (pep_BlurPostProcessing_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_BlurPostProcessing_gShaderProgramObject);

        glGetProgramiv(pep_BlurPostProcessing_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_BlurPostProcessing_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_BlurPostProcessing_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_BlurPostProcessing_gShaderProgramObject);
        pep_BlurPostProcessing_gShaderProgramObject = 0;

        glUseProgram(0);
    }
}

void BlurPostProcessing_Display(int pep_gToggleFboColorAttachement)
{
    int horizontal = 1;
    bool first_iteration = true;
    unsigned int iteration = 10;

    glUseProgram(pep_BlurPostProcessing_gShaderProgramObject);

    for (unsigned int i = 0; i < iteration; i++)
    {
        glUniform1i(pep_BlurPostProcessing_horizontalUniform, horizontal);
        glActiveTexture(GL_TEXTURE0);
        glUniform1i(pep_BlurPostProcessing_samplerUniform, 0);
        glUniform1i(pep_BlurPostProcessing_disableBlurUniform, pep_gToggleBlur);

        glBindFramebuffer(GL_FRAMEBUFFER, pep_BlurPostProcessing_Fbo[horizontal]);
        // ColorAttachment[1] is used for blur processing
        glBindTexture(GL_TEXTURE_2D, first_iteration ? pep_Blur_Fbo_TextureBuffer[1] : pep_BlurPostProcessing_Fbo_TextureBuffer[!horizontal]);

        glBindVertexArray(pep_BlurPostProcessing_Square_vao);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);

        glBindTexture(GL_TEXTURE_2D, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        horizontal = !horizontal;
        if (first_iteration)
        {
            first_iteration = false;
        }
    }

    glUseProgram(0);
}
////////////////////////////////////////////////////////////////////////////////////
// GLOBAL FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////
int Bloom_Initialize(int width, int height)
{
    //
    // Flow:
    // Blur_FBO -> BlurPostProcessing_FBO -> Blur_RenderToTexture
    //
    if (-1 == Blur_FBO_Initialize(width, height))
    {
        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Blur_FBO_Initialize: \n", __DATE__, __TIME__, __FILE__, __LINE__);

        return -1;
    }

    if (-1 == BlurPostProcessing_FBO_Initialize(width, height))
    {
        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - BlurPostProcessing_FBO_Initialize: \n", __DATE__, __TIME__, __FILE__, __LINE__);

        return -1;
    }

    if (-1 == BlurPostProcessing_Initialize(width, height))
    {
        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - BlurPostProcessing_Initialize: \n", __DATE__, __TIME__, __FILE__, __LINE__);

        return -1;
    }

    if (-1 == Blur_RenderToTexture_Initialize())
    {
        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Blur_RenderToTexture_Initialize: \n", __DATE__, __TIME__, __FILE__, __LINE__);

        return -1;
    }

    fprintf(pep_gpFile, "\nBlur\n");

    pep_Bloom_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "\nin vec4 vPosition;" \
        "\nin vec2 vTexCoord;"

         "\nout vec2 out_texcoord;"

        "\nvoid main(void)" \
        "\n{" \
            "\ngl_Position = vPosition;" \
            "\nout_texcoord = vTexCoord;" \
        "\n}";

    glShaderSource(pep_Bloom_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_Bloom_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Bloom_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Bloom_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Bloom_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_Bloom_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "\nin vec2 out_texcoord;" \

        "\nuniform sampler2D u_sampler_nonblur;" \
        "\nuniform sampler2D u_sampler_blur;" \

        "\nout vec4 FragColor;"

        "\nvoid main(void)" \
        "\n{" \
            "\nconst float gamma = 2.2;" \
            "\nfloat exposure = 1.0f;" \

            "\nvec3 blurColor = texture(u_sampler_blur, out_texcoord).rgb;" \
            "\nvec3 nonBlurColor = texture(u_sampler_nonblur, out_texcoord).rgb;" \

            "\nnonBlurColor += blurColor;" \

            "\nvec3 result = vec3(1.0) - exp(-nonBlurColor * exposure);" \

            "\nresult = pow(result, vec3(1.0 / gamma));"

            "\nFragColor = vec4(result, 1.0);" \

            //\nFragColor = texture(u_sampler_blur, out_texcoord);" \
            //"\nFragColor = texture(u_sampler_nonblur, out_texcoord);" \

           // "\nFragColor = texture(u_sampler_blur, out_texcoord) + texture(u_sampler_nonblur, out_texcoord);" \

        "\n}";

    glShaderSource(pep_Bloom_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_Bloom_gFragmentShaderObject);

    glGetShaderiv(pep_Bloom_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Bloom_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Bloom_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Bloom_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_Bloom_gShaderProgramObject, pep_Bloom_gVertexShaderObject);
    glAttachShader(pep_Bloom_gShaderProgramObject, pep_Bloom_gFragmentShaderObject);

    glBindAttribLocation(pep_Bloom_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_Bloom_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_Bloom_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Bloom_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_Bloom_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Bloom_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Bloom_BlurSamplerUniform = glGetUniformLocation(pep_Bloom_gShaderProgramObject, "u_sampler_blur");
    pep_Bloom_NonBlurSamplerUniform = glGetUniformLocation(pep_Bloom_gShaderProgramObject, "u_sampler_nonblur");

    //
    // Square
    //
    const GLfloat squareVertices[] = {
        1.0f,  1.0f,  0.0f,
        -1.0f, 1.0f,  0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f};
    const GLfloat squaretexcoords[] = {
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f};

    glGenVertexArrays(1, &pep_Bloom_Square_vao);
    glBindVertexArray(pep_Bloom_Square_vao);

    glGenBuffers(1, &pep_Bloom_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Bloom_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Bloom_Square_vbo_texcoords);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Bloom_Square_vbo_texcoords);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squaretexcoords), squaretexcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    return 0;
}

void Bloom_Display(int pep_gToggleFboColorAttachement)
{
    // Render

    Blur_RenderToTexture_Display();

    BlurPostProcessing_Display(1/*pep_gToggleFboColorAttachement*/); // ColorAttachment 1 used for blur

    glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

    // We are commenting below line
    // Reason 1: We have aaply perspective while drawing render to texture
    // Reason 3: Because of Reason 1, while Drawing Final Ouput we removed persepctive and matrix calculation from it

    //pep_Perspective_ProjectionMatrix = perspective(45.0f, (GLfloat)pep_gWidth / (GLfloat)pep_gHeight, 0.1f, 100.0f);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT /*| GL_STENCIL_BUFFER_BIT*/);

    glUseProgram(pep_Bloom_gShaderProgramObject);

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_Bloom_NonBlurSamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_Blur_Fbo_TextureBuffer[0]); // Non-Blur ColorBuffer

    glActiveTexture(GL_TEXTURE1);
    glUniform1i(pep_Bloom_BlurSamplerUniform, 1);
    glBindTexture(GL_TEXTURE_2D, pep_BlurPostProcessing_Fbo_TextureBuffer[1]); // Blur ColorBuffer

    glBindVertexArray(pep_Bloom_Square_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    glUseProgram(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    return;
}

void Bloom_Update(void)
{
    return;
}

void Bloom_ReSize(int width, int height)
{
    Blur_FBO_ReSize(width, height);
    BlurPostProcessing_FBO_ReSize(width, height);
    return;
}

void Bloom_Uninitialize(void)
{
    Blur_RenderToTexture_Uninitialize();
    BlurPostProcessing_Uninitialize();
    BlurPostProcessing_FBO_Uinitialize();
    Blur_FBO_Uinitialize();

    if (pep_Bloom_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Bloom_gShaderProgramObject);

        glGetProgramiv(pep_Bloom_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Bloom_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Bloom_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Bloom_gShaderProgramObject);
        pep_Bloom_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}
