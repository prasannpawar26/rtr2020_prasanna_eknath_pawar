#pragma once

#include "Common.h"

HRESULT HumanModel_Initialize(void);
void HumanModel_Display(void);
void HumanModel_Update(void);
void HumanModel_Uninitialize(void);


