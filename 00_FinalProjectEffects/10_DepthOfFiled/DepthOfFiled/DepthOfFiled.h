#pragma once

int DepthOfFiled_Initialize(int width, int height);
void DepthOfFiled_Display(int width, int height);
void DepthOfFiled_Update(void);
void DepthOfFiled_ReSize(int width, int height);
void DepthOfFiled_Uninitialize(void);
