
#include "Common.h"
#include "DepthOfFiled.h"

#define FBO_SIZE    2048

extern FILE     *pep_gpFile;
extern mat4     pep_Perspective_ProjectionMatrix;
extern int      pep_gWidth;
extern int      pep_gHeight;
extern float    pep_DOF_currentTime;
extern float    pep_DOF_camera_position_z;
extern bool     pep_DOF_paused;
extern float    pep_DOF_currentTime;
extern float    pep_DOF_focal_distance;
extern float    pep_DOF_focal_depth;

GLuint          pep_DOF_view_program;
GLuint          pep_DOF_view_program_vs;
GLuint          pep_DOF_view_program_fs;

GLuint          pep_DOF_filter_program;
GLuint          pep_DOF_filter_program_cs;

GLuint          pep_DOF_display_program;
GLuint          pep_DOF_display_program_vs;
GLuint          pep_DOF_display_program_fs;

GLuint          pep_DOF_fbo;
GLuint          pep_DOF_fbo_depth_tex;
GLuint          pep_DOF_fbo_color_tex;
GLuint          pep_DOF_temp_tex;

//
//uniform
//
GLint   pep_DOF_display_program_focal_distance_uniform;
GLint   pep_DOF_display_program_focal_depth_uniform;

GLint   pep_DOF_view_program_mv_matrix_uniform;
GLint   pep_DOF_view_program_proj_matrix_uniform;
GLint   pep_DOF_view_program_full_shading_uniform;
GLint   pep_DOF_view_program_diffuse_albedo_uniform;

GLuint          pep_DOF_display_program_quad_vao;
GLuint          pep_DOF_display_program_quad_vbo_position;

GLuint          pep_DOF_view_program_cube_vao;
GLuint          pep_DOF_view_program_cube_vbo_position;
GLuint          pep_DOF_view_program_cube_vbo_normal;

int ViewProgram_Initialize()
{
    glGenFramebuffers(1, &pep_DOF_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, pep_DOF_fbo);

    glGenTextures(1, &pep_DOF_fbo_depth_tex);

    glGenTextures(1, &pep_DOF_fbo_color_tex);
    glBindTexture(GL_TEXTURE_2D, pep_DOF_fbo_color_tex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, FBO_SIZE, FBO_SIZE);
    //glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, pep_DOF_fbo_color_tex, 0);
    //glTexImage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, FBO_SIZE, FBO_SIZE, 0, GL_RGBA, GL_FLOAT/*GL_BGR, GL_UNSIGNED_BYTE*/, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pep_DOF_fbo_color_tex, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, pep_DOF_fbo_depth_tex);
    glTexStorage2D(GL_TEXTURE_2D, 11, GL_DEPTH_COMPONENT32F, FBO_SIZE, FBO_SIZE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, pep_DOF_fbo_depth_tex, 0);
    

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        fprintf( pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n", __FILE__, __LINE__, __FUNCTION__);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        return -1;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenTextures(1, &pep_DOF_temp_tex);
    glBindTexture(GL_TEXTURE_2D, pep_DOF_temp_tex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, FBO_SIZE, FBO_SIZE);
    glBindTexture(GL_TEXTURE_2D, 0);

    pep_DOF_view_program_vs = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "\nuniform mat4 mv_matrix;"\
        "\nuniform mat4 proj_matrix;" \
        "\nuniform vec3 light_pos = vec3(100.0, 100.0, 100.0);" \

        "\n layout (location = 0) in vec4 position;" \
        "\n layout (location = 2) in vec3 normal;" \

        "\nout VS_OUT" \
        "\n{" \
        "       \nvec3 N;" \
        "       \nvec3 L;" \
        "       \nvec3 V;" \
        "\n}vs_out;" \

        "\nvoid main(void)" \
        "\n{" \
        "       \nvec4 P = mv_matrix * position;"\
        "       \nvs_out.N = mat3(mv_matrix) * normal;"\
        "       \nvs_out.L = light_pos - P.xyz;"\
        "       \nvs_out.V = -P.xyz;"\
        "       \ngl_Position = proj_matrix * P;"\
        "\n}";

    glShaderSource(pep_DOF_view_program_vs, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_DOF_view_program_vs);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_DOF_view_program_vs, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DOF_view_program_vs, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DOF_view_program_vs, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - view Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d view Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_DOF_view_program_fs = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "\nlayout (location = 0) out vec4 FragColor;" \

        "\nin VS_OUT" \
        "\n{" \
        "       \nvec3 N;" \
        "       \nvec3 L;" \
        "       \nvec3 V;" \
        "\n}fs_in;" \

        "\nuniform vec3 diffuse_albedo = vec3(0.9, 0.8, 1.0);" \
        "\nuniform vec3 specular_albedo = vec3(0.7);" \
        "\nuniform float specular_power = 300.0;" \
        "\nuniform bool full_shading = true;" \

        "\nvoid main(void)" \
        "\n{" \
        "       \nvec3 N = normalize(fs_in.N);"\
        "       \nvec3 L = normalize(fs_in.L);"\
        "       \nvec3 V = normalize(fs_in.V);"\
        "       \nvec3 R = reflect(-L, N);"\
        "       \nvec3 diffuse = max(dot(N, L), 0.0) * diffuse_albedo;"\
        "       \nvec3 specular = pow(max(dot(R, V), 0.0), specular_power) * specular_albedo;"\
        "       \nFragColor = vec4(diffuse + specular, fs_in.V.z);" \

        "       \nFragColor = vec4(diffuse_albedo, 1.0f);" \
        "\n}";

    glShaderSource(pep_DOF_view_program_fs, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_DOF_view_program_fs);

    glGetShaderiv(pep_DOF_view_program_fs, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DOF_view_program_fs, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DOF_view_program_fs, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - view Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d view Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DOF_view_program = glCreateProgram();
    glAttachShader(pep_DOF_view_program, pep_DOF_view_program_vs);
    glAttachShader(pep_DOF_view_program, pep_DOF_view_program_fs);

    glBindAttribLocation(pep_DOF_view_program, AMC_ATTRIBUTES_POSITION, "position");
    glBindAttribLocation(pep_DOF_view_program, AMC_ATTRIBUTES_NORMAL, "normal");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_DOF_view_program);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_DOF_view_program, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_DOF_view_program, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_DOF_view_program, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DOF_view_program_proj_matrix_uniform = glGetUniformLocation(pep_DOF_view_program, "proj_matrix");
    pep_DOF_view_program_mv_matrix_uniform = glGetUniformLocation(pep_DOF_view_program, "mv_matrix");
    pep_DOF_view_program_full_shading_uniform = glGetUniformLocation(pep_DOF_view_program, "full_shading");
    pep_DOF_view_program_diffuse_albedo_uniform = glGetUniformLocation(pep_DOF_view_program, "diffuse_albedo");


    const GLfloat cubeVertices[] = {
        // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,  

        // bottom
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,
        // front
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // back
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f, 
        -1.0f, -1.0f, -1.0f, 
        -1.0f, -1.0f, 1.0f, 
    };

    const GLfloat cubeNormals[] = {
        // top surface
        0.0f, 1.0f, 0.0f,  // top-right of top
        0.0f, 1.0f, 0.0f, // top-left of top
        0.0f, 1.0f, 0.0f, // bottom-left of top
        0.0f, 1.0f, 0.0f,  // bottom-right of top

        // bottom surface
        0.0f, -1.0f, 0.0f,  // top-right of bottom
        0.0f, -1.0f, 0.0f,  // top-left of bottom
        0.0f, -1.0f, 0.0f,  // bottom-left of bottom
        0.0f, -1.0f, 0.0f,   // bottom-right of bottom

        // front surface
        0.0f, 0.0f, 1.0f,  // top-right of front
        0.0f, 0.0f, 1.0f, // top-left of front
        0.0f, 0.0f, 1.0f, // bottom-left of front
        0.0f, 0.0f, 1.0f,  // bottom-right of front

        // back surface
        0.0f, 0.0f, -1.0f,  // top-right of back
        0.0f, 0.0f, -1.0f, // top-left of back
        0.0f, 0.0f, -1.0f, // bottom-left of back
        0.0f, 0.0f, -1.0f,  // bottom-right of back

        // left surface
        -1.0f, 0.0f, 0.0f, // top-right of left
        -1.0f, 0.0f, 0.0f, // top-left of left
        -1.0f, 0.0f, 0.0f, // bottom-left of left
        -1.0f, 0.0f, 0.0f, // bottom-right of left

        // right surface
        1.0f, 0.0f, 0.0f,  // top-right of right
        1.0f, 0.0f, 0.0f,  // top-left of right
        1.0f, 0.0f, 0.0f,  // bottom-left of right
        1.0f, 0.0f, 0.0f  // bottom-right of right
    };

    glGenVertexArrays(1, &pep_DOF_view_program_cube_vao);
    glBindVertexArray(pep_DOF_view_program_cube_vao);

    glGenBuffers(1, &pep_DOF_view_program_cube_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_DOF_view_program_cube_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_DOF_view_program_cube_vbo_normal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_DOF_view_program_cube_vbo_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void ViewProgram_Display(int width, int height)
{
    // Render

    //static const GLfloat zeros[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    static float diffuse_albedo[4] ={ 0.6f, 0.4f, 0.9f, 1.0f };
    static double last_time = 0.0;
    static double total_time = 0.0;

    if (!pep_DOF_paused)
        total_time += (pep_DOF_currentTime - last_time);
    last_time = pep_DOF_currentTime;

    const float f = (float)total_time + 30.0f;

    vmath::vec3 view_position = vmath::vec3(0.0f, 0.0f, pep_DOF_camera_position_z);

    vmath::mat4 model_matrix = mat4::identity();
    vmath::mat4 camera_view_matrix = mat4::identity();
    vmath::mat4 camera_proj_matrix = mat4::identity();

    camera_proj_matrix =
        vmath::perspective(
            45.0f,
            (float)width / (float)height,
            0.01f,
            100.0f
        );

    camera_view_matrix =
        vmath::lookat(view_position,
            vmath::vec3(0.0f),
            vmath::vec3(0.0f, 1.0f, 0.0f));

    model_matrix = 
        vmath::translate(0.0f, 0.0f, -7.0f) *
        vmath::rotate(f * 14.5f, 0.0f, 1.0f, 0.0f) *
        vmath::rotate(20.0f, 1.0f, 0.0f, 0.0f) /**
        vmath::translate(0.0f, -4.0f, 0.0f)*/;

    glEnable(GL_DEPTH_TEST);

    //static const GLfloat ones[] = { 1.0f };
    //static const GLfloat zero[] = { 0.0f };
    //static const GLfloat gray[] = { 0.1f, 0.1f, 0.1f, 0.0f };
    static const GLenum attachments[] = { GL_COLOR_ATTACHMENT0 };

    glBindFramebuffer(GL_FRAMEBUFFER, pep_DOF_fbo);
    //glDrawBuffers(1, attachments);
    //glClearBufferfv(GL_COLOR, 0, gray);
    //glClearBufferfv(GL_DEPTH, 0, ones);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, width, height);
    glUseProgram(pep_DOF_view_program);
    glUniformMatrix4fv(pep_DOF_view_program_proj_matrix_uniform, 1, GL_FALSE, camera_proj_matrix);
    glUniformMatrix4fv(pep_DOF_view_program_mv_matrix_uniform, 1, GL_FALSE, camera_view_matrix * model_matrix);
    glUniform3fv(pep_DOF_view_program_diffuse_albedo_uniform, 1, diffuse_albedo);
    glBindVertexArray(pep_DOF_view_program_cube_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);
    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return;
}

void ViewProgram_Uninitialize(void)
{

    if (pep_DOF_view_program_cube_vbo_normal)
    {
        glDeleteBuffers(1, &pep_DOF_view_program_cube_vbo_normal);
        pep_DOF_view_program_cube_vbo_normal = 0;
    }

    if (pep_DOF_view_program_cube_vbo_position)
    {
        glDeleteBuffers(1, &pep_DOF_view_program_cube_vbo_position);
        pep_DOF_view_program_cube_vbo_position = 0;
    }

    if (pep_DOF_view_program_cube_vao)
    {
        glDeleteBuffers(1, &pep_DOF_view_program_cube_vao);
        pep_DOF_view_program_cube_vao = 0;
    }

    if (pep_DOF_view_program)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_DOF_view_program);

        glGetProgramiv(pep_DOF_view_program, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_DOF_view_program, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from pep_DOF_view_program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_DOF_view_program, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader pep_DOF_view_program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_DOF_view_program);
        pep_DOF_view_program = 0;

        glUseProgram(0);
    }

    return;
}

int FilterProgram_Initialize()
{
    pep_DOF_filter_program_cs = glCreateShader(GL_COMPUTE_SHADER);
    const GLchar* computeShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "\nlayout (local_size_x = 1024) in;" \
        "\nshared vec3 shared_data[gl_WorkGroupSize.x * 2];" \

        "\n layout (binding = 0, rgba32f) readonly uniform image2D input_image;" \
        "\n layout (binding = 1, rgba32f) writeonly uniform image2D output_image;" \

        "\nvoid main(void)" \
        "\n{" \
        "       \nuint id = gl_LocalInvocationID.x;" \
        "       \nuint rd_id;" \
        "       \nuint wr_id;" \
        "       \nuint mask;" \
        "       \nivec2 P0 = ivec2(id * 2, gl_WorkGroupID.x);" \
        "       \nivec2 P1 = ivec2(id * 2 + 1, gl_WorkGroupID.x);" \
        "       \nconst uint steps = uint(log2(gl_WorkGroupSize.x)) + 1;" \
        "       \nuint step = 0;" \
        "       \nvec4 i0 = imageLoad(input_image, P0);" \
        "       \nvec4 i1 = imageLoad(input_image, P1);" \

        "       \nshared_data[P0.x] = i0.rgb;" \
        "       \nshared_data[P1.x] = i1.rgb;" \
        "       \nbarrier();" \

        "       \nfor (step = 0; step < steps; step++)" \
        "       \n{" \
        "           \nmask = (1 << step) - 1;" \
        "           \nrd_id = ((id >> step) << (step + 1)) + mask;" \
        "           \nwr_id = rd_id + 1 + (id & mask);" \
        "           \nshared_data[wr_id] += shared_data[rd_id];" \
        "           \nbarrier();" \
        "       \n}" \

        "       \nimageStore(output_image, P0.yx, vec4(shared_data[P0.x], i0.a));" \
        "       \nimageStore(output_image, P1.yx, vec4(shared_data[P1.x], i1.a));" \
        "\n}";

    glShaderSource(pep_DOF_filter_program_cs, 1, (const GLchar**)&computeShaderSourceCode, NULL);
    glCompileShader(pep_DOF_filter_program_cs);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_DOF_filter_program_cs, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DOF_filter_program_cs, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DOF_filter_program_cs, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Compute Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Compute Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DOF_filter_program = glCreateProgram();
    glAttachShader(pep_DOF_filter_program, pep_DOF_filter_program_cs);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_DOF_filter_program);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_DOF_filter_program, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_DOF_filter_program, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_DOF_filter_program, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    return 0;
}

void FilterProgram_Display(int width, int height)
{
    glUseProgram(pep_DOF_filter_program);
    glBindImageTexture(0, pep_DOF_fbo_color_tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
    glBindImageTexture(1, pep_DOF_temp_tex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
    glDispatchCompute(height, 1, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
    glBindImageTexture(0, pep_DOF_temp_tex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
    glBindImageTexture(1, pep_DOF_fbo_color_tex, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
    glDispatchCompute(width, 1, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
    glUseProgram(0);

    return;
}

void FilterProgram_Uninitialize(void)
{
    if (pep_DOF_filter_program)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_DOF_filter_program);

        glGetProgramiv(pep_DOF_filter_program, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_DOF_filter_program, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from pep_DOF_filter_program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_DOF_filter_program, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader pep_DOF_filter_program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_DOF_filter_program);
        pep_DOF_filter_program = 0;

        glUseProgram(0);
    }

    return;
}

int DisplayProgram_Initialize()
{

    pep_DOF_display_program_vs = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "\nvoid main(void)" \
        "\n{" \
        "       \nconst vec4 vertex[] = vec4[] ( vec4(-1.0, -1.0, 0.5, 1.0), vec4( 1.0, -1.0, 0.5, 1.0), vec4(-1.0,  1.0, 0.5, 1.0),vec4( 1.0,  1.0, 0.5, 1.0) );" \
        "       \ngl_Position = vertex[gl_VertexID];" \
        "\n}";

    glShaderSource(pep_DOF_display_program_vs, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_DOF_display_program_vs);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_DOF_display_program_vs, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DOF_display_program_vs, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DOF_display_program_vs, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_DOF_display_program_fs = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "\nlayout (binding = 0) uniform sampler2D input_image;" \
        "\nlayout (location = 0) out vec4 FragColor;" \

        "\nuniform float pep_DOF_focal_distance = 50.0;" \
        "\nuniform float pep_DOF_focal_depth = 30.0;" \

        "\nvoid main(void)" \
        "\n{" \
        "\nvec2 s = 1.0 / textureSize(input_image, 0);" \
        "\nvec2 C = gl_FragCoord.xy;" \

        "\nvec4 v = texelFetch(input_image, ivec2(gl_FragCoord.xy), 0).rgba;" \
        "\n float m;" \

        "\nif (v.w == 0.0) {" \
        "\n     m = 0.5;" \
        "\n}" \
        "\nelse {" \
        "\n m = abs(v.w - pep_DOF_focal_distance);" \
        "\n m = 0.5 + smoothstep(0.0, pep_DOF_focal_depth, m) * 7.5;" \
        "\n}" \

        "\n vec2 P0 = vec2(C * 1.0) + vec2(-m, -m);" \
        "\n vec2 P1 = vec2(C * 1.0) + vec2(-m, m);" \
        "\n vec2 P2 = vec2(C * 1.0) + vec2(m, -m);" \
        "\n vec2 P3 = vec2(C * 1.0) + vec2(m, m);" \

        "\n P0 *= s;" \
        "\n P1 *= s;" \
        "\n P2 *= s;" \
        "\n P3 *= s;" \

        "\n vec3 a = textureLod(input_image, P0, 0).rgb;" \
        "\n vec3 b = textureLod(input_image, P1, 0).rgb;" \
        "\n vec3 c = textureLod(input_image, P2, 0).rgb;" \
        "\n vec3 d = textureLod(input_image, P3, 0).rgb;" \

        "\n vec3 f = a - b - c + d;" \

        "\n m *= 2;" \

        "\n f /= float(m * m);"

        "\nFragColor = vec4(f, 1.0);" \
        "\n}";

    glShaderSource(pep_DOF_display_program_fs, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_DOF_display_program_fs);

    glGetShaderiv(pep_DOF_display_program_fs, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_DOF_display_program_fs, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_DOF_display_program_fs, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DOF_display_program = glCreateProgram();
    glAttachShader(pep_DOF_display_program, pep_DOF_display_program_vs);
    glAttachShader(pep_DOF_display_program, pep_DOF_display_program_fs);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_DOF_display_program);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_DOF_display_program, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_DOF_display_program, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_DOF_display_program, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_DOF_display_program_focal_distance_uniform = glGetUniformLocation(pep_DOF_display_program, "pep_DOF_focal_distance");
    pep_DOF_display_program_focal_depth_uniform = glGetUniformLocation(pep_DOF_display_program, "pep_DOF_focal_depth");

    //
    // Square
    //
    //const GLfloat squareVertices[] = {
    //    1.0f,  1.0f,  0.0f,
    //    -1.0f, 1.0f,  0.0f,
    //    -1.0f, -1.0f, 0.0f,
    //    1.0f, -1.0f, 0.0f};
    //const GLfloat squaretexcoords[] = {
    //    1.0f, 1.0f,
    //    0.0f, 1.0f,
    //    0.0f, 0.0f,
    //    1.0f, 0.0f};

    glGenVertexArrays(1, &pep_DOF_display_program_quad_vao);
    glBindVertexArray(pep_DOF_display_program_quad_vao);

    //glGenBuffers(1, &pep_DOF_display_program_quad_vbo_position);
    //glBindBuffer(GL_ARRAY_BUFFER, pep_DOF_display_program_quad_vbo_position);
    //glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    //glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    //glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return 0;
}

void DisplayProgram_Display()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, pep_DOF_fbo_color_tex);
    glDisable(GL_DEPTH_TEST);

    glUseProgram(pep_DOF_display_program);
    glUniform1f(pep_DOF_display_program_focal_distance_uniform, pep_DOF_focal_distance);
    glUniform1f(pep_DOF_display_program_focal_depth_uniform, pep_DOF_focal_depth);
    glBindVertexArray(pep_DOF_display_program_quad_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
    glUseProgram(0);

    return;
}

void DisplayProgram_Uninitialize(void)
{
    if (pep_DOF_display_program_quad_vbo_position)
    {
        glDeleteBuffers(1, &pep_DOF_display_program_quad_vbo_position);
        pep_DOF_display_program_quad_vbo_position = 0;
    }

    if (pep_DOF_display_program_quad_vao)
    {
        glDeleteBuffers(1, &pep_DOF_display_program_quad_vao);
        pep_DOF_display_program_quad_vao = 0;
    }

    if (pep_DOF_display_program)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_DOF_display_program);

        glGetProgramiv(pep_DOF_display_program, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_DOF_display_program, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from pep_DOF_display_program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_DOF_display_program, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader pep_DOF_display_program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_DOF_display_program);
        pep_DOF_display_program = 0;

        glUseProgram(0);
    }

    return;
}

////////////////////////////////////////////////////////////////////////////////////
// GLOBAL FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////
int DepthOfFiled_Initialize(int width, int height)
{
    //
    // Flow:
    // Blur_FBO -> BlurPostProcessing_FBO -> Blur_RenderToTexture
    //

    //glEnable(GL_DEPTH_TEST);

    fprintf(pep_gpFile, "\nBlur\n");

    ViewProgram_Initialize();

    FilterProgram_Initialize();

    DisplayProgram_Initialize();

    return 0;
}

void DepthOfFiled_Display(int width, int height)
{
    ViewProgram_Display(width, height);

    FilterProgram_Display(width, height);

    DisplayProgram_Display();

    return;
}

void DepthOfFiled_Update(void)
{
    return;
}

void DepthOfFiled_ReSize(int width, int height)
{
    
    return;
}

void DepthOfFiled_Uninitialize(void)
{
    DisplayProgram_Uninitialize();

    FilterProgram_Uninitialize();

    ViewProgram_Uninitialize();

    return;
}
