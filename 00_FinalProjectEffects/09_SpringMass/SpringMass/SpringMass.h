#pragma once

int SpringMass_Initialize(void);
void SpringMass_Display(int, int);
void SpringMass_Update(void);
void SpringMass_ReSize(int width, int height);
void SpringMass_Uninitialize(void);
