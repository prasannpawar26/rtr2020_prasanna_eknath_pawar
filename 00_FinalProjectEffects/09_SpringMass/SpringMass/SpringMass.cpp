
// position
// mass -> never changes
// velocity -> Speed
// accelleration = force / mass (change in speed)
// displacement =
// F = mass*(velocity / timestep) 
// F = springconstant (x � x0)
// Final velocity = initial velocity + accelleration * timestep

/*

1. estimate the **forces** caused by the displacement of its neighboring particles
2. apply the sum of the forces to our estimate of the particle's motion

we must understand how the force will change with respect to a change in position of the neighboring particles

damping force = damping constant * velocity  * (accelleration ?)


Cloth motion = Force (internal) + Force (external)

*/

#include "Common.h"
#include "SpringMass.h"

extern FILE *pep_gpFile;
extern bool pep_bFadeOut;
extern mat4 pep_Perspective_ProjectionMatrix;
extern int pep_AssignmentDisplay;

enum BUFFER_TYPE_t
{
    POSITION_A,
    POSITION_B,
    VELOCITY_A,
    VELOCITY_B,
    CONNECTION
};

enum
{
    POINTS_X            = 25,
    POINTS_Y            = 25,
    POINTS_TOTAL        = (POINTS_X * POINTS_Y),
    CONNECTIONS_TOTAL   = (POINTS_X - 1) * POINTS_Y + (POINTS_Y - 1) * POINTS_X
};

GLuint          m_vao[2];
GLuint          m_vbo[5];
GLuint          m_index_buffer;
GLuint          m_pos_tbo[2];

GLuint          m_C_loc;
GLuint          m_iteration_index;

extern bool            draw_points;
extern bool            draw_lines;
extern int             iterations_per_frame;

GLuint pep_SpringMass_m_update_program_vertex;
GLuint pep_SpringMass_m_update_program_fragment;
GLuint pep_SpringMass_m_update_program;

GLuint pep_SpringMass_tex_position_uniform;
GLuint pep_SpringMass_timestep_uniform;
GLuint pep_SpringMass_force_external_uniform;
GLuint pep_SpringMass_springconstant_uniform;
GLuint pep_SpringMass_dampingconstant_uniform;
GLuint pep_SpringMass_rest_length_uniform;

extern float timestep;
extern float springconstant;
extern float dampingconstant;
extern float rest_length;
extern float force_external;

bool bTimeStep = false;

GLuint pep_SpringMass_m_render_program_vertex;
GLuint pep_SpringMass_m_render_program_fragment;
GLuint pep_SpringMass_m_render_program;

int UpdateShader_Initialize(void)
{
    // velocity = Speed
    // accelleration =  increasing the speed
    // length of vector =  magnitude of a vector
    // 

    pep_SpringMass_m_update_program_vertex = glCreateShader(GL_VERTEX_SHADER);

    const GLchar* m_update_program_vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "layout (location = 0) in vec4 position_mass;" \
        "layout (location = 1) in vec3 velocity;" \
        "layout (location = 2) in ivec4 connection;" \

        "uniform samplerBuffer tex_position;" \

        "uniform float force_external;" \
        "uniform float timestep;" \
        "uniform float springconstant;" \
        "uniform float dampingconstant;" \
        "uniform float rest_length;" \

        "const vec3 gravity = vec3(0.0, -0.29, 0.0);" \

        "out vec4 tf_position_mass;" \
        "out vec3 tf_velocity;" \

        "void main(void)" \
        "{" \
        "   vec3 position = position_mass.xyz;" \
        "   float mass = position_mass.w;" \
        "   vec3 initialvelocity = velocity;" \

        "   vec3 force = gravity * mass - dampingconstant * initialvelocity;" \

        // Becomes false when force is applied
        "   bool fixed_node = true;" \

        /* Below For Loop
        1. estimate the **forces** caused by the displacement of its neighboring particles
        2. apply the sum of the forces to our estimate of the particle's motion
        */

        "   for (int i = 0; i < 4; i++) {" \
        "       if (connection[i] != -1) {" \
        "           vec3 othervertexposition = texelFetch(tex_position, connection[i]).xyz;" \
        "           vec3 d = othervertexposition - position;" \
        "           float x = length(d);" \
        "           force += -springconstant * (rest_length - x) * normalize(d);" \
        "           force += force_external;"
        "           fixed_node = false;" \
        "       }" \
        "   }" \

        // normalized direction along the spring = normalize(d)
        // 
        //BEcause of below line spring mass position is getting steady.

        "   if (fixed_node) {" \
        "       force = vec3(0.0);" \
        "   }" \

        "   vec3 accelleration = (force / mass);" \
        "   vec3 displacement = initialvelocity * timestep + 0.5 * accelleration * timestep * timestep;" \
        "   vec3 finalvelocity = initialvelocity + accelleration * timestep;" \

     //   "   displacement = clamp(displacement, vec3(-25.0), vec3(25.0));" \

        "   tf_position_mass = vec4(position + displacement, mass);" \
        "   tf_velocity = finalvelocity;" \
        "}";

    // position
    // mass -> never changes
    // velocity
    // accelleration = force / mass
    // displacement =
    // F = mass*(velocity / timestep) 
    // F = springconstant (x � x0)
    // Final velocity = initial velocity + accelleration * timestep


    glShaderSource(pep_SpringMass_m_update_program_vertex, 1, (const GLchar**)&m_update_program_vertexShaderSourceCode, NULL);
    glCompileShader(pep_SpringMass_m_update_program_vertex);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_SpringMass_m_update_program_vertex, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SpringMass_m_update_program_vertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SpringMass_m_update_program_vertex, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_SpringMass_m_update_program_fragment = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* m_update_program_fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "out vec4 Color;" \

        "void main(void)" \
        "{" \
        "  Color = vec4(1.0);" \
        "}";

    glShaderSource(pep_SpringMass_m_update_program_fragment, 1, (const GLchar**)&m_update_program_fragmentShaderSourceCode, NULL);
    glCompileShader(pep_SpringMass_m_update_program_fragment);

    glGetShaderiv(pep_SpringMass_m_update_program_fragment, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SpringMass_m_update_program_fragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SpringMass_m_update_program_fragment, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SpringMass_m_update_program = glCreateProgram();
    glAttachShader(pep_SpringMass_m_update_program, pep_SpringMass_m_update_program_vertex);
    glAttachShader(pep_SpringMass_m_update_program, pep_SpringMass_m_update_program_fragment);

    glBindAttribLocation(pep_SpringMass_m_update_program, AMC_ATTRIBUTES_POSITION, "position_mass");
    glBindAttribLocation(pep_SpringMass_m_update_program, AMC_ATTRIBUTES_VELOCITY, "velocity");
    glBindAttribLocation(pep_SpringMass_m_update_program, AMC_ATTRIBUTES_CONNECTION, "connection");

    static const char * tf_varyings[] = 
    {
        "tf_position_mass",
        "tf_velocity"
    };
    glTransformFeedbackVaryings(pep_SpringMass_m_update_program, 2, tf_varyings, GL_SEPARATE_ATTRIBS);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_SpringMass_m_update_program);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_SpringMass_m_update_program, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_SpringMass_m_update_program, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_SpringMass_m_update_program, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SpringMass_tex_position_uniform = glGetUniformLocation(pep_SpringMass_m_update_program, "tex_position");
    pep_SpringMass_timestep_uniform = glGetUniformLocation(pep_SpringMass_m_update_program, "timestep");
    pep_SpringMass_force_external_uniform = glGetUniformLocation(pep_SpringMass_m_update_program, "force_external");
    pep_SpringMass_springconstant_uniform = glGetUniformLocation(pep_SpringMass_m_update_program, "springconstant");
    pep_SpringMass_dampingconstant_uniform = glGetUniformLocation(pep_SpringMass_m_update_program, "dampingconstant");
    pep_SpringMass_rest_length_uniform = glGetUniformLocation(pep_SpringMass_m_update_program, "rest_length");

    return 0;
}

int UpdateShader_Uninitialize(void)
{
    if (pep_SpringMass_m_update_program)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_SpringMass_m_update_program);

        glGetProgramiv(pep_SpringMass_m_update_program, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_SpringMass_m_update_program, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_SpringMass_m_update_program, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_SpringMass_m_update_program);
        pep_SpringMass_m_update_program = 0;

        glUseProgram(0);
    }

    return 0;
}

int RenderShader_Initialize(void)
{
    pep_SpringMass_m_render_program_vertex = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* m_render_program_vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "layout (location = 0) in vec3 position;" \

        "void main(void)" \
        "{" \
        "   gl_Position = vec4(position * 0.03, 1.0);" \
        "}";

    glShaderSource(pep_SpringMass_m_render_program_vertex, 1, (const GLchar**)&m_render_program_vertexShaderSourceCode, NULL);
    glCompileShader(pep_SpringMass_m_render_program_vertex);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_SpringMass_m_render_program_vertex, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SpringMass_m_render_program_vertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SpringMass_m_render_program_vertex, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_SpringMass_m_render_program_fragment = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* m_render_program_fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "layout (location = 0) out vec4 FragColor;" \

        "void main(void)" \
        "{" \
        "  FragColor = vec4(1.0);" \
        "}";

    glShaderSource(pep_SpringMass_m_render_program_fragment, 1, (const GLchar**)&m_render_program_fragmentShaderSourceCode, NULL);
    glCompileShader(pep_SpringMass_m_render_program_fragment);

    glGetShaderiv(pep_SpringMass_m_render_program_fragment, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SpringMass_m_render_program_fragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SpringMass_m_render_program_fragment, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SpringMass_m_render_program = glCreateProgram();
    glAttachShader(pep_SpringMass_m_render_program, pep_SpringMass_m_render_program_vertex);
    glAttachShader(pep_SpringMass_m_render_program, pep_SpringMass_m_render_program_fragment);

    glBindAttribLocation(pep_SpringMass_m_render_program, AMC_ATTRIBUTES_POSITION, "position");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_SpringMass_m_render_program);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_SpringMass_m_render_program, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_SpringMass_m_render_program, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_SpringMass_m_render_program, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    return 0;
}

int RenderShader_Uninitialize(void)
{
    if (pep_SpringMass_m_render_program)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_SpringMass_m_render_program);

        glGetProgramiv(pep_SpringMass_m_render_program, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_SpringMass_m_render_program, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_SpringMass_m_render_program, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_SpringMass_m_render_program);
        pep_SpringMass_m_render_program = 0;

        glUseProgram(0);
    }

    return 0;
}

int SpringMass_Initialize(void)
{
    fprintf(pep_gpFile, "\nSpringMass\n");

    UpdateShader_Initialize();
    RenderShader_Initialize();

    //  The application fixes a few of the vertices in place so that the whole system
    // doesn�t just fall off the bottom of the screen

    //
    // The code to set up the initial positions and velocities
    // of each node and the connection vectors for our spring mass system
    //
    int i, j;
    vmath::vec4 * initial_positions = new vmath::vec4 [POINTS_TOTAL];
    vmath::vec3 * initial_velocities = new vmath::vec3 [POINTS_TOTAL];
    vmath::ivec4 * connection_vectors = new vmath::ivec4 [POINTS_TOTAL];

    int n = 0;

    for (j = 0; j < POINTS_Y; j++)
    {
        float fj = (float)j / (float)POINTS_Y;

        for (i = 0; i < POINTS_X; i++)
        {
            float fi = (float)i / (float)POINTS_X;

            initial_positions[n] = 
                vmath::vec4(
                    (fi - 0.5f) * (float)POINTS_X,
                    (fj - 0.5f) * (float)POINTS_Y,
                    0.6f * sinf(fi) * cosf(fj),
                    1.0f);

            initial_velocities[n] = vmath::vec3(0.0f);

            connection_vectors[n] = vmath::ivec4(-1);

            if (j != (POINTS_Y - 1))
            {
                if (i != 0)
                {
                    connection_vectors[n][0] = n - 1;
                }

                if (j != 0)
                {
                    connection_vectors[n][1] = n - POINTS_X;
                }

                if (i != (POINTS_X - 1))
                {
                    connection_vectors[n][2] = n + 1;
                }

                if (j != (POINTS_Y - 1))
                {
                    connection_vectors[n][3] = n + POINTS_X;
                }
            }

            n++;
        }
    }

    glGenVertexArrays(2, m_vao);
    glGenBuffers(5, m_vbo);

    for (i = 0; i < 2; i++)
    {
        glBindVertexArray(m_vao[i]);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[POSITION_A + i]);
        glBufferData(GL_ARRAY_BUFFER, POINTS_TOTAL * sizeof(vmath::vec4), initial_positions, GL_DYNAMIC_COPY);
        glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 4, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[VELOCITY_A + i]);
        glBufferData(GL_ARRAY_BUFFER, POINTS_TOTAL * sizeof(vmath::vec3), initial_velocities, GL_DYNAMIC_COPY);
        glVertexAttribPointer(AMC_ATTRIBUTES_VELOCITY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTES_VELOCITY);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[CONNECTION]);
        glBufferData(GL_ARRAY_BUFFER, POINTS_TOTAL * sizeof(vmath::ivec4), connection_vectors, GL_STATIC_DRAW);
        glVertexAttribIPointer(AMC_ATTRIBUTES_CONNECTION, 4, GL_INT, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTES_CONNECTION);
    }

    delete [] connection_vectors;
    delete [] initial_velocities;
    delete [] initial_positions;

    glGenTextures(2, m_pos_tbo);

    glBindTexture(GL_TEXTURE_BUFFER, m_pos_tbo[0]);
    glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_vbo[POSITION_A]);
    glBindTexture(GL_TEXTURE_BUFFER, 0);

    glBindTexture(GL_TEXTURE_BUFFER, m_pos_tbo[1]);
    glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, m_vbo[POSITION_B]);
    glBindTexture(GL_TEXTURE_BUFFER, 0);

    int lines = (POINTS_X - 1) * POINTS_Y + (POINTS_Y - 1) * POINTS_X;

    glGenBuffers(1, &m_index_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, lines * 2 * sizeof(int), NULL, GL_STATIC_DRAW);

    int * e = (int *)glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, lines * 2 * sizeof(int), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

    for (j = 0; j < POINTS_Y; j++)  
    {
        for (i = 0; i < POINTS_X - 1; i++)
        {
            *e++ = i + j * POINTS_X;
            *e++ = 1 + i + j * POINTS_X;
        }
    }

    for (i = 0; i < POINTS_X; i++)
    {
        for (j = 0; j < POINTS_Y - 1; j++)
        {
            *e++ = i + j * POINTS_X;
            *e++ = POINTS_X + i + j * POINTS_X;
        }
    }

   glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

    return 0;
}

void SpringMass_Display(int width, int height)
{
    int i;

    glUseProgram(pep_SpringMass_m_update_program);

    glUniform1f(pep_SpringMass_timestep_uniform, timestep);
    glUniform1f(pep_SpringMass_force_external_uniform, force_external);
    glUniform1f(pep_SpringMass_springconstant_uniform, springconstant);
    glUniform1f(pep_SpringMass_dampingconstant_uniform, dampingconstant);
    glUniform1f(pep_SpringMass_rest_length_uniform, rest_length);

    // we use transform feedback to store the
    // positions and velocities of each of the masses between each iteration of the algorithm
    //
    // For each vertex, we need a position, velocity, and mass
    glEnable(GL_RASTERIZER_DISCARD);

    for (i = iterations_per_frame; i != 0; --i)
    {
        glBindVertexArray(m_vao[m_iteration_index & 1]);
        glBindTexture(GL_TEXTURE_BUFFER, m_pos_tbo[m_iteration_index & 1]);
        m_iteration_index++;
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_vbo[POSITION_A + (m_iteration_index & 1)]);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, m_vbo[VELOCITY_A + (m_iteration_index & 1)]);
        glBeginTransformFeedback(GL_POINTS);
        glDrawArrays(GL_POINTS, 0, POINTS_TOTAL);
        glEndTransformFeedback();
    }

    glDisable(GL_RASTERIZER_DISCARD);
    glUseProgram(0);

    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    static const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    glClearBufferfv(GL_COLOR, 0, black);

    glUseProgram(pep_SpringMass_m_render_program);

    if (draw_points)
    {
        glPointSize(4.0f);
        glDrawArrays(GL_POINTS, 0, POINTS_TOTAL);
    }

    if (draw_lines)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_index_buffer);
        glDrawElements(GL_LINES, CONNECTIONS_TOTAL * 2, GL_UNSIGNED_INT, NULL);
    }

    glUseProgram(0);

    return;
}

void SpringMass_Update(void)
{
    return;
}

void SpringMass_ReSize(int width, int height)
{

}

void SpringMass_Uninitialize(void)
{
    glDeleteBuffers(5, m_vbo);
    glDeleteVertexArrays(2, m_vao);

    RenderShader_Uninitialize();
    UpdateShader_Uninitialize();

    return;
}

