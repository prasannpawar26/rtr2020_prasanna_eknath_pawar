
#include "Common.h"
#include "GodRays_Version2.h"
#include "Sphere.h"

extern int pep_gWidth;
extern int pep_gHeight;
extern FILE* gpFile;
extern float xTran;

GLuint pep_RTT_gVertexShaderObject;
GLuint pep_RTT_gFragmentShaderObject;
GLuint pep_RTT_gShaderProgramObject;

//Variable 
GLuint pep_RTT_vaoSphere;
GLuint pep_RTT_vboSpherePosition;
GLuint pep_RTT_vboSphereNormals;
GLuint pep_RTT_vboSphereElements;

GLuint pep_RTT_modelMatrixUniform;
GLuint pep_RTT_viewMatrixUniform;
GLuint pep_RTT_projectionMatrixUniform;
GLuint pep_RTT_ldUniform;
GLuint pep_RTT_laUniform;
GLuint pep_RTT_lsUniform;
GLuint pep_RTT_kdUniform;
GLuint pep_RTT_kaUniform;
GLuint pep_RTT_ksUniform;
GLuint pep_RTT_shininessMaterialUniform;
GLuint pep_RTT_lightPositionUniform;
GLuint pep_RTT_lIsPressed;
mat4 pep_RTT_perspectiveProjectionMatrix;


float pep_RTT_sphere_vertices[1146];
float pep_RTT_sphere_normals[1146];
float pep_RTT_sphere_texture[764];
unsigned short pep_RTT_sphere_elements[2280];

GLint pep_RTT_gNumVertices;
GLint pep_RTT_gNumElements;

GLfloat pep_RTT_lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat pep_RTT_lightDiffuse[4] = { 1.0f, .50f, .0f, 1.0f };
GLfloat pep_RTT_lightSpecular[4] = { 1.0f, .50f, .0f, 1.0f };
GLfloat pep_RTT_lightPosition[4] = { 0.0f, 0.0f, 10.0f, 1.0f };

GLfloat pep_RTT_materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat pep_RTT_materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat pep_RTT_materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat pep_RTT_materialShininess = 120.0f;

GLuint pep_RTT_ColorBuffersTexture;
GLuint pep_RTT_DepthBuffersTexture;
GLuint pep_RTT_FBO;

//GLuint pep_RTT_Scene_gVertexShaderObject;
//GLuint pep_RTT_Scene_gFragmentShaderObject;
//GLuint pep_RTT_Scene_gShaderProgramObject;
//GLuint pep_RTT_Scene_modelMatrixUniform;
//GLuint pep_RTT_Scene_viewMatrixUniform;
//GLuint pep_RTT_Scene_projectionMatrixUniform;
GLuint giVertexShaderObject;
GLuint giFragmentShaderObject;
GLuint giShaderProgramObject;
GLuint vao_rectangle;
GLuint vbo_rectangle;
GLuint mvpUniform;  // UNIFORM

GLuint pep_RTT_Scene_ColorBuffersTexture;
GLuint pep_RTT_Scene_DepthBuffersTexture;
GLuint pep_RTT_Scene_FBO;

//GLuint pep_RTT_Scene_vaoQuad;
//GLuint pep_RTT_Scene_vboQuad;
mat4 pep_RTT_Scene_perspectiveProjectionMatrix;

GLuint pep_GodRays_gVertexShaderObject;
GLuint pep_GodRays_gFragmentShaderObject;
GLuint pep_GodRays_gShaderProgramObject;

GLuint pep_GodRays_vaoQuad;
GLuint pep_GodRays_vboQuad;
GLuint pep_GodRays_vboTexcoordsQuad;
GLuint pep_GodRays_SamplerUniform;
GLuint pep_GodRays_SceneUniform;

int RTT_Scene_Initialize()
{
	glGenFramebuffers(1, &pep_RTT_Scene_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, pep_RTT_Scene_FBO);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glGenTextures(1, &pep_RTT_Scene_ColorBuffersTexture);
	glBindTexture(GL_TEXTURE_2D, pep_RTT_Scene_ColorBuffersTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, pep_gWidth, pep_gHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pep_RTT_Scene_ColorBuffersTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &pep_RTT_Scene_DepthBuffersTexture);
	glBindTexture(GL_TEXTURE_2D, pep_RTT_Scene_DepthBuffersTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, pep_gWidth, pep_gHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, pep_RTT_Scene_DepthBuffersTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n", __FILE__, __LINE__, __FUNCTION__);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return -1;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//define Vertex Shader Object
	giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	if (0 == giVertexShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed\n",
			__FILE__, __LINE__, __FUNCTION__);

		return -6;
	}

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	glShaderSource(giVertexShaderObject, 1,
		(const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(giVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(giVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (GL_FALSE == iShaderCompileStatus) {
		glGetShaderiv(giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giVertexShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
				return -7;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == giFragmentShaderObject) {
		fprintf(gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
			"Failed:\n\t%s\n",
			__FILE__, __LINE__, __FUNCTION__, szInfoLog);

		return -8;
	}

	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(0.0, 0.0, 1.0, 1.0);" \
		"}";

	glShaderSource(giFragmentShaderObject, 1,
		(const GLchar **)&fragmentShaderSourceCode, NULL);

	glCompileShader(giFragmentShaderObject);

	glGetShaderiv(giFragmentShaderObject, GL_COMPILE_STATUS,
		&iShaderCompileStatus);
	if (FALSE == iShaderCompileStatus) {
		glGetShaderiv(giFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giFragmentShaderObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(
					gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
				return -9;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	giShaderProgramObject = glCreateProgram();

	glAttachShader(giShaderProgramObject, giVertexShaderObject);
	glAttachShader(giShaderProgramObject, giFragmentShaderObject);

	glBindAttribLocation(giShaderProgramObject, AMC_ATTRIBUTES_POSITION,
		"vPosition");

	glLinkProgram(giShaderProgramObject);

	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (GL_FALSE == iProgramLinkStatus) {
		glGetShaderiv(giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (0 < iInfoLogLength) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (NULL != szInfoLog) {
				GLsizei written;

				glGetShaderInfoLog(giShaderProgramObject, iInfoLogLength, &written,
					szInfoLog);

				fprintf(gpFile,
					"FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
					"Failed:\n\t%s\n",
					__FILE__, __LINE__, __FUNCTION__, szInfoLog);

				free(szInfoLog);
				return -10;
			}
		}
	}
	fprintf(gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	mvpUniform = glGetUniformLocation(giShaderProgramObject, "u_mvp_matrix");

	// RECTANGLE
	const GLfloat rectangleVertices[] = {1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f,
		-1.0f, -1.0f, 0.0f, 1.0f,  -1.0f, 0.0f};

	glGenVertexArrays(1, &vao_rectangle);

	glBindVertexArray(vao_rectangle);

	glGenBuffers(1, &vbo_rectangle);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
		NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	pep_RTT_Scene_perspectiveProjectionMatrix = mat4::identity();

	return 0;
}


void RTT_Scene_Display()
{
	glBindFramebuffer(GL_FRAMEBUFFER, pep_RTT_Scene_FBO);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

	pep_RTT_Scene_perspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)pep_gWidth / (GLfloat)pep_gHeight), 0.1f, 100.0f);

	glUseProgram(giShaderProgramObject);
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(xTran, 0.0f, -5.75f)* scale(0.40f, 0.40f, 0.40f);
	modelViewProjectionMatrix = pep_RTT_Scene_perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(vao_rectangle);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return;
}

void RTT_Scene_Resize(int width, int height)
{
	if (pep_RTT_Scene_FBO)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pep_RTT_Scene_FBO);
		glBindTexture(GL_TEXTURE_2D, pep_RTT_Scene_ColorBuffersTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindTexture(GL_TEXTURE_2D, pep_RTT_Scene_DepthBuffersTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void RTT_Scene_Uninitialize(void)
{
	if (pep_RTT_Scene_FBO)
	{
		glDeleteFramebuffers(1, &pep_RTT_Scene_FBO);
		pep_RTT_Scene_FBO = 0;
	}

	if (vbo_rectangle)
	{
		glDeleteVertexArrays(1, &vbo_rectangle);
		vbo_rectangle = 0;
	}

	if (vao_rectangle)
	{
		glDeleteVertexArrays(1, &vao_rectangle);
		vao_rectangle = 0;
	}

	if (giShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(giShaderProgramObject);

		//Ask Program how many shader are Attached to You
		glGetProgramiv(giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if (pShader)
		{
			glGetAttachedShaders(giShaderProgramObject, shaderCount, &shaderCount, pShader);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(giShaderProgramObject, pShader[shaderNumber]);

				//Delete detach Shader
				glDeleteShader(pShader[shaderNumber]);
				pShader[shaderNumber] = 0;
			}
			free(pShader);
		}
		glDeleteProgram(giShaderProgramObject);
		giShaderProgramObject = 0;

		glUseProgram(0);
	}
}

int RTT_Initialize(void)
{

	//define Vertex Shader Object
	pep_RTT_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode =
	{
		" #version 450 core																			\n"
		" 																							\n"
		" in vec4 vPosition;																		\n"
		" in vec3 vNormal;																			\n"
		" 																							\n"
		" uniform mat4 u_m_matrix;																	\n"
		" uniform mat4 u_v_matrix;																	\n"
		" uniform mat4 u_projection;																\n"
		" uniform int u_lKeyPressed;																\n"
		" uniform vec4 u_light_position;															\n"
		" 																							\n"
		" out vec3 tNorm;																			\n"
		" out vec3 lightDirection;																	\n"
		" out vec3 viewer_vector;																	\n"
		" 																							\n"
		" void main(void)																			\n"
		" { 																						\n"
		"																							\n"
		"	if(u_lKeyPressed == 1)																	\n"
		"	{																						\n"
		"		vec4 eye_coordinate = u_v_matrix  * u_m_matrix  * vPosition;						\n"
		"		tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);							\n"
		"		lightDirection = normalize(vec3(u_light_position - eye_coordinate));				\n"
		"		viewer_vector = normalize(vec3(-eye_coordinate.xyz));								\n"
		"	}																						\n"
		"																							\n"
		"	gl_Position =u_projection * u_v_matrix * u_m_matrix * vPosition;						\n"
		" }  																						\n"
	};

	//Specify above source code to vertex shader object 
	glShaderSource(pep_RTT_gVertexShaderObject, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	//compile the vertex shader
	glCompileShader(pep_RTT_gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iLogInfoLength = 0;
	GLchar* szInfoLog = NULL;
	GLint iShaderProgramLinkStatus = 0;

	//Code
	glGetShaderiv(pep_RTT_gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(pep_RTT_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_RTT_gVertexShaderObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}

	//define Fragment Shader Object
	pep_RTT_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Vertex Shader Code
	const GLchar* fragmentShaderSourceCode =
	{
		"	#version 450 core																													\n"
		"																																		\n"
		"	in vec3 tNorm;																														\n"
		"	in vec3 lightDirection;																												\n"
		"	in vec3 viewer_vector;																												\n"
		"																																		\n"
		"	uniform vec3 u_la;																													\n"
		"	uniform vec3 u_ld;																													\n"
		"	uniform vec3 u_ls;																													\n"
		"	uniform vec3 u_ka;																													\n"
		"	uniform vec3 u_kd;																													\n"
		"	uniform vec3 u_ks;																													\n"
		"	uniform float u_material_shininess;																									\n"
		"	uniform int u_lKeyPressed;																											\n"
		"																																		\n"
		"	out vec4 fragColor;																													\n"
		"	void main(void)																														\n"
		"	{																																	\n"
		"		vec3 phong_ADS_light;																											\n"
		"		if(u_lKeyPressed == 1)																											\n"
		"		{																																\n"
		"			vec3 normalized_transformed_normals = normalize(tNorm);																		\n"
		"			vec3 normalized_light_direction = normalize(lightDirection);																\n"
		"			vec3 normalized_viewer_vector = normalize(viewer_vector);																	\n"
		"			vec3 ambient = u_la * u_ka;																									\n"
		"			float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);									\n"
		"			vec3  diffuse = u_ld * u_kd * tn_dot_ld;																					\n"
		"			vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals );								\n"
		"			vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0), u_material_shininess);			\n"
		"			phong_ADS_light = ambient + diffuse + specular;																				\n"
		"		}																																\n"
		"		else																															\n"
		"		{																																\n"
		"			phong_ADS_light = vec3(1.0, 1.0, 1.0);																						\n"
		"		}																																\n"
		"	fragColor = vec4(phong_ADS_light, 1.0) * vec4(1.0f, 0.90f, 0.80f, 1.0f);																								\n"
		//"	fragColor = vec4(1.0f, 0.90f, 0.80f, 1.0f);																								\n"
		"	}																																	\n"
	};

	//Specify above source code to vertex shader object 
	glShaderSource(pep_RTT_gFragmentShaderObject, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);
	fprintf(gpFile, "\n\nAfter Fragment Shader :\n \n\n");
	//compile the vertex shader
	glCompileShader(pep_RTT_gFragmentShaderObject);

	iShaderCompileStatus = 0;
	iLogInfoLength = 0;
	szInfoLog = NULL;

	//Code
	glGetShaderiv(pep_RTT_gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(pep_RTT_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_RTT_gFragmentShaderObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}

	//Create Shader Program Object
	pep_RTT_gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Shader Program
	glAttachShader(pep_RTT_gShaderProgramObject, pep_RTT_gVertexShaderObject);

	//Attach Vertex Shader to Shader Program
	glAttachShader(pep_RTT_gShaderProgramObject, pep_RTT_gFragmentShaderObject);

	//Pre-Linking Binding Vertex Attributes
	glBindAttribLocation(pep_RTT_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glBindAttribLocation(pep_RTT_gShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");

	//Link the Shader Program
	glLinkProgram(pep_RTT_gShaderProgramObject);

	//Code
	glGetProgramiv(pep_RTT_gShaderProgramObject, GL_COMPILE_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(pep_RTT_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_RTT_gShaderProgramObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Linking Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}

	//Post Linking retriving uniform Location
	pep_RTT_modelMatrixUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_m_matrix");
	pep_RTT_viewMatrixUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_v_matrix");
	pep_RTT_projectionMatrixUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_projection");
	pep_RTT_lIsPressed = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_lKeyPressed");
	pep_RTT_laUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_la");
	pep_RTT_ldUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_ld");
	pep_RTT_lsUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_ls");
	pep_RTT_lightPositionUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_light_position");
	pep_RTT_kaUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_ka");
	pep_RTT_kdUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_kd");
	pep_RTT_ksUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_ks");
	pep_RTT_shininessMaterialUniform = glGetUniformLocation(pep_RTT_gShaderProgramObject, "u_material_shininess");

	getSphereVertexData(pep_RTT_sphere_vertices, pep_RTT_sphere_normals, pep_RTT_sphere_texture, pep_RTT_sphere_elements);
	pep_RTT_gNumVertices = getNumberOfSphereVertices();
	pep_RTT_gNumElements = getNumberOfSphereElements();

	//Create vao
	glGenVertexArrays(1, &pep_RTT_vaoSphere);
	glBindVertexArray(pep_RTT_vaoSphere);

	glGenBuffers(1, &pep_RTT_vboSpherePosition);
	glBindBuffer(GL_ARRAY_BUFFER, pep_RTT_vboSpherePosition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pep_RTT_sphere_vertices), pep_RTT_sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pep_RTT_vboSphereNormals);
	glBindBuffer(GL_ARRAY_BUFFER, pep_RTT_vboSphereNormals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pep_RTT_sphere_normals), pep_RTT_sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pep_RTT_vboSphereElements);
	glBindBuffer(GL_ARRAY_BUFFER, pep_RTT_vboSphereElements);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pep_RTT_sphere_elements), pep_RTT_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glGenFramebuffers(1, &pep_RTT_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, pep_RTT_FBO);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glGenTextures(1, &pep_RTT_ColorBuffersTexture);
	glBindTexture(GL_TEXTURE_2D, pep_RTT_ColorBuffersTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, pep_gWidth, pep_gHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pep_RTT_ColorBuffersTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &pep_RTT_DepthBuffersTexture);
	glBindTexture(GL_TEXTURE_2D, pep_RTT_DepthBuffersTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, pep_gWidth, pep_gHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, pep_RTT_DepthBuffersTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : FrameBuffer Is Not Complete\n", __FILE__, __LINE__, __FUNCTION__);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return -1;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	

	pep_RTT_perspectiveProjectionMatrix = mat4::identity();

	return 0;
}

void RTT_Display(void)
{
	glViewport(0, 0, (GLsizei)pep_gWidth, (GLsizei)pep_gHeight);

	pep_RTT_perspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)pep_gWidth / (GLfloat)pep_gHeight), 0.1f, 100.0f);

	//Code
	glBindFramebuffer(GL_FRAMEBUFFER, pep_RTT_FBO);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(pep_RTT_gShaderProgramObject);
	glUniform1i(pep_RTT_lIsPressed, 1);

	glUniform3fv(pep_RTT_laUniform, 1, pep_RTT_lightAmbient);
	glUniform3fv(pep_RTT_ldUniform, 1, pep_RTT_lightDiffuse);
	glUniform3fv(pep_RTT_lsUniform, 1, pep_RTT_lightSpecular);
	glUniform4fv(pep_RTT_lightPositionUniform, 1, pep_RTT_lightPosition);

	glUniform3fv(pep_RTT_kaUniform, 1, pep_RTT_materialAmbient);
	glUniform3fv(pep_RTT_kdUniform, 1, pep_RTT_materialDiffuse);
	glUniform3fv(pep_RTT_ksUniform, 1, pep_RTT_materialSpecular);
	glUniform1f(pep_RTT_shininessMaterialUniform, pep_RTT_materialShininess);

	//Declaration of Matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 modelViewProjectionMatrix;

	//Initialize above matrices to there identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do Necessary Transformation
	modelMatrix = translate(0.0f, 0.0f, -6.0f);

	//Send Necessary Matrices to Shader in respective uniform
	glUniformMatrix4fv(pep_RTT_modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(pep_RTT_viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pep_RTT_projectionMatrixUniform, 1, GL_FALSE, pep_RTT_perspectiveProjectionMatrix);

	glBindVertexArray(pep_RTT_vaoSphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_RTT_vboSphereElements);
	glDrawElements(GL_TRIANGLES, pep_RTT_gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RTT_Resize(int width, int height)
{
	if (pep_RTT_FBO)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, pep_RTT_FBO);
		glBindTexture(GL_TEXTURE_2D, pep_RTT_ColorBuffersTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindTexture(GL_TEXTURE_2D, pep_RTT_DepthBuffersTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	
}

void RTT_Uninitialize(void)
{
	//Code
	if (pep_RTT_FBO)
	{
		glDeleteFramebuffers(1, &pep_RTT_FBO);
		pep_RTT_FBO = 0;
	}

	if (pep_RTT_vboSphereNormals)
	{
		glDeleteBuffers(1, &pep_RTT_vboSphereNormals);
		pep_RTT_vboSphereNormals = 0;
	}

	if (pep_RTT_vboSpherePosition)
	{
		glDeleteBuffers(1, &pep_RTT_vboSpherePosition);
		pep_RTT_vboSpherePosition = 0;
	}

	if (pep_RTT_vaoSphere)
	{
		glDeleteVertexArrays(1, &pep_RTT_vaoSphere);
		pep_RTT_vaoSphere = 0;
	}

	if (pep_RTT_gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_RTT_gShaderProgramObject);

		//Ask Program how many shader are Attached to You
		glGetProgramiv(pep_RTT_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if (pShader)
		{
			glGetAttachedShaders(pep_RTT_gShaderProgramObject, shaderCount, &shaderCount, pShader);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_RTT_gShaderProgramObject, pShader[shaderNumber]);

				//Delete detach Shader
				glDeleteShader(pShader[shaderNumber]);
				pShader[shaderNumber] = 0;
			}
			free(pShader);
		}
		glDeleteProgram(pep_RTT_gShaderProgramObject);
		pep_RTT_gShaderProgramObject = 0;

		glUseProgram(0);
	}
}

int GodRaysVersion2_Initialize()
{
	RTT_Initialize();

	RTT_Scene_Initialize();

	// God Rays
	//define Vertex Shader Object
	pep_GodRays_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar* vertexShaderSourceCode_GodRays =
	{
		"#version 450 core 								\n"
		"in vec4 vPosition;						\n"
		"in vec2 vTexcoords;" \
		"out vec2 out_texCoords;					\n"
		"out vec2 out_vTexcoords;					\n"

		" void main() 								\n"
		" { 										\n"
				"out_texCoords = vPosition.xy; 			\n"
				"out_vTexcoords = vTexcoords;"
				"gl_Position = vPosition * 2.0 - 1.0; 	\n"
		" } 										\n"

	};

	//Specify above source code to vertex shader object 
	glShaderSource(pep_GodRays_gVertexShaderObject, 1, (const GLchar * *)& vertexShaderSourceCode_GodRays, NULL);

	//compile the vertex shader
	glCompileShader(pep_GodRays_gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iLogInfoLength = 0;
	GLchar* szInfoLog = NULL;
	GLint iShaderProgramLinkStatus = 0;

	//Code
	glGetShaderiv(pep_GodRays_gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(pep_GodRays_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_GodRays_gVertexShaderObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nVertex Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}

	//define Fragment Shader Object
	pep_GodRays_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Vertex Shader Code
	//const GLchar* fragmentShaderSourceCode_GodRays =
	//{
	//	" #version 450 core 													 \n"
	//	" in vec2 out_texCoords;												 \n"
	//	" uniform sampler2D godRaysSampler; 										 \n"
	//	" out vec4 FragColor;													 \n"

	//	" void main() 															 \n"
	//	" { 																	 \n"
	//	" 	int Samples = 64; 													 \n"
	//	" 	float Intensity = 0.175, Decay = 0.96875; 							 \n"
	//	" 	vec2 TexCoord = out_texCoords.st;									 \n"
	//	" 	vec2 Direction = vec2(0.5) - TexCoord;								 \n"
	//	" 	Direction /= Samples; 												 \n"
	//	" 	vec3 Color = texture2D(godRaysSampler, TexCoord).rgb; 					 \n"
	//	" 	 																	 \n"
	//	" 	for(int Sample = 0; Sample < Samples; Sample++) 					 \n"
	//	" 	{ 																     \n"
	//	" 		Color += texture2D(godRaysSampler, TexCoord).rgb * Intensity; 		 \n"
	//	" 		Intensity *= Decay; 											 \n"
	//	" 		TexCoord += Direction; 											 \n"
	//	" 	} 																	 \n"
	//	" 	 																	 \n"
	//	" 	FragColor = vec4(Color, 1.0); 										 \n"
	//	" } 																	 \n"
	//};

	/*WORKING SHADER*/
	const GLchar* fragmentShaderSourceCode_GodRays =
	{
		" #version 450 core\n"
		" in vec2 out_texCoords;\n"
		"in vec2 out_vTexcoords;" \
		" uniform sampler2D godRaysSampler;\n"
		" uniform sampler2D sceneSampler;\n"
		" out vec4 color;\n"

		"uniform vec3 sunPos = vec3(0.0, 0.0, 0.0);"\
		"const float exposure = 0.3f;" \
		"const float decay = 0.96815;" \
		"const float density  = 0.926;" \
		"const float weight  = 0.587;"\

		" void main()\n"
		" {\n"
			"int NUM_SAMPLES = 64;" \
			"vec2 tc = out_texCoords.xy;"\

			"vec2 deltatexCoord = (tc - (sunPos.xy*0.5 + 0.5));"\
			"deltatexCoord *= 1.0/ float(NUM_SAMPLES);"\

			"float illuminationDecay = 1.0f;"\

			"vec4 godRayColor = texture(godRaysSampler , tc.xy)*0.4;"\

			"for(int i = 0 ; i< NUM_SAMPLES ; i++)"\
			"{"
				"tc-= deltatexCoord;"\

				"vec4 samp = texture(godRaysSampler , tc )*0.4;"

				"samp *= illuminationDecay*weight;"\

				"godRayColor += samp;"\

				"illuminationDecay *= decay;" \
			"}"\
			"vec4 realColor = texture(sceneSampler , out_texCoords);" \

			"color = ((vec4((vec3(godRayColor.r, godRayColor.g, godRayColor.b) * exposure), 1)) + (realColor*(1.0)));"\

		//"color = ((vec4((vec3(godRayColor.r, godRayColor.g, godRayColor.b) * exposure), 1)));"\

		"}\n"
	};

	//Specify above source code to vertex shader object 
	glShaderSource(pep_GodRays_gFragmentShaderObject, 1, (const GLchar * *)& fragmentShaderSourceCode_GodRays, NULL);
	//compile the vertex shader
	glCompileShader(pep_GodRays_gFragmentShaderObject);

	iShaderCompileStatus = 0;
	iLogInfoLength = 0;
	szInfoLog = NULL;

	//Code
	glGetShaderiv(pep_GodRays_gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(pep_GodRays_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(pep_GodRays_gFragmentShaderObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nFragment Shader Compilation Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}

	//Create Shader Program Object
	pep_GodRays_gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Shader Program
	glAttachShader(pep_GodRays_gShaderProgramObject, pep_GodRays_gVertexShaderObject);

	//Attach Vertex Shader to Shader Program
	glAttachShader(pep_GodRays_gShaderProgramObject, pep_GodRays_gFragmentShaderObject);

	glBindAttribLocation(pep_GodRays_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
	glBindAttribLocation(pep_GodRays_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexcoords");

	//Link the Shader Program
	glLinkProgram(pep_GodRays_gShaderProgramObject);

	//Code
	glGetProgramiv(pep_GodRays_gShaderProgramObject, GL_COMPILE_STATUS, &iShaderProgramLinkStatus);

	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(pep_GodRays_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iLogInfoLength);

		if (iLogInfoLength > 0)
		{
			szInfoLog = (GLchar*)malloc(sizeof(char) * iLogInfoLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(pep_GodRays_gShaderProgramObject, iLogInfoLength, &written, szInfoLog);
				fprintf(gpFile, "\n\nProgram Linking Error :\n%s \n\n", szInfoLog);
				free(szInfoLog);
				return -1;
			}
		}
	}

	pep_GodRays_SamplerUniform = glGetUniformLocation(pep_GodRays_gShaderProgramObject, "godRaysSampler");
	pep_GodRays_SceneUniform = glGetUniformLocation(pep_GodRays_gShaderProgramObject, "sceneSampler");

	const GLfloat quadVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	const GLfloat quadTexCoords[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0
	};

	//Create vao
	glGenVertexArrays(1, &pep_GodRays_vaoQuad);
	glBindVertexArray(pep_GodRays_vaoQuad);

	glGenBuffers(1, &pep_GodRays_vboQuad);
	glBindBuffer(GL_ARRAY_BUFFER, pep_GodRays_vboQuad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pep_GodRays_vboTexcoordsQuad);
	glBindBuffer(GL_ARRAY_BUFFER, pep_GodRays_vboTexcoordsQuad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	return 0;
}

void GodRaysVersion2_Display()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	RTT_Display();
	RTT_Scene_Display();

	glUseProgram(pep_GodRays_gShaderProgramObject);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pep_RTT_ColorBuffersTexture);
	glUniform1i(pep_GodRays_SamplerUniform, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, pep_RTT_Scene_ColorBuffersTexture);
	glUniform1i(pep_GodRays_SceneUniform, 1);
	
	glBindVertexArray(pep_GodRays_vaoQuad);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
}

void GodRaysVersion2_Uninitialize(void)
{
	//Code
	
	if (pep_GodRays_vboQuad)
	{
		glDeleteBuffers(1, &pep_GodRays_vboQuad);
		pep_GodRays_vboQuad = 0;
	}

	if (pep_GodRays_vaoQuad)
	{
		glDeleteBuffers(1, &pep_GodRays_vaoQuad);
		pep_GodRays_vaoQuad = 0;
	}

	if (pep_GodRays_gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(pep_GodRays_gShaderProgramObject);

		//Ask Program how many shader are Attached to You
		glGetProgramiv(pep_GodRays_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);

		if (pShader)
		{
			glGetAttachedShaders(pep_GodRays_gShaderProgramObject, shaderCount, &shaderCount, pShader);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(pep_GodRays_gShaderProgramObject, pShader[shaderNumber]);

				//Delete detach Shader
				glDeleteShader(pShader[shaderNumber]);
				pShader[shaderNumber] = 0;
			}
			free(pShader);
		}
		glDeleteProgram(pep_GodRays_gShaderProgramObject);
		pep_GodRays_gShaderProgramObject = 0;

		glUseProgram(0);
	}

	RTT_Scene_Uninitialize();
	RTT_Uninitialize();
}

void GodRaysVersion2_ReSize(int width, int height)
{
	RTT_Resize(width, height);
	RTT_Scene_Resize(width, height);
}