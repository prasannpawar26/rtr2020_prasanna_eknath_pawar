#pragma once

int GodRays_Initialize(int width, int height);
void GodRays_Display(int);
void GodRays_Update(void);
void GodRays_ReSize(int width, int height);
void GodRays_Uninitialize(void);
