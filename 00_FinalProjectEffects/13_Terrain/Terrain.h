#pragma once

#define THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_STONE 101
#define THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_VIJAY_KUNDALI 102

int Terrain_Initialize(void);
void Terrain_Display(void);
void Terrain_Update(void);
void Terrain_ReSize(int width, int height);
void Terrain_Uninitialize(void);
BOOL Terrain_LoadTexture(GLuint* texture, CHAR imageResourceID[]);
