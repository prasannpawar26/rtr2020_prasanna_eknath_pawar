#include "Common.h"
#include "Terrain.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

extern Camera camera;
extern mat4 cameraMatrix;
extern vmath::vec3 cameraPosition;
extern vmath::vec3 cameraFront;

GLuint pep_Terrain_gVertexShaderObject;
GLuint pep_Terrain_gFragmentShaderObject;
GLuint pep_Terrain_gShaderProgramObject;

GLuint pep_Terrain_vao;
GLuint pep_Terrain_vbo_position;
GLuint pep_Terrain_vbo_texcoord;
GLuint pep_Terrain_vbo_indices;
GLuint pep_Terrain_Texture;

GLuint pep_Terrain_SamplerUniform;
GLuint pep_Terrain_modelUniform;
GLuint pep_Terrain_viewUniform;
GLuint pep_Terrain_projectionUniform;

#define SIZE 800
#define VERTEX_COUNT_X 257
#define VERTEX_COUNT_Z 257

float* vertices = NULL;
float* normals = NULL;
float* textureCoords = NULL;
unsigned int* indices = NULL;

#define BITMAP_ID 0x4D42		// the universal bitmap ID
unsigned char*  pep_Terrain_Hitmap = NULL;
BITMAPINFOHEADER pep_Terrain_BitmapHeader;

int Terrain_Hitmap(const char *filename, BITMAPINFOHEADER *bitmapInfoHeader)
{
    FILE *filePtr;
    BITMAPFILEHEADER	bitmapFileHeader;
    int					imageIdx = 0;
    unsigned char		tempRGB;


    fopen_s(&filePtr, filename, "rb");
    if (filePtr == NULL)
    {
        return -1;
    }

    fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

    if (bitmapFileHeader.bfType != BITMAP_ID)
    {
        fclose(filePtr);
        return NULL;
    }

    fread(bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

    fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

    pep_Terrain_Hitmap = (unsigned char*)malloc(bitmapInfoHeader->biSizeImage);

    if (!pep_Terrain_Hitmap)
    {
        free(pep_Terrain_Hitmap);
        fclose(filePtr);
        return -1;
    }

    fread(pep_Terrain_Hitmap, 1, bitmapInfoHeader->biSizeImage, filePtr);

    if (pep_Terrain_Hitmap == NULL)
    {
        fclose(filePtr);
        return -1;
    }

    // swap the R and B values to get RGB since the bitmap color format is in BGR
    for (imageIdx = 0; imageIdx < (int)bitmapInfoHeader->biSizeImage; imageIdx += 3)
    {
        tempRGB = pep_Terrain_Hitmap[imageIdx];
        pep_Terrain_Hitmap[imageIdx] = pep_Terrain_Hitmap[imageIdx + 2];
        pep_Terrain_Hitmap[imageIdx + 2] = tempRGB;
    }

    fclose(filePtr);

    return 0;
}

int Terrain_Generate(void)
{
    vertices = (float*)malloc(VERTEX_COUNT_X * VERTEX_COUNT_Z * 3 * sizeof(float));
    memset(vertices, 0, VERTEX_COUNT_X * VERTEX_COUNT_Z * 3 * sizeof(float));

    normals = (float*)malloc(VERTEX_COUNT_X * VERTEX_COUNT_Z * 3 * sizeof(float));
    memset(normals, 0, VERTEX_COUNT_X * VERTEX_COUNT_Z * 3 * sizeof(float));

    textureCoords = (float*)malloc(VERTEX_COUNT_X * VERTEX_COUNT_Z * 2 * sizeof(float));
    memset(textureCoords, 0, VERTEX_COUNT_X * VERTEX_COUNT_Z * 2 * sizeof(float));

    indices = (unsigned*)malloc(6 * (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1) * sizeof( unsigned int));
    memset(indices, 0, 6 * (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1) * sizeof( unsigned int));

    int vertexPointer = 0;
    for (int i = 0; i < VERTEX_COUNT_Z; i++)
    {
        for (int j = 0; j < VERTEX_COUNT_X; j++)
        {
            vertices[vertexPointer * 3 + 0] = /*-*/(float)j /*/ ((float)VERTEX_COUNT)*//* * SIZE*/;//  j < (VERTEX_COUNT/2 ) ? -(VERTEX_COUNT/2 - j) : (VERTEX_COUNT/2 - j)
            vertices[vertexPointer * 3 + 1] = /*-1.0f;*/ (float)pep_Terrain_Hitmap[(j * VERTEX_COUNT_Z + i) * 3] * 0.10f;
            vertices[vertexPointer * 3 + 2] = /*-*/(float)i/* / ((float)VERTEX_COUNT)*/ /** SIZE*/;

            fprintf(pep_gpFile, "\nvertices[%d] : %f\n",vertexPointer * 3 + 0, vertices[vertexPointer * 3 + 0]);
            fprintf(pep_gpFile, "\nvertices[%d] : %f\n",vertexPointer * 3 + 1, vertices[vertexPointer * 3 + 1]);
            fprintf(pep_gpFile, "\nvertices[%d] : %f\n",vertexPointer * 3 + 2, vertices[vertexPointer * 3 + 2]);
            
            normals[vertexPointer * 3 + 0] = 0;
            normals[vertexPointer * 3 + 1] = 1;
            normals[vertexPointer * 3 + 2] = 0;

            textureCoords[vertexPointer * 2 + 0] = (float)j / ((float)VERTEX_COUNT_X - 1);
            textureCoords[vertexPointer * 2 + 1] = (float)i / ((float)VERTEX_COUNT_Z - 1);

            vertexPointer++;
        }
    }

    int pointer = 0;
    for (int gz = 0; gz < VERTEX_COUNT_Z - 1; gz++)
    {
        for (int gx = 0; gx < VERTEX_COUNT_X - 1; gx++)
        {
           /* int topLeft = (gz * VERTEX_COUNT) + gx;
            int topRight = topLeft + 1;
            int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
            int bottomRight = bottomLeft + 1;

            indices[pointer++] = topLeft;
            indices[pointer++] = bottomLeft;
            indices[pointer++] = topRight;
            indices[pointer++] = topRight;
            indices[pointer++] = bottomLeft;
            indices[pointer++] = bottomRight;*/

            int vertex_index = gz * VERTEX_COUNT_Z + gx;
            indices[pointer + 0] = vertex_index;
            indices[pointer + 1] =  vertex_index + VERTEX_COUNT_Z + 1;
            indices[pointer + 2] = vertex_index + 1;
            indices[pointer + 3] = vertex_index;
            indices[pointer + 4] = vertex_index + VERTEX_COUNT_Z;
            indices[pointer + 5] = vertex_index + VERTEX_COUNT_Z + 1;
            pointer += 6;
        }
    }

    for (int gz = 0; gz < (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1) * 6; gz++)
    {
        fprintf(pep_gpFile, "\indices[%d] : %d\n",gz, indices[gz]);
    }

    return 0;
}

int Terrain_Initialize(void)
{
    fprintf(pep_gpFile, "\nTerrain\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_Terrain_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \

        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \

        "out vec2 out_texcoord;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
            "out_texcoord = vTexCoord;" \
        "}";

    glShaderSource(pep_Terrain_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_Terrain_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_Terrain_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Terrain_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Terrain_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_Terrain_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \
        
        "uniform sampler2D u_sampler;" \
        
        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "FragColor = texture(u_sampler, out_texcoord * 5.0);" \

            //"FragColor = vec4(1.0f, 0.0f, 0.0f, 0.0f);" \

        "}";

    glShaderSource(pep_Terrain_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_Terrain_gFragmentShaderObject);

    glGetShaderiv(pep_Terrain_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_Terrain_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_Terrain_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Terrain_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_Terrain_gShaderProgramObject, pep_Terrain_gVertexShaderObject);
    glAttachShader(pep_Terrain_gShaderProgramObject, pep_Terrain_gFragmentShaderObject);

    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_Terrain_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_Terrain_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_Terrain_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_Terrain_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_Terrain_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_Terrain_modelUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_model_matrix");
    pep_Terrain_viewUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_view_matrix");
    pep_Terrain_projectionUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_projection_matrix");
    pep_Terrain_SamplerUniform = glGetUniformLocation(pep_Terrain_gShaderProgramObject, "u_sampler");

    Terrain_Hitmap("Bharat_Terrain.bmp", &pep_Terrain_BitmapHeader);
    Terrain_Generate();

    //
    // Cube
    //

    glGenVertexArrays(1, &pep_Terrain_vao);
    glBindVertexArray(pep_Terrain_vao);

    glGenBuffers(1, &pep_Terrain_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, VERTEX_COUNT_X * VERTEX_COUNT_Z * 3 * sizeof(float), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_Terrain_vbo_texcoord);
    glBufferData(GL_ARRAY_BUFFER, VERTEX_COUNT_X * VERTEX_COUNT_Z * 2 * sizeof(float), textureCoords, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_Terrain_vbo_indices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Terrain_vbo_indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1) * sizeof(unsigned int), indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    Terrain_LoadTexture(&pep_Terrain_Texture, MAKEINTRESOURCEA(THREEDSHAPESWITHTEXTURE_IDBITMAP_TEXTURE_VIJAY_KUNDALI));

    return 0;
}

void Terrain_Display(void)
{
    cameraMatrix		= camera.GetViewMatrix();
    //camera.Position	+= camera.Front * 0.5f;
    cameraPosition		= camera.GetCameraPosition();
    cameraFront			= camera.GetCameraFront();

    glEnable(GL_TEXTURE_2D);

    // Render
    glUseProgram(pep_Terrain_gShaderProgramObject);

    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;
    //
    // Cube
    //

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_Terrain_SamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_Terrain_Texture);

    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    modelMatrix = translate(-64.0f, 0.0f, -64.0f);
    static float z = 0.0f;
    static float y = 0.0f;
    viewMatrix = cameraMatrix;
    z += 1.1f;
    y += 0.1f;
    //TODO: LookAt 

    glUniformMatrix4fv(pep_Terrain_modelUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(pep_Terrain_viewUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_Terrain_projectionUniform, 1, GL_FALSE, pep_Perspective_ProjectionMatrix);

    glBindVertexArray(pep_Terrain_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_Terrain_vbo_indices);
    glDrawElements(GL_TRIANGLES, 6 * (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1), GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_TEXTURE_2D);

    return;
}

void Terrain_Update(void)
{
    return;
}

void Terrain_ReSize(int width, int height)
{

}

void Terrain_Uninitialize(void)
{
    if (pep_Terrain_vbo_texcoord)
    {
        glDeleteBuffers(1, &pep_Terrain_vbo_texcoord);
        pep_Terrain_vbo_texcoord = 0;
    }

    if (pep_Terrain_vbo_position)
    {
        glDeleteBuffers(1, &pep_Terrain_vbo_position);
        pep_Terrain_vbo_position = 0;
    }

    if (pep_Terrain_vao)
    {
        glDeleteVertexArrays(1, &pep_Terrain_vao);
        pep_Terrain_vao = 0;
    }

    if (pep_Terrain_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_Terrain_gShaderProgramObject);

        glGetProgramiv(pep_Terrain_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_Terrain_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_Terrain_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_Terrain_gShaderProgramObject);
        pep_Terrain_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

BOOL Terrain_LoadTexture(GLuint *texture, CHAR imageResourceID[])
{
    // variable declarations
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;

    // code
    hBitmap = (HBITMAP)LoadImageA(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (NULL == hBitmap)
    {
        return bStatus;
    }

    bStatus = TRUE;

    GetObject(hBitmap, sizeof(bmp), &bmp);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    DeleteObject(hBitmap);

    return bStatus;

}