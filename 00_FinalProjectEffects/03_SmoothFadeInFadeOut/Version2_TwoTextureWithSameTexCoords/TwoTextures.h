#pragma once

#define IDBITMAP_TEXTURE_STONE 101
#define IDBITMAP_TEXTURE_KUNDALI 102

int SmoothFadeInOutTwoTexture_Initialize(void);
void SmoothFadeInOutTwoTexture_Display(bool *pep_SmoothFadeInOutTwoTexture_bSmoothFade, float alpha_value);
void SmoothFadeInOutTwoTexture_Update(void);
void SmoothFadeInOutTwoTexture_ReSize(int width, int height);
void SmoothFadeInOutTwoTexture_Uninitialize(void);
BOOL SmoothFadeInOutTwoTexture_LoadTexture(GLuint* texture, CHAR imageResourceID[]);
