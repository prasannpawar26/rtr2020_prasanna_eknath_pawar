#include "Common.h"
#include "TwoTextures.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_SmoothFadeInOutTwoTexture_gVertexShaderObject;
GLuint pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject;
GLuint pep_SmoothFadeInOutTwoTexture_gShaderProgramObject;

GLuint pep_SmoothFadeInOutTwoTexture_Square_vao;
GLuint pep_SmoothFadeInOutTwoTexture_Square_vbo_position;
GLuint pep_SmoothFadeInOutTwoTexture_Square_vbo_texcoord;
GLuint pep_SmoothFadeInOutTwoTexture_TextureStone;
GLuint pep_SmoothFadeInOutTwoTexture_TextureKundali;

GLuint pep_SmoothFadeInOutTwoTexture_StoneSamplerUniform;
GLuint pep_SmoothFadeInOutTwoTexture_KundaliSamplerUniform;
GLuint pep_SmoothFadeInOutTwoTexture_mvpUniform;

GLuint pep_SmoothFadeInOutTwoTexture_SmoothInAndOutBlendingUniform;
float pep_SmoothFadeInOutTwoTexture_BlendingValue = 1.0f;

int SmoothFadeInOutTwoTexture_Initialize(void)
{
    fprintf(pep_gpFile, "\nSmoothFadeInOutTwoTexture\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_SmoothFadeInOutTwoTexture_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \

        "uniform mat4 u_mvp_matrix;" \

        "out vec2 out_texcoord;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_texcoord = vTexCoord;" \
        "}";

    glShaderSource(pep_SmoothFadeInOutTwoTexture_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_SmoothFadeInOutTwoTexture_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_SmoothFadeInOutTwoTexture_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SmoothFadeInOutTwoTexture_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SmoothFadeInOutTwoTexture_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord;" \
        
        "uniform sampler2D u_stone_sampler;" \
        "uniform sampler2D u_kundali_sampler;" \
        "uniform float u_blending;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "vec4 text_color1 = texture(u_stone_sampler, out_texcoord);" \
            "vec4 text_color2 = texture(u_kundali_sampler, out_texcoord);" \

            "FragColor = (text_color1 * u_blending) + (text_color2 * (1.0 - u_blending));" \
        "}";

    glShaderSource(pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject);

    glGetShaderiv(pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SmoothFadeInOutTwoTexture_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, pep_SmoothFadeInOutTwoTexture_gVertexShaderObject);
    glAttachShader(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, pep_SmoothFadeInOutTwoTexture_gFragmentShaderObject);

    glBindAttribLocation(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_SmoothFadeInOutTwoTexture_mvpUniform = glGetUniformLocation(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, "u_mvp_matrix");
    pep_SmoothFadeInOutTwoTexture_StoneSamplerUniform = glGetUniformLocation(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, "u_stone_sampler");
    pep_SmoothFadeInOutTwoTexture_KundaliSamplerUniform = glGetUniformLocation(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, "u_kundali_sampler");
    pep_SmoothFadeInOutTwoTexture_SmoothInAndOutBlendingUniform  = glGetUniformLocation(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, "u_blending");

    //
    // Cube
    //
    const GLfloat squareVertices[] = {
        // front
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,

    };

    glGenVertexArrays(1, &pep_SmoothFadeInOutTwoTexture_Square_vao);
    glBindVertexArray(pep_SmoothFadeInOutTwoTexture_Square_vao);

    glGenBuffers(1, &pep_SmoothFadeInOutTwoTexture_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_SmoothFadeInOutTwoTexture_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    const GLfloat squareTexcoords[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f
    };

    glGenBuffers(1, &pep_SmoothFadeInOutTwoTexture_Square_vbo_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_SmoothFadeInOutTwoTexture_Square_vbo_texcoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareTexcoords), squareTexcoords, GL_STATIC_DRAW);
     glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    glBindVertexArray(0);

    SmoothFadeInOutTwoTexture_LoadTexture(&pep_SmoothFadeInOutTwoTexture_TextureStone, MAKEINTRESOURCEA(IDBITMAP_TEXTURE_STONE));
    SmoothFadeInOutTwoTexture_LoadTexture(&pep_SmoothFadeInOutTwoTexture_TextureKundali, MAKEINTRESOURCEA(IDBITMAP_TEXTURE_KUNDALI));

    return 0;
}

void SmoothFadeInOutTwoTexture_Display(bool *pep_SmoothFadeInOutTwoTexture_bSmoothFade, float alpha_value)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE_2D);

    // Render
    glUseProgram(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Cube
    //

    //
    // 1. Here We Are Already Showing First Scene (Full Smiley). IT Means Alpha Is Already 1.0
    // 2. Because Of Point 1 We Are Blending From 1.0 To 0.0
    // 3. We Are Controlling Smooth Shading From OUT bool Parameter, If It Is False Then Only Smooth-Fade Logic Will Work
    // 4. Once Blending_Value Goes Below Zero
    //      = Then We Are Changing (i.e giKeyNum & TexCoord Are Changing BAsed On giKeyNum) What To Show For Smooth Shading
    //      = Again Blending_Value Set To 1 And In New If_Else Case Of giKeyNum Previous TextCoord1 Becomes TexCoord1 And New Value Assign To TexCoord2
    //
    //if (*pep_SmoothFadeInOutTwoTexture_bSmoothFade)
    //{
    //    pep_SmoothFadeInOutTwoTexture_BlendingValue -= 0.005f;
    //    if (pep_SmoothFadeInOutTwoTexture_BlendingValue < 0.0f)
    //    {
    //        pep_SmoothFadeInOutTwoTexture_BlendingValue = 0.0f;
    //        /*pep_SmoothFadeInOutTwoTexture_BlendingValue = 1.0f;*/
    //    }
    //}

    if (*pep_SmoothFadeInOutTwoTexture_bSmoothFade)  // 0.0 To 1.0  => Fade Out
    {
        pep_SmoothFadeInOutTwoTexture_BlendingValue += alpha_value;
        if (pep_SmoothFadeInOutTwoTexture_BlendingValue > 1.0)
        {
            pep_SmoothFadeInOutTwoTexture_BlendingValue = 1.0;
            *pep_SmoothFadeInOutTwoTexture_bSmoothFade = false;
        }
    }
    else // 0.0 To 1.0  => Fade In
    {
        pep_SmoothFadeInOutTwoTexture_BlendingValue += alpha_value;
        if (0.0 > pep_SmoothFadeInOutTwoTexture_BlendingValue)
        {
            pep_SmoothFadeInOutTwoTexture_BlendingValue = 0.0;
            *pep_SmoothFadeInOutTwoTexture_bSmoothFade = true;
        }
    }

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_SmoothFadeInOutTwoTexture_StoneSamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_SmoothFadeInOutTwoTexture_TextureStone);

    glActiveTexture(GL_TEXTURE1);
    glUniform1i(pep_SmoothFadeInOutTwoTexture_KundaliSamplerUniform, 1);
    glBindTexture(GL_TEXTURE_2D, pep_SmoothFadeInOutTwoTexture_TextureKundali);

    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_SmoothFadeInOutTwoTexture_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1f(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, 1.0f);
    glUniform1f(pep_SmoothFadeInOutTwoTexture_SmoothInAndOutBlendingUniform, pep_SmoothFadeInOutTwoTexture_BlendingValue);

    glBindVertexArray(pep_SmoothFadeInOutTwoTexture_Square_vao);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_BLEND);

    glDisable(GL_TEXTURE_2D);

    return;
}

void SmoothFadeInOutTwoTexture_Update(void)
{
    return;
}

void SmoothFadeInOutTwoTexture_ReSize(int width, int height)
{

}

void SmoothFadeInOutTwoTexture_Uninitialize(void)
{
    if (pep_SmoothFadeInOutTwoTexture_Square_vbo_texcoord)
    {
        glDeleteBuffers(1, &pep_SmoothFadeInOutTwoTexture_Square_vbo_texcoord);
        pep_SmoothFadeInOutTwoTexture_Square_vbo_texcoord = 0;
    }

    if (pep_SmoothFadeInOutTwoTexture_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_SmoothFadeInOutTwoTexture_Square_vbo_position);
        pep_SmoothFadeInOutTwoTexture_Square_vbo_position = 0;
    }

    if (pep_SmoothFadeInOutTwoTexture_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_SmoothFadeInOutTwoTexture_Square_vao);
        pep_SmoothFadeInOutTwoTexture_Square_vao = 0;
    }

    if (pep_SmoothFadeInOutTwoTexture_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject);

        glGetProgramiv(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_SmoothFadeInOutTwoTexture_gShaderProgramObject);
        pep_SmoothFadeInOutTwoTexture_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

BOOL SmoothFadeInOutTwoTexture_LoadTexture(GLuint *texture, CHAR imageResourceID[])
{
    // variable declarations
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;

    // code
    hBitmap = (HBITMAP)LoadImageA(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (NULL == hBitmap)
    {
        return bStatus;
    }

    bStatus = TRUE;

    GetObject(hBitmap, sizeof(bmp), &bmp);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    DeleteObject(hBitmap);

    return bStatus;

}
