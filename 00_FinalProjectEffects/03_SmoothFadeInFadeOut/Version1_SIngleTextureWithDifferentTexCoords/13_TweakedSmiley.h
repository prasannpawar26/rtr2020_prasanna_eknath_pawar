#pragma once

#define TWEAKEDSMILEY_IDBITMAP_TEXTURE_SMILEY 1301

int TweakedSmiley_Initialize(void);
void TweakedSmiley_Display(bool bSmoothFade);
void TweakedSmiley_Update(void);
void TweakedSmiley_ReSize(int width, int height);
void TweakedSmiley_Uninitialize(void);
BOOL TweakedSmiley_LoadTexture(GLuint* texture, CHAR imageResourceID[]);
