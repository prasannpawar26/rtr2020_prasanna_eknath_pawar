#include "Common.h"
#include "13_TweakedSmiley.h"

extern FILE *pep_gpFile;
extern mat4 pep_Perspective_ProjectionMatrix;

GLuint pep_TweakedSmiley_gVertexShaderObject;
GLuint pep_TweakedSmiley_gFragmentShaderObject;
GLuint pep_TweakedSmiley_gShaderProgramObject;

GLuint pep_TweakedSmiley_Square_vao;
GLuint pep_TweakedSmiley_Square_vbo_position;
GLuint pep_TweakedSmiley_Square_vbo_texcoord1;
GLuint pep_TweakedSmiley_Square_vbo_texcoord2;
GLuint pep_TweakedSmiley_TextureSmiley;

int giKeyNum = 1;

GLuint pep_TweakedSmiley_SamplerUniform;
GLuint pep_TweakedSmiley_mvpUniform;
GLuint pep_TweakedSmiley_AlphaUniform;

GLuint pep_TweakedSmiley_SmoothInAndOutBlendingUniform;
float blending_value = 1.0f;

GLfloat textureCoords_1[8];
GLfloat textureCoords_2[8];
int TweakedSmiley_Initialize(void)
{
    fprintf(pep_gpFile, "\nTweakedSmiley\n");

   /* fprintf(pep_gpFile, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(pep_gpFile, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(pep_gpFile, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(pep_gpFile, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint pep_numExt;

    glGetIntegerv(GL_NUM_EXTENSIONS, &pep_numExt);
    for (int i = 0; i < pep_numExt; i++)
    {
        fprintf(pep_gpFile, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }*/

    pep_TweakedSmiley_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec4 vPosition;" \
        "in vec2 vTexCoord1;" \
        "in vec2 vTexCoord2;" \

        "uniform mat4 u_mvp_matrix;" \

        "out vec2 out_texcoord1;" \
        "out vec2 out_texcoord2;" \

        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_texcoord1 = vTexCoord1;" \
            "out_texcoord2 = vTexCoord2;" \
        "}";

    glShaderSource(pep_TweakedSmiley_gVertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(pep_TweakedSmiley_gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_TweakedSmiley_gVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TweakedSmiley_gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TweakedSmiley_gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Vertex Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Vertex Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Initializing and compiling fragment shader\n", __DATE__, __TIME__, __FILE__, __LINE__);
    pep_TweakedSmiley_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "in vec2 out_texcoord1;" \
        "in vec2 out_texcoord2;" \
        
        "uniform sampler2D u_sampler;" \
        "uniform float u_blending;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \
            "vec4 text_color1 = texture(u_sampler, out_texcoord1);" \
            "vec4 text_color2 = texture(u_sampler, out_texcoord2);" \

            "FragColor = (text_color1 * u_blending) + (text_color2 * (1.0 - u_blending));" \
        "}";

    glShaderSource(pep_TweakedSmiley_gFragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    glCompileShader(pep_TweakedSmiley_gFragmentShaderObject);

    glGetShaderiv(pep_TweakedSmiley_gFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);
    if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_TweakedSmiley_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;
                glGetShaderInfoLog(pep_TweakedSmiley_gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Fragment Shader: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);
                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Fragment Shader Compiled Successfully\n",  __DATE__, __TIME__, __FILE__, __LINE__);


    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Creating shader program object and attaching shader "
        "sources\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_TweakedSmiley_gShaderProgramObject = glCreateProgram();
    glAttachShader(pep_TweakedSmiley_gShaderProgramObject, pep_TweakedSmiley_gVertexShaderObject);
    glAttachShader(pep_TweakedSmiley_gShaderProgramObject, pep_TweakedSmiley_gFragmentShaderObject);

    glBindAttribLocation(pep_TweakedSmiley_gShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_TweakedSmiley_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD0, "vTexCoord1");
    glBindAttribLocation(pep_TweakedSmiley_gShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD1, "vTexCoord2");

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Linking shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
    glLinkProgram(pep_TweakedSmiley_gShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_TweakedSmiley_gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_TweakedSmiley_gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_TweakedSmiley_gShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Error - Shader Program: \n\t%s\n", __DATE__, __TIME__, __FILE__, __LINE__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }

    fprintf(pep_gpFile, "%s\t%s\t%s\t%d Shader Program Linked Successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);

    pep_TweakedSmiley_mvpUniform = glGetUniformLocation(pep_TweakedSmiley_gShaderProgramObject, "u_mvp_matrix");
    pep_TweakedSmiley_SamplerUniform = glGetUniformLocation(pep_TweakedSmiley_gShaderProgramObject, "u_sampler");
    pep_TweakedSmiley_SmoothInAndOutBlendingUniform  = glGetUniformLocation(pep_TweakedSmiley_gShaderProgramObject, "u_blending");

    //
    // Cube
    //
    const GLfloat squareVertices[] = {
        // front
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,

    };

    glGenVertexArrays(1, &pep_TweakedSmiley_Square_vao);
    glBindVertexArray(pep_TweakedSmiley_Square_vao);

    glGenBuffers(1, &pep_TweakedSmiley_Square_vbo_position);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
    /*glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_TweakedSmiley_Square_vbo_texcoord1);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_texcoord1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4 * 2, NULL, GL_DYNAMIC_DRAW);
    /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_TweakedSmiley_Square_vbo_texcoord2);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_texcoord2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4 * 2, NULL, GL_DYNAMIC_DRAW);
    /* glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);*/
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    TweakedSmiley_LoadTexture(&pep_TweakedSmiley_TextureSmiley, MAKEINTRESOURCEA(TWEAKEDSMILEY_IDBITMAP_TEXTURE_SMILEY));

    return 0;
}

void TweakedSmiley_Display(bool bSmoothFade)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE_2D);

    // Render
    glUseProgram(pep_TweakedSmiley_gShaderProgramObject);

    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    //
    // Cube
    //

    //
    // 1. Here We Are Already Showing First Scene (Full Smiley). IT Means Alpha Is Already 1.0
    // 2. Because Of Point 1 We Are Blending From 1.0 To 0.0
    // 3. We Are Controlling Smooth Shading From OUT bool Parameter, If It Is False Then Only Smooth-Fade Logic Will Work
    // 4. Once Blending_Value Goes Below Zero
    //      = Then We Are Changing (i.e giKeyNum & TexCoord Are Changing BAsed On giKeyNum) What To Show For Smooth Shading
    //      = Again Blending_Value Set To 1 And In New If_Else Case Of giKeyNum Previous TextCoord1 Becomes TexCoord1 And New Value Assign To TexCoord2
    //
    if (bSmoothFade)
    {
        blending_value -= 0.005f;
        if (blending_value < 0.0f)
        {
            /*blending_value = 0.0f;*/
            blending_value = 1.0f;
            giKeyNum++;
            if (5 == giKeyNum)
            {
                giKeyNum = 1;
            }
        }
    }

    if (1 == giKeyNum)
    {
        textureCoords_1[0] = 0.0f;
        textureCoords_1[1] = 1.0f;
        textureCoords_1[2] = 1.0f;
        textureCoords_1[3] = 1.0f;
        textureCoords_1[4] = 1.0f;
        textureCoords_1[5] = 0.0f;
        textureCoords_1[6] = 0.0f;
        textureCoords_1[7] = 0.0f;

        textureCoords_2[0] = 0.0f;
        textureCoords_2[1] = 2.0f;
        textureCoords_2[2] = 2.0f;
        textureCoords_2[3] = 2.0f;
        textureCoords_2[4] = 2.0f;
        textureCoords_2[5] = 0.0f;
        textureCoords_2[6] = 0.0f;
        textureCoords_2[7] = 0.0f;
    }
    else if (2 == giKeyNum)
    {
        textureCoords_1[0] = 0.0f;
        textureCoords_1[1] = 2.0f;
        textureCoords_1[2] = 2.0f;
        textureCoords_1[3] = 2.0f;
        textureCoords_1[4] = 2.0f;
        textureCoords_1[5] = 0.0f;
        textureCoords_1[6] = 0.0f;
        textureCoords_1[7] = 0.0f;

        textureCoords_2[0] = 0.5f;
        textureCoords_2[1] = 0.5f;
        textureCoords_2[2] = 0.0f;
        textureCoords_2[3] = 0.5f;
        textureCoords_2[4] = 0.0f;
        textureCoords_2[5] = 0.0f;
        textureCoords_2[6] = 0.5f;
        textureCoords_2[7] = 0.0f;
    }
    else if (3 == giKeyNum)
    {
        textureCoords_1[0] = 0.5f;
        textureCoords_1[1] = 0.5f;
        textureCoords_1[2] = 0.0f;
        textureCoords_1[3] = 0.5f;
        textureCoords_1[4] = 0.0f;
        textureCoords_1[5] = 0.0f;
        textureCoords_1[6] = 0.5f;
        textureCoords_1[7] = 0.0f;

        textureCoords_2[0] = 0.5f;
        textureCoords_2[1] = 0.5f;
        textureCoords_2[2] = 0.5f;
        textureCoords_2[3] = 0.5f;
        textureCoords_2[4] = 0.5f;
        textureCoords_2[5] = 0.5f;
        textureCoords_2[6] = 0.5f;
        textureCoords_2[7] = 0.5f;
    }
    else if (4 == giKeyNum)
    {
        textureCoords_1[0] = 0.5f;
        textureCoords_1[1] = 0.5f;
        textureCoords_1[2] = 0.5f;
        textureCoords_1[3] = 0.5f;
        textureCoords_1[4] = 0.5f;
        textureCoords_1[5] = 0.5f;
        textureCoords_1[6] = 0.5f;
        textureCoords_1[7] = 0.5f;

        textureCoords_2[0] = 0.0f;
        textureCoords_2[1] = 1.0f;
        textureCoords_2[2] = 1.0f;
        textureCoords_2[3] = 1.0f;
        textureCoords_2[4] = 1.0f;
        textureCoords_2[5] = 0.0f;
        textureCoords_2[6] = 0.0f;
        textureCoords_2[7] = 0.0f;
    }

    glActiveTexture(GL_TEXTURE0);
    glUniform1i(pep_TweakedSmiley_SamplerUniform, 0);
    glBindTexture(GL_TEXTURE_2D, pep_TweakedSmiley_TextureSmiley);

    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = pep_Perspective_ProjectionMatrix * modelViewMatrix;

    glUniformMatrix4fv(pep_TweakedSmiley_mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1f(pep_TweakedSmiley_gShaderProgramObject, 1.0f);
    glUniform1f(pep_TweakedSmiley_SmoothInAndOutBlendingUniform, blending_value);

    glBindVertexArray(pep_TweakedSmiley_Square_vao);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_texcoord1);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_texcoord1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4 * 2, textureCoords_1, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_texcoord2);
    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_texcoord2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4 * 2, textureCoords_2, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, pep_TweakedSmiley_Square_vbo_position);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_BLEND);

    glDisable(GL_TEXTURE_2D);

    return;
}

void TweakedSmiley_Update(void)
{
    return;
}

void TweakedSmiley_ReSize(int width, int height)
{

}

void TweakedSmiley_Uninitialize(void)
{
    if (pep_TweakedSmiley_Square_vbo_texcoord2)
    {
        glDeleteBuffers(1, &pep_TweakedSmiley_Square_vbo_texcoord2);
        pep_TweakedSmiley_Square_vbo_texcoord2 = 0;
    }

    if (pep_TweakedSmiley_Square_vbo_texcoord1)
    {
        glDeleteBuffers(1, &pep_TweakedSmiley_Square_vbo_texcoord1);
        pep_TweakedSmiley_Square_vbo_texcoord1 = 0;
    }

    if (pep_TweakedSmiley_Square_vbo_position)
    {
        glDeleteBuffers(1, &pep_TweakedSmiley_Square_vbo_position);
        pep_TweakedSmiley_Square_vbo_position = 0;
    }

    if (pep_TweakedSmiley_Square_vao)
    {
        glDeleteVertexArrays(1, &pep_TweakedSmiley_Square_vao);
        pep_TweakedSmiley_Square_vao = 0;
    }

    if (pep_TweakedSmiley_gShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_TweakedSmiley_gShaderProgramObject);

        glGetProgramiv(pep_TweakedSmiley_gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_TweakedSmiley_gShaderProgramObject, shaderCount, &shaderCount,
                pShaders);

            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                fprintf(pep_gpFile, "%s\t%s\t%s\t%d Detaching shader sources from program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
                glDetachShader(pep_TweakedSmiley_gShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        fprintf(pep_gpFile, "%s\t%s\t%s\t%d Unlink shader program object\n", __DATE__, __TIME__, __FILE__, __LINE__);
        glDeleteProgram(pep_TweakedSmiley_gShaderProgramObject);
        pep_TweakedSmiley_gShaderProgramObject = 0;

        glUseProgram(0);
    }

    return;
}

BOOL TweakedSmiley_LoadTexture(GLuint *texture, CHAR imageResourceID[])
{
    // variable declarations
    HBITMAP hBitmap = NULL;
    BITMAP bmp;
    BOOL bStatus = FALSE;

    // code
    hBitmap = (HBITMAP)LoadImageA(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (NULL == hBitmap)
    {
        return bStatus;
    }

    bStatus = TRUE;

    GetObject(hBitmap, sizeof(bmp), &bmp);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, texture);
    glBindTexture(GL_TEXTURE_2D, *texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    DeleteObject(hBitmap);

    return bStatus;

}
