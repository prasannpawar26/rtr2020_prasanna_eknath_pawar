#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "Sphere.lib")

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXCOORD,
    AMC_ATTRIBUTES_INSTANCE0,
    AMC_ATTRIBUTES_INSTANCE1,
    AMC_ATTRIBUTES_INSTANCE2,
    AMC_ATTRIBUTES_INSTANCE3,
    AMC_ATTRIBUTES_INSTANCE4,
    AMC_ATTRIBUTES_INSTANCE5,
    AMC_ATTRIBUTES_INSTANCE6,
    AMC_ATTRIBUTES_INSTANCE7
};
