#pragma once

int HardwareInstancing_Initialize(void);
void HardwareInstancing_Uninitialize(void);
void HardwareInstancing_Display(void);
void HardwareInstancing_Resize(void);
void HardwareInstancing_Update(void);
