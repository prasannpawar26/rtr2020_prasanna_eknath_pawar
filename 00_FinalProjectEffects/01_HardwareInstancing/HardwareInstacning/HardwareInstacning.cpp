#include "Common.h"
#include "Sphere.h"

#define INSTANCE_COUNT 4

extern FILE *pep_gpFile;
extern mat4 pep_perspectiveProjectionMatrix;

GLuint pep_HardwareInstancing_giVertexShaderObject;
GLuint pep_HardwareInstancing_giFragmentShaderObject;
GLuint pep_HardwareInstancing_giShaderProgramObject;

GLuint pep_HardwareInstancing_ViewMatrixUniform;
GLuint pep_HardwareInstancing_ProjectionMatrixUniform;
GLuint pep_HardwareInstancing_LightAmbientUniform;
GLuint pep_HardwareInstancing_LightDiffuseUniform;
GLuint pep_HardwareInstancing_LightSpecularUniform;
GLuint pep_HardwareInstancing_LightPositionUniform;

vec4 pep_HardwareInstancing_LightAmbient = vec4(0.0f, 0.0f, 0.0f, 0.0f);
vec4 pep_HardwareInstancing_LightDiffuse = vec4(1.0f, 1.0f, 1.0f, 1.0f);
vec4 pep_HardwareInstancing_LightSpecular = vec4(1.0f, 1.0f, 1.0f, 1.0f);
vec4 pep_HardwareInstancing_LightPosition = vec4(0.0f, 0.0f, 3.0f, 1.0f);

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned int gNumVertices;
unsigned int gNumElements;

GLuint pep_HardwareInstancing_Vao;
GLuint pep_HardwareInstancing_VboPosition;
GLuint pep_HardwareInstancing_VboNormal;
GLuint pep_HardwareInstancing_VboTexcoord;
GLuint pep_HardwareInstancing_VboElement;

GLuint pep_HardwareInstancing_VboInstance_ModelMatrix;
GLuint pep_HardwareInstancing_VboInstance_Ka;
GLuint pep_HardwareInstancing_VboInstance_Kd;
GLuint pep_HardwareInstancing_VboInstance_Ks;
GLuint pep_HardwareInstancing_VboInstance_Kms;

vec4 pep_HardwareInstancing_MaterialAmbient = vec4(1.0f, 0.0f, 0.0f, 0.0f);
vec4 pep_HardwareInstancing_MaterialDiffuse = vec4(1.0f, 1.0f, 1.0f, 1.0f);
vec4 pep_HardwareInstancing_MaterialSpecular = vec4(1.0f, 1.0f, 1.0f, 1.0f);
GLfloat pep_HardwareInstancing_MaterialShiness = 50.0f;

struct HardwareInstanceData
{
    mat4 ModelMatrix[INSTANCE_COUNT];
    vec4 Ka[INSTANCE_COUNT];
    vec4 Kd[INSTANCE_COUNT];
    vec4 Ks[INSTANCE_COUNT];
    GLfloat Kms[INSTANCE_COUNT];
};

HardwareInstanceData pep_HardwareInstancing_InstanceData;

int HardwareInstancing_Initialize(void)
{
    pep_HardwareInstancing_giVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    if (0 == pep_HardwareInstancing_giVertexShaderObject)
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed\n",
            __FILE__, __LINE__, __FUNCTION__);

        return -1;
    }

    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : glCreateShader Successful\n",
        __FILE__, __LINE__, __FUNCTION__);
    fflush(pep_gpFile);

    const GLchar *vertexShaderSourceCode =
        "#version 450 core" \
        "\n" \

        "layout (location = 0) in vec4 vPosition;" \
        "layout (location = 1) in vec3 vNormal;" \
        "layout (location = 2) in vec2 vTexCoord;" \

        "layout (location = 3) in mat4 vModelMatrix;" \
        "layout (location = 7) in vec4 vKa;" \
        "layout (location = 8) in vec4 vKd;" \
        "layout (location = 9) in vec4 vKs;" \
        "layout (location = 10) in float vKms;" \

        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec3 u_light_ambient;" \
        "uniform vec3 u_light_diffuse;" \
        "uniform vec3 u_light_specular;" \
        "uniform vec4 u_light_position;" \

        "out vec3 light_direction0;" \
        "out vec3 tranformation_matrix;" \
        "out vec3 viewer_vector;" \

        "out vec3 material_ambient;" \
        "out vec3 material_diffuse;" \
        "out vec3 material_specular;" \
        "out float material_shiness;" \

        "void main(void)" \
        "{" \

            "vec4 eye_coordinates = u_view_matrix * vModelMatrix * vPosition;" \
            "tranformation_matrix = mat3(u_view_matrix * vModelMatrix) * vNormal;" \
            "viewer_vector = vec3(-eye_coordinates);" \
            "light_direction0 = vec3(u_light_position - eye_coordinates);" \

            "material_ambient = vKa.rgb;" \
            "material_diffuse = vKd.rgb;" \
            "material_specular = vKs.rgb;" \
            "material_shiness = vKms;" \

            "gl_Position = u_projection_matrix * u_view_matrix * vModelMatrix * vPosition;" \
        "}";

    glShaderSource(pep_HardwareInstancing_giVertexShaderObject, 1,
        (const GLchar **)&vertexShaderSourceCode, NULL);

    glCompileShader(pep_HardwareInstancing_giVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;

    glGetShaderiv(pep_HardwareInstancing_giVertexShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus); if (GL_FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_HardwareInstancing_giVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_HardwareInstancing_giVertexShaderObject, iInfoLogLength, &written,
                    szInfoLog);
                fprintf(
                    pep_gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation "
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Vertex Shader Compiled "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    pep_HardwareInstancing_giFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    if (0 == pep_HardwareInstancing_giFragmentShaderObject)
    {
        fprintf(pep_gpFile,
            "FILENAME: %s LINE: %d FUNCTION: %s : Error : glCreateShader "
            "Failed:\n\t%s\n",
            __FILE__, __LINE__, __FUNCTION__, szInfoLog);

        return -1;
    }

    const GLchar *fragmentShaderSourceCode =
        "#version 450 core" \
        "\n" \
        "in vec3 light_direction0;" \
        "in vec3 tranformation_matrix;" \
        "in vec3 viewer_vector;" \

        "in vec3 material_ambient;" \
        "in vec3 material_diffuse;" \
        "in vec3 material_specular;" \
        "in float material_shiness;" \

        "uniform vec3 u_light_ambient;" \
        "uniform vec3 u_light_diffuse;" \
        "uniform vec3 u_light_specular;" \
        "uniform vec4 u_light_position;" \

        "out vec4 FragColor;" \

        "void main(void)" \
        "{" \

            "vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
            "vec3 viewer_vector_normal = normalize(viewer_vector);" \

            "vec3 light_direction_normalize0 = normalize(light_direction0);" \
            "vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" \
            "float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" \

            "vec3 ambient0 = u_light_ambient * material_ambient;" \
            "vec3 diffuse0 = u_light_diffuse * material_diffuse * t_normal_dot_light_direction0;" \
            "vec3 specular0 = u_light_specular * material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), material_shiness);" \

            "vec3 phong_ads_light = ambient0 + diffuse0 + specular0;" \

            "FragColor = vec4(phong_ads_light, 1.0);" \
        "}";

    glShaderSource(pep_HardwareInstancing_giFragmentShaderObject, 1,
        (const GLchar **)&fragmentShaderSourceCode, NULL);

    glCompileShader(pep_HardwareInstancing_giFragmentShaderObject);

    glGetShaderiv(pep_HardwareInstancing_giFragmentShaderObject, GL_COMPILE_STATUS,
        &iShaderCompileStatus);

    if (FALSE == iShaderCompileStatus)
    {
        glGetShaderiv(pep_HardwareInstancing_giFragmentShaderObject, GL_INFO_LOG_LENGTH,
            &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetShaderInfoLog(pep_HardwareInstancing_giFragmentShaderObject, iInfoLogLength, &written,
                    szInfoLog);

                fprintf(
                    pep_gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Shader Compilation"
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Fragment Shader Compiled "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);
    fflush(pep_gpFile);
    pep_HardwareInstancing_giShaderProgramObject = glCreateProgram();

    glAttachShader(pep_HardwareInstancing_giShaderProgramObject, pep_HardwareInstancing_giVertexShaderObject);
    glAttachShader(pep_HardwareInstancing_giShaderProgramObject, pep_HardwareInstancing_giFragmentShaderObject);

    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_TEXCOORD, "vTexCoord");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_INSTANCE0, "vModelMatrix");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_INSTANCE4, "vKa");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_INSTANCE5, "vKd");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_INSTANCE6, "vKs");
    glBindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, AMC_ATTRIBUTES_INSTANCE7, "vKms");

    glLinkProgram(pep_HardwareInstancing_giShaderProgramObject);

    GLint iProgramLinkStatus = 0;
    iInfoLogLength = 0;
    szInfoLog = NULL;

    glGetProgramiv(pep_HardwareInstancing_giShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (GL_FALSE == iProgramLinkStatus)
    {
        glGetProgramiv(pep_HardwareInstancing_giShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (0 < iInfoLogLength)
        {
            szInfoLog = (GLchar *)malloc(iInfoLogLength);
            if (NULL != szInfoLog)
            {
                GLsizei written;

                glGetProgramInfoLog(pep_HardwareInstancing_giShaderProgramObject, iInfoLogLength, &written,
                    szInfoLog);
                fprintf(pep_gpFile,
                    "FILENAME: %s LINE: %d FUNCTION: %s : Error : Linking "
                    "Failed:\n\t%s\n",
                    __FILE__, __LINE__, __FUNCTION__, szInfoLog);

                free(szInfoLog);
                return -1;
            }
        }
    }
    fprintf(pep_gpFile,
        "FILENAME: %s LINE: %d FUNCTION: %s : Shader Program(s) Linked "
        "Successfully\n",
        __FILE__, __LINE__, __FUNCTION__);

    pep_HardwareInstancing_ViewMatrixUniform = glGetUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_view_matrix");
    pep_HardwareInstancing_ProjectionMatrixUniform = glGetUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_projection_matrix");
    pep_HardwareInstancing_LightAmbientUniform = glGetUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_ambient");
    pep_HardwareInstancing_LightDiffuseUniform = glGetUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_diffuse");
    pep_HardwareInstancing_LightSpecularUniform = glGetUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_specular");
    pep_HardwareInstancing_LightPositionUniform = glGetUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_position");

    // rectangle
    glGenVertexArrays(1, &pep_HardwareInstancing_Vao);

    glBindVertexArray(pep_HardwareInstancing_Vao);

    // rectangle-position
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

    glGenBuffers(1, &pep_HardwareInstancing_VboPosition);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboPosition);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_HardwareInstancing_VboNormal);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboNormal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_HardwareInstancing_VboTexcoord);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboTexcoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_textures), sphere_textures, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXCOORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_HardwareInstancing_VboElement);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_HardwareInstancing_VboElement);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //
    // Instancing Data
    //
    pep_HardwareInstancing_InstanceData.ModelMatrix[0] = mat4::identity() * translate(-1.50f, 1.0f, -7.0f);
    pep_HardwareInstancing_InstanceData.Ka[0] = pep_HardwareInstancing_MaterialAmbient;
    pep_HardwareInstancing_InstanceData.Kd[0] = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Ks[0] = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Kms[0] = 128.0f;

    pep_HardwareInstancing_InstanceData.ModelMatrix[1] = mat4::identity() * translate(-1.50f, -1.0f, -7.0f);
    pep_HardwareInstancing_InstanceData.Ka[1] = pep_HardwareInstancing_MaterialAmbient;
    pep_HardwareInstancing_InstanceData.Kd[1] = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Ks[1] = vec4(0.0f, 1.0f, 0.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Kms[1] = 128.0f;

    pep_HardwareInstancing_InstanceData.ModelMatrix[2] = mat4::identity() * translate(1.50f, -1.0f, -7.0f);
    pep_HardwareInstancing_InstanceData.Ka[2] = pep_HardwareInstancing_MaterialAmbient;
    pep_HardwareInstancing_InstanceData.Kd[2] = vec4(0.0f, 0.0f, 1.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Ks[2] = vec4(0.0f, 0.0f, 1.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Kms[2] = 128.0f;

    pep_HardwareInstancing_InstanceData.ModelMatrix[3] = mat4::identity() * translate(1.50f, 1.0f, -7.0f);
    pep_HardwareInstancing_InstanceData.Ka[3] = pep_HardwareInstancing_MaterialAmbient;
    pep_HardwareInstancing_InstanceData.Kd[3] = vec4(1.0f, 1.0f, 0.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Ks[3] = vec4(1.0f, 1.0f, 0.0f, 1.0f);
    pep_HardwareInstancing_InstanceData.Kms[3] = 128.0f;

    glGenBuffers(1, &pep_HardwareInstancing_VboInstance_ModelMatrix);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboInstance_ModelMatrix);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mat4) * INSTANCE_COUNT, &pep_HardwareInstancing_InstanceData.ModelMatrix[0], GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4), (void *)(0));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE0);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE1, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4), (void *)(16));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE1);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE2, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4), (void *)(32));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE2);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE3, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4), (void *)(48));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE3);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE0, 1);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE1, 1);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE2, 1);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE3, 1);
    glBindBuffer(GL_ARRAY_BUFFER, 0); 

    glGenBuffers(1, &pep_HardwareInstancing_VboInstance_Ka);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboInstance_Ka);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * INSTANCE_COUNT, &pep_HardwareInstancing_InstanceData.Ka[0], GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE4, 4, GL_FLOAT, GL_FALSE, 0, (void *)(0));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE4);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE4, 1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_HardwareInstancing_VboInstance_Kd);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboInstance_Kd);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * INSTANCE_COUNT, &pep_HardwareInstancing_InstanceData.Kd[0], GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE5, 4, GL_FLOAT, GL_FALSE, 0, (void *)(0));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE5);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE5, 1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_HardwareInstancing_VboInstance_Ks);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboInstance_Ks);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * INSTANCE_COUNT, &pep_HardwareInstancing_InstanceData.Ks[0], GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE6, 4, GL_FLOAT, GL_FALSE, 0, (void *)(0));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE6);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE6, 1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &pep_HardwareInstancing_VboInstance_Kms);
    glBindBuffer(GL_ARRAY_BUFFER, pep_HardwareInstancing_VboInstance_Kms);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * INSTANCE_COUNT, &pep_HardwareInstancing_InstanceData.Kms[0], GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTES_INSTANCE7, 1, GL_FLOAT, GL_FALSE, 0, (void *)(0));
    glEnableVertexAttribArray(AMC_ATTRIBUTES_INSTANCE7);
    glVertexAttribDivisor(AMC_ATTRIBUTES_INSTANCE7, 1);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void HardwareInstancing_Uninitialize(void)
{
    if (pep_HardwareInstancing_VboPosition)
    {
        glDeleteBuffers(1, &pep_HardwareInstancing_VboPosition);
        pep_HardwareInstancing_VboPosition = 0;
    }

    if (pep_HardwareInstancing_Vao)
    {
        glDeleteVertexArrays(1, &pep_HardwareInstancing_Vao);
        pep_HardwareInstancing_Vao = 0;
    }

    if (pep_HardwareInstancing_giShaderProgramObject)
    {
        GLsizei shaderCount;
        GLsizei shaderNumber;

        glUseProgram(pep_HardwareInstancing_giShaderProgramObject);

        glGetProgramiv(pep_HardwareInstancing_giShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
        if (pShaders)
        {
            glGetAttachedShaders(pep_HardwareInstancing_giShaderProgramObject, shaderCount, &shaderCount,
                pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                glDetachShader(pep_HardwareInstancing_giShaderProgramObject, pShaders[shaderNumber]);
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }

            free(pShaders);
        }

        glDeleteProgram(pep_HardwareInstancing_giShaderProgramObject);
        pep_HardwareInstancing_giShaderProgramObject = 0;

        glUseProgram(0);
    }
}

void HardwareInstancing_Display(void)
{
    glUseProgram(pep_HardwareInstancing_giShaderProgramObject);

    mat4 ModelMatrix;
    mat4 viewMatrix;
    mat4 projectionMatrix;

    ModelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    projectionMatrix = mat4::identity();

    projectionMatrix = pep_perspectiveProjectionMatrix;

    glUniformMatrix4fv(pep_HardwareInstancing_ViewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pep_HardwareInstancing_ProjectionMatrixUniform, 1, GL_FALSE, pep_perspectiveProjectionMatrix);
    glUniform3fv(pep_HardwareInstancing_LightAmbientUniform, 1, pep_HardwareInstancing_LightAmbient);
    glUniform3fv(pep_HardwareInstancing_LightDiffuseUniform, 1, pep_HardwareInstancing_LightDiffuse);
    glUniform3fv(pep_HardwareInstancing_LightSpecularUniform, 1, pep_HardwareInstancing_LightSpecular);
    glUniform4fv(pep_HardwareInstancing_LightPositionUniform, 1, pep_HardwareInstancing_LightPosition);

    glBindVertexArray(pep_HardwareInstancing_Vao);
    /*glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, gNumVertices, INSTANCE_COUNT);*/
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pep_HardwareInstancing_VboElement);
    glDrawElementsInstanced(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0, INSTANCE_COUNT);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    return;
}

void HardwareInstancing_Resize(void)
{

}

void HardwareInstancing_Update(void)
{

}
