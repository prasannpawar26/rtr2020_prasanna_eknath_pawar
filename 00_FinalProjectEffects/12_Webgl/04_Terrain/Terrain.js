
var pep_gTerrain_Texture = 1;

function Terrain()
{
	var pep_Terrain_vs;
	var pep_Terrain_fs;
	var pep_Terrain_program;

	var pep_Terrain_vao;
	var pep_Terrain_vbo_position;
	var pep_Terrain_vbo_texcoords;

	var pep_Terrain_modelMatrixUniform;
	var pep_Terrain_viewMatrixUniform;
	var pep_Terrain_projectionMatrixUniform;
	var pep_Terrain_samplerUniform;
	
	var pep_Terrain_texture_1 = 0;
	
	var SIZE = 800;
	var VERTEX_COUNT_X = 257;
	var VERTEX_COUNT_Z = 257;

	var vertices = null;
	var normals = null;
	var textureCoords = null;
	var indices = null;
	
	var height_map_pixels;
	var height_map_float;

	function Rand(min, max)
	{
		return parseFloat(Math.floor(Math.random() * (max - min) ) + min);
	}

	Terrain_HeightMap=function()
	{
		var height_canvas = document.createElement('canvas');
		height_canvas.width = 257;
		height_canvas.height = 257;
		var context = height_canvas.getContext('2d');
		context.drawImage(Bharat_Terrain, 0, 0);
		height_map_pixels = context.getImageData(0, 0, 257, 257).data;

		var float_array_size = height_map_pixels.length / 4;
		float_array_size += height_map_pixels.length % 4 ? 1: 0;
		height_map_float = new Float32Array(float_array_size);
		
		var max_color_value = 255 * 255 * 255;
		var array_index = 0;
		for(var index = 0; index < height_map_pixels.length; index += 4)
		{
			var temp = (height_map_pixels[index + 2] << 0) |  // left shift by 0
			(height_map_pixels[index + 1] << 8) | // left shift by 8
			(height_map_pixels[index + 0] << 16); // left shift by 16

			height_map_float[array_index] = parseFloat(temp / max_color_value);
			array_index += 1;
		}
	}
	
	Terrain_Generate=function()
	{
		vertices = new Float32Array(VERTEX_COUNT_X * VERTEX_COUNT_Z * 3);
		normals = new Float32Array(VERTEX_COUNT_X * VERTEX_COUNT_Z * 3);
		textureCoords = new Float32Array(VERTEX_COUNT_X * VERTEX_COUNT_Z * 2);
		indices = new Uint32Array(6 * (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1));
		
		var  vertexPointer = 0;
		var index = 0;
		for (var i = 0; i < VERTEX_COUNT_Z; i++)
		{
			for (var j = 0; j < VERTEX_COUNT_X; j++)
			{
				//console.log(height_map_float[(j * VERTEX_COUNT_Z + i)]);
				vertices[vertexPointer * 3 + 0] = parseFloat(j);
				vertices[vertexPointer * 3 + 1] = height_map_float[(j * VERTEX_COUNT_Z + i) * 3] * 20.0;
				vertices[vertexPointer * 3 + 2] = parseFloat(i);

				//console.log("[" + vertices[vertexPointer * 3 + 0] + "," + vertices[vertexPointer * 3 + 1] + "," + vertices[vertexPointer * 3 + 2] + "]");
				
				normals[vertexPointer * 3 + 0] = 0.0;
				normals[vertexPointer * 3 + 1] = 1.0;
				normals[vertexPointer * 3 + 2] = 0.0;

				textureCoords[vertexPointer * 2 + 0] =
					parseFloat(j) / parseFloat(VERTEX_COUNT_X - 1);
				textureCoords[vertexPointer * 2 + 1] =
					parseFloat(i) / parseFloat(VERTEX_COUNT_Z - 1);
				
				vertexPointer += 1;
				index += 4;
			}
		}
		
		var pointer = 0;
		for (var gz = 0; gz < VERTEX_COUNT_Z - 1; gz++)
		{
			for (var gx = 0; gx < VERTEX_COUNT_X - 1; gx++)
			{
				var vertex_index = gz * VERTEX_COUNT_Z + gx;
				indices[pointer + 0] = parseFloat(vertex_index);
				indices[pointer + 1] = parseFloat(vertex_index + VERTEX_COUNT_Z + 1);
				indices[pointer + 2] = parseFloat(vertex_index + 1);
				indices[pointer + 3] = parseFloat(vertex_index);
				indices[pointer + 4] = parseFloat(vertex_index + VERTEX_COUNT_Z);
				indices[pointer + 5] = parseFloat(vertex_index + VERTEX_COUNT_Z + 1);
				pointer += 6;
			}
		}
		
		/*
		for (var gz = 0; gz < (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1) * 6; gz += 6)
		{
			console.log(gz + " : " + indices[gz + 0] + " : " + indices[gz + 1] + " : " + indices[gz + 2] + " : " + indices[gz + 3]+ " : " + indices[gz + 4]+ " : " + indices[gz + 5]);
		}
		*/
	}

	this.Init=function()
	{
		Terrain_HeightMap();
		Terrain_Generate();
		
		// vertex shader
		var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTexcoords;" +
		
		"out vec2 texCoords;" +
		
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		
		"float random (in vec2 _st) {" +
			"return fract(sin(dot(_st.xy, vec2(12.9898,78.233))) * 43758.5453123);" +
		"}" +
		
		"void main(void)" +
		"{" +
			//"float y = random(vec2(vPosition.x, vPosition.z));" +
			
			//"vec4 position = vec4(vPosition.x, y, vPosition.z, vPosition.w);" +
			
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"texCoords = vTexcoords;" +
		"}";
		
		pep_Terrain_vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(pep_Terrain_vs, vertexShaderSourceCode);
		gl.compileShader(pep_Terrain_vs);
		if(gl.getShaderParameter(pep_Terrain_vs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Terrain_vs);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		console.log("RTR : vertexShaderObject Compiled Successfully");
		
		// fragment shader
		var fragmentShaderSourceCode = 
		"#version 300 es" +
		"\n" +
		"precision highp float;"+
		"in vec2 texCoords;" +
		
		"uniform highp sampler2D u_texture0_sampler;" +
		
		"out vec4 FragColor;" +
		

		
		"void main(void)" +
		"{" +
			"FragColor = texture(u_texture0_sampler, texCoords * 10.0);" +
			
			//"FragColor = vec4(1.0, 0.0, 0.0, 1.0);" +
		"}";
		
		pep_Terrain_fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(pep_Terrain_fs, fragmentShaderSourceCode);
		gl.compileShader(pep_Terrain_fs);
		if(gl.getShaderParameter(pep_Terrain_fs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Terrain_fs);
			if(erro.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		console.log("RTR : fragmentShaderObject Compiled Successfully");
		
		// shader program
		pep_Terrain_program = gl.createProgram();
		gl.attachShader(pep_Terrain_program, pep_Terrain_vs);
		gl.attachShader(pep_Terrain_program, pep_Terrain_fs);
		
		// pre-link binding of shader program object with vertex shader attributes
		gl.bindAttribLocation(pep_Terrain_program, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(pep_Terrain_program, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexcoords");
		
		// linking
		gl.linkProgram(pep_Terrain_program);
		if(!gl.getProgramParameter(pep_Terrain_program, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(pep_Terrain_program);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		console.log("RTR : shaderProgramObject Linked Successfully");
		
		// get uniform location
		pep_Terrain_modelMatrixUniform = gl.getUniformLocation(pep_Terrain_program, "u_model_matrix");
		pep_Terrain_viewMatrixUniform = gl.getUniformLocation(pep_Terrain_program, "u_view_matrix");
		pep_Terrain_projectionMatrixUniform = gl.getUniformLocation(pep_Terrain_program, "u_projection_matrix");
		pep_Terrain_samplerUniform = gl.getUniformLocation(pep_Terrain_program, "u_texture0_sampler");
		
		// Load Cube Texture
		pep_Terrain_texture_1 = gl.createTexture();
		pep_Terrain_texture_1.image = new Image();
		pep_Terrain_texture_1.image.src = "Stone.png";
		pep_Terrain_texture_1.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, pep_Terrain_texture_1);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pep_Terrain_texture_1.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}

		// vertices, colors, shader attribs, vbo_square_position, vao_square initializations
		pep_Terrain_vao = gl.createVertexArray();
		gl.bindVertexArray(pep_Terrain_vao);
		
		pep_Terrain_vbo_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Terrain_vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		pep_Terrain_vbo_texcoords = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Terrain_vbo_texcoords);
		gl.bufferData(gl.ARRAY_BUFFER, textureCoords, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		pep_Terrain_vbo_indices = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,pep_Terrain_vbo_indices);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
		
		gl.bindVertexArray(null);
	} // Init End
	
	this.Uninit=function()
	{
		if(pep_Terrain_vao)
		{
			gl.deleteVertexArray(pep_Terrain_vao);
			pep_Terrain_vao = null;
		}
		
		if(pep_Terrain_vbo_texcoords)
		{
			gl.deleteBuffer(pep_Terrain_vbo_texcoords);
			pep_Terrain_vbo_texcoords = null;
		}
		
		if(pep_Terrain_vbo_position)
		{
			gl.deleteBuffer(pep_Terrain_vbo_position);
			pep_Terrain_vbo_position = null;
		}

		if(pep_Terrain_program)
		{
			if(pep_Terrain_fs)
			{
				gl.detachShader(pep_Terrain_program, pep_Terrain_fs);
				gl.deleteShader(pep_Terrain_fs);
				pep_Terrain_fs = null;
			}
			
			if(pep_Terrain_vs)
			{
				gl.detachShader(pep_Terrain_program, pep_Terrain_vs);
				gl.deleteShader(pep_Terrain_vs);
				pep_Terrain_vs = null;
			}
			
			gl.deleteProgram(pep_Terrain_program);
			pep_Terrain_program = null;
		}
	}
	
	this.Resize=function(width, height)
	{
	}
	
	this.Update=function()
	{
	}
	
	this.Draw=function()
	{
		

		var modelMatrix = mat4.create();
		var viewMatrix = mat4.create();
		var projectionMatrix = mat4.create();

		mat4.identity(modelMatrix);
		mat4.identity(viewMatrix);
		mat4.identity(projectionMatrix);
		
		mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, 0.0]);
		mat4.rotateY(modelMatrix, modelMatrix, degtored(45.0));
		
		mat4.copy(projectionMatrix, perspectiveProjectionMatrix);
		mat4.copy(viewMatrix, cameraMatrix);
		
		gl.useProgram(pep_Terrain_program);
		gl.uniformMatrix4fv(pep_Terrain_modelMatrixUniform, false, modelMatrix);
		gl.uniformMatrix4fv(pep_Terrain_viewMatrixUniform, false, viewMatrix);
		gl.uniformMatrix4fv(pep_Terrain_projectionMatrixUniform, false, projectionMatrix);
		gl.uniform1i(pep_Terrain_samplerUniform, 0);

		var temp = 6 * (VERTEX_COUNT_X - 1) * (VERTEX_COUNT_Z - 1);
		
		gl.bindTexture(gl.TEXTURE_2D, pep_Terrain_texture_1);
		gl.bindVertexArray(pep_Terrain_vao);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, pep_Terrain_vbo_indices);
        gl.drawElements(gl.TRIANGLES, temp, gl.UNSIGNED_INT, 0);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
		gl.bindVertexArray(null);
		gl.bindTexture(gl.TEXTURE_2D, null);

		gl.useProgram(null);
	}
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}