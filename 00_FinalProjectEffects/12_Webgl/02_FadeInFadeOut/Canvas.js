//
// html and js file should be in one directory
//
// onload function

//
// fullscreen:
// every browser has its own elements for fullscreen.
// 
var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;
var pep_bFadeOut = false;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

var perspectiveProjectionMatrix;

var requestAnimationFrame =
	window.requestAnimationFrame || /*Google Chrome*/
	window.webkitRequestAnimationFrame || /*Apple - Safari*/
	window.mozRequestAnimationFrame || /*Mozilla Firefox*/
	window.oRequestAnimationFrame || /*Opera*/
	window.msRequestAnimationFrame; /*Microsoft - internet explorer*/

var pep_SceneTransition = null;
function main()
{
	// step 1: Get canvas from DOM (i.e html)
	// var => runtime la type decide hoil right hind side nusar type (Type-Infurance)
	// document => inbuild variable (by DOM)
	canvas = document.getElementById("PEP_CANVAS");
	if(!canvas)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		console.log("RTR: Obtaining Canvas Failed\n");
	}
	else
	{
		console.log("RTR: Obtaining Canvas Successed\n");
	}

	// step 2: Retrive width and height of canvas for a seak of information
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//
	// window=> is inbuild variable
	//
	
	//register event handlers
	
	// 3rd parameter is false => bubbled propogation
	// capture propogation => sent to our event listener then to other
	// bubbled propogation =>  event sent from bottom to super class
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	init();

	//warm-up resize call
	resize(); // till now windows, xwindows, android we have just call resize as warm-up call. but in javascript no repaint event hence need to call warm-up draw call.

	//warm-up draw call
	draw();
}

// browser independ code
function toggleFullScreen()
{
	var fullscreen_element =
	document.fullscreenElement || /*Goolge Chrome, Opera*/
	document.webkitFullscreenElement || /*Apple - Safari*/
	document.mozFullScreenElement || /*Mozilla Firefox*/
	document.msFullscreenElement || /* Microsoft - Internet Explore*/
	null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();
		}
		else if(canvas.webkitRequestFullscreen)
		{
			canvas.webkitRequestFullscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		}
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}
}

function keyDown(event)
{
	switch(event.keyCode)
	{ 
		case 27: // Escape
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
	
		case 70: //for 'f' or 'F'
			toggleFullScreen();
		break;
	}
}

function mouseDown()
{
	// alert("Mouse Is Clicked");
}

function init()
{
	// step 3: Get Drawing Context From Canvas
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		console.log("RTR: Failed To Get The Rendering Context For WebGL\n");
		return;
	}

	console.log("RTR: Get The Rendering Context For WebGL\n");

	// following two statements are specific to webgl context not avaliable in windows opengl context nighter xwindows nor android
	gl.viewportWidth = canvas.width; 
	gl.viewportHeight = canvas.height;

	pep_SceneTransition = new SceneTransition();
	pep_SceneTransition.Init();
	
	gl.clearColor(1.0, 1.0, 1.0, 1.0);
	//gl.clearDepth(1.0);
	//gl.enable(gl.DEPTH_TEST);
	//gl.depthFunc(gl.LEQUAL);
	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		//
		//
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	if (pep_bFadeOut)
    {
        pep_SceneTransition.Draw(true, 0.005);
    }
    else
    {
        pep_SceneTransition.Draw(false, -0.005);
    }

	requestAnimationFrame(draw, canvas); // this function is brower dependent
}

function uninitialize()
{
	if(pep_SceneTransition)
	{
		pep_SceneTransition.Uninit();
		pep_SceneTransition = null;
	}
	
}