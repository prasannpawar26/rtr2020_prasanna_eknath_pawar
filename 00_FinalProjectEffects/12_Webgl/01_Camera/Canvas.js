//
// html and js file should be in one directory
//
// onload function

//
// fullscreen:
// every browser has its own elements for fullscreen.
// 
var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_texcoords;
var pyramid_rotation = 0.0;

var vao_cube;
var vbo_cube_position;
var vbo_cube_texcoords;
var cube_rotation = 0.0;

var pyramid_texture = 0;
var cube_texture = 0;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;
var texture0_samplerUniform;

var perspectiveProjectionMatrix;

var pep_camera = null;
var radius = 10.0;
var camera_rotation_around_object = 0.0;
var cameraPosition = new Float32Array([0.0, 0.0, 10.0]);
var cameraFront = new Float32Array([0.0, 0.0, -1.0]);
var cameraUp = new Float32Array([0.0, 1.0, 0.0]);
var cameraMatrix;

//Camera camera(vec3(0.0f, 1.0f, 10.0f)); // Camera Initial Camera_Position

var requestAnimationFrame =
	window.requestAnimationFrame || /*Google Chrome*/
	window.webkitRequestAnimationFrame || /*Apple - Safari*/
	window.mozRequestAnimationFrame || /*Mozilla Firefox*/
	window.oRequestAnimationFrame || /*Opera*/
	window.msRequestAnimationFrame; /*Microsoft - internet explorer*/

function main()
{
	// step 1: Get canvas from DOM (i.e html)
	// var => runtime la type decide hoil right hind side nusar type (Type-Infurance)
	// document => inbuild variable (by DOM)
	canvas = document.getElementById("PEP_CANVAS");
	if(!canvas)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		console.log("RTR: Obtaining Canvas Failed\n");
	}
	else
	{
		console.log("RTR: Obtaining Canvas Successed\n");
	}

	// step 2: Retrive width and height of canvas for a seak of information
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	//
	// window=> is inbuild variable
	//
	
	//register event handlers
	
	// 3rd parameter is false => bubbled propogation
	// capture propogation => sent to our event listener then to other
	// bubbled propogation =>  event sent from bottom to super class
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("mousemove", mouseMove, false);
	window.addEventListener("resize", resize, false);

	init();

	//warm-up resize call
	resize(); // till now windows, xwindows, android we have just call resize as warm-up call. but in javascript no repaint event hence need to call warm-up draw call.

	//warm-up draw call
	draw();
}

// browser independ code
function toggleFullScreen()
{
	var fullscreen_element =
	document.fullscreenElement || /*Goolge Chrome, Opera*/
	document.webkitFullscreenElement || /*Apple - Safari*/
	document.mozFullScreenElement || /*Mozilla Firefox*/
	document.msFullscreenElement || /* Microsoft - Internet Explore*/
	null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();
		}
		else if(canvas.webkitRequestFullscreen)
		{
			canvas.webkitRequestFullscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		}
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}
}

function keyDown(event)
{
	switch(event.keyCode)
	{ 
		case 27: // Escape
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
	
		case 70: //for 'f' or 'F'
			toggleFullScreen();
		break;
		
		case 65: // 'A' CAMERA_LEFT
		pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_LEFT, 0.1);
		break;
		
		case 68: // 'D' CAMERA_RIGHT
		pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_RIGHT, 0.1);
		break;
		
		case 87: // 'W' CAMERA_FORWARD
		pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_FORWARD, 0.1);
		break;
		
		case 83: // 'S' CAMERA_BACKWARD
		pep_camera.Camera_ProcessKeyBoard(CameraMovementMacros.CAMERA_BACKWARD, 0.1);
		break;
		
		case 82: //'R'
		camera_rotation_around_object += 1.0;
		break;
		
		case 81: //'Q'
		camera_rotation_around_object -= 1.0;
		break;
	}
}

function mouseDown()
{
	// alert("Mouse Is Clicked");
}

function mouseMove(event)
{
	// alert("Mouse Is Clicked");
	console.log("x: "+ event.clientX);
	console.log("y: "+ event.clientY);
}

function init()
{
	pep_camera = new Camera();
	pep_camera.Camera_Init(cameraPosition, cameraFront, cameraUp);

	// step 3: Get Drawing Context From Canvas
	gl = canvas.getContext("webgl2");
	if(!gl)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		console.log("RTR: Failed To Get The Rendering Context For WebGL\n");
		return;
	}

	console.log("RTR: Get The Rendering Context For WebGL\n");

	// following two statements are specific to webgl context not avaliable in windows opengl context nighter xwindows nor android
	gl.viewportWidth = canvas.width; 
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec2 vTexcoords;" +
	
	"out vec2 texCoords;" +
	
	"uniform mat4 u_model_matrix;" +
	"uniform mat4 u_view_matrix;" +
	"uniform mat4 u_projection_matrix;" +
	
	"void main(void)" +
	"{" +
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
		
		//"gl_Position = u_projection_matrix * vPosition;" +
		
		"texCoords = vTexcoords;" +
	"}";
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");
	
	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"in vec2 texCoords;" +
	
	"uniform highp sampler2D u_texture0_sampler;" +
	
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
		"FragColor = texture(u_texture0_sampler, texCoords);" +
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");
	
	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexcoords");
	
	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");
	
	// get MVP uniform location
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	texture0_samplerUniform = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	
		// Load Pyramid Texture
	pyramid_texture = gl.createTexture();
	pyramid_texture.image = new Image();
	pyramid_texture.image.src = "stone.png";
	//pyramid_texture.image.crossOrigin  = "";
	pyramid_texture.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, pyramid_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pyramid_texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	// Load Cube Texture
	cube_texture = gl.createTexture();
	cube_texture.image = new Image();
	cube_texture.image.src = "Vijay_Kundali.png";
	cube_texture.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, cube_texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	// vertices, colors, shader attribs, vbo_square_position, vao_square initializations
	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	
	var pyramidVertices = new Float32Array([
		// front
			0.0, 1.0, 0.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,
			// right
			0.0, 1.0, 0.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,
			// back
			0.0, 1.0, 0.0,
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			// left
			0.0, 1.0, 0.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0, 1.0
	]);

	vbo_pyramid_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var pyramidTexcoords = new Float32Array([
		// Camera_Front
		0.5, 1.0, 0.0, 0.0, 1.0, 0.0,
		// Camera_Right
		0.5, 1.0, 1.0, 0.0, 0.0, 0.0,
		// Back
		0.5, 1.0, 1.0, 0.0, 0.0, 0.0,
		// Left
		0.5, 1.0, 0.0, 0.0, 1.0, 0.0
	]);
	
	vbo_pyramid_texcoords = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_texcoords);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidTexcoords, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	var cubeVertices = new Float32Array(
		[
			// top
			1.0, 1.0, -1.0,
			-1.0, 1.0, -1.0, 
			-1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,  

			// bottom
			1.0, -1.0, -1.0,
			-1.0, -1.0, -1.0,
			-1.0, -1.0,  1.0,
			1.0, -1.0,  1.0,

			// front
			1.0, 1.0, 1.0,
			-1.0, 1.0, 1.0,
			-1.0, -1.0, 1.0,
			1.0, -1.0, 1.0,

			// back
			1.0, 1.0, -1.0,
			-1.0, 1.0, -1.0,
			-1.0, -1.0, -1.0,
			1.0, -1.0, -1.0,

			// right
			1.0, 1.0, -1.0,
			1.0, 1.0, 1.0,
			1.0, -1.0, 1.0,
			1.0, -1.0, -1.0,

			// left
			-1.0, 1.0, 1.0,
			-1.0, 1.0, -1.0, 
			-1.0, -1.0, -1.0, 
			-1.0, -1.0, 1.0]
	);

	for( var i = 0; i < 72; i++)
	{
		if(cubeVertices[i] < 0.0)
			cubeVertices[i] = cubeVertices[i]+0.25;
		else if(cubeVertices[i] > 0.0)
			cubeVertices[i] = cubeVertices[i]-0.25;
		else
			cubeVertices[i] = cubeVertices[i];
	}
	
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	
	vbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var cubeTexcoords = new Float32Array([
		// Top
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Bottom
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Camera_Front
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Back
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Left
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Camera_Right
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0
	]);
	
	vbo_cube_texcoords = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_texcoords);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoords, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);

	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	// initialize projection matrix
	cameraMatrix = mat4.create();
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	if(bFullscreen == true)
	{
		//
		//
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);

	cameraMatrix = pep_camera.Camera_GetViewMatrix();
	
	//
	// camera rotating around himself
	//
	//mat4.rotateY(cameraMatrix, cameraMatrix, degtored(camera_rotation_around_object));
	//mat4.translate(cameraMatrix, cameraMatrix, [0.0, 0.0, radius * -1.0]);
	//
	//
	
	gl.useProgram(shaderProgramObject);
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var projectionMatrix = mat4.create();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionMatrix);
	
	mat4.translate(modelMatrix, modelMatrix, [-2.0, 0.0, /*-8.0*/0.0]);
	mat4.rotateY(modelMatrix, modelMatrix, degtored(pyramid_rotation));

	mat4.copy(viewMatrix, cameraMatrix);
	mat4.copy(projectionMatrix, perspectiveProjectionMatrix);

	gl.bindTexture(gl.TEXTURE_2D, pyramid_texture);
	gl.uniform1i(texture0_samplerUniform, 0);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, projectionMatrix);
	gl.bindVertexArray(vao_pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);
	gl.bindVertexArray(null);
	gl.bindTexture(gl.TEXTURE_2D, null);

	// CUBE
	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionMatrix);

	mat4.translate(modelMatrix, modelMatrix, [2.0, 0.0, /*-8.0*/0.0]);
	mat4.rotateX(modelMatrix, modelMatrix, degtored(cube_rotation));
	mat4.rotateY(modelMatrix, modelMatrix, degtored(cube_rotation));
	mat4.rotateZ(modelMatrix, modelMatrix, degtored(cube_rotation));
	
	mat4.copy(viewMatrix, cameraMatrix);
	mat4.copy(projectionMatrix, perspectiveProjectionMatrix);
	
	gl.bindTexture(gl.TEXTURE_2D, cube_texture);
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, projectionMatrix);
	gl.uniform1i(texture0_samplerUniform, 0);
	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);
	gl.bindTexture(gl.TEXTURE_2D, null);

	gl.useProgram(null);
	
	update();
	requestAnimationFrame(draw, canvas); // this function is brower dependent
}

function update()
{
	// code
	if (360.0 < pyramid_rotation)
	{
		pyramid_rotation = 0.0;
	}
	pyramid_rotation += 1.0;

	if (360.0 < cube_rotation)
	{
		cube_rotation = 0.0;
	}
	cube_rotation += 1.0;
	
	//cameraPosition = pep_camera.Camera_GetPosition();
	//cameraPosition[0] += 0.0;
	//cameraPosition[1] += 0.0;
	//cameraPosition[2] += 0.1;

	//pep_camera.Camera_SetPosition(cameraPosition); // Z-Axis movement
    //pep_camera.Camera_UpdateVectors();
}

function uninitialize()
{
	// code
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_square = null;
	}
	
	if(vbo_cube_texcoords)
	{
		gl.deleteBuffer(vbo_cube_texcoords);
		vbo_cube_texcoords = null;
	}
	
	if(vbo_cube_position)
	{
		gl.deleteBuffer(vbo_cube_position);
		vbo_cube_position = null;
	}
	
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_square = null;
	}
	
	if(vbo_pyramid_texcoords)
	{
		gl.deleteBuffer(vbo_pyramid_texcoords);
		vbo_pyramid_texcoords = null;
	}
	
	if(vbo_pyramid_position)
	{
		gl.deleteBuffer(vbo_pyramid_position);
		vbo_pyramid_position = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}