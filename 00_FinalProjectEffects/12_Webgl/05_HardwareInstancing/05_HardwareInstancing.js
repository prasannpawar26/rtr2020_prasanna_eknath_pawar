//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_NORMAL:1,
	AMC_ATTRIBUTE_TEXTURE0:2,
	AMC_ATTRIBUTE_INSTANCE0:3,
    AMC_ATTRIBUTE_INSTANCE1:4,
    AMC_ATTRIBUTE_INSTANCE2:5,
    AMC_ATTRIBUTE_INSTANCE3:6,
    AMC_ATTRIBUTE_INSTANCE4:7,
    AMC_ATTRIBUTE_INSTANCE5:8,
    AMC_ATTRIBUTE_INSTANCE6:9,
    AMC_ATTRIBUTE_INSTANCE7:10
}

var sphere_elements=[];
var sphere_vertices=[];
var sphere_normals=[];
var sphere_textures=[];

var numElements=0;
var maxElements=0;
var numVertices=0;

var pep_HardwareInstancing_VboPosition=0;
var pep_HardwareInstancing_VboNormal=0;
var pep_HardwareInstancing_VboTexcoord=0;
var pep_HardwareInstancing_VboElement=0;
var pep_HardwareInstancing_Vao=0;
var pep_HardwareInstancing_VboInstance_ModelMatrix;
	
// shader program related variables

var sphere = null;

var pep_HardwareInstancing_giVertexShaderObject;
var pep_HardwareInstancing_giFragmentShaderObject;
var pep_HardwareInstancing_giShaderProgramObject;

var pep_HardwareInstancing_ModelMatrixUniform;
var pep_HardwareInstancing_ViewMatrixUniform;
var pep_HardwareInstancing_ProjectionMatrixUniform;

var pep_HardwareInstancing_LightAmbientUniform;
var pep_HardwareInstancing_LightDiffuseUniform;
var pep_HardwareInstancing_LightSpecularUniform;
var pep_HardwareInstancing_LightPositionUniform;

var pep_HardwareInstancing_LightAmbient = [0.0, 0.0, 0.0];
var pep_HardwareInstancing_LightDiffuse = [1.0, 1.0, 1.0];
var pep_HardwareInstancing_LightSpecular = [1.0, 1.0, 1.0];
var pep_HardwareInstancing_LightPosition = [100.0, 100.0, 100.0, 1.0];

var pep_HardwareInstancing_MaterialAmbientUniform;
var pep_HardwareInstancing_MaterialDiffuseUniform;
var pep_HardwareInstancing_MaterialSpecularUniform;
var pep_HardwareInstancing_MaterialShinessUniform;

// material configuration
var pep_HardwareInstancing_MaterialAmbient = [0.0, 0.0, 0.0];
var pep_HardwareInstancing_MaterialDiffuse = [1.0, 1.0, 1.0];
var pep_HardwareInstancing_MaterialSpecular = [1.0, 1.0, 1.0];
var pep_HardwareInstancing_MaterialShiness = 50.0;

//////////////////

var matrices = [
  mat4.create(),
  mat4.create(),
  mat4.create(),
  mat4.create(),
];

//////////////////

// matrix
var perspectiveProjectionMatrix;
var modelMatrix;
var	viewMatrix;

// light toggled variable
var bLKeyPressed = false;

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element =
	document.fullscreenElement || /*Goolge Chrome, Opera*/
	document.webkitFullscreenElement || /*Apple - Safari*/
	document.mozFullScreenElement || /*Mozilla Firefox*/
	document.msFullscreenElement || /* Microsoft - Internet Explore*/
	null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();
		}
		else if(canvas.webkitRequestFullscreen)
		{
			canvas.webkitRequestFullscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		}
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
		
		bFullscreen = true;
	}
	else // already fullscreen
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.mozCancelFullScreen)
		{
			document.mozCancelFullScreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
		
		bFullscreen = false;
	}
}
function init()
{
	// code
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"layout (location = 0) in vec4 vPosition;" +
	"layout (location = 1) in vec3 vNormal;" +
	"layout (location = 2) in vec2 vTexCoord;" +
	"layout (location = 3) in mat4 vModelMatrix;" +

	"uniform mat4 u_model_matrix;" +
	"uniform mat4 u_view_matrix;" +
	"uniform mat4 u_projection_matrix;" +
	"uniform vec3 u_light_ambient;" +
	"uniform vec3 u_light_diffuse;" +
	"uniform vec3 u_light_specular;" +
	"uniform vec4 u_light_position;" +

	"out vec3 light_direction0;" +
	"out vec3 tranformation_matrix;" +
	"out vec3 viewer_vector;" +

	"out vec3 material_ambient;" +
	"out vec3 material_diffuse;" +
	"out vec3 material_specular;" +
	"out float material_shiness;" +

	"void main(void)" +
	"{" +
		"vec4 eye_coordinates = u_view_matrix * vModelMatrix * vPosition;" +
		"tranformation_matrix = mat3(u_view_matrix * vModelMatrix) * vNormal;" +
		"viewer_vector = vec3(-eye_coordinates);" +
		"light_direction0 = vec3(u_light_position - eye_coordinates);" +

		"gl_Position = u_projection_matrix * u_view_matrix * vModelMatrix * vPosition;" +
    "}";

	pep_HardwareInstancing_giVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(pep_HardwareInstancing_giVertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(pep_HardwareInstancing_giVertexShaderObject);
	if(gl.getShaderParameter(pep_HardwareInstancing_giVertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(pep_HardwareInstancing_giVertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : pep_HardwareInstancing_giVertexShaderObject Compiled Successfully");

	// fragment shader
	var fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"in vec3 light_direction0;" +
	"in vec3 tranformation_matrix;" +
	"in vec3 viewer_vector;" +

	"uniform vec3 u_material_ambient;" +
	"uniform vec3 u_material_diffuse;" +
	"uniform vec3 u_material_specular;" +
	"uniform float u_material_shiness;" +

	"uniform vec3 u_light_ambient;" +
	"uniform vec3 u_light_diffuse;" +
	"uniform vec3 u_light_specular;" +
	"uniform vec4 u_light_position;" +

	"out vec4 FragColor;" +

	"void main(void)" +
	"{" +

		"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
		"vec3 viewer_vector_normal = normalize(viewer_vector);" +

		"vec3 light_direction_normalize0 = normalize(light_direction0);" +
		"vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" +
		"float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" +

		"vec3 ambient0 = u_light_ambient * u_material_ambient;" +
		"vec3 diffuse0 = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction0;" +
		"vec3 specular0 = u_light_specular * u_material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_shiness);" +

		"vec3 phong_ads_light = ambient0 + diffuse0 + specular0;" +

		"FragColor = vec4(phong_ads_light, 1.0);" +
		
		//"FragColor = vec4(1.0, 1.0, 0.0, 1.0);" +
	"}";

	pep_HardwareInstancing_giFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(pep_HardwareInstancing_giFragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(pep_HardwareInstancing_giFragmentShaderObject);
	if(gl.getShaderParameter(pep_HardwareInstancing_giFragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(pep_HardwareInstancing_giFragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : pep_HardwareInstancing_giFragmentShaderObject Compiled Successfully");

	// shader program
	pep_HardwareInstancing_giShaderProgramObject = gl.createProgram();
	gl.attachShader(pep_HardwareInstancing_giShaderProgramObject, pep_HardwareInstancing_giVertexShaderObject);
	gl.attachShader(pep_HardwareInstancing_giShaderProgramObject, pep_HardwareInstancing_giFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
	gl.bindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");
	gl.bindAttribLocation(pep_HardwareInstancing_giShaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_INSTANCE0, "vModelMatrix");

	// linking
	gl.linkProgram(pep_HardwareInstancing_giShaderProgramObject);
	if(!gl.getProgramParameter(pep_HardwareInstancing_giShaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(pep_HardwareInstancing_giShaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : pep_HardwareInstancing_giShaderProgramObject Linked Successfully");

	pep_HardwareInstancing_ModelMatrixUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_model_matrix");
	pep_HardwareInstancing_ViewMatrixUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_view_matrix");
    pep_HardwareInstancing_ProjectionMatrixUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_projection_matrix");
    pep_HardwareInstancing_LightAmbientUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_ambient");
    pep_HardwareInstancing_LightDiffuseUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_diffuse");
    pep_HardwareInstancing_LightSpecularUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_specular");
    pep_HardwareInstancing_LightPositionUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_light_position");

	pep_HardwareInstancing_MaterialAmbientUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_material_ambient");
	pep_HardwareInstancing_MaterialDiffuseUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_material_diffuse");
	pep_HardwareInstancing_MaterialSpecularUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_material_specular");
	pep_HardwareInstancing_MaterialShinessUniform = gl.getUniformLocation(pep_HardwareInstancing_giShaderProgramObject, "u_material_shiness");
	
	// vertices, colors, shader attribs, vbo, pep_HardwareInstancing_Vao initializations
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);

	pep_HardwareInstancing_Vao=gl.createVertexArray();
	gl.bindVertexArray(pep_HardwareInstancing_Vao);
	
	// vbo for position
	pep_HardwareInstancing_VboPosition=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,pep_HardwareInstancing_VboPosition);
	gl.bufferData(gl.ARRAY_BUFFER,sphere_vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
						   3,
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	// vbo for normals
	pep_HardwareInstancing_VboNormal=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,pep_HardwareInstancing_VboNormal);
	gl.bufferData(gl.ARRAY_BUFFER,
				  sphere_normals,
				  gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,
						   3,
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	// vbo for texture
	pep_HardwareInstancing_VboTexcoord=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,pep_HardwareInstancing_VboTexcoord);
	gl.bufferData(gl.ARRAY_BUFFER,
				  sphere_textures,
				  gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,
						   2, // 2 is for S,T co-ordinates in our sphere_textures array
						   gl.FLOAT,
						   false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	// vbo for index
	pep_HardwareInstancing_VboElement=gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,pep_HardwareInstancing_VboElement);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
				  sphere_elements,
				  gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
	
	
	/*
		Instancing
	*/
	mat4.identity(matrices[0]);
	mat4.translate(matrices[0], matrices[0], [-7.50, 5.0, -17.0]);
	mat4.identity(matrices[1]);
	mat4.translate(matrices[1], matrices[1], [-7.50, -5.0, -17.0]);
	mat4.identity(matrices[2]);
	mat4.translate(matrices[2], matrices[2], [7.50, -5.0, -17.0]);
	mat4.identity(matrices[3]);
	mat4.translate(matrices[3], matrices[3], [7.50, 5.0, -17.0]);
	
	var instanceData = new Float32Array(64);
	
	var i = 0;
	for(var index = 0; index < 4; index += 1)
	{
		 instanceData[i + 0] = matrices[index][0];
		 instanceData[i + 1] = matrices[index][1];
		 instanceData[i + 2] = matrices[index][2];
		 instanceData[i + 3] = matrices[index][3];
		 instanceData[i + 4] = matrices[index][4];
		 instanceData[i + 5] = matrices[index][5];
		 instanceData[i + 6] = matrices[index][6];
		 instanceData[i + 7] = matrices[index][7];
		 instanceData[i + 8] = matrices[index][8];
		 instanceData[i + 9] = matrices[index][9];
		 instanceData[i + 10] = matrices[index][10];
		 instanceData[i + 11] = matrices[index][11];
		 instanceData[i + 12] = matrices[index][12];
		 instanceData[i + 13] = matrices[index][13];
		 instanceData[i + 14] = matrices[index][14];
		 instanceData[i + 15] = matrices[index][15];
		 
		 i += 16;
	}
	
	//console.log(matrices);
	console.log(instanceData);
	
	pep_HardwareInstancing_VboInstance_ModelMatrix = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, pep_HardwareInstancing_VboInstance_ModelMatrix);
	gl.bufferData(gl.ARRAY_BUFFER, instanceData, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_INSTANCE0, 4, gl.FLOAT, false, 64, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_INSTANCE0);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_INSTANCE1, 4, gl.FLOAT, false, 64, 16);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_INSTANCE1);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_INSTANCE2, 4, gl.FLOAT, false, 64, 32);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_INSTANCE2);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_INSTANCE3, 4, gl.FLOAT, false, 64, 48);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_INSTANCE3);
	
	gl.vertexAttribDivisor(WebGLMacros.AMC_ATTRIBUTE_INSTANCE0, 1);
    gl.vertexAttribDivisor(WebGLMacros.AMC_ATTRIBUTE_INSTANCE1, 1);
    gl.vertexAttribDivisor(WebGLMacros.AMC_ATTRIBUTE_INSTANCE2, 1);
    gl.vertexAttribDivisor(WebGLMacros.AMC_ATTRIBUTE_INSTANCE3, 1);
	
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	perspectiveProjectionMatrix = mat4.create();
	modelMatrix = mat4.create();
	viewMatrix = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(pep_HardwareInstancing_giShaderProgramObject);

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(pep_HardwareInstancing_ModelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(pep_HardwareInstancing_ViewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(pep_HardwareInstancing_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	gl.uniform3fv(pep_HardwareInstancing_LightAmbientUniform, pep_HardwareInstancing_LightAmbient);
    gl.uniform3fv(pep_HardwareInstancing_LightDiffuseUniform, pep_HardwareInstancing_LightDiffuse);
    gl.uniform3fv(pep_HardwareInstancing_LightSpecularUniform, pep_HardwareInstancing_LightSpecular);
    gl.uniform4fv(pep_HardwareInstancing_LightPositionUniform, pep_HardwareInstancing_LightPosition);

	gl.uniform3fv(pep_HardwareInstancing_MaterialAmbientUniform, pep_HardwareInstancing_MaterialAmbient);
	gl.uniform3fv(pep_HardwareInstancing_MaterialDiffuseUniform, pep_HardwareInstancing_MaterialDiffuse);
	gl.uniform3fv(pep_HardwareInstancing_MaterialSpecularUniform, pep_HardwareInstancing_MaterialSpecular);
	gl.uniform1f(pep_HardwareInstancing_MaterialShinessUniform, pep_HardwareInstancing_MaterialShiness);

	// code
	gl.bindVertexArray(pep_HardwareInstancing_Vao);

	// draw
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, pep_HardwareInstancing_VboElement);
	//gl.drawElements(gl.TRIANGLES, numElements, gl.UNSIGNED_SHORT, 0);
	gl.drawElementsInstanced(gl.TRIANGLES, numElements, gl.UNSIGNED_SHORT, 0, 4);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// code
	if(pep_HardwareInstancing_Vao)
	{
		gl.deleteVertexArray(pep_HardwareInstancing_Vao);
		pep_HardwareInstancing_Vao=null;
	}
	
	if(pep_HardwareInstancing_VboElement)
	{
		gl.deleteBuffer(pep_HardwareInstancing_VboElement);
		pep_HardwareInstancing_VboElement=null;
	}
	
	if(pep_HardwareInstancing_VboTexcoord)
	{
		gl.deleteBuffer(pep_HardwareInstancing_VboTexcoord);
		pep_HardwareInstancing_VboTexcoord=null;
	}
	
	if(pep_HardwareInstancing_VboNormal)
	{
		gl.deleteBuffer(pep_HardwareInstancing_VboNormal);
		pep_HardwareInstancing_VboNormal=null;
	}
	
	if(pep_HardwareInstancing_VboPosition)
	{
		gl.deleteBuffer(pep_HardwareInstancing_VboPosition);
		pep_HardwareInstancing_VboPosition=null;
	}
	
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}
		
	if(pep_HardwareInstancing_giShaderProgramObject)
	{
		if(pep_HardwareInstancing_giFragmentShaderObject)
		{
			gl.detachShader(pep_HardwareInstancing_giShaderProgramObject, pep_HardwareInstancing_giFragmentShaderObject);
			gl.deleteShader(pep_HardwareInstancing_giFragmentShaderObject);
			pep_HardwareInstancing_giFragmentShaderObject = null;
		}

		if(pep_HardwareInstancing_giVertexShaderObject)
		{
			gl.detachShader(pep_HardwareInstancing_giShaderProgramObject, pep_HardwareInstancing_giVertexShaderObject);
			gl.deleteShader(pep_HardwareInstancing_giVertexShaderObject);
			pep_HardwareInstancing_giVertexShaderObject = null;
		}

		gl.deleteProgram(pep_HardwareInstancing_giShaderProgramObject);
		pep_HardwareInstancing_giShaderProgramObject = null;
	}
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 70: // for 'f' or 'F'
			toggleFullScreen();
		break;

		case 27: // Escape
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
		
		case 76: // for 'L' or 'l'
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else
				bLKeyPressed = false;
		break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	// code
}