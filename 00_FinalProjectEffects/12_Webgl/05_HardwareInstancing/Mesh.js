// functions use without 'this' prefix are private to class.
// functions with 'this' prefix are 'public' and hence when they use
function Mesh()
{
    

    this.allocate=function(numIndices)
    {
        // code
        // first cleanup, if not initially empty
        cleanupMeshData();
        
        maxElements = numIndices;
        numElements = 0;
        numVertices = 0;
        
        var iNumIndices=numIndices/3;
        
        sphere_elements = new Uint16Array(iNumIndices * 3 * 2); // 3 is x,y,z and 2 is sizeof short
        sphere_vertices = new Float32Array(iNumIndices * 3 * 4); // 3 is x,y,z and 4 is sizeof float
        sphere_normals = new Float32Array(iNumIndices * 3 * 4); // 3 is x,y,z and 4 is sizeof float
        sphere_textures = new Float32Array(iNumIndices * 2 * 4); // 2 is s,t and 4 is sizeof float
    }

    // Add 3 vertices, 3 normal and 2 texcoords i.e. one triangle to the geometry.
    // This searches the current list for identical vertices (exactly or nearly) and
    // if one is found, it is added to the index array.
    // if not, it is added to both the index array and the vertex array.
    this.addTriangle=function(single_vertex, single_normal, single_texture)
    {
        //variable declarations
        const diff = 0.00001;
        var i, j;
        // code
        // normals should be of unit length
        normalizeVector(single_normal[0]);
        normalizeVector(single_normal[1]);
        normalizeVector(single_normal[2]);
        
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < numVertices; j++) //for the first ever iteration of 'j', numVertices will be 0 because of it's initialization in the parameterized constructor
            {
                if (isFoundIdentical(sphere_vertices[j * 3], single_vertex[i][0], diff) &&
                    isFoundIdentical(sphere_vertices[(j * 3) + 1], single_vertex[i][1], diff) &&
                    isFoundIdentical(sphere_vertices[(j * 3) + 2], single_vertex[i][2], diff) &&
                    
                    isFoundIdentical(sphere_normals[j * 3], single_normal[i][0], diff) &&
                    isFoundIdentical(sphere_normals[(j * 3) + 1], single_normal[i][1], diff) &&
                    isFoundIdentical(sphere_normals[(j * 3) + 2], single_normal[i][2], diff) &&
                    
                    isFoundIdentical(sphere_textures[j * 2], single_texture[i][0], diff) &&
                    isFoundIdentical(sphere_textures[(j * 2) + 1], single_texture[i][1], diff))
                {
                    sphere_elements[numElements] = j;
                    numElements++;
                    break;
                }
            }
            
            //If the single vertex, normal and texture do not match with the given, then add the corressponding triangle to the end of the list
            if (j == numVertices && numVertices < maxElements && numElements < maxElements)
            {
                sphere_vertices[numVertices * 3] = single_vertex[i][0];
                sphere_vertices[(numVertices * 3) + 1] = single_vertex[i][1];
                sphere_vertices[(numVertices * 3) + 2] = single_vertex[i][2];
                
                sphere_normals[numVertices * 3] = single_normal[i][0];
                sphere_normals[(numVertices * 3) + 1] = single_normal[i][1];
                sphere_normals[(numVertices * 3) + 2] = single_normal[i][2];
                
                sphere_textures[numVertices * 2] = single_texture[i][0];
                sphere_textures[(numVertices * 2) + 1] = single_texture[i][1];
                
                sphere_elements[numElements] = numVertices; //adding the index to the end of the list of sphere_elements/indices
                numElements++; //incrementing the 'end' of the list
                numVertices++; //incrementing coun of vertices
            }
        }
    }
    
    this.prepareToDraw=function()
    {
        
        
        // after sending data to GPU, now we can free our arrays
        //cleanupMeshData();
    }
    
    this.draw=function()
    {
        
    }
    
    this.getIndexCount=function()
    {
        // code
        return(numElements);
    }
    
    this.getVertexCount=function()
    {
        // code
        return(numVertices);
    }
    
    normalizeVector=function(v)
    {
        // code
        
        // square the vector length
        var squaredVectorLength=(v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
        
        // get square root of above 'squared vector length'
        var squareRootOfSquaredVectorLength=Math.sqrt(squaredVectorLength);
        
        // scale the vector with 1/squareRootOfSquaredVectorLength
        v[0] = v[0] * 1.0/squareRootOfSquaredVectorLength;
        v[1] = v[1] * 1.0/squareRootOfSquaredVectorLength;
        v[2] = v[2] * 1.0/squareRootOfSquaredVectorLength;
    }
    
    isFoundIdentical=function(val1, val2, diff)
    {
        // code
        if(Math.abs(val1 - val2) < diff)
            return(true);
        else
            return(false);
    }
    
    cleanupMeshData=function()
    {
        // code
        if(sphere_elements!=null)
        {
            sphere_elements=null;
        }
        
        if(sphere_vertices!=null)
        {
            sphere_vertices=null;
        }
        
        if(sphere_normals!=null)
        {
            sphere_normals=null;
        }
        
        if(sphere_textures!=null)
        {
            sphere_textures=null;
        }
    }
    
    this.deallocate=function()
    {
        
    }
}
