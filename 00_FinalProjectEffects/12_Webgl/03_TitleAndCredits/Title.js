
var pep_gTitle_Texture = 1;

function Title()
{
	var pep_Title_vs;
	var pep_Title_fs;
	var pep_Title_program;

	var pep_Title_vao;
	var pep_Title_vbo_position;
	var pep_Title_vbo_texcoords;

	var pep_Title_modelMatrixUniform;
	var pep_Title_viewMatrixUniform;
	var pep_Title_projectionMatrixUniform;
	var pep_Title_samplerUniform;
	
	var pep_Title_texture_1 = 0;
	var pep_Title_texture_2 = 0;
	var pep_Title_texture_3 = 0;
	
	this.Init=function()
	{
		// vertex shader
		var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTexcoords;" +
		
		"out vec2 texCoords;" +
		
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		
		"void main(void)" +
		"{" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"texCoords = vTexcoords;" +
		"}";
		
		pep_Title_vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(pep_Title_vs, vertexShaderSourceCode);
		gl.compileShader(pep_Title_vs);
		if(gl.getShaderParameter(pep_Title_vs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Title_vs);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		console.log("RTR : vertexShaderObject Compiled Successfully");
		
		// fragment shader
		var fragmentShaderSourceCode = 
		"#version 300 es" +
		"\n" +
		"precision highp float;"+
		"in vec2 texCoords;" +
		
		"uniform highp sampler2D u_texture0_sampler;" +
		
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = texture(u_texture0_sampler, texCoords);" +
		"}";
		
		pep_Title_fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(pep_Title_fs, fragmentShaderSourceCode);
		gl.compileShader(pep_Title_fs);
		if(gl.getShaderParameter(pep_Title_fs, gl.COMPILE_STATUS) == false)
		{
			var error = gl.getShaderInfoLog(pep_Title_fs);
			if(erro.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		console.log("RTR : fragmentShaderObject Compiled Successfully");
		
		// shader program
		pep_Title_program = gl.createProgram();
		gl.attachShader(pep_Title_program, pep_Title_vs);
		gl.attachShader(pep_Title_program, pep_Title_fs);
		
		// pre-link binding of shader program object with vertex shader attributes
		gl.bindAttribLocation(pep_Title_program, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
		gl.bindAttribLocation(pep_Title_program, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexcoords");
		
		// linking
		gl.linkProgram(pep_Title_program);
		if(!gl.getProgramParameter(pep_Title_program, gl.LINK_STATUS))
		{
			var error = gl.getProgramInfoLog(pep_Title_program);
			if(error.length > 0)
			{
				alert(error);
				uninitialize();
			}
		}
		console.log("RTR : shaderProgramObject Linked Successfully");
		
		// get uniform location
		pep_Title_modelMatrixUniform = gl.getUniformLocation(pep_Title_program, "u_model_matrix");
		pep_Title_viewMatrixUniform = gl.getUniformLocation(pep_Title_program, "u_view_matrix");
		pep_Title_projectionMatrixUniform = gl.getUniformLocation(pep_Title_program, "u_projection_matrix");
		pep_Title_samplerUniform = gl.getUniformLocation(pep_Title_program, "u_texture0_sampler");
		
		// Load Cube Texture
		pep_Title_texture_1 = gl.createTexture();
		pep_Title_texture_1.image = new Image();
		pep_Title_texture_1.image.src = "title_1.png";
		pep_Title_texture_1.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, pep_Title_texture_1);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pep_Title_texture_1.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		pep_Title_texture_2 = gl.createTexture();
		pep_Title_texture_2.image = new Image();
		pep_Title_texture_2.image.src = "title_2.png";
		pep_Title_texture_2.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, pep_Title_texture_2);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pep_Title_texture_2.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		pep_Title_texture_3 = gl.createTexture();
		pep_Title_texture_3.image = new Image();
		pep_Title_texture_3.image.src = "title_3.png";
		pep_Title_texture_3.image.onload = function ()
		{
			gl.bindTexture(gl.TEXTURE_2D, pep_Title_texture_3);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, pep_Title_texture_3.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		
		// vertices, colors, shader attribs, vbo_square_position, vao_square initializations
		var vertices = new Float32Array(
			[
				-1.0, -1.0, 1.0,
				-1.0, 1.0, 1.0,
				1.0, 1.0, 1.0,
				1.0, -1.0, 1.0
			]
		);
		
		pep_Title_vao = gl.createVertexArray();
		gl.bindVertexArray(pep_Title_vao);
		
		pep_Title_vbo_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Title_vbo_position);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		var texcoords = new Float32Array([
			//Front
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0
		]);
		
		pep_Title_vbo_texcoords = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, pep_Title_vbo_texcoords);
		gl.bufferData(gl.ARRAY_BUFFER, texcoords, gl.STATIC_DRAW);
		gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
		gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		
		gl.bindVertexArray(null);
	} // Init End
	
	this.Uninit=function()
	{
		if(pep_Title_vao)
		{
			gl.deleteVertexArray(pep_Title_vao);
			pep_Title_vao = null;
		}
		
		if(pep_Title_vbo_texcoords)
		{
			gl.deleteBuffer(pep_Title_vbo_texcoords);
			pep_Title_vbo_texcoords = null;
		}
		
		if(pep_Title_vbo_position)
		{
			gl.deleteBuffer(pep_Title_vbo_position);
			pep_Title_vbo_position = null;
		}

		if(pep_Title_program)
		{
			if(pep_Title_fs)
			{
				gl.detachShader(pep_Title_program, pep_Title_fs);
				gl.deleteShader(pep_Title_fs);
				pep_Title_fs = null;
			}
			
			if(pep_Title_vs)
			{
				gl.detachShader(pep_Title_program, pep_Title_vs);
				gl.deleteShader(pep_Title_vs);
				pep_Title_vs = null;
			}
			
			gl.deleteProgram(pep_Title_program);
			pep_Title_program = null;
		}
	}
	
	this.Resize=function(width, height)
	{
	}
	
	this.Update=function()
	{
	}
	
	this.Draw=function()
	{
		gl.useProgram(pep_Title_program);
	
		var modelMatrix = mat4.create();
		var viewMatrix = mat4.create();
		var projectionMatrix = mat4.create();

		mat4.identity(modelMatrix);
		mat4.identity(viewMatrix);
		mat4.identity(projectionMatrix);
		mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -3.0]);
		mat4.copy(projectionMatrix, perspectiveProjectionMatrix);

		gl.uniformMatrix4fv(pep_Title_modelMatrixUniform, false, modelMatrix);
		gl.uniformMatrix4fv(pep_Title_viewMatrixUniform, false, viewMatrix);
		gl.uniformMatrix4fv(pep_Title_projectionMatrixUniform, false, projectionMatrix);
		gl.uniform1i(pep_Title_samplerUniform, 0);
		
		var title_texture = 0;
		if(1 == pep_gTitle_Texture)
		{
			title_texture = pep_Title_texture_1;
		}
		else if(2 == pep_gTitle_Texture)
		{
			title_texture = pep_Title_texture_2;
		}
		else if(3 == pep_gTitle_Texture)
		{
			title_texture = pep_Title_texture_3;
		}
		
		gl.bindTexture(gl.TEXTURE_2D, /*pep_Title_texture_1*/title_texture);
		gl.bindVertexArray(pep_Title_vao);
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
		gl.bindVertexArray(null);
		gl.bindTexture(gl.TEXTURE_2D, null);

		gl.useProgram(null);
	}
}

