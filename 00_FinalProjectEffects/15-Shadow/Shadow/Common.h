#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

#include <stdio.h>
#include <windows.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#include "vmath.h"
#include "Sphere.h"

using namespace std;
using namespace vmath;

enum
{
	AMC_ATTRIBUTES_POSITION = 0,
	AMC_ATTRIBUTES_COLOR,
	AMC_ATTRIBUTES_NORMAL,
	AMC_ATTRIBUTES_TEXTCOORD0
};