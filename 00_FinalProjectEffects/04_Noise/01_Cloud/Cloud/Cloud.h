#pragma once

#include "Common.h"

#define CLOUD_IMAGE_WIDTH 128
#define CLOUD_IMAGE_HEIGHT 128
#define CLOUD_IMAGE_DEPTH 128

int Cloud_Initialize(void);
void Cloud_Resize(int, int);
void Cloud_Uninitialize(void);
void Cloud_Update(void);
void Cloud_Display(void);
