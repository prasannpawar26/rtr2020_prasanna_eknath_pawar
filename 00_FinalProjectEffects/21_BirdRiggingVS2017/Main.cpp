
#include "Common.h"
#include "BirdModel.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
//#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "assimp-vc141-mtd.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

float lightPosition[4] = {64.0f, 50.0f, 64.0f/*-0.699999332f, 5.88960028f, 14.0132608f*/, 1.00000000f}/*{0.0f, 3.589553f, -2.286757f, 1.000000f}*/;
//0x00007ff7ef845698 {12.2000113, 5.88960028, 14.0132608, 1.00000000}
HDC gHdc = NULL;
HGLRC gHglrc = NULL;
FILE *pep_gpFile = NULL;
HWND gHwnd = NULL;
DWORD dwStyle;
WINDOWPLACEMENT gWpPrev = {sizeof(WINDOWPLACEMENT)};

bool pep_bFadeOut;
bool gbIsFullScreen;
int pep_gWidth = 800;
int pep_gHeight = 600;

mat4 pep_Perspective_ProjectionMatrix;

Camera camera(vec3(0.0f, 0.0f, 4.0f)); // Camera Initial Position
mat4 cameraMatrix;
vec3 cameraPosition(vec3(0.0f, 0.0f, 4.0f));
vec3 cameraFront(vec3(0.0f, 0.0f, 0.0f));

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpSzCmdLine, int iCmdShow)
{
	// function prototype
	int Initialize(void);
	void Display(void);
	void Update(void);
	void ToggledFullScreen(void);

	// variable declarations
	bool bDone = false;
	MSG msg = {0};
	int iRet = 0;
	HWND hwnd;
	WNDCLASSEX wndClass;
	TCHAR szAppName[] = TEXT("MatrixGroup");

	// code
	if (0 != fopen_s(&pep_gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Created Successfully "
		"Successfully\n",
		__FILE__, __LINE__, __FUNCTION__);

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName, TEXT("MatrixGroup"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100,
		100, pep_gWidth, pep_gHeight, NULL, NULL, hInstance, NULL);
	gHwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	iRet = Initialize();
	if (0 != iRet)
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Choose Pixel Format "
			"Failed.\nExitting Now... ",
			__FILE__, __LINE__, __FUNCTION__);
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(pep_gpFile,
			"FILENAME: %s LINE: %d FUNCTION: %s : Initialize "
			"Successful.\n",
			__FILE__, __LINE__, __FUNCTION__);
	}

	//if (!PlaySoundW(L"Peder_B_Helland_Always_Final.wav", NULL,
	//	SND_NODEFAULT | SND_ASYNC | SND_FILENAME/* | SND_LOOP*/)) {
	//	fprintf(pep_gpFile, "PlaySound FAILED\n");
	//}

	//ToggledFullScreen();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (WM_QUIT == msg.message)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			Update();
			Display();
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggledFullScreen(void);
	void Uninitialize(void);
	void ReSize(int, int);

	// Variable Declarations
	POINT pt;

	static int old_x_pos;
	static int old_y_pos;

	static int new_x_pos;
	static int new_y_pos;

	static int x_offset;
	static int y_offset;

	// code
	switch (iMsg)
	{

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	case WM_ERASEBKGND:
		return 0;
		break;
	//case WM_MOUSEMOVE:
	//	new_x_pos = GET_X_LPARAM(lParam);
	//	new_y_pos = GET_Y_LPARAM(lParam);


	//	x_offset = new_x_pos - old_x_pos;
	//	y_offset = new_y_pos - old_y_pos;


	//	if (new_x_pos == 0 && old_x_pos < 10)
	//	{
	//		SetCursorPos(1920, new_y_pos);
	//		new_x_pos = 1920;
	//	}
	//	if (new_x_pos == 1919 && old_x_pos > 1900)
	//	{
	//		SetCursorPos(0, new_y_pos);
	//		new_x_pos = 0;
	//	}

	//	old_x_pos = new_x_pos;
	//	old_y_pos = new_y_pos;


	//	camera.ProcessMouseMovement(x_offset, y_offset);

	//	break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'F':
		case 'f':
			ToggledFullScreen();
			break;

		case 'T':
		case 't':
			//pep_Global_lightPosition[1] += 6.0f;
			fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Camera Position: vec3(%f, %f %f), Front: vec3(%f, %f, %f) Yaw: %f, Pitch: %f\n",
				__FILE__, __LINE__, __FUNCTION__, camera.Position[0], camera.Position[1], camera.Position[2], camera.Front[0], camera.Front[1], camera.Front[2], camera.Yaw, camera.Pitch);
			break;
			/*case 'G':
			case 'g':
			pep_Global_lightPosition[1] -= 6.0f;
			break;*/
		case 'A':
		case 'a':
			// if (gbIsCameraMovement)
			camera.ProcessKeyboard(LEFT);
			break;

		case 'W':
		case 'w':
			//if (gbIsCameraMovement)
			camera.ProcessKeyboard(FORWARD);
			break;

		case 'D':
		case 'd':
			//if (gbIsCameraMovement)
			camera.ProcessKeyboard(RIGHT);
			break;

		case 'S':
		case 's':
			//if (gbIsCameraMovement)
			camera.ProcessKeyboard(BACKWARD);
			break;
		}
	} break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggledFullScreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (false == gbIsFullScreen)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (WS_OVERLAPPEDWINDOW & dwStyle)
		{
			mi = {sizeof(MONITORINFO)};

			if (GetWindowPlacement(gHwnd, &gWpPrev) &&
				GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.top,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &gWpPrev);
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER |
			SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullScreen = false;
		ShowCursor(TRUE);
	}
}

void Display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	BirdModel_Display();
	SwapBuffers(gHdc);

	return;
}

void ReSize(int width, int height)
{
	// code
	if (0 == height)
	{
		height = 1;
	}
	pep_gWidth = width;
	pep_gHeight = height;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	pep_Perspective_ProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 5000.0f);

	return;
}

void Uninitialize(void)
{
	BirdModel_Uninitialize();

	if (wglGetCurrentContext() == gHglrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (NULL != gHglrc)
	{
		wglDeleteContext(gHglrc);
	}

	if (NULL != gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
	}

	fprintf(
		pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : Log File Closed Successfully \n",
		__FILE__, __LINE__, __FUNCTION__);

	if (NULL != pep_gpFile)
	{
		fclose(pep_gpFile);
	}

	return;
}

void Update(void)
{
	BirdModel_Update();
	camera.updateCameraVectors();
}

int Initialize(void)
{
	// variable declarations
	int pep_CubeMarching_index;
	PIXELFORMATDESCRIPTOR pep_CubeMarching_pfd;

	void ReSize(int, int);
	void Uninitialize(void);

	// code
	ZeroMemory(&pep_CubeMarching_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pep_CubeMarching_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pep_CubeMarching_pfd.nVersion = 1;
	pep_CubeMarching_pfd.cColorBits = 32;
	pep_CubeMarching_pfd.cDepthBits = 8;
	pep_CubeMarching_pfd.cRedBits = 8;
	pep_CubeMarching_pfd.cGreenBits = 8;
	pep_CubeMarching_pfd.cBlueBits = 8;
	pep_CubeMarching_pfd.cAlphaBits = 8;
	pep_CubeMarching_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize Start\n",
		__FILE__, __LINE__, __FUNCTION__);

	gHdc = GetDC(gHwnd);

	pep_CubeMarching_index = ChoosePixelFormat(gHdc, &pep_CubeMarching_pfd);
	if (0 == pep_CubeMarching_index) {
		return -1;
	}

	if (FALSE == SetPixelFormat(gHdc, pep_CubeMarching_index, &pep_CubeMarching_pfd)) {
		return -2;
	}

	gHglrc = wglCreateContext(gHdc);
	if (NULL == gHglrc)
	{
		return -3;
	}

	if (FALSE == wglMakeCurrent(gHdc, gHglrc)) {
		return -4;
	}

	fprintf(pep_gpFile,
		"FILENAME: %s LINE: %d FUNCTION: %s : wglCreateContext Successful\n",
		__FILE__, __LINE__, __FUNCTION__);

	GLenum pep_CubeMarching_result;
	pep_CubeMarching_result = glewInit();
	if (GLEW_OK != pep_CubeMarching_result)
	{
		return -5;
	}	

	gHglrc = wglCreateContextAttribsARB(gHdc, NULL, NULL);
	if (gHglrc == NULL)
	{
		DestroyWindow(gHwnd);
		return -4;
	}

	if (wglMakeCurrent(gHdc, gHglrc) == FALSE)
	{
		DestroyWindow(gHwnd);
		return -5;
	}

	BirdModel_Initialize();

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);


	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	pep_Perspective_ProjectionMatrix = mat4::identity();

	ReSize(pep_gWidth, pep_gHeight);

	fprintf(pep_gpFile, "FILENAME: %s LINE: %d FUNCTION: %s : Initialize End\n",
		__FILE__, __LINE__, __FUNCTION__);
	return 0;
}
