#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <memory.h>

typedef struct _NODE
{
	int data;
	struct _NODE *next;
}NODE, *PNODE;

enum
{
	EXIT = 0,
	CREATE_LIST,
	INSERT_AT_BEGINNING,
	INSERT_AT_END,
	INSERT_AT_POSITION,
	REMOVE_FROM_BEGINNING,
	REMOVE_FROM_END,
	REMOVE_FROM_POSITION,
	SHOW_DATA,
	DELETE_LIST
};

// Function Declarations
NODE *CreateList(void);
int InsertAtBeginning(NODE *, int);
int InsertAtEnd(NODE *, int);
int InsertAtPostion(NODE *head, int position, int data);
void DeleteList(void);
int RemoveFromBeginning(NODE *);
int RemoveFromEnd(NODE *);
int RemoveFromPosition(NODE *, int);
void ShowList(void);

// Global Variables
NODE *gpHead = NULL;

int main(void)
{
	// Variable Declarations
	int choice;

	// Code
	do
	{
		printf("\n\n");
		printf("Menu\n\n");
		printf("0. Exit\n");
		printf("1. CreateList\n");
		printf("2. InsertAtBegining\n");
		printf("3. InsertAtEnd\n");
		printf("4. InsertAtPosition\n");
		printf("5. RemoveFromBegining\n");
		printf("6. RemoveFromEnd\n");
		printf("7. RemoveFromPosition\n");
		printf("8. ShowList\n");
		printf("9. DeleteList\n");
		printf("\n\n");

		printf("Enter Your Choice : ");
		scanf("%d", & choice);

		printf("\n\n");

		switch (choice)
		{
			case EXIT:
			{

			}
			break;

			case CREATE_LIST:
			{
				gpHead = CreateList();
				if (NULL == gpHead)
				{
					printf("Failed To Create List. Exitting Now...\n\n");
					exit(0);
				}
				else
				{
					printf("Created Singly List Successfully\n\n");
				}
			}
				break;

			case DELETE_LIST:
			{
				DeleteList();
			}break;

			case INSERT_AT_BEGINNING:
			{
				int data;
				printf("Enter Data To Insert : \n\n");
				scanf("%d", &data);

				if (0 != InsertAtBeginning(gpHead, data))
				{
					printf("Insertion Of Data %d At Beginning Failed. \n\n", data);
				}
				else
				{
					printf("Data : %d Inserted At Beginning Successfully\n\n", data);
				}
			}
			break;

			case INSERT_AT_END:
			{
				int data;
				printf("Enter Data To Insert : \n\n");
				scanf("%d", &data);

				if (0 != InsertAtEnd(gpHead, data))
				{
					printf("Insertion Of Data %d At End Failed. \n\n", data);
				}
				else
				{
					printf("Data : %d Inserted At End Successfully\n\n", data);
				}
			}
			break;

			case INSERT_AT_POSITION:
			{
				int data;
				printf("Enter Data To Insert : \n\n");
				scanf("%d", &data);

				int position;
				printf("Insert At Position : \n\n");
				scanf("%d", &position);

				printf("\n\n");
				if (0 != InsertAtPostion(gpHead, position, data))
				{
					printf("Insertion Of Data %d At Position %d Failed. \n\n", data, position);
				}
				else
				{
					printf("Data : %d Inserted At Position %d Successfully\n\n", data, position);
				}
			}
			break;

			case REMOVE_FROM_BEGINNING:
			{
				if (0 != RemoveFromBeginning(gpHead))
				{
					printf("Failed To Remove Node From Beginning !!! \n\n");
				}
				else
				{
					printf("Successfully Removed Node From Beginning !!!\n\n");
				}
			}
			break;

			case REMOVE_FROM_END:
			{
				printf("REMOVE_FROM_END !!! \n\n");

				if (0 != RemoveFromEnd(gpHead))
				{
					printf("Failed To Remove Node From End !!! \n\n");
				}
				else
				{
					printf("Successfully Removed Node From End !!!\n\n");
				}
			}
			break;

			case REMOVE_FROM_POSITION:
			{
				int position;
				printf("Remove From Position : \n\n");
				scanf("%d", &position);

				if (0 != RemoveFromPosition(gpHead, position))
				{
					printf("Failed To Remove Node From Position : %d !!! \n\n", position);
				}
				else
				{
					printf("Successfully Removed Node From Position : %d !!!\n\n", position);
				}
			}
			break;

			case SHOW_DATA:
			{
				ShowList();
			}
			break;
		}
	} while (0 != choice);
	
	return 0;
}

NODE *CreateList(void)
{
	// Variable Delcarations
	NODE *temp = NULL;

	// Code
	temp = (NODE *)malloc(sizeof(NODE));
	if (NULL == temp)
	{
		return NULL;
	}
	memset(temp, 0, sizeof(NODE));

	temp->data = 0;
	temp->next = NULL;

	return temp;
}

void DeleteList(void)
{
	// Variable Declarations
	NODE *temp = NULL;
	NODE *ptrNode = NULL;

	// Code
	if (NULL != gpHead)
	{
		if (NULL != gpHead->next)
		{
			temp = gpHead->next;

			while (temp->next)
			{
				ptrNode = temp;
				temp = temp->next;

				ptrNode->next = NULL;

				free(ptrNode);
				ptrNode = NULL;
			}
		}

		free(gpHead);
		gpHead = NULL;
		printf("List Deleted Successfully\n\n");
	}
}

int InsertAtBeginning(NODE *head, int data)
{
	// Variable Declarations
	NODE *temp = NULL;

	// Code
	if (NULL == gpHead)
	{
		return -1;
	}

	temp = (NODE *)malloc(sizeof(NODE));
	if (NULL == temp)
	{
		return -1;
	}
	memset(temp, 0, sizeof(NODE));

	temp->data = data;
	temp->next = NULL;

	if (NULL == gpHead->next)
	{
		gpHead->next = temp;
	}
	else
	{
		temp->next = gpHead->next;
		gpHead->next = temp;
	}

	return 0;
}

int InsertAtEnd(NODE *head, int data)
{
	// Variable Declarations
	NODE *newNode = NULL;
	NODE *temp = NULL;

	// Code
	if (NULL == gpHead)
	{
		return -1;
	}

	newNode = (NODE *)malloc(sizeof(NODE));
	if (NULL == newNode)
	{
		return -1;
	}
	memset(newNode, 0, sizeof(NODE));

	newNode->data = data;
	newNode->next = NULL;

	if (NULL == gpHead->next)
	{
		gpHead->next = newNode;
	}
	else
	{
		temp = gpHead->next;

		while (NULL != temp->next)
		{
			temp = temp->next;
		}

		temp->next = newNode;
	}

	return 0;
}

int InsertAtPostion(NODE *head, int position, int data)
{
	// Variable Declarations
	NODE *newNode = NULL;
	NODE *temp = NULL;

	// Code
	if (NULL == gpHead)
	{
		return -1;
	}

	newNode = (NODE *)malloc(sizeof(NODE));
	if (NULL == newNode)
	{
		return -1;
	}
	memset(newNode, 0, sizeof(NODE));

	newNode->data = data;
	newNode->next = NULL;

	temp = gpHead->next;

	int i = position -1;
	while (NULL != temp->next)
	{
		i--;
		if (0 == i)
		{
			break;
		}
		else
		{
			temp = temp->next;
		}
	}

	if (0 != i)
	{
		printf("Position %d Is Not Valid For List \n\n", position);
		return -1;
	}

	newNode->next = temp->next;
	temp->next = newNode;

	return 0;
}

void ShowList(void)
{
	// Variable Declarations
	NODE *temp = NULL;

	// Code
	if (NULL == gpHead)
	{
		printf("List Is Not Created.\n\n");
		return;
	}

	if (NULL == gpHead->next)
	{
		printf("List Is Empty\n\n");
		return;
	}

	temp = gpHead->next;

	while (temp != NULL)
	{
		printf("%d->", temp->data);
		temp = temp->next;
	}
	printf("NULL\n");

	printf("\n\n");

	return;
}

int RemoveFromBeginning(NODE *head)
{
	// Variable Declarations
	NODE *temp = NULL;

	// Code
	if (NULL == gpHead)
	{
		return 0;
	}

	if (NULL == gpHead->next)
	{
		return 0;
	}

	temp = gpHead->next;

	gpHead->next = temp->next;

	temp->next = NULL;

	free(temp);
	temp = NULL;

	return 0;
}

int RemoveFromEnd(NODE *head)
{
	// Variable Declarations
	NODE *temp = NULL, *temp_back = NULL;

	// Code
	if (NULL == gpHead)
	{
		return -1;
	}

	if (NULL == gpHead->next)
	{
		return -1;
	}

	temp = gpHead->next;
	temp_back = gpHead;

	while (NULL != temp->next)
	{
		temp_back = temp;
		temp = temp->next;
	}

	temp_back->next = NULL;

	free(temp);
	temp = NULL;

	return 0;
}

int RemoveFromPosition(NODE *head, int position)
{
	// Variable Declarations
	NODE *temp_back = NULL;
	NODE *temp = NULL;
	// Code
	if (NULL == gpHead)
	{
		return -1;
	}

	temp_back = gpHead->next;

	int i = position - 1;
	while (NULL != temp_back->next)
	{
		i--;
		if (0 == i)
		{
			break;
		}
		else
		{
			temp_back = temp_back->next;
		}
	}

	if (0 != i)
	{
		printf("Position %d Is Not Valid For List \n\n", position);
		return -1;
	}

	temp = temp_back->next;

	temp_back->next = temp->next;

	temp->next = NULL;

	free(temp);

	temp = NULL;

	return 0;
}
