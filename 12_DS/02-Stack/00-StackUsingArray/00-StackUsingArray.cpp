#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 5
int stack[STACK_SIZE];

int top = -1;

int main(void)
{
	// Function Declarations
	bool IsEmpty(void);
	bool IsFull(void);
	void Push(int);
	int Pop(void);
	void PrintStack(void);

	printf("\n\n");
	printf("Pushing 10 Into Stack\n");
	Push(10);
	printf("Pushing 20 Into Stack\n");
	Push(20);
	printf("Pushing 30 Into Stack\n");
	Push(30);
	printf("Pushing 40 Into Stack\n");
	Push(40);
	printf("Pushing 50 Into Stack\n");
	Push(50);
	printf("Pushing 60 Into Stack\n");
	Push(60);

	printf("\n\n");
	PrintStack();

	int popped_element = 0;
	popped_element = Pop();
	popped_element = Pop();
	popped_element = Pop();
	popped_element = Pop();
	popped_element = Pop();
	popped_element = Pop();

	return 0;
}

void Push(int value)
{
	// Function Declarations
	bool IsFull();

	// Code
	if (IsFull())
	{
		printf("Stack Is Full !!! OverFlow...\n");
		return;
	}

	top = top + 1;
	stack[top] = value;

	return;
}

int Pop(void)
{
	// Function Declarations
	bool IsEmpty();

	// Code
	if (IsEmpty())
	{
		printf("Stack Is Empty !!! UnderFlow...\n");
		return -1;
	}

	int value = stack[top];
	top = top - 1;

	return value;
}

void PrintStack(void)
{
	// Function Declarations
	bool IsEmpty();

	if (IsEmpty())
	{
		printf("Stack Is Empty !!!!\n");
		return;
	}

	printf("---------------STACK-----------------------\n");
	for (int i = 0; i < STACK_SIZE; i++)
	{
		printf("Postion: %d Value: %d\n", i, stack[i]);
	}
	printf("-------------------------------------------\n");

	return;
}

bool IsEmpty(void)
{
	// Code
	if (-1 == top)
	{
		return true;
	}

	return false;
}

bool IsFull(void)
{
	if (top + 1 == STACK_SIZE)
	{
		return true;
	}

	return false;
}
