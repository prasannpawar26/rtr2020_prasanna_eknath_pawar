#include <stdio.h>
#include <stdlib.h>

typedef struct _NODE
{
	int data;
	struct _NODE* next;
}NODE;

typedef struct _STACK
{
	NODE* top;
}STACK;

STACK* Stack = NULL;

int main(void)
{
	// Function Declarations
	void Menu(void);
	int IsStackCreated(void);
	int IsEmptyStack(void);
	int CreateStack(void);
	int Push(int);
	int Pop(void);
	int DeleteStack(void);
	int DisplayStack(void);

	// Variable Declarations
	int choice;

	while (true)
	{
		Menu();

		printf("\n\n");

		printf("Enter Choice \n");
		scanf("%d", &choice);

		printf("\n\n");

		switch (choice)
		{
		case 0:
			exit(0);
			break;

		case 1:
			CreateStack();
			break;

		case 2:
			DeleteStack();
			break;

		case 3:
		{
			int data;

			printf("\n\n");

			printf("Enter Data To Push: \n");
			scanf("%d", &data);

			printf("\n\n");

			Push(data);
		}
			break;

		case 4:
		{
			int data;

			data = Pop();
		}
			break;

		case 5:
			DisplayStack();
			break;
		}
	}
}

void Menu(void)
{
	printf("\n Stack Operations\n\n");
	printf("\t0. Exit\n");
	printf("\t1. Create Stack\n");
	printf("\t2. Delete Stack\n");
	printf("\t3. Push\n");
	printf("\t4. Pop\n");
	printf("\t5. Display Stack\n");
	printf("\n\n");

	return;
}

int IsStackCreated(void)
{
	if (NULL == Stack)
	{
		return 0;
	}

	return 1;
}

int IsEmptyStack(void)
{
	// Code
	if (0 == IsStackCreated())
	{
		return 1;
	}

	if (NULL == Stack->top)
	{
		return 1;
	}

	return 0;
}

int CreateStack(void)
{
	// Code
	if (1 == IsStackCreated())
	{
		printf("Stack Is Already Created \n");
		return -1;
	}

	Stack = (STACK*)malloc(sizeof(STACK));
	if (NULL == Stack)
	{
		return -2;
	}

	Stack->top = NULL;

	printf("Stack Created Successfully\n\n");

	return 0;
}

int Push(int data)
{
	// Variable Declarations
	NODE* new_node;

	if (0 == IsStackCreated())
	{
		printf("Stack Is Not Created \n");
		return -1;
	}

	new_node = (NODE*)malloc(sizeof(NODE));
	if (NULL == new_node)
	{
		printf("Node Allocation Failed\n");
		return -2;
	}

	new_node->data = data;
	new_node->next = NULL;

	new_node->next = Stack->top;
	Stack->top = new_node;

	printf("Pushed Element : %d \n", Stack->top->data);

	return 0;
}

int Pop(void)
{
	// Variable Declartations
	NODE* temp;
	int data;

	if (0 == IsStackCreated())
	{
		printf("Stack Is Not Created\n");
		return -1;
	}

	if (1 == IsEmptyStack())
	{
		printf("Stack Is Empty\n");
		return -2;
	}

	temp = Stack->top;
	data = temp->data;
	Stack->top = Stack->top->next;

	free(temp);

	printf("Popped Element : %d\n", data);

	return data;
}

int DeleteStack(void)
{
	// Variable Declarations
	NODE* temp;

	// Code
	if (0 == IsStackCreated())
	{
		printf("Stack Is Not Created\n");
		return -1;
	}

	if (0 == IsEmptyStack())
	{
		while (NULL != Stack->top)
		{
			temp = Stack->top;
			Stack->top = Stack->top->next;
			free(temp);
			temp = NULL;
		}
	}

	free(Stack);
	Stack = NULL;

	printf("Stack Deleted Successfully \n\n");

	return 0;
}

int DisplayStack(void)
{
	// Variable Declarations
	NODE* temp;

	if (0 == IsStackCreated())
	{
		printf("Stack Is Not Created\n");
		return -1;
	}

	if (1 == IsEmptyStack())
	{
		printf("Stack Is Empty \n");
		return -2;
	}

	temp = Stack->top;
	printf("\t");

	while (NULL != temp)
	{
		printf("%d <-", temp->data);
		temp = temp->next;
	}
	printf("NULL\n");

	return 0;
}