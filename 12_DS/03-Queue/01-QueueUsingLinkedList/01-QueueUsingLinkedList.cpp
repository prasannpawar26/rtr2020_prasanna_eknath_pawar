#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <malloc.h>

typedef struct _NODE
{
	int data;
	struct _NODE* next;
}NODE;

typedef struct _QUEUE
{
	NODE *front;
	NODE *rear;
}QUEUE;

QUEUE* gQueue = NULL;

int main(void)
{
	// Function Delcarations
	void Menu(void);
	int IsQueueCreated(void);
	int CreateQueue(void);
	int IsEmptyQueue(void);
	int EnQueue(int);
	int DeQueue(int*);
	int DeleteQueue(void);
	void DisplayQueue(void);

	// Variable Delcarations
	int choice;

	while (true)
	{
		Menu();

		printf("Enter Choice\n");
		scanf("%d", &choice);

		switch (choice)
		{
			case 0:
			{
				exit(0);
			}
			break;

			case 1:
			{
				CreateQueue();
			}
			break;

			case 2:
			{
				DeleteQueue();
			}
			break;

			case 3:
			{
				int data;

				printf("Enter Data To Enqueue\n\n");
				scanf("%d", &data);

				EnQueue(data);
			}
			break;

			case 4:
			{
				int data;

				DeQueue(&data);
			}
			break;

			case 5:
			{
				DisplayQueue();
			}
			break;
		}
	}
}

void Menu(void)
{
	printf("\n\n");
	printf("0. Exit\n");
	printf("1. Create Queue\n");
	printf("2. Delete Queue\n");
	printf("3. EnQueue\n");
	printf("4. DeQueue\n");
	printf("5. DisplayQueue\n");
	printf("\n\n");

	return;
}

int CreateQueue(void)
{
	// Function Declarations
	int IsQueueCreated(void);

	// Variable Declrations
	QUEUE* temp_queue = NULL;

	if (1 == IsQueueCreated())
	{
		printf("Queue Is Already Created\n\n");
		return 0;
	}

	gQueue = (QUEUE*)malloc(sizeof(QUEUE));
	if (NULL == gQueue)
	{
		printf("Malloc Failed\n");
		return -1;
	}

	gQueue->front = gQueue->rear = NULL;

	printf("Queue Created Successfully\n\n");

	return 0;
}

int IsQueueCreated(void)
{
	if (NULL == gQueue)
	{
		return 0;
	}

	return 1;
}

int IsEmptyQueue(void)
{
	if (NULL == gQueue->front)
	{
		return 1;
	}

	return 0;
}

int EnQueue(int data)
{
	// Function Declarations
	int IsQueueCreated(void);

	// Variable Declarations
	NODE* new_node = NULL;

	if (0 == IsQueueCreated())
	{
		printf("Queue Is Not Created\n\n");
		return -1;
	}

	// Code
	new_node = (NODE*)malloc(sizeof(NODE));
	if (NULL == new_node)
	{
		printf("Malloc Failed\n\n");

		return -2;
	}

	new_node->data = data;
	new_node->next = NULL;

	if (NULL != gQueue->rear)
	{
		gQueue->rear->next = new_node;
	}

	gQueue->rear = new_node;


	if (NULL == gQueue->front)
	{
		gQueue->front = gQueue->rear;
	}

	printf("Enqueue Element Successfully\n\n");

	return 0;
}

int DeQueue(int* data)
{
	// Function Declarations
	int IsQueueCreated(void);
	int IsEmptyQueue(void);

	// Variable Declarations
	NODE* temp = NULL;

	if (0 == IsQueueCreated())
	{
		printf("Queue Is Not Created \n\n");

		return -1;
	}

	if (1 == IsEmptyQueue())
	{
		printf("Queue Is Empty\n\n");
		return 0;
	}

	temp = gQueue->front;
	*data = gQueue->front->data;

	gQueue->front = gQueue->front->next;

	free(temp);
	temp = NULL;

	printf("Data Dequeued : %d\n", *data);

	return 0;
}

int DeleteQueue(void)
{
	// Function Declarations
	int IsQueueCreated(void);
	int IsEmptyQueue(void);

	// Variable Declarations
	NODE* temp = NULL;

	if (0 == IsQueueCreated())
	{
		printf("Queue Is Not Created\n\n");
		return -1;
	}

	if (1 == IsEmptyQueue())
	{
		printf("Queue Is Already Empty\n\n");
	}
	else
	{
		while (NULL != gQueue->front)
		{
			temp = gQueue->front;
			gQueue->front = gQueue->front->next;
			free(temp);
			temp = NULL;
		}
	}

	free(gQueue);

	gQueue = NULL;

	return 0;
}

void DisplayQueue(void)
{
	// Function Declarations
	int IsQueueCreated(void);
	int IsEmptyQueue(void);

	// Variable Declarations
	NODE* temp = NULL;

	if (0 == IsQueueCreated())
	{
		printf("Queue Is Not Created\n\n");
		return;
	}

	if (1 == IsEmptyQueue())
	{
		printf("Queue Is Already Empty\n\n");
		return;
	}

	temp = gQueue->front;
	while (NULL != temp)
	{
		printf("%d <- ", temp->data);
		temp = temp->next;
	}
	printf("NULL\n");

	return;
}

