#include <stdio.h>
#include <stdlib.h>

int A[5] = { 12, 34, 45, 56, 67};

int main(void)
{
	// Function Declarations
	int LinearSearch(int [], int, int);

	// Variable Declarations
	int size = sizeof(A) / sizeof(A[0]);
	int index = 0;

	index = LinearSearch(A, size, 56);
	if (index != -1)
	{
		printf("Number %d Found At Index %d Using LinearSearch Unordered \n", 56, index);
	}
	else
	{
		printf("Number Not Found\n");
	}

	return 0;
}

int LinearSearch(int iArray[], int iSize, int data_to_search)
{
	int index = 0;

	for (index = 0; index < iSize; index++)
	{
		if (iArray[index] == data_to_search)
		{
			return index;
		}
	}

	return -1;
}


