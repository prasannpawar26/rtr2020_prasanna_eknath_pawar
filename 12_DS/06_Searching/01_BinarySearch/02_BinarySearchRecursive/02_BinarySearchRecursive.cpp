#include <stdio.h>
#include <stdlib.h>

int A[5] = { 12, 34, 45, 56, 67};

int main(void)
{
	// Function Declarations
	int BinarySearch(int [], int, int, int);

	// Variable Declarations
	int size = sizeof(A) / sizeof(A[0]);
	int index = 0;

	int low = 0;
	int high = size;
	int data_to_search = 56;

	index = BinarySearch(A, low, high, data_to_search);
	if (index != -1)
	{
		printf("Number %d Found At Index %d Using Binary Iterative Search\n", 56, index);
	}
	else
	{
		printf("Number Not Found\n");
	}

	return 0;
}

int BinarySearch(int iArray[], int low, int high, int data_to_search)
{
	int mid = low + ((high - low) / 2);

	if (low > high)
	{
		return -1;
	}

	if (iArray[mid] == data_to_search)
	{
		return mid;
	}
	else if (iArray[mid] < data_to_search)
	{
		return BinarySearch(iArray, mid + 1, high, data_to_search);
	}
	else
	{
		return BinarySearch(iArray, low, mid - 1, data_to_search);
	}

	return -1;
}


