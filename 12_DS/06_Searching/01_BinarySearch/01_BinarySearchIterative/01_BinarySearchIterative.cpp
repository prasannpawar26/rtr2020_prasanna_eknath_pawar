#include <stdio.h>
#include <stdlib.h>

int A[5] = { 12, 34, 45, 56, 67};

int main(void)
{
	// Function Declarations
	int BinarySearch(int [], int, int);

	// Variable Declarations
	int size = sizeof(A) / sizeof(A[0]);
	int index = 0;

	index = BinarySearch(A, size, 56);
	if (index != -1)
	{
		printf("Number %d Found At Index %d Using Binary Iterative Search\n", 56, index);
	}
	else
	{
		printf("Number Not Found\n");
	}

	return 0;
}

int BinarySearch(int iArray[], int iSize, int data_to_search)
{
	int iLow = 0;
	int iHigh = iSize - 1;

	while (iLow < iHigh)
	{
		int iMid = iLow + ((iHigh - iLow) / 2);

		if (iArray[iMid] == data_to_search)
		{
			return iMid;
		}
		else if(iArray[iMid] < data_to_search)
		{
			iLow = iMid + 1;
		}
		else
		{
			iHigh = iMid - 1;
		}
	}

	return -1;
}


