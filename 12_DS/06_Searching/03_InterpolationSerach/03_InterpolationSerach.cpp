#include <stdio.h>
#include <stdlib.h>

int A[5] = { 12, 34, 45, 56, 67};

int main(void)
{
	// Function Declarations
	int InterpolationSerach(int [], int, int);

	// Variable Declarations
	int size = sizeof(A) / sizeof(A[0]);
	int index = 0;

	index = InterpolationSerach(A, size, 56);
	if (index != -1)
	{
		printf("Number %d Found At Index %d Using Interpolation Serach \n", 56, index);
	}
	else
	{
		printf("Number Not Found\n");
	}

	return 0;
}

int InterpolationSerach(int iArray[], int iSize, int data_to_search)
{
	int low;
	int high;
	int position = 0;

	high = iSize - 1;
	low = 0;

	while (low <= high && data_to_search >= iArray[low] && data_to_search <=iArray[high])
	{
		if (low == high)
		{
			if (iArray[low] == data_to_search)
			{
				return low;
			}
			else
			{
				return -1;
			}
		}

		position = low + ((double)((data_to_search - iArray[low]) / (iArray[high] - iArray[low]) * (high - low)));

		if (iArray[position] == data_to_search)
		{
			return position;
		}

		if (iArray[position] < data_to_search)
		{
			low = position + 1;
		}
		else
		{
			high = position - 1;
		}
	}

	return -1;
}


