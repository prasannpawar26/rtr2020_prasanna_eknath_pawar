// ListEntry.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define PEP_MAX_LEN 255

/*
offset_of :
    1. NULL Is 0
    2. NULl(0) Is Considered As Base Addressed => null pointer
    3. We Are Type Casting NULL I.e 0 By TYPE strcture pointer
    4. Then Access Its Member By Using Arrow (->) Operator
    5. And Finally Accessing Address Of That Memory

*/
#define offset_of(type, member) ((unsigned int)&((type*)NULL)->member)


/*
container_of:
    1. address is pointer to one of the link field of the  TYPE structure.
    2. perfrom address - offset of the member of the TYPE structure = to get the base address of the TYPE strcture which contains the member
        a. to reduce the scale factor to one we are type casting the address to char *
        b. getting the offset of the member within the TYPE strcture
        c. perfrom subtaction of (char * type casted address) - offset of member
        d. result of c step will be the base address of the TYPE strcture
    
*/
#define container_of(address, type, member) ((type *)((char *)(address) - offset_of(type, member)))

typedef struct _LIST_ENTRY_
{
    struct _LIST_ENTRY_* next;
    struct _LIST_ENTRY_* prev;
}LIST_ENTRY, *PLIST_ENTRY;

typedef struct _BOOK_
{
    char name[10];
    char author[15];
    char publisher[20];
    float price;
    LIST_ENTRY list_entry;
}BOOK;

int main()
{
    printf("Offset Of 'name' : %u\n", offset_of(BOOK, name));
    printf("Offset Of 'author' : %u\n", offset_of(BOOK, author));
    printf("Offset Of 'publisher' : %u\n", offset_of(BOOK, publisher));
    printf("Offset Of 'price' : %u\n", offset_of(BOOK, price));
    printf("Offset Of 'list_entry' : %u\n", offset_of(BOOK, list_entry));

    printf("\n\n");

    BOOK book = { "AA", "BB", "CC", 4.5f, {NULL, NULL}};
    BOOK* p = &book;

    PLIST_ENTRY temp = &p->list_entry;

    BOOK* tempP = (BOOK*)((char *)(&p->list_entry) - offset_of(BOOK, list_entry));
    printf("Name : %s\n", tempP->name);
    printf("author : %s\n", tempP->author);
    printf("publisher : %s\n", tempP->publisher);
    printf("price : %f\n", tempP->price);

    printf("\n\n");
    BOOK* tempPP = container_of(temp, BOOK, list_entry);
    printf("Name : %s\n", tempPP->name);
    printf("author : %s\n", tempPP->author);
    printf("publisher : %s\n", tempPP->publisher);
    printf("price : %f\n", tempPP->price);
  

    printf("\n\n");

    return 0;
}
