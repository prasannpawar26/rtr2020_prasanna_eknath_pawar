#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function Declarations
	void PrintArray(int *, int);
	void BubbleSort(int *, int);

	// Variable Declarations
	int iArray[] = {34, 4, 12, 1, 2};
	int no_of_element = sizeof(iArray) / sizeof(int);

	// Code
	printf("Before Sorting\n\n");
	PrintArray(iArray, no_of_element);

	BubbleSort(iArray, no_of_element);

	printf("After Sorting\n\n");
	PrintArray(iArray, no_of_element);

	return 0;
}

void BubbleSort(int *iArray, int  elements)
{
	// Variable Declartations
	int pass = 0;
	int i = 0;
	int temp = 0;

	for (pass = elements - 1; pass >= 0; pass--)
	{
		for (i = 0; i < pass; i++)
		{
			if (iArray[i] > iArray[i + 1]) // Bigger Element At The End Of The Array
			{
				temp = iArray[i];
				iArray[i] = iArray[i + 1];
				iArray[i + 1] = temp;
			}
		}
	}
}

void PrintArray(int *iArray, int elements)
{
	for (int i = 0; i < elements; i++)
	{
		printf("\t A[%d] = %d\n", i, iArray[i]);
	}

	return;
}
