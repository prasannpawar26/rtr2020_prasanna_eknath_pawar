#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function Delcarations
	void MergeSort(int[], int, int);
	void PrintArray(int[], int);

	// Variable Declarations
	int A[] = { 12, 5, 67,34, 1 };
	int array_size = sizeof(A) / sizeof(A[0]);

	// Code
	printf("Before Sorting : \n");
	PrintArray(A, array_size);

	MergeSort(A, 0, array_size -1);

	printf("After Sorting : \n");
	PrintArray(A, array_size);

	return 0;
}

void MergeSort(int A[], int low, int high)
{
	// Function Declarations
	void Merge(int[], int, int, int);

	// Code
	if (low >= high)
	{
		return;
	}

	int mid = low + ((high - low) / 2);

	MergeSort(A, low, mid);
	MergeSort(A, mid + 1, high);

	Merge(A, low, mid, high);

	return;
}

void Merge(int A[], int low, int mid, int high)
{
	// Variable
	int i, j, k;

	// Number Of Elements In Divided Arries
	int n1 = mid - low + 1;
	int n2 = high - mid;

	int *lArray = (int *)malloc(sizeof(int) * n1);
	int *rArray = (int *)malloc(sizeof(int) * n2);

	// Copy Data To Temp Array
	for (i = 0; i < n1; i++)
	{
		lArray[i] = A[low + i];
	}

	for (j = 0; j < n2; j++)
	{
		rArray[j] = A[mid + 1 + j];
	}

	i = 0;
	j = 0;
	k = low;

	while (i < n1 && j < n2)
	{
		if (lArray[i] <= rArray[j])
		{
			A[k] = lArray[i];
			i++;
		}
		else
		{
			A[k] = rArray[j];
			j++;
		}

		k++;
	}

	// Copy Remaining Of Left Array
	while (i < n1)
	{
		A[k] = lArray[i];
		i++;
		k++;
	}

	// Copy Remaining Of Right Array
	while (j < n2)
	{
		A[k] = rArray[j];
		j++;
		k++;
	}

	free(lArray);
	free(rArray);

	return;
}

void PrintArray(int A[], int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("\tA[%d] = %d\n", i, A[i]);
	}

	return;
}
