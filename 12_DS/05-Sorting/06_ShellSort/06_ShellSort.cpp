#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function Declarations
	void PrintArray(int *, int);
	void ShellSort(int *, int);

	// Variable Declarations
	int iArray[] = {34, 4, 12, 1, 2};
	int no_of_element = sizeof(iArray) / sizeof(int);

	// Code
	printf("Before Sorting\n\n");
	PrintArray(iArray, no_of_element);

	ShellSort(iArray, no_of_element);

	printf("After Sorting\n\n");
	PrintArray(iArray, no_of_element);

	return 0;
}

void ShellSort(int *iArray, int  elements)
{
	// Variable Declartations
	int i = 0;
	int j = 0;
	int temp = 0;
	int interval = 0;

	for (interval = elements / 2; interval > 0; interval /= 2)
	{
		for (i = interval; i < elements; i++)
		{
			temp = iArray[i];
			j = i;
			while (j >= interval && iArray[j - interval] > temp)
			{
				iArray[j] = iArray[j - interval];
				j -= interval;
			}

			iArray[j] = temp;
		}
	}

	return;
}

void PrintArray(int *iArray, int elements)
{
	for (int i = 0; i < elements; i++)
	{
		printf("\t A[%d] = %d\n", i, iArray[i]);
	}

	return;
}
