#include <stdio.h>
#include <stdlib.h>

int A[5] = { 34, 6, 3, 78, 56 };

int main(void)
{
	// Function Declarations
	void QuickSort(int [], int, int);

	printf("Before Sorting : \n");

	for (int i = 0; i < 5; i++)
	{
		printf("\t A[%d] = %d\n", i, A[i]);
	}

	QuickSort(A, 0, 4);

	printf("After Sorting : \n");

	for (int i = 0; i < 5; i++)
	{
		printf("\t A[%d] = %d\n", i, A[i]);
	}

	return 0;
}

void QuickSort(int A[], int low, int high)
{
	// Function Declarations
	int Partition(int[], int, int);

	// Variable Declarations
	int pivot;

	if (high > low)
	{
		pivot = Partition(A, low, high);
		QuickSort(A, 0, pivot - 1);
		QuickSort(A, pivot, high);
	}

	return;
}

//
// Put All Smaller Elements Before Pivot Element
//
int Partition(int A[], int low, int high)
{
	// Function Declarations
	void Swap(int*, int*);

	// Variable Declarations
	int smaller_index;
	int pivot_item = A[high];

	// Code
	smaller_index = low - 1;

	for (int j = low; j < high; j++)
	{
		if (A[j] <= pivot_item)
		{
			smaller_index++;
			Swap(&A[smaller_index], &A[j]);
		}
	}

	// All Smaller Elements Than Pivot Item Are Placed Till Smaller Index
	// Hence Pivot Item Placed At Smaller Index + 1
	Swap(&A[smaller_index + 1], &A[high]);

	//reuturning index of pivot element
	return (smaller_index + 1);
}

void Swap(int *a, int *b)
{
	int t = *a;
	*a = *b;
	*b = t;

	return;
}
