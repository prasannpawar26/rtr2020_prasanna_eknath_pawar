#include <stdio.h>
#include <stdlib.h>

int main(void) {
  // Function Declarations
  void PrintArray(int[], int);
  void HeapSort(int[], int);

  int arr[] = {12, 5, 7, 18, 4, 1, 11, 12};
  int n = sizeof(arr) / sizeof(arr[0]);

  printf("Array Before HeapSort: \n");
  PrintArray(arr, n);

  HeapSort(arr, n);

  printf("Array After HeapSort : \n");
  PrintArray(arr, n);

  return 0;
}

void HeapSort(int array[], int array_size) {

  // Function Declarations
  void Heapify(int[], int, int);
  void Swap(int *, int *);

  // Code

  // Build The Heap -> Large Number At The Root
  for (int i = array_size - 1; i >= 0; i--) {
     Heapify(array, array_size, i);
  }

  // Move/Swap One By One Large Element From Heap With End Of Array
  for (int i = array_size - 1; i >= 0; i--) {

    Swap(&array[0], &array[i]);

    // Max Heapify Again With Excluding Last Element , Because Last Element Is
    // Already At Correct Position
    Heapify(array, i, 0);
  }

  return;
}

void Swap(int *a, int *b) {
  // Code
  int temp = *a;
  *a = *b;
  *b = temp;

  return;
}

void Heapify(int array[], int array_size, int node_index) {
  // Function Declarations
  void Swap(int *, int *);

  // Variable Declarations
  int largest_node;
  int left_of_node;
  int right_of_node;

  // Code
  largest_node = node_index;
  left_of_node = 2 * node_index + 1;
  right_of_node = 2 * node_index + 2;

  if (left_of_node < array_size && array[largest_node] < array[left_of_node]) {
    largest_node = left_of_node;
  }

  if (right_of_node < array_size &&
      array[largest_node] < array[right_of_node]) {
    largest_node = right_of_node;
  }

  if (largest_node != node_index) {
    Swap(&array[node_index], &array[largest_node]);

    // Recursively Heapify The Affected SubTree
    Heapify(array, array_size, largest_node);
  }

  return;
}

void PrintArray(int array[], int array_size) {
  // Code
  for (int i = 0; i < array_size; i++) {
    printf("\tArray[%d] : %d\n", i, array[i]);
  }

  return;
}