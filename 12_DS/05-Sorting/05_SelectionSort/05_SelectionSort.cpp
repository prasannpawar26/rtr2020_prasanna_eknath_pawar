#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function Declarations
	void PrintArray(int *, int);
	void SelectionSort(int *, int);

	// Variable Declarations
	int iArray[] = {34, 4, 12, 1, 2};
	int no_of_element = sizeof(iArray) / sizeof(int);

	// Code
	printf("Before Sorting\n\n");
	PrintArray(iArray, no_of_element);

	SelectionSort(iArray, no_of_element);

	printf("After Sorting\n\n");
	PrintArray(iArray, no_of_element);

	return 0;
}

void SelectionSort(int *iArray, int  elements)
{
	// Variable Declartations
	int i = 0;
	int j = 0;
	int temp = 0;
	int minIndex = 0;

	for (i = 0; i < elements; i++)
	{
		minIndex = i;

		for (j = i + 1; j < elements; j++) // Find The Min Element In Each Pass
		{
			if (iArray[minIndex] > iArray[j])
			{
				minIndex = j;
			}
		}

		temp = iArray[minIndex];
		iArray[minIndex] = iArray[i];
		iArray[i] = temp; // Place Min Element At Current Pass Index
	}

	return;
}

void PrintArray(int *iArray, int elements)
{
	for (int i = 0; i < elements; i++)
	{
		printf("\t A[%d] = %d\n", i, iArray[i]);
	}

	return;
}
