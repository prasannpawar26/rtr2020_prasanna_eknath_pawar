#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function Declarations
	void PrintArray(int *, int);
	void InsertionSort(int *, int);

	// Variable Declarations
	int iArray[] = {34, 4, 12, 1, 2};
	int no_of_element = sizeof(iArray) / sizeof(int);

	// Code
	printf("Before Sorting\n\n");
	PrintArray(iArray, no_of_element);

	InsertionSort(iArray, no_of_element);

	printf("After Sorting\n\n");
	PrintArray(iArray, no_of_element);

	return 0;
}

void InsertionSort(int *iArray, int  elements)
{
	// Variable Declartations
	int i = 0;
	int j = 0;
	int temp = 0;

	// Code
	for (i = 0; i < elements; i++)
	{
		temp = iArray[i];
		j = i;

		while (iArray[j - 1] > temp && j >= 1)
		{
			iArray[j] = iArray[j - 1];
			j--;
		}
		iArray[j] = temp;
	}
}

void PrintArray(int *iArray, int elements)
{
	for (int i = 0; i < elements; i++)
	{
		printf("\t A[%d] = %d\n", i, iArray[i]);
	}

	return;
}
