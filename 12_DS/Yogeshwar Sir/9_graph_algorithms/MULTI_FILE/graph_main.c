#include <stdio.h> 
#include <stdlib.h> 
#include "graph.h" 

int main (void) 
{
	result_t ret; 
	vertex_t cnt; 
	static edge_t E[] = { {0, 1}, {0, 5}, {1, 2}, {1, 5}, {2, 3}, {2, 4}, {3, 4}, {4, 5} }; 
	graph_t *g = create_graph (); 
	
	for (cnt=0; cnt < 6; ++cnt)
		if ((ret = add_vertex (g)) != SUCCESS)
		{
			fprintf (stderr, "Error in adding vertex to a graph\n"); 
			exit (EXIT_FAILURE); 
		}

	for (cnt=0; cnt < sizeof (E) / sizeof (edge_t); ++cnt)
	{
		if ((ret = add_edge (g, E[cnt].start, E[cnt].end)) != SUCCESS)
		{
			switch (ret)
			{
				case INVALID_EDGE: 
						fprintf (stderr, "Error in adding edge %d to %d\n", 
								 E[cnt].start, E[cnt].end); 
						break; 
				case NODE_NOT_FOUND: 
						fprintf (stderr, "One of the edge nodes not found\n"); 
						break; 
			}
			exit (EXIT_FAILURE); 
		}
	}

	print_graph (g); 
	dfs(g); 
	printf ("nr_vertices:%d\n", get_nr_vertices (g)); 
	printf ("nr_edges:%d\n", get_nr_edges (g)); 

	if (remove_edge (g, 0, 2) != SUCCESS)
	{
		fprintf (stderr, "remove_edge:Error\n"); 
	}
	else
	{
		fprintf (stderr, "remove_edge:Unexpected error\n"); 
		exit (EXIT_FAILURE); 
	}

	if (remove_edge (g, 1, 5) != SUCCESS)
	{
		fprintf (stderr, "remove_edge:Error\n"); 
		exit (EXIT_FAILURE); 
	}
	printf ("nr_vertices:%d\n", get_nr_vertices (g)); 
	printf ("nr_edges:%d\n", get_nr_edges (g)); 


	print_graph (g); 

	if((ret = remove_vertex(g, 2)) != SUCCESS)
	{
		fprintf(stderr, "remove_vertex : Error\n"); 
		exit(EXIT_FAILURE); 
	}

	printf("Printing graph after removal of vertex 2\n"); 
	print_graph(g); 
	printf ("nr_vertices:%d\n", get_nr_vertices (g)); 
	printf ("nr_edges:%d\n", get_nr_edges (g)); 

	ret = destroy (&g); 

	if (ret == SUCCESS && !g) 
		fprintf (stderr, "Graph destroyed successfully\n"); 
	else
		exit (EXIT_FAILURE); 

	exit (EXIT_SUCCESS); 
}
