#ifndef _GRAPH_H 
#define _GRAPH_H 

#define TRUE 			 1
#define FALSE 			 0 
#define SUCCESS			 1
#define FAILURE			 0 
#define NODE_NOT_FOUND 	-1
#define INVALID_EDGE	-2 

typedef int vertex_t; 
typedef int result_t; 
typedef int len_t; 
typedef enum {WHITE, GREY, BLACK} colour_t; 

typedef struct adj_node 
{
	vertex_t vertex; 
	struct adj_node *prev;
   	struct adj_node	*next; 
}adj_node_t; 

typedef struct vertex_node 
{
	vertex_t vertex; 
	vertex_t pred_vertex; 
	colour_t colour; 
	struct adj_node *head; 
	struct vertex_node *prev; 
    struct vertex_node *next; 
}vertex_node_t; 

typedef struct edge
{
	vertex_t start, end; 
}edge_t; 

typedef struct graph 
{
	struct vertex_node *head; 
	int nr_vertices; 
	int nr_edges; 
}graph_t; 

/* Graph interface routines */ 
graph_t 	*create_graph 	(void); 
result_t 	add_vertex 	  	(graph_t *g); 
result_t 	add_edge 	  	(graph_t *g, vertex_t v1, vertex_t v2); 
result_t	remove_edge	  	(graph_t *g, vertex_t v1, vertex_t v2); 
result_t 	remove_vertex 	(graph_t *g, vertex_t v); 
int			get_nr_vertices (graph_t *g); 
int 		get_nr_edges 	(graph_t *g); 
len_t		degree			(graph_t *g, vertex_t vertex); 
void 		print_graph   	(graph_t *g); 
void		dfs				(graph_t *g); 
result_t 	destroy		  	(graph_t **pp_g); 

/* Graph auxillary routines */ 
int 			next_vertex_number	  (void); 
result_t		is_valid_vertex		  (graph_t *g, vertex_t v); 
adj_node_t 		*get_adj_node 		  (vertex_t vertex); 
adj_node_t	 	*search_node_adj_list (adj_node_t *head, vertex_t vertex);
vertex_node_t 	*get_vertex_node 	  (vertex_t vertex); 
vertex_node_t	*search_node_vlist	  (vertex_node_t *head, vertex_t vertex); 
void			g_insert_vlist		  (vertex_node_t *beg, vertex_node_t *mid, vertex_node_t *end); 
void			g_insert_adj_list	  (adj_node_t *beg, adj_node_t *mid, adj_node_t *end); 
void			g_delete_adj_node	  (adj_node_t *node); 
void			g_delete_vertex_node  (vertex_node_t *node); 
result_t		destroy_vertex_list	  (vertex_node_t *head); 
result_t		destroy_adj_list	  (adj_node_t *head); 
len_t			length_adj_list		  (adj_node_t *head); 
void			dfs_visit			  (graph_t *g, vertex_t u); 	
/* auxillary routines */ 
void *xcalloc (size_t, size_t); 

#endif /* _GRAPH_H */ 
