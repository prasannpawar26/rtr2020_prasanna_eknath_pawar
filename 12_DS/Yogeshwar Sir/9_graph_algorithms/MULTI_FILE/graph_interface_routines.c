#include <stdio.h> 
#include <stdlib.h> 
#include "graph.h" 

graph_t *create_graph (void)
{
	graph_t *g = (graph_t*) xcalloc (1, sizeof (graph_t)); 
	
	g->head 		= (vertex_node_t*) xcalloc (1, sizeof (vertex_node_t)); 
	g->nr_vertices	= 0; 
	g->nr_edges		= 0; 

	g->head->head   	 = NULL; 
	g->head->vertex 	 = -1;  
	g->head->pred_vertex = -1; 
	
	g->head->next = g->head; 
	g->head->prev = g->head; 

	return (g); 
}

result_t add_vertex (graph_t *g)
{
	g_insert_vlist (g->head->prev, get_vertex_node (next_vertex_number ()), g->head); 
	++g->nr_vertices; 

	return (SUCCESS); 
}

result_t add_edge (graph_t *g, vertex_t v1, vertex_t v2) 
{
	vertex_node_t *n_v1, *n_v2; 
	
	if (v1 == v2)
		return (INVALID_EDGE); 

	n_v1  = search_node_vlist (g->head, v1); 
	n_v2  = search_node_vlist (g->head, v2); 
	
	if (!n_v1 || !n_v2)
		return (NODE_NOT_FOUND); 
	
	g_insert_adj_list (n_v1->head->prev, get_adj_node (v2), n_v1->head); 
	g_insert_adj_list (n_v2->head->prev, get_adj_node (v1), n_v2->head); 
	++g->nr_edges; 

	return (SUCCESS); 
}

result_t remove_edge (graph_t *g, vertex_t start, vertex_t end)
{
	vertex_node_t *node_start = search_node_vlist (g->head, start); 
	vertex_node_t *node_end   = search_node_vlist (g->head, end); 
	adj_node_t *adj_start, *adj_end; 

	if (!node_start || !node_end)
		return (NODE_NOT_FOUND);

	adj_start = search_node_adj_list (node_end->head, start); 
	adj_end   = search_node_adj_list (node_start->head, end); 

	if (!adj_start || !adj_end)
		return (INVALID_EDGE); 

	g_delete_adj_node (adj_start); 
	g_delete_adj_node (adj_end); 
	--g->nr_edges; 

	return (SUCCESS); 
}

result_t remove_vertex (graph_t *g, vertex_t vertex) 
{
	vertex_node_t *v_node = search_node_vlist (g->head, vertex); 
	vertex_node_t *other_node; 
	adj_node_t *run, *self_node; 
	result_t rs; 

	if (!v_node)
		return (NODE_NOT_FOUND); 

	for(run = v_node->head->next; run != v_node->head; run = run->next)
	{
		other_node = search_node_vlist(g->head, run->vertex); 
		self_node = search_node_adj_list(other_node->head, vertex); 
		g_delete_adj_node(self_node); 
		--g->nr_edges; 
	}

	destroy_adj_list(v_node->head); 
	g_delete_vertex_node(v_node); 
	--g->nr_vertices; 

	return (SUCCESS); 
}

len_t degree (graph_t *g, vertex_t vertex)
{
	vertex_node_t *v_node = search_node_vlist (g->head, vertex); 
	if (!v_node) 
		return (NODE_NOT_FOUND); 
	return (length_adj_list (v_node->head)); 
}

int get_nr_vertices (graph_t *g)
{
	return (g->nr_vertices); 
}

int get_nr_edges (graph_t *g) 
{
	return (g->nr_edges); 
}

void print_graph (graph_t *g) 
{
	vertex_node_t *run_v; 
	adj_node_t *run_adj; 

	for (run_v = g->head->next; run_v != g->head; run_v = run_v->next)
	{
		printf ("[%d]\t<--->\t", run_v->vertex); 
		for (run_adj = run_v->head->next; run_adj != run_v->head; run_adj = run_adj->next)
			printf ("[%d]<->", run_adj->vertex); 
		printf ("[end]\n"); 
	}
}

result_t destroy (graph_t **pp_g) 
{
	result_t rs; 
	graph_t *g = *pp_g;
	vertex_node_t *run, *run_n; 

	for (run = g->head->next; run != g->head; run = run_n)
	{
		run_n = run->next; 
		rs = destroy_adj_list (run->head); 
		if (rs != SUCCESS)
		{
			fprintf (stderr, "destroy:Error\n"); 
			exit (EXIT_FAILURE); 
		}
		free (run); 
	}
	
	free (g->head); 
	free (g); 
	*pp_g = NULL; 
	return (SUCCESS); 
}

void dfs(graph_t *g)
{
	vertex_node_t *run; 
	
	for(run = g->head->next; run != g->head; run = run->next)
	{
		if(run->colour == WHITE)
			dfs_visit(g, run->vertex); 
	}
}
