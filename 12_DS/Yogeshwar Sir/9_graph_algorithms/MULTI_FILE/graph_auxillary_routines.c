#include <stdio.h> 
#include <stdlib.h> 
#include "graph.h" 

static int next=-1; 

int next_vertex_number (void) 
{
	return (++next); 
}

result_t is_valid_vertex (graph_t *g, vertex_t v) 
{
	vertex_node_t *run; 
	
	for (run = g->head->next; run != g->head; run = run->next)
			if (run->vertex == v)
				return (TRUE); 

	return (FALSE); 
}

adj_node_t *get_adj_node (vertex_t vertex) 
{
	adj_node_t *node = (adj_node_t*) xcalloc (1, sizeof (adj_node_t)); 
	
	node->vertex 	  	  = vertex; 
		return (node); 
}

vertex_node_t *get_vertex_node (vertex_t vertex) 
{
	vertex_node_t *node = (vertex_node_t*) xcalloc (1, sizeof (vertex_node_t)); 
	
	node->vertex 	 = vertex; 
	node->pred_vertex 	  = -1; 
	node->colour	  	  = WHITE; 
	node->head 		 = get_adj_node (-1); 
	node->head->prev = node->head; 
	node->head->next = node->head; 

	return (node); 
}

vertex_node_t *search_node_vlist (vertex_node_t *head, vertex_t vertex) 
{
	vertex_node_t *run = head->next; 

	while (run != head)
	{
		if (run->vertex == vertex)
			return (run); 
		run = run->next; 
	}

	return (NULL); 
}

adj_node_t *search_node_adj_list (adj_node_t *head, vertex_t vertex)
{
	adj_node_t *run = head->next; 

	while (run != head)
	{
		if (run->vertex == vertex) 
			return (run); 
		run = run->next; 
	}

	return (NULL); 
}

adj_node_t *search_adj_node (graph_t *g, vertex_t v, vertex_t adj_v)
{
	vertex_node_t *v_node = search_node_vlist (g->head, v); 
	if (!v_node)
		return (NULL); 
	return (search_node_adj_list (v_node->head, adj_v)); 
}

void g_insert_vlist (vertex_node_t *beg, vertex_node_t *mid, vertex_node_t *end)
{
	mid->prev = beg; 
	mid->next = end; 
	beg->next = mid; 
	end->prev = mid; 
}

void g_insert_adj_list (adj_node_t *beg, adj_node_t *mid, adj_node_t *end) 
{
	mid->prev = beg; 
	mid->next = end; 
	beg->next = mid; 
	end->prev = mid; 
}

void g_delete_adj_node (adj_node_t *node) 
{
	adj_node_t *beg = node->prev, *end = node->next; 

	beg->next = end; 
	end->prev = beg; 

	free (node); 
}

void g_delete_vertex_node(vertex_node_t *node)
{
	node->next->prev = node->prev; 
	node->prev->next = node->next; 
	free(node); 
}

result_t destroy_vertex_list (vertex_node_t *head)
{
	vertex_node_t *run, *run_n; 

	for (run = head->next; run != head; run = run_n)
	{
		run_n = run->next; 
		free (run); 
	}

	free (head); 

	return (SUCCESS); 
}

result_t destroy_adj_list (adj_node_t *head)
{
	adj_node_t *run, *run_n; 

	for (run = head->next; run != head; run = run_n) 
	{
		run_n = run->next; 
		free (run); 
	}

	free (head); 

	return (SUCCESS); 
}

len_t length_adj_list (adj_node_t *head) 
{
	len_t len; 
	adj_node_t *run; 

	for (run = head->next, len = 0; run != head; run = run->next, ++len)
			; 

	return (len); 
}

void dfs_visit(graph_t *g, vertex_t u) 
{
	vertex_node_t *u_node = search_node_vlist(g->head, u); 
	vertex_node_t *current; 
	adj_node_t *run; 

	if(!u_node)
	{
		fprintf(stderr, "dfs_visit:unexpected error\n"); 
		exit(EXIT_FAILURE); 
	}	

	printf("[%d]\n", u_node->vertex); 
	u_node->colour = GREY; 
	
	for(run = u_node->head->next; run != u_node->head; run = run->next)
	{
		current = search_node_vlist(g->head, run->vertex); 
		if(!current)
		{
			fprintf(stderr, "dfs_visit:unexpected error\n"); 
			exit(EXIT_FAILURE); 
		}	

		current->pred_vertex = u; 
		if(current->colour == WHITE)
			dfs_visit(g, run->vertex); 
	}
	
	u_node->colour = BLACK; 
}
