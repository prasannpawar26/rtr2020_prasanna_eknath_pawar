#include <stdio.h> 
#include <stdlib.h> 

#define TRUE 	1
#define FALSE 	0 
#define SUCCESS 1
#define FAILURE 0 

#define DATA_NOT_FOUND  -1
#define LIST_EMPTY      -2

#define QUEUE_EMPTY LIST_EMPTY

#define VERTEX_NOT_FOUND 	-1
#define INVALID_EDGE 		-2 

#define uerror(fun_name) printf("%s:%d:Unexpected Error:\n", fun_name, __LINE__ )

struct dcll_node; 
struct adj_node;
struct vertex_node; 
struct edge; 

typedef struct dcll_node dcll_node_t; 
typedef dcll_node_t dcll_t; 
typedef dcll_t queue_t; 

typedef struct adj_node adj_node_t; 
typedef struct vertex_node vertex_node_t;  
typedef struct edge edge_t; 
typedef int vertex_t; 

typedef int result_t; 
typedef int len_t; 
typedef enum {WHITE, GREY, BLACK} colour_t; 

struct dcll_node
{
	vertex_node_t *node; 
	struct dcll_node *prev, *next; 
}; 

struct adj_node 
{
	vertex_t vertex; 
	double w; 
	struct adj_node *prev;
   	struct adj_node	*next; 
}; 

struct vertex_node 
{
	vertex_t vertex; 
	vertex_t pred_vertex; 
	colour_t colour; 
	struct adj_node *head; 
	struct vertex_node *prev; 
    struct vertex_node *next; 
}; 

struct edge
{
	vertex_t start, end; 
}; 

typedef struct graph 
{
	struct vertex_node *head; 
	int nr_vertices; 
	int nr_edges; 
}graph_t; 

/* Graph interface routines */ 

graph_t     *create_graph 	(void); 
result_t    add_vertex 	  	(graph_t *g); 
result_t    add_edge 	  	(graph_t *g, vertex_t v1, vertex_t v2); 
result_t    remove_edge	  	(graph_t *g, vertex_t v1, vertex_t v2); 
result_t 	remove_vertex 	(graph_t *g, vertex_t v); 
int			get_nr_vertices (graph_t *g); 
int 		get_nr_edges 	(graph_t *g); 
len_t		degree			(graph_t *g, vertex_t vertex); 
void 		print_graph   	(graph_t *g); 
void		dfs				(graph_t *g); 
result_t    bfs             (graph_t *g, vertex_t start_vertex); 
result_t 	destroy		  	(graph_t **pp_g); 

/* Graph auxillary routines */ 

static adj_node_t       *get_adj_node 		    (vertex_t vertex); 
static vertex_node_t    *get_vertex_node 	    (vertex_t vertex); 

static adj_node_t	    *search_node_adj_list   (adj_node_t *head, vertex_t vertex);
static vertex_node_t	*search_node_vlist	    (vertex_node_t *head, vertex_t vertex); 

static void			    g_insert_vlist		    (vertex_node_t *beg, vertex_node_t *mid, vertex_node_t *end); 
static void			    g_insert_adj_list	    (adj_node_t *beg, adj_node_t *mid, adj_node_t *end); 

static void			    g_delete_adj_node	    (adj_node_t *node); 
static void			    g_delete_vertex_node    (vertex_node_t *node); 

static result_t		    destroy_vertex_list	    (vertex_node_t *head); 
static result_t		    destroy_adj_list	    (adj_node_t *head); 

static int              next_vertex_number      (void); 
static result_t         is_valid_vertex         (graph_t *g, vertex_t v); 
static len_t			length_adj_list		    (adj_node_t *head); 
static void             reset                   (graph_t *g); 
static void			    dfs_visit			    (graph_t *g, vertex_t u); 	

/* Queue Interface Routines */ 
queue_t *create_queue(void); 
int		enqueue(queue_t *queue, vertex_node_t *v_node); 
int 	dequeue(queue_t *queue, vertex_node_t **pp_vnode); 
int     is_queue_empty(queue_t *queue); 
int 	destroy_queue(queue_t **pp_queue); 

/* List interface routines. [DATA = vertex_node_t*]  */  
dcll_t 	*create_list(void); 
int 	insert_end(dcll_t *list, vertex_node_t *v_node); 
int 	insert_beg(dcll_t *list, vertex_node_t *v_node); 
int 	del_beg(dcll_t *list); 
int 	del_end(dcll_t *list); 
int 	examine_and_delete_beg(dcll_t *list, vertex_node_t **pp_vnode); 
int 	examine_and_delete_end(dcll_t *list, vertex_node_t **pp_vnode); 
int 	is_empty(dcll_t *list);
int 	destroy_list(dcll_t **pp_list); 

/* List auxillary routines */ 
void 		g_insert(dcll_node_t *beg, dcll_node_t *mid, dcll_node_t *end); 
void 		g_delete(dcll_node_t *node); 
dcll_node_t *search_node(dcll_t *list, vertex_node_t *v_search_node);
dcll_node_t *get_dcll_node(vertex_node_t *v_node); 

/* auxillary routines */ 
static void *xcalloc (size_t, size_t); 

void test_traversals(void); 
void test_dijkstra(void); 
void test_bellman_ford(void); 
void test_prims(void); 
void test_kruskals(void); 

int main (void) 
{
	test_traversals(); 
	//test_dijkstra(); 
	//test_bellman_ford(); 
	//test_prims(); 
	//test_kruskals(); 

	exit (EXIT_SUCCESS); 
}

void test_traversals(void)
{
	result_t ret; 
	vertex_t cnt; 
	static edge_t E[] = { {0, 1}, {0, 5}, {1, 2}, {1, 5}, {2, 3}, {2, 4}, {3, 4}, {4, 5} }; 
	graph_t *g = create_graph (); 
	
	for (cnt=0; cnt < 6; ++cnt)
		if ((ret = add_vertex (g)) != SUCCESS)
		{
			fprintf (stderr, "Error in adding vertex to a graph\n"); 
			exit (EXIT_FAILURE); 
		}

	for (cnt=0; cnt < sizeof (E) / sizeof (edge_t); ++cnt)
	{
		if ((ret = add_edge (g, E[cnt].start, E[cnt].end)) != SUCCESS)
		{
			switch (ret)
			{
				case INVALID_EDGE: 
						fprintf (stderr, "Error in adding edge %d to %d\n", 
								 E[cnt].start, E[cnt].end); 
						break; 
				case VERTEX_NOT_FOUND: 
						fprintf (stderr, "One of the edge vertices not found\n"); 
						break; 
			}
			exit (EXIT_FAILURE); 
		}
	}

	print_graph (g); 

	puts("Depth First Traversal of a graph:");
    dfs(g); 
    puts("End of Depth First Traversal of a graph");

    puts("Breadth First Traversal of a graph"); 
    bfs(g, 3); 
    puts("End of Breadth First Traversal of a graph"); 

	printf ("nr_vertices:%d\n", get_nr_vertices (g)); 
	printf ("nr_edges:%d\n", get_nr_edges (g)); 

	if (remove_edge (g, 0, 2) != SUCCESS)
	{
		fprintf (stderr, "remove_edge:Error\n"); 
	}
	else
	{
		fprintf (stderr, "remove_edge:Unexpected error\n"); 
		exit (EXIT_FAILURE); 
	}

	if (remove_edge (g, 1, 5) != SUCCESS)
	{
		fprintf (stderr, "remove_edge:Error\n"); 
		exit (EXIT_FAILURE); 
	}
	printf ("nr_vertices:%d\n", get_nr_vertices (g)); 
	printf ("nr_edges:%d\n", get_nr_edges (g)); 


	print_graph (g); 

	if((ret = remove_vertex(g, 2)) != SUCCESS)
	{
		fprintf(stderr, "remove_vertex : Error\n"); 
		exit(EXIT_FAILURE); 
	}

	printf("Printing graph after removal of vertex 2\n"); 
	print_graph(g); 
	printf ("nr_vertices:%d\n", get_nr_vertices (g)); 
	printf ("nr_edges:%d\n", get_nr_edges (g)); 

	ret = destroy (&g); 

	if (ret == SUCCESS && !g) 
		fprintf (stderr, "Graph destroyed successfully\n"); 
	else
		exit (EXIT_FAILURE); 
}

graph_t *create_graph (void)
{
	graph_t *g = (graph_t*) xcalloc (1, sizeof (graph_t)); 
	
	g->head 		= (vertex_node_t*) xcalloc (1, sizeof (vertex_node_t)); 
	g->nr_vertices	= 0; 
	g->nr_edges		= 0; 

	g->head->head   	 = NULL; 
	g->head->vertex 	 = -1;  
	g->head->pred_vertex = -1; 
	
	g->head->next = g->head; 
	g->head->prev = g->head; 

	return (g); 
}

result_t add_vertex (graph_t *g)
{
	g_insert_vlist (g->head->prev, get_vertex_node (next_vertex_number ()), g->head); 
	++g->nr_vertices; 

	return (SUCCESS); 
}

result_t add_edge (graph_t *g, vertex_t v1, vertex_t v2) 
{
	vertex_node_t *n_v1, *n_v2; 
	
	if (v1 == v2)
		return (INVALID_EDGE); 

	n_v1  = search_node_vlist (g->head, v1); 
	n_v2  = search_node_vlist (g->head, v2); 
	
	if (!n_v1 || !n_v2)
		return (VERTEX_NOT_FOUND); 
	
	g_insert_adj_list (n_v1->head->prev, get_adj_node (v2), n_v1->head); 
	g_insert_adj_list (n_v2->head->prev, get_adj_node (v1), n_v2->head); 
	++g->nr_edges; 

	return (SUCCESS); 
}

result_t remove_edge (graph_t *g, vertex_t start, vertex_t end)
{
	vertex_node_t *node_start = search_node_vlist (g->head, start); 
	vertex_node_t *node_end   = search_node_vlist (g->head, end); 
	adj_node_t *adj_start, *adj_end; 

	if (!node_start || !node_end)
		return (VERTEX_NOT_FOUND);

	adj_start = search_node_adj_list (node_end->head, start); 
	adj_end   = search_node_adj_list (node_start->head, end); 

	if (!adj_start || !adj_end)
		return (INVALID_EDGE); 

	g_delete_adj_node (adj_start); 
	g_delete_adj_node (adj_end); 
	--g->nr_edges; 

	return (SUCCESS); 
}

result_t remove_vertex (graph_t *g, vertex_t vertex) 
{
	vertex_node_t *v_node = search_node_vlist (g->head, vertex); 
	vertex_node_t *other_node; 
	adj_node_t *run, *self_node; 
	result_t rs; 

	if (!v_node)
		return (VERTEX_NOT_FOUND); 

	for(run = v_node->head->next; run != v_node->head; run = run->next)
	{
		other_node = search_node_vlist(g->head, run->vertex); 
		self_node = search_node_adj_list(other_node->head, vertex); 
		g_delete_adj_node(self_node); 
		--g->nr_edges; 
	}

	destroy_adj_list(v_node->head); 
	g_delete_vertex_node(v_node); 
	--g->nr_vertices; 

	return (SUCCESS); 
}

len_t degree (graph_t *g, vertex_t vertex)
{
	vertex_node_t *v_node = search_node_vlist (g->head, vertex); 
	if (!v_node) 
		return (VERTEX_NOT_FOUND); 
	return (length_adj_list (v_node->head)); 
}

int get_nr_vertices (graph_t *g)
{
	return (g->nr_vertices); 
}

int get_nr_edges (graph_t *g) 
{
	return (g->nr_edges); 
}

void print_graph (graph_t *g) 
{
	vertex_node_t *run_v; 
	adj_node_t *run_adj; 

	for (run_v = g->head->next; run_v != g->head; run_v = run_v->next)
	{
		printf ("[%d]\t<--->\t", run_v->vertex); 
		for (run_adj = run_v->head->next; run_adj != run_v->head; run_adj = run_adj->next)
			printf ("[%d]<->", run_adj->vertex); 
		printf ("[end]\n"); 
	}
}

result_t destroy (graph_t **pp_g) 
{
	result_t rs; 
	graph_t *g = *pp_g;
	vertex_node_t *run, *run_n; 

	for (run = g->head->next; run != g->head; run = run_n)
	{
		run_n = run->next; 
		rs = destroy_adj_list (run->head); 
		if (rs != SUCCESS)
		{
			fprintf (stderr, "destroy:Error\n"); 
			exit (EXIT_FAILURE); 
		}
		free (run); 
	}
	
	free (g->head); 
	free (g); 
	*pp_g = NULL; 
	return (SUCCESS); 
}

void dfs(graph_t *g)
{
	vertex_node_t *run; 
	reset(g); 
	for(run = g->head->next; run != g->head; run = run->next)
	{
		if(run->colour == WHITE)
			dfs_visit(g, run->vertex); 
	}
}

result_t bfs(graph_t *g, vertex_t start_vertex)
{
    queue_t *queue = NULL; 
	vertex_node_t *s = NULL; 
    vertex_node_t *u = NULL; 
    vertex_node_t *v_of_adj = NULL; 
    adj_node_t *adj_run = NULL; 

	reset(g); 
	
	s = search_node_vlist(g->head, start_vertex); 
	if(!s)
		return (VERTEX_NOT_FOUND); 
		
	queue = create_queue(); 

    if(enqueue(queue, s) != SUCCESS)
    {
        uerror("bfs:enqueue"); 
        exit(EXIT_FAILURE); 
    }

    while(!is_queue_empty(queue))
    {
        if(dequeue(queue, &u) != SUCCESS)
        {
            uerror("bfs:is_queue_empty"); 
            exit(EXIT_FAILURE); 
        }

        printf("[%d]\n", u->vertex); 

        for(adj_run = u->head->next; adj_run != u->head; adj_run = adj_run->next)
        {
            v_of_adj = search_node_vlist(g->head, adj_run->vertex); 
            if(v_of_adj->colour == WHITE)
            {
                v_of_adj->colour = GREY; 
                v_of_adj->pred_vertex = u->vertex; 
                if(enqueue(queue, v_of_adj) != SUCCESS)
                {
                    uerror("bfs:enqueue"); 
                    exit(EXIT_FAILURE); 
                }
            }
        }
        u->colour = BLACK; 
    }

    if(destroy_queue(&queue) != SUCCESS && queue != NULL)
    {
        uerror("bfs:destroy_queue"); 
        exit(EXIT_FAILURE); 
    }
}

static int next_vertex_number (void) 
{
	static int next=-1; 
    return (++next); 
}

static result_t is_valid_vertex (graph_t *g, vertex_t v) 
{
	vertex_node_t *run; 
	
	for (run = g->head->next; run != g->head; run = run->next)
			if (run->vertex == v)
				return (TRUE); 

	return (FALSE); 
}

static adj_node_t *get_adj_node (vertex_t vertex) 
{
	adj_node_t *node	= (adj_node_t*) xcalloc (1, sizeof (adj_node_t)); 
	node->vertex 		= vertex; 
	node->w 			= 0.0; 
	return (node); 
}

static vertex_node_t *get_vertex_node (vertex_t vertex) 
{
	vertex_node_t *node = (vertex_node_t*) xcalloc (1, sizeof (vertex_node_t)); 
	
	node->vertex 	 	= vertex; 
	node->pred_vertex	= -1; 
	node->colour	  	= WHITE; 
	node->head 		 	= get_adj_node (-1); 
	node->head->prev 	= node->head; 
	node->head->next 	= node->head; 

	return (node); 
}

static vertex_node_t *search_node_vlist (vertex_node_t *head, vertex_t vertex) 
{
	vertex_node_t *run = head->next; 

	while (run != head)
	{
		if (run->vertex == vertex)
			return (run); 
		run = run->next; 
	}

	return (NULL); 
}

static adj_node_t *search_node_adj_list (adj_node_t *head, vertex_t vertex)
{
	adj_node_t *run = head->next; 

	while (run != head)
	{
		if (run->vertex == vertex) 
			return (run); 
		run = run->next; 
	}

	return (NULL); 
}

static adj_node_t *search_adj_node (graph_t *g, vertex_t v, vertex_t adj_v)
{
	vertex_node_t *v_node = search_node_vlist (g->head, v); 
	if (!v_node)
		return (NULL); 
	return (search_node_adj_list (v_node->head, adj_v)); 
}

static void g_insert_vlist (vertex_node_t *beg, vertex_node_t *mid, vertex_node_t *end)
{
	mid->prev = beg; 
	mid->next = end; 
	beg->next = mid; 
	end->prev = mid; 
}

static void g_insert_adj_list (adj_node_t *beg, adj_node_t *mid, adj_node_t *end) 
{
	mid->prev = beg; 
	mid->next = end; 
	beg->next = mid; 
	end->prev = mid; 
}

static void g_delete_adj_node (adj_node_t *node) 
{
	adj_node_t *beg = node->prev, *end = node->next; 

	beg->next = end; 
	end->prev = beg; 

	free (node); 
}

static void g_delete_vertex_node(vertex_node_t *node)
{
	node->next->prev = node->prev; 
	node->prev->next = node->next; 
	free(node); 
}

static result_t destroy_vertex_list (vertex_node_t *head)
{
	vertex_node_t *run, *run_n; 

	for (run = head->next; run != head; run = run_n)
	{
		run_n = run->next; 
		free (run); 
	}

	free (head); 

	return (SUCCESS); 
}

static result_t destroy_adj_list (adj_node_t *head)
{
	adj_node_t *run, *run_n; 

	for (run = head->next; run != head; run = run_n) 
	{
		run_n = run->next; 
		free (run); 
	}

	free (head); 

	return (SUCCESS); 
}

static len_t length_adj_list (adj_node_t *head) 
{
	len_t len; 
	adj_node_t *run; 

	for (run = head->next, len = 0; run != head; run = run->next, ++len)
			; 

	return (len); 
}

static void reset(graph_t *g)
{
    vertex_node_t *v_run; 
    for(v_run = g->head->next; v_run != g->head; v_run = v_run->next)
    {
        v_run->colour = WHITE; 
        v_run->pred_vertex = -1; 
    }
}

static void dfs_visit(graph_t *g, vertex_t u) 
{
	vertex_node_t *u_node = search_node_vlist(g->head, u); 
	vertex_node_t *current; 
	adj_node_t *run; 

	if(!u_node)
	{
		fprintf(stderr, "dfs_visit:unexpected error\n"); 
		exit(EXIT_FAILURE); 
	}	

	printf("[%d]\n", u_node->vertex); 
	u_node->colour = GREY; 
	
	for(run = u_node->head->next; run != u_node->head; run = run->next)
	{
		current = search_node_vlist(g->head, run->vertex); 
		if(!current)
		{
			fprintf(stderr, "dfs_visit:unexpected error\n"); 
			exit(EXIT_FAILURE); 
		}	

		current->pred_vertex = u; 
		if(current->colour == WHITE)
			dfs_visit(g, run->vertex); 
	}
	
	u_node->colour = BLACK; 
}

queue_t *create_queue(void)
{
    return (create_list()); 
}

int enqueue(queue_t *queue, vertex_node_t *v_node)
{
    return (insert_end(queue, v_node)); 
}

int dequeue(queue_t *queue, vertex_node_t **pp_vnode)
{
    return (examine_and_delete_beg(queue, pp_vnode)); 
}

int is_queue_empty(queue_t *queue)
{
    return (is_empty(queue)); 
}

int destroy_queue(queue_t **pp_queue)
{
    return (destroy_list(pp_queue)); 
}

dcll_t *create_list(void)
{
    dcll_t *list_head = get_dcll_node(0); 
    list_head->next = list_head->prev = list_head; 
    return (list_head); 
}

int insert_beg(dcll_t *list, vertex_node_t *v_node)
{
    dcll_node_t *new_node = get_dcll_node(v_node); 
    g_insert(list, new_node, list->next); 
    return (SUCCESS); 
}

int insert_end(dcll_t *list, vertex_node_t *v_node)
{
    dcll_node_t *new_node = get_dcll_node(v_node); 
    g_insert(list->prev, new_node, list); 
    return (SUCCESS); 
}

int del_beg(dcll_t *list)
{
    if(!is_empty(list))
    {
        g_delete(list->next); 
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int del_end(dcll_t *list)
{
    if(!is_empty(list))
    {
        g_delete(list->prev); 
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int examine_and_delete_beg(dcll_t *list, vertex_node_t **pp_vnode)
{
    if(!is_empty(list))
    {
        *pp_vnode = list->next->node;  
        g_delete(list->next);
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int examine_and_delete_end(dcll_t *list, vertex_node_t **pp_vnode)
{
     if(!is_empty(list))
    {
        *pp_vnode = list->prev->node; 
        g_delete(list->prev); 
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int is_empty(dcll_t *list)
{
    return (list->prev == list && list->next == list); 
}

int destroy_list(dcll_t **pp_list)
{
    dcll_t *p_list = *pp_list; 
    dcll_node_t *run, *run_n; 

    for(run = p_list->next; run != p_list; run = run_n)
    {
        run_n = run->next; 
        free(run); 
    }

    free(p_list); 
    *pp_list = NULL; 
    return (SUCCESS); 
}

void g_insert(dcll_node_t *beg, dcll_node_t *mid, dcll_node_t *end)
{
    mid->next = end; 
    mid->prev = beg; 
    beg->next = mid; 
    end->prev = mid; 
}

void g_delete(dcll_node_t *node)
{
    node->next->prev = node->prev; 
    node->prev->next = node->next; 
    free(node); 
}

dcll_node_t *search_node(dcll_t *list, vertex_node_t *v_search_node)
{
    dcll_node_t *run; 

    for(run = list->next; run != list; run = run->next)
        if(run->node == v_search_node)
            return (run); 

    return (NULL); 
}

dcll_node_t *get_dcll_node(vertex_node_t *v_node)
{
    dcll_node_t *p = (dcll_node_t*)xcalloc(1, sizeof(dcll_node_t)); 
    p->node = v_node; 
    return (p); 
}

void *xcalloc (size_t nr_elements, size_t size_per_element) 
{
	void *tmp; 

	tmp = calloc (nr_elements, size_per_element); 
	if (!tmp) 
	{
		fprintf (stderr, "calloc:fatal:out of memory\n"); 
		exit (EXIT_FAILURE); 
	}
    
	return (tmp); 
}