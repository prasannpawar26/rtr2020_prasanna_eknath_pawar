#include <stdio.h> 
#include <stdlib.h> 
#include "../graph.h"

#define SUCCESS 1
#define FAILURE 0 

#define DATA_NOT_FOUND  2
#define LIST_EMPTY      3

#define QUEUE_EMPTY LIST_EMPTY

typedef struct dcll_node
{
    vertex_node_t *node; 
    struct dcll_node *prev, *next; 
}dcll_node_t; 

typedef dcll_node_t list_t; 
typedef list_t queue_t; 

/* Queue interface routines [DATA = vertex_node_t*] */ 
queue_t *create_queue(void); 
int enqueue(queue_t *queue, vertex_node_t *v_node); 
int dequeue(queue_t *queue, vertex_node_t **pp_vnode); 
int destroy_queue(queue_t **pp_queue); 

/* List interface routines. [DATA = vertex_node_t*]  */  
list_t *create_list(void); 
int insert_end(list_t *list, vertex_node_t *v_node); 
int insert_beg(list_t *list, vertex_node_t *v_node); 
int del_beg(list_t *list); 
int del_end(list_t *list); 
int examine_and_delete_beg(list_t *list, vertex_node_t **pp_vnode); 
int examine_and_delete_end(list_t *list, vertex_node_t **pp_vnode); 
int is_empty(list_t *list);
int destroy_list(list_t **pp_list); 

/* List auxillary routines */ 
void g_insert(dcll_node_t *beg, dcll_node_t *mid, dcll_node_t *end); 
void g_delete(dcll_node_t *node); 
dcll_node_t *search_node(list_t *list, vertex_node_t *v_search_node);
dcll_node_t *get_dcll_node(vertex_node_t *v_node); 

/* Auxillary routines */ 
void *xcalloc(size_t nr, size_t size); 


int main(void)
{
    vertex_node_t *v_node_arr[10] = {NULL}; 
    int i; 
    queue_t *queue_vnode = create_queue(); 
    vertex_node_t *v_current_node = NULL; 

    for(i = 0; i < 10; ++i)
        v_node_arr[i] = (vertex_node_t*)xcalloc(1, sizeof(vertex_node_t)); 
    
    for(i = 0; i < 10; ++i)
        printf("v_node_arr[%d]:%p\n", i, v_node_arr[i]); 

    for(i = 9; i > -1; --i)
        if(enqueue(queue_vnode, v_node_arr[i]) == FAILURE)
            exit(EXIT_FAILURE); 

    while(dequeue(queue_vnode, &v_current_node) != QUEUE_EMPTY)
        printf("dequeue:%p\n", v_current_node); 

    destroy_queue(&queue_vnode); 

    return (0); 
}

queue_t *create_queue(void)
{
    return (create_list()); 
}

int enqueue(queue_t *queue, vertex_node_t *v_node)
{
    return (insert_end(queue, v_node)); 
}

int dequeue(queue_t *queue, vertex_node_t **pp_vnode)
{
    return (examine_and_delete_beg(queue, pp_vnode)); 
}

int destroy_queue(queue_t **pp_queue)
{
    return (destroy_list(pp_queue)); 
}

list_t *create_list(void)
{
    list_t *list_head = get_dcll_node(0); 
    list_head->next = list_head->prev = list_head; 
    return (list_head); 
}

int insert_beg(list_t *list, vertex_node_t *v_node)
{
    dcll_node_t *new_node = get_dcll_node(v_node); 
    g_insert(list, new_node, list->next); 
    return (SUCCESS); 
}

int insert_end(list_t *list, vertex_node_t *v_node)
{
    dcll_node_t *new_node = get_dcll_node(v_node); 
    g_insert(list->prev, new_node, list); 
    return (SUCCESS); 
}

int del_beg(list_t *list)
{
    if(!is_empty(list))
    {
        g_delete(list->next); 
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int del_end(list_t *list)
{
    if(!is_empty(list))
    {
        g_delete(list->prev); 
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int examine_and_delete_beg(list_t *list, vertex_node_t **pp_vnode)
{
    if(!is_empty(list))
    {
        *pp_vnode = list->next->node;  
        g_delete(list->next);
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int examine_and_delete_end(list_t *list, vertex_node_t **pp_vnode)
{
     if(!is_empty(list))
    {
        *pp_vnode = list->prev->node; 
        g_delete(list->prev); 
        return (SUCCESS); 
    }

    return (LIST_EMPTY); 
}

int is_empty(list_t *list)
{
    return (list->prev == list && list->next == list); 
}

int destroy_list(list_t **pp_list)
{
    list_t *p_list = *pp_list; 
    dcll_node_t *run, *run_n; 

    for(run = p_list->next; run != p_list; run = run_n)
    {
        run_n = run->next; 
        free(run); 
    }

    free(p_list); 
    *pp_list = NULL; 
    return (SUCCESS); 
}

void g_insert(dcll_node_t *beg, dcll_node_t *mid, dcll_node_t *end)
{
    mid->next = end; 
    mid->prev = beg; 
    beg->next = mid; 
    end->prev = mid; 
}

void g_delete(dcll_node_t *node)
{
    node->next->prev = node->prev; 
    node->prev->next = node->next; 
    free(node); 
}

dcll_node_t *search_node(list_t *list, vertex_node_t *v_search_node)
{
    dcll_node_t *run; 

    for(run = list->next; run != list; run = run->next)
        if(run->node == v_search_node)
            return (run); 

    return (NULL); 
}

dcll_node_t *get_dcll_node(vertex_node_t *v_node)
{
    dcll_node_t *p = (dcll_node_t*)xcalloc(1, sizeof(dcll_node_t)); 
    p->node = v_node; 
    return (p); 
}

void *xcalloc(size_t nr, size_t size)
{
    void *p = calloc(nr, size); 
    if(!p)
    {
        fprintf(stderr, "calloc:fatal:out of memory\n"); 
        exit(EXIT_FAILURE); 
    }

    return (p); 
}