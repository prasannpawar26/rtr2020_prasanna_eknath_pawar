#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 

#define CAP 		1000000
#define LEFT(i) 	2*i+1 
#define RIGHT(i) 	2*i+2 
#define PARENT(i) 	((i-1)/2)

typedef struct heap
{
	int *p_arr; 
	int nr_elements; 
	int heap_size; 
}heap_t; 

void 		*xcalloc		(int nr_elements, int size_per_element); 
void 		input			(heap_t *heap); 
void 		heap_sort 		(heap_t *heap); 
void 		output 			(heap_t *heap);
static void max_heapify		(heap_t *heap, int i);
static void build_max_heap 	(heap_t *heap); 

int main(int argc, char *argv[])
{
	heap_t *heap; 

	if(argc != 2)
	{
		fprintf(stderr, "Usage Error : %s nr_elements\n", argv[0]); 
		exit(EXIT_FAILURE); 
	}

	heap 				= (heap_t*)xcalloc(1, sizeof(heap_t));
	heap->nr_elements 	= atoi(argv[1]); 
	heap->p_arr 		= (int*)xcalloc(heap->nr_elements, sizeof(int));

	input(heap);  
	heap_sort(heap); 
	output(heap);

	free(heap->p_arr); 
	free(heap); 

	exit(EXIT_SUCCESS); 
}

void heap_sort (heap_t *heap)
{
	int i, tmp; 

	build_max_heap(heap); 
	for(i=heap->nr_elements-1; i > 0; i--)
	{
		tmp 			= heap->p_arr[0];
		heap->p_arr[0] 	= heap->p_arr[i]; 
		heap->p_arr[i]	= tmp; 
		--heap->heap_size; 
		max_heapify(heap, 0); 	
	}
}

static void build_max_heap(heap_t *heap)
{
	int i; 
	heap->heap_size = heap->nr_elements-1; 
	for(i = (heap->nr_elements / 2) - 1; i > 0; i--)
		max_heapify(heap, i); 
}

static void max_heapify (heap_t *heap, int i)
{
	int left_index, right_index, largest_index, tmp; 

	left_index 	= LEFT(i); 
	right_index = RIGHT(i); 

	if(left_index < heap->heap_size && 
		heap->p_arr[left_index] > heap->p_arr[i])
		largest_index = left_index; 
	else
		largest_index = i; 

	if(right_index < heap->heap_size && 
		heap->p_arr[right_index] > heap->p_arr[largest_index])
		largest_index = right_index; 

	if(largest_index != i)
	{
		tmp						   = heap->p_arr[i]; 
		heap->p_arr[i] 			   = heap->p_arr[largest_index]; 
		heap->p_arr[largest_index] = tmp; 

		max_heapify(heap, largest_index); 
	}
}

void input(heap_t *heap)
{
	int i; 

	srand(time(0)); 
	for(i=0; i < heap->nr_elements; i++)
		heap->p_arr[i] = rand() % CAP; 
}

void output(heap_t *heap)
{
	int i; 
	for(i=0; i < heap->nr_elements; i++)
		printf("A[%d]:%d\n", i, heap->p_arr[i]); 
}

void *xcalloc(int nr_elements, int size_per_element)
{
	void *ptr = calloc(nr_elements, size_per_element); 
	
	if(!ptr)
	{
		fprintf(stderr, "calloc:out of memory\n"); 
		exit(EXIT_FAILURE); 
	}

	return (ptr); 
}
