#include <stdio.h> 
#include <stdlib.h> 
#include <pthread.h> 
#include "stack.h" 

stack_t *create_stack (void) 
{
	stack_t *st = (stack_t*) xcalloc (1, sizeof (stack_t)); 
	st->top = -1; 
	pthread_mutex_init (&st->lock, NULL); 
	return (st); 
}

result_t push (stack_t *stack, int element)
{
	pthread_mutex_lock (&stack->lock); 
	stack->top++; 
	if (stack->top == MAX_ELEMENTS) 
	{
		pthread_mutex_unlock (&stack->lock);
		return (STACK_FULL); 
	}
	else
	{
		stack->arr[stack->top] = element; 
	}
	pthread_mutex_unlock (&stack->lock); 
	return (SUCCESS); 
}

result_t pop (stack_t *stack, int *p_element) 
{
	pthread_mutex_lock (&stack->lock); 
	if (stack->top < 0) 
	{
		pthread_mutex_unlock (&stack->lock); 
		return (STACK_EMPTY); 
	}
	else
	{
		*p_element = stack->arr[stack->top]; 
		stack->top--; 
	}

	pthread_mutex_unlock (&stack->lock); 
	return (SUCCESS); 

}

result_t top (stack_t *stack, int *p_element) 
{
	pthread_mutex_lock (&stack->lock); 
	if (stack->top < 0) 
	{
		pthread_mutex_unlock (&stack->lock); 
		return (STACK_EMPTY); 
	}
	else
	{
		*p_element = stack->arr[stack->top]; 
	}

	pthread_mutex_unlock (&stack->lock); 
	return (SUCCESS); 
}

result_t is_empty (stack_t *stack) 
{
	result_t ret; 
	pthread_mutex_lock (&stack->lock); 
	if (!stack->top) 
		ret = TRUE; 
	else
		ret = FALSE; 
	pthread_mutex_unlock (&stack->lock); 
	return (ret); 
}

void *xcalloc (int nr_elements, int size_per_element) 
{
	void *ptr; 

	ptr = calloc (nr_elements, size_per_element); 
	if (!ptr) 
	{
		fprintf (stderr, "xcalloc:out of memory\n"); 
		exit (EXIT_FAILURE); 
	}

	return (ptr); 
}
