#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 
#include <unistd.h> 
#include <pthread.h> 
#include "stack.h"

#define NR_OP 1500 
#define TOTAL_OP 4 
#define MAX_ELEMENT_VALUE 10000
#define PUSH 0 
#define POP 1 
#define TOP 2 
#define IS_EMPTY 3

void *stack_op_invoke (void*); 

FILE *fp; 
pthread_mutex_t stdout_lock=PTHREAD_MUTEX_INITIALIZER; 

int main (int argc, char *argv[]) 
{
	pthread_t th1, th2, th3, th4, th5, th6; 
	stack_t *st1, *st2; 

	if (argc != 2) 
	{
		fprintf (stderr, "Usage error:%s log_file\n", argv[0]); 
		exit (EXIT_FAILURE); 
	}
	
	fp = fopen (argv[1], "a"); 
	if (!fp) 
	{
		fprintf (stderr, "Error in opening file:%s\n", argv[1]); 
		exit (EXIT_FAILURE); 
	}

	st1 = create_stack (); 
	st2 = create_stack (); 
	
	srand (time (0)); 

	pthread_create (&th1, NULL, stack_op_invoke, (void*)st1); 
	pthread_create (&th2, NULL, stack_op_invoke, (void*)st1); 
	pthread_create (&th3, NULL, stack_op_invoke, (void*)st1); 
	pthread_create (&th4, NULL, stack_op_invoke, (void*)st2); 
	pthread_create (&th5, NULL, stack_op_invoke, (void*)st2); 
	pthread_create (&th6, NULL, stack_op_invoke, (void*)st2); 
	
	pthread_join (th1, NULL); 
	pthread_join (th2, NULL); 
	pthread_join (th3, NULL); 
	pthread_join (th4, NULL); 
	pthread_join (th5, NULL); 
	pthread_join (th6, NULL); 

	exit (EXIT_SUCCESS); 
}

void *stack_op_invoke (void *args) 
{
	stack_t *st=(stack_t*)args; 
	int cnt, n_op, val; 
	result_t rs; 
	
	for (cnt=0; cnt < NR_OP; cnt++) 
	{
		n_op = rand () % TOTAL_OP;  		
		switch (n_op) 
		{
			case PUSH: 
				val = rand () % MAX_ELEMENT_VALUE; 
				rs = push (st, val); 
				pthread_mutex_lock (&stdout_lock); 
				if (rs == SUCCESS) 
				{
					fprintf (fp, "thread:%lx:push:%d\n", 
						 (unsigned long)pthread_self (), 
						 val); 
				}
				else if (rs == STACK_FULL)
				{
					fprintf (fp, "thread:%lx:push:STACK FULL\n", 
						(unsigned long)pthread_self ()); 
				}
				else
				{
					fprintf (fp, "thread:%lx:push:ERROR\n", 
						(unsigned long)pthread_self()); 
				}
				pthread_mutex_unlock (&stdout_lock); 
				break; 
			case POP: 
				rs = pop (st, &val); 
				pthread_mutex_lock (&stdout_lock); 
				if (rs == SUCCESS) 
				{
					fprintf (fp, "thread:%lx pop:%d\n", 
						(unsigned long)pthread_self (), 
						val); 
				}
				else if (rs == STACK_EMPTY) 
				{
					fprintf (fp, "thread:%lx pop:STACK EMPTY\n", 
						(unsigned long)pthread_self()); 
				}
				else
				{
					fprintf (fp, "thread:%lx pop:ERROR\n", 
						(unsigned long)pthread_self ()); 
				}
				pthread_mutex_unlock (&stdout_lock); 
				break; 
			case TOP: 
				rs = top (st, &val); 
				pthread_mutex_lock (&stdout_lock); 
				if (rs == SUCCESS) 
				{
					fprintf (fp, "thread:%lx top:%d\n", 
						(unsigned long)pthread_self (), 
						val); 
				}
				else if (rs == STACK_EMPTY) 
				{
					fprintf (fp, "thread:%lx top:STACK EMPTY\n", 
						(unsigned long)pthread_self()); 
				}
				else
				{
					fprintf (fp, "thread:%lx top:ERROR\n", 
						(unsigned long)pthread_self ()); 
				}
				pthread_mutex_unlock (&stdout_lock); 
				break; 
			case IS_EMPTY: 
				rs = is_empty (st); 
				pthread_mutex_lock (&stdout_lock); 
				if (rs == TRUE) 
				{
					fprintf (fp, "thread:%lx is_empty:TRUE\n", 
						(unsigned long)pthread_self ()); 
				}
				else
				{
					fprintf (fp, "thread:%lx is_empty:FALSE\n", 
						(unsigned long)pthread_self ()); 

				}
				pthread_mutex_unlock (&stdout_lock); 
				break; 
		}

	}
}
