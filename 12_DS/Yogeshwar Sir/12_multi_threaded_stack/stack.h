#ifndef _STACK_H 
#define _STACK_H 

#include <pthread.h> 

#define MAX_ELEMENTS 100 
#define TRUE 0 
#define FALSE 1  	
#define SUCCESS 0 
#define STACK_FULL 2  
#define STACK_EMPTY 3  

typedef int result_t; 
typedef struct stack 
{
	int arr[MAX_ELEMENTS]; 
	int top; 
	pthread_mutex_t lock; 
}stack_t; 

stack_t *create_stack (void); 
result_t push (stack_t *stack, int element); 
result_t pop (stack_t *stack, int *p_element); 
result_t top (stack_t *stack, int *p_element); 
result_t is_empty (stack_t *stack); 
result_t destroy_stack (stack_t *stack); 

void *xcalloc (int, int); 

#endif /* _STACK_H */ 
