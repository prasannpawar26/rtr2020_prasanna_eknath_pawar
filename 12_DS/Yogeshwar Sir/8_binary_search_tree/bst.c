#include <stdio.h> 
#include <stdlib.h> 
#include "bst.h" 

bst_t *bst_create () 
{
	return ((bst_t*) xcalloc (1, sizeof (bst_t))); 
}

result_t bst_insert (bst_t *p_tree, data_t n_data)
{
	node_t *root, *run, *new_node; 
	new_node = get_node (n_data); 

	root = p_tree->root; 

	if (!root && !p_tree->nr_elements) 
	{
		p_tree->root = new_node; 
		p_tree->nr_elements++; 
		return (SUCCESS); 
	}

	run = root; 

	while (1) 
	{
		if (n_data <= run->data)  
		{
			if (run->left) 
			{
				run = run->left; 
				continue; 
			}
			else
			{
				run->left = new_node; 
				new_node->parent = run; 
				++p_tree->nr_elements; 
				return (SUCCESS); 
			}
		}
		else if (n_data > run->data) 
		{
			if (run->right) 
			{
				run = run->right; 
				continue; 
			}
			else
			{
				run->right = new_node; 
				new_node->parent = run; 
				++p_tree->nr_elements; 
				return (SUCCESS); 
			}
		}
	}
}

void bst_preorder (bst_t *p_tree)
{
	node_t *run = p_tree->root; 
	if (run)
	{ 
		__bst_preorder (run); 
	}
}

void bst_inorder (bst_t *p_tree) 
{
	node_t *run = p_tree->root; 
	if (run)
	{	
		__bst_inorder (run); 
	}
}

void bst_postorder (bst_t *p_tree) 
{
	node_t *run = p_tree->root; 
	if (run)
	{	
		__bst_postorder (run); 
	}
}

result_t bst_maximum (bst_t *p_tree, data_t *p_data) 
{
	node_t *root = p_tree->root, *run; 

	if (!root) 
	{
		return (TREE_EMPTY); 
	}	
	
	run = root; 

	while (run->right)
	{
		run = run->right; 
	}

	*p_data = run->data; 
	return (SUCCESS); 
}

result_t bst_minimum (bst_t *p_tree, data_t *p_data) 
{
	node_t *root = p_tree->root, *run; 
	
	if (!root) 
	{
		return (TREE_EMPTY); 
	}

	run = root; 

	while (run->left) 
	{
		run = run->left; 
	}

	*p_data = run->data; 
	return (SUCCESS); 
}

result_t search_bst_nonrecursive(bst_t *p_tree, data_t search_data) 
{
	node_t *root = p_tree->root, *run; 

	if (!root)
	{
		return (TREE_EMPTY);
	}

	run = root; 

	while (run)
	{
		if (run->data == search_data)
			return (TRUE); 
		else if (run->data > search_data)
			run = run->left; 
		else
			run = run->right; 
	}

	return (FALSE); 
}

result_t search_bst_recursive (bst_t *p_tree, data_t search_data) 
{
	node_t *root = p_tree->root, *node; 	
	if(!root)
		return (FALSE); 

	node = __search_bst_recursive(root, search_data); 
	if(node)
		return (TRUE); 
	return (FALSE); 


}
