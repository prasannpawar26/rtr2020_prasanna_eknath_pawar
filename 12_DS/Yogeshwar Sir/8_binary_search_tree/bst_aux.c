#include <stdio.h> 
#include <stdlib.h> 
#include "bst.h" 

node_t *get_node (data_t data) 
{
	node_t *new_node = (node_t*) xcalloc (1, sizeof (node_t)); 
	new_node->data = data; 
	return (new_node); 
}

void __bst_preorder (node_t *node) 
{
	if (node) 
	{
		printf ("%d\n", node->data); 
		__bst_preorder (node->left); 
		__bst_preorder (node->right); 
	}
}

void __bst_inorder (node_t *node) 
{
	if (node) 
	{
		__bst_inorder (node->left); 
		printf ("%d\n", node->data); 
		__bst_inorder (node->right); 
	}
}

void __bst_postorder (node_t *node) 
{
	if (node) 
	{
		__bst_postorder (node->left); 
		__bst_postorder (node->right); 
		printf ("%d\n", node->data); 
	}
}

node_t *__search_bst_recursive(node_t *node, data_t search_data) 
{
	if(node)
	{
		if(node->data == search_data)
			return (node); 
		else if(node->data > search_data) 
			__search_bst_recursive(node->left); 
		else 
			__search_bst_recursive(node->right); 
	}

	return ((node_t*)NULL); 
}
