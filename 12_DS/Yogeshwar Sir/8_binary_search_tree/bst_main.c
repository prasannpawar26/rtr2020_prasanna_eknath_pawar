#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 
#include "bst.h" 

#define CAP 10000000

int main (void) 
{
	bst_t *p_tree; 
	result_t rs; 
	data_t max, min; 
	int i; 

	srand (time (0)); 

	p_tree = bst_create (); 

	for (i=0; i < 1000000; i++) 
	{
		rs = bst_insert (p_tree, rand () % CAP); 
		if (rs != SUCCESS)
		{
			fprintf (stderr, "Unexpected error\n"); 
			exit (EXIT_FAILURE); 
		}
	}

	printf ("Preorder traversal:\n"); 
	bst_preorder (p_tree); 
	
	printf ("Inorder traversal:\n"); 
	bst_inorder (p_tree); 
	
	printf ("Postorder traversal:\n"); 
	bst_postorder (p_tree); 

	rs = bst_maximum (p_tree, &max); 
	if (rs == SUCCESS)
	{
		printf ("max:%d\n", max); 
	}
	else if (rs == TREE_EMPTY) 
	{
		printf ("max:Tree empty\n"); 
	}

	rs = bst_minimum (p_tree, &min); 
	if (rs == SUCCESS)
	{
		printf ("min:%d\n", min); 
	}
	else if (rs == TREE_EMPTY) 
	{
		printf ("min:Tree empty\n"); 
	}

	return (0); 
}
