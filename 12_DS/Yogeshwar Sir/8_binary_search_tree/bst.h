#ifndef _BST_H 
#define _BST_H 

#define SUCCESS		0	
#define FAILURE		1
#define DATA_NOT_FOUND	2 
#define TREE_EMPTY	3


typedef int data_t; 
typedef int result_t; 

typedef struct node 
{
	data_t data; 
	struct node *left, *right, 
		    *parent; 
}node_t; 

typedef struct bst
{
	node_t *root; 
	unsigned long nr_elements; 
}bst_t; 

/* tree interface routines */ 
bst_t *bst_create (void); 
result_t bst_insert (bst_t *p_tree, data_t n_data); 
result_t bst_search (bst_t *p_tree, data_t s_data); 
result_t bst_delete (bst_t *p_tree, data_t e_data); 
result_t bst_maximum (bst_t *p_tree, data_t *p_data); 
result_t bst_minimum (bst_t *p_tree, data_t *p_data); 
void bst_inorder (bst_t *p_tree); 
void bst_preorder (bst_t *p_tree); 
void bst_postorder (bst_t *p_tree); 
result_t bst_destroy (bst_t **pp_tree); 

/* tree auxillary routines */ 
node_t *predecessor (node_t *node); 
node_t *successor (node_t *node); 
node_t *get_node (data_t data); 
void   __bst_preorder (node_t *node); 
void   __bst_inorder (node_t *node); 
void   __bst_postorder (node_t *node); 
node_t *search_node (bst_t *p_tree, data_t s_data); 
/* auxaillary routines */ 
void *xcalloc (int nr_elements, int size_per_element); 
#endif /* _BST_H */ 
