#include <stdio.h> 
#include <stdlib.h> 

typedef struct bst_node
{
	int data; 
	struct bst_node *left; 
	struct bst_node *right; 
	struct bst_node *parent; 
}bst_node_t; 

typedef struct lst_node
{
	struct bst_node *bst_node; 
	struct lst_node *prev; 
	struct lst_node *next; 
}lst_node_t; 

typedef lst_node_t list_t; 
typedef list_t stack_t; 
void g_insert(lst_node_t *beg, lst_node_t *mid, lst_node_t *end); 
void g_delete(lst_node_t *node); 
lst_node_t *get_lst_node(int new_data); 
void *xcalloc(size_t nr, size_t size); 

list_t *create_list(); 
int insert_beg(list_t *lst, int data); 
int insert_end(list_t *lst, int data); 
int	del_beg(list_t *lst); 
int del_end(list_t *lst); 
int is_empty(list_t *lst);  
int examine_beg(list_t *lst, int *datai); 
int examine_end(list_t *lst, int *data); 
int examine_and_delete_beg(list_t *lst, int *data); 
int examine_and_delete_end(list_t *lst, int *data); 

stack_t *create_stack(void); 
int push(stack_t *stack, bst_node_t *bst_node); 
int pop(stack_t *stack, bst_node_t **pp_bst_node); 

stack_t *create_stack(void) 
{
	return create_list(); 
}

int push(stack_t *stack, bst_node_t *bst_node)
{
	return insert_beg(stack, bst_node); 
}

int pop(stack_t *stack, bst_node_t **pp_bst_node)
{
	return examine_and_delete_beg(stack, pp_bst_node); 
}

list_t *create_list(void) 
{
	list_t *head_node = get_lst_node(0); 
	head_node->prev = head_node->next = head_node; 
	return (head_node); 
}

int insert_beg(list_t *lst, int data) 
{
	g_insert(lst, get_lst_node(data), lst->next); 
	return (SUCCESS); 
}

int insert_end(list_t *lst, int data) 
{
	g_insert(lst->prev, get_lst_node(data), lst); 
	return (SUCCESS); 
}

int del_beg(list_t *lst) 
{
	if(is_empty(lst)) 
		return LIST_EMPTY; 
	g_delete(lst->next); 
	return (SUCCESS); 
}

int del_end(list_t *lst) 
{
	if(is_empty(lst))
		return LIST_EMPTY; 
	g_delete(lst->prev); 
	return (SUCCESS); 
}

int is_empty(list_t *lst) 
{
	return (lst->next == lst && lst->prev == lst); 
}

int examine_beg(list_t *lst, int *data)
{
	if(is_empty(lst))
		return LIST_EMPTY; 
	*data = lst->next->data; 
	return (SUCCESS); 
}

int examine_end(list_t *lst, int *data)
{
	if(is_empty(lst))
		return LIST_EMPTY; 
	*data = lst->prev->data; 
	return (SUCCESS); 
}

int examine_and_delete_beg(list_t *lst, int *data)
{
	if(is_empty(lst))
		return LIST_EMPTY; 
	*data = lst->next->data; 
	g_delete(lst->next); 
	return (SUCCESS); 
}

int examine_and_delete_end(list_t *lst, int *data)
{
	if(is_empty(lst))
		return LIST_EMPTY; 
	*data = lst->prev->data; 
	g_delete(lst->prev); 
	return (SUCCESS);
}

void g_insert(lst_node_t *beg, lst_node_t *mid, lst_node_t *end) 
{
	mid->prev = beg; 
	mid->next = end; 
	beg->next = mid; 
	end->prev = mid; 
}

void g_delete(lst_node_t *lst_node)
{
	lst_node->prev->next = lst_node->next; 
	lst_node->next->prev = lst_node->prev; 
	free(lst_node); 
}

lst_node_t *get_lst_node(int data) 
{
	lst_node_t *new_node = (lst_node_t*)xcalloc(1, sizeof(lst_node_t)); 
	new_node->data = data; 
	return (new_node); 
}

void *xcalloc(size_t nr, size_t size)
{
	void *p = calloc(nr, size); 
	if(!p)
	{
		fprintf(stderr, "xcalloc:fatal:out of memory\n"); 
		exit(EXIT_FAILURE); 
	}
	return (p); 
}
