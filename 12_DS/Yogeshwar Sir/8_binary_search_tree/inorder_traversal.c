inorder_nrc(bst_t *p_tree) 
{
	bst_node_t *run = p_tree->root; 
	stack_t *st1 = create_stack(); 
	int rs; 

	while(1)
	{
		while(run != NULL)
		{
			push(st1, run); 
			run = run->left; 
		}
		
		rs = pop(st1, &run); 
		if(rs == STACK_EMPTY)
			break; 

		printf("%d\n", run->data); 

		run = run->right; 
	}
}
