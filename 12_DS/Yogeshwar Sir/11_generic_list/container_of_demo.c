#include <stdio.h> 
#include <stdlib.h> 

#define OFFSET_OF(T,x)	(unsigned long)&(((T*)0)->x)

#define CONTAINER_OF(addr, T, x)	((T*)((char*)addr - OFFSET_OF(T,x)))

struct A1{
	int i_num; 
	char c_ans; 
	double d_num; 
}; 

struct A2{
	int i_num; 
	char c_ans; 
	double d_num; 
}__attribute__((packed)); 

struct A1 inA1 = {10, 'A', 3.14}; 
struct A2 inA2 = {20, 'B', 6.28}; 

int main(void)
{
	double *pd1 = &inA1.d_num, *pd2	= &inA2.d_num; 

	printf("inA1.i_num=%d\n", CONTAINER_OF(pd1, struct A1, d_num)->i_num); 
	printf("inA1.c_ans=%c\n", CONTAINER_OF(pd1, struct A1, d_num)->c_ans); 

	printf("inA2.i_num=%d\n", CONTAINER_OF(pd2, struct A2, d_num)->i_num); 
	printf("inA2.c_ans=%c\n", CONTAINER_OF(pd2, struct A2, d_num)->c_ans); 

	return (0); 
}
