#include <stdio.h> 
#include <stdlib.h> 

#define OFFSET_OF(T,x)	(unsigned long)&(((T*)0)->x)

struct A1{
	int i_num; 
	char c_ans; 
	double d_num; 
}; 

struct A2{
	int i_num; 
	char c_ans; 
	double d_num; 
}__attribute__((packed)); 

struct A3{
	short int s1, s2; 
	char arr[7]; 
	long int n1, n2; 	
}; 

int main(void)
{
	printf("sizeof(struct A1):%lu\nsizeof(struct A2):%lu\nsizeof(struct A3):%lu\n", 
			sizeof(struct A1), sizeof(struct A2), sizeof(struct A3)); 

	printf("OFFSET_OF(struct A1, i_num):%lu\n", OFFSET_OF(struct A1, i_num)); 
	printf("OFFSET_OF(struct A1, c_ans):%lu\n", OFFSET_OF(struct A1, c_ans)); 
	printf("OFFSET_OF(struct A1, d_num):%lu\n", OFFSET_OF(struct A1, d_num)); 
	printf("OFFSET_OF(struct A2, i_num):%lu\n", OFFSET_OF(struct A2, i_num)); 
	printf("OFFSET_OF(struct A2, c_ans):%lu\n", OFFSET_OF(struct A2, c_ans)); 
	printf("OFFSET_OF(struct A2, d_num):%lu\n", OFFSET_OF(struct A2, d_num)); 
	printf("OFFSET_OF(struct A3, s1):%lu\n", OFFSET_OF(struct A3, s1)); 
	printf("OFFSET_OF(struct A3, s2):%lu\n", OFFSET_OF(struct A3, s2)); 
	printf("OFFSET_OF(struct A3, arr):%lu\n", OFFSET_OF(struct A3, arr)); 
	printf("OFFSET_OF(struct A3, n1):%lu\n", OFFSET_OF(struct A3, n1)); 
	printf("OFFSET_OF(struct A3, n2):%lu\n", OFFSET_OF(struct A3, n2)); 

	return (0); 
}
