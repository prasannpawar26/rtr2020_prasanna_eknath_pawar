#include <stdio.h> 
#include <stdlib.h> 

#define OFFSET_OF(T,x)				(unsigned long)(&((T*)0)->x)
#define CONTAINER_OF(addr, T, x)	((T*)((char*)addr - OFFSET_OF(T,x)))

struct list_head{
	struct list_head *prev, *next; 
}; 

struct signal{
	char __pad[32]; 
}; 

struct mm_struct{
	char __pad[64]; 
}; 

struct task_struct{
	int pid, ruid, euid, suid; 
	volatile long state; 
	struct list_head master; 
	struct signal sig; 
	struct mm_struct mm; 
}; 

void g_insert(struct list_head *beg, struct list_head *mid, struct list_head *end); 
void *xcalloc(int, int); 

struct list_head init; 

int main(void) 
{
	struct task_struct *init_task, *proc1, *proc2, *proc3, *curr; 
	struct list_head *run = NULL; 

	init_task = (struct task_struct*)xcalloc(1, sizeof(struct task_struct)); 
	proc1 = (struct task_struct*)xcalloc(1, sizeof(struct task_struct)); 
	proc2 = (struct task_struct*)xcalloc(1, sizeof(struct task_struct)); 
	proc3 = (struct task_struct*)xcalloc(1, sizeof(struct task_struct)); 
	
	init.next = init.prev = &init; 

	init_task->pid = 1; 
	init_task->ruid = init_task->euid = init_task->suid = 0; 

	proc1->pid = 2288; 
	proc1->ruid = proc1->euid = proc1->suid = 1000; 

	proc2->pid = 3721; 
	proc2->ruid = proc2->euid = proc2->suid = 1001; 

	proc3->pid = 5982; 
	proc3->ruid = 1000; 
	proc3->euid = proc3->suid = 0; 
	
	g_insert(init.prev, &init_task->master, &init); 
	g_insert(init.prev, &proc1->master, &init); 
	g_insert(init.prev, &proc2->master, &init); 
	g_insert(init.prev, &proc3->master, &init); 

	for(run = init.next; run != &init; run = run->next)
	{
		curr = CONTAINER_OF(run, struct task_struct, master); 
		printf("curr->pid=%d curr->ruid=%d curr->euid=%d curr->suid=%d\n", 
				curr->pid, curr->ruid, curr->euid, curr->suid); 	
	}

	free(init_task); 
	free(proc1); 
	free(proc2); 
	free(proc3); 

	return (0); 
}

void g_insert(struct list_head *beg, struct list_head *mid, struct list_head *end)
{
	mid->prev = beg; 
	mid->next = end; 
	beg->next = mid; 
	end->prev = mid; 
}

void *xcalloc(int nr_elements, int size_per_element)
{
	void *p = calloc(nr_elements, size_per_element); 
	if(!p){
		fprintf(stderr, "calloc:fatal:out of memory\n"); 
		exit(EXIT_FAILURE); 
	}

	return (p); 
}

