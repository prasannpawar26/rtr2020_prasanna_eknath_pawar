#ifndef _STACK_H 
#define _STACK_H 

#include "list.h"  

#define STACK_EMPTY LIST_EMPTY 

typedef list_t stack_t; 

stack_t *create_stack(); 
result_t push(stack_t *stack, data_t data); 
result_t top(stack_t *stack, data_t *p_data); 
result_t pop(stack_t *stack, data_t *p_data); 
result_t is_stack_empty(stack_t *stack); 
result_t destroy_stack(stack_t **pp_stack); 

#endif /* _STACK_H */ 
