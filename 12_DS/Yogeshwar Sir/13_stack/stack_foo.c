#define STACK_DIMENSION	10 

typedef struct stack
{
	int arr[STACK_DIMENSION]; 
	int top; 
}stack_t; 


stack_t *create_stack(void)
{
	stack_t *st = (stack_t*) xcalloc(1, sizeof(stack_t)); 
	st->top = -1; 
}

result_t push(stack_t *st, data_t data) 
{
	++st->top; 
	if(st->top == STACK_DIMENSION)
		return (STACK_FULL); 
	st->top = data; 
	return (SUCCESS); 
}

result_t pop(stack_t *st, data_t *p_data) 
{
	if(st->top == -1) 
		return (STACK_EMPTY);
	*p_data = st->arr[st->top]; 
	--st->top; 
	return (SUCCESS); 
}

result_t top(stack_t *st, data_t *p_data) 
{
	if(st->top == -1) 
		return (STACK_EMPTY);
	*p_data = st->arr[st->top]; 
	return (SUCCESS); 
}

result_t is_stack_empty(stack_t *st) 
{
	return (st->top == -1); 
}

result_t destroy_stack(stack_t **pp_stack) 
{
	free(*pp_stack); 
	*pp_stack = NULL;
	return (SUCCESS); 
}

