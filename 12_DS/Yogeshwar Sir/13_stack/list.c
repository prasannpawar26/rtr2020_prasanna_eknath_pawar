#include <stdio.h> 
#include <stdlib.h> 
#include "list.h" 

list_t *create_list(void) 
{
	list_t *tmp_ptr = get_node(0); 
	tmp_ptr->next = tmp_ptr->prev = tmp_ptr; 
	return (tmp_ptr); 
}

result_t insert_beg(list_t *lst, data_t new_data) 
{
	g_insert(lst, get_node(new_data), lst->next); 
	return (SUCCESS); 
}

result_t insert_end(list_t *lst, data_t new_data) 
{
	g_insert(lst->prev, get_node(new_data), lst); 
	return (SUCCESS); 
}

result_t insert_after_data(list_t *lst, data_t e_data, data_t new_data) 
{
	node_t *e_node = search_node(lst, e_data); 

	if(!e_node) 
		return (DATA_NOT_FOUND); 
	
	g_insert(e_node, get_node(new_data), e_node->next); 
	return (SUCCESS); 
}

result_t insert_before_data(list_t *lst, data_t e_data, data_t new_data) 
{
	node_t *e_node = search_node(lst, e_data); 

	if(!e_node) 
		return (DATA_NOT_FOUND); 

	g_insert(e_node->prev, get_node(new_data), e_node); 
	return (SUCCESS); 
}

result_t delete_beg(list_t *lst) 
{
	
	if(lst->next == lst && lst->prev == lst) 
		return (LIST_EMPTY); 
	
	g_delete(lst->next); 
	return (SUCCESS); 
}

result_t delete_end(list_t *lst) 
{
	if(lst->next == lst && lst->prev == lst) 
		return (LIST_EMPTY); 

	g_delete(lst->prev); 
	return (SUCCESS); 
}

result_t delete_data(list_t *lst, data_t d_data) 
{
	node_t *d_node = search_node(lst, d_data); 

	if(!d_node) 
		return (DATA_NOT_FOUND); 

	g_delete(d_node); 
	return (SUCCESS); 
}

result_t examine_beg(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->next->data; 
	return (SUCCESS); 
}

result_t examine_end(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->prev->data; 
	return (SUCCESS); 
}

result_t examine_and_delete_beg(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->next->data; 
	g_delete(lst->next); 
	return (SUCCESS); 
}

result_t examine_and_delete_end(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->prev->data; 
	g_delete(lst->prev); 
	return (SUCCESS); 
}


result_t find(list_t *lst, data_t f_data) 
{
	node_t *f_node = search_node(lst, f_data); 
	if(f_node) 
		return (TRUE); 
	return (FALSE); 
}

bool is_empty(list_t *lst) 
{
	if(lst->next == lst && lst->prev == lst) 
		return (TRUE); 
	return (FALSE); 
}

len_t len(list_t *lst) 
{
	len_t len=0; 
	node_t *run; 

	for(run = lst->next; run != lst; run = run->next, ++len)
			; 

	return (len); 
}

void display(list_t *lst) 
{
	node_t *run=NULL; 
	
	printf("[beg]<->"); 

	for(run=lst->next; run != lst; run = run->next) 
		printf("[%d]<->", run->data); 

	printf("[end]\n"); 
}

data_t *to_array(list_t *lst, len_t *p_len) 
{
	data_t *arr = NULL; 
	len_t length = len(lst); 
	node_t *run = NULL; 
	int i = 0;; 

	if(length == 0)
		return (NULL); 

	*p_len = length; 
	arr = (data_t*) xcalloc(length, sizeof(data_t)); 

	for(run = lst->next; run != lst; run = run->next)
	{
		arr[i]  = run->data; 
		i++; 
	}

	/*for(run = lst->next; run != lst; arr[i++] = run->data, run=run->next)
			; */ 

	return (arr); 
}

list_t *concat(list_t *lst1, list_t *lst2) 
{
	list_t *lst3 = create_list(); 
	node_t *run, *tmp_node; 

	for(run = lst1->next; run != lst1; run = run->next)
		insert_end(lst3, run->data); 
	for(run = lst2->next; run != lst2; run = run->next)
		insert_end(lst3, run->data); 

	return (lst3); 
}

list_t *merge(list_t *lst1, list_t *lst2)
{
	list_t *lst3 = create_list(); 
	node_t *run1 = lst1->next, *run2 = lst2->next; 
	flag_t from_lst1 = FALSE, from_lst2 = FALSE; 
	
	while(1) 
	{
		if(run1 == lst1)
		{
			from_lst1 = TRUE; 
			break; 
		}
	
		if(run2 == lst2)
		{
			from_lst2 = TRUE; 
			break; 
		}

		if(run1->data <= run2->data)
		{
			insert_end(lst3, run1->data); 
			run1 = run1->next; 
		}
		else 
		{
			insert_end(lst3, run2->data); 
			run2 = run2->next; 
		}

	}

	if(from_lst1)
	{
		while(run2 != lst2) 
		{
			insert_end(lst3, run2->data); 
			run2 = run2->next; 
		}
	}

	if(from_lst2) 
	{
		while(run1 != lst1) 
		{
			insert_end(lst3, run1->data); 
			run1 = run1->next; 
		}
	}
	
	return (lst3); 
}


result_t destroy_list(list_t **pp_lst) 
{
	list_t *lst = *pp_lst; 	
	node_t *run, *run_next; 

	for(run = lst->next; run != lst; run = run_next) 
	{
		run_next = run->next; 
		free(run); 
	}

	free(lst); 
	*pp_lst = NULL; 
	return (SUCCESS); 
}

static void g_insert(node_t *beg, node_t *mid, node_t *end) 
{
	mid->next = end; 
	mid->prev = beg; 
	beg->next = mid; 
	end->prev = mid; 
}

static void g_delete(node_t *node) 
{
	node->next->prev = node->prev; 
	node->prev->next = node->next; 
	free(node); 
}

node_t *search_node(list_t *lst, data_t search_data) 
{
	node_t *run = lst->next; 
	while(run != lst) 
	{
		if(run->data == search_data) 
				return (run); 
		run = run->next; 
	}

	return ((node_t*)NULL); 
}

static node_t *get_node(data_t new_data) 
{
	node_t *tmp_ptr = (node_t*)xcalloc(1, sizeof(node_t)); 
	tmp_ptr->data = new_data; 
	return (tmp_ptr); 
}

static void *xcalloc(int nr_elements, size_t size_per_element) 
{
	void *ptr = calloc(nr_elements, size_per_element); 
	if(!ptr) 
	{
		fprintf(stderr, "xcalloc:fatal:out of memory\n"); 
		exit(EXIT_FAILURE); 
	}

	return (ptr); 
}
