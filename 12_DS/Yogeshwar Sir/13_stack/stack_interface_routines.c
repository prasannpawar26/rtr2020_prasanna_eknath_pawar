#include <stdio.h> 
#include <stdlib.h> 
#include "list.h" 
#include "stack.h"

stack_t *create_stack (void) 
{
	return ((stack_t*)create_list ()); 
}

result_t push (stack_t *stack, data_t object) 
{
	return (insert_beg ((list_t*)stack, object)); 
}

result_t pop (stack_t *stack, data_t *p_object) 
{
	return (examine_and_delete_beg (stack, p_object)); 	
}

result_t top (stack_t *stack, data_t *p_object) 
{
	return (examine_beg (stack, p_object)); 
}

result_t is_stack_empty (stack_t *stack) 
{
	return (is_empty (stack)); 
}
