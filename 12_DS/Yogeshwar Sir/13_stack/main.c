#include <stdio.h> 
#include <stdlib.h> 
#include "stack.h" 

#ifndef STACK_DIMENSION 
#define STACK_DIMENSION 10 
#endif 

int main (void) 
{
	stack_t *st1; 
	data_t data; 
	int i; 

	st1 = create_stack (); 
	

	if (is_stack_empty (st1) == TRUE)
	{
		printf ("main:1:stack is empty\n"); 
	}
	else 
	{
		printf ("main:1:stack is not empty\n"); 
	}

	push (st1, 10); 
	push (st1, 20); 
	push (st1, 30); 

	if (is_stack_empty (st1) == TRUE)
	{
		printf ("main:2:stack is empty\n"); 
	}
	else 
	{
		printf ("main:2:stack is not empty\n"); 
	}

	top (st1, &data); 
	printf("top:%d\n", data); 

	while ((pop (st1, &data)) != STACK_EMPTY)
	{
		printf ("pop:data:%d\n", data); 
	}

	return (0); 
}
