#include <stdio.h> 
#include <stdlib.h> 
#include "list.h" 

/* List Interface Routines */ 

list_t *create_list(void)
{
	list_t *lst = get_node(0); 
	lst->next = lst->prev = lst; 
	return (lst); 
}

result_t insert_beg(list_t *lst, data_t new_data)
{
	g_insert(lst, get_node(new_data), lst->next); 
	return (SUCCESS); 
}

result_t insert_end(list_t *lst, data_t new_data) 
{
	g_insert(lst->prev, get_node(new_data), lst); 
	return (SUCCESS); 
}

result_t insert_after_data(list_t *lst, data_t e_data, data_t new_data)
{
	node_t *e_node = search_node(lst, e_data); 
	if(!e_node)
		return (DATA_NOT_FOUND); 
	g_insert(e_node, get_node(new_data), e_node->next); 
	return (SUCCESS); 
}

result_t insert_before_data(list_t *lst, data_t e_data, data_t new_data)
{
	node_t *e_node = search_node(lst, e_data); 
	if(!e_node)
		return (DATA_NOT_FOUND); 
	g_insert(e_node->prev, get_node(new_data), e_node); 
	return (SUCCESS); 
}

result_t delete_beg(list_t *lst)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	g_delete(lst->next);
	return (SUCCESS); 
}

result_t delete_end(list_t *lst)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	g_delete(lst->prev); 
	return (SUCCESS); 
}

result_t delete_data(list_t *lst, data_t e_data)
{
	node_t *e_node = search_node(lst, e_data); 
	if(!e_node)
		return (DATA_NOT_FOUND); 
	g_delete(e_node); 
	return (SUCCESS); 
}

result_t examine_beg(list_t *lst, data_t *p_data) 
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->next->data; 
	return (SUCCESS); 
}

result_t examine_end(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->prev->data; 
	return (SUCCESS); 
}

result_t examine_and_delete_beg(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->next->data; 
	g_delete(lst->next);
	return (SUCCESS); 
}

result_t examine_and_delete_end(list_t *lst, data_t *p_data)
{
	if(is_empty(lst))
		return (LIST_EMPTY); 
	*p_data = lst->prev->data; 
	g_delete(lst->prev); 
	return (SUCCESS); 
}

result_t find(list_t *lst, data_t f_data)
{
	node_t *f_node = search_node(lst, f_data); 
	if(f_node)
		return (TRUE); 
	return (FALSE); 
}

void display(list_t *lst) 
{
	node_t *run; 

	printf("[beg]<->"); 

	for(run = lst->next; run != lst; run = run->next)
		printf("[%d]<->", run->data); 

	printf("[end]\n"); 
}

result_t is_empty(list_t *lst)
{
	return (lst->next == lst && lst->prev == lst); 
}

len_t len(list_t *lst) 
{
	node_t *run = lst->next; 
	len_t len = 0; 
	
	for(; run != lst; run = run->next, ++len)
		; 

	return (len); 
}

data_t *to_array(list_t *lst, len_t *p_len)
{
	len_t lst_len = len(lst); 
	data_t *arr; 
	node_t *run; 
	int i; 

	if(lst_len <= 0)
		return (NULL); 

	arr = (data_t*)xcalloc(lst_len, sizeof(data_t)); 
	
	for(run = lst->next, i = 0; run != lst; run = run->next, ++i)
		arr[i] = run->data; 

	*p_len = lst_len; 
	return (arr); 
}

list_t *to_list(data_t *p_data, len_t len)
{
	list_t *new_list = create_list(); 
	int i; 

	for(i = 0; i < len; i++)
		insert_end(new_list, p_data[i]); 

	return (new_list); 
}

list_t *merge(list_t *lst1, list_t *lst2)
{
	list_t *lst3 = create_list(); 
	node_t *run1 = lst1->next, *run2 = lst2->next; 
	flag_t from_lst1 = FALSE, from_lst2 = FALSE; 
	
	while(TRUE){

		if(run1 == lst1){
			from_lst1 = TRUE; 
			break; 
		}

		if(run2 == lst2){
			from_lst2 = TRUE; 
			break; 
		}

		if(run1->data <= run2->data){
			insert_end(lst3, run1->data); 
			run1 = run1->next; 
		}
		else{
			insert_end(lst3, run2->data); 
			run2 = run2->next; 
		}
	}
	
	if(from_lst1){
		while(run2 != lst2){
			insert_end(lst3, run2->data); 
			run2 = run2->next; 
		}
	}
	else if(from_lst2){
		while(run1 != lst1){
			insert_end(lst3, run1->data); 
			run1 = run1->next; 
		}
	}
	
	return (lst3); 
}

/*
void ncat(list_t **pp_lst, int nr_lists, ...); 

main()
{
	list_t *lst1, lst2, lst3, lst4, lst5; 
	list_t *master_lst; 
	//	populate lst1 to lst5 
	
	ncat(&master_lst, 5, lst1, lst2, lst3, lst4, lst5); 
}
*/ 

list_t *concat(list_t *lst1, list_t *lst2)
{
	list_t *new_list = create_list(); 
	node_t *run; 

	for(run = lst1->next; run != lst1; run = run->next)
		insert_end(new_list, run->data); 

	for(run = lst2->next; run != lst2; run = run->next)
		insert_end(new_list, run->data); 

	return (new_list); 
}

result_t destroy_list(list_t **pp_list) 
{
	list_t *p_list = *pp_list; 
	node_t *run, *run_next; 
	
	for(run = p_list->next; run != p_list; run = run_next){
		run_next = run->next; 
		free(run); 
	}

	free(p_list); 
	*pp_list = NULL; 
	return (SUCCESS); 
}

/* List auxillary routines */ 

void g_insert(node_t *beg, node_t *mid, node_t *end)
{
	mid->next = end;
	mid->prev = beg; 
	beg->next = mid; 
	end->prev = mid; 
}

void g_delete(node_t *e_node)
{
	e_node->next->prev = e_node->prev; 
	e_node->prev->next = e_node->next;
	free(e_node); 
	e_node = NULL;
}

node_t *search_node(list_t *lst, data_t s_data)
{
	node_t *run; 

	for(run = lst->next; run != lst; run = run->next)
			if(run->data == s_data)
					return (run); 

	return (NULL); 
}

static node_t *get_node(data_t new_data) 
{
	node_t *new_node = (node_t*)xcalloc(1, sizeof(node_t)); 
	new_node->data = new_data; 
	return (new_node); 
}

/* Auxillary routines */ 
static void *xcalloc(size_t nr_elements, size_t size_per_element)
{
	void *p = calloc(nr_elements, size_per_element); 
	if(!p){
		fprintf(stderr, "xcalloc:fatal:out of memory\n"); 
		exit(EXIT_FAILURE); 
	}
	return (p); 
}


