#include <stdio.h> 
#include <stdlib.h> 
#include "list.h" 

#define uerror(fun_name) printf("%s:%d:Unexpected Errors", fun_name, __LINE__ )

int main(void) 
{
	list_t *lst = create_list(), *lst1, *lst2, *cat_list=NULL, *merge_list=NULL; 
	data_t data, *arr=NULL; 
	result_t rs; 
	len_t lst_len, arr_len; 
	int i; 

	if(is_empty(lst))
		puts("1:List is empty"); 
	else{
		uerror("is_empty"); 
		exit(EXIT_FAILURE); 
	}

	if(delete_beg(lst) == LIST_EMPTY)
		puts("2:Cannot del_beg from empty list"); 
	else{
		uerror("del_beg"); 
		exit(EXIT_FAILURE); 
	}

	if(delete_end(lst) == LIST_EMPTY)
		puts("3:Cannot del_end from empty list");
	else{
		uerror("del_end"); 
		exit(EXIT_FAILURE); 
	}

	if(examine_beg(lst, &data) == LIST_EMPTY)
		puts("4:Cannot examine_beg from empty list"); 
	else{
		uerror("examine_beg"); 
		exit(EXIT_FAILURE); 
	}
		
	if(examine_end(lst, &data) == LIST_EMPTY)
		puts("5:Cannot examine_end from empty list"); 
	else{
		uerror("examine_end"); 
		exit(EXIT_FAILURE); 
	}
	
	if(examine_and_delete_beg(lst, &data) == LIST_EMPTY)
		puts("6:Cannot examine_and_delete_beg from empty list"); 
	else{
		uerror("examine_and_delete_beg"); 
		exit(EXIT_FAILURE); 
	}	

	if(examine_and_delete_end(lst, &data) == LIST_EMPTY)
		puts("7:Cannot examine_and_delete_end from empty list");  
	else{
		uerror("examine_and_delete_end"); 
		exit(EXIT_FAILURE); 
	}

	for(data = 0; data < 5; data++){
		if(insert_beg(lst, data) != SUCCESS){
			uerror("insert_beg"); 
			exit(EXIT_FAILURE); 
		}
	}
	printf("8:display:insert_beg:"); 
	display(lst); 

	for(data = 5; data < 10; ++data){
		if(insert_end(lst, data) != SUCCESS){
			uerror("insert_end"); 
			exit(EXIT_FAILURE); 
		}
	}
	printf("9:display:insert_end:"); 
	display(lst); 
	
	if(insert_after_data(lst, 1000, 100) != DATA_NOT_FOUND){
		uerror("insert_afer_data"); 
		exit(EXIT_FAILURE); 
	}
	puts("10:insert_after_data:1000 is not in list"); 

	if(insert_before_data(lst, -435, 200) != DATA_NOT_FOUND){
		uerror("inesrt_before_data"); 
		exit(EXIT_FAILURE); 
	}
	puts("11:insert_before_data:-435 is not in list"); 

	if(insert_after_data(lst, 0, 100) != SUCCESS){
		uerror("insert_after_data"); 
		exit(EXIT_FAILURE); 
	}
	printf("12:insert_after_data:"); 
	display(lst); 

	if(insert_before_data(lst, 0, 200) != SUCCESS){
		uerror("insert_before_data"); 
		exit(EXIT_FAILURE); 
	}
	printf("13:insert_before_data:"); 
	display(lst); 

	if(delete_beg(lst) != SUCCESS){
		uerror("delete_beg"); 
		exit(EXIT_FAILURE); 
	}
	printf("14:del_beg:"); 
	display(lst); 

	if(delete_end(lst) != SUCCESS){
		uerror("delete_end"); 
		exit(EXIT_FAILURE); 
	}
	printf("15:del_end:"); 
	display(lst); 

	if(delete_data(lst, -234) != DATA_NOT_FOUND){
		uerror("delete_data"); 
		exit(EXIT_FAILURE); 
	}
	printf("16:delete_data:"); 
	display(lst); 
	
	if(delete_data(lst, 0) != SUCCESS){
		uerror("delete_data"); 
		exit(EXIT_FAILURE); 
	}
	printf("17:delete_data:0:");
	display(lst); 

	if(examine_beg(lst, &data) != SUCCESS){
		uerror("examine_beg");
		exit(EXIT_FAILURE); 
	}
	printf("18:examine_beg:%d\n", data); 

	if(examine_end(lst, &data) != SUCCESS){
		uerror("examine_end"); 
		exit(EXIT_FAILURE); 
	}
	printf("19:examine_end:%d\n", data); 

	if(examine_and_delete_beg(lst, &data) != SUCCESS){
		uerror("examine_and_delete_beg"); 
		exit(EXIT_FAILURE); 
	}
	printf("20:examine_and_delete_beg:"); 
	display(lst); 

	if(examine_and_delete_end(lst, &data) != SUCCESS){
		uerror("examine_and_delete_end"); 
		exit(EXIT_FAILURE); 
	}
	printf("21:examine_and_delete_end:"); 
	display(lst); 
	
	if(is_empty(lst) == TRUE){
		uerror("is_empty"); 
		exit(EXIT_FAILURE); 
	}
	puts("22:is_empty:List is not empty"); 

	lst_len = len(lst); 
	printf("23:len:length:%d\n", lst_len); 

	if(find(lst, -1) != FALSE){
		uerror("find"); 
		exit(EXIT_FAILURE); 
	}
	puts("24:find:-1 is not in the list"); 

	if(find(lst, 6) != TRUE){
		uerror("find"); 
		exit(EXIT_FAILURE); 
	}
	puts("25:find:6 is present in the list"); 

	if((arr = to_array(lst, &arr_len)) == NULL){
		uerror("to_array"); 
		exit(EXIT_FAILURE); 
	}

	printf("26:to_array:"); 
	for(i=0; i < arr_len; ++i)
		printf("[%d]", arr[i]); 
	printf("\n"); 
	
	rs = destroy_list(&lst); 
	if(rs == SUCCESS && lst == NULL)
		puts("27:destroy_list:List is successfully destroyed"); 
	else{
		uerror("destroy_list"); 
		exit(EXIT_FAILURE); 	
	}
	
	lst1 = create_list(); 
	lst2 = create_list(); 

	for(data = 1; data < 10; ++data)
		if(insert_end(lst1, 10 * data) != SUCCESS){
			uerror("insert_end"); 
			exit(EXIT_FAILURE); 
		}

	for(data = 5; data < 125 ; data += 10)
		if(insert_end(lst2, data) != SUCCESS){
			uerror("insert_end"); 
			exit(EXIT_FAILURE); 
		}	

	printf("28:lst1:"); 
	display(lst1); 

	printf("29:lst2:"); 
	display(lst2); 

	if((cat_list = concat(lst1, lst2)) == NULL){
		uerror("concat"); 
		exit(EXIT_FAILURE); 
	}
	printf("30:concat:"); 
	display(cat_list); 

	if((merge_list = merge(lst1, lst2)) == NULL){
		uerror("merge"); 
		exit(EXIT_FAILURE); 
	}
	printf("31:merge:"); 
	display(merge_list); 

	if((rs = destroy_list(&lst1)) == SUCCESS && lst1 == NULL)
		puts("32:destroy_list:lst1 is destroyed successfully"); 
	else{
		uerror("destroy_list"); 
		exit(EXIT_FAILURE); 
	}

	if((rs = destroy_list(&lst2)) == SUCCESS && lst2 == NULL)
		puts("33:destroy_list:lst2 is destroyed successfully"); 
	else{
		uerror("destroy_list"); 
		exit(EXIT_FAILURE); 
	}

	if((rs = destroy_list(&cat_list)) == SUCCESS && cat_list == NULL)
		puts("34:destroy_list:cat_list is destroyed successfully"); 
	else{
		uerror("destroy_list"); 
		exit(EXIT_FAILURE); 
	}

	if((rs = destroy_list(&merge_list)) == SUCCESS && merge_list == NULL)
		puts("35:destroy_list:merge_list is destroyed successfully"); 
	else{
		uerror("destroy_list"); 
		exit(EXIT_FAILURE); 
	}

	exit(EXIT_SUCCESS); 
}

