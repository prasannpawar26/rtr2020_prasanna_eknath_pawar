#include <stdio.h> 
#include <stdlib.h> 

#define TRUE 1
#define FALSE 0 
#define SUCCESS 1
#define FAILURE 0 
#define DATA_NOT_FOUND 2 
#define LIST_EMPTY 3 
#define QUEUE_EMPTY LIST_EMPTY 

struct node
{
	int data; 
	struct node *prev; 
	struct node *next; 
}; 

typedef struct node node_t; 
typedef node_t list_t; 
typedef list_t priority_queue_t; 

priority_queue_t *create_priority_queue(void); 
int enqueue(priority_queue_t *p_prio_queue, int data); 
int max_dequeue(priority_queue_t *p_prio_queue, int *data); 
int min_dequeue(priority_queue_t *p_prio_queue, int *data); 
int destroy_priority_queue(priority_queue_t **pp_prio_queue); 
int is_queue_empty(priority_queue_t *p_prio_queue); 

static int examine_beg(list_t *p_lst, int *p_data); 
static int examine_end(list_t *p_lst, int *p_data); 
static int examine_and_delete_beg(list_t *p_lst, int *p_data); 
static int examine_and_delete_end(list_t *p_lst, int *p_data); 
static int is_list_empty(list_t *p_lst); 
static int destroy_list(list_t **pp_lst); 
static void g_insert(node_t *p_beg, node_t *p_mid, node_t *p_end); 
static void g_delete(node_t *p_node); 
static node_t *search_node(list_t *p_lst, int s_data); 
static node_t *get_node(int data); 
static void *xcalloc(size_t nr_elements, size_t size_per_element); 

int main()
{
	int A[] = {10, 154, 3, 457, 546, 4652, 463, 45763, 336, 7352, 5355,  735}; 
	int i; 
	int ret; 
	int min_data; 
	int max_data; 

	priority_queue_t *p_prio_queue = create_priority_queue(); 

	for(i = 0; i < sizeof(A) / sizeof(A[0]); i++)
	{
		ret = enqueue(p_prio_queue, A[i]); 
		if(ret != SUCCESS)
		{
			fprintf(stderr, "Unexpected error\n"); 
			exit(EXIT_FAILURE); 
		}
	}

	for(i = 0; i < 3; i++)
	{
		ret = max_dequeue(p_prio_queue, &max_data); 
		if(ret != SUCCESS)
		{
			fprintf(stderr, "Unexpected error\n"); 
			exit(EXIT_FAILURE); 
		}

		printf("Max element:%d\n", max_data); 
	}

	for(i = 0; i < 4; i++)
	{
		ret = min_dequeue(p_prio_queue, &min_data); 
		if(ret != SUCCESS)
		{
			fprintf(stderr, "Unexpected error\n"); 
			exit(EXIT_FAILURE); 
		}

		printf("Min element:%d\n", min_data); 
	}

	if(destroy_priority_queue(&p_prio_queue) != SUCCESS && !p_prio_queue)
		printf("Priority queue is destroyed with success\n"); 

	return (EXIT_SUCCESS); 
}

priority_queue_t *create_priority_queue(void)
{
	priority_queue_t *p_prio_queue = (priority_queue_t*)xcalloc(1, sizeof(node_t)); 
	p_prio_queue->next = p_prio_queue->prev = p_prio_queue; 
	return (p_prio_queue); 
}

int enqueue(priority_queue_t *p_prio_queue, int data) 
{
	node_t *p_run = NULL; 

	for(p_run = p_prio_queue->next; p_run != p_prio_queue; p_run = p_run->next)	
		if(p_run->data > data)
			break; 

	g_insert(p_run->prev, get_node(data), p_run); 
	return (SUCCESS); 
}

int max_dequeue(priority_queue_t *p_prio_queue, int *data)
{
	return examine_and_delete_end(p_prio_queue, data); 
}

int min_dequeue(priority_queue_t *p_prio_queue, int *data)
{
	return examine_and_delete_beg(p_prio_queue, data); 
}

int destroy_priority_queue(priority_queue_t **pp_prio_queue)
{
	return destroy_list(pp_prio_queue); 
}

static int examine_beg(list_t *p_lst, int *p_data)
{
	if(is_list_empty(p_lst) == TRUE)
		return (QUEUE_EMPTY); 
	*p_data = p_lst->next->data; 
	return (SUCCESS); 
}

static int examine_end(list_t *p_lst, int *p_data)
{
	if(is_list_empty(p_lst) == TRUE)
		return (QUEUE_EMPTY); 
	*p_data = p_lst->prev->data; 
	return (SUCCESS); 
}

static int examine_and_delete_beg(list_t *p_lst, int *p_data)
{
	if(is_list_empty(p_lst) == TRUE)
		return (QUEUE_EMPTY); 
	*p_data = p_lst->next->data; 
	g_delete(p_lst->next); 
	return (SUCCESS); 
}

static int examine_and_delete_end(list_t *p_lst, int *p_data)
{
	if(is_list_empty(p_lst) == TRUE)
		return (QUEUE_EMPTY); 
	*p_data = p_lst->prev->data; 
	g_delete(p_lst->prev); 
	return (SUCCESS); 
}

static int is_list_empty(list_t *p_lst)
{
	return (p_lst->next == p_lst && p_lst->prev == p_lst); 
}

static int destroy_list(list_t **pp_lst)
{
	list_t *p_lst = *pp_lst; 
	node_t *run, *run_n; 

	for(run = p_lst->next; run != p_lst; run = run_n)
	{
		run_n = run->next; 
		free(run); 
	}

	*pp_lst = NULL; 
	return (SUCCESS); 
}


static void g_insert(node_t *p_beg, node_t *p_mid, node_t *p_end)
{
	p_mid->next = p_end; 
	p_mid->prev = p_beg; 
	p_beg->next = p_mid; 
	p_end->prev = p_mid; 
}

static void g_delete(node_t *p_node)
{
	p_node->prev->next = p_node->next; 
	p_node->next->prev = p_node->prev; 
	free(p_node); 
}

static node_t *search_node(list_t *p_lst, int s_data)
{
	node_t *p_run; 

	for(p_run = p_lst->next; p_run != p_lst; p_run = p_run->next)
		if(p_run->data == s_data)
			return (p_run); 
	
	return (NULL); 
}

static node_t *get_node(int data)
{
	node_t *p_node = (node_t*)xcalloc(1, sizeof(node_t)); 
	p_node->data = data; 
	return (p_node); 
}

static void *xcalloc(size_t nr_elements, size_t size_per_element)
{
	void *p = calloc(nr_elements, size_per_element); 
	if(!p)
	{
		fprintf(stderr, "calloc:fatal:out of memory\n"); 
		exit(EXIT_FAILURE); 
	}

	return (p); 
}