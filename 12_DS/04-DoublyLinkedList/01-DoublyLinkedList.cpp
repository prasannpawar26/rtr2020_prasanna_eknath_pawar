#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>


typedef  struct _NODE
{
	int data;
	struct _NODE* next;
	struct _NODE* prev;
}NODE;

NODE* gHead = NULL;

int main(void)
{
	// Function Declarations
	void Insert(int, int);
	void Delete(int);
	void Count(void);
	void Display(void);
	void Menu(void);

	// Variable Declarations
	int option;
	int position;


	// Code
	Menu();

	while (true)
	{
		printf("Enter Choice\n\n");
		scanf("%d", &option);

		switch (option)
		{
			case 0:
			{
				exit(0);
			}
			break;

			case 1:
			{
				int data = 0;
				printf("Enter Position Of Node To Be Insert\n\n");
				scanf("%d", &position);

				printf("Enter Data\n\n");
				scanf("%d", &data);

				Insert(data, position);
			}
				break;

			case 2:
			{
				printf("Enter Position Of Node To Be Delete\n\n");
				scanf("%d", &position);
				Delete(position);
			}
			break;

			case 3:
			{
				Count();
			}
			break;

			case 4:
			{
				Display();
			}
			break;

			default:
				printf("Enter Correct Choice\n\n");
				Menu();
				break;
		} // End Of Switch Case
	} // End Of While

	return 0;
}

void Menu(void)
{
	printf("Menu :\n\n");
	printf("0. Exit\n\n");
	printf("1. Insert\n\n");
	printf("2. Delete\n\n");
	printf("3. Count\n\n");
	printf("4. Display\n\n");

	return;
}

void Insert(int data, int position)
{
	// Variable Delcarations
	NODE* new_node;
	NODE* temp;
	int traverse;

	// Code
	new_node = (NODE*)malloc(sizeof(NODE));
	if (NULL == new_node)
	{
		printf("Memory Allocation Failed\n\n");
		return;
	}
	memset(new_node, 0, sizeof(NODE));

	new_node->data = data;
	new_node->next = NULL;
	new_node->prev = NULL;

	if (NULL == gHead)
	{
		gHead = new_node;
	}

	if (1 == position)
	{
		new_node->next = gHead;
		gHead->prev = new_node;
		new_node->prev = NULL;
		gHead = new_node;

		printf("New Node Inserted Successfully\n\n");
		return;
	}

	temp = gHead;
	while (NULL != temp->next && traverse < position)
	{
		traverse++;
		temp = temp->next;
	}

	temp->next = new_node;
	new_node->prev = temp;

	return;
}

void Delete(int position)
{
	// Variable Declarations
	NODE* temp;
	NODE* delete_node;
	int traverse;

	// Code
	if (NULL == gHead)
	{
		printf("List Is Empty\n\n");
		return;
	}

	if (1 == position)
	{
		temp = gHead;
		gHead = temp->next;
		gHead->prev = NULL;

		temp->next = NULL;
		temp->prev = NULL;

		free(temp);
		temp = NULL;

		printf("Node Deleted From Position : %d Successfully \n\n", position);
	}

	temp = gHead;
	traverse = 1;

	while (NULL != temp->next && traverse < (position - 1))
	{
		traverse++;
		temp = temp->next;
	}

	if (NULL != temp->next)
	{
		delete_node = temp->next;
		temp->next = delete_node->next;

		if (NULL != delete_node->next)
		{
			delete_node->next->prev = temp;
		}

		delete_node->next = NULL;
		delete_node->prev = NULL;

		free(delete_node);
		delete_node = NULL;

		printf("Node Deleted From Position : %d Succssfully\n\n", position);
	}

	return;
}

void Display(void)
{
	// Variable Declarations
	NODE* temp;

	if (NULL == gHead)
	{
		printf("List Is Empty\n\n");

		return;
	}

	temp = gHead;
	while (NULL != temp)
	{
		printf("%d <->", temp->data);
		temp = temp->next;
	}
	printf(" NULL\n\n");

	return;
}

void Count(void)
{
	// Variable Delcarations
	NODE* temp = NULL;
	int count = 0;

	// Code
	if (NULL == gHead)
	{
		printf("List Is Empty\n\n");

		return;
	}

	temp = gHead;
	while (NULL != temp)
	{
		count++;
		temp = temp->next;
	}
	printf("Node Count = %d\n", count);

	return;
}
