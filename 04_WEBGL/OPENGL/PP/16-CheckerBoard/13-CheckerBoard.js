//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// shader program related variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_rectangle1;
var vbo_position1;
var vbo_texture1;

var checkerboard_texture = 0;
var checker_image_width = 64;
var checker_image_height = 64;

var checker_image = new Uint8Array(checker_image_width * checker_image_height * 4);  
	
// uniforms
var mvpUniform;
var uniform_texture0_sampler;

var perspectiveProjectionMatrix;

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}

function init()
{
	// code	
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec2 vTexture0_Coord;" +
	"out vec2 out_texture0_coord;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_texture0_coord = vTexture0_Coord;" +
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	// fragment shader
	var fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"in vec2 out_texture0_coord;" +
	"uniform highp sampler2D u_texture0_sampler;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
		"FragColor = texture(u_texture0_sampler, out_texture0_coord);" +
	"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	// get MVP uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	
	// Load Procedural  Texture	
	makeCheckerImage();
	
	checkerboard_texture = gl.createTexture();	

	gl.bindTexture(gl.TEXTURE_2D, checkerboard_texture);
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);

	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, checker_image_width, checker_image_height, 0, gl.RGBA, gl.UNSIGNED_BYTE, checker_image);

	gl.bindTexture(gl.TEXTURE_2D, null);

	// vertices, colors, shader attribs, vbo, vao initializations
	
	// Rectangle -1
	vao_rectangle1 = gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle1);

	vbo_position1 = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position1);
	gl.bufferData(gl.ARRAY_BUFFER, 0, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var rectangleTexcoords1 = new Float32Array([
	0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0
	]);
	
	vbo_texture1 = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture1);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleTexcoords1, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	// Rectangle - 2
	vao_rectangle2 = gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle2);

	vbo_position2 = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position2);
	gl.bufferData(gl.ARRAY_BUFFER, 0, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	var rectangleTexcoords2 = new Float32Array([
	0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0
	]);
	
	vbo_texture2 = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture2);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleTexcoords2, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.TEXTURE_2D);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function makeCheckerImage()
{
	var i, j, c;

	for (i = 0; i < 64; i++)
	{
		for (j = 0; j < 64; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
			checker_image[(i * 64 + j) * 4 + 0] = c;
			checker_image[(i * 64 + j) * 4 + 1] = c;
			checker_image[(i * 64 + j) * 4 + 2] = c;
			checker_image[(i * 64 + j) * 4 + 3] = 255;
		}
	}
	
	return;
}
	
function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
	gl.bindTexture(gl.TEXTURE_2D, checkerboard_texture);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.8]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
	gl.uniform1i(uniform_texture0_sampler, 0);
	
	var rectangleVertices1 = new Float32Array([
		// Right-Top
		0.0, 1.0, 0.0,
		// Left-Top
		-2.0, 1.0, 0.0,
		// Left-Bottom
		-2.0, -1.0, 0.0,
		// Right-Bottom
		0.0, -1.0, 0.0
	]);
	
	gl.bindVertexArray(vao_rectangle1);
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position1);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices1, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);
	
	var rectangleVertices2 = new Float32Array([
		// Right-Top
		2.41421, 1.0, -1.41421,
		// Left-Top
		1.0, 1.0, 0.0,
		// Left-Bottom
		1.0, -1.0, 0.0,
		// Right-Bottom
		2.41421, -1.0, -1.41421
	]);
	
	gl.bindVertexArray(vao_rectangle2);
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position2);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices2, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);
	
	gl.bindTexture(gl.TEXTURE_2D, null);
	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// code
	
	if(checkerboard_texture)
	{
		gl.deleteTexture(checkerboard_texture);
		checkerboard_texture = 0;
	}
	
	if(vbo_texture2)
	{
		gl.deleteBuffer(vbo_texture2);
		vbo_texture2 = null;
	}
	
	if(vbo_position2)
	{
		gl.deleteBuffer(vbo_position2);
		vbo_position2 = null;
	}
	
	if(vao_rectangle2)
	{
		gl.deleteVertexArray(vao_rectangle2);
		vao_rectangle2 = null;
	}
	
	
	if(vbo_texture1)
	{
		gl.deleteBuffer(vbo_texture1);
		vbo_texture1 = null;
	}
	
	if(vbo_position1)
	{
		gl.deleteBuffer(vbo_position1);
		vbo_position1 = null;
	}
	
	if(vao_rectangle1)
	{
		gl.deleteVertexArray(vao_rectangle1);
		vao_rectangle1 = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 70: // for 'f' or 'F'
			toggleFullScreen();
			break;

		case 27: // Escape
			uninitialize();
			// close our application's tab
			//window.close(); // may not work in Firefox but works in safari and chrome.
			break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	// code
}