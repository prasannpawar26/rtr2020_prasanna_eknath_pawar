//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

var g_width = 0;
var g_height = 0;

var keyPress = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// shader program related variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;

// Uniforms
var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var lightAmbientUniform0;
var lightDiffuseUniform0;
var lightSpecularUniform0;
var lightPositionUniform0;

var materialAmbientUniform;
var materialDiffuseUniform;
var materialSpecularUniform;
var materialShinessUniform;

var keyLPressedUniform;

// light configuration : This values common to all view ports
var lightAmbient0 = [0.0, 0.0, 0.0];
var lightDiffuse0 = [1.0, 1.0, 1.0];
var lightSpecular0 = [1.0, 1.0, 1.0];
var lightPosition0 = [0.0, 0.0, 0.0, 1.0];


var light0_rotationX = 0.0;
var light0_rotationY = 0.0;
var light0_rotationZ = 0.0;

// material configuration : Per sphere and also per viewport this are different
var materialAmbient;
var materialDiffuse;
var materialSpecular;
var materialShiness;

// matrix
var perspectiveProjectionMatrix;
var modelMatrix;
var	viewMatrix;

// light toggled variable
var bLKeyPressed = false;


// per vertex - vertex shader
var perVertex_VertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	
	"struct matrices {" +
		"mat4 model_matrix;" +
		"mat4 view_matrix;" +
		"mat4 projection_matrix;" +
	"};" +
	"uniform matrices u_matrices;" +
	
	"uniform int u_key_L_pressed;" +

	"struct light_configuration {" +
		"vec3 light_ambient;" +
		"vec3 light_diffuse;" +
		"vec3 light_specular;" +
		"vec4 light_position;" +
	"};" +
	"uniform light_configuration u_light_configuration0;" +

	"struct material_configuration {" +
		"vec3 material_ambient;" +
		"vec3 material_diffuse;" +
		"vec3 material_specular;" +
		"float material_shiness;" +
	"};" +
	"uniform material_configuration u_material_configuration;" +
	
	"vec3 light_direction0;" +
	"vec3 tranformation_matrix;" +
	"vec3 viewer_vector;" +
	
	"out vec3 phong_ads_light;" +
	
	"void main(void)" +
	"{" +
		"if(1 == u_key_L_pressed)" +
		"{" +
			"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +

			"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" +
			"viewer_vector = vec3(-eye_coordinates);" +
			"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
			"vec3 viewer_vector_normal = normalize(viewer_vector);" +

			"light_direction0 = vec3(u_light_configuration0.light_position - eye_coordinates);" +
			"vec3 light_direction_normalize0 = normalize(light_direction0);" +
			"vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" +
			"vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +

			"phong_ads_light = ambient0 + diffuse0 + specular0;" + 
		"}" +
		"else" +
		"{" +
			"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
		"}" +

		  "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
	  "}";

// per vertex - fragment shader
var perVertex_FragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"in vec3 phong_ads_light;" +
	"uniform int u_key_L_pressed;" +
	"out vec4 FragColor;" +
	
	"void main(void)" +
	"{" +
		"FragColor = vec4(phong_ads_light, 1.0);" +
	"}";

// per fragment - vertex shader
var perFragment_VertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	"struct matrices {" +
		"mat4 model_matrix;" +
		"mat4 view_matrix;" +
		"mat4 projection_matrix;" +
	"};" +
	"uniform matrices u_matrices;" +
	
	"uniform int u_key_L_pressed;" +

	"struct light_configuration {" +
		"vec3 light_ambient;" +
		"vec3 light_diffuse;" +
		"vec3 light_specular;" +
		"vec4 light_position;" +
	"};" +
	"uniform light_configuration u_light_configuration0;" +

	"out vec3 light_direction0;" +
	"out vec3 tranformation_matrix;" +
	"out vec3 viewer_vector;" +
	
	"void main(void)" +
	"{" +
		"if(1 == u_key_L_pressed)" +
		"{" +
			"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +

			"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" +
			"viewer_vector = vec3(-eye_coordinates);" +

			"light_direction0 = vec3(u_light_configuration0.light_position - eye_coordinates);" +
		  "}" +

		  "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
	  "}";

// per fragment - fragment shader
var perFragment_FragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	
	"in vec3 light_direction0;" +
	"in vec3 tranformation_matrix;" +
	"in vec3 viewer_vector;" +

	"uniform int u_key_L_pressed;" +
	
	"struct light_configuration {" +
		"vec3 light_ambient;" +
		"vec3 light_diffuse;" +
		"vec3 light_specular;" +
		"vec4 light_position;" +
	"};" +
	"uniform light_configuration u_light_configuration0;" +

	"struct material_configuration {" +
		"vec3 material_ambient;" +
		"vec3 material_diffuse;" +
		"vec3 material_specular;" +
		"float material_shiness;" +
	"};" +
	"uniform material_configuration u_material_configuration;" +

	"out vec4 FragColor;" +
	"vec3 phong_ads_light;" +
	"void main(void)" +
	"{" +
		"if(1 == u_key_L_pressed)" +
		"{" +
			"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
			"vec3 viewer_vector_normal = normalize(viewer_vector);" +

			"vec3 light_direction_normalize0 = normalize(light_direction0);" +
			"vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" +
			"vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +

			"phong_ads_light = ambient0 + diffuse0 + specular0;" + 
		"}" +
		"else" +
		"{" +
			"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
		"}" +

		"FragColor = vec4(phong_ads_light, 1.0);" +
		"}";

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}

function init_shaders(vertexShaderSourceCode, fragmentShaderSourceCode)
{
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_matrices.model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_matrices.view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_matrices.projection_matrix");
	
	lightAmbientUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_ambient");
	lightDiffuseUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_diffuse");
	lightSpecularUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_specular");
	lightPositionUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_position");
		
	materialAmbientUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_ambient");
	materialDiffuseUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_diffuse");
	materialSpecularUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_specular");
	materialShinessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_shiness");
	
	keyLPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_key_L_pressed");
}

function init()
{
	// code
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	
	materialAmbient = new Array();
	for(var i = 0; i < 6; i++)
	{
		materialAmbient[i] = new Array();
		for(var j = 0; j < 4; j++)
		{
			materialAmbient[i][j] = new Array();
		}
	}
	
	materialDiffuse = new Array();
	for(var i = 0; i < 6; i++)
	{
		materialDiffuse[i] = new Array();
		for(var j = 0; j < 4; j++)
		{
			materialDiffuse[i][j] = new Array();
		}
	}
	
	materialSpecular = new Array();
	for(var i = 0; i < 6; i++)
	{
		materialSpecular[i] = new Array();
		for(var j = 0; j < 4; j++)
		{
			materialSpecular[i][j] = new Array();
		}
	}
	
	materialShiness = new Array();
	for(var i = 0; i < 6; i++)
	{
		materialShiness[i] = new Array();
	}
	// Material values will be unique per sphere hence declare an 2d-array of it
	// 1st Colume, Gems
	// 1st Sphere -> Emerald
	materialAmbient[0][0][0] = 0.0215;
	materialAmbient[0][0][1] = 0.1745;
	materialAmbient[0][0][2] = 0.0215;
	
	materialDiffuse[0][0][0] = 0.07568;
	materialDiffuse[0][0][1] = 0.61424;
	materialDiffuse[0][0][2] = 0.07568;
	
	materialSpecular[0][0][0] = 0.633;
	materialSpecular[0][0][1] = 0.727811;
	materialSpecular[0][0][2] = 0.633;
	materialShiness[0][0] = 0.6 * 128;
	// 2nd Sphere -> Emerald
	materialAmbient[1][0][0] = 0.135;
	materialAmbient[1][0][1] = 0.2225;
	materialAmbient[1][0][2] = 0.1575;
	materialDiffuse[1][0][0] = 0.54;
	materialDiffuse[1][0][1] = 0.89;
	materialDiffuse[1][0][2] = 0.63;
	materialSpecular[1][0][0] = 0.316228;
	materialSpecular[1][0][1] = 0.316228;
	materialSpecular[1][0][2] = 0.316228;
	materialShiness[1][0] = 0.1 * 128;
	// 3rd Sphere -> Obsidian
	materialAmbient[2][0][0] = 0.05375;
	materialAmbient[2][0][1] = 0.05;
	materialAmbient[2][0][2] = 0.06625;
	
	materialDiffuse[2][0][0] = 0.18275;
	materialDiffuse[2][0][1] = 0.17;
	materialDiffuse[2][0][2] = 0.22525;

	materialSpecular[2][0][0] = 0.332741;
	materialSpecular[2][0][1] = 0.328634;
	materialSpecular[2][0][2] = 0.346435;

	materialShiness[2][0] = 0.3 * 128;
	//4th Sphere -> Pearl
	materialAmbient[3][0][0] = 0.25;
	materialAmbient[3][0][1] = 0.20725;
	materialAmbient[3][0][2] = 0.20725;

	materialDiffuse[3][0][0] = 1.0;
	materialDiffuse[3][0][1] = 0.829;
	materialDiffuse[3][0][2] = 0.829;

	materialSpecular[3][0][0] = 0.296648;
	materialSpecular[3][0][1] = 0.296648;
	materialSpecular[3][0][2] = 0.296648;

	materialShiness[3][0] = 0.088 * 128;
	//5th Sphere->Ruby
	materialAmbient[4][0][0] = 0.1745;
	materialAmbient[4][0][1] = 0.01175;
	materialAmbient[4][0][2] = 0.01175;

	materialDiffuse[4][0][0] = 0.61424;
	materialDiffuse[4][0][1] = 0.04136;
	materialDiffuse[4][0][2] = 0.04136;

	materialSpecular[4][0][0] = 0.727811;
	materialSpecular[4][0][1] = 0.626959;
	materialSpecular[4][0][2] = 0.626959;

	materialShiness[4][0] = 0.6 * 128;
	//6th Sphere->Turquoise
	materialAmbient[5][0][0] = 0.1;
	materialAmbient[5][0][1] = 0.18725;
	materialAmbient[5][0][2] = 0.1745;

	materialDiffuse[5][0][0] = 0.396;
	materialDiffuse[5][0][1] = 0.74151;
	materialDiffuse[5][0][2] = 0.69102;

	materialSpecular[5][0][0] = 0.297254;
	materialSpecular[5][0][1] = 0.30829;
	materialSpecular[5][0][2] = 0.306678;

	materialShiness[5][0] = 0.1 * 128;
	// 2nd Column : Metal
	// 1st Sphere-> Brass
	materialAmbient[0][1][0] = 0.329412;
	materialAmbient[0][1][1] = 0.223529;
	materialAmbient[0][1][2] = 0.027451;

	materialDiffuse[0][1][0] = 0.780392;
	materialDiffuse[0][1][1] = 0.568627;
	materialDiffuse[0][1][2] = 0.113725;

	materialSpecular[0][1][0] = 0.992157;
	materialSpecular[0][1][1] = 0.941176;
	materialSpecular[0][1][2] = 0.807843;

	materialShiness[0][1] = 0.21794872 * 128;
	// 2nd Sphere, Bronze
	materialAmbient[1][1][0] = 0.2125;
	materialAmbient[1][1][1] = 0.1275;
	materialAmbient[1][1][2] = 0.054;

	materialDiffuse[1][1][0] = 0.714;
	materialDiffuse[1][1][1] = 0.4284;
	materialDiffuse[1][1][2] = 0.18144;

	materialSpecular[1][1][0] = 0.393548;
	materialSpecular[1][1][1] = 0.271906;
	materialSpecular[1][1][2] = 0.166721;

	materialShiness[1][1] = 0.2 * 128;
	// 3rd Sphere, Chrome
	materialAmbient[2][1][0] = 0.25;
	materialAmbient[2][1][1] = 0.25;
	materialAmbient[2][1][2] = 0.25;

	materialDiffuse[2][1][0] = 0.4;
	materialDiffuse[2][1][1] = 0.4;
	materialDiffuse[2][1][2] = 0.4;

	materialSpecular[2][1][0] = 0.774597;
	materialSpecular[2][1][1] = 0.774597;
	materialSpecular[2][1][2] = 0.774597;

	materialShiness[2][1] = 0.6 * 128;
	// 4th Sphere, Copper
	materialAmbient[3][1][0] = 0.19125;
	materialAmbient[3][1][1] = 0.0735;
	materialAmbient[3][1][2] = 0.0225;

	materialDiffuse[3][1][0] = 0.7038;
	materialDiffuse[3][1][1] = 0.27048;
	materialDiffuse[3][1][2] = 0.0828;

	materialSpecular[3][1][0] = 0.256777;
	materialSpecular[3][1][1] = 0.137622;
	materialSpecular[3][1][2] = 0.086014;

	materialShiness[3][1] = 0.1 * 128;
	// 5th Sphere, Gold
	materialAmbient[4][1][0] = 0.24725;
	materialAmbient[4][1][1] = 0.1995;
	materialAmbient[4][1][2] = 0.0745;

	materialDiffuse[4][1][0] = 0.75164;
	materialDiffuse[4][1][1] = 0.60648;
	materialDiffuse[4][1][2] = 0.22648;

	materialSpecular[4][1][0] = 0.628281;
	materialSpecular[4][1][1] = 0.555802;
	materialSpecular[4][1][2] = 0.366065;

	materialShiness[4][1] = 0.4 * 128;
	// 6th Sphere, Silver
	materialAmbient[5][1][0] = 0.19225;
	materialAmbient[5][1][1] = 0.19225;
	materialAmbient[5][1][2] = 0.19225;

	materialDiffuse[5][1][0] = 0.50754;
	materialDiffuse[5][1][1] = 0.50754;
	materialDiffuse[5][1][2] = 0.50754;

	materialSpecular[5][1][0] = 0.508273;
	materialSpecular[5][1][1] = 0.508273;
	materialSpecular[5][1][2] = 0.508273;

	materialShiness[5][1] = 0.4 * 128;
	// 3rd Column : Plastic
	// 1st Sphere, Black
	materialAmbient[0][2][0] = 0.0;
	materialAmbient[0][2][1] = 0.0;
	materialAmbient[0][2][2] = 0.0;

	materialDiffuse[0][2][0] = 0.01;
	materialDiffuse[0][2][1] = 0.01;
	materialDiffuse[0][2][2] = 0.01;

	materialSpecular[0][2][0] = 0.50;
	materialSpecular[0][2][1] = 0.50;
	materialSpecular[0][2][2] = 0.50;

	materialShiness[0][2] = 0.25 * 128;
	// 2nd Sphere, Cyan
	materialAmbient[1][2][0] = 0.0;
	materialAmbient[1][2][1] = 0.1;
	materialAmbient[1][2][2] = 0.06;

	materialDiffuse[1][2][0] = 0.0;
	materialDiffuse[1][2][1] = 0.50980392;
	materialDiffuse[1][2][2] = 0.50980392;

	materialSpecular[1][2][0] = 0.50196078;
	materialSpecular[1][2][1] = 0.50196078;
	materialSpecular[1][2][2] = 0.50196078;

	materialShiness[1][2] = 0.25 * 128;
	// 3rd Sphere, Green
	materialAmbient[2][2][0] = 0.0;
	materialAmbient[2][2][1] = 0.0;
	materialAmbient[2][2][2] = 0.0;

	materialDiffuse[2][2][0] = 0.1;
	materialDiffuse[2][2][1] = 0.35;
	materialDiffuse[2][2][2] = 0.1;

	materialSpecular[2][2][0] = 0.45;
	materialSpecular[2][2][1] = 0.55;
	materialSpecular[2][2][2] = 0.45;

	materialShiness[2][2] = 0.25 * 128;
	// 4th Sphere, Red
	materialAmbient[3][2][0] = 0.0;
	materialAmbient[3][2][1] = 0.0;
	materialAmbient[3][2][2] = 0.0;

	materialDiffuse[3][2][0] = 0.5;
	materialDiffuse[3][2][1] = 0.0;
	materialDiffuse[3][2][2] = 0.0;

	materialSpecular[3][2][0] = 0.7;
	materialSpecular[3][2][1] = 0.6;
	materialSpecular[3][2][2] = 0.6;

	materialShiness[3][2] = 0.25 * 128;
	// 5th Sphere, White
	materialAmbient[4][2][0] = 0.0;
	materialAmbient[4][2][1] = 0.0;
	materialAmbient[4][2][2] = 0.0;

	materialDiffuse[4][2][0] = 0.55;
	materialDiffuse[4][2][1] = 0.55;
	materialDiffuse[4][2][2] = 0.55;

	materialSpecular[4][2][0] = 0.70;
	materialSpecular[4][2][1] = 0.70;
	materialSpecular[4][2][2] = 0.70;

	materialShiness[4][2] = 0.25 * 128;
	// 6th Sphere, Yellow
	materialAmbient[5][2][0] = 0.0;
	materialAmbient[5][2][1] = 0.0;
	materialAmbient[5][2][2] = 0.0;

	materialDiffuse[5][2][0] = 0.5;
	materialDiffuse[5][2][1] = 0.5;
	materialDiffuse[5][2][2] = 0.0;

	materialSpecular[5][2][0] = 0.60;
	materialSpecular[5][2][1] = 0.60;
	materialSpecular[5][2][2] = 0.50;

	materialShiness[5][2] = 0.25 * 128;
	//4th Column : Rubber
	// 1st Sphere, Black
	materialAmbient[0][3][0] = 0.02;
	materialAmbient[0][3][1] = 0.02;
	materialAmbient[0][3][2] = 0.02;

	materialDiffuse[0][3][0] = 0.01;
	materialDiffuse[0][3][1] = 0.01;
	materialDiffuse[0][3][2] = 0.01;

	materialSpecular[0][3][0] = 0.40;
	materialSpecular[0][3][1] = 0.40;
	materialSpecular[0][3][2] = 0.40;

	materialShiness[0][3] = 0.078125 * 128;
	// 2nd Sphere, Cyan
	materialAmbient[1][3][0] = 0.0;
	materialAmbient[1][3][1] = 0.05;
	materialAmbient[1][3][2] = 0.05;

	materialDiffuse[1][3][0] = 0.4;
	materialDiffuse[1][3][1] = 0.5;
	materialDiffuse[1][3][2] = 0.5;

	materialSpecular[1][3][0] = 0.04;
	materialSpecular[1][3][1] = 0.7;
	materialSpecular[1][3][2] = 0.7;

	materialShiness[1][3] = 0.078125 * 128;
	// 3rd Sphere, Green
	materialAmbient[2][3][0] = 0.0;
	materialAmbient[2][3][1] = 0.05;
	materialAmbient[2][3][2] = 0.0;

	materialDiffuse[2][3][0] = 0.4;
	materialDiffuse[2][3][1] = 0.5;
	materialDiffuse[2][3][2] = 0.4;

	materialSpecular[2][3][0] = 0.04;
	materialSpecular[2][3][1] = 0.7;
	materialSpecular[2][3][2] = 0.04;

	materialShiness[2][3] = 0.078125 * 128;
	// 4th Sphere, Red
	materialAmbient[3][3][0] = 0.05;
	materialAmbient[3][3][1] = 0.0;
	materialAmbient[3][3][2] = 0.0;

	materialDiffuse[3][3][0] = 0.5;
	materialDiffuse[3][3][1] = 0.4;
	materialDiffuse[3][3][2] = 0.4;

	materialSpecular[3][3][0] = 0.7;
	materialSpecular[3][3][1] = 0.04;
	materialSpecular[3][3][2] = 0.04;

	materialShiness[3][3] = 0.078125 * 128;
	// 5th Sphere, White
	materialAmbient[4][3][0] = 0.05;
	materialAmbient[4][3][1] = 0.05;
	materialAmbient[4][3][2] = 0.05;
	
	materialDiffuse[4][3][0] = 0.5;
	materialDiffuse[4][3][1] = 0.5;
	materialDiffuse[4][3][2] = 0.5;
	
	materialSpecular[4][3][0] = 0.7;
	materialSpecular[4][3][1] = 0.7;
	materialSpecular[4][3][2] = 0.7;
	
	materialShiness[4][3] = 0.078125 * 128;
	// 6th Sphere, Yellow
	materialAmbient[5][3][0] = 0.05;
	materialAmbient[5][3][1] = 0.05;
	materialAmbient[5][3][2] = 0.0;
	
	materialDiffuse[5][3][0] = 0.5;
	materialDiffuse[5][3][1] = 0.5;
	materialDiffuse[5][3][2] = 0.4;
	
	materialSpecular[5][3][0] = 0.70;
	materialSpecular[5][3][1] = 0.70;
	materialSpecular[5][3][2] = 0.04;
	
	materialShiness[5][3] = 0.078125 * 128;
	
	init_shaders(perFragment_VertexShaderSourceCode, perFragment_FragmentShaderSourceCode);
	
	// vertices, colors, shader attribs, vbo, vao initializations
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);	

	// set clear color
	gl.clearColor(0.5, 0.5, 0.5, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	perspectiveProjectionMatrix = mat4.create();
	modelMatrix = mat4.create();
	viewMatrix = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	g_width = canvas.width;
	g_height = canvas.height;
}

function draw24sphere()
{
	var height = g_height;
	var width = 0;
	
	for (var colume = 0; colume < 4; colume++)
	{
		height -= (g_height/6);

		for (var row = 0; row < 6; row++)
		{
			gl.viewportWidth = g_width / 4;
			gl.viewportHeight = g_height / 6;
			
			gl.viewport(width, height, g_width / 4, g_height / 6);
			
			mat4.identity(perspectiveProjectionMatrix);
			mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(g_width / 4) / parseFloat(g_height / 6), 0.1, 100.0);
			
			mat4.identity(modelMatrix);
			mat4.identity(viewMatrix);
			mat4.translate(viewMatrix, viewMatrix, [0.0, 0.0, -5.0]);
	
			gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
			gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
			gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
			
			if(bLKeyPressed)
			{	
				gl.uniform1i(keyLPressedUniform, 1);	
				
				gl.uniform3fv(lightAmbientUniform0, lightAmbient0);		
				gl.uniform3fv(lightDiffuseUniform0, lightDiffuse0);	
				gl.uniform3fv(lightSpecularUniform0, lightSpecular0);
				
				if (1 == keyPress)
				{
					lightPosition0[0] = 0.0;
					lightPosition0[1] = Math.cos(light0_rotationX) + Math.sin(light0_rotationX);
					lightPosition0[2] = Math.cos(light0_rotationX) - Math.sin(light0_rotationX);
					lightPosition0[3] = 1.0;
				}
				else if (2 == keyPress)
				{
					lightPosition0[0] = Math.cos(light0_rotationY) - Math.sin(light0_rotationY);
					lightPosition0[1] = 0.0;
					lightPosition0[2] = Math.cos(light0_rotationY) + Math.sin(light0_rotationY);
					lightPosition0[3] = 1.0;
				}
				else if (3 == keyPress)
				{
					lightPosition0[0] = Math.cos(light0_rotationZ) - Math.sin(light0_rotationZ);
					lightPosition0[1] = Math.cos(light0_rotationZ) + Math.sin(light0_rotationZ);
					lightPosition0[2] = 0.0;
					lightPosition0[3] = 1.0;
				}
				gl.uniform4fv(lightPositionUniform0, lightPosition0);
						
				gl.uniform3fv(materialAmbientUniform, materialAmbient[row][colume]);		
				gl.uniform3fv(materialDiffuseUniform, materialDiffuse[row][colume]);
				gl.uniform3fv(materialSpecularUniform, materialSpecular[row][colume]);	
				gl.uniform1f(materialShinessUniform, materialShiness[row][colume]);
				
			} // End of if
			else
			{
				gl.uniform1i(keyLPressedUniform, 0);
			} // End Of Else
				
			sphere.draw();
			
			height -= (g_height / 6);
	
		} // Inner For Loop
		
		width += (g_width / 4);
		height = g_height;
	} // Outer For Loop
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	draw24sphere();
	
	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize_shader()
{
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function uninitialize()
{
	// code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}
		
	uninitialize_shader();
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 27: 
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
		
		case 70: // for 'f' or 'F'
			uninitialize_shader();
			init_shaders(perFragment_VertexShaderSourceCode, perFragment_FragmentShaderSourceCode);
		break;
		
		case 86: // for 'v' or 'V'
			uninitialize_shader();
			init_shaders(perVertex_VertexShaderSourceCode, perVertex_FragmentShaderSourceCode);
		break;
		
		case 88: // For X
			keyPress = 1;
			light0_rotationX = 0.0;
		break;
		
		case 89: // For Y
			keyPress = 2;
			light0_rotationY = 0.0;
		break;
		
		case 90: // For Z
			keyPress = 3;
			light0_rotationZ = 0.0;
		break;

		case 69: // for 'e' or 'E'
			toggleFullScreen();
		break;
		
		case 76: // for 'L' or 'l'
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else
				bLKeyPressed = false;
		break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	// code
	
	if (1 == keyPress)
	{
		if (light0_rotationX >= 360.0) 
		{
			  light0_rotationX = 0.0;
		}
		light0_rotationX += 0.05;
	}
	else if (2 == keyPress)
	{
		if (light0_rotationY >= 360.0)
		{
			  light0_rotationY = 0.0;
		}
		light0_rotationY += 0.05;
	}
	else if (3 == keyPress)
	{
		if (light0_rotationZ >= 360.0)
		{
			  light0_rotationZ = 0.0;
		}
		light0_rotationZ += 0.05;
	}
}