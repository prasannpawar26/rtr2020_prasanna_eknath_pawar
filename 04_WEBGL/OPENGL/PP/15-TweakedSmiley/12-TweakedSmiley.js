//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// shader program related variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_rectangle;
var vbo_position;
var vbo_texture;

var smile_texture = 0;

// uniforms
var mvpUniform;
var uniform_texture0_sampler;

var perspectiveProjectionMatrix;

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

var rectangleTexcoords = new Float32Array([
		0.0, 1.0,
		1.0, 1.0, 
		1.0, 0.0, 
		0.0, 0.0,
	]);
	
var keyPressed = 0;

// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}

function init()
{
	// code	
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec2 vTexture0_Coord;" +
	"out vec2 out_texture0_coord;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_texture0_coord = vTexture0_Coord;" +
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	// fragment shader
	var fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"in vec2 out_texture0_coord;" +
	"uniform highp sampler2D u_texture0_sampler;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
		"FragColor = texture(u_texture0_sampler, out_texture0_coord);" +
	"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	// Load Smiley Texture
	smile_texture = gl.createTexture();
	smile_texture.image = new Image();
	smile_texture.image.src = "smiley.png";
	smile_texture.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, smile_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, smile_texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	// get MVP uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");

	// vertices, colors, shader attribs, vbo, vao initializations

	var rectangleVertices = new Float32Array([
		1.0, 1.0, 0.0, -1.0, 1.0, 0.0, -1.0, -1.0, 0.0, 1.0, -1.0, 0.0
	]);
	
	// Rectangle
	vao_rectangle = gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle);

	vbo_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	vbo_texture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
	gl.bufferData(gl.ARRAY_BUFFER, 0, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	// initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindTexture(gl.TEXTURE_2D, smile_texture);
	gl.uniform1i(uniform_texture0_sampler, 0);
	
	if (0 == keyPressed) {
    // TODO : Need To be White Rectangle, But As Of know Its Full Face Smiley
    rectangleTexcoords[0] = 0.0;
    rectangleTexcoords[1] = 1.0;
    rectangleTexcoords[2] = 1.0;
    rectangleTexcoords[3] = 1.0;
    rectangleTexcoords[4] = 1.0;
    rectangleTexcoords[5] = 0.0;
    rectangleTexcoords[6] = 0.0;
    rectangleTexcoords[7] = 0.0;
  } else if (1 == keyPressed) {
    rectangleTexcoords[0] = 0.5;
    rectangleTexcoords[1] = 0.5;
    rectangleTexcoords[2] = 0.0;
    rectangleTexcoords[3] = 0.5;
    rectangleTexcoords[4] = 0.0;
    rectangleTexcoords[5] = 0.0;
    rectangleTexcoords[6] = 0.5;
    rectangleTexcoords[7] = 0.0;
  } else if (2 == keyPressed) {
    rectangleTexcoords[0] = 0.0;
    rectangleTexcoords[1] = 1.0;
    rectangleTexcoords[2] = 1.0;
    rectangleTexcoords[3] = 1.0;
    rectangleTexcoords[4] = 1.0;
    rectangleTexcoords[5] = 0.0;
    rectangleTexcoords[6] = 0.0;
    rectangleTexcoords[7] = 0.0;
  } else if (3 == keyPressed) {
    rectangleTexcoords[0] = 0.0;
    rectangleTexcoords[1] = 2.0;
    rectangleTexcoords[2] = 2.0;
    rectangleTexcoords[3] = 2.0;
    rectangleTexcoords[4] = 2.0;
    rectangleTexcoords[5] = 0.0;
    rectangleTexcoords[6] = 0.0;
    rectangleTexcoords[7] = 0.0;
  } else if (4 == keyPressed) {
    rectangleTexcoords[0] = 0.5;
    rectangleTexcoords[1] = 0.5;
    rectangleTexcoords[2] = 0.5;
    rectangleTexcoords[3] = 0.5;
    rectangleTexcoords[4] = 0.5;
    rectangleTexcoords[5] = 0.5;
    rectangleTexcoords[6] = 0.5;
    rectangleTexcoords[7] = 0.5;
  }
  
	gl.bindVertexArray(vao_rectangle);
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
	gl.bufferData(gl.ARRAY_BUFFER, rectangleTexcoords, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);
		
	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// code
	
	if(smile_texture)
	{
		gl.deleteTexture(smile_texture);
		smile_texture = 0;
	}
	
	if(vbo_texture)
	{
		gl.deleteBuffer(vbo_texture);
		vbo_texture = null;
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;
	}
	
	if(vao_rectangle)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 70: // for 'f' or 'F'
			toggleFullScreen();
			break;

		case 27: // Escape
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
			break;
			
		case 48:
          keyPressed = 0;
		  console.log("RTR: keyPressed = 0\n");
          break;

        case 49:
          keyPressed = 1;
		  console.log("RTR: keyPressed = 1\n");
          break;

        case 50:
          keyPressed = 2;
		  console.log("RTR: keyPressed = 2\n");
          break;

        case 51:
          keyPressed = 3;
		  console.log("RTR: keyPressed = 3\n");
          break;

        case 52:
          keyPressed = 4;
          break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	// code
}