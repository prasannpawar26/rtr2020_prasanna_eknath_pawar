//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// shader program related variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;

// Uniforms
var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var lightAmbientUniform0;
var lightDiffuseUniform0;
var lightSpecularUniform0;
var lightPositionUniform0;

var lightAmbientUniform1;
var lightDiffuseUniform1;
var lightSpecularUniform1;
var lightPositionUniform1;

var lightAmbientUniform2;
var lightDiffuseUniform2;
var lightSpecularUniform2;
var lightPositionUniform2;

var materialAmbientUniform;
var materialDiffuseUniform;
var materialSpecularUniform;
var materialShinessUniform;

var keyLPressedUniform;

// light configuration
var lightAmbient0 = [0.0, 0.0, 0.0];
var lightDiffuse0 = [1.0, 0.0, 0.0];
var lightSpecular0 = [1.0, 0.0, 0.0];
var lightPosition0 = [0.0, 0.0, 0.0, 1.0];

var lightAmbient1 = [0.0, 0.0, 0.0];
var lightDiffuse1 = [0.0, 1.0, 0.0];
var lightSpecular1 = [0.0, 1.0, 0.0];
var lightPosition1 = [0.0, 0.0, 0.0, 1.0];

var lightAmbient2 = [0.0, 0.0, 0.0];
var lightDiffuse2 = [0.0, 0.0, 1.0];
var lightSpecular2 = [0.0, 0.0, 1.0];
var lightPosition2 = [0.0, 0.0, 0.0, 1.0];

var light0_angle = 0.0;
var light1_angle = 0.0;
var light2_angle = 0.0;

// material configuration
var materialAmbient = [0.0, 0.0, 0.0];
var materialDiffuse = [1.0, 1.0, 1.0];
var materialSpecular = [1.0, 1.0, 1.0];
var materialShiness = 50.0;

// matrix
var perspectiveProjectionMatrix;
var modelMatrix;
var	viewMatrix;

// light toggled variable
var bLKeyPressed = false;


// per vertex - vertex shader
var perVertex_VertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	
	"struct matrices {" +
		"mat4 model_matrix;" +
		"mat4 view_matrix;" +
		"mat4 projection_matrix;" +
	"};" +
	"uniform matrices u_matrices;" +
	
	"uniform int u_key_L_pressed;" +

	"struct light_configuration {" +
		"vec3 light_ambient;" +
		"vec3 light_diffuse;" +
		"vec3 light_specular;" +
		"vec4 light_position;" +
	"};" +
	"uniform light_configuration u_light_configuration0;" +
	"uniform light_configuration u_light_configuration1;" +
	"uniform light_configuration u_light_configuration2;" +

	"struct material_configuration {" +
		"vec3 material_ambient;" +
		"vec3 material_diffuse;" +
		"vec3 material_specular;" +
		"float material_shiness;" +
	"};" +
	"uniform material_configuration u_material_configuration;" +
	
	"vec3 light_direction0;" +
	"vec3 light_direction1;" +
	"vec3 light_direction2;" +

	"vec3 tranformation_matrix;" +
	"vec3 viewer_vector;" +
	
	"out vec3 phong_ads_light;" +
	
	"void main(void)" +
	"{" +
		"if(1 == u_key_L_pressed)" +
		"{" +
			"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +

			"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" +
			"viewer_vector = vec3(-eye_coordinates);" +
			"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
			"vec3 viewer_vector_normal = normalize(viewer_vector);" +

			"light_direction0 = vec3(u_light_configuration0.light_position - eye_coordinates);" +
			"vec3 light_direction_normalize0 = normalize(light_direction0);" +
			"vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" +
			"vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +
			
			"light_direction1 = vec3(u_light_configuration1.light_position - eye_coordinates);" +
			"vec3 light_direction_normalize1 = normalize(light_direction1);" +
			"vec3 reflection_vector1 = reflect(-light_direction_normalize1, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction1 = max(dot(light_direction_normalize1, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient1 = u_light_configuration1.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse1 = u_light_configuration1.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction1;" +
			"vec3 specular1 = u_light_configuration1.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector1, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +
			
			"light_direction2 = vec3(u_light_configuration2.light_position - eye_coordinates);" +
			"vec3 light_direction_normalize2 = normalize(light_direction2);" +
			"vec3 reflection_vector2 = reflect(-light_direction_normalize2, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction2 = max(dot(light_direction_normalize2, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient2 = u_light_configuration2.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse2 = u_light_configuration2.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction2;" +
			"vec3 specular2 = u_light_configuration2.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector2, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +

			"phong_ads_light = ambient0 + ambient1 + ambient2 + diffuse0 + diffuse1 + diffuse2 + specular0 + specular1 + specular2;" + 
		"}" +
		"else" +
		"{" +
			"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
		"}" +

		  "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
	  "}";

// per vertex - fragment shader
var perVertex_FragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"in vec3 phong_ads_light;" +
	"uniform int u_key_L_pressed;" +
	"out vec4 FragColor;" +
	
	"void main(void)" +
	"{" +
		"FragColor = vec4(phong_ads_light, 1.0);" +
	"}";

// per fragment - vertex shader
var perFragment_VertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	"struct matrices {" +
		"mat4 model_matrix;" +
		"mat4 view_matrix;" +
		"mat4 projection_matrix;" +
	"};" +
	"uniform matrices u_matrices;" +
	
	"uniform int u_key_L_pressed;" +

	"struct light_configuration {" +
		"vec3 light_ambient;" +
		"vec3 light_diffuse;" +
		"vec3 light_specular;" +
		"vec4 light_position;" +
	"};" +
	"uniform light_configuration u_light_configuration0;" +
	"uniform light_configuration u_light_configuration1;" +
	"uniform light_configuration u_light_configuration2;" +

	"out vec3 light_direction0;" +
	"out vec3 light_direction1;" +
	"out vec3 light_direction2;" +

	"out vec3 tranformation_matrix;" +
	"out vec3 viewer_vector;" +
	"void main(void)" +
	"{" +
		"if(1 == u_key_L_pressed)" +
		"{" +
			"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +

			"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" +
			"viewer_vector = vec3(-eye_coordinates);" +

			"light_direction0 = vec3(u_light_configuration0.light_position - eye_coordinates);" +
			"light_direction1 = vec3(u_light_configuration1.light_position - eye_coordinates);" +
			"light_direction2 = vec3(u_light_configuration2.light_position - eye_coordinates);" +

		  "}" +

		  "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
	  "}";

// per fragment - fragment shader
var perFragment_FragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;" +
	"precision lowp int;" +
	"in vec3 light_direction0;" +
	"in vec3 light_direction1;" +
	"in vec3 light_direction2;" +
	"in vec3 tranformation_matrix;" +
	"in vec3 viewer_vector;" +

	"uniform int u_key_L_pressed;" +
	
	"struct light_configuration {" +
		"vec3 light_ambient;" +
		"vec3 light_diffuse;" +
		"vec3 light_specular;" +
		"vec4 light_position;" +
	"};" +
	"uniform light_configuration u_light_configuration0;" +
	"uniform light_configuration u_light_configuration1;" +
	"uniform light_configuration u_light_configuration2;" +

	"struct material_configuration {" +
		"vec3 material_ambient;" +
		"vec3 material_diffuse;" +
		"vec3 material_specular;" +
		"float material_shiness;" +
	"};" +
	"uniform material_configuration u_material_configuration;" +

	"out vec4 FragColor;" +
	"vec3 phong_ads_light;" +
	"void main(void)" +
	"{" +
		"if(1 == u_key_L_pressed)" +
		"{" +
			"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
			"vec3 viewer_vector_normal = normalize(viewer_vector);" +

			"vec3 light_direction_normalize0 = normalize(light_direction0);" +
			"vec3 reflection_vector0 = reflect(-light_direction_normalize0, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction0 = max(dot(light_direction_normalize0, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" +
			"vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +

			"vec3 light_direction_normalize1 = normalize(light_direction1);" +
			"vec3 reflection_vector1 = reflect(-light_direction_normalize1, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction1 = max(dot(light_direction_normalize1, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient1 = u_light_configuration1.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse1 = u_light_configuration1.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction1;" +
			"vec3 specular1 = u_light_configuration1.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector1, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +

			"vec3 light_direction_normalize2 = normalize(light_direction2);" +
			"vec3 reflection_vector2 = reflect(-light_direction_normalize2, tranformation_matrix_normalize);" +
			"float t_normal_dot_light_direction2 = max(dot(light_direction_normalize2, tranformation_matrix_normalize), 0.0f);" +
			"vec3 ambient2 = u_light_configuration2.light_ambient * u_material_configuration.material_ambient;" +
			"vec3 diffuse2 = u_light_configuration2.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction2;" +
			"vec3 specular2 = u_light_configuration2.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector2, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +

			"phong_ads_light = ambient0 + ambient1 + ambient2 + diffuse0 + diffuse1 + diffuse2 + specular0 + specular1 + specular2;" + 
		"}" +
		"else" +
		"{" +
			"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
		"}" +

		"FragColor = vec4(phong_ads_light, 1.0);" +
		"}";

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}

function init_shaders(vertexShaderSourceCode, fragmentShaderSourceCode)
{
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_matrices.model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_matrices.view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_matrices.projection_matrix");
	
	lightAmbientUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_ambient");
	lightDiffuseUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_diffuse");
	lightSpecularUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_specular");
	lightPositionUniform0 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration0.light_position");
	
	lightAmbientUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration1.light_ambient");
	lightDiffuseUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration1.light_diffuse");
	lightSpecularUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration1.light_specular");
	lightPositionUniform1 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration1.light_position");
	
	lightAmbientUniform2 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration2.light_ambient");
	lightDiffuseUniform2 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration2.light_diffuse");
	lightSpecularUniform2 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration2.light_specular");
	lightPositionUniform2 = gl.getUniformLocation(shaderProgramObject, "u_light_configuration2.light_position");
		
	materialAmbientUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_ambient");
	materialDiffuseUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_diffuse");
	materialSpecularUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_specular");
	materialShinessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_configuration.material_shiness");
	
	keyLPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_key_L_pressed");
}

function init()
{
	// code
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	init_shaders(perFragment_VertexShaderSourceCode, perFragment_FragmentShaderSourceCode);
	
	// vertices, colors, shader attribs, vbo, vao initializations
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);	

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	perspectiveProjectionMatrix = mat4.create();
	modelMatrix = mat4.create();
	viewMatrix = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.translate(viewMatrix, viewMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
		
	if(bLKeyPressed)
	{	
		gl.uniform1i(keyLPressedUniform, 1);	
		
		gl.uniform3fv(lightAmbientUniform0, lightAmbient0);		
		gl.uniform3fv(lightDiffuseUniform0, lightDiffuse0);	
		gl.uniform3fv(lightSpecularUniform0, lightSpecular0);
		lightPosition0[0] = Math.cos(light0_angle) - Math.sin(light0_angle);
		lightPosition0[1] = Math.cos(light0_angle) + Math.sin(light0_angle);
		lightPosition0[2] = 0.0;
		lightPosition0[3] = 1.0;
		gl.uniform4fv(lightPositionUniform0, lightPosition0);
		
		gl.uniform3fv(lightAmbientUniform1, lightAmbient1);		
		gl.uniform3fv(lightDiffuseUniform1, lightDiffuse1);	
		gl.uniform3fv(lightSpecularUniform1, lightSpecular1);		
		lightPosition1[0] = Math.cos(light1_angle) - Math.sin(light1_angle);
		lightPosition1[2] = Math.cos(light1_angle) + Math.sin(light1_angle);
		lightPosition1[1] = 0.0;
		lightPosition1[3] = 1.0;
		gl.uniform4fv(lightPositionUniform1, lightPosition1);
		
		gl.uniform3fv(lightAmbientUniform2, lightAmbient2);		
		gl.uniform3fv(lightDiffuseUniform2, lightDiffuse2);	
		gl.uniform3fv(lightSpecularUniform2, lightSpecular2);
		lightPosition2[2] = Math.cos(light2_angle) - Math.sin(light2_angle);
		lightPosition2[1] = Math.cos(light2_angle) + Math.sin(light2_angle);
		lightPosition2[0] = 0.0;
		lightPosition2[3] = 1.0;
		gl.uniform4fv(lightPositionUniform2, lightPosition2);
		
		gl.uniform3fv(materialAmbientUniform, materialAmbient);		
		gl.uniform3fv(materialDiffuseUniform, materialDiffuse);
		gl.uniform3fv(materialSpecularUniform, materialSpecular);	
		gl.uniform1f(materialShinessUniform, materialShiness);
	}
	else
	{
		gl.uniform1i(keyLPressedUniform, 0);
	}
	
	sphere.draw();
	
	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize_shader()
{
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function uninitialize()
{
	// code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}
		
	uninitialize_shader();
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 27: 
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
		
		case 70: // for 'f' or 'F'
			uninitialize_shader();
			init_shaders(perFragment_VertexShaderSourceCode, perFragment_FragmentShaderSourceCode);
		break;
		
		case 86: // for 'v' or 'V'
			uninitialize_shader();
			init_shaders(perVertex_VertexShaderSourceCode, perVertex_FragmentShaderSourceCode);
		break;

		case 69: // for 'e' or 'E'
			toggleFullScreen();
		break;
		
		case 76: // for 'L' or 'l'
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else
				bLKeyPressed = false;
		break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	// code
	if (light0_angle >= 360.0) {
			light0_angle = 0.0;
		}
		light0_angle += 0.01;

		if (light1_angle >= 360.0) {
			light1_angle = 0.0;
		}
		light1_angle += 0.01;

		if (light2_angle >= 360.0) {
			light2_angle = 0.0;
		}
		light2_angle += 0.01;
}