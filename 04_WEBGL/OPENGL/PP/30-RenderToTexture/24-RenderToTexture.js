//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


// vertex shader
var rtt_vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"precision mediump int;"+
	"in vec4 vPosition;"+
    "in vec2 vTexCoord;" +
    "uniform mat4 u_mvp_matrix;" +
    "out vec2 out_texcoord;" +
    "void main(void)" +
    "{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
    "out_texcoord = vTexCoord;" +
    "}";
	
// fragment shader
var rtt_fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"precision mediump int;"+
	"in vec2 out_texcoord;" +
    "uniform sampler2D u_sampler;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
		"FragColor = texture(u_sampler, out_texcoord);" +
    "}";
	
	// vertex shader
var program2_vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"precision mediump int;"+
	"in vec4 vPosition;"+
    "in vec2 vTexCoord;" +
    "uniform mat4 u_mvp_matrix;" +
    "out vec2 out_texcoord;" +
    "void main(void)" +
    "{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
    "out_texcoord = vTexCoord;" +
    "}";
	
// fragment shader
var program2_fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;"+
	"precision mediump int;"+
	"in vec2 out_texcoord;" +
    "uniform sampler2D u_sampler;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
		"FragColor = texture(u_sampler, out_texcoord);" +
    "}";

// Frame Buffers 
var fbo;
var render_buffer_texture;
var render_buffer_depth;

var modelViewMatrix;
var modelViewProjectionMatrix;
var translateMatrix;
var rotationMatrix;
var perspectiveProjectionMatrix;

// Program 1 -> Used For FBO (Render To Texture)
var rtt_VertexShaderObject;
var rtt_FragmentShaderObject;
var rtt_ShaderProgramObject;

var rtt_vao_cube;
var rtt_vbo_cube_vertex;
var rtt_vbo_cube_texture;
var rtt_rotation_angle_cube;
var rtt_texture_kundali = 0;
var rtt_cubeRotation = 0.0;

var rtt_mvpUniform;
var rtt_sampler_uniform;

// Program 2
var program2_VertexShaderObject;
var program2_FragmentShaderObject;
var program2_ShaderProgramObject;

var program2_vao_cube;
var program2_vbo_cube_vertex;
var program2_vbo_cube_texture;
var program2_rotation_angle_cube;
var program2_texture = 0;
var program2_cubeRotation = 0.0;

var program2_mvpUniform;
var program2_sampler_uniform;


// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}


function inner_fbo_initialize(width, height)
{	
	fbo = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);
	
	render_buffer_texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, render_buffer_texture);
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, render_buffer_texture, 0);
	
	gl.bindTexture(gl.TEXTURE_2D, null);
	
	// depth 
	render_buffer_depth = gl.createRenderbuffer();
	gl.bindRenderbuffer(gl.RENDERBUFFER, render_buffer_depth);
	
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH24_STENCIL8, width, height);
	
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT,
						gl.RENDERBUFFER, render_buffer_depth);

	gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	
	if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) != gl.FRAMEBUFFER_COMPLETE)
	{
		System.out.println("RTR: glCheckFramebufferStatus Failed");
		uninitialize();
		System.exit(0);
	}
		
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}



function init_program1()
{
	rtt_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(rtt_vertexShaderObject, rtt_vertexShaderSourceCode);
	gl.compileShader(rtt_vertexShaderObject);
	if(gl.getShaderParameter(rtt_vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(rtt_vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");


	// Fragment Shader
	rtt_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(rtt_fragmentShaderObject, rtt_fragmentShaderSourceCode);
	gl.compileShader(rtt_fragmentShaderObject);
	if(gl.getShaderParameter(rtt_fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(rtt_fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	rtt_shaderProgramObject = gl.createProgram();
	gl.attachShader(rtt_shaderProgramObject, rtt_vertexShaderObject);
	gl.attachShader(rtt_shaderProgramObject, rtt_fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(rtt_shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	
	gl.bindAttribLocation(rtt_shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");

	// linking
	gl.linkProgram(rtt_shaderProgramObject);
	if(!gl.getProgramParameter(rtt_shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(rtt_shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	// get MVP uniform location
	rtt_mvpUniform = gl.getUniformLocation(rtt_shaderProgramObject, "u_mvp_matrix");
	rtt_sampler_uniform = gl.getUniformLocation(rtt_shaderProgramObject, "u_sampler");

	// vertices, colors, shader attribs, vbo, vao initializations

	// For Cube
	var cubeVertices1 = new Float32Array([
		// front
		1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0,
		// back
		1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, -1.0,
		// right
		1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0,  1.0, -1.0, -1.0,
		// left
		-1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0,  -1.0, -1.0, -1.0,
		// top
		1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
		// bottom
		1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0,
	]);

	rtt_vao_cube = gl.createVertexArray();
	gl.bindVertexArray(rtt_vao_cube);

	rtt_vbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, rtt_vbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices1, gl.STATIC_DRAW);

	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	var cubeTexcoords1 = new Float32Array([
		// Top
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Bottom
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Front
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Back
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Left
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Right
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
	]);
	
	rtt_vbo_cube_texture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, rtt_vbo_cube_texture);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoords1, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);	
	
	// Load Vijay_Kundali Texture
	rtt_texture_kundali = gl.createTexture();
	
	rtt_texture_kundali.image = new Image();
	rtt_texture_kundali.image.src = "Vijay_Kundali.png";
	rtt_texture_kundali.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, rtt_texture_kundali);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, rtt_texture_kundali.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}

}

function init_program2()
{
	program2_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource( program2_vertexShaderObject,  program2_vertexShaderSourceCode);
	gl.compileShader( program2_vertexShaderObject);
	if(gl.getShaderParameter( program2_vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog( program2_vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	// Fragment Shader
	program2_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource( program2_fragmentShaderObject,  program2_fragmentShaderSourceCode);
	gl.compileShader( program2_fragmentShaderObject);
	if(gl.getShaderParameter( program2_fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog( program2_fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	 program2_shaderProgramObject = gl.createProgram();
	gl.attachShader( program2_shaderProgramObject,  program2_vertexShaderObject);
	gl.attachShader( program2_shaderProgramObject,  program2_fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation( program2_shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	
	gl.bindAttribLocation( program2_shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");

	// linking
	gl.linkProgram( program2_shaderProgramObject);
	if(!gl.getProgramParameter( program2_shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog( program2_shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	// get MVP uniform location
	program2_mvpUniform = gl.getUniformLocation( program2_shaderProgramObject, "u_mvp_matrix");
	program2_sampler_uniform = gl.getUniformLocation( program2_shaderProgramObject, "u_sampler");

	// vertices, colors, shader attribs, vbo, vao initializations

	// For Cube
	var cubeVertices2 = new Float32Array([
		// front
		1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0,
		// back
		1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, -1.0,
		// right
		1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0,  1.0, -1.0, -1.0,
		// left
		-1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0,  -1.0, -1.0, -1.0,
		// top
		1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
		// bottom
		1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0,
	]);

	program2_vao_cube = gl.createVertexArray();
	gl.bindVertexArray( program2_vao_cube);

	program2_vbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,  program2_vbo_cube_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices2, gl.STATIC_DRAW);

	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);

	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	var cubeTexcoords2 = new Float32Array([
		// Top
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Bottom
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Front
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Back
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Left
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
		// Right
		0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0,
	]);
	
	 program2_vbo_cube_texture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,  program2_vbo_cube_texture);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoords2, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);	
}


function init()
{
	// code
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	init_program1();
	init_program2();
	
	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// initialize projection matrix
	modelViewMatrix  = mat4.create();
	modelViewProjectionMatrix = mat4.create();
	translateMatrix = mat4.create();
	rotationMatrix = mat4.create();
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
	
	inner_fbo_initialize(canvas.width, canvas.height);
}

function draw1()
{
	// code
	gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);
	gl.useProgram(rtt_shaderProgramObject);
	gl.activeTexture(gl.TEXTURE0);
	gl.uniform1i(rtt_sampler_uniform, 0);
	
	gl.clearDepth(1.0);
	gl.clearColor(1.0, 1.0, 0.0, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT);
	
	gl.bindTexture(gl.TEXTURE_2D, rtt_texture_kundali);
		
	mat4.identity(translateMatrix);
	mat4.identity(rotationMatrix);
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(translateMatrix, translateMatrix, [0.0, 0.0, -4.0]);
	mat4.rotateX(rotationMatrix, rotationMatrix, degtored(rtt_cubeRotation));
	mat4.rotateY(rotationMatrix, rotationMatrix, degtored(rtt_cubeRotation));
	mat4.rotateZ(rotationMatrix, rotationMatrix, degtored(rtt_cubeRotation));
	
	mat4.multiply(modelViewMatrix, translateMatrix, rotationMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(rtt_mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(rtt_vao_cube);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);

	gl.bindTexture(gl.TEXTURE_2D, null);
	
	gl.useProgram(null);

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}

function draw2()
{
	gl.clearColor(0.0, 0.0, 1.0, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT);
	
	gl.useProgram(program2_shaderProgramObject);
	gl.activeTexture(gl.TEXTURE0);
	gl.uniform1i(program2_sampler_uniform, 0);

	gl.bindTexture(gl.TEXTURE_2D, render_buffer_texture);
		
	mat4.identity(translateMatrix);
	mat4.identity(rotationMatrix);
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
  
	mat4.translate(translateMatrix, translateMatrix, [0.0, 0.0, -4.0]);
	mat4.rotateX(rotationMatrix, rotationMatrix, degtored(program2_cubeRotation));
	mat4.rotateY(rotationMatrix, rotationMatrix, degtored(program2_cubeRotation));
	mat4.rotateZ(rotationMatrix, rotationMatrix, degtored(program2_cubeRotation));
	
	mat4.multiply(modelViewMatrix, translateMatrix, rotationMatrix);
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(program2_mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(program2_vao_cube);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);

	gl.useProgram(null);
	
	gl.bindTexture(gl.TEXTURE_2D, null);

}


function draw()
{
	draw1();
	draw2();
	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize1()
{
	// code

	if(rtt_vao_cube)
	{
		gl.deleteVertexArray(rtt_vao_cube);
		rtt_vao_cube = null;
	}

	if(rtt_vbo_cube_position)
	{
		gl.deleteBuffer(rtt_vbo_cube_position);
		rtt_vbo_cube_position = null;
	}

	if(rtt_shaderProgramObject)
	{
		if(rtt_fragmentShaderObject)
		{
			gl.detachShader(rtt_shaderProgramObject, rtt_fragmentShaderObject);
			gl.deleteShader(rtt_fragmentShaderObject);
			rtt_fragmentShaderObject = null;
		}

		if(rtt_vertexShaderObject)
		{
			gl.detachShader(rtt_shaderProgramObject, rtt_vertexShaderObject);
			gl.deleteShader(rtt_vertexShaderObject);
			rtt_vertexShaderObject = null;
		}

		gl.deleteProgram(rtt_shaderProgramObject);
		rtt_shaderProgramObject = null;
	}
}

function uninitialize2()
{
	// code

	if(program2_vao_cube)
	{
		gl.deleteVertexArray(program2_vao_cube);
		program2_vao_cube = null;
	}

	if(program2_vbo_cube_position)
	{
		gl.deleteBuffer(program2_vbo_cube_position);
		program2_vbo_cube_position = null;
	}

	if(program2_shaderProgramObject)
	{
		if(program2_fragmentShaderObject)
		{
			gl.detachShader(program2_shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(program2_fragmentShaderObject);
			program2_fragmentShaderObject = null;
		}

		if(program2_vertexShaderObject)
		{
			gl.detachShader(program2_shaderProgramObject, program2_vertexShaderObject);
			gl.deleteShader(program2_vertexShaderObject);
			program2_vertexShaderObject = null;
		}

		gl.deleteProgram(program2_shaderProgramObject);
		program2_shaderProgramObject = null;
	}
}

function uninitialize()
{
	uninitialize1();
	uninitialize2();
}


function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 70: // for 'f' or 'F'
			toggleFullScreen();
		break;

		case 27: // Escape
			uninitialize();
		// close our application's tab
		window.close(); // may not work in Firefox but works in safari and chrome.
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update1()
{

	if (360.0 < rtt_cubeRotation) {
		rtt_cubeRotation = 0.0;
	}
	rtt_cubeRotation += 1.0;
}

function update2()
{

	if (360.0 < program2_cubeRotation) {
		program2_cubeRotation = 0.0;
	}
	program2_cubeRotation += 0.50;
}

function update()
{

	update1();
	update2();
}