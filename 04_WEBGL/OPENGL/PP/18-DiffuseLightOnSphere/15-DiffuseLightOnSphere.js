//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// shader program related variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var sphere = null;

// uniforms
var modelViewMatrixUniform;
var projectionMatrixUniform;
var ldUniform;
var kdUniform;
var lightPositionUniform;
var LKeyPressedUniform;

// matrix
var perspectiveProjectionMatrix;

var bLKeyPressed = false;

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}

function init()
{
	// code
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec3 vNormal;" +
	"uniform mat4 u_model_view_matrix;" +
	"uniform mat4 u_projection_matrix;" +
	"uniform mediump int u_LKeyPressed;" +
	"uniform vec3 u_Ld;" +
	"uniform vec3 u_Kd;" +
	"uniform vec4 u_light_position;" +
	"out vec3 diffuse_light;" +
	"void main(void)" +
	"{" +
		"if(u_LKeyPressed == 1)" +
		"{" +
			"vec4 eyeCoordinates = u_model_view_matrix * vPosition;" +
			"vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);" +
			"vec3 light_source = normalize(vec3(u_light_position - eyeCoordinates));" +
			"diffuse_light = u_Ld * u_Kd * max(dot(light_source, tnorm), 0.0);" +
		"}" +
		
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" +
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	// fragment shader
	var fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;"+
	"in vec3 diffuse_light;" +
	"uniform int u_LKeyPressed;" +
	"out vec4 FragColor;" +
	
	"void main(void)" +
	"{" +
		"vec4 color;" +
		"if(u_LKeyPressed == 1)" +
		"{" +
			"color = vec4(diffuse_light, 1.0);" +
		"}" +
		"else" +
		"{" +
			"color = vec4(1.0, 1.0, 1.0, 1.0);" +
		"}" +
		
		"FragColor = color;" +
	"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(erro.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	// model view matrix
	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_view_matrix");
	// projection matrix
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");
	
	// light properties
	ldUniform = gl.getUniformLocation(shaderProgramObject,
	"u_Ld");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject,
	"u_light_position");
	// material
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
		
	// vertices, colors, shader attribs, vbo, vao initializations
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);	

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	if(bLKeyPressed)
	{
		gl.uniform1i(LKeyPressedUniform, 1);
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5);
		var lightPosition = [0.0, 0.0, 2.0, 1.0];
		gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform, 0);
	}
	
	var modelViewMatrix = mat4.create();
	
	// For Pyramid
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -6.0]);
		
	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	sphere.draw();
	
	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// code
	if(sphere)
	{
		sphere.deallocate();
		sphere = null;
	}
		
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 70: // for 'f' or 'F'
			toggleFullScreen();
		break;

		case 27: // Escape
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
		
		case 76: // for 'L' or 'l'
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else
				bLKeyPressed = false;
		break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	// code
}