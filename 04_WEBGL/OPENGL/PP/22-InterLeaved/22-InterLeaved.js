//global variables
var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width = 0;
var canvas_original_height = 0;

const WebGLMacros = // When Whole 'WebGLMacros' Is Const, All Inside It Are Automatically Const.
{
	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,
}

// shader program related variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

// vao, vbo variables
var vao_cube;
var vbo_cube_vcnt;

// animation variables
var rotation_angle_cube = 0.0;

// uniforms
var sampler_uniform;
var texture_marble;

// UNIFORMS
var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;
var lightAmbientUniform;
var lightDiffuseUniform;
var lightSpecularUniform;
var materialAmbientUniform;
var materialDiffuseUniform;
var materialSpecularUniform;
var materialShinessUniform;
var lightPositionUniform;
var keyLPressedUniform;

// light configuration
var lightAmbient = [0.250, 0.250, 0.250];
var lightDiffuse = [1.0, 1.0, 1.0];
var lightSpecular = [1.0, 1.0, 1.0];
var lightPosition = [100.0, 100.0, 100.0, 1.0];

// material configuration
var materialAmbient = [0.250, 0.250, 0.250];
var materialDiffuse = [1.0, 1.0, 1.0,];
var materialSpecular = [1.0, 1.0, 1.0];
var materialShiness = 128.0;

// matrix
var modelMatrix;
var viewMatrix;
var perspectiveProjectionMatrix;
var translateMatrix;
var rotationMatrix;


var bLKeyPressed = false;

// To Start Animation: To Have requestAnimationFrame() To Be called "Cross-Browser" Compatible
var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

// To Stop Animation : TO Have cancelAnimationFrame() To Be Called "Cross-Browser" Compatible
var cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || webkitCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


// onload function
function main()
{
	// get <canvas> element
	canvas = document.getElementById("RTR_CANVAS");
	if(!canvas)
		console.log("Obtaining Canvas failed\n");

	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	// register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

// browser independed code
function toggleFullScreen()
{
	// code

	var fullscreen_element = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

	// if not fullscreen
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();

		bFullScreen = true;
	}
	else // if laready full screen
	{
		if(document.exitFullScreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();

		bFullScreen = false;
	}
}

function init()
{
	// code
	// get WebGL2.0 Context
	gl = canvas.getContext("webgl2");
	if(null == gl)
	{
		console.log("Failed To Get The Rendering Context For WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode =
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"precision lowp int;" +
	
	"in vec4 vPosition;" +
      "in vec4 vColor;" +
      "in vec3 vNormal;" +
      "in vec2 vTexCoord;" +

      "uniform mat4 u_model_matrix;" +
      "uniform mat4 u_view_matrix;" +
      "uniform mat4 u_projection_matrix;" +
      "uniform int u_key_L_pressed;" +
      "uniform vec4 u_light_position;" +

      "out vec2 out_texcoord;" +
      "out vec4 out_color;" +
      "out vec3 light_direction;" +
      "out vec3 tranformation_matrix;" +
      "out vec3 viewer_vector;" +
      "void main(void)" +
      "{" +
          "if(1 == u_key_L_pressed)" +
          "{" +
              "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
              "tranformation_matrix = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
              "light_direction = vec3(u_light_position - eye_coordinates);" +
              "viewer_vector = vec3(-eye_coordinates);" +
          "}" +

          "out_texcoord = vTexCoord;" +
          "out_color = vColor;" +
          "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
      "}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
	var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : vertexShaderObject Compiled Successfully");

	// fragment shader
	var fragmentShaderSourceCode =
	"#version 300 es" +
	"\n" +
	
	"precision highp float;"+
	"precision lowp int;" +
	
	"in vec2 out_texcoord;" +
	
      "in vec4 out_color;" +
      "in vec3 light_direction;" +
      "in vec3 tranformation_matrix;" +
      "in vec3 viewer_vector;" +

      "uniform sampler2D u_sampler;" +
      "uniform int u_key_L_pressed;" +
      "uniform vec3 u_light_ambient;" +
      "uniform vec3 u_light_diffuse;" +
      "uniform vec3 u_light_specular;" +
      "uniform vec3 u_material_ambient;" +
      "uniform vec3 u_material_diffuse;" +
      "uniform vec3 u_material_specular;" +
      "uniform float u_material_shiness;" +

      "out vec4 FragColor;" +

      "vec4 text;" +
      "vec3 phong_ads_light;" +
      "void main(void)" +
      "{" +
          "if(1 == u_key_L_pressed)" +
          "{" +
              "vec3 light_direction_normalize = normalize(light_direction);" +
              "vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
              "vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" +
              "vec3 viewer_vector_normal = normalize(viewer_vector);" +
              "float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0);" +
              "vec3 ambient = u_light_ambient * u_material_ambient;" +
              "vec3 diffuse = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction;" +
              "vec3 specular = u_light_specular * u_material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0), u_material_shiness);" +
              "phong_ads_light= ambient + diffuse + specular;" +
          "}" +
          "else" +
          "{" +
              "phong_ads_light = vec3(1.0, 1.0, 1.0);"+
          "}" +

          "text = texture(u_sampler, out_texcoord);" +
          "FragColor = vec4((vec3(out_color * text) * phong_ads_light), 1.0);" +
      "}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : fragmentShaderObject Compiled Successfully");

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexCoord");

	// linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	console.log("RTR : shaderProgramObject Linked Successfully");

	// model view matrix
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
	
	lightAmbientUniform = gl.getUniformLocation(shaderProgramObject, "u_light_ambient");
	
	lightDiffuseUniform = gl.getUniformLocation(shaderProgramObject, "u_light_diffuse");
	
	lightSpecularUniform = gl.getUniformLocation(shaderProgramObject, "u_light_specular");
	
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	materialAmbientUniform = gl.getUniformLocation(shaderProgramObject, "u_material_ambient");
	
	materialDiffuseUniform = gl.getUniformLocation(shaderProgramObject, "u_material_diffuse");
	
	materialSpecularUniform = gl.getUniformLocation(shaderProgramObject, "u_material_specular");
	
	materialShinessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shiness");
	
	keyLPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_key_L_pressed");
	
	sampler_uniform = gl.getUniformLocation(shaderProgramObject, "u_sampler");
	
	// Load marble Texture
	texture_marble = gl.createTexture();
	texture_marble.image = new Image();
	texture_marble.image.src = "marble.png";
	texture_marble.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, texture_marble);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_marble.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
	
	// vertices, colors, shader attribs, vbo, vao initializations

	//vertex, color, normals, textcoord
    // TOP
	// BOTTOM
	// FRONT
	 // BACK
	 // RIGHT
	 // LEFT
	 
	var cube_vcnt = new Float32Array([
      1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
      -1.0, 1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0,
      -1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
      1.0,  1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
	  
      1.0,  -1.0, -1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
      -1.0, -1.0, -1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0,
      -1.0, -1.0, 1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 1.0,
      1.0,  -1.0, 1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0,
      
      1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
      -1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
      -1.0, -1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0,
      1.0, -1.0, 1.0, 0.0, 0.0, 1.0,  0.0, 0.0, 1.0, 0.0, 1.0,
     
      1.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
      -1.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
      -1.0, -1.0, -1.0, 0.0, 1.0, 1.0, 0.0, 0.0, -1.0, 1.0, 1.0,
      1.0, -1.0, -1.0, 0.0, 1.0, 1.0, 0.0,  0.0, -1.0, 0.0, 1.0,
      
      1.0, 1.0, -1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0,
      1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0,
      1.0, -1.0, 1.0, 1.0, 0.0, 1.0, 1.0,  0.0, 0.0, 1.0, 1.0,
      1.0, -1.0, -1.0, 1.0, 0.0, 1.0, 1.0,  0.0, 0.0, 0.0, 1.0,
      
      -1.0, 1.0, -1.0, 1.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
      -1.0, 1.0, 1.0, 1.0, 1.0, 0.0, -1.0, 0.0,  0.0, 1.0, 0.0,
      -1.0, -1.0, 1.0, 1.0, 1.0, 0.0, -1.0, 0.0, 0.0, 1.0, 1.0,
       -1.0, -1.0, -1.0, 1.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0,
	]);

	// Cube
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	
	vbo_cube_vcnt = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_vcnt);
	gl.bufferData(gl.ARRAY_BUFFER, cube_vcnt, gl.STATIC_DRAW);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 11 * 4, 0 * 4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 11 * 4, 3 * 4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 11 * 4, 6 * 4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 11 * 4, 9 * 4);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
  
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
			
	gl.bindVertexArray(null);

	// set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	modelMatrix = mat4.create();
	viewMatrix = mat4.create();
	perspectiveProjectionMatrix = mat4.create();
	translateMatrix  = mat4.create();
	rotationMatrix  = mat4.create();
}

function resize()
{
	// code
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function draw()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);
	
	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(translateMatrix);
	mat4.identity(rotationMatrix);


	mat4.translate(translateMatrix, translateMatrix, [0.0, 0.0, -3.0]);
	
	mat4.rotateX(rotationMatrix, rotationMatrix, degtored(rotation_angle_cube));
	mat4.rotateY(rotationMatrix, rotationMatrix, degtored(rotation_angle_cube));
	mat4.rotateZ(rotationMatrix, rotationMatrix, degtored(rotation_angle_cube));

	mat4.multiply(modelMatrix, translateMatrix, rotationMatrix);

	mat4.translate(viewMatrix, viewMatrix, [0.0, 0.0, -3.0]);
	

	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false,
                     perspectiveProjectionMatrix);
	if(bLKeyPressed)
	{
		gl.uniform1i(keyLPressedUniform, 1);
		gl.uniform3fv(lightAmbientUniform, lightAmbient);
		gl.uniform3fv(lightDiffuseUniform, lightDiffuse);
		gl.uniform3fv(lightSpecularUniform, lightSpecular);
		gl.uniform4fv(lightPositionUniform, lightPosition);
		gl.uniform3fv(materialAmbientUniform, materialAmbient);
		gl.uniform3fv(materialDiffuseUniform, materialDiffuse);
		gl.uniform3fv(materialSpecularUniform, materialSpecular);
		gl.uniform1f(materialShinessUniform, materialShiness);
	}
	else
	{
		gl.uniform1i(keyLPressedUniform, 0);
	}
		
	// For Pyramid
	
	gl.bindTexture(gl.TEXTURE_2D, texture_marble);
	
	gl.bindVertexArray(vao_cube);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);
	
	//gl.bindTexture(gl.TEXTURE_2D, null);
	
	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	// code
	if(vbo_cube_vcnt)
	{
		gl.deleteBuffer(vbo_cube_vcnt);
		vbo_cube_vcnt = null;
	}
	
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}

		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	// code
	switch(event.keyCode)
	{
		case 70: // for 'f' or 'F'
			toggleFullScreen();
		break;

		case 27: // Escape
			uninitialize();
			// close our application's tab
			window.close(); // may not work in Firefox but works in safari and chrome.
		break;
		
		case 76: // for 'L' or 'l'
			if(bLKeyPressed == false)
				bLKeyPressed = true;
			else
				bLKeyPressed = false;
		break;
	}
}

function mouseDown()
{
	// code
}

function degtored(degrees)
{
	return (degrees * Math.PI / 180);
}

function update()
{
	if (360.0 < rotation_angle_cube) {
		rotation_angle_cube = 0.0;
	}
	rotation_angle_cube += 1.0;
}