//
// html and js file should be in one directory
//
// onload function
function main()
{
	// step 1: Get canvas from DOM (i.e html)
	// var => runtime la type decide hoil right hind side nusar type (Type-Infurance)
	// document => inbuild variable (by DOM)
	var canvas = document.getElementById("PEP_CANVAS");
	if(!canvas)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		console.log("RTR: Obtaining Canvas Failed\n");
	}
	else
	{
		console.log("RTR: Obtaining Canvas Successed\n");
	}

	// step 2: Retrive width and height of canvas for a seak of information
	console.log("RTR: Canvas Width : "+canvas.width+" And Canvas Height: "+canvas.height+"\n"); // "+" Positional 
	
	// step 3: Get Drawing Context From Canvas
	var context = canvas.getContext("2d");
	if(!context)
	{
		 // Every Browser Has Its "Broswer(Java-Script) Console" -> There This Message Get Displayed.
		 // Enable This Explicitly
		console.log("RTR: Obtaining Context Failed\n");
	}
	else
	{
		console.log("RTR: Obtaining Context Successed\n");
	}

	// step 4: Fill Canvas width black color
	context.fillStyle="black"; //"#000000" => fillStyle Is Function Called As Setter Function/Also Called As Properties
	context.fillRect(0, 0, canvas.width, canvas.height);

	// step 5: Center The Text
	context.textAlign="center";
	context.textBaseline="middle"

	// step 6: Text String
	var str="Hello World !!!";

	// step 7: Text Font
	context.font="48px sans-serif";

	// step 8: Text Color
	context.fillStyle="white"; // #FFFFFF

	// step 9: Display Text
	context.fillText(str, canvas.width/2, canvas.height/2);
	
	//
	// window=> is inbuild variable
	//
	
	//register event handlers
	
	// 3rd parameter is false => bubbled propogation
	// capture propogation => sent to our event listener then to other
	// bubbled propogation =>  event sent from bottom to super class
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{
	alert("Key Is Pressed");
}

function mouseDown()
{
	alert("Mouse Is Clicked");
}
