//
//  GLESView.m
//  3DShapeAnimation
//
//  Created by Prasanna Pawar on 20/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTURE0,
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    BOOL isLightEnable;
    
    // shader variables
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
   
    GLuint vao_cube;
    GLuint vbo_cube_vcnt;
    
    GLuint sampler_uniform;
    GLuint texture_marble;
    
    GLfloat rotation_angle_cube;
    
    // Uniforms
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint lightAmbientUniform;
    GLuint lightDiffuseUniform;
    GLuint lightSpecularUniform;
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShinessUniform;
    GLuint lightPositionUniform;
    GLuint keyLPressedUniform;
    
    // Application Variables Associated With Uniforms
    float lightAmbient[4];
    float lightDiffuse[4];
    float lightSpecular[4];
    float lightPosition[4];
    
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShiness;
    
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 perspectiveProjectionMatrix;
    vmath::mat4 translateMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 scaleMatrix;
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    GLuint bmpTexture;
    
    NSString *textureFileNameWithPath=[[NSBundle mainBundle] pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        printf("Can Not Find Texture File\n");
        return 0;
    }
    
    CGImageRef cgImage = bmpImage.CGImage;
    
    int width = (int)CGImageGetWidth(cgImage);
    int height = (int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void *pixels = (void *)CFDataGetBytePtr(imageData);
    
    glGenTextures(1, &bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Set 1 rather than default 4 , for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    
    return bmpTexture;
}

- (id)initWithFrame:(CGRect)frame // initialize
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;// or [super layer]
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer]; // get data for render buffur from opengl(renderer)
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer: %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // Hard Code Initialiations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        isLightEnable=NO;
        
        lightAmbient[0] = 0.250f;
        lightAmbient[1] = 0.250f;
        lightAmbient[2] = 0.250f;
        lightAmbient[3] = 0.0f;
        
        lightDiffuse[0] = 1.0f;
        lightDiffuse[1] = 1.0f;
        lightDiffuse[2] = 1.0f;
        lightDiffuse[3] = 1.0f;
        
        lightSpecular[0] = 1.0f;
        lightSpecular[1] = 1.0f;
        lightSpecular[2] = 1.0f;
        lightSpecular[3] = 1.0f;
        
        lightPosition[0] = 100.0f;
        lightPosition[1] = 100.0f;
        lightPosition[2] = 100.0f;
        lightPosition[3] = 1.0f;
        
        
        materialAmbient[0] = 0.250f;
        materialAmbient[1] = 0.250f;
        materialAmbient[2] = 0.250f;
        materialAmbient[3] = 0.0f;
        
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        materialSpecular[0] =1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        materialShiness = 50.0f;
        
        
        //Shader Related Code Here
        
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "precision mediump int;" \
        
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "in vec3 vNormal;" \
        "in vec2 vTexCoord;" \
        
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_key_L_pressed;" \
        "uniform vec4 u_light_position;" \
        
        "out vec2 out_texcoord;" \
        "out vec4 out_color;" \
        "out vec3 light_direction;" \
        "out vec3 tranformation_matrix;" \
        "out vec3 viewer_vector;" \
        
        "void main(void)" \
        "{" \
            "if(1 == u_key_L_pressed)" \
            "{" \
                "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
                "tranformation_matrix = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
                "light_direction = vec3(u_light_position - eye_coordinates);" \
                "viewer_vector = vec3(-eye_coordinates);" \
            "}" \
        
            "out_texcoord = vTexCoord;" \
            "out_color = vColor;" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_COMPILE_STATUS)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Vertex Shader Compile Successfully\n");
        
        // Fragment Shader
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide shader code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "precision mediump int;" \
        
        "in vec2 out_texcoord;" \
        "in vec4 out_color;" \
        "in vec3 light_direction;" \
        "in vec3 tranformation_matrix;" \
        "in vec3 viewer_vector;" \
        
        "uniform sampler2D u_sampler;" \
        "uniform int u_key_L_pressed;" \
        "uniform vec3 u_light_ambient;" \
        "uniform vec3 u_light_diffuse;" \
        "uniform vec3 u_light_specular;" \
        "uniform vec3 u_material_ambient;" \
        "uniform vec3 u_material_diffuse;" \
        "uniform vec3 u_material_specular;" \
        "uniform float u_material_shiness;" \
        
        "out vec4 FragColor;" \
        
        "vec4 text;" \
        "vec3 phong_ads_light;" \
        
        "void main(void)" \
        "{" \
            "if(1 == u_key_L_pressed)"
            "{" \
                "vec3 light_direction_normalize = normalize(light_direction);" \
                "vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" \
                "vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" \
                "vec3 viewer_vector_normal = normalize(viewer_vector);" \
                "float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" \
                "vec3 ambient = u_light_ambient * u_material_ambient;" \
                "vec3 diffuse = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction;" \
                "vec3 specular = u_light_specular * u_material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), u_material_shiness);" \
                "phong_ads_light= ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
                "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);"
            "}" \
        
            "text = texture(u_sampler, out_texcoord);" \
            "FragColor = vec4((vec3(out_color * text) * phong_ads_light), 1.0);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Fragment Shader Compilation Log: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
                
            }
        }
        printf("Fragement Shader Compile Successfully\n");
        
        // SHADER Program
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // Attributes Binding Before Linking
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_COLOR, "vColor");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_TEXTURE0, "vTexCoord");
        
        // link
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if( iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Shader Program LinkLog: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Shader Program Link Successfully\n");
        
        // Uniform Binding After Linking
        modelMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        lightAmbientUniform =
        glGetUniformLocation(shaderProgramObject, "u_light_ambient");
        lightDiffuseUniform =
        glGetUniformLocation(shaderProgramObject, "u_light_diffuse");
        lightSpecularUniform =
        glGetUniformLocation(shaderProgramObject, "u_light_specular");
        lightPositionUniform =
        glGetUniformLocation(shaderProgramObject, "u_light_position");
        
        materialAmbientUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_ambient");
        materialDiffuseUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_diffuse");
        materialSpecularUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_specular");
        materialShinessUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_shiness");
        
        keyLPressedUniform =
        glGetUniformLocation(shaderProgramObject, "u_key_L_pressed");
        
        sampler_uniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
  
        // load texture
        texture_marble=[self loadTextureFromBMPFile:@"marble" :@"bmp"];
        
        // CUBE
        glGenVertexArrays(1, &vao_cube);
        
        glBindVertexArray(vao_cube);
        
        //
        const GLfloat cube_vcnt[] = {
            //vertex, color, normals, textcoord
            // TOP
            1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f,  1.0f,  0.0f, 0.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f,  1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            // BOTTOM
            1.0f,  -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
            -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f,  0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
            1.0f,  -1.0f, 1.0f,  0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
            // FRONT
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            // BACK
            1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f,  0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,  0.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f,  0.0f, -1.0f, 0.0f, 1.0f,
            // RIGHT
            1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  0.0f, 0.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  0.0f, 0.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f,  1.0f,  0.0f, 0.0f, 0.0f, 1.0f,
            // LEFT
            -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
            -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f,  0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f,  0.0f, 1.0f
        };
        
        glGenBuffers(1, &vbo_cube_vcnt);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vcnt);
        
        glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(GLfloat), cube_vcnt,
                     GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE,
                              11 * sizeof(GLfloat), (void *)(0 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_COLOR, 3, GL_FLOAT, GL_FALSE,
                              11 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTES_COLOR);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE,
                              11 * sizeof(GLfloat), (void *)(6 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_TEXTURE0, 2, GL_FLOAT, GL_FALSE,
                              11 * sizeof(GLfloat), (void *)(9 * sizeof(GLfloat)));
        glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXTURE0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        //
        
        glClearDepthf(1.0f);
        
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
        
        // Gesture Recognition
        
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    
    return(self);
}

/* This is required in multithread rendering
 // only override drawRect: if you perfrom custom drawing.
 // An Empty implemetation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 }
 */

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender // draw or display
{
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    translateMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -1.0f);
    rotationMatrix = vmath::rotate(rotation_angle_cube, 1.0f, 0.0f, 0.0f);
    rotationMatrix =
    rotationMatrix * vmath::rotate(rotation_angle_cube, 0.0f, 1.0f, 0.0f);
    rotationMatrix =
    rotationMatrix * vmath::rotate(rotation_angle_cube, 0.0f, 0.0f, 1.0f);
    scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
    
    modelMatrix = translateMatrix * rotationMatrix * scaleMatrix;
    viewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                       perspectiveProjectionMatrix);
    if (isLightEnable == YES) {
        glUniform1i(keyLPressedUniform, 1);
        glUniform3fv(lightAmbientUniform, 1, lightAmbient);
        glUniform3fv(lightDiffuseUniform, 1, lightDiffuse);
        glUniform3fv(lightSpecularUniform, 1, lightSpecular);
        glUniform4fv(lightPositionUniform, 1, lightPosition);
        glUniform3fv(materialAmbientUniform, 1, materialAmbient);
        glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
        glUniform3fv(materialSpecularUniform, 1, materialSpecular);
        glUniform1f(materialShinessUniform, materialShiness);
    } else {
        glUniform1i(keyLPressedUniform, 0);
    }
    
    glBindTexture(GL_TEXTURE_2D, texture_marble);
    glBindVertexArray(vao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    if(rotation_angle_cube > 360.0f)
    {
        rotation_angle_cube = 0.0f;
    }
    rotation_angle_cube += 1.0f;
}

-(void)layoutSubviews // resize
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
    
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode]; // => Add drawView TO Default RunLoop
        
        isAnimating= YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // code
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(isLightEnable == NO)
    {
        isLightEnable = YES;
    }
    else
    {
        isLightEnable = NO;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UITapGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UITapGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    glDeleteTextures(1, &texture_marble);
    
    if (vbo_cube_vcnt)
    {
        glDeleteBuffers(1, &vbo_cube_vcnt);
        vbo_cube_vcnt = 0;
    }
    
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
}

@end

