//
//  AppDelegate.m
//  StaticSmiley
//
//  Created by Prasanna Pawar on 20/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

#import "GLESView.h"

@implementation AppDelegate
{
@private
    UIWindow *mainWindow;
    ViewController *mainViewController;
    GLESView *glesView;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // get screen bounds for fullscreen
    CGRect screenBounds=[[UIScreen mainScreen]bounds];
    
    // initialize window variable corrsponding to screen bounds
    mainWindow=[[UIWindow alloc]initWithFrame:screenBounds];
    
    mainViewController=[[ViewController alloc]init];
    
    [mainWindow setRootViewController:mainViewController];
    
    // initialize view variable corresponding to screen bounds
    glesView=[[GLESView alloc]initWithFrame:screenBounds];
    
    [mainViewController setView:glesView];
    
    [glesView release];
    
    // add the ViewController's view as subview to the window
    [mainWindow addSubview:[mainViewController view]];
    
    // make window key window and visible
    [mainWindow makeKeyAndVisible];
    
    [glesView startAnimation];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [glesView stopAnimation];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [glesView startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [glesView stopAnimation];
}

- (void)dealloc
{
    [glesView release];
    
    [mainViewController release];
    
    [mainWindow release];
    
    [super dealloc];
}

@end
