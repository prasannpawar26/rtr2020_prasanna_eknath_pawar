//
//  AppDelegate.h
//  StaticSmiley
//
//  Created by Prasanna Pawar on 20/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

