//
//  MyView.h
//  Window
//
//  Created by Prasanna Pawar on 19/12/19.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>

@end
