//
//  GLESView.h
//  2DShapes
//
//  Created by Prasanna Pawar on 19/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

@end
