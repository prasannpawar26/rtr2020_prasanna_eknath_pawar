//
//  main.m
//  Ortho
//
//  Created by Prasanna Pawar on 19/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    int return_value = 0;
    
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
    
    //[[UIApplication sharedApplication] setDelegate:[[AppDelegate alloc]init]];
    
    return_value = UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    
    [pPool release];
    
    return return_value;
}
