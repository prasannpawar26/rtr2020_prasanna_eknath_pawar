//
//  GLESView.m
//  TwoSteadyLights
//
//  Created by Prasanna Pawar on 22/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

// uniforms structure
struct light_configuration_uniform {
    GLuint lightAmbient;
    GLuint lightDiffuse;
    GLuint lightSpecular;
    GLuint lightPosition;
};

struct light_configuration {
    float lightAmbient[4];
    float lightDiffuse[4];
    float lightSpecular[4];
    float lightPosition[4];
};

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTURE0,
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    BOOL isLightEnable;
    
    // shader variables
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    // vao, vbo variables
    GLuint vao_pyramid;
    GLuint vbo_pyramid_vertex;
    GLuint vbo_pyramid_normal;
    GLfloat rotation_angle_pyramid;
    
    struct light_configuration light0;
    struct light_configuration light1;
    float materialAmbient[4];
    float materialDiffuse[4];
    float materialSpecular[4];
    float materialShiness;  // Also Try 128.0f
    
    // Uniforms
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint keyLPressedUniform;
    struct light_configuration_uniform light_configuration_uniform0;
    struct light_configuration_uniform light_configuration_uniform1;
    
    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShinessUniform;
    
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 translateMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(CGRect)frame // initialize
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;// or [super layer]
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer]; // get data for render buffur from opengl(renderer)
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer: %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // Hard Code Initialiations
        isAnimating=NO;
        isLightEnable = NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        //Shader Related Code Here
        
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        
        "struct matrices {" \
        "mat4 model_matrix;" \
        "mat4 view_matrix;" \
        "mat4 projection_matrix;" \
        "};" \
        "uniform matrices u_matrices;" \
        
        "struct light_configuration {"
            "vec3 light_ambient;" \
            "vec3 light_diffuse;" \
            "vec3 light_specular;" \
            "vec4 light_position;" \
        "};"
        "uniform light_configuration u_light_configuration0;" \
        "uniform light_configuration u_light_configuration1;" \
        
        "struct material_configuration {" \
        "vec3 material_ambient;" \
        "vec3 material_diffuse;" \
        "vec3 material_specular;" \
        "float material_shiness;" \
        "};" \
        "uniform material_configuration u_material_configuration;" \
        
        "uniform int u_key_L_pressed;" \
        "out vec3 phong_ads_light;" \
        
        "void main(void)" \
        "{" \
        "if(1 == u_key_L_pressed)" \
        "{" \
            "vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
            "vec3 t_normal = normalize(mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal);" \
            "vec3 viewer_vector = normalize(vec3(-eye_coordinates));" \
        
            "vec3 light_direction0 = normalize(vec3(u_light_configuration0.light_position - eye_coordinates));" \
            "float t_normal_dot_light_direction0 = max(dot(light_direction0, t_normal), 0.0f);" \
            "vec3 reflection_vector0 = reflect(-light_direction0, t_normal);" \
            "vec3 ambient0 = u_light_configuration0.light_ambient * u_material_configuration.material_ambient;" \
            "vec3 diffuse0 = u_light_configuration0.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction0;" \
            "vec3 specular0 = u_light_configuration0.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector0, viewer_vector), 0.0f), u_material_configuration.material_shiness);" \
        
            "vec3 light_direction1 = normalize(vec3(u_light_configuration1.light_position - eye_coordinates));" \
            "float t_normal_dot_light_direction1 = max(dot(light_direction1, t_normal), 0.0f);" \
            "vec3 reflection_vector1 = reflect(-light_direction1, t_normal);" \
            "vec3 ambient1 = u_light_configuration1.light_ambient * u_material_configuration.material_ambient;" \
            "vec3 diffuse1 = u_light_configuration1.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction1;" \
            "vec3 specular1 = u_light_configuration1.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector1, viewer_vector), 0.0f), u_material_configuration.material_shiness);" \
        
            "phong_ads_light = ambient0+ ambient1 + diffuse0 + diffuse1 + specular0 + specular1;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
        "}" \
        
        "gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_COMPILE_STATUS)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Vertex Shader Compile Successfully\n");
        
        // Fragment Shader
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide shader code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 phong_ads_light;" \
        
        "uniform int u_key_L_pressed;" \
        
        "out vec4 FragColor;" \
        
        "void main(void)" \
        "{" \
            "FragColor = vec4(phong_ads_light, 1.0);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Fragment Shader Compilation Log: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
                
            }
        }
        printf("Fragement Shader Compile Successfully\n");
        
        // SHADER Program
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
        
        // link
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if( iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Shader Program LinkLog: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Shader Program Link Successfully\n");
        
        // Uniform Binding After Linking
        modelMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_matrices.model_matrix");
        viewMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_matrices.view_matrix");
        projectionMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_matrices.projection_matrix");
        
        light_configuration_uniform0.lightAmbient = glGetUniformLocation(
                                                                         shaderProgramObject, "u_light_configuration0.light_ambient");
        light_configuration_uniform0.lightDiffuse = glGetUniformLocation(
                                                                         shaderProgramObject, "u_light_configuration0.light_diffuse");
        light_configuration_uniform0.lightSpecular = glGetUniformLocation(
                                                                          shaderProgramObject, "u_light_configuration0.light_specular");
        light_configuration_uniform0.lightPosition = glGetUniformLocation(
                                                                          shaderProgramObject, "u_light_configuration0.light_position");
        
        light_configuration_uniform1.lightAmbient = glGetUniformLocation(
                                                                         shaderProgramObject, "u_light_configuration1.light_ambient");
        light_configuration_uniform1.lightDiffuse = glGetUniformLocation(
                                                                         shaderProgramObject, "u_light_configuration1.light_diffuse");
        light_configuration_uniform1.lightSpecular = glGetUniformLocation(
                                                                          shaderProgramObject, "u_light_configuration1.light_specular");
        light_configuration_uniform1.lightPosition = glGetUniformLocation(
                                                                          shaderProgramObject, "u_light_configuration1.light_position");
        
        materialAmbientUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_ambient");
        materialDiffuseUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_diffuse");
        materialSpecularUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_specular");
        materialShinessUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_configuration.material_shiness");
        
        keyLPressedUniform =
        glGetUniformLocation(shaderProgramObject, "u_key_L_pressed");
        
        light0.lightAmbient[0] = 0.0f;
        light0.lightAmbient[1] = 0.0f;
        light0.lightAmbient[2] = 0.0f;
        light0.lightAmbient[3] = 0.0f;
        
        light0.lightDiffuse[0] = 1.0f;
        light0.lightDiffuse[1] = 0.0f;
        light0.lightDiffuse[2] = 0.0f;
        light0.lightDiffuse[3] = 1.0f;
        
        light0.lightSpecular[0] = 1.0f;
        light0.lightSpecular[1] = 0.0f;
        light0.lightSpecular[2] = 0.0f;
        light0.lightSpecular[3] = 1.0f;
        
        light0.lightPosition[0] = -2.0f;
        light0.lightPosition[1] = 0.0f;
        light0.lightPosition[2] = 0.0f;
        light0.lightPosition[3] = 1.0f;
        
        light1.lightAmbient[0] = 0.0f;
        light1.lightAmbient[1] = 0.0f;
        light1.lightAmbient[2] = 0.0f;
        light1.lightAmbient[3] = 0.0f;
        
        light1.lightDiffuse[0] = 0.0f;
        light1.lightDiffuse[1] = 0.0f;
        light1.lightDiffuse[2] = 1.0f;
        light1.lightDiffuse[3] = 1.0f;
        
        light1.lightSpecular[0] = 0.0f;
        light1.lightSpecular[1] = 0.0f;
        light1.lightSpecular[2] = 1.0f;
        light1.lightSpecular[3] = 1.0f;
        
        light1.lightPosition[0] = 2.0f;
        light1.lightPosition[1] = 0.0f;
        light1.lightPosition[2] = 0.0f;
        light1.lightPosition[3] = 1.0f;
        
        materialAmbient[0] = 0.0f;
        materialAmbient[1] = 0.0f;
        materialAmbient[2] = 0.0f;
        materialAmbient[3] = 0.0f;
        
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        materialSpecular[0] = 1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        materialShiness = 50.0f;
        
        // Pyramid
        glGenVertexArrays(1, &vao_pyramid);
        glBindVertexArray(vao_pyramid);
        
        // pyramid-vertex
        const GLfloat pyramidVertices[] = {
            // FORNT
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
            // BACK
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
            // RIGHT
            0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
            // LEFT
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f};
        
        glGenBuffers(1, &vbo_pyramid_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_vertex);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices,
                     GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        const GLfloat pyramidNormals[] = {
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            
            0.894427f,  0.447214f, 0.0f,
            0.894427f,  0.447214f, 0.0f,
            0.894427f,  0.447214f, 0.0f,
            
            0.0f, 0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
            
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f};
        
        glGenBuffers(1, &vbo_pyramid_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals,
                     GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        //
        
        glClearDepthf(1.0f);
        
        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
        
        // Gesture Recognition
        
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    
    return(self);
}

/* This is required in multithread rendering
 // only override drawRect: if you perfrom custom drawing.
 // An Empty implemetation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 }
 */

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender // draw or display
{
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    translateMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    rotationMatrix = vmath::rotate(rotation_angle_pyramid, 0.0f, 1.0f, 0.0f);
    modelMatrix = translateMatrix * rotationMatrix;
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                       perspectiveProjectionMatrix);
    
    if (isLightEnable == YES)
    {
        printf("Light Enabled\n");
        glUniform1i(keyLPressedUniform, 1);
        
        glUniform3fv(light_configuration_uniform0.lightAmbient, 1,
                     light0.lightAmbient);
        glUniform3fv(light_configuration_uniform0.lightDiffuse, 1,
                     light0.lightDiffuse);
        glUniform3fv(light_configuration_uniform0.lightSpecular, 1,
                     light0.lightSpecular);
        glUniform4fv(light_configuration_uniform0.lightPosition, 1,
                     light0.lightPosition);
        
        glUniform3fv(light_configuration_uniform1.lightAmbient, 1,
                     light1.lightAmbient);
        glUniform3fv(light_configuration_uniform1.lightDiffuse, 1,
                     light1.lightDiffuse);
        glUniform3fv(light_configuration_uniform1.lightSpecular, 1,
                     light1.lightSpecular);
        glUniform4fv(light_configuration_uniform1.lightPosition, 1,
                     light1.lightPosition);
        
        glUniform3fv(materialAmbientUniform, 1, materialAmbient);
        glUniform3fv(materialDiffuseUniform, 1, materialDiffuse);
        glUniform3fv(materialSpecularUniform, 1, materialSpecular);
        glUniform1f(materialShinessUniform, materialShiness);
    }
    else
    {
        glUniform1i(keyLPressedUniform, 0);
    }
    
    glBindVertexArray(vao_pyramid);
    
    glDrawArrays(GL_TRIANGLES, 0, 12);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    rotation_angle_pyramid = rotation_angle_pyramid + 1.0f;
}

-(void)layoutSubviews // resize
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
    
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode]; // => Add drawView TO Default RunLoop
        
        isAnimating= YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // code
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(isLightEnable == NO)
    {
        isLightEnable = YES;
    }
    else
    {
        isLightEnable = NO;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UITapGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UITapGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    if (vbo_pyramid_normal) {
        glDeleteBuffers(1, &vbo_pyramid_normal);
        vbo_pyramid_normal = 0;
    }
    
    if (vbo_pyramid_vertex) {
        glDeleteBuffers(1, &vbo_pyramid_vertex);
        vbo_pyramid_vertex = 0;
    }
    
    if (vao_pyramid) {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
}

@end

