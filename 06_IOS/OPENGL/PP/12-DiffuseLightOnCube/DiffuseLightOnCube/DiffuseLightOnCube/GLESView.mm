//
//  GLESView.m
//  DiffuseLightOnCube
//
//  Created by Prasanna Pawar on 20/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTURE0,
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    BOOL isLightEnable;
    
    // shader variables
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_normal;
    GLfloat rotation_cube_angle;
    
    // uniforms
    GLuint modelViewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint lightDiffuseUniform;
    GLuint materialDiffuseUniform;
    GLuint lightPositionUniform;
    GLuint keyLPressedUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(CGRect)frame // initialize
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;// or [super layer]
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer]; // get data for render buffur from opengl(renderer)
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer: %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // Hard Code Initialiations
        rotation_cube_angle = 0.0f;
        isAnimating=NO;
        isLightEnable = NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        //Shader Related Code Here
        
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "precision mediump int;" \
        
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        
        "uniform mat4 u_modelview_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_key_L_pressed;" \
        "uniform vec3 u_light_diffuse;" \
        "uniform vec3 u_material_diffuse;" \
        "uniform vec4 u_light_position;" \
        
        "out vec3 diffuse_color;" \
        "void main(void)" \
        "{" \
            "if(1 == u_key_L_pressed)" \
            "{" \
                "vec4 eye_coordinates = u_modelview_matrix * vPosition;" \
                "mat3 normalMatrix = mat3(transpose(inverse(u_modelview_matrix)));" \
                "vec3 t_normal = normalize(normalMatrix * vNormal);" \
                "vec3 lightSource = vec3(vec3(u_light_position) - eye_coordinates.xyz);" \
                "diffuse_color = u_light_diffuse * u_material_diffuse * dot(lightSource, "
                "t_normal);" \
            "}" \
        
            "gl_Position = u_projection_matrix * u_modelview_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_COMPILE_STATUS)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Vertex Shader Compile Successfully\n");
        
        // Fragment Shader
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide shader code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "precision mediump int;" \
        
        "in vec3 diffuse_color;" \
        
        "uniform int u_key_L_pressed;" \
        
        "out vec4 FragColor;" \
        
        "void main(void)" \
        "{" \
            "if(1 == u_key_L_pressed)" \
            "{" \
                "FragColor = vec4(diffuse_color, 1.0);" \
            "}" \
            "else" \
            "{" \
                "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
            "}" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Fragment Shader Compilation Log: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
                
            }
        }
        printf("Fragement Shader Compile Successfully\n");
        
        // SHADER Program
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_NORMAL, "vNormal");
        
        // link
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if( iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Shader Program LinkLog: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Shader Program Link Successfully\n");
        
        // Uniform Binding After Linking
        modelViewMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_modelview_matrix");
        projectionMatrixUniform =
        glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        lightDiffuseUniform =
        glGetUniformLocation(shaderProgramObject, "u_light_diffuse");
        materialDiffuseUniform =
        glGetUniformLocation(shaderProgramObject, "u_material_diffuse");
        lightPositionUniform =
        glGetUniformLocation(shaderProgramObject, "u_light_position");
        keyLPressedUniform =
        glGetUniformLocation(shaderProgramObject, "u_key_L_pressed");

        // CUBE
        const GLfloat cubeVertices[] = {// TOP
            1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
            // BOTTOM
            1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
            // FRONT
            1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
            // BACK
            1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
            // RIGHT
            1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
            // LEFT
            -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f, -1.0f, -1.0f};
        
        const GLfloat cubeNormals[] = {
            0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
            0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
            
            0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,
            0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,
            
            0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
            0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
            
            0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,
            0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,
            
            1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
            1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
            
            -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,
            -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f
        };
        
        glGenVertexArrays(1, &vao_cube);
        
        glBindVertexArray(vao_cube);
        
        glGenBuffers(1, &vbo_cube_position);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices,
                     GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normals
        glGenBuffers(1, &vbo_cube_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals,
                     GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTES_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTES_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        //
        
        glClearDepthf(1.0f);
        
        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
        
        // Gesture Recognition
        
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    
    return(self);
}

/* This is required in multithread rendering
 // only override drawRect: if you perfrom custom drawing.
 // An Empty implemetation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 }
 */

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender // draw or display
{
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix;
    vmath::mat4 translateMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 scaleMatrix;
    
    // cube
    translateMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    scaleMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    
    translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(rotation_cube_angle, 1.0f, 0.0f, 0.0f);
    rotationMatrix =
    rotationMatrix * vmath::rotate(rotation_cube_angle, 0.0f, 1.0f, 0.0f);
    rotationMatrix =
    rotationMatrix * vmath::rotate(rotation_cube_angle, 0.0f, 0.0f, 1.0f);
    scaleMatrix = vmath::scale(1.0f, 1.0f, 1.0f);
    
    modelViewMatrix = translateMatrix * rotationMatrix * scaleMatrix;
    
    glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE,
                       perspectiveProjectionMatrix);
    
    if (isLightEnable == YES)
    {
        glUniform1i(keyLPressedUniform, 1);
        glUniform3f(lightDiffuseUniform, 1.0f, 1.0f, 1.0f);
        glUniform3f(materialDiffuseUniform, 0.50f, 0.50f, 0.50f);
        glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    } else {
        glUniform1i(keyLPressedUniform, 0);
    }
    
    glBindVertexArray(vao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    rotation_cube_angle += 1.0f;
}

-(void)layoutSubviews // resize
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
    
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode]; // => Add drawView TO Default RunLoop
        
        isAnimating= YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // code
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(isLightEnable == NO)
    {
        isLightEnable = YES;
    }
    else
    {
        isLightEnable = NO;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UITapGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UITapGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    if (vbo_cube_normal)
    {
        glDeleteBuffers(1, &vbo_cube_normal);
        vbo_cube_normal = 0;
    }
    
    if (vbo_cube_position)
    {
        glDeleteBuffers(1, &vbo_cube_position);
        vbo_cube_position = 0;
    }
    
    if (vao_cube)
    {
        glDeleteVertexArrays(1, &vao_cube);
        vao_cube = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
}

@end

