//
//  GLESView.m
//  CheckerBoard
//
//  Created by Prasanna Pawar on 22/01/20.
//  Copyright © 2020 Prasanna Pawar. All rights reserved.
//


#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

GLubyte check_image[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];

enum
{
    AMC_ATTRIBUTES_POSITION = 0,
    AMC_ATTRIBUTES_COLOR,
    AMC_ATTRIBUTES_NORMAL,
    AMC_ATTRIBUTES_TEXTURE0,
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_rectangle_1;
    GLuint vbo_position_1;
    GLuint vbo_texture_1;
    
    GLuint vao_rectangle_2;
    GLuint vbo_position_2;
    GLuint vbo_texture_2;
    
    GLuint texture_math;
    
    GLuint sampler_uniform;
    
    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
}

- (id)initWithFrame:(CGRect)frame // initialize
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;// or [super layer]
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer]; // get data for render buffur from opengl(renderer)
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer: %s | GL Version : %s | GLSL Version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // Hard Code Initialiations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        //Shader Related Code Here
        
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        
        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \
        "out vec2 out_textcoord;" \
        "uniform mat4 u_mvp_matrix;" \
        
        "void main(void)" \
        "{" \
            "gl_Position = u_mvp_matrix * vPosition;" \
            "out_textcoord = vTexCoord;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_COMPILE_STATUS)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Vertex Shader Compile Successfully\n");
        
        // Fragment Shader
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide shader code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        
        "uniform sampler2D u_sampler;" \
        "in vec2 out_textcoord;" \
        "out vec4 FragColor;" \
        
        " void main(void)" \
        "{" \
            "FragColor = texture(u_sampler, out_textcoord);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Fragment Shader Compilation Log: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
                
            }
        }
        printf("Fragement Shader Compile Successfully\n");
        
        // SHADER Program
        //create
        shaderProgramObject = glCreateProgram();
        
        // attach
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_POSITION, "vPosition");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTES_TEXTURE0,
                             "vTexCoord");
        
        // link
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if( iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    
                    printf("Shader Program LinkLog: %s\n", szInfoLog);
                    
                    free(szInfoLog);
                    
                    [self release];
                }
            }
        }
        printf("Shader Program Link Successfully\n");
        //get MVP uniform location
        
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        sampler_uniform = glGetUniformLocation(shaderProgramObject, "u_sampler");
        
        // load texture
        texture_math = [self loadTexture];
        
        // RECTANGLE_1
        
        glGenVertexArrays(1, &vao_rectangle_1);
        
        glBindVertexArray(vao_rectangle_1);
        
        // vertex
        glGenBuffers(1, &vbo_position_1);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_1);
        
        glBufferData(GL_ARRAY_BUFFER, 12, NULL, GL_DYNAMIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //texture
        const GLfloat textureCoords_1[] =
        {0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f, 1.0f};
        
        glGenBuffers(1, &vbo_texture_1);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_1);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords_1), textureCoords_1,
                     GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXTURE0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        //
        
        // RECTANGLE_2
        
        glGenVertexArrays(1, &vao_rectangle_2);
        
        glBindVertexArray(vao_rectangle_2);
        
        // vertex
        glGenBuffers(1, &vbo_position_2);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_2);
        
        glBufferData(GL_ARRAY_BUFFER, 12, NULL, GL_DYNAMIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTES_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //texture
        const GLfloat textureCoords_2[] =
        {0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f, 1.0f};
        
        glGenBuffers(1, &vbo_texture_2);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_2);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords_2), textureCoords_2,
                     GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTES_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0,
                              NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTES_TEXTURE0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        glClearDepthf(1.0f);
        
        glEnable(GL_DEPTH_TEST);
        
        glEnable(GL_TEXTURE_2D);
        
        glDepthFunc(GL_LEQUAL);
        
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue
        
        // Gesture Recognition
        
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    
    return(self);
}

-(void)makeCheckerImage
{
    // Variable Delcarations
    int i, j, c;
    
    for (i = 0; i < CHECK_IMAGE_HEIGHT; i++) {
        for (j = 0; j < CHECK_IMAGE_WIDTH; j++) {
            c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            check_image[i][j][0] = (GLubyte)c;
            check_image[i][j][1] = (GLubyte)c;
            check_image[i][j][2] = (GLubyte)c;
            check_image[i][j][3] = 255;
        }
    }
    
    return;
}

-(GLuint)loadTexture
{
    GLuint bmpTexture;
    
    [self makeCheckerImage];
    
    glGenTextures(1, &bmpTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Set 1 rather than default 4 , for better performance
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECK_IMAGE_WIDTH, CHECK_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, check_image);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    return bmpTexture;
}

/* This is required in multithread rendering
 // only override drawRect: if you perfrom custom drawing.
 // An Empty implemetation adversely affects performance during animation
 - (void)drawRect:(CGRect)rect
 {
 }
 */

+(Class)layerClass
{
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender // draw or display
{
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glUniform1i(sampler_uniform, 0);
    
    // Rectangle 1
    
    glBindTexture(GL_TEXTURE_2D, texture_math);
    
    glBindVertexArray(vao_rectangle_1);
    
    const GLfloat rectangleVertices_1[] =
    {
        0.0f, 1.0f,  0.0f,
        -2.0f, 1.0f,  0.0f,
        -2.0f, -1.0f, 0.0f,
        0.0f, -1.0f, 0.0f
    };
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_1);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices_1), rectangleVertices_1, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_1);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    // Rectangle 2
    
    glBindTexture(GL_TEXTURE_2D, texture_math);
    
    glBindVertexArray(vao_rectangle_2);
    
    const GLfloat rectangleVertices_2[] =
    {
        // Right-Top
        2.41421f, 1.0f, -1.41421f,
        // Left-Top
        1.0f, 1.0f, 0.0f,
        // Left-Bottom
        1.0f, -1.0f, 0.0f,
        // Right-Bottom
        2.41421f, -1.0f, -1.41421f
    };
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_2);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices_2), rectangleVertices_2, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_2);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews // resize
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/height, 0.1f, 100.0f);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
    
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode]; // => Add drawView TO Default RunLoop
        
        isAnimating= YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // code
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UITapGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UITapGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    if (vbo_texture_1)
    {
        glDeleteBuffers(1, &vbo_texture_1);
        vbo_texture_1 = 0;
    }
    
    if (vbo_position_1)
    {
        glDeleteBuffers(1, &vbo_position_1);
        vbo_position_1 = 0;
    }
    
    if (vao_rectangle_1)
    {
        glDeleteVertexArrays(1, &vao_rectangle_1);
        vao_rectangle_1 = 0;
    }
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
}

@end

