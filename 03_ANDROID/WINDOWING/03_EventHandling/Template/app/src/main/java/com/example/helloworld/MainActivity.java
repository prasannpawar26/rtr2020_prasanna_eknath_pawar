package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.content.pm.ActivityInfo;
import android.view.View;

//import android.graphics.Color;
//import android.view.Gravity;
//import androidx.appcompat.widget.AppCompatTextView;

public class MainActivity extends AppCompatActivity {

	private MyView myView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setContentView(R.layout.activity_main);
		//this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);	
		//AppCompatTextView myView = new AppCompatTextView(this);
		//myView.setBackgroundColor(Color.rgb(0, 0, 0));
		//myView.setTextColor(Color.rgb(0, 255, 0));
		//myView.setTextSize(60);
		//myView.setGravity(Gravity.CENTER);
		//myView.setText("Hellow World !!!");
		
		// Set Our Own View As Our Main View
		this.getWindow().getDecorView().setSystemUiVisibility( // This Code From Pie and Onwords
			View.SYSTEM_UI_FLAG_IMMERSIVE |
			View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
			View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
			View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
			View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
			View.SYSTEM_UI_FLAG_FULLSCREEN
		);
		
		super.onCreate(savedInstanceState);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		myView = new MyView(this);
		
		setContentView(myView);
    }
}