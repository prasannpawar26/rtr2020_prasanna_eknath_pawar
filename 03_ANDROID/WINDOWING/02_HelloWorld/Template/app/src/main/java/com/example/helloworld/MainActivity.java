package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Color;
import android.view.Gravity;
import androidx.appcompat.widget.AppCompatTextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
		
		this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);
		
		AppCompatTextView myView = new AppCompatTextView(this);
		
		myView.setBackgroundColor(Color.rgb(0, 0, 0));
		myView.setTextColor(Color.rgb(0, 255, 0));
		myView.setTextSize(60);
		myView.setGravity(Gravity.CENTER);
		myView.setText("Hellow World !!!");
		
		// Set Our Own View As Our Main View
		setContentView(myView);
    }
}