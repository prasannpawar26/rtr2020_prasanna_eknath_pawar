#include <jni.h>
#include <string>

#include <GLES3/gl32.h>

//GLM
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

//JNI information and error logs
#include <android/log.h>

#define LOG_TAG "SAM: glOpenGLES32OrthoNative"
#define LOGINFO(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGERROR(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

enum {
    PEP_ATTRIBUTE_VERTEX = 0,
    PEP_ATTRIBUTE_COLOR,
    PEP_ATTRIBUTE_NORMAL,
    PEP_ATTRIBUTE_TEXTURE0,
};

GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo_color,gVbo_position;
GLint gMUniform;
GLint gVUniform;
GLint gPUniform;

glm::mat4 gOrthographicProjectionMatrix;


GLuint loadAndCompileShader(GLenum shaderType, const char *sourceCode)
{
    GLuint shaderID = glCreateShader(shaderType);

    if(shaderID)
    {
        glShaderSource(shaderID,1,&sourceCode,NULL);
        glCompileShader(shaderID);

        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus= 0;
        char *szInfoLog = NULL;

        glGetShaderiv(shaderID,GL_COMPILE_STATUS,&iShaderCompiledStatus);
        if(iShaderCompiledStatus==GL_FALSE)
        {
            glGetShaderiv(shaderID,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog =(char*)malloc(iInfoLogLength);

                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(shaderID,iInfoLogLength,&written,szInfoLog);
                    LOGERROR("Could not compile shader %d:\n%s\n",shaderType,szInfoLog);
                    free(szInfoLog);
                }

                glDeleteShader(shaderID);
                shaderID=0;
            }
        }

    }
    return shaderID;
}

GLuint createProgramObjectAndLinkShader(GLuint vertexShaderID, GLuint fragmentShaderID) {
    if(!vertexShaderID || !fragmentShaderID)
        return 0;

    GLuint program = glCreateProgram();

    if(program)
    {
        glAttachShader(program,vertexShaderID);
        glAttachShader(program,fragmentShaderID);
        glBindAttribLocation(program, PEP_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(program, PEP_ATTRIBUTE_COLOR, "vColor");
        glLinkProgram(program);

        GLint iInfoLogLength = 0;
        GLint iShaderProgramLinkStatus = 0;
        char *szInfoLog = NULL;

        glGetShaderiv(program,GL_LINK_STATUS,&iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus ==GL_FALSE){
            glGetShaderiv(program,GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength>0)
            {
                szInfoLog = (char*)malloc(iInfoLogLength);
                if(szInfoLog!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(program,iInfoLogLength,&written,szInfoLog);
                    LOGERROR("Could not link program : \n%s\n",szInfoLog);
                    free(szInfoLog);
                }

                glDeleteProgram(program);
                program=0;
            }
        }
    }

    return program;
}

GLuint createProgramObject(const char *vertexSource, const char *fragmentSource)
{
    GLuint vsID = loadAndCompileShader(GL_VERTEX_SHADER,vertexSource);
    GLuint fsID = loadAndCompileShader(GL_FRAGMENT_SHADER,fragmentSource);

    return createProgramObjectAndLinkShader(vsID,fsID);
}

//logcat: GLES INFO
static void printGLString(const char *name,GLenum s)
{
    LOGINFO("GL %s = %s\n",name,(const char*)glGetString(s));
}

void printOpenGLESInfo()
{
    printGLString("VERSION",GL_VERSION);
    printGLString("Vendor",GL_VENDOR);
    printGLString("Renderer",GL_RENDERER);
    printGLString("EXTENSIONS",GL_EXTENSIONS);
    printGLString("GL Shading Language",GL_SHADING_LANGUAGE_VERSION);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_orthographicprojection_GLESNativeLib_resize(JNIEnv *env, jclass type, jint width, jint height) {

    GLfloat  aspectRation;

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    if(height==0)
        height=1;

    aspectRation = width >height ? (GLfloat)width/(GLfloat)height : (GLfloat)height/(GLfloat)width;

    if(width <= height)
        gOrthographicProjectionMatrix = glm::ortho(-100.0f,100.0f,(-100.0f/aspectRation),(100.0f/aspectRation),-100.0f,100.0f);
    else
        gOrthographicProjectionMatrix = glm::ortho((-100.0f*aspectRation),(100.0f*aspectRation),-100.0f,100.0f,-100.0f,100.0f);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_orthographicprojection_GLESNativeLib_init(JNIEnv *env, jclass type) {

    printOpenGLESInfo();
    const GLchar *vertexShaderSource =
            "#version 320 es"\
            "\n"\
            "in vec4 vPosition;"\
            "in vec4 vColor;"\
            "uniform mat4 u_m_matrix;"\
            "uniform mat4 u_v_matrix;"\
            "uniform mat4 u_p_matrix;"\
            "out vec4 out_color;"\
            "void main(void)"\
            "{"\
            "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;"\
            "out_color = vColor;"\
            "}";
    const GLchar *fragmentShaderSource =
            "#version 320 es"\
            "\n"\
            "precision highp float;"\
            "in vec4 out_color;"\
            "out vec4 FragColor;"\
            "void main(void){"\
                "FragColor = out_color;"\
            "}";
    gShaderProgramObject = createProgramObject(vertexShaderSource,fragmentShaderSource);

    gMUniform = glGetUniformLocation(gShaderProgramObject,"u_m_matrix");
    gVUniform = glGetUniformLocation(gShaderProgramObject,"u_v_matrix");
    gPUniform = glGetUniformLocation(gShaderProgramObject,"u_p_matrix");

    const GLfloat triangleVertices[] = {
            0.0f,50.0f,0.0f,//apex
            -50.0f,-50.0f,0.0f,//lb
            50.0f,-50.0f,0.0f//rb
    };

    const GLfloat triangleColors[] = {
            1.0f,0.0f,0.0f,
            0.0f,1.0f,0.0f,
            0.0f,0.0f,1.0f
    };

    glGenVertexArrays(1,&gVao);
    glBindVertexArray(gVao);
    glGenBuffers(1,&gVbo_position);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(triangleVertices),triangleVertices,GL_STATIC_DRAW);
    glVertexAttribPointer(PEP_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(PEP_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    glGenBuffers(1,&gVbo_color);
    glBindBuffer(GL_ARRAY_BUFFER,gVbo_color);
    glBufferData(GL_ARRAY_BUFFER,sizeof(triangleColors),triangleColors,GL_STATIC_DRAW);
    glVertexAttribPointer(PEP_ATTRIBUTE_COLOR,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(PEP_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.1f,0.2f,0.3f,0.0f);

    glm::mat4 gOrthographicProjectionMatrix = glm::mat4(1.0);

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_orthographicprojection_GLESNativeLib_display(JNIEnv *env, jclass type) {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

    glm::mat4 modelMatrix = glm::mat4(1.0);
    glm::mat4 viewMatrix = glm::mat4(1.0);

    glUniformMatrix4fv(gMUniform,1,GL_FALSE,glm::value_ptr(modelMatrix));
    glUniformMatrix4fv(gVUniform,1,GL_FALSE,glm::value_ptr(viewMatrix));
    glUniformMatrix4fv(gPUniform,1,GL_FALSE,glm::value_ptr(gOrthographicProjectionMatrix));

    glBindVertexArray(gVao);
    glDrawArrays(GL_TRIANGLES,0,3);
    glBindVertexArray(0);

    glUseProgram(0);

}