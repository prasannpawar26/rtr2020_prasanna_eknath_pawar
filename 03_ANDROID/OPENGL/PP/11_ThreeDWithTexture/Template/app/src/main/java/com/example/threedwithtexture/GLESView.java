package com.example.threedwithtexture;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

// For Texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderShaderObject;
	
	private int[] vao_pyramid = new int[1];
	private int[] vbo_pyramid_position = new int[1];
	private int[] vbo_pyramid_texcoords = new int[1];
	private int[] texture_stone = new int[1];
	private float pyramid_rotation;
	
	private int[] vao_cube = new int[1];
	private int[] vbo_cube_position = new int[1];
	private int[] vbo_cube_texcoords = new int[1];
	private int[] texture_kundali = new int[1];
	private float cube_rotation;
		
	private int mvpUniform;
	private int sampler_uniform;
	private float perspectiveProjectionMatrix[] = new float[16];
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_VERSION = "+version);
		
		String glslversion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_SHADING_LANGUAGE_VERSION = "+glslversion);
		
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("RTR: onSurfaceCreated GL_VENDOR = " +vendor);
		
		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("RTR: onSurfaceCreated GL_RENDERER = " +renderer);
			 
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		System.out.println("RTR: onSurfaceChanged");
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		update();
		
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//System.out.println("RTR: "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//System.out.println("RTR: "+"Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("RTR: "+"Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR: "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	private void initialize(GL10 gl)
	{
		System.out.println("RTR: "+"initialize start");
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexcoords;" +
			"uniform mat4 u_mvp_matrix;" +
			"out vec2 texcoords;" +
			
			"void main(void)" +
			"{" +
				"texcoords = vTexcoords;" +
				"gl_Position = u_mvp_matrix * vPosition;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iShaderCompiledStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(
			vertexShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println(
				"RTR: Vertex shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Vertex shader compilation successful");
		//
		// Fragment Shader
		//
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			
			"uniform highp sampler2D u_sampler;" +
			
			"in vec2 texcoords;" +
			
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
				"FragColor = texture(u_sampler, texcoords);" +
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0] = 0; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(
			fragmentShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println(
				"RTR: Fragement shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Fragement shader compilation successful");
		
		//
		// Shader Program Object
		//
		shaderShaderObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderShaderObject, vertexShaderObject);
		GLES32.glAttachShader(shaderShaderObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexcoords");
		
		GLES32.glLinkProgram(shaderShaderObject);
		
		int[] iShaderProgramlinkStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(
			shaderShaderObject,
			GLES32.GL_LINK_STATUS,
			iShaderProgramlinkStatus,
			0
		);
		if(iShaderProgramlinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv
			(
				shaderShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderShaderObject);
				System.out.println(
				"RTR: Shader Program Linking Log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize glLinkProgram successful");
		
		mvpUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_mvp_matrix");
		sampler_uniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_sampler");
		
		final int size_of_float = 4;
		final int component_of_position = 3;
		final int component_of_texcoords = 2;
		
		//
		// pyramid
		//
		final float pyramidVertices[] = new float[]
		{
			// front
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// right
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// back
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			// left
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};
		
		final float pyramidTexcoords[] = new float[]
		{
			// FRONT
			0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			//BACK
			0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
			// RIGHT
			0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
			// LEFT
			0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
		};
		
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		GLES32.glGenBuffers(1, vbo_pyramid_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_position[0]);	
		ByteBuffer byteBufferPyramidPosition = ByteBuffer.allocateDirect(pyramidVertices.length * size_of_float); // 4 is for size of float
		byteBufferPyramidPosition.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferPyramidPosition = byteBufferPyramidPosition.asFloatBuffer();
		floatBufferPyramidPosition.put(pyramidVertices);
		floatBufferPyramidPosition.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * size_of_float, floatBufferPyramidPosition, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, component_of_position, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	
		GLES32.glGenBuffers(1, vbo_pyramid_texcoords, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_texcoords[0]);	
		ByteBuffer byteBufferPyramidTexcoords = ByteBuffer.allocateDirect(pyramidTexcoords.length * size_of_float); // 4 is for size of float
		byteBufferPyramidTexcoords.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferPyramidTexcoords = byteBufferPyramidTexcoords.asFloatBuffer();
		floatBufferPyramidTexcoords.put(pyramidTexcoords);
		floatBufferPyramidTexcoords.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidTexcoords.length * size_of_float, floatBufferPyramidTexcoords, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0, component_of_texcoords, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		//
		// Cube
		//
		final float cubeVertices[] = new float[]
		{
			// top
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f, 
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,  

			// bottom
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			1.0f, -1.0f,  1.0f,

			// front
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// back
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// right
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// left
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 
			-1.0f, -1.0f, -1.0f, 
			-1.0f, -1.0f, 1.0f
		};
		
		final float cubeTexcoords[] = new float[]
		{
			// TOP
			  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			// BOTTOM
			  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			// FRONT
			  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			// BACK
			  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			// RIGHT
			  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			// LEFT
			  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
		};
		
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glGenBuffers(1, vbo_cube_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]);	
		//
		// java JVM based ahe
		// we are passing array natively from JAVA TO JVM TO CPU
		//
		// step 1: ByteBuffer Allocate Memory Natively.
		// Step 2: Follow native cpu byte order.
		// Step 3: treate like float 
		// step 4: fill data
		// step 5: reset to ZERO the index
		ByteBuffer byteBufferCubePosition = ByteBuffer.allocateDirect(cubeVertices.length * size_of_float); // 4 is for size of float
		byteBufferCubePosition.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferCubePosition = byteBufferCubePosition.asFloatBuffer();
		floatBufferCubePosition.put(cubeVertices);
		floatBufferCubePosition.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * size_of_float, floatBufferCubePosition, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, component_of_position, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	
		GLES32.glGenBuffers(1, vbo_cube_texcoords, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_texcoords[0]);	
		ByteBuffer byteBufferCubeTexcoords = ByteBuffer.allocateDirect(cubeTexcoords.length * size_of_float); // 4 is for size of float
		byteBufferCubeTexcoords.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferCubeTexcoords = byteBufferCubeTexcoords.asFloatBuffer();
		floatBufferCubeTexcoords.put(cubeTexcoords);
		floatBufferCubeTexcoords.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexcoords.length * size_of_float, floatBufferCubeTexcoords, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0, component_of_texcoords, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
		// Andriod Compiler Generates Interger ID Of Resources kept in "raw directory"
		texture_stone[0] = loadGLTexture(R.raw.stone);
		texture_kundali[0] = loadGLTexture(R.raw.kundaliinverted);
		
		System.out.println("RTR: "+"initialize end");
	}
	
	private void update()
	{
		cube_rotation += 1.0f;
		if(cube_rotation > 360.0f)
		{
			cube_rotation = 0.0f;
		}
		
		pyramid_rotation += 1.0f;
		if(pyramid_rotation > 360.0f)
		{
			pyramid_rotation = 0.0f;
		}
	}
	
	private int loadGLTexture(int imageFileResourceID)
	{
		int[] texture = new int[1];
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		
		// DoNot Scale Image
		options.inScaled = false;
		
		// Take All Resources From ACtivity Class And Match With imageFileResourceID. And Return Matched bitmap
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);
		
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		
		// texImage2D  internally calls glTexImage2D
		// Parameter  "3, 4, 5, 8, 9" of glTexImage2D => 3rd Parameter of texImage2D
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		return texture[0];
	}
	
	private void uninitialize()
	{

		GLES32.glDeleteTextures(1, texture_kundali, 0);
		GLES32.glDeleteTextures(1, texture_stone, 0);
		
		if(0 != vbo_pyramid_texcoords[0])
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_texcoords, 0);
			vbo_pyramid_texcoords[0] = 0;
		}
		
		if(0 != vbo_pyramid_position[0])
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_position, 0);
			vbo_pyramid_position[0] = 0;
		}
		
		if(0 != vao_pyramid[0])
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0] = 0;
		}
		
		if(0 != vbo_cube_texcoords[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube_texcoords, 0);
			vbo_cube_texcoords[0] = 0;
		}
		
		if(0 != vbo_cube_position[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube_position, 0);
			vbo_cube_position[0] = 0;
		}
		
		if(0 != vao_cube[0])
		{
			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0] = 0;
		}
		
		GLES32.glUseProgram(shaderShaderObject);
		
		int[] shaderCount = new int[1];
		GLES32.glGetProgramiv(shaderShaderObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		
		int[] shaders = new int[shaderCount[0]];
		
		GLES32.glGetAttachedShaders(shaderShaderObject, shaderCount[0], shaderCount, 0, shaders, 0);
		
		for(int shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
		{
				GLES32.glDetachShader(shaderShaderObject, shaders[shaderNumber]);
				
				GLES32.glDeleteShader(shaders[shaderNumber]);
				
				shaders[shaderNumber] = 0;
		}
		
		GLES32.glDeleteProgram(shaderShaderObject);
		shaderShaderObject = 0;
		
		GLES32.glUseProgram(0);
		
	}
	
	private void resize(int width, int height)
	{
		if (0 == height)
		{
			height = 1;
		}
		
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderShaderObject);
		
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float translationMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] = new float[16];
		float tempMatrix[] = new float[16];
		
		
		// 
		// Pyramid
		//
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(translationMatrix, 0, -2.0f, 0.0f, -7.0f);
		Matrix.setRotateM(rotationMatrix, 0, pyramid_rotation, 0.0f, 1.0f, 0.0f);
		
		Matrix.multiplyMM(modelViewMatrix, 0, translationMatrix, 0, rotationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glUniform1i(sampler_uniform, 0);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_stone[0]);
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glBindVertexArray(0);		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		// 
		// Cube
		//
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.setIdentityM(tempMatrix, 0);
		
		Matrix.translateM(translationMatrix, 0, 2.0f, 0.0f, -7.0f);
		Matrix.setRotateM(rotationMatrix, 0, cube_rotation, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, cube_rotation, 0.0f, 1.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, cube_rotation, 0.0f, 0.0f, 1.0f);
		Matrix.scaleM(scaleMatrix, 0, 0.750f, 0.750f, 0.750f);
		
		Matrix.multiplyMM(tempMatrix, 0, translationMatrix, 0, rotationMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, tempMatrix, 0, scaleMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		GLES32.glBindVertexArray(vao_cube[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		GLES32.glUseProgram(0);
		
		requestRender(); // swapbuffer
	}
}