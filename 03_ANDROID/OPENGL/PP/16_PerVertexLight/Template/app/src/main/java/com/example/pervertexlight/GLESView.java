package com.example.pervertexlight;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderShaderObject;

	// Uniforms
	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;
	private int lightAmbientUniform;
	private int lightDiffuseUniform;
	private int lightSpecularUniform;
	private int materialAmbientUniform;
	private int materialDiffuseUniform;
	private int materialSpecularUniform;
	private int materialShinessUniform;
	private int lightPositionUniform;
	private int keyLPressedUniform;
	

	// Application Variables Associated With Uniforms
	private float[] lightAmbient = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
	private float[] lightDiffuse = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightSpecular = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightPosition = new float[]{100.0f, 100.0f, 100.0f, 1.0f};

	private float[] materialAmbient = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
	private float[] materialDiffuse = new float[]{0.50f, 0.20f, 0.70f, 1.0f};
	private float[] materialSpecular = new float[]{0.70f, 0.70f, 0.70f, 1.0f};
	private float materialShiness = 50.0f;  // Also Try 128.0f

	int lKeyPressed = 0;

	private boolean isLightEnable = false;
	
	private float[] modelMatrix = new float[16]; // 4 * 4 Dimension
	private float[] viewMatrix = new float[16]; // 4 * 4 Dimension
	private float[] perspectiveProjectionMatrix = new float[16]; // 4 * 4 Dimension
	private float[] translateMatrix = new float[16]; // 4 * 4 Dimension
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	private int numVertices;
    private int numElements;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_VERSION = "+version);
		
		String glslversion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_SHADING_LANGUAGE_VERSION = "+glslversion);
		
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("RTR: onSurfaceCreated GL_VENDOR = " +vendor);
		
		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("RTR: onSurfaceCreated GL_RENDERER = " +renderer);
			 
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		System.out.println("RTR: onSurfaceChanged");
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		update();
		
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//System.out.println("RTR: "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//System.out.println("RTR: "+"Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("RTR: "+"Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR: "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		if(true == isLightEnable)
		{
			isLightEnable = false;
		}
		else
		{
			isLightEnable = true;
		}
		return(true);
	}
	
	private void initialize(GL10 gl)
	{
		System.out.println("RTR: "+"initialize start");
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform vec3 u_light_ambient;" +
			"uniform vec3 u_light_diffuse;" +
			"uniform vec3 u_light_specular;" +
			"uniform vec4 u_light_position;" +
			"uniform vec3 u_material_ambient;" +
			"uniform vec3 u_material_diffuse;" +
			"uniform vec3 u_material_specular;" +
			"uniform float u_material_shiness;" +
			"uniform int u_key_L_pressed;" +
			"out vec3 phong_ads_light;" +
			"void main(void)" +
			"{" +
				"if(1 == u_key_L_pressed)" +
				"{" +
					"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
					"vec3 t_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
					"vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));" +
					"float t_normal_dot_light_direction = max(dot(light_direction, t_normal), 0.0f);" +
					"vec3 reflection_vector = reflect(-light_direction, t_normal);" +
					"vec3 viewer_vector = normalize(vec3(-eye_coordinates));" +
					"vec3 ambient = u_light_ambient * u_material_ambient;" +
					"vec3 diffuse = u_light_diffuse * u_material_diffuse * t_normal_dot_light_direction;" +
					"vec3 specular = u_light_specular * u_material_specular * pow(max(dot(reflection_vector, viewer_vector), 0.0f), u_material_shiness);" +

					"phong_ads_light = ambient + diffuse + specular;" +
				"}" +
				"else" +
				"{" +
					"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
				"}" +
				
				"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iShaderCompiledStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(
			vertexShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println(
				"RTR: Vertex shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Vertex shader compilation successful");
		//
		// Fragment Shader
		//
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"in vec3 phong_ads_light;" +
			"uniform int u_key_L_pressed;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
				"FragColor = vec4(phong_ads_light, 1.0);" +
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0] = 0; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(
			fragmentShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println(
				"RTR: Fragement shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Fragement shader compilation successful");
		
		//
		// Shader Program Object
		//
		shaderShaderObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderShaderObject, vertexShaderObject);
		GLES32.glAttachShader(shaderShaderObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderShaderObject);
		
		int[] iShaderProgramlinkStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(
			shaderShaderObject,
			GLES32.GL_LINK_STATUS,
			iShaderProgramlinkStatus,
			0
		);
		if(iShaderProgramlinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv
			(
				shaderShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderShaderObject);
				System.out.println(
				"RTR: Shader Program Linking Log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize glLinkProgram successfully");
		
		final int size_of_float = 4;
		final int size_of_short = 2;
		
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_model_matrix");
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_view_matrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_projection_matrix");
		
		lightAmbientUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_light_ambient");
		lightDiffuseUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_light_diffuse");
		lightSpecularUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_light_specular");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_light_position");
		
		materialAmbientUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_material_ambient");
		materialDiffuseUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_material_diffuse");
		materialSpecularUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_material_specular");
		materialShinessUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_material_shiness");
		
		keyLPressedUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_key_L_pressed");

		Sphere sphere=new Sphere();
		float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		
		// vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * size_of_float);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * size_of_float,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * size_of_float);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * size_of_float,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * size_of_short);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * size_of_short,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
		System.out.println("RTR: "+"initialize end");
	}
	
	private void update()
	{
	}
	
	private void uninitialize()
	{

		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
		
		GLES32.glUseProgram(shaderShaderObject);
		
		int[] shaderCount = new int[1];
		GLES32.glGetProgramiv(shaderShaderObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		
		int[] shaders = new int[shaderCount[0]];
		
		GLES32.glGetAttachedShaders(shaderShaderObject, shaderCount[0], shaderCount, 0, shaders, 0);
		
		for(int shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
		{
				GLES32.glDetachShader(shaderShaderObject, shaders[shaderNumber]);
				
				GLES32.glDeleteShader(shaders[shaderNumber]);
				
				shaders[shaderNumber] = 0;
		}
		
		GLES32.glDeleteProgram(shaderShaderObject);
		shaderShaderObject = 0;
		
		GLES32.glUseProgram(0);
		
	}
	
	private void resize(int width, int height)
	{
		if (0 == height)
		{
			height = 1;
		}
		
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderShaderObject);
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		
		Matrix.translateM(viewMatrix, 0, 0.0f, 0.0f, -3.0f);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		if (true == isLightEnable)
		{
			GLES32.glUniform1i(keyLPressedUniform, 1);
			
			ByteBuffer lightAmbientByteBuffer=ByteBuffer.allocateDirect(lightAmbient.length * 4);
			lightAmbientByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer lightAmbientByteBufferBuffer=lightAmbientByteBuffer.asFloatBuffer();
			lightAmbientByteBufferBuffer.put(lightAmbient);
			lightAmbientByteBufferBuffer.position(0);
			GLES32.glUniform3fv(lightAmbientUniform, 1, lightAmbientByteBufferBuffer);
			
			ByteBuffer lightDiffuseByteBuffer=ByteBuffer.allocateDirect(lightDiffuse.length * 4);
			lightDiffuseByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer lightDiffuseByteBufferBuffer=lightDiffuseByteBuffer.asFloatBuffer();
			lightDiffuseByteBufferBuffer.put(lightDiffuse);
			lightDiffuseByteBufferBuffer.position(0);
			GLES32.glUniform3fv(lightDiffuseUniform, 1, lightDiffuseByteBufferBuffer);
			
			ByteBuffer lightSpecularByteBuffer=ByteBuffer.allocateDirect(lightSpecular.length * 4);
			lightSpecularByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer lightSpecularByteBufferBuffer=lightSpecularByteBuffer.asFloatBuffer();
			lightSpecularByteBufferBuffer.put(lightSpecular);
			lightSpecularByteBufferBuffer.position(0);
			GLES32.glUniform3fv(lightSpecularUniform, 1, lightSpecularByteBufferBuffer);
			
			ByteBuffer lightPositionByteBuffer=ByteBuffer.allocateDirect(lightPosition.length * 4);
			lightPositionByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer lightPositionByteBufferBuffer=lightPositionByteBuffer.asFloatBuffer();
			lightPositionByteBufferBuffer.put(lightPosition);
			lightPositionByteBufferBuffer.position(0);
			GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionByteBufferBuffer);
			
			ByteBuffer materialAmbientByteBuffer=ByteBuffer.allocateDirect(materialAmbient.length * 4);
			materialAmbientByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer materialAmbientByteBufferBuffer=materialAmbientByteBuffer.asFloatBuffer();
			materialAmbientByteBufferBuffer.put(materialAmbient);
			materialAmbientByteBufferBuffer.position(0);
			GLES32.glUniform3fv(materialAmbientUniform, 1, materialAmbientByteBufferBuffer);
			
			ByteBuffer materialDiffuseByteBuffer=ByteBuffer.allocateDirect(materialDiffuse.length * 4);
			materialDiffuseByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer materialDiffuseByteBufferBuffer=materialDiffuseByteBuffer.asFloatBuffer();
			materialDiffuseByteBufferBuffer.put(materialDiffuse);
			materialDiffuseByteBufferBuffer.position(0);
			GLES32.glUniform3fv(materialDiffuseUniform, 1, materialDiffuseByteBufferBuffer);
			
			ByteBuffer materialSpecularByteBuffer=ByteBuffer.allocateDirect(materialSpecular.length * 4);
			materialSpecularByteBuffer.order(ByteOrder.nativeOrder());
			FloatBuffer materialSpecularByteBufferBuffer=materialSpecularByteBuffer.asFloatBuffer();
			materialSpecularByteBufferBuffer.put(materialSpecular);
			materialSpecularByteBufferBuffer.position(0);
			GLES32.glUniform3fv(materialSpecularUniform, 1, materialSpecularByteBufferBuffer);
			
			GLES32.glUniform1f(materialShinessUniform, materialShiness);
		} else {
			GLES32.glUniform1i(keyLPressedUniform, 0);
		}
		
		// bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);
		
		requestRender(); // swapbuffer
	}
}