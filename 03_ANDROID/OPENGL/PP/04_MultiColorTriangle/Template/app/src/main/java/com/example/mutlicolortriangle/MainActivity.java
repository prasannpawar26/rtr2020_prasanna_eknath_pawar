package com.example.mutlicolortriangle;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.content.pm.ActivityInfo;
import android.view.View;

//import android.graphics.Color;
//import android.view.Gravity;
//import androidx.appcompat.widget.AppCompatTextView;

public class MainActivity extends AppCompatActivity {

	private GLESView glesView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setContentView(R.layout.activity_main);
		
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Set Our Own View As Our Main View
		this.getWindow().getDecorView().setSystemUiVisibility( // This Code From Pie and Onwords
			View.SYSTEM_UI_FLAG_IMMERSIVE |
			View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
			View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
			View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
			View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
			View.SYSTEM_UI_FLAG_FULLSCREEN |
			View.SYSTEM_UI_FLAG_LOW_PROFILE
		);
				
		super.onCreate(savedInstanceState);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		glesView = new GLESView(this);
		
		setContentView(glesView);
    }
}