package com.example.mutlicolortriangle;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderShaderObject;
	
	private int[] vao = new int[1];
	private int[] vbo_position = new int[1];
	private int[] vbo_color = new int[1];
	private int mvpUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_VERSION = "+version);
		
		String glslversion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_SHADING_LANGUAGE_VERSION = "+glslversion);
		
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("RTR: onSurfaceCreated GL_VENDOR = " +vendor);
		
		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("RTR: onSurfaceCreated GL_RENDERER = " +renderer);
			 
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		System.out.println("RTR: onSurfaceChanged");
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//System.out.println("RTR: "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//System.out.println("RTR: "+"Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("RTR: "+"Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR: "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	private void initialize(GL10 gl)
	{
		System.out.println("RTR: "+"initialize start");
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec4 vColor;" +
			"uniform mat4 u_mvp_matrix;" +
			"out vec4 Color;" +
			"void main(void)" +
			"{" +
				"Color = vColor;" +
				"gl_Position = u_mvp_matrix * vPosition;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iShaderCompiledStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(
			vertexShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println(
				"RTR: Vertex shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Vertex shader compilation successful");
		//
		// Fragment Shader
		//
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec4 Color;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
				"FragColor = Color;" +
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0] = 0; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(
			fragmentShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println(
				"RTR: Fragement shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Fragement shader compilation successful");
		
		//
		// Shader Program Object
		//
		shaderShaderObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderShaderObject, vertexShaderObject);
		GLES32.glAttachShader(shaderShaderObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_COLOR, "vColor");
		
		GLES32.glLinkProgram(shaderShaderObject);
		
		int[] iShaderProgramlinkStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(
			shaderShaderObject,
			GLES32.GL_LINK_STATUS,
			iShaderProgramlinkStatus,
			0
		);
		if(iShaderProgramlinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv
			(
				shaderShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderShaderObject);
				System.out.println(
				"RTR: Shader Program Linking Log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize glLinkProgram successful");
		
		mvpUniform = GLES32.glGetUniformLocation(
			shaderShaderObject,
			"u_mvp_matrix"
		);
		
		final int size_of_float = 4;
		
		final float triangleVertices[] = new float[]
		{
			0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f
		};
		
		final float triangleColor[] = new float[]
		{
			1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f
		};
		
		GLES32.glGenVertexArrays(1, vao, 0);
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glGenBuffers(1, vbo_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);	
		//
		// java JVM based ahe
		// we are passing array natively from JAVA TO JVM TO CPU
		//
		// step 1: ByteBuffer Allocate Memory Natively.
		// Step 2: Follow native cpu byte order.
		// Step 3: treate like float 
		// step 4: fill data
		// step 5: reset to ZERO the index
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * size_of_float); // 4 is for size of float
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(triangleVertices);
		verticesBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * size_of_float, verticesBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_color, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
		ByteBuffer byteBufferColor = ByteBuffer.allocateDirect(triangleColor.length * size_of_float); // 4 is for size of float
		byteBufferColor.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBufferColor.asFloatBuffer();
		colorBuffer.put(triangleColor);
		colorBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleColor.length * size_of_float, colorBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	
		GLES32.glBindVertexArray(0);
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
		System.out.println("RTR: "+"initialize end");
	}
	
	private void uninitialize()
	{
		
		
	}
	
	private void resize(int width, int height)
	{
		if (0 == height)
		{
			height = 1;
		}
		
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderShaderObject);
		
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.0f);
		
		Matrix.multiplyMM
		(
			modelViewProjectionMatrix,
			0,
			perspectiveProjectionMatrix,
			0,
			modelViewMatrix,
			0
		);
		
		GLES32.glUniformMatrix4fv
		(
			mvpUniform,
			1,
			false,
			modelViewProjectionMatrix,
			0
		);
		
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		
		GLES32.glBindVertexArray(0);
		
		GLES32.glUseProgram(0);
		
		requestRender(); // swapbuffer
	}
}