package com.example.bluescreen;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR : "+version);
		
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("RTR : "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("RTR : "+"Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("RTR : "+"Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR : "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	private void initialize(GL10 gl)
	{
		GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	}
	
	private void uninitialize()
	{
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		requestRender(); // swapbuffer
	}
}