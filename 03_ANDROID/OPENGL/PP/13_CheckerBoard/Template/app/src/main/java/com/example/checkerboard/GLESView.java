package com.example.checkerboard;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

// For Texture
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderShaderObject;
		
	private int[] vao_cube1 = new int[1];
	private int[] vbo_cube1_vertex = new int[1];
	private int[] vbo_cube1_textcoords = new int[1];

	private int[] vao_cube2 = new int[1];
	private int[] vbo_cube2_vertex = new int[1];
	private int[] vbo_cube2_textcoords = new int[1];
	
	private int[] texture_math = new int[1];

	private int mvpUniform;
	private int sampler_uniform;

	private final int checker_image_width = 64;
	private final int checker_image_height = 64;
	private byte[] checkerImage = new byte[checker_image_height * checker_image_width * 4];
	
	private float perspectiveProjectionMatrix[] = new float[16];
	
	final int size_of_float = 4;
	final int component_of_position = 3;
	final int component_of_texcoords = 2;

	int white_rectangle = 1;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_VERSION = "+version);
		
		String glslversion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_SHADING_LANGUAGE_VERSION = "+glslversion);
		
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("RTR: onSurfaceCreated GL_VENDOR = " +vendor);
		
		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("RTR: onSurfaceCreated GL_RENDERER = " +renderer);
			 
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		System.out.println("RTR: onSurfaceChanged");
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		update();
		
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//System.out.println("RTR: "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//System.out.println("RTR: "+"Single Tap");

		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("RTR: "+"Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR: "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	private void initialize(GL10 gl)
	{
		System.out.println("RTR: "+"initialize start");
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec4 vPosition;" +
			"in vec2 vTexCoord;" +
			"uniform mat4 u_mvp_matrix;" +
			"out vec2 out_texcoord;" +
			"void main(void)" +
			"{" +
				"gl_Position = u_mvp_matrix * vPosition;" +
				"out_texcoord = vTexCoord;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iShaderCompiledStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(
			vertexShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println(
				"RTR: Vertex shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Vertex shader compilation successful");
		//
		// Fragment Shader
		//
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			
			"in vec2 out_texcoord;" +
			"uniform sampler2D u_sampler;" +
			
			"out vec4 FragColor;" +
			
			"void main(void)" +
			"{" +
				"FragColor = texture(u_sampler, out_texcoord);" +
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0] = 0; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(
			fragmentShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println(
				"RTR: Fragement shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Fragement shader compilation successful");
		
		//
		// Shader Program Object
		//
		shaderShaderObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderShaderObject, vertexShaderObject);
		GLES32.glAttachShader(shaderShaderObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexCoord");
		
		GLES32.glLinkProgram(shaderShaderObject);
		
		int[] iShaderProgramlinkStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(
			shaderShaderObject,
			GLES32.GL_LINK_STATUS,
			iShaderProgramlinkStatus,
			0
		);
		if(iShaderProgramlinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv
			(
				shaderShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderShaderObject);
				System.out.println(
				"RTR: Shader Program Linking Log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize glLinkProgram successful");
		
		mvpUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_mvp_matrix");
		sampler_uniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_sampler");
		
		//
		// Cube1
		//
		GLES32.glGenVertexArrays(1, vao_cube1, 0);
		GLES32.glBindVertexArray(vao_cube1[0]);
		
		GLES32.glGenBuffers(1, vbo_cube1_vertex, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube1_vertex[0]);
		//
		// java JVM based ahe
		// we are passing array natively from JAVA TO JVM TO CPU
		//
		// step 1: ByteBuffer Allocate Memory Natively.
		// Step 2: Follow native cpu byte order.
		// Step 3: treate like float 
		// step 4: fill data
		// step 5: reset to ZERO the index
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 12 * size_of_float, null, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, component_of_position, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	
		final float cubeTexCoords1[] = new float[]
		{
			// Right Top
			1.0f, 1.0f,
			// Left Top
			0.0f, 1.0f,
			// Right Top
			0.0f, 0.0f,
			// Right Top
			1.0f, 0.0f
		};
		
		GLES32.glGenBuffers(1, vbo_cube1_textcoords, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube1_textcoords[0]);	
		ByteBuffer byteBufferCube1Texcoords = ByteBuffer.allocateDirect(cubeTexCoords1.length * size_of_float); // 4 is for size of float
		byteBufferCube1Texcoords.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferCube1Texcoords = byteBufferCube1Texcoords.asFloatBuffer();
		floatBufferCube1Texcoords.put(cubeTexCoords1);
		floatBufferCube1Texcoords.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexCoords1.length * size_of_float, floatBufferCube1Texcoords, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0, component_of_texcoords, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		//
		// Cube2
		//
		final float cubeTexCoords2[] = new float[]
		{
			// Right Top
			1.0f, 1.0f,
			// Left Top
			0.0f, 1.0f,
			// Right Top
			0.0f, 0.0f,
			// Right Top
			1.0f, 0.0f
		};
		
		GLES32.glGenVertexArrays(1, vao_cube2, 0);
		GLES32.glBindVertexArray(vao_cube2[0]);
		GLES32.glGenBuffers(1, vbo_cube2_vertex, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube2_vertex[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 12 * size_of_float, null, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, component_of_position, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glGenBuffers(1, vbo_cube2_textcoords, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube2_textcoords[0]);
		ByteBuffer byteBufferCube2Texcoords = ByteBuffer.allocateDirect(cubeTexCoords2.length * size_of_float); // 4 is for size of float
		byteBufferCube2Texcoords.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferCube2Texcoords = byteBufferCube2Texcoords.asFloatBuffer();
		floatBufferCube2Texcoords.put(cubeTexCoords2);
		floatBufferCube2Texcoords.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexCoords2.length * size_of_float, floatBufferCube2Texcoords, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0, component_of_texcoords, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
		texture_math[0] = loadGLTexture();
		
		System.out.println("RTR: "+"initialize end");
	}
	
	private void update()
	{
	}
	
	private int loadGLTexture()
	{
		int[] texture = new int[1];
		
		MakeCheckerImage();
		
		// Converting Array To Such A Buffer Which Can Pass To Buffer Data
		ByteBuffer textureByteBuffer = ByteBuffer.allocateDirect(checker_image_height * checker_image_width * 4);
		textureByteBuffer.order(ByteOrder.nativeOrder());
		textureByteBuffer.put(checkerImage);
		textureByteBuffer.position(0);
						
		Bitmap bitmap = Bitmap.createBitmap(checker_image_width, checker_image_height, Bitmap.Config.ARGB_8888);
		
		bitmap.copyPixelsFromBuffer(textureByteBuffer);
				
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		
		GLES32.glGenTextures(1, texture, 0);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);
		
		// texImage2D  internally calls glTexImage2D
		// Parameter  "3, 4, 5, 8, 9" of glTexImage2D => 3rd Parameter of texImage2D
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		return texture[0];
	}
	private void MakeCheckerImage()
	{
		int i, j, c;
		
		for(i = 0; i < checker_image_height; i++)
		{
			for(j = 0; j < checker_image_width; j++)
			{
				c = ((i & 0x8) ^ (j & 0x8)) * 255;
				checkerImage[(i * 64 +j) * 4 +0] = (byte)c;
				checkerImage[(i * 64 +j) * 4 +1] = (byte)c;
				checkerImage[(i * 64 +j) * 4 +2] = (byte)c;
				checkerImage[(i * 64 +j) * 4 +3] = (byte)0xFF;
			}
		}
		
		return;
	}
	
	private void uninitialize()
	{

		GLES32.glDeleteTextures(1, texture_math, 0);
		
		
		if(0 != vbo_cube2_textcoords[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube2_textcoords, 0);
			vbo_cube2_textcoords[0] = 0;
		}
		
		if(0 != vbo_cube2_vertex[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube2_vertex, 0);
			vbo_cube2_vertex[0] = 0;
		}
		
		if(0 != vao_cube2[0])
		{
			GLES32.glDeleteVertexArrays(1, vao_cube2, 0);
			vao_cube2[0] = 0;
		}
		
		if(0 != vbo_cube1_textcoords[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube1_textcoords, 0);
			vbo_cube1_textcoords[0] = 0;
		}
		
		if(0 != vbo_cube1_vertex[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube1_vertex, 0);
			vbo_cube1_vertex[0] = 0;
		}
		
		if(0 != vao_cube1[0])
		{
			GLES32.glDeleteVertexArrays(1, vao_cube1, 0);
			vao_cube1[0] = 0;
		}
		
		GLES32.glUseProgram(shaderShaderObject);
		
		int[] shaderCount = new int[1];
		GLES32.glGetProgramiv(shaderShaderObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		
		int[] shaders = new int[shaderCount[0]];
		
		GLES32.glGetAttachedShaders(shaderShaderObject, shaderCount[0], shaderCount, 0, shaders, 0);
		
		for(int shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
		{
				GLES32.glDetachShader(shaderShaderObject, shaders[shaderNumber]);
				
				GLES32.glDeleteShader(shaders[shaderNumber]);
				
				shaders[shaderNumber] = 0;
		}
		
		GLES32.glDeleteProgram(shaderShaderObject);
		shaderShaderObject = 0;
		
		GLES32.glUseProgram(0);
		
	}
	
	private void resize(int width, int height)
	{
		if (0 == height)
		{
			height = 1;
		}
		
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderShaderObject);
		
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glUniform1i(sampler_uniform, 0);

		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -6.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
		
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_math[0]);

		//cube -1
		GLES32.glBindVertexArray(vao_cube1[0]);
		
		final float[] cube1Vertex = new float[]
		{
			// Right-Top
			0.0f, 1.0f, 0.0f,
			// Left-Top
			-2.0f, 1.0f, 0.0f,
			// Left-Bottom
			-2.0f, -1.0f, 0.0f,
			// Right-Bottom
			0.0f, -1.0f, 0.0f
		};
			
		// Converting Array To Such A Buffer Which Can Pass To Buffer Data
		ByteBuffer cube1ByteBuffer = ByteBuffer.allocateDirect(cube1Vertex.length * 4);
		cube1ByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cube1PositionBuffer = cube1ByteBuffer.asFloatBuffer();
		cube1PositionBuffer.put(cube1Vertex);
		cube1PositionBuffer.position(0);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube1_vertex[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube1Vertex.length * size_of_float, cube1PositionBuffer, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		
		GLES32.glBindVertexArray(0);

		//cube -2
		GLES32.glBindVertexArray(vao_cube2[0]);
		
		final float[] cube2Vertex = new float[]
		{
			// Right-Top
			2.41421f, 1.0f, -1.41421f,
			// Left-Top
			1.0f, 1.0f, 0.0f,
			// Left-Bottom
			1.0f, -1.0f, 0.0f,
			// Right-Bottom
			2.41421f, -1.0f, -1.41421f
		};
			
		// Converting Array To Such A Buffer Which Can Pass To Buffer Data
		ByteBuffer cube2ByteBuffer = ByteBuffer.allocateDirect(cube2Vertex.length * 4);
		cube2ByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cube2PositionBuffer = cube2ByteBuffer.asFloatBuffer();
		cube2PositionBuffer.put(cube2Vertex);
		cube2PositionBuffer.position(0);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube2_vertex[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube2Vertex.length * 4, cube2PositionBuffer, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		GLES32.glUseProgram(0);
		
		requestRender(); // swapbuffer
	}
}