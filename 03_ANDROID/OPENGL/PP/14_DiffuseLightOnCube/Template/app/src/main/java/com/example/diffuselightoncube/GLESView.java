package com.example.diffuselightoncube;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderShaderObject;
	
	private int[] vao_cube = new int[1];
	private int[] vbo_cube_position = new int[1];
	private int[] vbo_cube_normal = new int[1];
	private float cube_rotation;

	private int modelViewMatrixUniform;
	private int projectionMatrixUniform;
	private int lightDiffuseUniform;
	private int materialDiffuseUniform;
	private int lightPositionUniform;
	private int keyLPressedUniform;

	private boolean isLightEnable = false;
	
	private float perspectiveProjectionMatrix[] = new float[16];
	
	final int size_of_float = 4;
	final int component_of_position = 3;
	final int component_of_normal = 3;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_VERSION = "+version);
		
		String glslversion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_SHADING_LANGUAGE_VERSION = "+glslversion);
		
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("RTR: onSurfaceCreated GL_VENDOR = " +vendor);
		
		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("RTR: onSurfaceCreated GL_RENDERER = " +renderer);
			 
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		System.out.println("RTR: onSurfaceChanged");
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		update();
		
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//System.out.println("RTR: "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//System.out.println("RTR: "+"Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("RTR: "+"Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR: "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		if(true == isLightEnable)
		{
			isLightEnable = false;
		}
		else
		{
			isLightEnable = true;
		}
		return(true);
	}
	
	private void initialize(GL10 gl)
	{
		System.out.println("RTR: "+"initialize start");
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_modelview_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform int u_key_L_pressed;" + 
			"uniform vec3 u_light_diffuse;" +
			"uniform vec3 u_material_diffuse;" +
			"uniform vec4 u_light_position;" +
			"out vec3 diffuse_color;" +
			"void main(void)" +
			"{" +
				"if(1 == u_key_L_pressed)" +
				"{" +
					"vec4 eye_coordinates = u_modelview_matrix * vPosition;" +
					"mat3 normalMatrix = mat3(transpose(inverse(u_modelview_matrix)));" +
					"vec3 t_normal = normalize(normalMatrix * vNormal);" +
					"vec3 lightSource = vec3(vec3(u_light_position) - eye_coordinates.xyz);" +
					"diffuse_color = u_light_diffuse * u_material_diffuse * max(dot(lightSource, t_normal), 0.0);" +
				"}" +
				"gl_Position = u_projection_matrix * u_modelview_matrix * vPosition;" +
			"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		
		int[] iShaderCompiledStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(
			vertexShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println(
				"RTR: Vertex shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Vertex shader compilation successful");
		//
		// Fragment Shader
		//
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"in vec3 diffuse_color;" +
			"uniform int u_key_L_pressed;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
				"if(1 == u_key_L_pressed)" +
				"{" +
					"FragColor = vec4(diffuse_color, 1.0);" +
				"}" +
				"else" +
				"{" +
					"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
				"}" +
			"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		
		iShaderCompiledStatus[0] = 0; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(
			fragmentShaderObject,
			GLES32.GL_COMPILE_STATUS,
			iShaderCompiledStatus,
			0
		);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(
				fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println(
				"RTR: Fragement shader compilation log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize Fragement shader compilation successful");
		
		//
		// Shader Program Object
		//
		shaderShaderObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderShaderObject, vertexShaderObject);
		GLES32.glAttachShader(shaderShaderObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderShaderObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderShaderObject);
		
		int[] iShaderProgramlinkStatus = new int[1]; // pointer sarkhe use karyla aapan 1 ch array banvtoy
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(
			shaderShaderObject,
			GLES32.GL_LINK_STATUS,
			iShaderProgramlinkStatus,
			0
		);
		if(iShaderProgramlinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv
			(
				shaderShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,
				iInfoLogLength,
				0
			);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderShaderObject);
				System.out.println(
				"RTR: Shader Program Linking Log = "+ szInfoLog
				);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("RTR: "+"initialize glLinkProgram successful");
		
		modelViewMatrixUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_modelview_matrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_projection_matrix");
		lightDiffuseUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_light_diffuse");
		materialDiffuseUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_material_diffuse");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_light_position");
		keyLPressedUniform = GLES32.glGetUniformLocation(shaderShaderObject, "u_key_L_pressed");

		
		//
		// Cube
		//
		final float cubeVertices[] = new float[]
		{
			// top
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f, 
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,  

			// bottom
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			1.0f, -1.0f,  1.0f,

			// front
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// back
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// right
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// left
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 
			-1.0f, -1.0f, -1.0f, 
			-1.0f, -1.0f, 1.0f
		};
		
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glGenBuffers(1, vbo_cube_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]);	
		//
		// java JVM based ahe
		// we are passing array natively from JAVA TO JVM TO CPU
		//
		// step 1: ByteBuffer Allocate Memory Natively.
		// Step 2: Follow native cpu byte order.
		// Step 3: treate like float 
		// step 4: fill data
		// step 5: reset to ZERO the index
		ByteBuffer byteBufferCubePosition = ByteBuffer.allocateDirect(cubeVertices.length * size_of_float); // 4 is for size of float
		byteBufferCubePosition.order(ByteOrder.nativeOrder());
		FloatBuffer floatBufferCubePosition = byteBufferCubePosition.asFloatBuffer();
		floatBufferCubePosition.put(cubeVertices);
		floatBufferCubePosition.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * size_of_float, floatBufferCubePosition, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX, component_of_position, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
	
		// color
		final float[] cubeNormals = new float[]
		{
			0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
			0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

			0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,
			0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,

			0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
			0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,

			0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,
			0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,

			1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
			1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

			-1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,
			-1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f
		};
		
		GLES32.glGenBuffers(1, vbo_cube_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_normal[0]);
		
		ByteBuffer cubeByteBuffer_Normals = ByteBuffer.allocateDirect(cubeNormals.length * 4);
		cubeByteBuffer_Normals.order(ByteOrder.nativeOrder());
		FloatBuffer cubeNormalBuffer = cubeByteBuffer_Normals.asFloatBuffer();
		cubeNormalBuffer.put(cubeNormals);
		cubeNormalBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeNormals.length * size_of_float, cubeNormalBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL, component_of_normal , GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
		System.out.println("RTR: "+"initialize end");
	}
	
	private void update()
	{
		cube_rotation += 1.0f;
		if(cube_rotation > 360.0f)
		{
			cube_rotation = 0.0f;
		}
		
	}
	
	private void uninitialize()
	{
		if(0 != vbo_cube_normal[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube_normal, 0);
			vbo_cube_normal[0] = 0;
		}

		if(0 != vbo_cube_position[0])
		{
			GLES32.glDeleteBuffers(1, vbo_cube_position, 0);
			vbo_cube_position[0] = 0;
		}
		
		if(0 != vao_cube[0])
		{
			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0] = 0;
		}
		
		GLES32.glUseProgram(shaderShaderObject);
		
		int[] shaderCount = new int[1];
		GLES32.glGetProgramiv(shaderShaderObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		
		int[] shaders = new int[shaderCount[0]];
		
		GLES32.glGetAttachedShaders(shaderShaderObject, shaderCount[0], shaderCount, 0, shaders, 0);
		
		for(int shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
		{
				GLES32.glDetachShader(shaderShaderObject, shaders[shaderNumber]);
				
				GLES32.glDeleteShader(shaders[shaderNumber]);
				
				shaders[shaderNumber] = 0;
		}
		
		GLES32.glDeleteProgram(shaderShaderObject);
		shaderShaderObject = 0;
		
		GLES32.glUseProgram(0);
		
	}
	
	private void resize(int width, int height)
	{
		if (0 == height)
		{
			height = 1;
		}
		
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderShaderObject);
		
		float modelViewMatrix[] = new float[16];
		float translationMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		// 
		// triangle
		//
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -7.0f);
		Matrix.setRotateM(rotationMatrix, 0, cube_rotation, 1.0f, 0.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, cube_rotation, 0.0f, 1.0f, 0.0f);
		Matrix.rotateM(rotationMatrix, 0, cube_rotation, 0.0f, 0.0f, 1.0f);
		
		Matrix.multiplyMM(modelViewMatrix, 0, translationMatrix, 0, rotationMatrix, 0);
		GLES32.glUniformMatrix4fv
		(
			modelViewMatrixUniform,
			1,
			false,
			modelViewMatrix,
			0
		);
		GLES32.glUniformMatrix4fv
		(
			projectionMatrixUniform,
			1,
			false,
			perspectiveProjectionMatrix,
			0
		);
		
		if (true == isLightEnable)
		{
			GLES32.glUniform1i(keyLPressedUniform, 1);
			GLES32.glUniform3f(lightDiffuseUniform, 1.0f, 1.0f, 1.0f);
			GLES32.glUniform3f(materialDiffuseUniform, 0.50f, 0.50f, 0.50f);
			GLES32.glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
		}
		else
		{
			GLES32.glUniform1i(keyLPressedUniform, 0);
		}
		
		GLES32.glBindVertexArray(vao_cube[0]);
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		
		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);
		
		requestRender(); // swapbuffer
	}
}