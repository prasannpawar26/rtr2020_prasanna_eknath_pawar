package com.example.toggleshaders;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView; // for opengl surface view and all related
import javax.microedition.khronos.opengles.GL10; // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context; // final in java = const in c/c++.
	private GestureDetector gestureDetector;
	
	private int perVertex_VertexShaderObject;
	private int perVertex_FragmentShaderObject;
	private int perVertex_ShaderProgramObject;
	
	private int perFragment_VertexShaderObject;
	private int perFragment_FragmentShaderObject;
	private int perFragment_ShaderProgramObject;
	
	// Uniforms
	private int perVertex_modelMatrixUniform;
	private int perVertex_viewMatrixUniform;
	private int perVertex_projectionMatrixUniform;
	private int perVertex_lightAmbientUniform;
	private int perVertex_lightDiffuseUniform;
	private int perVertex_lightSpecularUniform;
	private int perVertex_materialAmbientUniform;
	private int perVertex_materialDiffuseUniform;
	private int perVertex_materialSpecularUniform;
	private int perVertex_materialShinessUniform;
	private int perVertex_lightPositionUniform;
	private int perVertex_keyLPressedUniform;
	
	private int perFragment_modelMatrixUniform;
	private int perFragment_viewMatrixUniform;
	private int perFragment_projectionMatrixUniform;
	private int perFragment_lightAmbientUniform;
	private int perFragment_lightDiffuseUniform;
	private int perFragment_lightSpecularUniform;
	private int perFragment_materialAmbientUniform;
	private int perFragment_materialDiffuseUniform;
	private int perFragment_materialSpecularUniform;
	private int perFragment_materialShinessUniform;
	private int perFragment_lightPositionUniform;
	private int perFragment_keyLPressedUniform;

	// Application Variables Associated With Uniforms
	private float[] lightAmbient = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
	private float[] lightDiffuse = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightSpecular = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightPosition = new float[]{100.0f, 100.0f, 100.0f, 1.0f};

	private float[] materialAmbient = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
	private float[] materialDiffuse = new float[]{0.50f, 0.20f, 0.70f, 1.0f};
	private float[] materialSpecular = new float[]{0.70f, 0.70f, 0.70f, 1.0f};
	private float materialShiness = 50.0f;  // Also Try 128.0f

	int lKeyPressed = 0;

	private boolean isLightEnable = false;
	private boolean isPerFragment = false;
	
	private float[] modelMatrix = new float[16]; // 4 * 4 Dimension
	private float[] viewMatrix = new float[16]; // 4 * 4 Dimension
	private float[] perspectiveProjectionMatrix = new float[16]; // 4 * 4 Dimension
	private float[] translateMatrix = new float[16]; // 4 * 4 Dimension
	
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	private int numVertices;
    private int numElements;

	private final String preVertex_VertexShaderSourceCode =
	String.format(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"precision lowp int;" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +

		"struct matrices {" +
			"mat4 model_matrix;" +
			"mat4 view_matrix;" +
			"mat4 projection_matrix;" +
		"};" +
		"uniform matrices u_matrices;" +

		"struct light_configuration {" +
			"vec3 light_ambient;" +
			"vec3 light_diffuse;" +
			"vec3 light_specular;" +
			"vec4 light_position;" +
		"};" +
		"uniform light_configuration u_light_configuration;" +

		"struct material_configuration {" +
			"vec3 material_ambient;" +
			"vec3 material_diffuse;" +
			"vec3 material_specular;" +
			"float material_shiness;" +
		"};" +
		"uniform material_configuration u_material_configuration;" +

		"uniform int u_key_L_pressed;" +
		"out vec3 phong_ads_light;" +
		
		"void main(void)" +
		"{" +
			"if(1 == u_key_L_pressed)" +
			"{" +
				"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
				"vec3 t_normal = normalize(mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal);" +
				"vec3 light_direction = normalize(vec3(u_light_configuration.light_position - eye_coordinates));" +
				"float t_normal_dot_light_direction = max(dot(light_direction, t_normal), 0.0f);" +
				"vec3 reflection_vector = reflect(-light_direction, t_normal);" +
				"vec3 viewer_vector = normalize(vec3(-eye_coordinates));" +
				"vec3 ambient = u_light_configuration.light_ambient * u_material_configuration.material_ambient;" +
				"vec3 diffuse = u_light_configuration.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction;" +
				"vec3 specular = u_light_configuration.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector, viewer_vector), 0.0f), u_material_configuration.material_shiness);" +
				"phong_ads_light = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
				"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
			"}" +

		"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
	  "}");
	
	private final String preVertex_FragmentShaderSourceCode =
	String.format(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"precision lowp int;" +
		"in vec3 phong_ads_light;" +
		"uniform int u_key_L_pressed;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = vec4(phong_ads_light, 1.0);" +
		"}");
	
	private final String preFragment_VertexShaderSourceCode =
		String.format(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"precision lowp int;" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"struct matrices {" +
			"mat4 model_matrix;" +
			"mat4 view_matrix;" +
			"mat4 projection_matrix;" +
		"};" +
		"uniform matrices u_matrices;" +
		"uniform int u_key_L_pressed;" +
		"struct light_configuration {" +
			"vec3 light_ambient;" +
			"vec3 light_diffuse;" +
			"vec3 light_specular;" +
			"vec4 light_position;" +
		"};" +
		"uniform light_configuration u_light_configuration;" +
  
		"out vec3 light_direction;" +
		"out vec3 tranformation_matrix;" +
		"out vec3 viewer_vector;" +
		
		"void main(void)" +
		"{" +
			"if(1 == u_key_L_pressed)" +
			"{" +
				"vec4 eye_coordinates = u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
				"tranformation_matrix = mat3(u_matrices.view_matrix * u_matrices.model_matrix) * vNormal;" +
				"light_direction = vec3(u_light_configuration.light_position - eye_coordinates);" +
				"viewer_vector = vec3(-eye_coordinates);" +
			"}" +

		"gl_Position = u_matrices.projection_matrix * u_matrices.view_matrix * u_matrices.model_matrix * vPosition;" +
	"}");
		
	// "precision highp float" : In Embedded Memory And Power Is Not Luxary(RISC ARCH.), Hence To Save These Need To Use Right Precision At Right Place.
	// Maximum Processing Is Happened In Fragment Shader , Hence This Line Is Added In Fragment Shader.
	private final String preFragment_FragmentShaderSourceCode =
	String.format(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"precision lowp int;" +
		"in vec3 light_direction;" +
		"in vec3 tranformation_matrix;" +
		"in vec3 viewer_vector;" +
		"uniform int u_key_L_pressed;" +
		"struct light_configuration {" +
			"vec3 light_ambient;" +
			"vec3 light_diffuse;" +
			"vec3 light_specular;" +
			"vec4 light_position;" +
		"};" +
		"uniform light_configuration u_light_configuration;" +

		"struct material_configuration {" +
			"vec3 material_ambient;" +
			"vec3 material_diffuse;" +
			"vec3 material_specular;" +
			"float material_shiness;" +
		"};" +
		"uniform material_configuration u_material_configuration;" +

		"out vec4 FragColor;" +
		"vec3 phong_ads_light;" +
		"void main(void)" +
		"{" +
			"if(1 == u_key_L_pressed)" +
			"{" +
				"vec3 light_direction_normalize = normalize(light_direction);" +
				"vec3 tranformation_matrix_normalize = normalize(tranformation_matrix);" +
				"vec3 reflection_vector = reflect(-light_direction_normalize, tranformation_matrix_normalize);" +
				"vec3 viewer_vector_normal = normalize(viewer_vector);" +
				"float t_normal_dot_light_direction = max(dot(light_direction_normalize, tranformation_matrix_normalize), 0.0f);" +
				"vec3 ambient = u_light_configuration.light_ambient * u_material_configuration.material_ambient;" +
				"vec3 diffuse = u_light_configuration.light_diffuse * u_material_configuration.material_diffuse * t_normal_dot_light_direction;" +
				"vec3 specular = u_light_configuration.light_specular * u_material_configuration.material_specular * pow(max(dot(reflection_vector, viewer_vector_normal), 0.0f), u_material_configuration.material_shiness);" +
				"phong_ads_light= ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
				"phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" +
			"}" +

			"FragColor = vec4(phong_ads_light, 1.0);" +
	"}");
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		// Accordingly set EGLContext to current supported version of OpenGLES.
		setEGLContextClientVersion(3); //3 => //opengl che main version
		
		// Set renderer for drawing on the GLSurfaceView
		setRenderer(this);
		
		// Render the view only when there is change in the drawing data
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY): 
		// 
		// RePaint When RenderMode is Dirty.
		// Because Of This Call We Are Directly Jump Into GameLoop. GameLoop Is In NDK (We Dont Write GameLoop In Android)
		// 
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
		gestureDetector =
			new GestureDetector(
				context,
				this, // We want Listener
				null, // We Dont want Handler
				false
				);
				
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// OpenGL-ES version check
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_VERSION = "+version);
		
		String glslversion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: onSurfaceCreated GL_SHADING_LANGUAGE_VERSION = "+glslversion);
		
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("RTR: onSurfaceCreated GL_VENDOR = " +vendor);
		
		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("RTR: onSurfaceCreated GL_RENDERER = " +renderer);
			 
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)  // == Resize
	{
		System.out.println("RTR: onSurfaceChanged");
		resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		update();
		
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		} 
		
		return(true) ;
	} // onTouchEvent
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//System.out.println("RTR: "+"Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//System.out.println("RTR: "+"Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		if(true == isLightEnable)
		{
			isLightEnable = false;
		}
		else
		{
			isLightEnable = true;
		}
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.out.println("RTR: "+"On Scroll");
		System.exit(0); // successful exit
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		if(false == isPerFragment)
		{
			final int[] validateStatus = new int[1];
			GLES32.glGetProgramiv(perFragment_ShaderProgramObject, GLES32.GL_VALIDATE_STATUS, validateStatus, 0);
			System.out.println("RTR: validating program: " + validateStatus[0]);
			//initialize_perfragment_shaders();
			isPerFragment = true;
			System.out.println("RTR: preFragment");
		}
		else
		{
			final int[] validateStatus = new int[1];
			GLES32.glGetProgramiv(perVertex_ShaderProgramObject, GLES32.GL_VALIDATE_STATUS, validateStatus, 0);
			System.out.println("RTR: validating program: " + validateStatus[0]);
			//initialize_pervertex_shaders();
			isPerFragment = false;
			System.out.println("RTR: preVertex");
		}
		return(true);
	}
	
	private void initialize_pervertex_shaders()
	{
				
		perVertex_VertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		perVertex_FragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		perVertex_ShaderProgramObject = GLES32.glCreateProgram();
				
		GLES32.glShaderSource(perVertex_VertexShaderObject, preVertex_VertexShaderSourceCode);	
		GLES32.glCompileShader(perVertex_VertexShaderObject);
		
		GLES32.glShaderSource(perVertex_FragmentShaderObject, preVertex_FragmentShaderSourceCode);
		GLES32.glCompileShader(perVertex_FragmentShaderObject);
		
		GLES32.glAttachShader(perVertex_ShaderProgramObject, perVertex_VertexShaderObject);
		GLES32.glAttachShader(perVertex_ShaderProgramObject, perVertex_FragmentShaderObject);
		GLES32.glBindAttribLocation(perVertex_ShaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(perVertex_ShaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(perVertex_ShaderProgramObject);
				
		perVertex_modelMatrixUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_matrices.model_matrix");
		perVertex_viewMatrixUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_matrices.view_matrix");
		perVertex_projectionMatrixUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_matrices.projection_matrix");
		
		perVertex_lightAmbientUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_light_configuration.light_ambient");
		perVertex_lightDiffuseUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_light_configuration.light_diffuse");
		perVertex_lightSpecularUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_light_configuration.light_specular");
		perVertex_lightPositionUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_light_configuration.light_position");
		
		perVertex_materialAmbientUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_material_configuration.material_ambient");
		perVertex_materialDiffuseUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_material_configuration.material_diffuse");
		perVertex_materialSpecularUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_material_configuration.material_specular");
		perVertex_materialShinessUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_material_configuration.material_shiness");
		
		perVertex_keyLPressedUniform = GLES32.glGetUniformLocation(perVertex_ShaderProgramObject, "u_key_L_pressed");
		
	}
	
	private void initialize_perfragment_shaders()
	{
				
		perFragment_VertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		perFragment_FragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		perFragment_ShaderProgramObject = GLES32.glCreateProgram();
				
		GLES32.glShaderSource(perFragment_VertexShaderObject, preFragment_VertexShaderSourceCode);	
		GLES32.glCompileShader(perFragment_VertexShaderObject);
		
		GLES32.glShaderSource(perFragment_FragmentShaderObject, preFragment_FragmentShaderSourceCode);
		GLES32.glCompileShader(perFragment_FragmentShaderObject);
		
		GLES32.glAttachShader(perFragment_ShaderProgramObject, perFragment_VertexShaderObject);
		GLES32.glAttachShader(perFragment_ShaderProgramObject, perFragment_FragmentShaderObject);
		GLES32.glBindAttribLocation(perFragment_ShaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(perFragment_ShaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(perFragment_ShaderProgramObject);
				
		perFragment_modelMatrixUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_matrices.model_matrix");
		perFragment_viewMatrixUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_matrices.view_matrix");
		perFragment_projectionMatrixUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_matrices.projection_matrix");
		
		perFragment_lightAmbientUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_light_configuration.light_ambient");
		perFragment_lightDiffuseUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_light_configuration.light_diffuse");
		perFragment_lightSpecularUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_light_configuration.light_specular");
		perFragment_lightPositionUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_light_configuration.light_position");
		
		perFragment_materialAmbientUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_material_configuration.material_ambient");
		perFragment_materialDiffuseUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_material_configuration.material_diffuse");
		perFragment_materialSpecularUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_material_configuration.material_specular");
		perFragment_materialShinessUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_material_configuration.material_shiness");
		
		perFragment_keyLPressedUniform = GLES32.glGetUniformLocation(perFragment_ShaderProgramObject, "u_key_L_pressed");
		
	}
	
	private void initialize(GL10 gl)
	{
		
		final int size_of_float = 4;
		final int size_of_short = 2;
		
		initialize_pervertex_shaders();
		initialize_perfragment_shaders();

		Sphere sphere=new Sphere();
		float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		
		// vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * size_of_float);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * size_of_float,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * size_of_float);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * size_of_float,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * size_of_short);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * size_of_short,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
		
		System.out.println("RTR: "+"initialize end");
	}
	
	private void update()
	{
	}
	
	private void uninitialize_pervertex_shaders()
	{
		GLES32.glUseProgram(perVertex_ShaderProgramObject);
		
		int[] shaderCount = new int[1];
		GLES32.glGetProgramiv(perVertex_ShaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		
		int[] shaders = new int[shaderCount[0]];
		GLES32.glGetAttachedShaders(perVertex_ShaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
		
		for(int shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
		{
				GLES32.glDetachShader(perVertex_ShaderProgramObject, shaders[shaderNumber]);
				GLES32.glDeleteShader(shaders[shaderNumber]);
		}
				
		GLES32.glDeleteProgram(perVertex_ShaderProgramObject);
		perVertex_ShaderProgramObject = 0;
		
		GLES32.glUseProgram(0);
	}
	
	private void uninitialize_perfragment_shaders()
	{
		GLES32.glUseProgram(perFragment_ShaderProgramObject);
		
		int[] shaderCount = new int[1];
		GLES32.glGetProgramiv(perFragment_ShaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
		
		int[] shaders = new int[shaderCount[0]];
		GLES32.glGetAttachedShaders(perFragment_ShaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);
		
		for(int shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
		{
				GLES32.glDetachShader(perFragment_ShaderProgramObject, shaders[shaderNumber]);
				GLES32.glDeleteShader(shaders[shaderNumber]);
		}
		
		GLES32.glDeleteProgram(perFragment_ShaderProgramObject);
		perFragment_ShaderProgramObject = 0;
		
		GLES32.glUseProgram(0);
	}
	
	private void uninitialize()
	{

		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
		
		uninitialize_pervertex_shaders();
		uninitialize_perfragment_shaders();
		
	}
	
	private void resize(int width, int height)
	{
		if (0 == height)
		{
			height = 1;
		}
		
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		if(false == isPerFragment) {
			GLES32.glUseProgram(perVertex_ShaderProgramObject);	
		} else {
			GLES32.glUseProgram(perFragment_ShaderProgramObject);
		}
		
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(translateMatrix, 0);
		
		
		Matrix.translateM(viewMatrix, 0, 0.0f, 0.0f, -3.0f);
		
		if(false == isPerFragment)
		{
			GLES32.glUniformMatrix4fv(perVertex_modelMatrixUniform, 1, false, modelMatrix, 0);
			GLES32.glUniformMatrix4fv(perVertex_viewMatrixUniform, 1, false, viewMatrix, 0);
			GLES32.glUniformMatrix4fv(perVertex_projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		}
		else
		{
			GLES32.glUniformMatrix4fv(perFragment_modelMatrixUniform, 1, false, modelMatrix, 0);
			GLES32.glUniformMatrix4fv(perFragment_viewMatrixUniform, 1, false, viewMatrix, 0);
			GLES32.glUniformMatrix4fv(perFragment_projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		}

		if (true == isLightEnable)
		{
			if(false == isPerFragment)
			{
				GLES32.glUniform1i(perVertex_keyLPressedUniform, 1);
				
				ByteBuffer lightAmbientByteBuffer=ByteBuffer.allocateDirect(lightAmbient.length * 4);
				lightAmbientByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightAmbientByteBufferBuffer=lightAmbientByteBuffer.asFloatBuffer();
				lightAmbientByteBufferBuffer.put(lightAmbient);
				lightAmbientByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perVertex_lightAmbientUniform, 1, lightAmbientByteBufferBuffer);
				
				ByteBuffer lightDiffuseByteBuffer=ByteBuffer.allocateDirect(lightDiffuse.length * 4);
				lightDiffuseByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightDiffuseByteBufferBuffer=lightDiffuseByteBuffer.asFloatBuffer();
				lightDiffuseByteBufferBuffer.put(lightDiffuse);
				lightDiffuseByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perVertex_lightDiffuseUniform, 1, lightDiffuseByteBufferBuffer);
				
				ByteBuffer lightSpecularByteBuffer=ByteBuffer.allocateDirect(lightSpecular.length * 4);
				lightSpecularByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightSpecularByteBufferBuffer=lightSpecularByteBuffer.asFloatBuffer();
				lightSpecularByteBufferBuffer.put(lightSpecular);
				lightSpecularByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perVertex_lightSpecularUniform, 1, lightSpecularByteBufferBuffer);
				
				ByteBuffer lightPositionByteBuffer=ByteBuffer.allocateDirect(lightPosition.length * 4);
				lightPositionByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightPositionByteBufferBuffer=lightPositionByteBuffer.asFloatBuffer();
				lightPositionByteBufferBuffer.put(lightPosition);
				lightPositionByteBufferBuffer.position(0);
				GLES32.glUniform4fv(perVertex_lightPositionUniform, 1, lightPositionByteBufferBuffer);
				
				ByteBuffer materialAmbientByteBuffer=ByteBuffer.allocateDirect(materialAmbient.length * 4);
				materialAmbientByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer materialAmbientByteBufferBuffer=materialAmbientByteBuffer.asFloatBuffer();
				materialAmbientByteBufferBuffer.put(materialAmbient);
				materialAmbientByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perVertex_materialAmbientUniform, 1, materialAmbientByteBufferBuffer);
				
				ByteBuffer materialDiffuseByteBuffer=ByteBuffer.allocateDirect(materialDiffuse.length * 4);
				materialDiffuseByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer materialDiffuseByteBufferBuffer=materialDiffuseByteBuffer.asFloatBuffer();
				materialDiffuseByteBufferBuffer.put(materialDiffuse);
				materialDiffuseByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perVertex_materialDiffuseUniform, 1, materialDiffuseByteBufferBuffer);
				
				ByteBuffer materialSpecularByteBuffer=ByteBuffer.allocateDirect(materialSpecular.length * 4);
				materialSpecularByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer materialSpecularByteBufferBuffer=materialSpecularByteBuffer.asFloatBuffer();
				materialSpecularByteBufferBuffer.put(materialSpecular);
				materialSpecularByteBufferBuffer.position(0);	
				GLES32.glUniform3fv(perVertex_materialSpecularUniform, 1, materialSpecularByteBufferBuffer);
				
				GLES32.glUniform1f(perVertex_materialShinessUniform, materialShiness);
			} 
			else
			{
				GLES32.glUniform1i(perFragment_keyLPressedUniform, 1);
				
				ByteBuffer lightAmbientByteBuffer=ByteBuffer.allocateDirect(lightAmbient.length * 4);
				lightAmbientByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightAmbientByteBufferBuffer=lightAmbientByteBuffer.asFloatBuffer();
				lightAmbientByteBufferBuffer.put(lightAmbient);
				lightAmbientByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perFragment_lightAmbientUniform, 1, lightAmbientByteBufferBuffer);
				
				ByteBuffer lightDiffuseByteBuffer=ByteBuffer.allocateDirect(lightDiffuse.length * 4);
				lightDiffuseByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightDiffuseByteBufferBuffer=lightDiffuseByteBuffer.asFloatBuffer();
				lightDiffuseByteBufferBuffer.put(lightDiffuse);
				lightDiffuseByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perFragment_lightDiffuseUniform, 1, lightDiffuseByteBufferBuffer);
				
				ByteBuffer lightSpecularByteBuffer=ByteBuffer.allocateDirect(lightSpecular.length * 4);
				lightSpecularByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightSpecularByteBufferBuffer=lightSpecularByteBuffer.asFloatBuffer();
				lightSpecularByteBufferBuffer.put(lightSpecular);
				lightSpecularByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perFragment_lightSpecularUniform, 1, lightSpecularByteBufferBuffer);
				
				ByteBuffer lightPositionByteBuffer=ByteBuffer.allocateDirect(lightPosition.length * 4);
				lightPositionByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer lightPositionByteBufferBuffer=lightPositionByteBuffer.asFloatBuffer();
				lightPositionByteBufferBuffer.put(lightPosition);
				lightPositionByteBufferBuffer.position(0);
				GLES32.glUniform4fv(perFragment_lightPositionUniform, 1, lightPositionByteBufferBuffer);
				
				ByteBuffer materialAmbientByteBuffer=ByteBuffer.allocateDirect(materialAmbient.length * 4);
				materialAmbientByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer materialAmbientByteBufferBuffer=materialAmbientByteBuffer.asFloatBuffer();
				materialAmbientByteBufferBuffer.put(materialAmbient);
				materialAmbientByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perFragment_materialAmbientUniform, 1, materialAmbientByteBufferBuffer);
				
				ByteBuffer materialDiffuseByteBuffer=ByteBuffer.allocateDirect(materialDiffuse.length * 4);
				materialDiffuseByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer materialDiffuseByteBufferBuffer=materialDiffuseByteBuffer.asFloatBuffer();
				materialDiffuseByteBufferBuffer.put(materialDiffuse);
				materialDiffuseByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perFragment_materialDiffuseUniform, 1, materialDiffuseByteBufferBuffer);
				
				ByteBuffer materialSpecularByteBuffer=ByteBuffer.allocateDirect(materialSpecular.length * 4);
				materialSpecularByteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer materialSpecularByteBufferBuffer=materialSpecularByteBuffer.asFloatBuffer();
				materialSpecularByteBufferBuffer.put(materialSpecular);
				materialSpecularByteBufferBuffer.position(0);
				GLES32.glUniform3fv(perFragment_materialSpecularUniform, 1, materialSpecularByteBufferBuffer);
				
				GLES32.glUniform1f(perFragment_materialShinessUniform, materialShiness);
			}
		}
		else
		{
			if(false == isPerFragment)
			{
				GLES32.glUniform1i(perVertex_keyLPressedUniform, 0);
			}
			else
			{
				GLES32.glUniform1i(perFragment_keyLPressedUniform, 0);
			}
		}
		
		// bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);
		
		requestRender(); // swapbuffer
	}
}